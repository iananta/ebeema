<?php

namespace App\Constants;

class ProductType
{
    const BROCHURE = 'brochure';
    const FORM = 'form';

    const types = [
        self::BROCHURE => 'Brochure',
        self::FORM => 'Form',
    ];

}
