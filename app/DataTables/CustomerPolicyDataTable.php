<?php

namespace App\DataTables;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\Models\CustomerPolicy;
use Auth;
class CustomerPolicyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('output',function($query){
                $policyDet = json_decode($query->output);
                return $policyDet[0]->policyNo ?? 'N\A';
            })
            ->editColumn('agent.username',function($query){
                $username=$query->agent->username ?? 'N/A';
                $phonenumber=$query->agent->phone_number ?? 'N/A';
                $agentEmail=$query->agent->email ?? 'N/A';
                return '  <ul style="list-style: none">
                                <li><b>Name:</b> '.$username.'</li>
                                <li><b>Contact:</b> '.$phonenumber.'</li>
                                <li><b>Email:</b> '.$agentEmail.'</li>
                            </ul>';
            })
            ->editColumn('agent_name',function($query){
                return $query->agent->username ?? '';
            })
            ->editColumn('agent_phone',function($query){
                return $query->agent->phone_number ?? '';
            })
            ->editColumn('customer.customer_name',function($query){
                $customerName=$query->customer->customer_name ?? 'N/A';
                $customerPhone=$query->customer->phone ?? 'N/A';
                $customerEmail=$query->customer->email ?? 'N/A';
                $customerImage=$query->kyc ?? 'N/A';
                $bikeImage=getBikeImage($query->reference_number);
                $bikeBlank=$bikeImage != '#' ? 'target="_blank"' : '';
                $blueBookImage=getBluebookImage($query->reference_number);
                $blueBookBlank=$blueBookImage != '#' ? 'target="_blank"' : '';
                return '<ul style="list-style: none">
                                <li><b>Name:</b> '.$customerName.'</li>
                                <li><b>Contact:</b> '.$customerPhone.'
                                    <br> '.$customerEmail.'</li>
                                <li><b>Kyc Images:</b> <a href="'.($customerImage->photo ?? '#').'" '.($customerImage->photo !=null ? 'target="_blank"' : '#').'">photo</a> | <a href="'.($customerImage->citfrntimg ?? '#').'" '.($customerImage->citfrntimg !=null ? 'target="_blank"' : '#').'>citizen Front</a> | <a href="'.($customerImage->citbackimg ?? '#').'" '.($customerImage->citbackimg !=null ? 'target="_blank"' : '#').'>citizen Back</a> | <a href="'.($customerImage->cpmregimg ?? '#').'" '.($customerImage->cmpregimg !=null ? 'target="_blank"' : '#').'>company Register / Pan</a></li>
                                <li><b>Bluebook Image:</b> <a href="'.$bikeImage.'" '.$bikeBlank.'>open</a>
                                </li>
                                <li><b>Bike Image:</b> <a href="'.$blueBookImage.'"  '.$blueBookBlank.'>open</a>
                                </li>
                            </ul>';
            })
            ->editColumn('customer_name',function($query){
                return $query->customer->customer_name ?? '';
            })
            ->editColumn('customer_phone',function($query){
                return $query->customer->phone ?? '';
            })
             ->editColumn('customer_email',function($query){
                return $query->customer->email ?? '';
            })
            ->editColumn('for',function($query){
                if($query->CLASSID == 21)
                    return 'Motor Cycle';
                elseif($query->CLASSID == 22)
                    return 'Commercial Vehicle';
                elseif($query->CLASSID == 23)
                    return 'Private Vehicle';

            })
            ->editColumn('EXPUTILITIESAMT',function($query){
                return 'Rs'.number_format($query->EXPUTILITIESAMT, 2, '.', ',');
            })
            ->editColumn('PAIDAMT',function($query){
                return number_format($query->PAIDAMT, 2, '.', ',');
            })
            ->editColumn('payment_method',function($query){
                return ucfirst($query->payment_method) ?? 'N/A';
            })
            ->editColumn('status',function($query){
                $html='';
                $selectedOne='';
                $selectedTwo='';
                $selectedZero='';
                $selectedDisble= $query->policy_status == 2 ? "disabled" : '';
                if($query->status == 1){
                    $html='Payment Successful';
                    $selectedOne=$query->policy_status == 1 ? "selected" : '';
                }
                elseif($query->status == 2) {
                    $html='Payment Error';
                    $selectedTwo=$query->policy_status == 2 ? "selected" : '';
                }
                else{
                    $html='Pending';
                    $selectedZero=$query->policy_status == 0 ? "selected" : '';
                }
                if(\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2 && !$_GET['action']){
                    $html.='<select style="width: 150px" onchange="sendStatusToServer('.$query->id.')"
                                        id="statusValue" class="form-control"
                                        name="status" '.$selectedDisble.'>
                                    <option value="0" '.$selectedZero.'>New Policy
                                    </option>
                                    <option value="1" '.$selectedOne.'>Policy Verified
                                    </option>
                                    <option value="2" '.$selectedTwo.'>
                                        Refunded/Canceled
                                    </option>
                        </select>';
                }
                return $html;
            })
            ->editColumn('created_at',function($query){
                return $query->created_at->format('Y-m-d h:i:s');
            })
            ->addColumn('action',function($query){
                $html='';
                $policyDet = json_decode($query->output);
                $accepatanceNo=$policyDet[0]->AcceptanceNo ?? "0";
                $proformano=$policyDet[0]->proformano ?? "0";
                $refNo=$query->reference_number ?? "0";
                if(is_array($policyDet)){
                    $html.='<form action="'.route('nonLife.calculator.pdf.load').'" method="GET"
                                      enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="'.csrf_token().'">
                                    <input type="hidden" name="docid" value="'.$accepatanceNo.'">
                                    <input type="hidden" name="proformano" value="'.$proformano.'">
                                    <button class="btn btn-primary btn-sm" type="submit">Pdf Download</button>
                                </form>';
                    if($query->status == 1 && $query->image_upload_status == 0){
                        $html.='<form class="mt-2" action="'.route('nonLife.calculator.document.upload').'"
                                          method="GET"
                                          enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="'.csrf_token().'">
                                        <input type="hidden" name="ref_id" value="'.$refNo.'">
                                        <button class="btn btn-secondary btn-sm" type="submit">Upload Document</button>
                                    </form>';
                    }
                }else{
                    if($query->status == 1){
                        $html.='N/A';
                    }
                }
                return $html;
            })
            ->escapeColumns('agent_details','customer','status','action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PolicyDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CustomerPolicy $model)
    {
        //dd($_GET);
        $model=$model->with('customer','agent');
        if(\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2 )
            $model=$model;
        else if(\Auth::user()->role_id == 9){
            $model=$model->whereIn('company_id',\Auth::user()->userAgents->pluck('company_id'));
        }
        else
            $model=$model->where('user_id', Auth::user()->id)->orderByDesc('id');
        if($_GET['search']['value']){
           $search=$_GET['search']['value'];
           $model=$model->where(function($query) use($search){
                    $query->whereHas('customer',function($query) use ($search){
                        return $query->where('customer_name', 'like', '%'.$search.'%');
                    })
                    ->orWhere('output', 'like', '%' . $search . '%')
                    ->orWhere('reference_number', 'like','%'.$search. '%');
                })->orWhere(function($query) use($search){
                    $query->whereHas('agent',function($query) use ($search){
                        return $query->where('username', 'like', '%'.$search.'%');
                    });
                });

        }
        if(isset($_GET['from']) && !empty($_GET['from']) && isset($_GET['to']) && !empty($_GET['to'])){
            $model=$model->whereDate('created_at', '>=',$_GET['from']  . ' 00:00:00')->whereDate('created_at', '<=',$_GET['to'] . ' 00:00:00');
        }
        
        if(isset($_GET['action']) && (!isset($_GET['excelCustomizer']) || $_GET['excelCustomizer'] == 'limited')){
            $model=$model->offset($_GET['start']);
            $model=$model->limit($_GET['length']);
        }
        return $model->latest()->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        if(admin_role_id() == 1){
            $buttons=   [
                [
                    'excel'
                ],
                [
                    'text'=>$this->customizeOption(),
                    'className'=>'excel_customizer_wrapper'
                ]
            ];
        }else{
            $buttons=[];
        }
        return $this->builder()
                    ->setTableId('customerPolicydataTableTable')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                     ->parameters([
                        "dom"=>'<"top"<"left-col"f><"right-col"lB>>rtip',
                        'searchDelay' => 100,
                        'language' => [
                            "search" => '<i class="fa fa-search"></i>',
                            "searchPlaceholder" => "Search",
                            "sLengthMenu"=> "_MENU_",
                        ],
                        'buttons' =>$buttons
                    ]);
    }

    protected function customizeOption(){
        $html='<select name="policy" class="excel_customizer">
                    <option value="limited" selected="selected">Customize Excel</option>
                    <option value="all">All</option>
                </select>';
        return $html;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns1=[
            Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('reference_number')->title('Reference No.')->searchable(true)->orderable(false),
            Column::make('policyNo')->title('Policy No.')->data('output')->name('output')->searchable(true)->orderable(false),
            Column::make('user_id')->title('Agent Details')->data('agent.username')->name('agent.username')->searchable(true)->orderable(false)->exportable(false),
            Column::make('customer_id')->title('Customer')->data('customer.customer_name')->name('customer.customer_name')->searchable(true)->orderable(false)->exportable(false),
        ];
        if(isset($_GET['action']) && !empty($_GET['action'])){
            $columns1[]=Column::make('agent_name')->title('Agent Name')->searchable(false)->orderable(false);
            $columns1[]=Column::make('agent_phone')->title('Agent Number')->searchable(false)->orderable(false);
            $columns1[]=Column::make('customer_name')->title('Customer Name')->searchable(false)->orderable(false);
            $columns1[]=Column::make('customer_phone')->title('Customer Number')->searchable(false)->orderable(false);
            $columns1[]=Column::make('customer_email')->title('Customer Email')->searchable(false)->orderable(false);

        }
        $columns2=[
            Column::make('for')->title('For')->searchable(false)->orderable(false),
            Column::make('TYPECOVER')->title('type')->searchable(false)->orderable(false),
            Column::make('EXPUTILITIESAMT')->title('Sum Insured')->searchable(false)->orderable(false),
            Column::make('VEHICLENO')->title('Vehicle No')->searchable(true)->orderable(false),
            Column::make('PAIDAMT')->title('Paid Amount')->searchable(false)->orderable(false),
            Column::make('TRANS_DATE')->title('Transaction Date')->searchable(false)->orderable(false),
            Column::make('payment_method')->title('Payment From')->searchable(false)->orderable(false),
            Column::make('status')->title('Status')->searchable(false)->orderable(false)->exportable(false),
            Column::make('created_at')->title('Created At')->searchable(true)->orderable(false),
            Column::computed('action')
                  ->title('Download Pdf')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
        $columns=array_merge($columns1,$columns2);
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Policy_' . date('YmdHis');
    }
}
