<?php

namespace App\DataTables;

use App\Models\Lead;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CustomerlistDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('policytype',function($query){
                $policyType = $query->policy_type;
                if($policyType==1){
                   return 'Life';
                }
                elseif($policyType==2){
                   return 'Non Life';
                }else{
                   return 'Not Selected';
                }
            })
            ->addColumn('created_at',function($query){
                return $query->created_at ?? '';
            })
            ->editColumn('city',function($query){
                return $query->getCity->city_name ?? '';
            })
            ->editColumn('user.username',function($query){
                return ucfirst($query->user->username ?? '');
            })
            ->editColumn('leadtype.type',function($query){
                 return '<span class="lead-'.@$query->leadtype->type.'">'.@$query->leadtype->type.'</span>';
            })
           /* ->editColumn('verify',function($query){
                $activeRed='';
                $activeGreen='';
                if($query->verify == '0'){
                    $activeRed='active';

                }
                if($query->verify == '1'){
                    $activeGreen='active';
    
                }
                return '<span class="customer-status-wrapper"><button data-id="'.$query->id.'" data-verify="1" class="customer-status-change-button green-btn '.$activeGreen.'">Verified</button><button data-id="'.$query->id.'"  data-verify="0" class="customer-status-change-button red-btn '.$activeRed.'">Not Verify</button></span>';
            })*/
            ->addColumn('action',function($query){
                return lead_action_button($query,$from='customer');
            })
            ->escapeColumns('leadtype','verify');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Customerlist $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Lead $model)
    {
        $model=$model->getleadCustomer();
        if($_GET['search']['value']){
           $search=$_GET['search']['value'];
           $model=$model->where(function($query) use ($search){
                $query->whereHas('leadtype',function($query) use ($search){
                    return $query->where('type', 'like', '%'.$search.'%');
                })
                ->orWhere('customer_name', 'like', '%' . $search . '%')
                ->orWhere('sum_insured', 'like', '%' . $search . '%')
                ->orWhere('category','like', '%' . $search . '%');

            });
          
       }
       if(isset($_GET['customFilter'])){
            $filterData=[];
            foreach($_GET['customFilter'] as $filter){
                if($filter['value']){
                    $filterData[$filter['name']]=$filter['value'];
                }
            }
            if(isset($filterData['created_at'])){
                $model->whereDate('created_at', '=',$filterData['created_at']  . ' 00:00:00');
                unset($filterData['created_at']);
            }
            $model=$model->where($filterData);
        }
        if(isset($_GET['policy_type'])){
            if( $_GET['policy_type'] == 1 || $_GET['policy_type'] == 2 ){
                $model=$model->where('policy_type',$_GET['policy_type']);
            }else if($_GET['policy_type'] == 3){
                  $model=$model->where('policy_type',Null);
            }

        }
        $order=isset($_GET['order'][0]['dir']) ? $_GET['order'][0]['dir'] : 'desc';
        $model=$model->orderBy('updated_at',$order)->newQuery();
       if(isset($_GET['action'])){
            $model->offset($_GET['start']);
            $model->limit($_GET['length']);
       }
       return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons=  [
            [
                'text' =>'<i class="fa fa-plus"></i> ' . 'Customer',
                'className' => 'btn btn-primary c-primary-btn add-modal shadow-none mx-2',
                'data-toggle' =>'modal',
                'data-target' =>'#addModal'
            ],
            [
                'text' =>'<i class="fa fa-filter"> </i>&nbsp;' . 'Apply Filter',
                'className'=>'apply-filter'
            ],
            [
                'text' =>$this->policyFilter(),
                'className' => 'policy-btn-life',
            ],
        ];
        if(admin_role_id()== 1 ){
            $buttons[]='excel';
        }
        return $this->builder()
                    ->setTableId('customerlist_table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lBfrtip')
                    ->orderBy(0)
                    ->parameters([
                        "dom"=>'<"top"<"left-col"f><"right-col"lB>>rtip',
                        'searchDelay' => 5000,
                        'language' => [
                            "search" => '<i class="fa fa-search"></i>',
                            "searchPlaceholder" => "Search",
                            "sLengthMenu"=> "_MENU_",
                        ],
                        'buttons'      => $buttons
                    ]);
    }
    protected function policyFilter(){
        $selectLife=@$_GET['policy_type']==1 ? 'selected' :'';
        $selectNonLife=@$_GET['policy_type']==2 ? 'selected' :'';
        $selectOther=@$_GET['policy_type']==3 ? 'selected' :'';
        $nonSelected=@!isset($GET['policy_type']) ? 'selected' : '';
        $html='<select name="policy" class="policy_change">
                                                <option value="" '.$nonSelected.'>Policy Filter</option>
                                                <option value="1" '.$selectLife.'>Life</option>
                                                <option value="2" '.$selectNonLife.'>Non Life</option>
                                                <option value="3" '.$selectOther.'>Others</option>
                </select>';
        return $html;
    }


    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        $column= [
            Column::make('id')->orderable(true),
            Column::make('created_at')->title('Date Added')->data('created_at')->name('created_at')->searchable(false)->orderable(false),
            Column::make('customer_name')->searchable(true)->orderable(false),
            Column::make('city')->title('Address')->data('city')->name('city')->searchable(false)->orderable(false),
            Column::make('phone')->name('phone')->searchable(false)->orderable(false),
            Column::make('email')->name('email')->searchable(false)->orderable(false),
            Column::make('policy_type')->title('Policytype')->data('policytype')->name('policytype')->searchable(false)->orderable(false),
            Column::make('category')->title('Category')->searchable(true)->orderable(false),
            //Column::make('insurance_company_name')->title('Company')->searchable(false)->orderable(false),
            Column::make('sum_insured')->title('Sum Insured')->searchable(true)->orderable(false),
            Column::make('maturity_period')->title('Term')->searchable(false)->orderable(false),
            Column::make('leadtype_id')->title('Lead Status')->data('leadtype.type')->name('leadtype.type')->orderable(false),
            Column::make('user_id')->title('Sales Person')->data('user.username')->name('user.username')->searchable(false)->orderable(false),
        ];
        // if(auth()->user()->role_id== 1){
        //     $column [] =  Column::make('verify')->title('Verify')->searchable(true)->orderable(false);
        // }
        $column[]= Column::computed('action')
                  ->exportable(false)
                  ->width(60)
                  ->addClass('text-center');
        return $column;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Customerlist_' . date('YmdHis');
    }
}
