<?php

namespace App\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\Models\MotorCalculationData;
use App\Models\GeneralSetting;
use Auth;

class DraftPolicyDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('id',function($query){
                return '# DP-'.$query->id;
            })
            ->editColumn('agent.username',function($query){
                $username=$query->agent->username ?? '';
                $phone=$query->agent->phone_number ?? '';
                $email=$query->agent->emai ?? '';
                return $username.'/'.$phone.'/'.$email;
            })
            ->editColumn('customer.INSUREDNAME_ENG',function($query){
                return $query->customer->INSUREDNAME_ENG ?? '';
            })
            ->editColumn('EXPUTILITIESAMT',function($query){
                return 'Rs'.number_format($query->EXPUTILITIESAMT, 2, '.', ',');
            })
            ->editColumn('PAIDAMT',function($query){
                $html=' <ul>
                                <li>
                                    Rs. '.number_format($query->TOTALNETPREMIUM, 2, '.', ',').'
                                </li>
                                <li>Ref. No : '.$query->payment_ref_id ?? 'N/A'.'</li>
                            </ul>';
                return $html;
            })
            ->editColumn('created_at',function($query){
                return $query->created_at->format('d/M/Y');
            })
            ->editColumn('status',function($query){
                $html='';
                if($query->status == 1){
                    $html.='Payment Successful';
                }
                elseif($query->status == 2){
                    $html.='Payment Error' ;
                }
                else{
                    $html.='Pending';
                }
                $html.='<br><b>Payment From :'.ucfirst($query->payment_method).'</b>';
                return $html;
            })
            ->editColumn('created_at',function($query){
                return $query->created_at->format('Y-m-d h:i:s');
            })
            ->editColumn('proceed',function($query){
                $html='';
                 $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
                if (\Auth::user()->role_id == 1 || \Auth::user()->role_id == 9){
                    $html.='N/A';
                }else{
                     if($query->status == 1){
                        $html.=' <form action="'.(route('nonLife.calculator.make.draft.policy')).'" method="POST">
                                            <input type="hidden" name="_token" value="'.csrf_token().'" />
                                            <input type="hidden" name="reference_number" value="'.$query->payment_ref_id.'">
                                            <input type="hidden" name="policy_id" value="'.$query->id.'">
                                            <input type="hidden" name="status" value="'.$query->status.'">
                                            <button type="submit" class="btn btn-primary">Make Policy</button>
                                        </form>';
                    }elseif($query->status == 2 || $query->status == 0){
                        $policyPayment = json_decode($query->payment_token_details ?? '');
                        $token=json_decode($policyPayment)->TokenId ?? '0';
                        $html.=' <form action="'.route('nonLife.calculator.make.draft.policy').'" method="POST">
                                           <input type="hidden" name="_token" value="'.csrf_token().'" />
                                            <input type="hidden" name="reference_number" value="'.$query->payment_ref_id.'">
                                            <input type="hidden" name="policy_id" value="'.$query->id.'">
                                            <input type="hidden" name="status" value="'.$query->status.'">
                                            <button type="submit" class="btn btn-secondary btn-sm">Proceed to Payment</button>
                                        </form>';
                        $html.='<form class="mt-2" action="'.route('nonLife.calculator.check.policy.payment').'" method="POST">
                                            <input type="hidden" name="_token" value="'.csrf_token().'" />
                                            <input type="hidden" name="RefId" value="'.@$query->payment_ref_id.'">
                                            <input type="hidden" name="MerchantCode" value="'.$merchantCode.'">
                                            <input type="hidden" name="TokenId" value="'.$token.'">
                                            <input type="hidden" name="policy_id" value="'.@$query->id.'">
                                            <input type="hidden" name="status" value="'.@$query->status.'">
                                            <button type="submit" class="btn btn-info btn-sm" title="Check Payment Status and continue to make policy if payment done successfully !">Check Payment</button>
                                        </form>';
                    }
                }

                return $html;
            })
            ->escapeColumns('PAIDAMT','status','proceed');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\DraftPolicyDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MotorCalculationData $model)
    {
        $model=$model->with('customer','agent');
        if(\Auth::user()->role_id == 1 || \Auth::user()->role_id == 2)
            $model=$model;
        else if(\Auth::user()->role_id == 9){
            $model=$model->whereIn('company_id',\Auth::user()->userAgents->pluck('company_id'));
        }
        else
            $model=$model->where('user_id', Auth::user()->id)->orderByDesc('id');
        if($_GET['search']['value']){
           $search=$_GET['search']['value'];
            $model=$model->where(function($query) use($search){
                     $query->whereHas('agent',function($query) use ($search){
                        return $query->where('username', 'like', '%'.$search.'%');
                    })->orWhere('VEHICLENO','like','%' . $search . '%')
                     ->orWhere('created_at', 'like', '%' . $search . '%');;
                })->orWhere(function($query) use($search){
                    $query->whereHas('customer',function($query) use ($search){
                        return $query->where('INSUREDNAME_ENG', 'like', '%'.$search.'%');
                    });
                });;
        }
        $model=$model->latest()->newQuery();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('draftpolicydatatabletable')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        "dom"=>'<"top"<"left-col"f><"right-col"l>>rtip',
                        'searchDelay' => 100,
                        'language' => [
                            "search" => '<i class="fa fa-search"></i>',
                            "searchPlaceholder" => "Search",
                            "sLengthMenu"=> "_MENU_",
                        ]
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('id')->title('Id')->data('id')->name('id')->searchable(false),
            Column::make('agent_details')->title('Agent Name')->data('agent.username')->name('agent.username')->searchable(true)->orderable(false),
            Column::make('INSUREDNAME_ENG')->title('Customer')->data('customer.INSUREDNAME_ENG')->name('customer.INSUREDNAME_ENG')->searchable(true)->orderable(false),
            Column::make('TYPECOVER')->title('Type')->searchable(false)->orderable(false),
            Column::make('EXPUTILITIESAMT')->title('Sum Insured')->searchable(false)->orderable(false),
            Column::make('VEHICLENO')->title('Vehicle No')->searchable(true)->orderable(false),
            Column::make('PAIDAMT')->title('Paid Amount')->searchable(false)->orderable(false),
            Column::make('created_at')->title('Transaction Date')->searchable(false)->orderable(false),
            Column::make('status')->title('Status')->searchable(false)->orderable(false),
            Column::make('proceed')->title('Proceed')->searchable(false)->orderable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DraftPolicy_' . date('YmdHis');
    }
}
