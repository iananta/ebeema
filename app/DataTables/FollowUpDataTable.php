<?php

namespace App\DataTables;

use App\Models\Lead;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class FollowUpDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('leadsource.name',function($query){
                return $query->leadsource->name ?? '';
            })
            ->editColumn('city',function($query){
                return $query->getCity->city_name ?? '';
            })
            ->addColumn('follow_up_date',function($query){
                return '<span class="follow-up">'.@$query->remark->first()->follow_up_date.' '.@$query->remark->first()->follow_up_time.'</span>';
            })
            ->addColumn('action',function($query){
                return '<button data-follow="'.@$query->remark->first()->follow_up_date." ".@$query->remark->first()->follow_up_time.'" data-id="'.@$query->remark->first()->id.'" class="follow_up_edit"><i class="fa fa-edit"></i></button>';
            })
            ->escapeColumns('follow_up_date','action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\FollowUpDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Lead $model)
    {
        $model=$model->leadFollowup();
        $model= $model->newQuery();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('followupdatatable_table')
                    ->columns($this->getColumns())
                    ->minifiedAjax(route('getfollowupList'))
                    ->dom('rtip')
                    ->orderBy(1)
                    ->parameters([
                        'paging' => true,
                        'pageLength'=>5,
                        'searching' => true,
                        'info' => false,
                        'searchDelay' => 100,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('customer_name')->title('Full Name'),
            Column::make('leadsource_id')->title('Lead Source')->data('leadsource.name')->name('leadsource.name')->orderable(false),
            Column::make('phone')->title('Phone Number')->orderable(false)->searchable(false),
            Column::make('city')->title('Address')->orderable(false)->searchable(false),
            Column::make('profession')->title('Profession')->orderable(false)->searchable(false),
            Column::make('follow_up_date')->title('Follow Up Date')->orderable(false),
            Column::make('action')->title('Action')->searchable(false)->orderable(false)

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'FollowUp_' . date('YmdHis');
    }
}
