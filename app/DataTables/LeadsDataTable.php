<?php

namespace App\DataTables;

use App\Models\Lead;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Http\Request;

class LeadsDataTable extends DataTable
{
    //protected $actions = ['print', 'export','myCustomAction'];
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query, Request $request)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('created_at', function ($query) {
                return $query->created_at ?? '';
            })
            ->editColumn('leadsource.name', function ($query) {
                return $query->leadsource->name ?? '';
            })
            ->editColumn('leadtype.type', function ($query) {
                return '<span class="lead-' . @$query->leadtype->type . '">' . @$query->leadtype->type . '</span>';
            })
            ->editColumn('fb_lead_form_id',function($query){
                return @$query->seoform->form_name;
            })
            ->editColumn('city', function ($query) {
                return $query->getCity->city_name ?? '';
            })
            ->editColumn('user.username', function ($query) {
                return ucfirst($query->user->username ?? '');
            })
            ->editColumn('policytype', function ($query) {
                $policyType = $query->policy_type;
                if ($policyType == 1) {
                    return 'Life';
                }elseif ($policyType == 2) {
                    return 'Non Life';
                }
                else if($policyType == 3){
                    return 'Corporate Plan';
                }
                else {
                    return 'Not Selected';
                }
            })
            ->editColumn('verify',function($query){
                $activeRed='';
                $activeGreen='';
                if($query->verify == '0'){
                    $activeRed='active';

                }
                if($query->verify == '1'){
                    $activeGreen='active';

                }
                $dataInfo='data-policytype="'.$query->policy_type.'"  data-name="'.$query->customer_name.'" data-phone="'.$query->phone.'" data-email="'.$query->email.'" data-company="'.$query->insurance_company_name.'" data-category="'.$query->category.'" data-premium="'.$query->premium.'" data-product="'.@$query->product->name.'"  data-suminsured="'.$query->sum_insured.'"';
                $button='<span class="customer-status-wrapper"><button '.$dataInfo.' data-id="'.$query->id.'" data-verify="1" class="customer-status-change-button green-btn '.$activeGreen.'">Verified</button><button '.$dataInfo.' data-id="'.$query->id.'"  data-verify="0" class="customer-status-change-button red-btn '.$activeRed.'">Not Verify</button></span>';
                return ($query->is_user == 1) ? $button : '<span class="text-red">Not a customer</span>';
            })
            ->editColumn('remark', function ($query) {
                $follow = $query->remark->first();
                if (!empty($follow->remark)) {
                    return '<span tools="' . @$follow->remark . '" class="custom-tooltip remark-btn">' . \Str::limit(@$follow->remark, 40, '....') . '</span>';

                } else {
                    return '';
                }
            })
            ->addColumn('action', function ($query) {
                return lead_action_button($query, $from = 'lead');
            })
            ->escapeColumns('verify','leadtype', 'remark');

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Lead $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Lead $model)
    {
        $model = $model->getLeads();
        if ($_GET['search']['value']) {
            $search = $_GET['search']['value'];
            $model = $model->where(function ($query) use ($search) {
                $query->whereHas('seoform',function($query) use ($search){
                        return $query->where('form_name', 'like', '%'.$search.'%');
                })
                ->orWhere('customer_name', 'like', '%' . $search . '%');
            });

        }
        if(isset($_GET['customFilter'])){
            $filterData=[];
            foreach($_GET['customFilter'] as $filter){
                if($filter['value']){
                    $filterData[$filter['name']]=$filter['value'];
                }
            }
            if(isset($filterData['created_at'])){
                $model=$model->whereDate('created_at', '=',$filterData['created_at']  . ' 00:00:00');
                unset($filterData['created_at']);
            }
            $model=$model->where($filterData);
        }
        if (isset($_GET['policy_type'])) {
            if ($_GET['policy_type'] == 1 || $_GET['policy_type'] == 2) {
                $model = $model->where('policy_type', $_GET['policy_type']);
            } else if ($_GET['policy_type'] == 3) {
                $model = $model->where('policy_type',3);
            }else if($_GET['policy_type'] == 4) {
                $model=$model->where('policy_type',Null);
            }

        }
        $order = isset($_GET['order'][0]['dir']) ? $_GET['order'][0]['dir'] : 'desc';
        $model = $model->orderBy('updated_at', $order)->newQuery();
        if(isset($_GET['action']) && (!isset($_GET['excelCustomizer']) || $_GET['excelCustomizer'] == 'limited')){
            $model=$model->offset($_GET['start']);
            $model=$model->limit($_GET['length']);
        }
        return $model;

    }

    protected function customizeOption(){
        $html='<select name="policy" class="excel_customizer">
                    <option value="limited" selected="selected">Customize</option>
                    <option value="all">All</option>
                </select>';
        return $html;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons= [

            [
                'text' => '<i class="fa fa-plus"></i> ' . 'Leads',
                'className' => 'btn btn-primary c-primary-btn add-modal shadow-none mx-2',
                'data-toggle' => 'modal',
                'data-target' => '#addModal'
            ],
            [
                'text' => 'Import Lead',
                'className' => 'import-button'
            ],
            [
                'text' =>'<i class="fa fa-filter"> </i>&nbsp;' . 'Apply Filter',
                'className'=>'apply-filter'
            ],
            [
                'text' => $this->policyFilter(),
                'className' => 'policy-btn-life',
            ],

        ];
        if(admin_role_id() == 1){
            $buttons[]=   [
                [
                    'excel'
                ],
                [
                    'text'=>$this->customizeOption(),
                    'className'=>'excel_customizer_wrapper'
                ]
            ];
        }
        return $this->builder()
            ->setTableId('leads_table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('lBfrtip')
            ->orderBy(0)
            ->parameters([
                "dom" => '<"top"<"left-col"f><"right-col"lB>>rtip',
                'searchDelay' => 5000,
                'pageLength'=>50,
                'language' => [
                    "search" => '<i class="fa fa-search"></i>',
                    "searchPlaceholder" => "Search",
                    "sLengthMenu" => "_MENU_",
                ],
                'buttons' =>$buttons
            ]);
    }

    protected function policyFilter()
    {
        $selectLife = @$_GET['policy_type'] == 1 ? 'selected' : '';
        $selectNonLife = @$_GET['policy_type'] == 2 ? 'selected' : '';
        $selectCorporate= @$_GET['policy_type'] == 3 ? 'selected' : '';
        $nonSelected = @!isset($GET['policy_type']) ? 'selected' : '';
        $selectOther = @$_GET['policy_type'] == 4 ? 'selected' : '';

        $html = '<select name="policy" class="policy_change">
            <option value="" ' . $nonSelected . '>Policy Filter</option>
            <option value="1" ' . $selectLife . '>Life</option>
            <option value="2" ' . $selectNonLife . '>Non Life</option>
            <option value="3" ' . $selectCorporate . '>Corporate Plan</option>
            <option value="4" ' . $selectOther. '>Others</option>
        </select>';
        return $html;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column=[
           Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
           Column::make('created_at')->title('Date Added')->data('created_at')->name('created_at')->searchable(false)->orderable(false),
           Column::make('customer_name')->searchable(true)->orderable(false),
           Column::make('city')->title('Address')->data('city')->name('city')->orderable(false),
           Column::make('phone')->searchable(false)->orderable(false),
           Column::make('policy_type')->title('Policy Type')->data('policytype')->name('policytype')->searchable(false)->orderable(false),
           Column::make('category')->title('Categogy')->searchable(true)->orderable(false),
           Column::make('sum_insured')->title('Sum Insured')->orderable(false),
           Column::make('leadtype_id')->title('Lead Status')->data('leadtype.type')->name('leadtype.type')->orderable(false),
           Column::make('fb_lead_form_id')->title('Fb Lead Form Name')->orderable(false)->searchable(true),



        ];
        if(control('view-all-lead-table')){
            $column[]=Column::make('user_id')->title('Sales Person')->data('user.username')->name('user.username')->searchable(false)->orderable(false);
        }
         if(auth()->user()->role_id== 1){
            $column [] =  Column::make('verify')->title('Verify')->searchable(true)->orderable(false);
        }
        $column[]= Column::make('remark')->title('Latest Remark')->data('remark')->name('remark')->searchable(false)->orderable(false);
        if(!isset($_GET['action'])){
            $column[]= Column::computed('action')
           ->exportable(true)
           ->width(60)
           ->addClass('text-center');
        }
        return $column;
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Leads_' . date('YmdHis');
    }

}
