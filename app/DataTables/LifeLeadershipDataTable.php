<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class LifeLeadershipDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $count=0;
        if(isset($_GET['start']) && $_GET['start'] == 0){
            \Session::put('count',$count);
        }
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('phone_number',function($query){
                return $query->phone_number ?? '';
            })
            ->addColumn('image',function($query){
                $count=\Session::get('count') + 1;
                \Session::put('count',$count);
                //if(!isset($_GET['start']) || $_GET['start'] == 0){
                    if($count == 1){
                        return '<span class="position-image"><img src=" '.asset("backend/img/first.svg").'" /></span>';
                    }
                    else if($count == 2){
                        return '<span class="position-image"><img src=" '.asset("backend/img/second.png").'" /></span>';

                    }else if($count == 3){
                        return '<span class="position-image third-img"><img src="'.asset('backend/img/first.svg').'" /></span>';
                    }else{
                        return null;
                    }
               // }
            })
            ->editColumn('username',function($query){
                return ucfirst($query->username);
            })
            ->editColumn('customers_sum_premium',function($query){
                return 'Rs. '.$query->customers_sum_premium;
            })
            ->escapeColumns('image','customers_sum_premium');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\LifeLeadershipDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->withSum('customers','premium')->where("role_id",6)->orderBy('id','desc')->orderBy('customers_sum_premium','desc')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('lifeleadershipdatatableTable')
                    ->columns($this->getColumns())
                    ->minifiedAjax(route('getLifeLeadershipList'))
                    ->dom('rtip')
                    ->orderBy(1)
                    ->parameters([
                        'paging' => true,
                        'pageLength'=>5,
                        'searching' => true,
                        'info' => false,
                        'searchDelay' => 100,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')->title('Position')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('image')->title('')->searchable(false)->orderable(true),
            Column::make('username')->title('Full Name')->searchable(false)->orderable(false),
            Column::make('phone_number')->title('Phone No')->searchable(false)->orderable(false),
            Column::make('customers_sum_premium')->title('Total Sales')->searchable(false)->orderable(false)
          
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'LifeLeadership_' . date('YmdHis');
    }
}
