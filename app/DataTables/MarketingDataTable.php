<?php

namespace App\DataTables;

use App\Models\Lead;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MarketingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('created_at',function($query){
                return $query->created_at ?? '';
            })
            ->escapeColumns('leadtype');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\MarketingDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Lead $model)
    {
        $model=$model->whereNotNull('fb_lead_form_id')->orWhereNotNull('fb_lead_id');
        if(\Auth::user()->role_id != 1){
            $userId=\Auth::user()->id;
            $model=$model->where('user_id',$userId);
        }
        if($_GET['search']['value']){
           $search=$_GET['search']['value'];
           $model=$model->where(function($query) use ($search){
                $query->whereHas('leadtype',function($query) use ($search){
                    return $query->where('type', 'like', '%'.$search.'%');
                })
                ->orWhere('customer_name', 'like', '%' . $search . '%')
                ->orWhere('sum_insured', 'like', '%' . $search . '%')
                ->orWhere('category','like', '%' . $search . '%');

            });
       }
       $model=$model->orderBy('id','desc')->newQuery();
       if(isset($_GET['action'])){
            $model->offset($_GET['start']);
            $model->limit($_GET['length']);
       }
       return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('marketingdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lfrtip')
                    ->orderBy(1) 
                    ->parameters([
                        "dom"=>'<"top"<"left-col"f><"right-col"l>>rtip',
                        'searchDelay' => 100,
                        'language' => [
                            "search" => '<i class="fa fa-search"></i>',
                            "searchPlaceholder" => "Search",
                        ],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
       return[
                    Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
                    Column::make('created_at')->title('Date Added')->data('created_at')->name('created_at')->searchable(false)->orderable(false),
                    Column::make('customer_name')->searchable(true)->orderable(false),
                    Column::make('city')->title('Address')->data('city')->name('city')->searchable(false)->orderable(false),
                    Column::make('category')->title('Categogy')->searchable(true)->orderable(false),
                    Column::make('sum_insured')->title('Sum Insured')->data('sum_insured')->name('sum_insured'),
                    Column::make('fb_lead_form_id')->title('Fb Lead Form Id')->searchable(false)->orderable(false),
                    Column::computed('fb_lead_id')
                          ->exportable(true)
                          ->width(60)
                          ->addClass('text-center'),
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Marketing_' . date('YmdHis');
    }
}
