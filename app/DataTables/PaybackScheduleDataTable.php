<?php

namespace App\DataTables;

use App\Models\PaybackSchedule;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PaybackScheduleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('companies.name',function($query){
                return $query->companies->name;
            })
            ->editColumn('products.name',function($query){
                return $query->products->name;
            })
            ->addColumn('action',function($query){
                return '<a href="'.route('admin.payback.edit',['id'=>$query->id]).'" id="'.$query->id.'"
                               title="Edit ">
                                <button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button>
                            </a>&nbsp;
                            <a href="javascript:;" title="Delete Payment" class="delete-payment"
                               id="'.$query->id.'">
                                <button type="button" class="btn btn-danger btn-flat"><i
                                        class="fa fa-trash"></i></button>
                            </a>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PaybackScheduleDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PaybackSchedule $model)
    {
        $model=$model::with('products','companies');
        if ($_GET['search']['value']) {
            $search = $_GET['search']['value'];
            $model = $model->whereHas('companies',function($query) use ($search){
                                    return $query->where('name', 'like', '%'.$search.'%');
                                 })
                        ->orWhereHas('products',function($query) use ($search){
                            return $query->where('name','like','%'.$search.'%');
                        });
        }
        if(isset($_GET['customFilter'])){
            $filterData=[];
            foreach($_GET['customFilter'] as $filter){
                if($filter['value']){
                    $filterData[$filter['name']]=$filter['value'];
                }
            }
            $model=$model->where($filterData);
        }
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
         $buttons= [
           
            [
                'text' => '<i class="fa fa-plus"></i> ' . 'Create',
                'className' => 'payback-create-btn',
            ],
            [
                'text' =>'<i class="fa fa-filter"> </i>&nbsp;' . 'Apply Filter',
                'className'=>'apply-filter'
            ],
        ];
        return $this->builder()
                    ->setTableId('paybackscheduledatatableTable')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        "dom" => '<"top"<"left-col"f><"right-col"lB>>rtip',
                        'searchDelay' => 5000,
                        'pageLength'=>10,
                        'language' => [
                            "search" => '<i class="fa fa-search"></i>',
                            "searchPlaceholder" => "Search",
                            "sLengthMenu" => "_MENU_",
                        ],
                        'buttons' =>$buttons
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('term_year')->title('Term Year')->searchable(false)->orderable(false),
            Column::make('payback_year')->title('PayBack Schedule')->searchable(false)->orderable(false),
            Column::make('rate')->title('Rate')->searchable(false)->orderable(false),
            Column::make('product_id')->title('Product Name')->data('products.name')->name('products.name')->searchable(true)->orderable(false),

            Column::make('company_id')->title('Company Name')->data('companies.name')->name('companies.name')->searchable(true)->orderable(false),
            Column::computed('action')->title('Company Name')
            ->exportable(false)
            ->printable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PaybackSchedule_' . date('YmdHis');
    }
}
