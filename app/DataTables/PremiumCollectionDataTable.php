<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PremiumCollectionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('action',function($query){
                return '<button data-id="'.$query->id.'" class="btn btn-success btn-sm view-premium-collection"><i class="fa fa-eye"></i></button>';
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PremiumCollectionDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $model=$model->where('role_id',6)->with('premiumPaidByCustomer');
        if ($_GET['search']['value']) {
            $search = $_GET['search']['value'];
            $model=$model->where('username','like', '%' . $search . '%');
        }
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('premiumcollectiondatatableTable')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('frtip')
                    ->orderBy(1) 
                    ->parameters([
                    "dom" => '<"top"<"left-col"f><"right-col"l>>rtip',
                    'searchDelay' => 100,
                    'language' => [
                        "search" => '<i class="fa fa-search"></i>',
                        "searchPlaceholder" => "Search",
                        "sLengthMenu" => "_MENU_",
                    ],
                ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('username')->title('Sales Person')->searchable(true)->orderable(false),
            Column::make('phone_number')->title('Phone No')->data('phone_number')->name('phone_number')->searchable(false)->orderable(false),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PremiumCollection_' . date('YmdHis');
    }
}
