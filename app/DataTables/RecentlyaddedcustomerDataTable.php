<?php

namespace App\DataTables;

use App\Models\Lead;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RecentlyaddedcustomerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('city',function($query){
                return $query->getCity->city_name ?? '';
            })
            ->editColumn('leadsource.name',function($query){
                return $query->leadsource->name ?? '';
            })
            ->editColumn('premium_paid_by_customer_sum_premium_amount',function($query){
                return ($query->premium_paid_by_customer_sum_premium_amount) ? 'Rs.'.$query->premium_paid_by_customer_sum_premium_amount : 'Rs.0';
            })
            ->addColumn('created_at',function($query){
                return $query->created_at ?? '';
            })
            ->addColumn('action',function($query){
                return '<button data-id="'.$query->id.'" data-from="customer" title="Edit Lead" class="btn btn-primary btn-sm edit-modal-btn action-btn"><i class="fa fa-edit"></i></button>';
            })
            ->escapeColumns('premium_paid_by_customer_sum_premium_amount','action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\RecentlyaddedcustomerDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Lead $model)
    {
        $model=$model->getleadCustomer();
        if(isset($_GET['endDate']) && !empty($_GET['endDate'])){
           $model=$model->whereDate('created_at','<=',$_GET['endDate']);
        }
        $model=$model->orderBy('created_at','desc')->newQuery();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('recentlyaddedcustomerdatatable_table')
                    ->columns($this->getColumns())
                    ->minifiedAjax(route('getrecentlyaddCustomer'))
                    ->dom('rtip')
                    ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')->title('S.No')->data('DT_RowIndex')->name('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('created_at')->title('Date Added')->data('created_at')->name('created_at')->searchable(false)->orderable(false),
            Column::make('customer_name')->title('Full Name')->orderable(false),
            Column::make('phone')->title('Phone Number')->orderable(false),
            Column::make('leadsource_id')->title('Lead Source')->data('leadsource.name')->name('leadsource.name')->orderable(false),
            Column::make('city')->title('Address')->orderable(false),
            Column::make('profession')->title('Profession')->orderable(false),
            Column::make('premium_paid_by_customer_sum_premium_amount')->title('Amount')->data('premium_paid_by_customer_sum_premium_amount')->name('premium_paid_by_customer_sum_premium_amount')->orderable(false),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Recentlyaddedcustomer_' . date('YmdHis');
    }
}
