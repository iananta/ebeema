<?php

namespace App\DataTables;

use App\Models\SeoForm;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SeoFormDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        \Session::put('rownum',0);
        return datatables()
            ->eloquent($query)
            ->addColumn('sno',function($query){
                $rownum = \Session::get('rownum') + 1;
                \Session::put('rownum', $rownum);
                return $rownum ?? '';
            })
            ->addColumn('action',function($query){
                $html = '
                     <a href="'.route('admin.seoForm.edit',['id'=>$query->id]).'"
                        class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                    <a href="javascript:;" title="Delete leadsource"
                    class="btn btn-danger delete-seo-form btn-sm"
                    data-id="' . $query->id . '"><i class="fa fa-trash"></i>
                    </a>';
       
                return $html;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\SeoFormDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SeoForm $model)
    {
        if ($_GET['search']['value']) {
            $search = $_GET['search']['value'];
            $model = $model->where(function ($query) use ($search) {
                    $query->where('form_name', 'like', '%' . $search . '%')
                    ->orWhere('form_id', 'like', '%' . $search . '%');
            });

        }
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {

        $buttons= [
            [
                'text' => '<i class="fa fa-plus"></i> ' . 'Create',
                'className' => 'seoform-create-btn',
                'action'=>'window.location = '.route('admin.seoForm.create').''
            ],
        ];
        return $this->builder()
                    ->setTableId('seoformdatatableTable')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                   ->parameters([
                        "dom" => '<"top"<"left-col"f><"right-col"lB>>rtip',
                        'searchDelay' => 100,
                        'language' => [
                            "search" => '<i class="fa fa-search"></i>',
                            "searchPlaceholder" => "Search",
                            "sLengthMenu" => "_MENU_",
                        ],
                    ])
                   ->buttons(
                        Button::make('create')->action("window.location = '".route('admin.seoForm.create')."';"),
                       
                    );;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('sno')->name('sno'),
            Column::make("form_name")->title('Form Name'),
            Column::make('form_id')->title('Form Id'),
            Column::make('created_at'),
            Column::computed('action')
                        ->exportable(false)
                        ->width(60)
                        ->addClass('text-center'),

            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'SeoForm_' . date('YmdHis');
    }
}
