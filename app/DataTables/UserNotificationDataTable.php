<?php

namespace App\DataTables;

use App\Models\UserActivityLog;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserNotificationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        if(isset($_GET['start']) && $_GET['start'] == 0){
            \Session::put('rownum',0);
        }
        return datatables()
            ->eloquent($query)
            ->addColumn('sno',function(){
                $rownum=\Session::get('rownum') + 1;
                \Session::put('rownum',$rownum);
                return $rownum;
            })
                ->editColumn('user.username',function($query){
                    return $query->user->username ?? '';
                })
                ->editColumn('created_at',function($query){
                    return $query->created_at->format('Y-m-d h:i:s');
                });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Userlog $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserActivityLog $model)
    { 
        $model=$model->with('user');
        if($_GET['search']['value'] && !empty($_GET['search']['value'])){
            $search=$_GET['search']['value'];
            $model=$model->where(function($query) use ($search){
                $query->whereHas('user',function($query) use ($search){
                    return $query->where('username', 'like', '%'.$search.'%');
                });
               
            });
        }
        $model=$model->orderBy('id','desc')->newQuery();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('usernotification-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('flrtip')
                    ->orderBy(1)
                    ->parameters([
                        "dom"=>'<"top"<"left-col"f><"right-col"l>>rtip',
                        'paging' => true,
                        'pageLength'=>10,
                        'searching' => true,
                        'info' => false,
                        'searchDelay' => 100,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if(auth()->user()->id == 1){
        return [
            Column::make('sno')->title('S.No'),
            Column::make('user.username')->title('User')->orderable(false)->searchable(true),
            Column::make('RESPONSE')->title('notification')->orderable(false)->searchable(false),
            Column::make('created_at')->orderable(false)->searchable(false),
        ];
    }else{
        return [
            Column::make('sno')->title('S.No'),
            Column::make('RESPONSE')->title('notification')->orderable(false)->searchable(false),
            Column::make('created_at')->orderable(false)->searchable(false),
        ];

    }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Userlog_' . date('YmdHis');
    }
}
