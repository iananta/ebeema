<?php

namespace App\DataTables;

use App\Models\Video;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class VideoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        if(isset($_GET['start']) && $_GET['start'] == 0){
            \Session::put('rownum',0);
        }
        return datatables()
            ->eloquent($query)
            ->addColumn('sno',function(){
                $rownum=\Session::get('rownum') + 1;
                \Session::put('rownum',$rownum);
                return $rownum;
            })
            ->editColumn('title',function($query){
                return $query->title ?? '';
            })
            ->editColumn('file',function($query){
                return $query->file ?? '';
            })
            ->addColumn('action',function($query){
                $html = '
                     <a href="'.route('admin.tape.show',['id'=>$query->id]).'"
                        class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                  ';
                return $html;
            });
    }
    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Video $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Video $model)
    {
        if($_GET['search']['value'] && !empty($_GET['search']['value'])){
            $search=$_GET['search']['value'];
            $model->where('title', 'like', '%'.$search.'%');
        }
       return $model->orderBy('id','desc')->newQuery();
        
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons= [
            [
                'text' => '<i class="fa fa-plus"></i> ' . 'Add new',
                'className' => 'add-video',
                
            ],
          
        ];
        return $this->builder()
        ->setTableId('video-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('lBfrtip')
        ->orderBy(0)
        ->parameters([
            "dom" => '<"top"<"left-col"f><"right-col"lB>>rtip',
            'searchDelay' => 1000,
            'pageLength'=>50,
            'language' => [
                "search" => '<i class="fa fa-search"></i>',
                "searchPlaceholder" => "Search",
                "sLengthMenu" => "_MENU_",
                "buttons" => ['reload'],
                [
                    'text' => '<i class="fa fa-plus"></i> ' . 'Add new',
                    'className' => 'add-video',
                    
                ],
            ],
            'buttons' =>$buttons
          
        ]);
      
      
       
            
       
}


    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('sno')->title('S.No'),
            Column::make('title')->orderable(false)->searchable(true),
            Column::make('file')->orderable(false)->searchable(false),
            Column::make('created_at')->orderable(false)->searchable(false),
            Column::computed('action')
            ->exportable(false)
            ->width(60)
            ->addClass('text-center'),
        ];
    }
   

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Video_' . date('YmdHis');
    }
}
