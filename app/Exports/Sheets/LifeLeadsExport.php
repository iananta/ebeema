<?php

namespace App\Exports\Sheets;

use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;

class LifeLeadsExport implements FromView, WithTitle
{
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    public function view(): View
    {
        $data['leads'] = $this->builder->where('policy_cat', 1)->get();
        return view('Backend.Leads.AllLeads.lead-list', $data);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Life';
    }
}
