<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class OtherLeadsExport implements FromView, WithTitle
{
    /**
     * @var Builder
     */
    private $builder;

    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $data['leads'] = $this->builder->where('policy_cat', null)->get();
        return view('Backend.Leads.AllLeads.lead-list', $data);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Others';
    }
}
