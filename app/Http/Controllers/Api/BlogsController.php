<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Blogcate;
use Illuminate\Http\Request;
use Response;
use Exception;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blogs(Request $request)
    {
        try {
            $data['blogCatagories'] = Blogcate::latest()->get();
            if (isset($request['search'])){
                $data['blogs'] = Blog::where('title', 'like', '%' . $_GET['search'] . '%')->get();
            }
            else {
                $data['blogs'] = Blog::with('category')->latest()->get();
            }
            if ($data) {
                return Response::json(["response_code" => 1, 'success' => true, 'message' => "data fetch successfully.", 'data' => $data], 200);
            }
            return Response::json(["response_code" => 0, 'success' => false, 'message' => "Something went wrong."], 500);
        } catch (Exception $e) {
            return Response::json(["response_code" => 0, 'success' => false, "message" => "Something went wrong./" . $e->getMessage()], 400);
        }
    }
    public function detail($id)
    {
        $data['detail'] = Blog::findOrfail($id);
        if($data['detail']){
            return Response::json(["response_code" => 1, 'success' => true, 'message' => "data fetch successfully.", 'data' => $data], 200);
        }
        return Response::json(["response_code" => 0, 'success' => false, 'message' => "Something went wrong."], 500);
    }
}
