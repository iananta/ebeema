<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Lead;
use Response;
use mysql_xdevapi\Exception;
use Illuminate\Validation\Validator;

class LeadController extends Controller
{
    public function storeLead(Request $request)
    {
        try {
            $response = array('response_code' => 0, 'success' => false);
            $validator = \Validator::make($request->all(),
                [
                    'customer_name' => 'required',
                    'leadsource_id' => 'required',
                    'email' => 'required',
                    'user_id' => 'required',
                    'phone' => 'required',
                ],
                [
                    'customer_name.required' => 'Name Cannot Be Empty!!',
                    'leadsource_id.required' => 'Lead source Cannot Be Empty!!',
                    'email.required' => 'Email address Cannot Be Empty!!',
                    'user_id.required' => 'User Or Person who added it Cannot Be Empty!!',
                    'phone.required' => 'Mobile number Cannot Be Empty!!',
                ]
            );

            if ($validator->fails()) {
//                $errors = $validator->errors();
//                return $errors->toJson();
                $response['response'] = $validator->messages();
                return ($response);
            } else {
                $data = $request->except('_token', 'policy_doc', 'identity_doc');
//                return Response::json(["response_code" => 0, 'success' => true, "data" => $data, 'message' => "Lead added successfully."], 200);
//                dd($data, "hello");
                $leads = Lead::create($data);
                if ($leads) {
                    return Response::json(["response_code" => 1, 'success' => true, 'message' => "Lead added successfully."], 200);
                }
                return Response::json(["response_code" => 0, 'success' => false, 'message' => "Something went wrong."], 500);
            }
        } catch (Exception $e) {
//            dd("hello");
            return Response::json(["response_code" => 0, 'success' => false, "message" => "Lead not created." . $e->getMessage()], 400);
        }
    }
}
