<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CalculationController;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Response;
use Exception;
use Illuminate\Validation\Validator;

class LifeCalculatorController extends Controller
{
    public function lifeCalculationCategory(Request $request)
    {
        try {
            $data['catagories'] = ProductCategory::get(['name', 'min_age', 'max_age', 'min_term', 'max_term', 'min_sum', 'max_sum', 'category_code', 'child_age_range', 'proposer_age_range', 'husband_age_range', 'wife_age_range', 'status']);
            if ($data) {
                return Response::json(["response_code" => 1, 'success' => true, 'message' => "data fetch successfully.", 'data' => $data], 200);
            }
            return Response::json(["response_code" => 0, 'success' => false, 'message' => "Something went wrong."], 500);
        } catch (Exception $e) {
            return Response::json(["response_code" => 0, 'success' => false, "message" => "Something went wrong./" . $e->getMessage()], 400);
        }
    }

    public function lifeCalculation(Request $request)
    {
        try {
            $request->merge(['api_calculation' => 1]);
            $response = array('response_code' => 0, 'success' => false);
            $validator = \Validator::make($request->all(),
                [
                    'category' => 'required',
                    'age' => 'required',
                    'term' => 'required',
                    'sum_assured' => 'required',
                    'invest' => 'required',
                ],
                [
                    'category.required' => 'Category Cannot Be Empty!!',
                    'age.required' => 'Age Cannot Be Empty!!',
                    'term.required' => 'Term value Cannot Be Empty!!',
                    'sum_assured.required' => 'Sum Assured Amount Cannot Be Empty!!',
                    'invest.required' => 'Investment Per Year Cannot Be Empty!!',
                ]
            );

            if ($validator->fails()) {
                $response['response'] = $validator->messages();
                return ($response);
            } else {
                $data = $request->all();
                $calcuation = new CalculationController;
                $data['products'] = $calcuation->frontendCompare($request);
                if ($data) {
                    return Response::json(["response_code" => 1, 'success' => true, 'message' => "Calculation done successfully.", 'data' => $data], 200);
                }
                return Response::json(["response_code" => 0, 'success' => false, 'message' => "Something went wrong / Calculation error."], 500);
            }
        } catch (Exception $e) {
            return Response::json(["response_code" => 0, 'success' => false, "message" => "Calculation error." . $e->getMessage()], 400);
        }
    }
}
