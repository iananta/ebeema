<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\PremiumPay;
use App\Models\CronJobReminderLog;
use App\Models\GeneralSetting;
use App\Models\PremiumPaidByCustomer;
use App\Models\SmsResponse;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Mail;
use Response;
use DateTime;
use Illuminate\Validation\Validator;

class ReminderController extends Controller
{
    public function PremiumPayableCheck(Request $request)
    {
        try {
            $parameters = GeneralSetting::where('key', "premium_reminder_parameters")->first()->value;
            $requestedData = json_decode($parameters);
            $days = explode(",", $requestedData->days);
            $sms = $requestedData->sms;
            $email = $requestedData->email;
            $smsCount = 0;
            $mailCount = 0;
            if ($days == null || $sms <= 0 && $email <= 0) {
                $response = array('response_code' => 0, 'success' => false, 'response' => 'input parameters missing');
                return $response;
            }
            $date = date('Y-m-d');
//            dd($days,$sms,$email,$date);
            foreach ($days as $day) {
                $allPremiums = PremiumPaidByCustomer::where('verify', 1)->with('planselected');
                $selectedAfterDate = (new DateTime($date))->modify('+' . $day . 'day')->format('Y-m-d');
                $data[$day . "dayList"] = $allPremiums->where('next_paid_date', '=', $selectedAfterDate)->latest()->get();
                foreach ($data[$day . "dayList"] as $dt) {
                    $dayy = $day > 0 ? $day." days remaining" : "today";
                    $premiumPaidLists = PremiumPaidByCustomer::where('planselected_id', $dt->planselected_id)->where('lead_id', $dt->lead_id)->where('id', "!=", $dt->id)->pluck('paid_date');
                    if (!in_array($dt->next_paid_date, $premiumPaidLists->toArray())) {
                        if ($email == 1 && $data[$day . "dayList"] != null) {
                            $mailCount++;
                            Mail::to("azizdulal.ad@gmail.com")
                                ->queue(new PremiumPay($dt,$dayy));
                        }
                        if ($sms == 1 && $data[$day . "dayList"] != null) {
                            $curl = curl_init();
                            $apiBasicUser = base64_encode("demohtptestint:xbse(3da");
                            $customer_phone = '9813312456';
                            if ($dt && GeneralSetting::where('key', 'sms_activation')->first()->value && GeneralSetting::where('key', 'sms_activation')->first()->value == 1 && $customer_phone) {
                                $smsCount++;
                                $customer_name = strtok($dt->planselected->name, " ");
                                $message = 'Hi%20' . $customer_name . ',%0aYour%20due%20date%20is%20' . $dt->next_paid_date . '%20i.e%20'.preg_replace('/\s+/', '%20', $dayy).'%20for%20' . $dt->planselected->category . '%20policy%20renewal%20of%20NRs.%20' . $dt->premium_amount . '%20for%20policy%20number:' . $dt->planselected->policy_number . '.%0aThank%20you%20for%20choosing%20Ebeema.';
//                        dd($message);
                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "https://sms.isplservices.com/smpp/sendsms?to=977" . $customer_phone . "&from=ISPL&text=" . $message,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => '',
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 10,
                                    CURLOPT_FOLLOWLOCATION => true,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => 'GET',
                                    CURLOPT_HTTPHEADER => array(
                                        'Authorization: Basic ' . $apiBasicUser
                                    ),
                                ));
//dd($curl);
                                $smsReply = curl_exec($curl);
//            dd ($smsReply);
                                if ($smsReply) {
                                    $smsDta['customer_id'] = $dt->lead_id ?? null;
                                    $smsDta['reference_number'] = "premium-pay";
                                    $smsDta['message'] = $message ?? 'N/A';
                                    $smsDta['response'] = $smsReply ?? 'N/A';
                                    SmsResponse::create($smsDta);
                                }
                            }
                            curl_close($curl);
                        }
                    }
                }
            }
//            dd($mailCount,$smsCount);
//            return ($data);
            if ($mailCount > 0 || $smsCount > 0) {
                $smsText = $sms ? $smsCount . " SMS " : '';
                $mailText = $email ? ("/ " . $mailCount . " Mail ") : '';
                CronJobReminderLog::create([
                    "type" => "reminder",
                    "sms_sent" => $smsCount,
                    "mail_sent" => $mailCount,
                    "output" => $smsText . $mailText . "for Premium Payable sent successfully."
                ]);
                return Response::json(["response_code" => 1, 'success' => true, 'message' => $smsText . $mailText . "for Premium Payable sent successfully."], 200);
            }
            CronJobReminderLog::create([
                "type" => "reminder",
                "sms_sent" => $smsCount,
                "mail_sent" => $mailCount,
                "output" => "Something went wrong / No any datas found."
            ]);
            return Response::json(["response_code" => 0, 'success' => false, 'message' => "Something went wrong / No any datas found."], 500);
        } catch (Exception $e) {
            return Response::json(["response_code" => 0, 'success' => false, "message" => "Something went wrong." . $e->getMessage()], 400);
        }
    }
}
