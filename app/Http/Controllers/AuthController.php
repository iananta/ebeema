<?php

namespace App\Http\Controllers;

use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TokenStore\TokenCache;
use Session;
use Auth;

class AuthController extends Controller
{
    public function signin()
    {
        // Initialize the OAuth client
        $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => config('azure.appId') ?? Auth::user()->outlookCredentials->OAUTH_APP_ID,
            'clientSecret' => config('azure.appSecret') ?? Auth::user()->outlookCredentials->OAUTH_APP_SECRET,
            'redirectUri' => config('azure.redirectUri'),
            'urlAuthorize' => config('azure.authority') . config('azure.authorizeEndpoint'),
            'urlAccessToken' => config('azure.authority') . config('azure.tokenEndpoint'),
            'urlResourceOwnerDetails' => '',
            'scopes' => config('azure.scopes')
        ]);
// dd($oauthClient);
        $authUrl = $oauthClient->getAuthorizationUrl();

        // Save client state so we can validate in callback
        session(['oauthState' => $oauthClient->getState()]);

        // Redirect to AAD signin page
        return redirect()->away($authUrl);
    }

    public function callback(Request $request)
    {
//        return session::get('oauthState');
//        dd($request->all());
        try {
            // Validate state
            $expectedState = session('oauthState');
            $request->session()->forget('oauthState');
            $providedState = $request->query('state');

            if (!isset($expectedState)) {
                // If there is no expected state in the session,
                // do nothing and redirect to the home page.
                return redirect('/user/calendar');
            }

            if (!isset($providedState) || $expectedState != $providedState) {
                return redirect('/user/calendar')
                    ->with('error', 'Invalid auth state')
                    ->with('errorDetail', 'The provided auth state did not match the expected value');
            }

            // Authorization code should be in the "code" query param
            $authCode = $request->query('code');
            if (isset($authCode)) {
                // Initialize the OAuth client
                $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
                    'clientId' => config('azure.appId'),
                    'clientSecret' => config('azure.appSecret'),
                    'redirectUri' => config('azure.redirectUri'),
                    'urlAuthorize' => config('azure.authority') . config('azure.authorizeEndpoint'),
                    'urlAccessToken' => config('azure.authority') . config('azure.tokenEndpoint'),
                    'urlResourceOwnerDetails' => '',
                    'scopes' => config('azure.scopes')
                ]);

                try {
                    // Make the token request
                    $accessToken = $oauthClient->getAccessToken('authorization_code', [
                        'code' => $authCode
                    ]);

                    $graph = new Graph();
                    $graph->setAccessToken($accessToken->getToken());

                    $user = $graph->createRequest('GET', '/me?$select=displayName,mail,mailboxSettings,userPrincipalName')
                        ->setReturnType(Model\User::class)
                        ->execute();

                    $tokenCache = new TokenCache();
                    $tokenCache->storeTokens($accessToken, $user);

                    return redirect('/user/calendar');
                } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                    return redirect('/user/calendar')
                        ->with('error', 'Error requesting access token')
                        ->with('errorDetail', json_encode($e->getResponseBody()));
                }
            }

            return redirect('/user/calendar')
                ->with('error', $request->query('error'))
                ->with('errorDetail', $request->query('error_description'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function signout()
    {
        $tokenCache = new TokenCache();
        $tokenCache->clearTokens();
        Auth::user()->outlookCredentials->delete();
        return redirect('/user/calendar');
    }
}
