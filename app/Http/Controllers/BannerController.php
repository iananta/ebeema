<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    private $banner;

    public function __construct(Banner $banner){
        $this->banner=$banner;
    }


    /**
     * @return list of banner
     * 
     */
    public function index(){
        $data['banners']=$this->banner->all();
        return view('Backend.Banner.index')->with($data);
    }


    /**
     * @return create form 
     */
    public function create(){
        return view('Backend.Banner.form');
    }


    /**
     * @return store data in database
     * 
     */
    public function store(Request $request){
        $data=$this->validate($request,[
            'title'=>'required',
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:10048',
            'description'=>'required'
        ]);
        $this->banner->create($data);
        return redirect()->route('admin.banner.index')->with('message','Banner created successfully');

    }


    /**
     * 
     *  @return banner edit form
     */

    public function edit($id){
        $data['banner']=$this->banner->findOrFail($id);
        return view('Backend.Banner.form')->with($data);
    }

    /**
     * @return update database
     */

    public function update(Request $request,$id){
         $data=$this->validate($request,[
            'title'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg,webp|max:10048',
            'description'=>'required'
        ]);
         $banner=$this->banner->findOrFail($id);
         if($request->file('image')){
            $file_path='uploads/banner/'.$banner->image;
            if($banner->image){
                 unlink($file_path);
            }
         }
        $banner->update($data);
        return redirect()->route('admin.banner.index')->with('message','Banner updated successfully');
    }



    /**
     * 
     * @return delete form database
     */

    public function delete($id){
        $banner=$this->banner->findOrFail($id);
         if($banner->image){
            $file_path='uploads/banner/'.$banner->image;
            unlink($file_path);
         }
         $banner->delete();
         return redirect()->route('admin.banner.index')->with('message','Banner delete successfully');
    }


}
