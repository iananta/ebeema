<?php

namespace App\Http\Controllers;


use App\Models\Blog;
use App\Models\Blogcate;
use Dropbox\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class BlogController extends Controller
{
    public function index(Request $request)
    {
        if (control('frontend-dynamics')) {
            $data['blogcates'] = Blogcate::all();
            $data['blogs'] = Blog::with('category')->get();
            return view('blog.index')->with($data);

        } else {
            return view('layouts.backend.denied');
        };
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'blogcate_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move(public_path('images/blogCategory'), $filename);
            // $blogs->image = $filename;
        }
        $blog = Blog::create([
            "slug" => $request->slug ?? Str::slug($request->title, "-"),
            "blogcate_id" => $request->blogcate_id,
            "title" => $request->title,
            "description" => $request->description,
            "meta_title" => $request->meta_title,
            "meta_description" => $request->meta_description,
            "image" => $filename
        ]);
        return redirect()->back()
            ->with('success', 'Blog created successfully.');
    }

    public function edit($id)
    {

        $blogs = Blog::findorFail($id);
        $blogcates = Blogcate::latest()->get();
        //$blogcates = Blogcate::all();
        return view('blog.edit')->with('blogs', $blogs)
            ->with('blogcates', $blogcates);
    }

    public function show($id)
    {
//        $blogs = Blog::findOrFail($id)->delete();
//        return redirect()->route('blogs.index')->withSuccessMessage('Blog is deleted successfully.');
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, array(
            'slug' => 'required',
            'blogcate_id' => 'required',
            'title' => 'required',
            'description' => 'required',
        ));

        $data['slug'] = $request->slug;
        $data['title'] = $request->title;
        $data['description'] = $request->description;
        $data['blogcate_id'] = $request->blogcate_id;
        $data['meta_title'] = $request->meta_title;
        $data['meta_description'] = $request->meta_description;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '_' . $extension;
            $file->move(public_path('images/blogCategory'), $filename);
            $data['image'] = $filename;
        }
        //save
        $blogs = Blog::whereId($id)->update($data);

        return redirect()->route('blogs.index', $blogs)->withSuccessMessage('Blog updated successfully');

    }

    public function destroy($id)
    {
        $blog = Blog::findOrfail($id);
        $blog->delete();

        if ($blog) {
            return response()->json([
                'type' => 'success',
                'message' => 'Blog is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'Blog can not be deleted.'
        ], 422);
    }


}
