<?php

namespace App\Http\Controllers;

use App\Models\Blogcate;
use Illuminate\Http\Request;

class BlogcateController extends Controller
{
        public function index()
    {
        if (control('frontend-dynamics')) {
            $blogcates = Blogcate::all();
            return view('blogcate.index')->with('blogcates', $blogcates);;
        } else {
            return view('layouts.backend.denied');
        };
    }

        public function create()
    {
        // $blogcates = blogcate::all();
        // return view('blogcate.create',$blogcates);
    }

        public function store(Request $request)
    {
        $this->validate(
        $request,
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'blog category is required!!',
            ]
        );
        $blogcate = Blogcate::create($request->all());
        return redirect()->route('blogcates.index',$blogcate->id)
        ->withSuccessMessage('blog category created successfully');
    }

        public function edit( $id)
    {
        $blogcates=Blogcate::findorfail($id);
		return view('blogcate.edit',compact('blogcates'));
    }

    public function update(Request $request,$id)
    {
        try{
        $blogcates = Blogcate::find($id);
        $this->validate($request, array(
            'name' => 'required'
        ));
		$blogcates->name=$request->input('name');
		$blogcates->save();
		return redirect()->route('blogcates.index',$blogcates->id)->withSuccessMessage('Blog category has been updated successfully');
        } catch (\Exception $e) {
        return $e->getMessage();
        }
    }
     public function destroy($id)
    {
        $blog = Blogcate::findOrfail($id);
        $blog->delete();

        if ($blog) {
            return response()->json([
                'type' => 'success',
                'message' => 'Blog is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'Blog can not be deleted.'
        ], 422);
    }
}





