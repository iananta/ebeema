<?php

namespace App\Http\Controllers;

use App\Models\BonusRateEP;
use App\Models\Company;
use App\Models\CoupleAgeDifference;
use App\Models\CrcRate;
use App\Models\Feature;
use App\Models\GeneralSetting;
use App\Models\LoadingCharge;
use App\Models\PaybackSchedule;
use App\Models\Policy_age;
use App\Models\Product;
use App\Models\UploadProductPdf;
use App\Models\ProductCategory;
use App\Models\ProductFeature;
use App\Models\ProductFeatureRates;
use App\Mail\CompareResult;
use App\Models\Rate_endowment;
use App\Models\SumAssured;
use App\Models\Term;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use PDF;
use Mail;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Html;
use PhpOffice\PhpWord\TemplateProcessor;

class CalculationController extends Controller
{

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function lifeCalculation(Request $request)  //no
    {
        try {
            $data['selectedAge'] = $request->age;
            $data['selectedHusbandAge'] = $request->husband_age;
            $data['selectedWifeAge'] = $request->wife_age;
            $data['selectedChildAge'] = $request->child_age;
            $data['selectedProposersAge'] = $request->proposer_age;
            $data['selectedTerm'] = $request->term;

            $data['selectedSumAssured'] = $request->sum_assured;
            $data['selectedMop'] = $request->loading_charge;
            $data['selectedCompany'] = $request->company;
            $data['selectedProduct'] = $request->product;
            $data['features'] = $request->features;

            $product = Product::whereId($data['selectedProduct'])->first();
            $data['selectedCategory'] = $product->category;

            $data = $this->prepareAdditionalData($data, 'calculator');

            $data['subTerms'] = $this->getSubTerms($data['selectedMop']);

            $data['companies'] = $this->getRequiredData();

            return view('Backend.Life.result', $data);
        } catch (Exception $e) {
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function backendCompare(Request $request) //no
    {

                try {
            $data['clientName'] = $request->full_name;
            $data['clientEmail'] = $request->email;
            $data['selectedAge'] = $request->age;
            $data['selectedHusbandAge'] = $request->husband_age;
            $data['selectedWifeAge'] = $request->wife_age;
            $data['selectedChildAge'] = $request->child_age;
            $data['selectedProposersAge'] = $request->proposer_age;
            $data['selectedTerm'] = $request->term;
            $data['selectedCategory'] = $request->category;
            $data['selectedSumAssured'] = $request->sum_assured;
            $data['selectedMop'] = $request->mop;
            $data['selectedCompanies'] = $request->companies;
            $data['selectedProducts'] = $request->products;
            $data['features'] = $request->features;
//            dd($data['features']);
            if ($request->download_pdf  || $request->download_word || $request->mail_pdf) {
//                dd($request->products);
                $data['selectedProducts'] = json_decode($request->products);
                $data['features'] = json_decode($request->features);
//                dd(gettype($data['selectedProducts']));
            }
            if($request->company_id){
                $data=new UploadProductPdf();
                $attachment=$request->attachment;
                $filename=time().'.'.$attachment->getClientOriginalExtension();
                $request->attachment->move('uploads/products/',$filename);
                $data->attachment=$filename;
                $data->title=$request->title;
                $data->description=$request->description;
                $data->Code=$request->Code;
                $data->company_id=$request->company_id;
                $data->type=$request->type;
                $data->product_id = $request->product_id;
                $data->save();
                return redirect()->back();
            }
//            else {
//            $data['selectedProducts'] = $request->products;
//            $data['features'] = $request->features;
//                dd(gettype($data['selectedProducts']));
//        }
            $data = $this->prepareAdditionalData($data, 'compare');
            $data['subTerms'] = $this->getSubTerms($data['selectedMop']);
            if($data['features']){
                $fea=[];
                foreach($data['features'] as $feature){
                    $fea[]=str_replace('_',' ',strtoupper($feature));
                }
                $data['features']=$fea;
            }
            if ($request->download_pdf) {
                try {
//                return view('Backend.Compare.pdf', $data);
                    $pdf = PDF::loadView('Backend.Compare.pdf', $data)->setPaper('a4','landscape');
//            dd($pdf->download('compare.pdf'));
                return $pdf->download('compare.pdf');
                }
                catch (\Exception $e) {
                    return redirect()->back()->withErrors($e->getMessage());
                }
            }
            if ($request->mail_pdf) {
                try {
                $pdf = PDF::loadView('Backend.Compare.pdf', $data)->setPaper('a4','landscape');
                //$pdf->setWatermarkImage(public_path('uploads/ebeema_logo.png'));
                // Send mail to customer
//                    dd($data);
                $mail = Mail::to(Auth::user())
                    ->send(new CompareResult($data, $pdf->output()));
//                dd($mail);
                    return view('Backend.Compare.result', $data)->withSuccessMessage("Successfully Mailed");
                }
                catch (\Exception $e) {
                  return redirect()->back()->withErrors($e->getMessage());
                }
            }

            if($request->download_word){
                //return view('Backend.Compare.pdf', $data);
                $headers = array(
                    "Content-type"=>"text/html",
                    "Content-Disposition"=>"attachment;Filename=compare.doc"
                );
                $content =\View::make('Backend.Compare.docs', $data)->render();
                return \Response::make($content,200, $headers);
            }
            //dd($data);
            return view('Backend.Compare.result', $data);
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function frontendCompare(Request $request) //no
    {
       try {
        $data['invest'] = $request->invest;
        $data['birth_date'] = $request->birth_date;
        $data['birth_month'] = $request->birth_month;
        $data['birth_year'] = $request->birth_year;
        $data['selectedAge'] = $request->age;
        $data['selectedHusbandAge'] = $request->husband_age;
        $data['selectedWifeAge'] = $request->wife_age;
        $data['selectedChildAge'] = $request->child_age;
        $data['selectedProposersAge'] = $request->proposer_age;
        $data['selectedTerm'] = $request->term ?? $request->term1;
        $data['selectedCategory'] = $request->category;
        $data['selectedSumAssured'] = $request->sum_assured;
        $data['selectedMop'] = $request->mop;
        $data['features'] = $request->sort ? json_decode($request->features) : $request->features;
        $data['sort'] = $request->sort ? $request->sort : 'premium_high_low';
        $data['date_type']=$request['date-type'];
        $data['company_id'] =$request['company_id'] ?? [];
        //dd($data);
        $data = $this->prepareAdditionalData($data, 'compare');

        $data['products'] = $this->sortItemsBy($data['products'], $data);
        $data['categories'] = Product::get()->unique('category')->pluck('category');
        $data['mops'] = GeneralSetting::where('type', 'mop')->pluck('value');

        $data['terms'] = Term::orderBy('term')
            ->where('term', '>', 0)
            ->get('term')
            ->unique('term')
            ->pluck('term');

        $data['availableFeatures'] = $this->getCategoryFeature($data['selectedCategory']);

        $data['subTerms'] = $this->getSubTerms($data['selectedMop']);

        if ($request->api_calculation && $request->api_calculation == 1) {
            return $data['products'];
        }
        if ($request->is_ajax && $request->is_ajax == 1) {

            return view('frontend.Partials.compareResult', $data);
        }

        return view('frontend.compare', $data);
        } catch (\Exception $e) {

            return redirect()->back();
        }
    }

    private function sortItemsBy($products, $sort)
    {
        switch ($sort['sort']) {
            case 'premium_high_low':
                $products = $products->sortByDesc('premiumAmountWithBenefit');
                break;
            case 'maturity_high_to_low':
                $products = $products->sortByDesc('totalPremiumAmount');
                break;
            case 'maturity_low_to_high':
                $products = $products->sortBy('totalPremiumAmount');
                break;
            case 'net_gain_high_to_low':
                $products = $products->sortByDesc('netGain');
                break;
            case 'net_gain_low_to_high':
                $products = $products->sortBy('netGain');
                break;
            default:
                $products = $products->sortBy('premiumAmountWithBenefit');
                break;
        }
        if(count($sort['company_id']) > 0){
            $products=$products->whereIn('company_id',$sort['company_id']);
        }
        return $products;
    }

    public function selectedPlanCalculation(Request $request)
    {
//        try {
//        dd($request->all());
        $data['bonus'] = $request->bonus;
        $data['net_gain'] = $request->net_gain;
        $data['return'] = $request->return;
        $data['company_id'] = $request->company_id;
        $data['invest'] = $request->invest;
        $data['premium'] = $request->premium;
        $data['birth_date'] = $request->birth_date;
        $data['birth_month'] = $request->birth_month;
        $data['birth_year'] = $request->birth_year;
        $data['selectedAge'] = $request->age;
        $data['selectedHusbandAge'] = $request->husband_age;
        $data['selectedWifeAge'] = $request->wife_age;
        $data['selectedChildAge'] = $request->child_age;
        $data['selectedProposersAge'] = $request->proposer_age;
        $data['selectedTerm'] = $request->term;
        $data['selectedCategory'] = $request->category;
        $data['selectedSumAssured'] = $request->sum_assured;
        $data['selectedMop'] = $request->loading_charge;
        $data['selectedProduct'] = $request->product_id;
        if ($request->features == 'null') {
            $data['features'] = [];
        } else {
            $data['features'] = json_decode($request->features);
        }


        $data = $this->prepareAdditionalData($data, 'calculator');

//        dd($data);
        $data['loadingCharges'] = GeneralSetting::where('type', 'mop')->pluck('value');

        $data['subTerms'] = $this->getSubTerms($data['selectedMop']);

        return view('frontend.confirmation', $data);
//        } catch (Exception $e) {
//            return redirect()->back();
//        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getCategoryDetails(Request $request)
    {
        return ProductCategory::where('category_code', $request->category)->first();
    }

    /**
     * @param $mop
     * @return int
     */
    private function getSubTerms($mop)
    {
        $sub = 1;

        if ($mop == 'half_yearly') {
            $sub = 2;
        } elseif ($mop == 'quarterly') {
            $sub = 4;
        } elseif ($mop == 'monthly') {
            $sub = 12;
        }

        return $sub;
    }

    /**
     * @param $data
     * @param $calculationType // 'calculator', 'compare
     * @return mixed
     */
    private function prepareAdditionalData($data, $calculationType) // no
    {
        $data['isCouplePlan'] = $data['selectedCategory'] === 'couple';
        $data['isChildPlan'] = $data['selectedCategory'] === 'children' || $data['selectedCategory'] === 'education';

        if ($data['selectedCategory'] == 'children') {
            $data['actualTerm'] = $data['selectedTerm'];
//            $data['actualTerm'] = $data['selectedTerm'] - $data['selectedChildAge'];
//            dd($data);
        } elseif ($data['selectedCategory'] == 'education') {
            $data['actualTerm'] = $data['selectedTerm'];
        } else {
            $data['actualTerm'] = $data['selectedTerm'];
        }

        $data['term'] = Term::whereTerm($data['selectedTerm'])->first();

        if ($data['isCouplePlan']) {
            $ages = collect([$data['selectedHusbandAge'], $data['selectedWifeAge']]);
            $addDifference = CoupleAgeDifference::select('add_age')
                ->whereAgeDifference($ages->max() - $ages->min())
                ->first();

            if ($addDifference) {
                $addAge = $addDifference->add_age;
                unset($addDifference);
            } else {
                $addAge = 0;
            }

            $data['averageAge'] = floatval($ages->min()) + floatval($addAge);
            unset($addAge);

            $data['age'] = Policy_age::where('age', $data['averageAge'])->first();
        } else {
            if ($data['isChildPlan']) {
                $data['selectedAge'] = $data['selectedChildAge'];
            }
            $data['age'] = Policy_age::whereAge($data['selectedAge'])->first();
        }

        if ($calculationType == 'calculator') {
            $product = Product::whereId($data['selectedProduct'])->first();
            $data['product'] = $this->calculate($product, $data, $calculationType);
            unset($product);
        } else {
            if (array_key_exists('selectedProducts', $data)) {
//                $productIDs = json_decode($data['selectedProducts']);
                $productIDs = $data['selectedProducts'];
                $products = Product::whereType('life')
                    ->with('uploadProductPdf')
                    ->whereCategory($data['selectedCategory'])
                    ->whereIsActive(true)
                    ->whereIn('id', $productIDs)
                    ->select(
                        'id', 'name', 'company_id', 'code',
                        'premium_rate_divider', 'has_loading_charge_on_features',
                        'benefit as benefit_details'
                    )
                    ->get();
            }
            else {
                $products = Product::whereType('life')
                    ->whereCategory($data['selectedCategory'])
                    ->whereIsActive(true)
                    ->select(
                        'id', 'name', 'company_id', 'code',
                        'premium_rate_divider', 'has_loading_charge_on_features',
                        'benefit as benefit_details'
                    )
                    ->get();
            }

            foreach ($products as $key => $product) {
                $product = $this->calculate($product, $data, $calculationType);
                if ($product->unset) {
                    unset($products[$key]);
                }
            }
//dd($products);
            $data['products'] = $products;
//            dd($data);
        }

        return $data;
    }

    /**
     * @param $product
     * @param $data
     * @param $calculationType // 'calculator', 'compare
     * @return mixed
     */
    private function calculate($product, $data, $calculationType)
    {
        $settings = GeneralSetting::whereType('calculation')->get()->pluck('value', 'key');

        $product->currentAge = $data['age']->age;
        $product->currentTerm = $data['term']->term;

        $product->isLic = $product->company->code == 'LIC';
        $product->isNational = $product->company->code == 'NAT';
        $product->isNlic = $product->company->code == 'NLIC';


        if ($calculationType == 'compare') {
            $maxAgeInDB = Policy_age::max('age');
            $maxTermInDB = Term::max('term');

            // table rates starts
            $data['age'] = $this->getRelevantAge($data['age']->age, $product, $data['age']->age, $maxAgeInDB);

            if ($data['age']) {
                $product->currentAge = $data['age']->age;
            } else {
                $product->unset = true;
                return $product;

            }

            $arr = $this->getRelevantTerm($data['term']->term, $data['age'], $product, $data['term']->term, $maxTermInDB);

            $tableRate = null;

            if ($arr) {
                $data['term'] = $arr[1];
                $data['actualTerm'] = $data['term']->term;
                $product->currentTerm = $data['term']->term;
            } else {
                $product->unset = true;
                return $product;

            }
        }

        $tableRate = Rate_endowment::whereAgeId($data['age']->id)
            ->whereTermId($data['term']->id)
            ->whereProductId($product->id)
            ->first();

        if ($tableRate) {
            $product->rate = floatval($tableRate->rate);
        } else {
            $product->unset = true;
            return $product;
        }
        unset($tableRate);

        // loading charges starts
        $loadingCharge = LoadingCharge::whereProductId($product->id)->first();

        if ($loadingCharge && is_numeric($loadingCharge[($data['selectedMop'])])) {
            $product->mop = floatval($loadingCharge[$data['selectedMop']]);
        } else {
            $product->unset = true;
            return $product;

        }
        unset($loadingCharge);

        if ($product->isLic) {
            $product->mopRate = floatval($product->mop ? $product->rate * $product->mop : $product->rate);
        }
        // loading charges ends

        // discount on sa starts
        $discountOnSA = SumAssured::where('first_amount', '<=', $data['selectedSumAssured'])
            ->where('second_amount', '>=', $data['selectedSumAssured'])
            ->whereProductId($product->id)
            ->first();

        $product->discountRate = $discountOnSA ? floatval($discountOnSA->discount_value) : 0;
        unset($discountOnSA);

        if ($product->code == 'NAT_9' && $data['selectedSumAssured'] >= 1000000) {
            $product->discountRate = 0;
        }
        // discount on sa ends

        // new rate
        $product->newRate = floatval(($product->isLic ? $product->mopRate : $product->rate) - floatval($product->discountRate));

        // premium calculation starts here
        $product->premiumAmount = floatval((floatval($product->newRate) / floatval($product->premium_rate_divider)) * floatval($data['selectedSumAssured']));

        if ($product->code == 'NAT_9' && floatval($data['selectedSumAssured']) >= 1000000) {
            $product->premiumBeforeDiscount = floatval($product->premiumAmount);
            $product->discountOnPremium = floatval(2 / $settings['hundreds']) * floatval($product->premiumAmount);
            $product->premiumAmount -= $product->discountOnPremium;

        } else {
            $product->discountOnPremium = null;
        }

        if (!$product->isLic) {
            $product->premiumBeforeCharge = floatval($product->premiumAmount);

            $product->mopAmount = (floatval($product->mop) / $settings['hundreds']) * floatval($product->premiumBeforeCharge);
            $product->premiumAmount = floatval($product->premiumBeforeCharge + floatval($product->mopAmount));
        }
        // premium calculation ends here

        $product->features = $this->getFeatures($product, $data, $settings);
        if (count($product->features) > 0) {
            $product->benefit = floatval($product->features->sum('amount'));
        } else {
            $product->benefit = 0;
        }

        $product->premiumAmountWithBenefit = floatval(floatval($product->premiumAmount) + floatval($product->benefit));

        if ($product->has_loading_charge_on_features) {
            $product->mopBenefit = floatval($product->features->sum('mopRate'));
            $product->premiumAmountWithBenefit += floatval($product->mopBenefit);
        }
        // benefit calculation ends here

        $product->totalPremiumAmount = floatval($product->premiumAmountWithBenefit);

        $product->yearlyPremium = floatval($product->premiumAmountWithBenefit / 1);
        $product->halfYearlyPremium = floatval($product->premiumAmountWithBenefit / 2);
        $product->quarterlyPremium = floatval($product->premiumAmountWithBenefit / 4);
        $product->monthlyPremium = floatval($product->premiumAmountWithBenefit / 12);

        // bonus calculation starts here
        $bonus = $this->getBonus($product, $data);
        $product->bonusYearly = floatval($bonus->yearly);
        $product->bonus = floatval($bonus->endOfPeriod);
        $product->bonusRate = floatval($bonus->rate);
        $product->totalPremiumAmount = floatval($bonus->total);
        unset($bonus);
        // bonus calculation ends here

        // net gain calculation starts here
        $paying_term = $product->paying_term()->where('term_id', $data['term']->id)->first();

        if ($paying_term) {
            $product->payingTerm = floatval($paying_term->paying_year);
            $product->hasPayingTerm = true;
        } else {
            $product->payingTerm = floatval($data['term']->term);
            $product->hasPayingTerm = false;
        }

        // if product has paying term then productPayingTerm = paying year
        // else productPayingTerm = product current term
        if ($data['selectedMop'] == 'half_yearly') {
            $product->actualPremium = floatval($product->halfYearlyPremium * 2) * floatval($product->payingTerm);
        } elseif ($data['selectedMop'] == 'quarterly') {
            $product->actualPremium = floatval($product->quarterlyPremium * 4) * floatval($product->payingTerm);
        } elseif ($data['selectedMop'] == 'monthly') {
            $product->actualPremium = floatval($product->monthlyPremium * 12) * floatval($product->payingTerm);
        } else {
            $product->actualPremium = floatval($product->yearlyPremium) * floatval($product->payingTerm);
        }

        $product->netGain = floatval($product->totalPremiumAmount) - floatval($product->actualPremium);
        // net gain calculation ends here

        $product->paybackSchedules = $this->getPaybackSchedule($product, floatval($data['selectedSumAssured']));

        if ($data['isChildPlan']) {
            $crcRate = CrcRate::whereAgeId($data['age']->id)
                ->whereProductId($product->id)
                ->first();

            $product->crcRate = $crcRate ? floatval($crcRate->one_time_charge) : 0;

            $product->crcAmount = floatval($product->crcRate) / floatval($settings['thousands']) * floatval($data['selectedSumAssured']);

        } else {
            $product->crcRate = 0;
            $product->crcAmount = 0;
        }

        $featureIds = ProductFeature::where('product_id', $product->id)->pluck('feature_id');
        $product->availableFeatures = Feature::whereIn('id', $featureIds)->get();
        unset($featureId);

        return $product;
    }

    /**
     * @param $category
     * @return mixed
     */
    private function getCategoryFeature($category) // no
    {
        $products = Product::where('category', $category)->pluck('id');
        $featureIds = ProductFeature::whereIn('product_id', $products->toArray())->pluck('feature_id');
        return Feature::whereIn('id', $featureIds)->get();
    }

    /**
     * @return mixed
     */
    private function getRequiredData() // no
    {
        if (in_array('all', AgentCat())) {
            $companies = Company::orderBy('created_at', 'asc')->get();
        } else {
            $companies = Company::whereIn('id', AgentCategories())->get();
        }

        // retrieving companies (optimizing query with only selecting id and name)
        $companies = Company::select('id', 'name', 'code')
            ->where('is_active', '1')
            ->where('type', 'life')
            ->with(['products' => function ($query) {
                $query->whereIsActive(true)
                    ->select('id', 'name', 'company_id', 'category');
            }])
            ->whereHas('products')
            ->orderBy('name', 'asc')
            ->get();
        ;
        // looping through retrieved companies
        foreach ($companies as $company) {

            // looping through retrieved products
            foreach ($company->products as $product) {

                // temp_rates is for temporary use
                // retrieving Rate_endowment list related to the product (through relation)
                $product->temp_rates = $product->rates;

                // creating an empty array to store terms
                $termArray = [];
                $ageArray = [];
                $proposerAgeArray = [];

                // looping rates (of only unique terms)
                foreach ($product->temp_rates->unique('term_id') as $rate) {
                    // pushing term value to array
                    // accessing term column of term table through rate_endowment
                    $termArray[] = intval($rate->term->term);
                }
                // setting terms in product
                $product->terms = $termArray;
                unset($termArray);

                foreach ($product->temp_rates->unique('age_id') as $ageRate) {
                    $ageArray[] = intval($ageRate->age->age);
                }
                $product->ages = $ageArray;
                $ageCollect = collect($ageArray);
                unset($ageArray);

                if ($company->code == 'NAT') {
                    $term_rider = Feature::whereCode('term_rider')->first();
                    $product_feature = ProductFeature::whereFeatureId($term_rider->id)->whereProductId($product->id)->first();
                } else {
                    $pwb = Feature::whereCode('pwb')->first();
                    $product_feature = ProductFeature::whereFeatureId($pwb->id)->whereProductId($product->id)->first();
                }

                if ($product_feature) {
                    foreach ($product_feature->rates->unique('age_id') as $item) {
                        $proposerAgeArray[] = intval($item->age->age);
                    }
                }

                $product->proposer_ages = $proposerAgeArray;

                $product->max_age = $ageCollect->max();
                $product->min_age = $ageCollect->min();
                unset($ageCollect);

                unset($product->rates);
                unset($product->temp_rates);
            }
        }

        return $companies;
    }

    /**
     * @param $currentAge
     * @param $product
     * @param $selectedAge
     * @param $maxAgeInDB
     * @return null
     */
    private function getRelevantAge($currentAge, $product, $selectedAge, $maxAgeInDB) //no
    {
        if ($currentAge <= $maxAgeInDB) {
            $age = Policy_age::where('age', $currentAge)->first();

            $tableRate = Rate_endowment::whereAgeId($age->id)
                ->whereProductId($product->id)
                ->first();

            if (!$tableRate) {

                if ($currentAge <= $selectedAge && $currentAge != 0) {
                    return $this->getRelevantAge(--$currentAge, $product, $selectedAge, $maxAgeInDB);
                } else {
                    if ($currentAge < $selectedAge) {
                        $currentAge = $selectedAge;
                    }
                    return $this->getRelevantAge(++$currentAge, $product, $selectedAge, $maxAgeInDB);
                }
            } else {
                return $age;
            }
        } else {
            return null;
        }
    }

    /**
     * @param $currentTerm
     * @param $age
     * @param $product
     * @param $selectedTerm
     * @param $maxTermInDB
     * @return array|null
     */
    private function getRelevantTerm($currentTerm, $age, $product, $selectedTerm, $maxTermInDB) // no
    {
        if ($currentTerm <= $maxTermInDB) {
            $term = Term::where('term', $currentTerm)->first();

            $tableRate = Rate_endowment::whereAgeId($age->id)
                ->whereTermId($term->id)
                ->whereProductId($product->id)
                ->first();

            if (!$tableRate) {

                if ($currentTerm <= $selectedTerm && $currentTerm != 0) {
                    return $this->getRelevantTerm(--$currentTerm, $age, $product, $selectedTerm, $maxTermInDB);
                } else {
                    if ($currentTerm < $selectedTerm) {
                        $currentTerm = $selectedTerm;
                    }
                    return $this->getRelevantTerm(++$currentTerm, $age, $product, $selectedTerm, $maxTermInDB);
                }
            } else {
                return [$tableRate, $term];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $product
     * @param $data
     * @param $settings
     * @return string
     */
    private function getFeatures($product, $data, $settings)
    {
        try{
        $features = $data['features'] ? $data['features'] : [];

        $compulsoryFeatureIds = ProductFeature::where('product_id', $product->id)
            ->where('is_compulsory', '1')
            ->pluck('feature_id');

        $compulsoryFeatureCodes = Feature::whereIn('id', $compulsoryFeatureIds)->pluck('code')->toArray();

        if(count($compulsoryFeatureCodes) > 0){
            $features = array_unique(array_merge($features, $compulsoryFeatureCodes));
//            $features = json_decode($features);
        }
//        dd(gettype($features));
        if ($features && count($features)) {
            $termId = Term::where('term', $product->currentTerm)->pluck('id');
            $ageId = Policy_age::where('age', $product->currentAge)->pluck('id');

            foreach ($features as $featureCode) {

                if ($data['isChildPlan']) {
                    if ($product->isLic) {
                        $termId = Term::where('term', $data['actualTerm'])->first()->id;
                    }
                    if ($product->isNlic && $product->currentTerm > 18) {
                        $termRequired = 18 - $data['selectedChildAge'];
                        $termId = Term::where('term',$termRequired)->first()->id;
                    }
                    $ageId = Policy_age::where('age', $data['selectedProposersAge'])->first()->id;
                }

                $featureRow = Feature::whereCode($featureCode)->first();

                $productFeature = ProductFeature::whereProductId($product->id)
                    ->whereFeatureId($featureRow->id)
                    ->first();

                if ($productFeature) {
                    $featureRateRow = ProductFeatureRates::whereAgeId($ageId)
                        ->whereTermId($termId)
                        ->whereProductFeatureId(
                            $productFeature->id
                        )
                        ->first();

                    $data[$featureCode . 'Rate'] = $featureRateRow ? floatval($featureRateRow->rate) : 0;

                    if ($product->isLic && $data['features'] && $data['isChildPlan']) {
                        $data[$featureCode . 'Amount'] = (floatval($data[$featureCode . 'Rate']) / floatval($settings['hundreds'])) * floatval($product->premiumAmount);
                    } elseif ($product->isNlic && $data['features'] && $data['isChildPlan']) {
                        $data[$featureCode . 'Amount'] = (floatval($data[$featureCode . 'Rate']) / floatval($settings['hundreds'])) * floatval($product->premiumBeforeCharge);
//                        dd($data[$featureCode . 'Amount']);
                    } elseif ($featureCode == 'term_rider') {

                        if ($product->isNational && $data['selectedSumAssured'] >= $settings['term_rider_national']) {
                            $data[$featureCode . 'Amount'] = floatval($data[$featureCode . 'Rate']) / floatval($settings['thousands']) * floatval($settings['term_rider_national']);
                        } elseif ($product->isLic && $data['selectedSumAssured'] >= $settings['term_rider']) {
                            $data[$featureCode . 'Amount'] = floatval($data[$featureCode . 'Rate']) / floatval($settings['thousands']) * floatval($settings['term_rider']);
                        } else {
                            $data[$featureCode . 'Amount'] = floatval($data[$featureCode . 'Rate']) / floatval($settings['thousands']) * floatval($data['selectedSumAssured']);
                        }

                    } else {
                        $data[$featureCode . 'Amount'] = floatval($data[$featureCode . 'Rate']) / floatval($settings['thousands']) * floatval(($data['selectedSumAssured']));
                    }

                    if ($product->has_loading_charge_on_features) {
                        $data[$featureCode . 'RateWithMop'] = floatval(($product->mop / $settings['hundreds'])) * floatval($data[$featureCode . 'Amount']);
                        $data[$featureCode . 'AmountWithMop'] = floatval($data[$featureCode . 'RateWithMop']) + floatval($data[$featureCode . 'Amount']);
                    }
                }
            }
        }
            $featureArr = [];

            if ($features && count($features)) {
            foreach ($features as $code) {
                if (array_key_exists($code . 'Rate', $data)) {

                    if ($product->has_loading_charge_on_features) {
                        $featureArr[] = [
                            'code' => $code,
                            'rate' => $data[$code . 'Rate'],
                            'amount' => $data[$code . 'Amount'],
                            'mopRate' => $data[$code . 'RateWithMop'],
                            'mopAmount' => $data[$code . 'AmountWithMop'],
                        ];
                    } else {
                        $featureArr[] = [
                            'code' => $code,
                            'rate' => $data[$code . 'Rate'],
                            'amount' => $data[$code . 'Amount']
                        ];
                    }
                }
            }
        }
        return collect($featureArr);
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $product
     * @param $data
     * @return Collection
     */
    private function getBonus($product, $data)
    {
        $bonus = BonusRateEP::select('term_rate as rate')
            ->where('first_year', '<=', $data['term']->term)
            ->where('second_year', '>=', $data['term']->term)
            ->whereProductId($product->id)
            ->first();

        if ($bonus) {
            $bonus->yearly = floatval($data['selectedSumAssured']) * floatval($bonus->rate);
            if ($data['isChildPlan'] && $product->isLic) {
                $bonus->endOfPeriod = floatval($bonus->yearly) * floatval($data['actualTerm']);
            } else {
                $bonus->endOfPeriod = floatval($bonus->yearly) * floatval($product->currentTerm);
            }
            $bonus->total = floatval($bonus->endOfPeriod) + floatval($data['selectedSumAssured']);
        } else {
            $bonus = collect();
            $bonus->rate = 0;
            $bonus->yearly = 0;
            $bonus->endOfPeriod = 0;
            $bonus->total = floatval($data['selectedSumAssured']);
        }
        return $bonus;
    }

    /**
     * @param $product
     * @param $sumAssured
     * @return mixed
     */
    private function getPaybackSchedule($product, $sumAssured)
    {
//        dd('here');
        // Payback schedule with amount
        $paybackSchedule = PaybackSchedule::whereTermYear($product->currentTerm)
            ->whereProductId($product->id)
            ->select('payback_year', 'rate')
            ->get();
        foreach ($paybackSchedule as $payback) {
            $payback->amount = floatval($sumAssured) * floatval(($payback->rate / 100));
        }

        return $paybackSchedule;
    }
}
