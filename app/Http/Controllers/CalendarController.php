<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use App\Models\Calendar;
use App\Models\GeneralSetting;
use App\Models\UserOutlookCredential;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;
use App\TimeZones\TimeZones;
use Session;
use Http;
use Auth;
use Exception;

class CalendarController extends Controller
{
    public function index()
    {
//        dd(session()->all());
        if (! Session::has('accessToken') && ! Session::has('refreshToken')) {
            $this->CheckUserLogin();
        }
        $data = $this->loadViewData();
//        dd($data);
        return view('Backend.Calendar.index', $data);
    }

    public function CheckUserLogin()
    {
        $user = Auth::user();
//        dd($user);
        if (Session::has('accessToken') && Session::has('refreshToken')) {
            $objetoRequest = new \Illuminate\Http\Request();
            $objetoRequest->setMethod('POST');
            $objetoRequest->request->add([
                "check" => 1,
                "oc_id" => $user->outlookCredentials->id ?? '',
                "user_id" => $user->id ?? '',
                "access_token" => Session::get('accessToken'),
                "refresh_token" => Session::get('refreshToken'),
                "token_expires" => Session::get('tokenExpires'),
                "user_name" => Session::get('userName'),
                "user_email" => Session::get('userEmail'),
                "user_time_zone" => Session::get('userTimeZone')
            ]);
            $this->outlookDetails($objetoRequest);
            $tokenCache = new TokenCache();
            $tokenCache->getAccessToken();
            return ('login Stored');
        } else {
            if (isset($user->outlookCredentials->id) && $user->outlookCredentials->access_token && $user->outlookCredentials->refresh_token) {
                $userOutlookDetails = UserOutlookCredential::find($user->outlookCredentials->id)->first();
//                dd($userOutlookDetails);
                $accessToken = [
                    "accessToken" => $userOutlookDetails->access_token,
                    "refreshToken" => $userOutlookDetails->refresh_token,
                    "tokenExpires" => $userOutlookDetails->token_expires,
                    "userName" => $userOutlookDetails->user_name,
                    "userEmail" => $userOutlookDetails->user_email,
                    "userTimeZone" => $userOutlookDetails->user_time_zone ?? "Nepal Standard Time",
                ];
                session::put($accessToken);
                $tokenCache = new TokenCache();
                $tokenCache->getAccessToken();
                return ('login Success');
            }
        }
    }

    public function calendar()
    {
        $viewData = $this->loadViewData();

        $graph = $this->getGraph();

        // Get user's timezone
        $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);

        // Get start and end of week
        $startOfWeek = new \DateTimeImmutable('sunday -1 week', $timezone);
        $endOfWeek = new \DateTimeImmutable('sunday', $timezone);

        $viewData['dateRange'] = $startOfWeek->format('M j, Y') . ' - ' . $endOfWeek->format('M j, Y');

        $queryParams = array(
            'startDateTime' => $startOfWeek->format(\DateTimeInterface::ISO8601),
            'endDateTime' => $endOfWeek->format(\DateTimeInterface::ISO8601),
            // Only request the properties used by the app
            '$select' => 'subject,organizer,start,end',
            // Sort them by start time
            '$orderby' => 'start/dateTime',
            // Limit results to 25
            '$top' => 25
        );

        // Append query parameters to the '/me/calendarView' url
        $getEventsUrl = '/me/calendarView?' . http_build_query($queryParams);

        $events = $graph->createRequest('GET', $getEventsUrl)
            // Add the user's timezone to the Prefer header
            ->addHeaders(array(
                'Prefer' => 'outlook.timezone="' . $viewData['userTimeZone'] . '"'
            ))
            ->setReturnType(Model\Event::class)
            ->execute();

//        return response()->json($events);
        $viewData['events'] = $events;
        return view('Backend.Calendar.view', $viewData);
    }

    private function getGraph(): Graph
    {
        // Get the access token from the cache
        $tokenCache = new TokenCache();
        $accessToken = $tokenCache->getAccessToken();

        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);
//        dd($graph);
        return $graph;
    }

    public function getNewEventForm()
    {
        $viewData = $this->loadViewData();

        return view('Backend.Calendar.newEvent', $viewData);
    }

    public function createNewEvent(Request $request)
    {
//        dd($request->all());
        // Validate required fields
        if (isset($request->type) && $request->type = "add") {
//            $request->merge([
//                eventSubject
//                    eventAttendees
//                    eventStart
//                    eventEnd
//                    eventBody
//                "eventSubject" => $request->title,
//                "eventStart" => $request->start,
//                "eventEnd" => $request->end
//            ]);
        } else {
            $request->validate([
                'eventSubject' => 'nullable|string',
                'eventAttendees' => 'nullable|string',
                'eventStart' => 'required|date',
                'eventEnd' => 'required|date',
                'eventBody' => 'nullable|string'
            ]);
        }
//dd($request->all());
        $viewData = $this->loadViewData();

        $graph = $this->getGraph();

        // Attendees from form are a semi-colon delimited list of
        // email addresses
        $attendeeAddresses = explode(';', $request->eventAttendees);

        // The Attendee object in Graph is complex, so build the structure
        $attendees = [];
        foreach ($attendeeAddresses as $attendeeAddress) {
            array_push($attendees, [
                // Add the email address in the emailAddress property
                'emailAddress' => ['address' => $attendeeAddress], // Set the attendee type to required
                'type' => 'required']);
        }

// Build the event
        $newEvent = [
            'subject' => $request->eventSubject,
            'attendees' => $attendees,
            'start' => [
                'dateTime' => $request->eventStart,
                'timeZone' => $viewData['userTimeZone']
            ],
            'end' => [
                'dateTime' => $request->eventEnd,
                'timeZone' => $viewData['userTimeZone']
            ],
            'body' => [
                'content' => $request->eventBody,
                'contentType' => 'text'
            ]
        ];
//dd($newEvent);
// POST /me/events
        $response = $graph->createRequest('POST', '/me/events')
            ->attachBody($newEvent)
            ->setReturnType(Model\Event::class)
            ->execute();

        if (isset($request->type) && $request->type = "add") {
            $data['eventId'] = $response->getId();
            return $data;
        }

        return redirect('/user/calendar/view');
    }

    public function updateEvent(Request $request, $id)
    {
        try {
            $calendar_id = $id;
//        dd($calendar_id,$request->all());
            $viewData = $this->loadViewData();
            $updateUrl = '/me/events/' . $calendar_id;

            // Validate required fields
            if (isset($request->type) && $request->type = "update") {
//                $request->merge([
//                    "eventSubject" => $request->title,
//                    "eventStart" => $request->start,
//                    "eventEnd" => $request->end
//                ]);
            } else {
                $request->validate([
                    'eventSubject' => 'nullable|string',
                    'eventAttendees' => 'nullable|string',
                    'eventStart' => 'required|date',
                    'eventEnd' => 'required|date',
                    'eventBody' => 'nullable|string'
                ]);
            }
//dd($request->all());
            // Attendees from form are a semi-colon delimited list of
            // email addresses
            $attendeeAddresses = explode(';', $request->eventAttendees);

            // The Attendee object in Graph is complex, so build the structure
            $attendees = [];
            foreach ($attendeeAddresses as $attendeeAddress) {
                array_push($attendees, [// Add the email address in the emailAddress property
                    'emailAddress' => ['address' => $attendeeAddress],
                    // Set the attendee type to required
                    'type' => 'required']);
            }

            // Build the event
            $newEvent = [
                'subject' => $request->eventSubject,
                'attendees' => $attendees,
                'start' => [
                    'dateTime' => $request->eventStart,
                    'timeZone' => $viewData['userTimeZone']
                ],
                'end' => [
                    'dateTime' => $request->eventEnd,
                    'timeZone' => $viewData['userTimeZone']
                ],
                'body' => [
                    'content' => $request->eventBody,
                    'contentType' => 'text'
                ]
            ];
//dd($newEvent);

            $events = $this->getGraph()->createRequest('PATCH', $updateUrl)
                ->attachBody($newEvent)
                ->setReturnType(Model\Event::class)
                ->execute();

            if (isset($request->type) && $request->type = "update") {
                $data['eventId'] = $events->getId();
                return $data;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        try {
            $calendar_id = $request->id;
//        dd($calendar_id);
            $delete_url = '/me/events/' . $calendar_id;

            $events = $this->getGraph()->createRequest('DELETE', $delete_url)
                ->setReturnType(Model\Event::class)
                ->execute();
//        dd($events);
            return redirect('/user/calendar/view')->withErrorMessage("Deleted Successfully");
        } catch (Exception $e) {
            return redirect('/user/calendar/view')->withErrorMessage($e->getMessage());
        }
    }

    public function deleteEvent($id)
    {
        try {
            $calendar_id = $id;
//        dd($calendar_id);
            $delete_url = '/me/events/' . $calendar_id;

            $events = $this->getGraph()->createRequest('DELETE', $delete_url)
                ->execute();
//        dd($events);
            return response()->json($events);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function outlookDetails(Request $request)
    {
//        return $request;
        try {
            if ($request->oc_id != null) {
                $cal = UserOutlookCredential::findOrFail($request->oc_id);
                $cal->update($request->all());
            } else {
                $cal = UserOutlookCredential::create($request->all());
            }
            if ($request->check == 1) {
                return $cal;
            }
            return redirect()->back()->withSuccess('Credentials Updated');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors("Err" . $e->getMessage());
        }
    }
}
