<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\CompanyDetail;
use App\Models\User;
use App\Notifications\CompanyCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (control('create-user')) {
            $companies = Company::all();
            return view('company.index')->with('companies', $companies);
        } else {
            return view('layouts.backend.denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'code' => 'required|unique:companies',
            ],
            [
                'name.required' => 'Name Cannot Be Empty!!',
                'code.required' => 'Code Cannot Be Empty!!',
            ]
        );
        if ($request->hasFile('company_logo')) {
            $file = $request->file('company_logo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '_' . $extension;
            $file->move(public_path('images/company'), $filename);
            $request->merge(['logo' => $filename]);
        }
        $company = Company::create($request->all());

        if ($request->ajax()) {
            if ($company) {
                return response()->json([
                    'status' => 'ok',
                    'user' => $company,
                    'message' => 'Company is added successfully.'
                ], 200);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Company can not be added.'
                ], 422);
            }
        } else {
            return redirect()->route('admin.companies.index')->withSuccessMessage('Company added succesfully');
        }
    }

    public function product($id)
    {
        $company = Company::findorFail($id);
        return view('company.productView')->with('company', $company);
    }

    public function companyDetails(Request $request, $id)
    {
        $data['company'] = Company::findorFail($id);
        return view('company.details')->with('company', $data['company']);
    }

    public function companyDetailsUpdate(Request $request, $id)
    {
//        return $request;
        if ($request->company_details_id) {
            $details = CompanyDetail::findOrFail($request->company_details_id);
        } else {
            $details = new CompanyDetail;
        }
        $details->company_id = $request->company_id;
        $details->api_url = $request->company_api;
        $details->status = $request->status;
        $titles = $request->imepay_title;
        $values = $request->imepay_value;
        $api_titles = $request->api_titles;
        $api_values = $request->api_values;
        $api_details = array();
        if ($api_titles) {
            foreach ($api_titles as $key => $title) {
                if ($title) {
                    $val = $api_values[$key];
                    $api_details[$title] = $val;
                }
            }
            $details->api_details = json_encode($api_details);
        }
        $imepay_credentials = array();
        if ($titles) {
            foreach ($titles as $key => $title) {
                if ($title) {
                    $val = $values[$key];
                    $imepay_credentials[$title] = $val;
                }
            }
            $details->imepay_credentials = json_encode($imepay_credentials);
        }
//        return $details;
        $details->save();
        return redirect()->route('admin.companies.index')->withSuccessMessage('Company Details updated succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findorFail($id);
        return view('company.edit')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_method', '_token', 'company_logo');
        if ($request->hasFile('company_logo')) {
            $file = $request->file('company_logo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '_' . $extension;
            $file->move(public_path('images/company'), $filename);
            $data['logo'] = $filename;
        }
        $company = Company::whereId($id)->update($data);

        if ($company) {
            return redirect()->route('admin.companies.index')->withSuccessMessage('Company is Updated successfully.');
        }
        return redirect()->back()->withInput()->withWarningMessage('Company can not be updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        if ($company) {
            return response()->json([
                'type' => 'success',
                'message' => 'Company is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'Company can not deleted.'
        ], 422);
    }

    public function updateCompanyStatus($id)
    {
        try {
            $company = Company::findOrFail($id);
            $company->is_active == '0' ? $company->is_active = '1' : $company->is_active = '0';
            $company->save();
            $message = ($company->is_active == '1') ? "Company is activated successfully." : "Company is deactivated successfully.";
            return response()->json([
                'is_active' => 'ok',
                'cat' => $company,
                'message' => $message
            ], 200);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
