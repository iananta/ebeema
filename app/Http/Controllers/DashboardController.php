<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\Meeting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\DataTables\MyperformanceDataTable;
use App\DataTables\RecentlyaddedcustomerDataTable;
use App\DataTables\FollowUpDataTable;
use App\DataTables\LifeLeadershipDataTable;
use App\DataTables\NonlifeLeadershipDataTable;
use App\Models\PremiumPaidByCustomer;

use App\Models\Remark;
use Carbon\Carbon;
use DateTime;
use DataTables;
use Symfony\Component\HttpFoundation\Session\Session;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(RecentlyaddedcustomerDataTable $recentlyCustomer, FollowUpDataTable $followDataTable,LifeLeadershipDataTable $lifeLeadershipDataTable,NonlifeLeadershipDataTable $nonlifeLeadershipDataTable)
    {
        $data['title'] = "Dashboard";
        $data['lifeLeadershipDataTable']=$lifeLeadershipDataTable->html();
        $data['recentlyCustomer'] = $recentlyCustomer->html();
        $data['followDataTable'] = $followDataTable->html();
        $data['nonlifeLeadershipDataTable']=$nonlifeLeadershipDataTable->html();
        $data['getPercentageCalculation'] = $this->getPercentageCalculation(date('Y-m-d'));
        $data['getTotalCalculation'] = $this->getTotalCalculation();
        return view('Backend.Dashboard.index', $data);
    }


    public function getLifeLeadershipList(LifeLeadershipDataTable $lifeLeadershipDataTable){

        return $lifeLeadershipDataTable->render('dashboard');
    }

    public function getrecentlyCustomer(RecentlyaddedcustomerDataTable $recentlyCustomer)
    {
        return $recentlyCustomer->render('dashboard');
    }

    public function getfollowupList(FollowUpDataTable $followDataTable)
    {
        return $followDataTable->render('dashboard');
    }

    public function getNonlifeLeaderList(NonlifeLeadershipDataTable $nonlifeLeadershipDataTable){
        return $nonlifeLeadershipDataTable->render('dashboard');
    }



    public function followUpdate(Request $request)
    {
        $data = $request->except('_token');
        $date = Carbon::parse($data['follow_up_date']);
        $meeting = Meeting::where('remark_id', $data['follow_up_id'])->first();
        if ($meeting) {
            $request->merge([
                "id" => $meeting->id,
                "type" => "update",
                "eventStart" => $data['follow_up_date'],
                "eventEnd" => $date->format('Y-m-d') . " 23:59",
                "eventSubject" => $meeting->title,
                "eventAttendees" => $meeting->eventAttendees,
                "eventBody" => $meeting->eventBody
            ]);
            $calendar = new FullCalenderController();
            $calendar->actionmeet($request);
        } else {
            Remark::where('id', $data['follow_up_id'])->update([
                'follow_up_date' => $date->format('Y-m-d'),
                'follow_up_time' => $date->format('H:s')
            ]);
        }
        return response()->json(['message' => 'Follow up date Update Successfully']);
    }

    public function filterRevenue(Request $request)
    {
        $requestData = $request->except('_token');
        $data['totalCalculation'] = $this->getTotalCalculation($request->date);
        $data['percentageCalculation'] = $this->getPercentageCalculation($request->date);
        return response()->json(['data' => $data]);
    }

    protected function getTotalCalculation($date = null)
    {
        return [
            'totalLeads' => Lead::leadsCount($date),
            'totalCustomers' => Lead::customersCount($date),
            'totalUsers' => User::totalUser($date),
            'totalSales' => PremiumPaidByCustomer::totalSales($date)
        ];
    }

    protected function getPercentageCalculation($date = null)
    {
        $yesterdayDate = $this->getYesterdayDate($date);
        $currentLeadsTotal = Lead::leadsCount($date);
        $yesterdayLeadsTotal = Lead::leadsCount($yesterdayDate);
        return [
            'percentageLeads' => $this->percentageCalculation($currentLeadsTotal, $yesterdayLeadsTotal),
        ];

    }

    protected function percentageCalculation($currentTotal, $yesterdayTotal)
    {
        //$currentTotal=(int)$currentTotal;
        //$yesterdayTotal=(int)$yesterdayTotal;
        //return (int)$yesterdayTotal / $currentTotal * 100;
        return null;
    }


    protected function getYesterdayDate($date)
    {
        return date('Y-m-d', strtotime('-1 day', strtotime($date)));
    }


}
