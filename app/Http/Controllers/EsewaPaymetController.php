<?php

namespace App\Http\Controllers;

use App\Models\EsewaPaymet;
use App\Models\Lead;
use App\Models\MotorCalculationData;
use App\Models\User;
use App\Notifications\EsewaPaymentFailed;
use App\Notifications\EsewaPaymentSuccess;
use App\Notifications\PaymentFailed;
use App\Notifications\PaymentSuccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Notification;

class EsewaPaymetController extends Controller
{
    public function getCheckout($data = null)
    {
        $policy = MotorCalculationData::findOrFail(Session::get('motorCalcId'));
        $data = (array)$this->submitEsewa($policy);
        return $data;
    }

    private function submitEsewa($policy)
    {
        if (Session::has('motorCalcId')) {
            $amount = $policy->TOTALNETPREMIUM;
            $data = collect([]);
            $data->pid = $policy->payment_ref_id;
//            $data->pid = str_pad(Session::get('motorCalcId'), 20, '0', STR_PAD_LEFT);
            $data->amt = $amount;
            $data->pdc = 0;
            $data->psc = 0;
            $data->txAmt = 0;
            $data->tAmt = $amount;
            $data->scd = env('ESEWA_MERCHANT_ID') ? env('ESEWA_MERCHANT_ID') : 'EPAYTEST';
            return $data;
        }
    }

    public function successTransaction(Request $request)
    {
        try {
//        dd($request->all());
            if (Session::get('motorCalcId')) {
                $policy = MotorCalculationData::findOrFail(Session::get('motorCalcId'));
                $totalamount = floatval($policy->TOTALNETPREMIUM);

                if (floatval($request->amt) != floatval($totalamount)) {
                    $data['data'] = "There is error on Payment Procedure ! Please contact Administrative.";
                    return view('Backend.NonLife.bike.policy', $data);
                }
                $url = env('ESEWA_MERCHANT_URL') ? env('ESEWA_MERCHANT_URL') : 'https://uat.esewa.com.np/epay/transrec';

                $data = [
                    'amt' => $totalamount,
                    'rid' => $request->refId,
                    'pid' => $policy->payment_ref_id,
                    'scd' => env('ESEWA_MERCHANT_ID') ? env('ESEWA_MERCHANT_ID') : 'EPAYTEST',
                    'paymentMethod' => 'esewa',
                    'payableAmount' => $totalamount,
                    'referenceId' => $request->refId,
                ];
//            dd($data);
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);
//            dd($response);
                curl_close($curl);
                if (strpos($response, 'Success') != false) {
                    $storeDta = [
                        'payment_method' => "esewa",
                        'payment_url' => json_encode($request->all()) ?? '',
                        'payment_output' => json_encode($response) ?? '',
                        'status' => 1,
                    ];
                    $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
                     $data['makePolicy'] = 1;
                    $data['paymentMethod'] = 'esewa';
//                    dd(data);
                    Notification::send(Auth::user(),new PaymentSuccess($data));
                    return redirect()->route('nonLife.calculator.motor.make.policy', $data)->withSuccessMessage('Payment was successful');
                } else {
                    // Curse and humiliate the user for cancelling this most sacred payment (yours)
                    $data['data'] = "The Procedure is cancelled ! Please contact Administrative.";
                    Notification::send(Auth::user(),new PaymentFailed($data));
//                    dd(new EsewaPaymentFailed($data));
                    return view('Backend.NonLife.bike.policy', $data)->with('message', 'Payment failed!');
                }
            }
        } catch (\Exception $e) {
            $data['data'] = "Something Went Wrong : " . $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
        }

    }

    public function failedTransaction(Request $request)
    {
        try {
            if (Session::get('motorCalcId')) {
                $policy = MotorCalculationData::findOrFail(Session::get('motorCalcId'));
                $totalamount = floatval($policy->TOTALNETPREMIUM);
                $url = env('ESEWA_MERCHANT_URL') ? env('ESEWA_MERCHANT_URL') : 'https://uat.esewa.com.np/epay/transrec';

                $data = [
                    'amt' => $totalamount,
                    'rid' => $request->ref,
                    'pid' => str_pad(Session::get('motorCalcId'), 20, '0', STR_PAD_LEFT),
                    'scd' => env('ESEWA_MERCHANT_ID') ? env('ESEWA_MERCHANT_ID') : 'EPAYTEST',
                    'PayableAmount' => $totalamount,
                    'referenceId' => $request->ref,
                ];
//            dd($data);
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);

//            dd($response);
                if (strpos($response, 'failure') != false) {
                    // Curse and humiliate the user for cancelling this most sacred payment (yours)
                    $data['data'] = "The Payment Procedure is cancelled ! Please contact Administrative for any assistance.";
                    curl_close($curl);
                    $storeDta = [
                        'payment_method' => "esewa",
                        'payment_url' => json_encode($request->all()) ?? '',
                        'status' => 2,
                    ];
                    $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
                    Session::forget('motorCalcId');
                    $data['paymentMethod'] = 'esewa';
//                dd($calcData);
                    Notification::send(User::where('designation', 'Super Admin')->get(),new PaymentFailed($data));

                    return view('Backend.NonLife.bike.policy', $data);
                }
            }
            $data['data'] = "something went wrong. Please Contact Administrative !";
            return view('Backend.NonLife.bike.policy', $data);
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
//            dd($data);
            return view('Backend.NonLife.bike.policy', $data);
            $e->getMessage();
//            return redirect()->back();
        }
    }
}
