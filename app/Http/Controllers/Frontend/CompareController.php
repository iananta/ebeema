<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\CompareResult;
use App\Models\BonusRateEP;
use App\Models\CoupleAgeDifference;
use App\Models\Feature;
use App\Models\GeneralSetting;
use App\Models\LoadingCharge;
use App\Models\Lead;
use App\Models\PaybackSchedule;
use App\Models\Policy_age;
use App\Models\Product;
use App\Models\ProductFeature;
use App\Models\ProductFeatureRates;
use App\Models\Rate_endowment;
use App\Models\SumAssured;
use App\Models\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\CompareController as BackendCompareController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewLeadCreated;
use App\Models\User;
class CompareController extends Controller
{
    public function leadCreate(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'phone' => 'exclude_if:phone_number,null|digits_between:10,13',
                'province' => 'required',
            ],
            [
                'name.required' => 'Name Cannot Be Empty!!',
                'phone.required' => 'Mobile number Cannot Be Empty!!',
                'province.required' => 'Province should be selected!!',
            ]
        );
        $data = array(
            "is_user" => "0",
            "dob" => date('Y-m-d', strtotime($request->dob)),
            'age' => $request->age,
            "customer_name" => $request->name,
            "phone" => $request->phone,
            "email" => $request->email,
            "province"=>$request->province,
            'sum_insured'=>$request->sumassured,
            'maturity_period'=>$request->term,
            "leadsource_id" => 1,
            "leadtype_id" => 2,
            'policy_type' => 1,
            'category'=>$request->category
        );
        $lead = Lead::create($data);
        Session::forget('LEAD_ID');
        Session::put('LEAD_ID',$lead->id);
        if($lead){
            Notification::send(User::findorfail(1),new NewLeadCreated($lead));
        }
        if(!$request->ajax() && $lead){
            return redirect()->back()->with('message','Successfully submitted');
        }
        if ($lead) {
            Session::put('customerLeadId', $lead->id);
            return response()->json([
                'type' => 'success',
                'message' => 'Successfully Added.',
                'data' => 1,
            ],200);
        }
    }

    public function showCompareForm()
    {
        $data['categories'] = Product::get()->unique('category')->pluck('category');
        $data['mops'] = GeneralSetting::where('type', 'mop')->pluck('value');

        return view('frontend.calculator-screen', $data);
    }

    public function viewPlan(Request $request){
        $benifit=Product::with('company')->where('id',$request->id)->first();
        $headerPart='<div class="invoice-header-wrapper">
                        <div class="web-logo">
                            <img src="'.asset('frontend/img/logo.png').'">
                        </div>
                        <div class="company-invoice-wrapper">
                            <div class="invoice-company-logo">
                                <img src="'.asset('/images/company/'.$benifit->company->logo).'">
                            </div>
                            <div class="invoice-company-name">
                                <p>'.$benifit->company->name.'&nbsp;&nbsp;&nbsp;('.$benifit->name.')</p>
                            </div>
                        </div>
                        </div>
                    </div>';
        if(!empty($benifit)){
            return response()->json(['benifit'=>$benifit->benefit,'headerPart' =>$headerPart],200);
        }else{
            return response()->json(['benifit'=>'Not Found']);
        }
    }
}
