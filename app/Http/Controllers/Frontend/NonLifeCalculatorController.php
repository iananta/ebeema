<?php

namespace App\Http\Controllers\Frontend;

use Auth;
use App\Models\KYC;
use Carbon\Carbon;
use App\Models\CompanyDetail;
use App\Models\GeneralSetting;
use App\Models\CustomerPolicy;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\MotorCalculationData;
use Illuminate\Http\Request;
use App\Models\ImepayResponse;
use App\Models\Lead;
use Illuminate\Http\Response;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use PDF;
use DB;
use Mail;
// use Request; 
use Redirect;
use Storage;

class NonLifeCalculatorController extends Controller
{
    public function CalculatorData($dta)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $datas = array(
            "ClassId" => $dta,
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetExcessOwnDamage",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
              "TYPECOVER":"",
              "VEHICLETYPE":"",
              "CATEGORYID":"",
              "CLASSCODE":""
          }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
      ));
        $data['excessdamages'] = json_decode(curl_exec($curl));
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetCategoryList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $data['categories'] = json_decode(curl_exec($curl));
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeyearList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
             "NAMEOFVEHICLE":"20"
         }',
         CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
     ));

        $data['makeYearLists'] = json_decode(curl_exec($curl));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetTypeofCoverList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
             "NAMEOFVEHICLE":""
         }',
         CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
     ));
        $data['typeCovers'] = json_decode(curl_exec($curl));
        curl_close($curl);
        return $this->variable = $data;
    }

    public function customerAjaxData()
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetKYCCategory",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $kycCat = json_decode(curl_exec($curl));
        $data['kycCategories'] = $kycCat->data ?? array();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetInsuredType",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $insuredTypes = json_decode(curl_exec($curl));
        $data['insuredTypes'] = $insuredTypes->data ?? array();
        

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/getkycclassification",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $kycClassifications = json_decode(curl_exec($curl));
        $data['kycClassifications'] = $kycClassifications->data ?? array();
    

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetKYCRiskCategory",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $riskCategories = json_decode(curl_exec($curl));
        $data['riskCategories'] = $riskCategories->data ?? array();
      
       

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/Area/GetProvince",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $provinces = json_decode(curl_exec($curl));
        $data['provinces'] = $provinces->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetKYCOccupation",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $kycOccupations = json_decode(curl_exec($curl));
        $data['kycOccupations'] = $kycOccupations->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetIncomeSource",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $kycincomesources = json_decode(curl_exec($curl));
        $data['kycincomesources'] = $kycincomesources->data ?? array();
        curl_close($curl);
        return $this->variable = $data;
    }

    public function nonlifecalculationForm(Request $request)
    {
        try {
            Session::forget('motorCalcId');
            if($request->pathInfo !='/Nonlifeinsurance/calculator/bike'){
               $classId=21;
           }
           $this->CalculatorData($classId);
           $data = $this->variable ?? [];
           $data['classId']=$classId;
           return view('frontend.nonlife-calculator-screen')->with($data);
       } catch (\Exception $e) {
        return $data['data'] = $e->getMessage();
    }
}

    public function calculationMotor(Request $request)
    {
        // dd($request->all());
        try {
            $companiesDetail = CompanyDetail::where('status', 1)->get();
            if ($request->CLASSID == 21) {
                Session::put('calculationFor', 'Bike');
            } elseif ($request->CLASSID == 22) {
                Session::put('calculationFor', 'Commercial Vehicle');
            } elseif ($request->CLASSID == 23) {
                Session::put('calculationFor', 'Private Vehicle');
            }
           
            $url = GeneralSetting::where('key', 'api_url')->first()->value;
            $datas = array(
                "EXPUTILITIESAMT" => $request->EXPUTILITIESAMT ?? '0',
                "CATEGORYID" => $request->CATEGORYID,
                "CARRYCAPACITY" => $request->CARRYCAPACITY ?? '0',
                "YEARMANUFACTURE" => $request->YEARMANUFACTURE,
                "CCHP" => $request->CCHP,
                "EODAMT" => $request->EODAMT ?? '0',
                "PADRIVER" => $request->PADRIVER ?? '0',
                "PAPASSENGER" => $request->PAPASSENGER ?? '0',
                "PACONDUCTOR" => $request->PACONDUCTOR ?? '0',
                "NOOFEMPLOYEE" => $request->NOOFEMPLOYEE ?? '0',
                "NCDYR" => $request->NCDYR ?? '0',
                "NOOFPASSENGER" => $request->NOOFPASSENGER ?? '0',
                "PRIVATE_USE" => $request->PRIVATE_USE ?? '0',
                "INCLUDE_TOWING" => $request->INCLUDE_TOWING ?? '0',
                "ISGOVERNMENT" => $request->ISGOVERNMENT ?? '0',
                "pool_premium" => $request->pool_premium ?? '0',
                "TYPECOVER" => $request->TYPECOVER,
                "CLASSID" => $request->CLASSID,
                "HASAGENT" => $request->HASAGENT ?? 0,
                "EXCLUDE_POOL" => $request->EXCLUDE_POOL ?? '0',
                "compulsaryexcessamount" => $request->compulsaryexcessamount ?? '0',
                "Driver" => $request->Driver ?? '0',
                "PASSCAPACITY" => $request->PASSCAPACITY ?? '0',
            );
            $datas['stamp'] = 20;
            if ($request->EXPUTILITIESAMT < 100000) {
                $datas['stamp'] = 10;
            }
            $start = microtime(true);
            $companyData=[];
            $requests = [];
            $mh = curl_multi_init();
            foreach ($companiesDetail as $key=>$company) {
                $cmpUrl = $company->api_url. "/Api/Calculator/CalculateMotorPremium";
                $requests[$key]['curl_handle'] =curl_init($cmpUrl);

                curl_setopt($requests[$key]['curl_handle'], CURLOPT_RETURNTRANSFER, true);
                curl_setopt($requests[$key]['curl_handle'], CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($requests[$key]['curl_handle'], CURLOPT_TIMEOUT, 5);
                curl_setopt($requests[$key]['curl_handle'], CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt_array($requests[$key]['curl_handle'], array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 2,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($datas),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                    ),
                ));

                curl_multi_add_handle($mh,  $requests[$key]['curl_handle']);
                $companyData[]=[
                    'company_id' => $company->company_id ?? 'N/A',
                    'company_name' => $company->company->name ?? 'N/A',
                    'company_code' => $company->company->code ?? 'N/A',
                    'company_image' => $company->company->logo ?? 'N/A',
                ];
                
            }
            $stillRunning = false;
            do {
                curl_multi_exec($mh, $stillRunning);
            } while ($stillRunning);
            foreach($requests as $key => $request){
                curl_multi_remove_handle($mh, $request['curl_handle']);
                $requests[$key]['content'] = curl_multi_getcontent($request['curl_handle']);
                $requests[$key]['http_code'] = curl_getinfo($request['curl_handle'], CURLINFO_HTTP_CODE);
                $companyData[$key]['data']=json_decode($requests[$key]['content']);
                curl_close($requests[$key]['curl_handle']);
            }
            curl_multi_close($mh);
            $premiums= $companyData;
         
            $datas['time_elapsed_secs'] = microtime(true) - $start;
            Session::forget('reqCalData');
            Session::put('reqCalData', $datas); 
            Session::forget('premiums');
            Session::put('premiums', $premiums);
            
          // dd($premiums);
            if ($premiums) {
                return redirect()->route('frontend.nonlife.insurance.bike.view');
            } else if (Session::get('motorCalcId') && Session::get('crudType') == 'update') {
                return redirect()->back();
            } else {
                return redirect()->back()->withInput()->withErrors('Something went wrong. Please contact support for further assitance.');
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->withInput()->withErrors('Something went wrong.' . $e->getMessage());
//            return $e->getMessage();
        }

    }

    public function calculationMotorview()
    {
    try{
        
        Session::flash('Selectplan', 'you need to Login');
        $data['requested_data'] = Session::get('reqCalData');                                                  
        $data['premiums'] = Session::get('premiums');
        return view('frontend.non-lifecompare', $data);
    } catch (\Exception $e) {
        dd($e->getMessage());
        return redirect()->back()->withInput()->withErrors('Something went wrong.' . $e->getMessage());
    }
    }
    function getMotorDetails($reqData)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;

        $curl = curl_init();
        $makeVehicle = array(
            "CLASSID" => $reqData['CLASSID'],
        );
//            dd($makeVehicle);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeVehicleList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($makeVehicle),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['manufacturers'] = json_decode(curl_exec($curl));
//dd($data);
        $cat = array(
            "CLASSID" => $reqData['CLASSID'],
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetVehicleNameList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($cat),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['vehicleNames'] = json_decode(curl_exec($curl));

        //Occupation List
        $typc = array(
            "NAMEOFVEHICLE" => '',
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetOccupationList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($typc),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['occupationLists'] = json_decode(curl_exec($curl));
        $cat = array(
            "NAMEOFVEHICLE" => $reqData['CLASSID'],
            "MAKEVEHICLEID" => $reqData['MAKEVEHICLEID']
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeModel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($cat),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $data['makeModels'] = json_decode(curl_exec($curl));
        
        curl_close($curl);
        return $data;
    }

    public function nonlifedetail(Request $request)
    {
  
    if($request->has('merchantCode'))
    {
        $data['paymentFrom'] = $request->PaymentFrom ?? 'N/A';
        $data['output'] = $request->output;
        $data['data'] = $request->data;
        $data['premiums']=Session::get('premiums'); 
        return view('frontend.nonlifeinsurance.invoice',$data);
    }else{
       
        $this->customerAjaxData();
        $data = $this->variable;
        // $kyc = KYC::where('customer_id', Session::get('customer_id'))->first();
        $datas['user_id'] = Auth::user()->id;
        $companyDetail = CompanyDetail::where('company_id',$request->company_id)->first();
        $curl = curl_init();
        $newRefID = $request->company_code.'-' . $this->random_strings(5) . '-' . Carbon::now()->format('dmy');
        $merchantDetails = json_decode($companyDetail->imepay_credentials);
        $url = $merchantDetails->ime_pay_token;
        $data['ime_pay_checkout_url'] = $merchantDetails->ime_pay_checkout;
        $module = base64_encode($merchantDetails->ime_module);
        $imeUser = base64_encode($merchantDetails->ime_pay_username . ':' . $merchantDetails->ime_pay_user_password);
        $merchantCode = $merchantDetails->ime_merchant_code;
        $data['merchantCode'] = $merchantDetails->ime_merchant_code;
        $data['merchantName'] = $merchantDetails->ime_merchant_name;
        
        $pricePayable = 0;
        if (Session::get('motorCalcId')) {
            $mtData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
            $pricePayable = $mtData->TOTALNETPREMIUM;
            if (isset($mtData)) {
                if (session::get('regenerate')) {
                    $refnumber = $newRefID;
                } else {
                    $refnumber = $mtData->payment_ref_id ?? $newRefID;
                }
            }
        } else {
            $mtData = [];
            $refnumber = '';
        }
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"MerchantCode\":\"$merchantCode\", \"Amount\":\" 1.00\", \"RefId\":\"$refnumber\" } ",
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Basic ' . $imeUser,
                'Module: ' . $module,
            ],
        ));
        if (curl_exec($curl) === false) {
            echo 'Curl error: ' . curl_error($curl);
        } else {
            $response = curl_exec($curl);
        }
        curl_close($curl);
        $data['paymentInfo'] = json_decode($response);
      
        $url = $companyDetail->api_url;
        $curl = curl_init();
        $data['categoryLists'] = json_decode(curl_exec($curl));
        $makeVehicle = array(
            "CLASSID" => Session::get('CLASSID'),
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeVehicleList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($makeVehicle),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $data['manufacturers'] = json_decode(curl_exec($curl));
            //MakeModels
        $cat = array(
            "NAMEOFVEHICLE" => $request['CLASSID'],
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeModel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($cat),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $data['makeModels'] = json_decode(curl_exec($curl));
            //Vechile Name
        $cat = array(
            "CLASSID" => $request['CLASSID'],
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetVehicleNameList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($cat),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $data['vehicleNames'] = json_decode(curl_exec($curl));
        
            //Occupation List
        $typc = array(
            "NAMEOFVEHICLE" => '',
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetOccupationList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($typc),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        
        $data['company_premiums']=json_decode($request->company_id);
        $data['occupationLists'] = json_decode(curl_exec($curl));
        $datas['user_id'] = Auth::user()->id;
        $data['premiums'] =json_decode($request->premium);
        $data['requested_data'] = $request;
        $calcData = '';
        $storeDta = [
            'company_id' => $request->company_id,
            'payment_ref_id' => $data['paymentInfo']->RefId ?? '',
            'payment_token_details' => json_encode($response) ?? '',
        ];
        if (Session::get('motorCalcId') && $data['paymentInfo']->ResponseCode == 0) {
         $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
     }
     if (session::get('regenerate')) {
        return response()->json([
            'type' => 'success',
            'message' => 'Reference Id generated Successfully'
        ], 200);
    }
  
    // $data['directData'] = MotorCalculationData::all()->last();
    $data['directData'] = MotorCalculationData::where('user_id',Auth::user()->id)->orderByDesc('id')->first();
    // if (Session::get('motorCalcId')) {
    if (session()->has('motorCalcId')) {
        $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['CLASSID', 'customer_id', 'created_at', 'MODEL', 'MAKEMODELID', 'VEHICLENAMEID', 'bluebook_image', 'bike_image', 'MAKEVEHICLEID', 'TYPECOVER', 'YEARMANUFACTURE', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id']);
        $datas = $this->getMotorDetails($data['formData']);
    }
    $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
    $data['company_id'] = Session::get('reqCalData');
    $data['company_code'] = Session::get('premiums');
    // $data['CHASISNO'] = mysql_real_escape_string($_POST['department']);
    // $data['ENGINENO'] = mysql_real_escape_string($_POST['name']);
    Session::put('ime_pay_checkout_url',$data['ime_pay_checkout_url']);
    Session::put('paymentInfo',$data['paymentInfo']);
    return view('frontend.nonlifeinsurance.bike.detailmotor',$data,$datas);
    }
    }    

    public function paymentview(Request $request)
    {
        $data['paymentFrom'] = $request->PaymentFrom ?? 'N/A';
        $data['output'] = $request->output;
        $data['data'] = $request->data;
        $data['premiums']=Session::get('premiums'); 
        return view('frontend.nonlifeinsurance.invoice',$data);
    }


    public function detailmotor(Request $request)
    {   
    
    $this->validate(
        $request,
        [
            'VEHICLENO' => 'required',
            'ENGINENO' => 'required',
            'CHASISNO' => 'required',
            'MAKEVEHICLEID' => 'required',
            'MODEUSE' => 'required',
            'MAKEMODELID' => 'required',
            'bluebook_images' => 'mimes:jpeg,png,pdf',
        ],
        [
            'VEHICLENO.required' => 'Vehicle No is required!!',
            'ENGINENO' => 'Engine No is required',
            'CHASISNO' => 'Chasis No is required',
            'MAKEVEHICLEID' => 'Make Vehicle ID is required',
            'MODEUSE' => 'required',
            'MAKEMODELID' => 'required',
            'bluebook_images' => 'file must be smaller than 2MB',

        ]
    );
    try {
        if ($request->file('bluebook_images')) {
            $path = $request->bluebook_images->store('motorBluebook', 'public');
            $imgFile = Storage::disk('public')->url($path);
            $request->merge(['bluebook_image' => $imgFile]);
        }
        if ($request->file('bike_images')) {
            $path1 = $request->bike_images->store('motorBike', 'public');
            $imgFile1 = Storage::disk('public')->url($path1);
            $request->merge(['bike_image' => $imgFile1]);
        }
        $data['user_id'] = Auth::user()->id;
        $request->merge(Session::get('reqCalData'));
        $calcData = '';
        if (!Session::get('motorCalcId')) {
            $calcData = MotorCalculationData::create($request->all());
                // MotorCalculationData::findOrFail($calcData->id)->update($data['CalculationData']);
            Session::forget('motorCalcId');
            Session::put('motorCalcId', $calcData->id);
        } else {
            $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($request->except('_token'));
                // Session::put('crudType', 'update');
        }
        $data['users'] = MotorcalculationData::where('VEHICLENO', $request->VEHICLENO)->first();
            //dd($data['users']);
        $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['YEARMANUFACTURE', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id']);
        $data['makePolicy'] = '1';
        Session::flash('submit', 'You need to login');
        // $vehicleData = view('frontend.nonlifeinsurance.review', $data)->render();
        return response()->json([
            'success' => 'Vehicle Created Successfully',
            
        ]);  
   
            //return response()->json([$value1, $value2]);    
    } catch (\Exception $e) {
        dd($e->getMessage());
        return redirect()->back()->withInput()->withErrors('Something went wrong.' . $e->getMessage());
    }
}


    //Private Car
    public function carCalculator(Request $request)
    {
        try{
            Session::forget('motorCalcId');
            if(($request->pathInfo !='/Nonlifeinsurance/calculator/privatecar')){
                $classId=23;
            }
            $this->CalculatorData($classId);
            $data = $this->variable ?? [];
            $data['classId']=$classId;
            return view('frontend.nonlife-calculator-screen', $data);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return redirect()->back()->withInput()->withErrors('Something went wrong.' . $e->getMessage());
    //            return $e->getMessage();
        }
    }
    //CommercialCar
    public function commercialCarCalculator(Request $request)
    {
        try{
        if(($request->pathInfo !='/Nonlifeinsurance/calculator/commerical-car')){
            $classId=22;
        }
        Session::forget('motorCalcId');
        $this->CalculatorData($classId);
        $data = $this->variable ?? [];
        $data['classId'] = $classId;
        return view('frontend.nonlife-calculator-screen', $data);
    } catch (\Exception $e) {
        dd($e->getMessage());
        return redirect()->back()->withInput()->withErrors('Something went wrong.' . $e->getMessage());
    }
    }

    // After Vechile Detail form
    public function getModel(Request $request)
    {
        $datas = array(
            "MAKEVEHICLEID" => $request->mfComp,
        );
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeModel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $output = json_decode(curl_exec($curl));
        $data['output'] = $output->data ?? $output->Message;

        curl_close($curl);
        return $data;
    }

    public function random_strings($length_of_string)
    {
            // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

            // Shufle the $str_result and returns substring
            // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);
    }

        // After select customer update database
    public function choosecustomer(Request $request)
    {
        
    }

    //Motor payment for individual
    public function paymentBikeFirstParty(Request $request)
    {
    try {   
        $kyc = KYC::where('customer_id', $request->customer_id)->first();
        Session::put('KycId', $kyc->KYCID);
        if ($kyc) {
            $curl = curl_init();
            $refnumber = 'NB-' . $this->random_strings(5) . '-' . Carbon::now()->format('dmy');
            $url = GeneralSetting::where('key', 'ime_pay_token')->first()->value;
            $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
            $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
            $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
            $pricePayable = 0;
            if (Session::get('motorCalcId')) {
                $mtData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
                $pricePayable = $mtData->TOTALNETPREMIUM;
            }

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{ \"MerchantCode\":\"$merchantCode\", \"Amount\":\"$pricePayable\", \"RefId\":\"$refnumber\" } ",
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $imeUser,
                    'Module: ' . $module
                ],
            ));
            if (curl_exec($curl) === false) {
                echo 'Curl error: ' . curl_error($curl);
            } else {
                $response = curl_exec($curl);
            }
            $data['paymentInfo'] = json_decode($response);
            curl_close($curl);
            $data['ime_pay_checkout_url'] = GeneralSetting::where('key', 'ime_pay_checkout')->first()->value;
            $calcData = '';
            $storeDta = [
                'customer_id' => $request->customer_id,
                'payment_ref_id' => $data['paymentInfo']->RefId ?? '',
                'payment_token_details' => json_encode($response) ?? '',
            ];
            if (Session::get('motorCalcId')) {
                $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
            }
            return view('frontend.nonlifeinsurance.bike.payments', $data);
        } else {
            return response()->json([
                'type' => 'error',
                'message' => 'Kyc not Added ! Please Add the Kyc for the customer.'
            ], 200);
        }
    } catch (\Exception $e) {
        return $e->getMessage();
        return redirect()->back()->withErrors($e->getMessage());
    }
}

public function compulsaryExcess(Request $request)
{
    $url = GeneralSetting::where('key', 'api_url')->first()->value;
    $datas = array(
        "TYPECOVER" => $request->TYPECOVER,
        "CATEGORYID" => $request->CATEGORYID,
        "YEARMANUFACTURE" => $request->YEARMANUFACTURE,
    );
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetCompulsoryExcess",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($datas),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
    ));

    $excessCal = json_decode(curl_exec($curl));
    curl_close($curl);
    $excessVal = $excessCal->data ? $excessCal->data[0]->CompulsoryExcess : '0';
    if ($excessVal > 0) {
        return response()->json([
            'type' => 'success',
            'CompulsoryExcess' => $excessVal,
            'message' => 'Calculated successfully.',
        ], 200);
    } else {
        return response()->json([
            'type' => 'success',
            'CompulsoryExcess' => $excessVal,
            'message' => 'Calculation error.',
        ], 200);
    }
}

public function excessDamage(Request $request)
{
    $url = GeneralSetting::where('key', 'api_url')->first()->value;
    $datas = array(
        "TYPECOVER" => $request->TYPECOVER,
        "CATEGORYID" => $request->CATEGORYID,
        "CLASSCODE" => $request->CLASSCODE,
        "VEHICLETYPE" => $request->VEHICLETYPE,
    );
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetExcessOwnDamage",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($datas),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
    ));
    $excessdamages = json_decode(curl_exec($curl));
    curl_close($curl);
    $excessdamages = $excessdamages->data ? $excessdamages->data : '';
    if ($excessdamages) {
        return response()->json([
            'type' => 'success',
            'excessdamages' => $excessdamages,
            'message' => 'Calculated successfully.',
        ], 200);
    } else {
        return response()->json([
            'type' => 'success',
            'excessdamages' => $excessdamages,
            'message' => 'Calculation error.',
        ], 200);
    }
}

public function policyDone(Request $request)
{
    try {
        Session::forget('motorCalcId');
        $data['paymentFrom'] = $request->PaymentFrom ?? 'N/A';
        $data['output'] = $request->output;
        $data['data'] = $request->data;
        return view('frontend.nonlifeinsurance.invoice',$data);
    } catch (\Exception $e) {
        dd($e->getMessage());
        $data['output'] = $request->output;
        $data['data'] = $request->data;
        $data['PaymentFrom'] = $request->payment_method ?? 'N/A';
        return view('frontend.nonlifeinsurance.bike.policies', $data);
    }
}

    // public function viewPolicy(Request $request)
    // {

    // }

public function viewDraftPolicy(Request $request)
{
    if (Auth::user()->role_id == 1) {
        $data['policies'] = MotorCalculationData::latest()->get();
    } else {
        $data['policies'] = MotorCalculationData::where('user_id', Auth::user()->id)->orderByDesc('id')->get();
    }
    return view('', $data);
}

public function imePaySuccess(Request $request)
{
    try {
        $curl = curl_init();
        $url = GeneralSetting::where('key', 'ime_pay_confirm')->first()->value;
        $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
        $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
        $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
        $date = Carbon::now();
        $response = base64_decode($request->data);
        $pieces = explode("|", $response);
       
        $data['pieces'] = $pieces;
        $datas = array(
            "MerchantCode" => $data['merchantCode'],
            "RefId" => $pieces[4],
            "TokenId" => $pieces[6],
            "TransactionId" => $pieces[3],
            "Msisdn" => $pieces[2],
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Basic ' . $imeUser,
                'Module: ' . $module,
            ],
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        if ($response === false) {
            $data['data'] = "Payment Validation Error";
            return view('frontend.nonlifeinsurance.bike.policies', $data);
            echo 'Payment Validation error ' . $response;
        } else {
            $data['paymentInfo'] = json_decode($response);
            if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
                ImepayResponse::create([
                    "MerchantCode" => "INSURANCE",
                    "ImeTxnStatus" => $pieces[0],
                    "ResponseDescription" => $pieces[1],
                    "Msisdn" => $pieces[2],
                    "TransactionId" => $pieces[3],
                    "RefId" => $pieces[4],
                    "TranAmount" => $pieces[5],
                    "TokenId" => $pieces[6],
                    "RequestDate" => $date,
                    "ResponseDate" => $date,
                ]);
            }
            $storeDta = [
                'payment_url' => json_encode($request->data) ?? '',
                'status' => 1,
            ];
            if (Session::get('motorCalcId')) {
                $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
            }
            $data['customers'] = Lead::where('is_user', 1)->get();
            $data['makePolicy'] = 1;
            return redirect()->route('motor.construct.policy', $data);
        }
    } catch (\Exception $e) {
        $data['data'] = $e->getMessage();
        return view('', $data);
        return $e->getMessage();
    }
}

public function imePayCancel(Request $request)
{
    $data['customers'] = Lead::where('is_user', 1)->get();
    $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
    $date = Carbon::now();
    $response = base64_decode($request->data);
    $pieces = explode("|", $response);
    $data['pieces'] = $pieces;
    try {
        if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
            ImepayResponse::create([
                "MerchantCode" => "INSURANCE",
                "ImeTxnStatus" => $pieces[0],
                "ResponseDescription" => $pieces[1],
                "Msisdn" => $pieces[2],
                "TransactionId" => $pieces[3],
                "RefId" => $pieces[4],
                "TranAmount" => $pieces[5],
                "TokenId" => $pieces[6],
                "RequestDate" => $date,
                "ResponseDate" => $date,
            ]);
        }
        $storeDta = [
            'payment_url' => json_encode($request->data) ?? '',
            'status' => 2,
        ];
        if (Session::get('motorCalcId')) {
            $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
        }
        return redirect()->back()->withInput()->withErrors('Something went wrong. Try again');
    } catch (\Exception $e) {
        $data['data'] = $e->getMessage();
        return view('frontend.nonlifeinsurance.bike.policies', $data);
        $e->getMessage();
    }
}

public function makeDraftPolicy(Request $request)
{
 
    try {
        if ($request->policy_id && $request->reference_number && $request->status == 1) {
            $dta = CustomerPolicy::where('reference_number', $request->reference_number)->first();
            if ($dta && $dta->status == 1) {
                return redirect()->back()->withErrors('Policy Already Made');
            }
            Session::forget('motorCalcId');
            Session::put('motorCalcId', $request->policy_id);
            return redirect()->route('nonLife.calculator.motor.make.policy');
        }
        if ($request->policy_id && !$request->reference_number) {
            $this->customerAjaxData();
            $data = $this->variable;
            Session::forget('motorCalcId');
            Session::put('motorCalcId', $request->policy_id);
            if (Session::get('motorCalcId')) {
                $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['YEARMANUFACTURE', 'TYPECOVER', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'PRIVATE_USE', 'ISGOVERNMENT', 'HASAGENT', 'HAS_TRAILOR', 'BRANCHID', 'CLASSID', 'DEPTID', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'MAKEVEHICLEID', 'MAKEMODELID', 'MODEL', 'VEHICLENAMEID', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id', 'customer_id']);
            }
            $data['makePolicy'] = '1';
            $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
            return view('', $data);
        }
        if ($request->policy_id && $request->reference_number && $request->status == 0 || $request->policy_id && $request->reference_number && $request->status == 2) {
            $this->customerAjaxData();
            $data = $this->variable;
            Session::forget('motorCalcId');
            Session::put('motorCalcId', $request->policy_id);
            if (Session::get('motorCalcId')) {
                $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['YEARMANUFACTURE', 'TYPECOVER', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'PRIVATE_USE', 'ISGOVERNMENT', 'HASAGENT', 'HAS_TRAILOR', 'BRANCHID', 'CLASSID', 'DEPTID', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'MAKEVEHICLEID', 'MAKEMODELID', 'MODEL', 'VEHICLENAMEID', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id', 'customer_id']);
                $datas = $this->getVehicleDetail($data['formData']);
            }
            $data['makePolicy'] = '1';
            $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
            return view('frontend.nonlifeinsurance.bike.detailmotor', $data,$datas);
        }
    } catch (\Exception $e) {
        $data['data'] = $e->getMessage();
        return view('ss', $data);
    }
}

public function makeOnlinePolicy(Request $request)
{
    
    try {
        $output = ["Something went wrong"];
        if (Session::get('motorCalcId')) {
            $calcData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
            $policy = CustomerPolicy::where('reference_number', $calcData->payment_ref_id)->first();
            if (isset($policy) && $calcData->payment_ref_id == $policy->reference_number) {
                $data['data'] = "Policy Already Made. Please Start Again to Add new policy. To see policy click below.";
                return view('frontend.nonlifeinsurance.invoice',$data);
            }
        }
        if (Session::get('motorCalcId')) {
            $calcData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
        }
        $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        if ($calcData && $calcData['TYPECOVER'] == "TP") {
            $agentCode = "0000";
        } else {
            $agentCode = Auth::User()->userAgents->first()->liscence_number ?? '0000';
        }
        $datas = array(
            "KYCID" => $calcData->customer->KYCID ?? "0000",
            "BRANCHID" => $calcData['BRANCHID'] ?? 1,
            "DEPTID" => $calcData['DEPTID'] ?? 1,
            "HASAGENT" => $calcData['HASAGENT'] ?? 0,
            "AGENTCODE" => $agentCode,
            "INCLUDE_TOWING" => $calcData['INCLUDE_TOWING'] ?? 0,
            "ISGOVERNMENT" => $calcData['ISGOVERNMENT'] ?? 0,
            "EXCLUDE_POOL" => $calcData['EXCLUDE_POOL'] ?? 0,
            "CLASSID" => $calcData['CLASSID'] ?? 21,
            "CATEGORYID" => $calcData['CATEGORYID'] ?? 0,
            "TYPECOVER" => $calcData['TYPECOVER'] ?? '',
            "BUSSOCCPCODE" => $calcData['BUSSOCCPCODE'] ?? 0,
            "MODEUSE" => $calcData['MODEUSE'] ?? '',
            "MAKEVEHICLEID" => $calcData['MAKEVEHICLEID'] ?? 0,
            "MAKEVEHICLE" => "cfO;",
            "HAS_TRAILOR" => $calcData['HAS_TRAILOR'] ?? 0,
            "MAKEMODELID" => $calcData['MAKEMODELID'] ?? 0,
            "MODEL" => $calcData['MODEL'] ?? '',
            "VEHICLENAMEID" => $calcData['VEHICLENAMEID'] ?? 0,
            "NAMEOFVEHICLE" => ":^f/ :sn a",
            "YEARMANUFACTURE" => $calcData['YEARMANUFACTURE'] ?? 0,
            "CCHP" => $calcData['CCHP'] ?? 0,
            "CARRYCAPACITY" => $calcData['CARRYCAPACITY'] ?? 0,
            "REGDATE" => $calcData['REGISTRATIONDATE'] ?? null,
            "EXPUTILITIESAMT" => $calcData['EXPUTILITIESAMT'] ?? 0,
            "UTILITIESAMT" => 0,
            "OGCPU" => true,
            "VEHICLENO" => $calcData['VEHICLENO'] ?? '',
            "RUNNINGVEHICLENO" => "",
            "EVEHICLENO" => $calcData['VEHICLENO'] ?? '',
            "ERUNNINGVEHICLENO" => "",
            "ENGINENO" => $calcData['ENGINENO'] ?? '',
            "CHASISNO" => $calcData['CHASISNO'] ?? '',
            "EODAMT" => $calcData['EODAMT'] ?? 0,
            "NOOFVEHICLES" => "0",
            "NCDYR" => $calcData['NCDYR'] ?? 0,
            "PADRIVER" => $calcData['PADRIVER'] ?? 0,
            "NOOFEMPLOYEE" => $calcData['NOOFEMPLOYEE'] ?? 0,
            "PACONDUCTOR" => $calcData['PACONDUCTOR'] ?? 0,
            "PACLEANER" => 0,
            "NOOFPASSENGER" => $calcData['NOOFPASSENGER'] ?? 0,
            "Driver" => $calcData['Driver'] ?? 0,
            "PASSCAPACITY" => $calcData['PASSCAPACITY'] ?? 0,
            "PAPASSENGER" => $calcData['PAPASSENGER'] ?? 0,
            "ESTCOST" => $calcData['EXPUTILITIESAMT'] ?? 0,
            "OTHERSI" => 0.00,
            "OTHERSIDESC" => "",
            "SHOWROOM" => 0,
            "Vehicleage" => 0,
            "BASICPREMIUM_A" => $calcData['BASICPREMIUM_A'] ?? 0,
            "THIRDPARTYPREMIUM_B" => $calcData['THIRDPARTYPREMIUM_B'] ?? 0,
            "DRIVERPREMIUM_C" => $calcData['DRIVERPREMIUM_C'] ?? 0,
            "HELPERPREMIUM_D" => $calcData['HELPERPREMIUM_D'] ?? 0,
            "PASSENGERPREM_E" => $calcData['PASSENGERPREM_E'] ?? 0,
            "RSDPREMIUM_F" => $calcData['RSDPREMIUM_F'] ?? 0,
            "PAIDAMT" => $calcData['TOTALNETPREMIUM'] ?? 0,
            "STAMPDUTY" => $calcData['stamp'] ?? 0,
            "VATRATE" => $calcData['VAT'] ?? 0,
            "VATAMT" => $calcData['VATAMT'] ?? 0,
            "COD" => $calcData['compulsaryexcessamount'] ?? 0,
            "MERCHANT_TRANS_NO" => $calcData['payment_ref_id'] ?? 0,
            "TRANS_DATE" => Carbon::now()->format('d-M-Y'),
            "MERCHANT_CODE" => 
            "imepay@lmtrading",
            "MERCHANT_PASSWORD" => "",
            "TOTAL_PREMIUM_BEFORE_VAT" => $calcData['NETPREMIUM'] ?? 0,
            "customer_id" => $calcData->customer_id ?? 0,
            "company_id" => $calcData->company_id ?? 0,
            "status" => $calcData->status ?? 0,
            "user_id" => $calcData['user_id'] ?? 0,
            "FOCODE" => '0738',
            "payment_method" => $calcData['payment_method'] ?? 'n/a'
        );
        $curl = curl_init();
        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/api/Policy/MakeOnlinePolicy",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $apiUser,
            ),
        ));
        $output = json_decode(curl_exec($curl));
        $data['output'] = $output ?? $output->message;
        $data['data'] = $output->data ?? $output->message;
        curl_close($curl);
        if($output->data != null && $output->data[0]->FLAG == "SUCCESS") {
            $datas['output'] = json_encode($data['data']);
            $datas['reference_number'] = $calcData['payment_ref_id'] ?? '0';
            $datas['newCstPolicy'] = CustomerPolicy::create($datas);
            $data['PaymentFrom'] = $calcData['payment_method'] ?? 'N/A';
            return redirect()->route('fronted.nonlife.details', $data);
        }
        else{
            if (isset($calcData)) {
                $calcData->update(["payment_output" => json_encode($output)]);
                        // dd($calcData);
            }
            return redirect(route('fronted.nonlife.details', $data));
        }
    } catch (\Exception $e) {
        $data['data'] = json_encode($output) ?? $e->getMessage();
        $data['output'] = $e->getMessage();
        if (isset($calcData)) {
            $calcData->update(["payment_output" => json_encode($output)]);
        }
        return redirect(route('fronted.nonlife.details', $data));
    }
}
public function getCatType(Request $request)
{
    $url = GeneralSetting::where('key', 'api_url')->first()->value;
    $data = array(
        "CATEGORYID" => $request->CATEGORYID,
    );
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetClassCategory",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
    ));

    $CategoryType = json_decode(curl_exec($curl));
    curl_close($curl);
    $CategoryTypeData = $CategoryType->data[0] ?? $output->Message;
    if ($CategoryType) {
        return response()->json([
            'type' => 'success',
            'CategoryType' => $CategoryTypeData,
            'message' => 'Calculated successfully.',
        ], 200);
    } else {
        return response()->json([
            'type' => 'error',
            'CategoryType' => '',
            'message' => 'Calculation error.',
        ], 500);
    }
}

public function getDebitNote(Request $request)
{
    try {
        $data['data'] = MotorCalculationData::find($request->motorId);
        return $data;
        $pdf = PDF::loadView('frontend.nonlifeinsurance.debitgetnote', $data);
        return $pdf->download('Draft-policy.pdf');
    } catch (\Exception $e) {
        echo('Something went wrong, Cannot download PDF');
    }
}

function getVehicleDetail($reqData)
{
    $url = GeneralSetting::where('key', 'api_url')->first()->value;
    $curl = curl_init();
    $makeVehicle = array(
        "CLASSID" => $reqData['CLASSID'],
    );
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeVehicleList",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($makeVehicle),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $data['manufacturers'] = json_decode(curl_exec($curl));
    $cat = array(
        "CLASSID" => $reqData['CLASSID'],
    );
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetVehicleNameList",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($cat),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));
    $data['vehicleNames'] = json_decode(curl_exec($curl));

        //Occupation List
    $typc = array(
        "NAMEOFVEHICLE" => '',
    );
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetOccupationList",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($typc),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));
    $data['occupationLists'] = json_decode(curl_exec($curl));

    $cat = array(
        "NAMEOFVEHICLE" => $reqData['CLASSID'],
        "MAKEVEHICLEID" => $reqData['MAKEVEHICLEID']
    );
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeModel",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($cat),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
        ),
    ));
    $data['makeModels'] = json_decode(curl_exec($curl));
    curl_close($curl);
    return $data;
}

public function loadPdf(Request $request)
{
   
    $url = GeneralSetting::where('key', 'pdf_api_url')->first()->value;
   
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url . "/api/Reports/PreviewDocument?docid=" . $request->docid,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 10,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));
    $pdf = json_decode(curl_exec($curl));
    curl_close($curl);
    if ($pdf != null && $pdf->filePath != "Error") {
        $filename = 'NB-' . $request->proformano . '.pdf';
        $tempImage = tempnam(sys_get_temp_dir(), $filename);
        copy($url . $pdf->filePath, $tempImage);

        return response()->download($tempImage, $filename);
    }
    return redirect()->back()->withErrors('Cannot Download PDF at the Movement.');
}

}
