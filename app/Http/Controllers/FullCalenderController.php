<?php

namespace App\Http\Controllers;

use App\Models\Remark;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Meeting;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\AuthController;
use Auth;
use Session;

class FullCalenderController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Event::whereDate('start', '>=', $request->start)
                ->whereDate('end', '<=', $request->end)
                ->get(['id', 'title', 'start', 'end']);
            return response()->json($data);
        }
        return view('full-calender');
    }

    public function meeting(Request $request)
    {
        if (!Session::has('accessToken') && !Session::has('refreshToken')) {
            $calendar = new CalendarController();
            $calendar->CheckUserLogin();
        }
        if ($request->ajax()) {
            if (Auth::user()->role_id == 1 && Auth::user()->role_id == 2) {
                $data = Meeting::get(['id', 'title', 'start', 'end', 'eventAttendees', 'eventBody']);
            } else {
                $data = Meeting::where('user_id', Auth::user()->id)->get(['id', 'title', 'start', 'end', 'eventAttendees', 'eventBody']);
            }
            return response()->json($data);
        }
        $data = $this->loadViewData();
        return view('full-calender-meeting', $data);
    }


//    public function action(Request $request)
//    {
//    	if($request->ajax())
//    	{
//    		if($request->type == 'add')
//    		{
//    			$event = Event::create([
//    				'title'		=>	$request->title,
//    				'start'		=>	$request->start,
//    				'end'		=>	$request->end
//    			]);
//
//    			return response()->json($event);
//    		}
//
//    		if($request->type == 'update')
//    		{
//    			$event = Event::find($request->id)->update([
//    				'title'		=>	$request->title,
//    				'start'		=>	$request->start,
//    				'end'		=>	$request->end
//    			]);
//
//    			return response()->json($event);
//    		}
//
//    		if($request->type == 'delete')
//    		{
//    			$event = Event::find($request->id)->delete();
//
//    			return response()->json($event);
//    		}
//    	}
//	}

    public function actionmeet(Request $request)
    {
        $mailData = $this->loadViewData();
        $evId = '';

        if ($request->ajax()) {
            if ($request->type == 'add') {
                if (isset($mailData['userName']) && isset($mailData['userEmail'])) {
                    $calendar = new CalendarController();
                    $eventOp = $calendar->createNewEvent($request);
                    $evId = $eventOp['eventId'];
                }
                $meeting = Meeting::create([
                    'title' => $request->eventSubject,
                    'start' => $request->eventStart,
                    'end' => $request->eventEnd,
                    'outlook_event_id' => $evId,
                    'user_id' => Auth::user()->id,
                    'eventAttendees' => $request->eventAttendees,
                    'eventBody' => $request->eventBody,
                    'remark_id' => $request->remark_id,
                    'lead_id' => $request->lead_id
                ]);
                return response()->json([
                    'type' => 'success',
                    'meeting' => $meeting,
                    'message' => 'Calendar event created successfully.'
                ], 200);
            }

            if ($request->type == 'update') {
                $evId = Meeting::find($request->id)->outlook_event_id;
                if (isset($mailData['userName']) && isset($mailData['userEmail']) && $evId) {
                    $calendar = new CalendarController();
                    $eventOp = $calendar->updateEvent($request, $evId);
                    $evId = $eventOp['eventId'];
                }
                $meeting = Meeting::find($request->id)->update([
                    'title' => $request->eventSubject,
                    'start' => $request->eventStart,
                    'end' => $request->eventEnd,
                    'outlook_event_id' => $evId,
                    'user_id' => Auth::user()->id,
                    'eventAttendees' => $request->eventAttendees,
                    'eventBody' => $request->eventBody
                ]);
                $remarkId = Meeting::find($request->id)->remark_id;
                if ($remarkId) {
                    $date = Carbon::parse($request->eventStart);
                    Remark::where('id', $remarkId)->update([
                        'follow_up_date' => $date->format('Y-m-d'),
                        'follow_up_time' => $date->format('H:i'),
                        'remark' => $request->eventSubject
                    ]);
                }

//                return response()->json($meeting);
                return response()->json([
                    'type' => 'success',
                    'meeting' => $meeting,
                    'message' => 'Calendar event updated successfully.'
                ], 200);
            }

            if ($request->type == 'delete') {
                $meeting = Meeting::find($request->id);
                if (isset($mailData['userName']) && isset($mailData['userEmail']) && $meeting->outlook_event_id) {
                    $calendar = new CalendarController();
                    $eventOp = $calendar->deleteEvent($meeting->outlook_event_id);
                }
                $meeting->delete();

                return response()->json([
                    'type' => 'success',
                    'meeting' => $meeting,
                    'message' => 'Calendar event deleted successfully.'
                ], 200);
            }
        }
    }
}
