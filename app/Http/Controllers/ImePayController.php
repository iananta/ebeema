<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use App\Models\MotorCalculationData;
use App\Models\ImepayResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ImePayController extends Controller
{
    function random_strings($length_of_string)
    {
        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);
    }

    public function getPayment($reqData = null, $paymetDetails)
    {
//        dd($reqData);
        $curl = curl_init();

        $refnumber = Carbon::now()->format('dmy') . '-' . 'NB-' . $this->random_strings(5);
        $url = GeneralSetting::where('key', 'ime_pay_token')->first()->value;
        $module = $paymetDetails['module'];
        $imeUser = $paymetDetails['imeUser'];
        $merchantCode = $paymetDetails['merchantCode'];
        $data['merchantCode'] = $merchantCode;
        $data['merchantName'] = $paymetDetails['merchantName'];
        $pricePayable = $paymetDetails['pricePayable'];
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"MerchantCode\":\"$merchantCode\", \"Amount\":\"$pricePayable\", \"RefId\":\"$refnumber\" } ",
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Basic ' . $imeUser,
                'Module: ' . $module
            ],
        ));
        if (curl_exec($curl) === false) {
            echo 'Curl error: ' . curl_error($curl);
        } else {
            $response = curl_exec($curl);
//            dd($response);
            Session::put('travelDatas.reference_id',json_decode($response)->RefId);
//            dd($dt);
//            dd($response);
        }
        $data['paymentInfo'] = json_decode($response);
        curl_close($curl);
        $data['ime_pay_checkout_url'] = GeneralSetting::where('key', 'ime_pay_checkout')->first()->value;
        return $this->variable = $data;
    }

    public function imePaySuccess(Request $request)
    {
        $curl = curl_init();
        $url = GeneralSetting::where('key', 'ime_pay_confirm')->first()->values;
        $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
        $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
        $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
        $date = Carbon::now();
        $response = base64_decode($request->data);
        $pieces = explode("|", $response);
        $data['pieces'] = $pieces;
        $datas = array(
            "MerchantCode" => $data['merchantCode'],
            "RefId" => $pieces[4],
            "TokenId" => $pieces[6],
            "TransactionId" => $pieces[3],
            "Msisdn" => $pieces[2],
        );
//dd(json_encode($datas));
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Basic ' . $imeUser,
                'Module: ' . $module
            ],
        ));
        $response = curl_exec($curl);
//            dd($response);
        if (curl_exec($curl) === false) {
//            echo 'Payment Validation error ' . curl_error($curl);
        } else {
            $response = curl_exec($curl);
        }
        $data['paymentInfo'] = json_decode($response);
        curl_close($curl);
//        dd($data);
        try {
            if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
                ImepayResponse::create([
                    "MerchantCode" => "INSURANCE",
                    "ImeTxnStatus" => $pieces[0],
                    "ResponseDescription" => $pieces[1],
                    "Msisdn" => $pieces[2],
                    "TransactionId" => $pieces[3],
                    "RefId" => $pieces[4],
                    "TranAmount" => $pieces[5],
                    "TokenId" => $pieces[6],
                    "RequestDate" => $date,
                    "ResponseDate" => $date,
                ]);
            }
            $storeDta = [
                'payment_url' => json_encode($request->data) ?? '',
                'status' => 1,
            ];
//            if (Session::get('motorCalcId')) {
//                $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
//            }
        } catch (\Exception $e) {
            return $e->getMessage();
//            return redirect()->back();
        }
        $data['makePolicy'] = 1;
        return redirect()->route('travel.calculator.customer.create.policy', $data);
//        $this->makeOnlinePolicy($data);

    }

    public function imePayCancil(Request $request)
    {
        $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
        $date = Carbon::now();
        $response = base64_decode($request->data);
        $pieces = explode("|", $response);
        $data['pieces'] = $pieces;
//        dd($request);
        try {
            if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
                ImepayResponse::create([
                    "MerchantCode" => "INSURANCE",
                    "ImeTxnStatus" => $pieces[0],
                    "ResponseDescription" => $pieces[1],
                    "Msisdn" => $pieces[2],
                    "TransactionId" => $pieces[3],
                    "RefId" => $pieces[4],
                    "TranAmount" => $pieces[5],
                    "TokenId" => $pieces[6],
                    "RequestDate" => $date,
                    "ResponseDate" => $date,
                ]);
            }
            $storeDta = [
                'payment_url' => json_encode($request->data) ?? '',
                'status' => 2,
            ];
//            dd($storeDta);
//            if (Session::get('motorCalcId')) {
//                $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
//            }
        } catch (\Exception $e) {
            $e->getMessage();
//            return redirect()->back();
        }
//        dd($data);
        $data['data'] = "Payment Cancilled Error";
        return view('Backend.Travel.travel-policy', $data);
    }
}
