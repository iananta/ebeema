<?php

namespace App\Http\Controllers;
use App\Http\Controllers\FullCalenderController;
use App\Notifications\LeadRemarksCreated;
use App\Notifications\PremiumPaid;
use App\Notifications\PremiumUpdate;
use Illuminate\Http\Request;
use DataTables;
use App\Exports\LeadsExport;
use App\Imports\LeadsImport;
use App\Models\Lead;
use App\Models\City;
use App\Models\Leadsdocs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Remark;
use App\Models\User;
use App\Models\Planselected;
use App\Models\PremiumPaidByCustomer;
use App\DataTables\LeadsDataTable;
use App\DataTables\CustomerlistDataTable;
use App\DataTables\MarketingDataTable;
use Illuminate\Support\Facades\Notification;
use PDF;
use Carbon\Carbon;

class LeadController extends Controller
{
    public $selectedClass = null;
    public $selectedSection = null;
    public $sections = null;

    /**
     * Display a listing of the resource.
     *
     * @return leads list datatable
     */
    public function index(LeadsDataTable $dataTable)
    {
        return $dataTable->render('Backend.Leads.AllLeads.leads');
    }

    /**
     * @return customer list datatable
     */
    public function customersList(CustomerlistDatatable $dataTable)
    {
        return $dataTable->render('Backend.Leads.AllLeads.leads');

    }

    /**
     * @return marketing leads datatable
     */
    public function marketingLead(MarketingDataTable $dataTable){
       return $dataTable->render('Backend.Leads.AllLeads.leads');
    }


    /**
     * @return get city according to provision response
     */

    public function getCity(Request $request)
    {
        $cities=City::where('province_id',$request->id)->get(['id','city_name']);
        return response()->json(['cities'=>$cities],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//      return $request;
        $validatedData = $request->validate([
            'customer_name' => ['required'],
        ]);
        $data = $request->except('_token');
        if($data['dob']){
            $data['age']=Carbon::parse($request->dob)->age;
        }
        $lead = Lead::create($data);
        if ($lead) {
            if($lead->is_user == 1){
                $this->createPlanselected($lead);
            }
            return redirect()->back()->withSuccessMessage('Customer is added successfully.');
        }
        return redirect()->back()->withInput()->withWarningMessage('Customer can not be created.');
    }

    /**
     *get lead according to id
     *
     * @param \App\Models\Lead $lead
     * @return \Illuminate\Http\Response
     */
    public function getspecificLead(Request $request){
        $data['lead']=Lead::with('planSelecteds')->where('id',$request->id)->first();
        $data['from']=$request->from;
        $html=\View::make('Backend.Leads.AllLeads.includes.generate-edit-html')->with($data)->render();
        return response()->json(['html'=>$html,'lead'=>$data['lead']],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Lead $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $leadId=$request->id;
        $lead=Lead::with('city')->where('id',$leadId)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Lead $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = $request->except('_token','company_idArr','product_idArr','premiumArr','mopArr','issued_dateArr','policy_numberArr');
        $data['insurance_company_name']=$request->company_idArr[0] ?? '';
        $data['product_id']=$request->product_idArr[0] ?? '';
        $data['premium']=$request->premiumArr[0] ?? '';
        $data['mop']=$request->mopArr[0] ?? '';
        $data['age']=Carbon::parse($request->dob)->age;
        $data['updated_by']=auth()->user()->id;
        $lead = Lead::findOrfail($request->id);
        $leaId=$lead->id;
        $lead->update($data);

        if($lead){
            if($lead->is_user == 1 && isset($request->customer_update)){
                $planData=[
                    'issued_date'=>$request->issued_dateArr[0] ?? '',
                    'policy_number'=>$request->policy_numberArr[0] ?? ''
                ];
                $sel=$this->createPlanselected($lead,$planData);

            }
            if(isset($request->plan_idArr) && is_array($request->plan_idArr)){
                foreach($request->plan_idArr as $key=>$plan_id){
                    $planData[]=[
                        'id'=>$plan_id,
                        'category'=>$request->categoryArr[$key] ?? '',
                        'premium'=>$request->premiumArr[$key],
                        'company_id'=>$request->company_idArr[$key],
                        'product_id'=>$request->product_idArr[$key],
                        'term'=>$request->termArr[$key],
                        'mop'=>$request->mopArr[$key],
                        'policy_number'=>$request->policy_numberArr[$key],
                        'issued_date'=>$request->issued_dateArr[$key]
                    ];
                 }
                DB::table('planselected')->upsert($planData,'id');
                if(isset($request->add_premium_paid) && $request->add_premium_paid ==1){
                    
                    $nextDate=$this->getNextDate($request->paid_date_new,$lead->mop);
                    $premium = PremiumPaidByCustomer::create([
                        'planselected_id'=>$request->plan_idArr[0],
                        'premium_amount'=>$request->premium_amount_new,
                        'paid_date'=>$request->paid_date_new,
                        'next_paid_date'=>$nextDate,
                        'status'=>$request->status,
                        'user_id'=>auth()->user()->id,
                        'lead_id'=>$leaId
                    ]);
                    Notification::send(Auth::user(), new PremiumPaid($premium->load('lead', 'planselected')));
                }
            }
            return response()->json(['message'=>'Lead updated successfully'],200);
        }else{
            return response()->json(['error'=>'Something were wrong'],500);
        }
    }

    protected function getNextDate($date,$mop){
        if($mop == 'yearly'){
            $nextDate=date('Y-m-d', strtotime('+1 year', strtotime($date)) );
        }else if($mop== 'half_yearly'){
            $nextDate=date('Y-m-d', strtotime('+6 month', strtotime($date)) );
        }else if($mop == 'quarterly'){
            $nextDate=date('Y-m-d', strtotime('+4 month', strtotime($date)) );
        }else{
            $nextDate=date('Y-m-d', strtotime('+1 month', strtotime($date)) );
        }
        return $nextDate;
    }

    /**
     * update premium padi by customer
     */
    public function updatePremiumPaidByCustomer(Request $request){
        $data=$request->except('id','_token');
        $premium=PremiumPaidByCustomer::with('lead','planselected')->find($request->id);
        $data['next_paid_date']=$this->getNextDate($data['paid_date'],$premium->lead->mop);
        $premium->update($data);
//        dd($premium);
//       dd($updatePremium);
        Notification::send(Auth::user(), new PremiumUpdate($premium));
        return response()->json(['message'=>'Premium Paid by Customer Updated successfully'],200);
    }

    /**
     * create planselected
    */
    protected function createPlanselected($lead,$planData){
        $planData=[
            'name'=>$lead->customer_name ?? '',
            'phone' => $lead->phone ?? '',
            'email' => $lead->email ?? '',
            'age' =>$lead->age ?? '',
            'category'=> $lead->category ?? '',
            'sum_assured' => $lead->sum_insured ?? '',
            'term'=>$lead->maturity_period ?? '',
            'lead_id'=>$lead->id,
            'premium'=>$lead->premium ?? 0,
            'company_id'=>$lead->insurance_company_name ?? '' ,
            'product_id'=>$lead->product_id ?? '',
            'mop'=>$lead->mop ?? '',
            'issued_date'=>$planData['issued_date'] ?? '',
            'policy_number'=>$planData['policy_number'] ?? ''
        ];
        $plan=Planselected::where('lead_id',$lead->id)->exists();
        if(!$plan){
            Planselected::create($planData);
        }else{
            Planselected::where('lead_id',$lead->id)->update($data);
        }
        return $plan;

    }
    /**
     * @return convert to policy form
     */
    public function getFormConvertToPolicy(){
        $data['from']='lead';
        $form=\View::make('Backend.Leads.AllLeads.includes.convert-to-policy')->with($data)->render();
        return response()->json(['form'=>$form],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Lead $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flag = Lead::findOrfail($id);
        $flag->delete();
        if ($flag) {
            return response()->json([
                'type' => 'success',
                'message' => 'Lead is deleted successfully.'
            ], 200);
        }
        return response()->json([
            'type' => 'error',
            'message' => 'Lead can not be deleted.'
        ], 422);
    }

    public function show($id)
    {
        $leads = Lead::all();
        $lead = Lead::findOrfail($id);
        return view('Backend.Leads.Leads.show', compact('leads', 'lead'));

    }

    /**
     * @return add lead remark
     *
    **/
    public function addRemarkLead(Request $request){
        if($request->ajax()){
            $remark=$request->remark;
            $followStringDate=strtotime($request->follow_up_date);
            $followDate=date('Y-m-d',$followStringDate);
            $lead = Remark::create([
                'lead_id'=>$request->lead_id,
                'follow_up_date' =>$followDate,
                'follow_up_time' =>$request->follow_up_time,
                'remark'=>$remark
            ]);
            $request->merge([
                "eventSubject" => $request->remark,
                "lead_id" => $request->lead_id,
                "eventStart" => $followDate.' '.$request->follow_up_time,
                "eventEnd" => $followDate.' 23:59',
                "type" => "add",
                "remark_id" => $lead->id,
            ]);
            $calendar = new FullCalenderController();
            $calendar->actionmeet($request);
//            dd($request->all());
            //Notification::send(Auth::user(), new LeadRemarksCreated($lead));
            return response()->json(['message'=>'Successfully Created','lead'=>$lead],200);
        }
    }

    /**
     * @return  upload document
    */

    public function leadDocument(Request $request){
            $documents=$request->file('documents');
            if($request->filelength > 2){
                return response()->json(['message' => 'You cannot upload more than 2 document'],500);
            }
            if($documents && $request->filelength <=2){
                $destinationpath = 'uploads/leads/'.$request->lead;
                unset($documents[0]);
                unset($documents[3]);
                foreach($documents as $key=>$document){
                    $extension = strrchr($document->getClientOriginalName(), '.');
                    $new_file_name =\Str::random(15).'_'. time();
                    $docs = $document->move($destinationpath, $new_file_name . $extension);
                    $data['file'] = isset($docs) ? $new_file_name . $extension : null;
                    Leadsdocs::create([
                        'docs' => $data['file'],
                        'lead_id'=>$request->lead
                    ]);
                }
                return response()->json(['message'=>'Document uploaded successfully'],200);
            }else{
                return response()->json(['message'=>'Something were wrong'],500);
            }


    }

    /**
     * @return get product according to company
    */
    public function getProduct(Request $request){
        $data=$request->except('_token');
        $product=Product::where(['is_active'=>'1','company_id'=>$data['companyId']])->get(['id','name']);
        return response()->json(['product'=>$product],200);
    }

    /**
     * @return delete document
    */
    public function documentDelete(Request $request){
        $document=Leadsdocs::findOrFail($request->id);
        $file_path='uploads/leads/'.$document->lead_id.'/'.$document->docs;
        unlink($file_path);
        $document->delete();
        return response()->json(['message'=>'Document deleted successfully'],200);
    }

    public function importFile(Request $request) {
        $file = new LeadsImport([]);
        \Maatwebsite\Excel\Facades\Excel::import($file, request()->file('file'));
        return back();
    }

    public function findUser(Request $request){
        $users=User::where('role_id',$request->id)->get(['id','username']);
        return response()->json(['users'=>$users],200);
    }

    public function customerVerify(Request $request){
         $customer=Lead::where('id',$request->id)->update($request->except('_token','id'));
        if($customer){
            $message=$request->verify == 0 ? 'Customer changed to not verified' : 'Customer changed to verified';
        }
        return response()->json(['message'=>$message],200);
    }




}
