<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Lead;
use App\Models\LifeClaim;
use App\Models\Product;
use Illuminate\Http\Request;

class LifeClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['lifeClaim'] = LifeClaim::all();
        return view('Backend.LifeClaim.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['companies'] = Company::all();
        return view('Backend.LifeClaim.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hasFile('claim_image')) {
            $file = $request->file('claim_image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '_' . $extension;
            $file->move(public_path('images/claims'), $filename);
            $request->merge(['image' => $filename]);
        }
        LifeClaim::create($request->all());
        return redirect()->route('life.claim')->withSuccessMessage('Life Claim Added Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['companies'] = Company::all();
        $data['claim'] = LifeClaim::findOrFail($id);
        return view('Backend.LifeClaim.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_method', '_token', 'claim_image');
        if ($request->hasFile('claim_image')) {
            $file = $request->file('claim_image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '_' . $extension;
            $file->move(public_path('images/claims'), $filename);
            $data['image'] = $filename;
        }
        $claim = LifeClaim::whereId($id)->update($data);

        return redirect()->route('life.claim')->withSuccessMessage('Life claim is Updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pay = LifeClaim::findOrfail($id);
        $pay->delete();

        if ($pay) {
            return response()->json([
                'type' => 'success',
                'message' => 'Life Claim is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'Life Claim can not be deleted.'
        ], 422);
    }
}
