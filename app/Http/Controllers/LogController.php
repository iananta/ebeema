<?php

namespace App\Http\Controllers;

use App\Models\user_activity_logs;
use App\Models\UserActivityLog;
use DB;
use Auth;
use App\DataTables\UserlogDataTable;
use App\DataTables\UserNotificationDataTable;



class LogController extends Controller
{
    public function userlogs(UserlogDataTable $dataTable)
    {
        return $dataTable->render('Useractivity.index');
    }

    public function userlogNotification(UserActivityLog $activityLog)
    {
        $activityLogs = $activityLog->newQuery()
            ->join('users', 'users.id', 'user_activity_logs.user_id')
            ->where('user_activity_logs.RESPONSE', '<>', null)
            ->when(\Illuminate\Support\Facades\Auth::user()->designation !== 'Super Admin', function ($query) {
                $query->where('user_activity_logs.user_id', \Illuminate\Support\Facades\Auth::user()->id);
            })
            ->select(
                'user_id',
                'RESPONSE as response',
                'user_activity_logs.created_at as createdAt',
                'users.username'
            )
            ->orderByDesc('createdAt')
            ->limit(5)
            ->get();

        return response()->json([
            'data' => $activityLogs
        ]);
    }
    public function notificationList(UserActivityLog $activityLog,UserNotificationDataTable $dataTable){
        $activityLog = $activityLog->newQuery()
            ->join('users', 'users.id', 'user_activity_logs.user_id')
            ->where('user_activity_logs.RESPONSE', '<>', null)
            ->when(\Illuminate\Support\Facades\Auth::user()->designation !== 'Super Admin', function ($query) {
                $query->where('user_activity_logs.user_id', \Illuminate\Support\Facades\Auth::user()->id);
            })
            ->select(
                'user_id',
                'RESPONSE as response',
                'user_activity_logs.created_at as createdAt',
                'users.username'
            )
            ->get()->sortByDesc('createdAt');
//        dd($activityLog);
        return $dataTable->render('Useractivity.userNotificationList',compact('activityLog'));
    }
}
