<?php

namespace App\Http\Controllers;

use App\Models\MotorCalculationData;
use Illuminate\Http\Request;
use App\Models\MailTemplate;
use Exception;

class MailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['mail'] = MailTemplate::all();
        return view('emails.mailTemplate.index', $data);
    }

    public function changeStatus($id)
    {
        try {
            $mail = MailTemplate::findOrFail($id);
            $mail->status == '0' ? $mail->status = '1' : $mail->status = '0';
            $mail->save();
            $message = ($mail->status == '1') ? "Mail is activated successfully." : "Mail is deactivated successfully.";
            return response()->json([
                'status' => 'ok',
                'mail' => $mail,
                'message' => $message
            ], 200);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function resetMail($id){
        $mail = MailTemplate::findOrFail($id);
        $mail->body = $mail->default_body;
        $mail->save();
        return redirect()->back()->withSuccessMessage('The form was reset!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('emails.mailTemplate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->merge([
           "default_body" => $request->body
        ]);
//        dd($request->all());
        try {
            $mail = MailTemplate::create($request->all());

            return redirect()->route('admin.mail.index')->withSuccessMessage('Mail is added successfully.');
        }catch (Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mail = MailTemplate::findorFail($id);
        return view('emails.mailTemplate.edit', compact('mail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data['title'] = $request->title;
            $data['to'] = $request->to;
            $data['cc'] = $request->cc;
            $data['bcc'] = $request->bcc;
            $data['subject'] = $request->subject;
            $data['body'] = $request->body;
            $mail = MailTemplate::where('id', $id)->update($data);
            if ($mail) {
                return redirect()->route('admin.mail.index')->withSuccessMessage('Mail is Updated successfully.');
            }
            return redirect()->back()->withInput()->withWarningMessage('Mail can not be updated.');
        }catch (Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mail = MailTemplate::findOrFail($id);
            $mail->delete();
            return response()->json([
                'status' => 'ok',
                'message' => 'Mail is deleted successfully.'
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Mail can not be deleted.'
            ], 422);
        }
    }
}
