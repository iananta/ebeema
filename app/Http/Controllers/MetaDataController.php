<?php

namespace App\Http\Controllers;

use App\Models\MetaData;
use Exception;
use Illuminate\Http\Request;

class MetaDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['metas'] = MetaData::all();
        return view('metaData.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('metaData.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $mail = MetaData::create($request->all());
            return redirect()->route('meta.index')->withSuccessMessage('Meta Data is added successfully.');
        }catch (Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function changeStatus($id)
    {
        try {
            $meta = MetaData::findOrFail($id);
            $meta->status == '0' ? $meta->status = '1' : $meta->status = '0';
            $meta->save();
            $message = ($meta->status == '1') ? "Meta-data is activated successfully." : "Meta-data is deactivated successfully.";
            return response()->json([
                'status' => 'ok',
                'meta' => $meta,
                'message' => $message
            ], 200);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MetaData  $metaData
     * @return \Illuminate\Http\Response
     */
    public function show(MetaData $metaData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MetaData  $metaData
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = MetaData::findorFail($id);
        return view('metaData.edit', compact('meta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MetaData  $metaData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'meta_title' => 'required',
                'meta_description' => 'required',
            ]);

            $data['meta_title'] = $request->meta_title;
            $data['meta_description'] = $request->meta_description;

            $meta = MetaData::where('id', $id)->update($data);
            if ($meta) {
                return redirect()->route('meta.index')->withSuccessMessage('Meta-data is Updated successfully.');
            }
            return redirect()->back()->withInput()->withWarningMessage('Meta-data can not be updated.');
        }catch (Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MetaData  $metaData
     * @return \Illuminate\Http\Response
     */
    public function destroy(MetaData $metaData)
    {
        //
    }
}
