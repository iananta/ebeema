<?php

namespace App\Http\Controllers;

use App\Models\MotorManufactureCompany;
use Exception;
use Illuminate\Http\Request;

class MotorManufactureCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['manufactures'] = MotorManufactureCompany::all();
            return view('Backend.NonLife.Manufactures.index', $data);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong' . $e->getMessage());
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cat = MotorManufactureCompany::create($request->all());
            return redirect()->back()->withSuccessMessage('Manufacture Created Successfully');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\MotorManufactureCompany $motorManufactureCompany
     * @return \Illuminate\Http\Response
     */
    public function show(MotorManufactureCompany $motorManufactureCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\MotorManufactureCompany $motorManufactureCompany
     * @return \Illuminate\Http\Response
     */
    public function edit(MotorManufactureCompany $motorManufactureCompany)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\MotorManufactureCompany $motorManufactureCompany
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MotorManufactureCompany $motorManufactureCompany)
    {
        try {
            $asd = $motorManufactureCompany->update([
                "category" => $request->category,
                "name" => $request->name,
                "code" => $request->code,
            ]);
            if ($asd == true) {
                return redirect()->back()->withSuccessMessage('Manufacture Updated Successfully');
            } else {
                return redirect()->back()->withErrors('Something went wrong, Cannot Update');
            }
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\MotorManufactureCompany $motorManufactureCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $manufacture = MotorManufactureCompany::findorfail($id);
            $manufacture->delete();

            return response()->json([
                'type' => 'success',
                'message' => 'Manufacture deteled successfully.'
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'type' => 'error',
                'message' => 'Manufacture can not deleted.'
            ], 422);
        }
    }

    public function statusChange($id)
    {
        try {
            $manufacture = MotorManufactureCompany::find($id);
//            dd($manufacture);
            if ($manufacture->status == 1) {
                $manufacture->status = 0;
            } else {
                $manufacture->status = 1;
            }
            $manufacture->update();

            return response()->json([
                'type' => 'success',
                'manufacture' => $manufacture,
                'message' => 'Active status changed successfully.'
            ], 200);
        } catch (\Exception $e) {
//            return $e->getMessage();
            return response()->json([
                'type' => 'error',
                'message' => 'Status can not be changed.'
            ], 422);
        }
    }
}
