<?php

namespace App\Http\Controllers;

use App\Imports\ManufactureModelImport;
use App\Models\MotorManufactureCompanyModel;
use App\Models\MotorManufactureCompany;
use Auth;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MotorManufactureCompanyModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['mmc'] = MotorManufactureCompanyModel::with('getmmc')->get();
        $data['models'] = MotorManufactureCompanyModel::all();
        return view('Backend.NonLife.ManufactureModels.index',$data);
    }

    public function uploadExcel(Request $request)
    {          
        // $file = $request->file('file');
      
        // Excel::import(new ManufactureModelImport($request->manufacture_id), $file);   
        // Excel::import(new ManufactureModelImport,request()->file('file'));     
       
        // return redirect()->back()->withSuccessMessage('Document uploaded successfully.');
        try {
            if ($request->hasFile('excel_file')) {
                $rateFile = new ManufactureModelImport([
                    'manufacture_id' => $request->manufacture_id,
                ]);
                Excel::import($rateFile, request()->file('excel_file'));
            }
            return redirect()->back()->withSuccessMessage('Models Imported Successfully!');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.                                  
     *
     * @param \App\Models\MotorManufactureCompanyModel $motorManufactureCompanyModel     
     * @return \Illuminate\Http\Response  
     */
    public function destroy(MotorManufactureCompanyModel $motorManufactureCompanyModel)
    {
        try {
            $model = MotorManufactureCompanyModel::findorfail($id);
            $model->delete();
            return response()->json([
                'type' => 'success',
                'message' => 'Model deteled successfully.'
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'type' => 'error',
                'message' => 'Model can not deleted.'
            ], 422);
        }
    }

    public function statusChange($id)
    {
        try {
            $model = MotorManufactureCompanyModel::find($id);
//            dd($model);
            if ($model->status == 1) {
                $model->status = 0;
            } else {
                $model->status = 1;
            }
            $model->update();

            return response()->json([
                'type' => 'success',
                'manufacture' => $model,
                'message' => 'Active status changed successfully.'
            ], 200);
        } catch (\Exception $e) {
//            return $e->getMessage();
            return response()->json([
                'type' => 'error',
                'message' => 'Status can not be changed.'
            ], 422);
        }
    }
}
