<?php

namespace App\Http\Controllers;

use App\Http\Controllers\EsewaPaymetController;
use App\Models\MailTemplate;
use App\Models\SMSTemplate;
use App\Models\TravelMedicalPolicy;
use App\Notifications\PaymentSuccess;
use App\Notifications\PaymentFailed;
use DB;
use App\Mail\NonLifePolicy;
use App\Exports\PolicyExport;
use App\Models\CustomerPolicy;
use App\Models\GeneralSetting;
use App\Models\ImepayResponse;
use App\Models\KYC;
use App\Models\SmsResponse;
use App\Models\Lead;
use App\Models\MotorCalculationData;
use App\Http\Controllers\EsewaPaymetController;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Auth;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Storage;
use \PDF;
use Redirect;
use Session;
use CURLFILE;
use function GuzzleHttp\Promise\all;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Log;
use Illuminate\Support\Facades\Crypt;
use App\DataTables\CustomerPolicyDataTable;
use App\DataTables\DraftPolicyDataTable;

class NonLifeCalculatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function customerAjaxData()
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetKYCCategory",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $kycCat = json_decode(curl_exec($curl));
        $data['kycCategories'] = $kycCat->data ?? array();


        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetInsuredType",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $insuredTypes = json_decode(curl_exec($curl));
        $data['insuredTypes'] = $insuredTypes->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/getkycclassification",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $kycClassifications = json_decode(curl_exec($curl));
        $data['kycClassifications'] = $kycClassifications->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetKYCRiskCategory",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $riskCategories = json_decode(curl_exec($curl));
        $data['riskCategories'] = $riskCategories->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/Area/GetProvince",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $provinces = json_decode(curl_exec($curl));
        $data['provinces'] = $provinces->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetKYCOccupation",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $kycOccupations = json_decode(curl_exec($curl));
        $data['kycOccupations'] = $kycOccupations->data ?? array();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/KYC/GetIncomeSource",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $kycincomesources = json_decode(curl_exec($curl));
        $data['kycincomesources'] = $kycincomesources->data ?? array();

        curl_close($curl);
//        dd($data);
        return $this->variable = $data;
    }

    public function CalculatorData($dta)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;

//        $requestContent = [
//            'headers' => [
//                'Accept' => 'application/json',
//                'Content-Type' => 'application/json'
//            ],
//            'json' => [
//                'NAMEOFVEHICLE'=>'',
//                 'debug' => true
//            ]
//        ];
//
//        try {
//            $client = new GuzzleHttpClient();
//
//            $apiRequest = $client->request('POST', $url . "/Api/MotorSetup/GetTypeofCoverList", $requestContent);
//
//            $response = json_decode($apiRequest->getBody());
//
//            dd($response);
//        } catch (\Exception $e) {
//            $e->getMessage();
//            // For handling exception.
//        }

        $datas = array(
            "ClassId" => $dta,
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetExcessOwnDamage",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                                      "TYPECOVER":"",
                                        "VEHICLETYPE":"",
                                        "CATEGORYID":"",
                                        "CLASSCODE":""
                                    }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['excessdamages'] = json_decode(curl_exec($curl));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetCategoryList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['categories'] = json_decode(curl_exec($curl));
//dd($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeyearList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                                       "NAMEOFVEHICLE":"20"
                                    }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['makeYearLists'] = json_decode(curl_exec($curl));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetTypeofCoverList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                                       "NAMEOFVEHICLE":""
                                    }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['typeCovers'] = json_decode(curl_exec($curl));

        curl_close($curl);
//        dd($data);
        $data['classId'] = $dta ?? '';
        return $this->variable = $data;
    }

    public function getCatType(Request $request)
    {
//        return $request;
        $url = GeneralSetting::where('key', 'api_url')->first()->value;

        $data = array(
            "CATEGORYID" => $request->CATEGORYID,
        );
//        dd($data);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetClassCategory",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $CategoryType = json_decode(curl_exec($curl));
        curl_close($curl);
//        dd($CategoryType);
        $CategoryTypeData = $CategoryType->data[0] ?? $output->Message;

        if ($CategoryType) {
            return response()->json([
                'type' => 'success',
                'CategoryType' => $CategoryTypeData,
                'message' => 'Calculated successfully.'
            ], 200);
        } else {
            return response()->json([
                'type' => 'error',
                'CategoryType' => '',
                'message' => 'Calculation error.'
            ], 500);
        }
//dd($data);
    }


    public function claimByKyc(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Reports/GetKYCClaimList';
        $apiUser = "Basic " . base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);
//dd($url,$apiUser);
        $data = array(
            "KYCID" => 297184,
        );
        $client = new Client();
        $response = $client->request('POST',
            $url,
            [
                'headers' => [
                    'Authorization' => $apiUser
                ],
                'form_params' => [
                    'KYCID' => 297184
                ]
            ]);

        $claimData = json_decode($response->getBody()->getContents(), true);
        $data = $claimData['data'] ?? [];
//dd($data);
        return view('Backend.NonLife.claim.claim-kyc', ['data' => $data]);
    }

    public function claimStatus(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Reports/GetClaimStatus';
        $apiUser = "Basic " . base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $claimNo = $request->query('claim_number', null);

        if (!$claimNo) {
            throw new \Exception('Claim number is required.');
        }
        $client = new Client();
        $response = $client->request('POST',
            $url,
            [
                'headers' => [
                    'Authorization' => $apiUser
                ],
                'form_params' => [
                    'CLAIMNO' => $claimNo
                ]
            ]);

        $claimData = json_decode($response->getBody()->getContents(), true);
        $data = $claimData['data'] ?? [];
//dd($data);
        return view('Backend.NonLife.claim.claim-status', ['data' => $data]);
    }

    public function claimAssessmentSummary(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Reports/GetClaimAssessmentSummary';
        $apiUser = "Basic " . base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $claimNo = $request->query('claim_number', null);

        if (!$claimNo) {
            throw new \Exception('Claim number is required.');
        }
        $client = new Client();
        $response = $client->request('POST',
            $url,
            [
                'headers' => [
                    'Authorization' => $apiUser
                ],
                'form_params' => [
                    'CLAIMNO' => $claimNo
                ]
            ]);
        $claimData = json_decode($response->getBody()->getContents(), true);
//        dd($claimData);
        $data = $claimData['data'] ?? [];
//        dd($data);
        return view('Backend.NonLife.claim.claim-assessment', ['data' => $data]);
    }

    public function claimSurveyorData(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Reports/GetClaimSurveyorData';
        $apiUser = "Basic " . base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $claimNo = $request->query('claim_number', null);

        if (!$claimNo) {
            throw new \Exception('Claim number is required.');
        }
        $client = new Client();
        $response = $client->request('POST',
            $url,
            [
                'headers' => [
                    'Authorization' => $apiUser
                ],
                'form_params' => [
                    'CLAIMNO' => $claimNo
                ]
            ]);
        $claimData = json_decode($response->getBody()->getContents(), true);
//      dd($claimData);
        $data = $claimData['data'] ?? [];
//   dd($data);
        return view('Backend.NonLife.claim.claim-survey', ['data' => $data]);
    }

    public function intimateClaim(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Claim/IntimateClaim';

        $client = new Client();

        $response = $client->request('POST',
            $url,
            [
                'form_params' => [
                    'ClaimDetails' => '{
                                        "USERID":"46",
                                        "NAME":"test",
                                        "POLICYNO":"01234567",
                                        "CONTACTNO":"9849960530",
                                        "VEHICLENO":"BA 1 KHA 4052",
                                        "PROVINCEID":"1",
                                        "DISTRICTID":"1",
                                        "MNUVDCID":"1",
                                        "WARDNO":"10",
                                        "ADDRESS":"Hattisar",
                                        "REMARK":"testing",
                                        "DateOfLoss":"2021-01-01"
                                        }',
                    'file' => new CURLFILE('/C:/Users/Puku/Pictures/Screenshots/3D.png')
                ]
            ]);
        $claimData = json_decode($response->getBody()->getContents(), true);
        dd($claimData);
        $data = $claimData['data'] ?? [];
//    dd($data);
        return view('Backend.NonLife.claim.intimate-claim', ['data' => $data]);
    }

    public function claimIntimationList(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Claim/GetClaimIntimationList';
        $apiUser = "Basic " . base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $claimNo = $request->query('claim_number', null);

        if (!$claimNo) {
            throw new \Exception('Claim number is required.');
        }
        $client = new Client();
        $response = $client->request('POST',
            $url,
            [
                'headers' => [
                    'Authorization' => $apiUser
                ],
                'form_params' => [
                    'USERID' => "46"
                ]
            ]);
        $claimData = json_decode($response->getBody()->getContents(), true);
//     dd($claimData);
        $data = $claimData['data'] ?? [];
// dd($data);
        return view('Backend.NonLife.claim.intimate-list', ['data' => $data]);
    }

    public function claimDocumentData(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value . '/Api/Reports/GetClaimDocumentData';
        $apiUser = "Basic " . base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $claimNo = $request->query('claim_number', null);

        if (!$claimNo) {
            throw new \Exception('Claim number is required.');
        }
        $client = new Client();
        $response = $client->request('POST',
            $url,
            [
                'headers' => [
                    'Authorization' => $apiUser
                ],
                'form_params' => [
                    'CLAIMNO' => $claimNo
                ]
            ]);
        $claimData = json_decode($response->getBody()->getContents(), true);
//     dd($claimData);
        $data = $claimData['data'] ?? [];
// dd($data);
        return view('Backend.NonLife.claim.claim-document', ['data' => $data]);
    }

    public function foreignCurrencyExchange(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $client = new Client();
        $response = $client->request('POST',
            $url . '/API/NRB/GetForeignExchangeRates',
            [
                'headers' => [
                    'Authorization' => "Basic TE1UUkFESU5HOmxtdHJhZGluZyFAIzEyMygp"
                ],
                'form_params' => [
                    'CountryCode' => "USD"
                ]
            ]);
        $claimData = json_decode($response->getBody()->getContents(), true);
//      dd($claimData);
        $data = $claimData['data'] ?? [];
//    dd($data);
        return view('Backend.NonLife.claim.currencyExchange', ['data' => $data]);
    }

    public function motorDocList(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
//        $claimNo = $request->query('claim_number', null);
//
//        if (!$claimNo) {
//            throw new \Exception('Claim number is required.');
//        }
        $client = new Client();
        $response = $client->request('POST',
            'http://203.78.160.19:9198/Api/Motor/GetMotorDocumentList',
            [
                'headers' => [
                    'Authorization' => "Basic TE1UUkFESU5HOmxtdHJhZGluZyFAIzEyMygp"
                ],
                'form_params' => [
                    'PROVINCECODE' => "RA",
                    'VEHICLELOTNO' => "1",
                    'VEHICLERUNNINGNO' => "6614",
                    'POLICYNO' => "BTM/MC-TP/F/00172/077/078"
                ]
            ]);
        $motorData = json_decode($response->getBody()->getContents(), true);
//    dd($motorData);
        $data = $motorData['data'] ?? [];
// dd($data);
        return view('Backend.NonLife.Renewal.motorList', ['data' => $data]);
    }

    public function motorInfo(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
//        $documentNo = $request->query('Documentno', null);
//
//        if (!$documentNo) {
//            throw new \Exception('Document number is required.');
//        }
        $client = new Client();
        $response = $client->request('POST',
            'http://203.78.160.19:9198/Api/Motor/GetMotorInfo',
            [
                'headers' => [
                    'Authorization' => "Basic TE1UUkFESU5HOmxtdHJhZGluZyFAIzEyMygp"
                ],
                'form_params' => [
                    'DOCUMENTNO' => "BTM/MC-TP/R/00047/078/079"
                ]
            ]);
        $motorData = json_decode($response->getBody()->getContents(), true);
//     dd($motorData);
        $data = $motorData['data'] ?? [];
//    dd($data);
        return view('Backend.NonLife.Renewal.motorInfo', ['data' => $data]);
    }

    public function proformaDetails(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $client = new Client();
        $response = $client->request('GET',
            'http://203.78.160.19:9198/Api/GetProforma/GetProforma/677406',
            [
                'headers' => [
                    'Authorization' => "Basic TE1UUkFESU5HOmxtdHJhZGluZyFAIzEyMygp"
                ]
            ]);
        $proformaData = json_decode($response->getBody()->getContents(), true);
// dd($proformaData['properties']);
        $data = $proformaData['properties'] ?? [];
        dd($data);
        return view('Backend.NonLife.Proforma.proformaDetail', ['data' => $data]);

    }

    public function makePayment(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $client = new Client();
        $response = $client->request('POST',
            'http://203.78.160.19:9198/Api/GetProforma/MakePayment',
            [
                'headers' => [
                    'Authorization' => "Basic TE1UUkFESU5HOmxtdHJhZGluZyFAIzEyMygp"
                ],
                'form_params' => [
                    'request_id' => "677406",
                    'amount' => "100.00",
                    'transaction_code' => "Proforma is valid",
                ]
            ]);
        $proformaData = json_decode($response->getBody()->getContents(), true);
//     dd($proformaData);
        $data = $proformaData['data'] ?? [];
//        dd($data);
        return view('Backend.NonLife.Proforma.proformaPayment', ['data' => $data]);
    }

    public function policyByKYC(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $client = new Client();

        $response = $client->request('POST',
            sprintf('%s/API/Reports/GetPolicyListByKYC', config('app.external_url')),
            [
                'headers' => [
                    'Authorization' => "Basic TE1UUkFESU5HOmxtdHJhZGluZyFAIzEyMygp"
                ],
                'json' => [
                    'KYCNO' => "IGI/01/076/077/101000169838"
                ]
            ]);
//       dd($response);
        $policyData = json_decode($response->getBody()->getContents(), true);
//    dd($policyData);
        $data = $policyData['data'] ?? [];
//dd($data);
        return view('Backend.NonLife.claim.policy-kyc', ['data' => $data]);
    }

    public function calculationMotor(Request $request)
    {
        try {
            $year = date("Y");
            if ($year == $request->YEARMANUFACTURE && $request->NCDYR == 1) {
                return redirect()->back()->withInput()->withErrors('No Claim Discount Cannot be taken for this year. Please select other year.');
            }
//        return $request;
            $data['stamp'] = 20;
            if ($request->EXPUTILITIESAMT < 100000) {
                $data['stamp'] = 10;
            }
            if ($request->CLASSID == 21) {
                Session::put('calculationFor', 'Bike');
            } elseif ($request->CLASSID == 22) {
                Session::put('calculationFor', 'Commercial Vehicle');
            } elseif ($request->CLASSID == 23) {
                Session::put('calculationFor', 'Private Vehicle');
            }
//        return $request;
//return (Session::get('calculationFor'));
            $url = GeneralSetting::where('key', 'api_url')->first()->value;
            $datas = array(
                "EXPUTILITIESAMT" => $request->EXPUTILITIESAMT ?? '0',
                "CATEGORYID" => $request->CATEGORYID,
                "CARRYCAPACITY" => $request->CARRYCAPACITY ?? '0',
                "YEARMANUFACTURE" => $request->YEARMANUFACTURE,
                "CCHP" => $request->CCHP,
                "EODAMT" => $request->EODAMT ?? '0',
                "PADRIVER" => $request->PADRIVER ?? '0',
                "PAPASSENGER" => $request->PAPASSENGER ?? '0',
                "PACONDUCTOR" => $request->PACONDUCTOR ?? '0',
                "NOOFEMPLOYEE" => $request->NOOFEMPLOYEE ?? '0',
                "NCDYR" => $request->NCDYR ?? '0',
                "NOOFPASSENGER" => $request->NOOFPASSENGER ?? '0',
                "PRIVATE_USE" => $request->PRIVATE_USE ?? '0',
                "INCLUDE_TOWING" => $request->INCLUDE_TOWING ?? '0',
                "ISGOVERNMENT" => $request->ISGOVERNMENT ?? '0',
                "TYPECOVER" => $request->TYPECOVER,
                "CLASSID" => $request->CLASSID,
                "HASAGENT" => $request->HASAGENT ?? '0',
                "EXCLUDE_POOL" => $request->EXCLUDE_POOL ?? '0',
                "compulsaryexcessamount" => $request->compulsaryexcessamount ?? '0',
                "Driver" => $request->Driver ?? '0',
                "PASSCAPACITY" => $request->PASSCAPACITY ?? '0',
            );
//            return $datas;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/Api/Calculator/CalculateMotorPremium",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($datas),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $data['output'] = json_decode(curl_exec($curl));

            $data['PremDetails'] = $data['output']->PremDetails ?? array();

            curl_close($curl);
            $data['requested_data'] = [$datas];
            $datas['user_id'] = Auth::user()->id;
            Session::forget('reqCalData');
            Session::put('reqCalData', $datas);
//            return (Session::get('reqCalData'));

//            dd($data);
            if ($data['output']) {
                $curl = curl_init();
                $makeVehicle = array(
                    "CLASSID" => $request->CLASSID,
                );
//            dd($makeVehicle);
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeVehicleList",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($makeVehicle),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $data['manufacturers'] = json_decode(curl_exec($curl));
//dd($data);
                $cat = array(
                    "CLASSID" => $request->CLASSID,
                );
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "/Api/MotorSetup/GetVehicleNameList",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($cat),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $data['vehicleNames'] = json_decode(curl_exec($curl));


                $cat = array(
                    "NAMEOFVEHICLE" => $request->CLASSID,
                );
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "/Api/MotorSetup/GetCategoryList",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($cat),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $data['categoryLists'] = json_decode(curl_exec($curl));


                // For Type Of Cover

                $typc = array(
                    "NAMEOFVEHICLE" => $request->TYPECOVER,
                );
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "/Api/MotorSetup/GetTypeofCoverList",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($typc),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $data['typeCovers'] = json_decode(curl_exec($curl));

                //Occupation List
                $typc = array(
                    "NAMEOFVEHICLE" => '',
                );
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "/Api/MotorSetup/GetOccupationList",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($typc),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));

                $data['occupationLists'] = json_decode(curl_exec($curl));

                curl_close($curl);

                return view('Backend.NonLife.bike.result', $data);
            } else {
                return redirect()->back()->withInput()->withErrors('Something went wrong. Please check your input values.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors('Something went wrong.' . $e->getMessage());
//            return $e->getMessage();
        }
    }

    function getModel(Request $request)
    {
        $datas = array(
            "MAKEVEHICLEID" => $request->mfComp,
        );
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeModel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $output = json_decode(curl_exec($curl));
        $data['output'] = $output->data ?? $output->Message;

        curl_close($curl);
        return $data;
    }

    function random_strings($length_of_string)
    {
        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);
    }

    function random_number($length_of_string)
    {
        // String of all alphanumeric character
        $str_result = '0123456789';

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);
    }

    function getMotorDetails($reqData)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;

        $curl = curl_init();
        $makeVehicle = array(
            "CLASSID" => $reqData['CLASSID'],
        );
//            dd($makeVehicle);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeVehicleList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($makeVehicle),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['manufacturers'] = json_decode(curl_exec($curl));
//dd($data);
        $cat = array(
            "CLASSID" => $reqData['CLASSID'],
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetVehicleNameList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($cat),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['vehicleNames'] = json_decode(curl_exec($curl));

        //Occupation List
        $typc = array(
            "NAMEOFVEHICLE" => '',
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetOccupationList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($typc),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['occupationLists'] = json_decode(curl_exec($curl));


        $cat = array(
            "NAMEOFVEHICLE" => $reqData['CLASSID'],
            "MAKEVEHICLEID" => $reqData['MAKEVEHICLEID']
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetMakeModel",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($cat),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $data['makeModels'] = json_decode(curl_exec($curl));

        curl_close($curl);
        return $data;
    }

//select customer
    public function selectCustomer(Request $request)
    {
        try {
//        return $request;
            $this->customerAjaxData();
            $data = $this->variable;
//        return $request;

            if ($request->file('bluebook_images')) {
                $path = $request->bluebook_images->store('motorBluebook', 'public');
                $imgFile = Storage::disk('public')->url($path);
                $request->merge(['bluebook_image' => $imgFile]);
            }
            if ($request->file('bike_images')) {
                $path1 = $request->bike_images->store('motorBike', 'public');
                $imgFile1 = Storage::disk('public')->url($path1);
                $request->merge(['bike_image' => $imgFile1]);
            }
            $request->merge(Session::get('reqCalData'));
//        dd($request);
            $calcData = '';
            if (!Session::get('motorCalcId')) {
                $calcData = MotorCalculationData::create($request->all());
                Session::forget('motorCalcId');
                Session::put('motorCalcId', $calcData->id);
            } else {
                $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($request->except('_token'));
            }
//            dd($calcData);
            $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['CLASSID', 'customer_id', 'created_at', 'MODEL', 'MAKEMODELID', 'VEHICLENAMEID', 'bluebook_image', 'bike_image', 'MAKEVEHICLEID', 'TYPECOVER', 'YEARMANUFACTURE', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id']);
            $data['makePolicy'] = '1';
            $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
            $detas = $this->getMotorDetails($data['formData']);

//        dd($data, $detas);
            return view('Backend.NonLife.bike.selectCustomer', $data, $detas);
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
//            return view('Backend.NonLife.bike.selectCustomer', $data);
        }
    }

    public function motorPayment(Request $request)
    {
//        return $request;
        try {
            $kyc = KYC::where('customer_id', $request->customer_id)->first();
//            dd($kyc);
//        Session::put('KycId', $kyc->KYCID);
            if ($kyc) {
                $curl = curl_init();
                do {
                    $newRefID = 'IGI-' . $this->random_strings(5) . '-' . Carbon::now()->format('dmy');
//                    dd($newRefID);
                } while (MotorCalculationData::where("payment_ref_id", "=", $newRefID)->first());

                $url = GeneralSetting::where('key', 'ime_pay_token')->first()->value;
                $testPrice = GeneralSetting::where('key', 'policy_test_price')->first()->value ?? '0';
                $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
                $data['showEsewa'] = GeneralSetting::where('key', 'esewa_payment')->first()->value ?? "0";
                $data['showConnectIps'] = GeneralSetting::where('key', 'connectips_payment')->first()->value ?? "0";
                $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
                $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
                $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
                $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
                $pricePayable = 0;
                if (Session::get('motorCalcId')) {
                    $mtData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
                    $pricePayable = $mtData->TOTALNETPREMIUM;
                    if ($testPrice == 1) {
                        $pricePayable = floatval(10);
                    }
                    if (isset($mtData)) {
                        if ($request->regenerate) {
                            $refnumber = $newRefID;
                        } else {
                            $refnumber = $mtData->payment_ref_id ?? $newRefID;
                        }
                    }
                } else {
                    $mtData = [];
                    $refnumber = '';
                }

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "{ \"MerchantCode\":\"$merchantCode\", \"Amount\":\"$pricePayable\", \"RefId\":\"$refnumber\" } ",
                    CURLOPT_HTTPHEADER => [
                        'Content-Type: application/json',
                        'Authorization: Basic ' . $imeUser,
                        'Module: ' . $module
                    ],
                ));
                if (curl_exec($curl) === false) {
                    echo 'Curl error: ' . curl_error($curl);
                } else {
                    $response = curl_exec($curl);
                }
                $data['paymentInfo'] = json_decode($response);
                curl_close($curl);
                $data['ime_pay_checkout_url'] = GeneralSetting::where('key', 'ime_pay_checkout')->first()->value;
                $calcData = '';
//            dd($request);
                $storeDta = [
                    'customer_id' => $request->customer_id,
                    'payment_ref_id' => $data['paymentInfo']->RefId ?? '',
                    'payment_token_details' => json_encode($response) ?? '',
                ];
                if (Session::get('motorCalcId') && $data['paymentInfo']->ResponseCode == 0) {
                    $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
                }
                $data['policy'] = $mtData;
                $esewa = new EsewaPaymetController;
                $data['esewa'] = $esewa->getCheckout();

                if ($data['showConnectIps'] == 1) {
                    $data['connectips'] = $this->getConnectipsData([
                        'amount' => $pricePayable,
                        'referenceId' => $refnumber
                    ]);
                }

                if ($request->regenerate == 1) {
                    return response()->json([
                        'type' => 'success',
                        'message' => 'Reference Id generated Successfully'
                    ], 200);
                }
                return view('Backend.NonLife.bike.payment', $data);
            } else {
                return response()->json([
                    'type' => 'error',
                    'message' => 'Kyc not Added ! Please Add the Kyc for the customer.'
                ], 200);
            }
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
//            return $e->getMessage();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    private function getConnectipsData(array $details)
    {
        $merchantId = GeneralSetting::where('key', 'connectips_merchant_id')->first()->value;
        $appId = GeneralSetting::where('key', 'connectips_app_id')->first()->value;
        $appName = GeneralSetting::where('key', 'connectips_app_name')->first()->value;
        $txnId = $this->random_number(5);
        $txnDate = Carbon::now()->toDateString();
        $txnCurrency = 'NPR';
        $txnAmount = $details['amount'] * GeneralSetting::where('key', 'hundreds')->first()->value;
        $referenceId = $details['referenceId'];
        $remarks = '';
        $particulars = '';

        $certificate = \Illuminate\Support\Facades\Storage::get('/public/certificates/connectips.pfx');

        $tokenMessage = "MERCHANTID=$merchantId,APPID=$appId,APPNAME=$appName,TXNID=$txnId,TXNDATE=$txnDate,TXNCRNCY=$txnCurrency,TXNAMT=$txnAmount,REFERENCEID=$referenceId,REMARKS=$remarks,PARTICULARS=$particulars,TOKEN=TOKEN";
//        dd($tokenMessage);

        if (openssl_pkcs12_read($certificate, $cert_info, '123')) {
            $private_key = openssl_pkey_get_private($cert_info['pkey']);

        } else {
            throw new \Exception('Unable to read certificate.');
        }

        if (openssl_sign($tokenMessage, $signature, $private_key, 'sha256WithRSAEncryption')) {
            $hash = base64_encode($signature);
            openssl_free_key($private_key);
        } else {
            throw new \Exception('Unable to sign certificate.');
        }

        return [
            'merchantId' => $merchantId,
            'appId' => $appId,
            'appName' => $appName,
            'txnId' => $txnId,
            'txnDate' => $txnDate,
            'txnCurrency' => $txnCurrency,
            'txnAmount' => $txnAmount,
            'referenceId' => $referenceId,
            'remarks' => $remarks,
            'particulars' => $particulars,
            'token' => $hash
        ];
    }

    public function compulsaryExcess(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
//        return $request;
        $datas = array(
            "TYPECOVER" => $request->TYPECOVER,
            "CATEGORYID" => $request->CATEGORYID,
            "YEARMANUFACTURE" => $request->YEARMANUFACTURE,
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetCompulsoryExcess",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $excessCal = json_decode(curl_exec($curl));
//        dd($excessCal);
        curl_close($curl);
        $excessVal = $excessCal->data ? $excessCal->data[0]->CompulsoryExcess : '0';
        if ($excessVal >= 0) {
            return response()->json([
                'type' => 'success',
                'CompulsoryExcess' => $excessVal,
                'message' => 'Calculated successfully.'
            ], 200);
        } else {
            return response()->json([
                'type' => 'success',
                'CompulsoryExcess' => $excessVal,
                'message' => 'Calculation error.'
            ], 200);
        }
    }

    public function excessDamage(Request $request)
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
//        return $request;
        $datas = array(
            "TYPECOVER" => $request->TYPECOVER,
            "CATEGORYID" => $request->CATEGORYID,
            "CLASSCODE" => $request->CLASSCODE,
            "VEHICLETYPE" => $request->VEHICLETYPE
        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/MotorSetup/GetExcessOwnDamage",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $excessdamages = json_decode(curl_exec($curl));

        curl_close($curl);

        $excessdamages = $excessdamages->data ? $excessdamages->data : '';
//        dd($excessdamages);
        if ($excessdamages) {
            return response()->json([
                'type' => 'success',
                'excessdamages' => $excessdamages,
                'message' => 'Calculated successfully.'
            ], 200);
        } else {
            return response()->json([
                'type' => 'success',
                'excessdamages' => $excessdamages,
                'message' => 'Calculation error.'
            ], 200);
        }
    }

    public function imePaySuccess(Request $request)
    {
        try {
            $curl = curl_init();
            $url = GeneralSetting::where('key', 'ime_pay_confirm')->first()->value;
            $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
            $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
            $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
            $date = Carbon::now();
            $response = base64_decode($request->data);
            $pieces = explode("|", $response);
            $data['pieces'] = $pieces;
            $datas = array(
                "MerchantCode" => $data['merchantCode'],
                "RefId" => $pieces[4],
                "TokenId" => $pieces[6],
                "TransactionId" => $pieces[3],
                "Msisdn" => $pieces[2],
            );
//dd(json_encode($datas));
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($datas),
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $imeUser,
                    'Module: ' . $module
                ],
            ));
            $response = curl_exec($curl);
//            dd($response);
            curl_close($curl);
            if ($response === false) {
                $data['data'] = "Payment Validation Error";
                return view('Backend.NonLife.bike.policy', $data);
                echo 'Payment Validation error ' . $response;
            } else {
                $data['paymentInfo'] = json_decode($response);
                if ($data['paymentInfo']->ResponseCode == 0) {
//            dd($data);
                    if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
                        ImepayResponse::create([
                            "MerchantCode" => "INSURANCE",
                            "ImeTxnStatus" => $pieces[0],
                            "ResponseDescription" => $pieces[1],
                            "Msisdn" => $pieces[2],
                            "TransactionId" => $pieces[3],
                            "RefId" => $pieces[4],
                            "TranAmount" => $pieces[5],
                            "TokenId" => $pieces[6],
                            "RequestDate" => $date,
                            "ResponseDate" => $date,
                        ]);
                    }
                    $storeDta = [
                        'payment_method' => "imepay",
                        'payment_url' => json_encode($request->data) ?? '',
                        'status' => 1,
                    ];
                    if (Session::get('motorCalcId')) {
                        $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
                    }
//                $data['customers'] = Lead::where('is_user', 1)->get();
                    $data['makePolicy'] = 1;
                    $data['paymentMethod'] = 'imepay';
                    $data['referenceId'] = $pieces[4] ?? "N/A";
                    $data['payableAmount'] = $pieces[5] ?? 'N/A';
                    Notification::send(Auth::user(), new PaymentSuccess($data));
                    return redirect()->route('nonLife.calculator.motor.make.policy', $data);
                } else {
                    $data['data'] = $data['paymentInfo']->ResponseDescription ?? "Something went wrong";
                    return view('Backend.NonLife.bike.policy', $data);
                }
            }
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
//            return $e->getMessage();
//            return redirect()->back();
        }
    }

    public function imePayCancil(Request $request)
    {
//        $data['customers'] = Lead::where('is_user', 1)->get();
        $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
        $date = Carbon::now();
        $response = base64_decode($request->data);
        $pieces = explode("|", $response);
        $data['pieces'] = $pieces;
//        dd($data);
        try {
            $curl = curl_init();
            $url = GeneralSetting::where('key', 'ime_pay_recheck')->first()->value;
            $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
            $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
            $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
            $date = Carbon::now();
            $datas = array(
                "MerchantCode" => $data['merchantCode'],
                "RefId" => $pieces[4],
                "TokenId" => $pieces[6]
            );
//dd(json_encode($datas));
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($datas),
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $imeUser,
                    'Module: ' . $module
                ],
            ));
            $response = curl_exec($curl);
//            dd($response);
            curl_close($curl);
            if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
                ImepayResponse::create([
                    "MerchantCode" => $data['merchantCode'],
                    "ImeTxnStatus" => $pieces[0],
                    "ResponseDescription" => $pieces[1],
                    "Msisdn" => $pieces[2],
                    "TransactionId" => $pieces[3],
                    "RefId" => $pieces[4],
                    "TranAmount" => $pieces[5],
                    "TokenId" => $pieces[6],
                    "RequestDate" => $date,
                    "ResponseDate" => $date,
                ]);
            }
            $storeDta = [
                'payment_method' => "imepay",
                'payment_url' => json_encode($request->data) ?? '',
                'status' => 2,
            ];
            if (Session::get('motorCalcId')) {
                $calcData = MotorCalculationData::find(Session::get('motorCalcId'));
                $calcData->update($storeDta);
                $data['draftPolicy'] = $calcData;
            }
            $data['paymentInfo'] = json_decode($response);
            $data['paymentMethod'] = 'imepay';
            $data['referenceId'] = $pieces[4] ?? "N/A";
            $data['payableAmount'] = $pieces[5] ?? 'N/A';
            Notification::send(Auth::user(), new PaymentFailed($data));
//            dd($data);
            return view('Backend.NonLife.bike.payment-result', $data);
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
            $e->getMessage();
//            return redirect()->back();
        }
    }

    public function paymentSuccess(Request $request)
    {
        try {
            $curl = curl_init();
            $merchantId = GeneralSetting::where('key', 'connectips_merchant_id')->first()->value;
            $appId = GeneralSetting::where('key', 'connectips_app_id')->first()->value;
            $appPwd = GeneralSetting::where('key', 'connectips_app_pwd')->first()->value;
            $txnId = $request->get('TXNID');

            if (Session::get('motorCalcId')) {
                $policy = MotorCalculationData::findOrFail(\Illuminate\Support\Facades\Session::get('motorCalcId'));
                $totalamount = floatval($policy->TOTALNETPREMIUM);
            }
            $txnAmount = intval($totalamount * GeneralSetting::where('key', 'hundreds')->first()->value);
            $tokenMessage = "MERCHANTID=$merchantId,APPID=$appId,REFERENCEID=$txnId,TXNAMT=$txnAmount";

            $detail = [
                'merchantId' => $merchantId,
                'appId' => $appId,
                'referenceId' => $txnId,
                'txnAmt' => $txnAmount,
                'token' => $this->generateConnectipsToken($tokenMessage),
            ];

            curl_setopt_array($curl, array(
                CURLOPT_USERPWD => $appId . ":" . $appPwd,
                CURLOPT_URL => 'https://uat.connectips.com/connectipswebws/api/creditor/validatetxn',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($detail),
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                ],
            ));
            $response = json_decode(curl_exec($curl), true);
//            dd($response);
            curl_close($curl);

            if ($response['status'] === 'SUCCESS') {
                $storeDta = [
                    'payment_method' => "connectips",
                    'payment_output' => json_encode($response) ?? '',
                    'status' => 1,
                ];
                if (Session::get('motorCalcId')) {
                    $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
                }
                $data['customers'] = Lead::where('is_user', 1)->get();
                $data['makePolicy'] = 1;
                $data['calcData'] = $calcData;
                $data['customers'] = Lead::where('is_user', 1)->get();
                $data['makePolicy'] = 1;
                $data['paymentMethod'] = 'ConnectIPS';
                $data['referenceId'] = $txnId ?? "N/A";
                $data['payableAmount'] = $txnAmount ?? 'N/A';
                Notification::send(Auth::user(), new PaymentSuccess($data));
                return redirect()->route('nonLife.calculator.motor.make.policy', $data);
            }
            $data['data'] = "Payment Validation Error";
            $data['paymentMethod'] = 'ConnectIPS';
            $data['referenceId'] = $txnId ?? "N/A";
            $data['payableAmount'] = $txnAmount ?? 'N/A';
            Notification::send(Auth::user(), new PaymentFailed($data));
            return view('Backend.NonLife.bike.policy', $data);
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
        }
    }


    public function paymentCancil(Request $request)
    {

        $data['customers'] = Lead::where('is_user', 1)->get();
        $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
        $data['pieces'] = '';
        $txnId = $request->get('TXNID');
        if (Session::get('motorCalcId')) {
            $policy = MotorCalculationData::findOrFail(\Illuminate\Support\Facades\Session::get('motorCalcId'));
            $totalamount = floatval($policy->TOTALNETPREMIUM);
        }
        $txnAmount = intval($totalamount * GeneralSetting::where('key', 'hundreds')->first()->value);
        $storeDta = [
            'payment_method' => "connectIps",
            'payment_url' => json_encode($request->all()),
            'status' => 2,
        ];

        if (Session::get('motorCalcId')) {
            $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
        }
        $data['paymentMethod'] = 'ConnectIPS';
        $data['referenceId'] = $txnId ?? "N/A";
        $data['payableAmount'] = $txnAmount ?? 'N/A';
        Notification::send(Auth::user(), new PaymentFailed($data));
        return view('Backend.NonLife.bike.payment-result', $data);
    }

    public function updateMotorInfo(Request $request)
    {
        if (Session::get('motorCalcId')) {
            $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($request->except('_token'));
            if ($calcData) {
                return response()->json([
                    'type' => 'success',
                    'message' => 'Motor Info updated Successfully.',
                    'data' => $calcData,
                ], 200);
            }
        }
        return response()->json([
            'type' => 'error',
            'message' => 'Something went wrong.',
            'data' => '',
        ], 500);
    }

    public function makeDraftPolicy(Request $request)
    {
        try {
            if ($request->policy_id && $request->reference_number && $request->status == 1) {
                $dta = CustomerPolicy::where('reference_number', $request->reference_number)->first();
                if ($dta && $dta->status == 1) {
                    return redirect()->back()->withErrors('Policy Already Made');
                }
//            return ('payed');
                Session::forget('motorCalcId');
                Session::put('motorCalcId', $request->policy_id);
                return redirect()->route('nonLife.calculator.motor.make.policy');
            }
            if ($request->policy_id && !$request->reference_number) {
                $this->customerAjaxData();
                $data = $this->variable;
                Session::forget('motorCalcId');
                Session::put('motorCalcId', $request->policy_id);
                if (Session::get('motorCalcId')) {
                    $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['CLASSID', 'customer_id', 'created_at', 'MODEL', 'MAKEMODELID', 'VEHICLENAMEID', 'bluebook_image', 'bike_image', 'MAKEVEHICLEID', 'TYPECOVER', 'YEARMANUFACTURE', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id']);
                    $detas = $this->getMotorDetails($data['formData']);
                }
                $data['makePolicy'] = '1';
                $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
                return view('Backend.NonLife.bike.selectCustomer', $data, $detas);
            }
            if ($request->policy_id && $request->reference_number && $request->status == 0 || $request->policy_id && $request->reference_number && $request->status == 2) {
                $this->customerAjaxData();
                $data = $this->variable;
                Session::forget('motorCalcId');
                Session::put('motorCalcId', $request->policy_id);
                if (Session::get('motorCalcId')) {
                    $data['formData'] = MotorCalculationData::find(Session::get('motorCalcId'))->only(['CLASSID', 'customer_id', 'created_at', 'MODEL', 'MAKEMODELID', 'VEHICLENAMEID', 'bluebook_image', 'bike_image', 'MAKEVEHICLEID', 'TYPECOVER', 'YEARMANUFACTURE', 'CCHP', 'CARRYCAPACITY', 'EXPUTILITIESAMT', 'compulsaryexcessamount', 'pool_premium', 'INCLUDE_TOWING', 'BUSSOCCPCODE', 'BASICPREMIUM_A', 'THIRDPARTYPREMIUM_B', 'DRIVERPREMIUM_C', 'HELPERPREMIUM_D', 'PASSENGERPREM_E', 'RSDPREMIUM_F', 'NETPREMIUM', 'THIRDPARTYPREMIUM', 'stamp', 'OTHERPREMIUM', 'TOTALVATABLEPREMIUM', 'TOTALNETPREMIUM', 'VAT', 'VATAMT', 'VEHICLENO', 'ENGINENO', 'CHASISNO', 'EODAMT', 'MODEUSE', 'REGISTRATIONDATE', 'payment_ref_id']);
                    $detas = $this->getMotorDetails($data['formData']);
                }
                $data['makePolicy'] = '1';
                $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
                return view('Backend.NonLife.bike.selectCustomer', $data, $detas);
            }
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return view('Backend.NonLife.bike.policy', $data);
        }
    }

    public function checkPolicyPayment(Request $request)
    {
        try {
            $refCheck = CustomerPolicy::where('reference_number', $request->RefId)->first();
            if ($refCheck) {
                return redirect()->back()->withErrorMessage('This payment is already used to make policy !');
            }
            $curl = curl_init();
            $url = GeneralSetting::where('key', 'ime_pay_recheck')->first()->value;
            $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
            $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
            $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
            $date = Carbon::now();
            $datas = array(
                "MerchantCode" => $request['MerchantCode'],
                "RefId" => $request['RefId'],
                "TokenId" => $request['TokenId']
            );
//dd(json_encode($datas));
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($datas),
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $imeUser,
                    'Module: ' . $module
                ],
            ));
            $response = curl_exec($curl);
//            dd($response);
            curl_close($curl);
            if ($response === false) {
                $data['data'] = "Payment Validation Error";
                return view('Backend.NonLife.bike.policy', $data);
//                dd('Payment Validation error ' . $response);
            } else {
                $data['paymentInfo'] = json_decode($response);
                if ($data['paymentInfo']->ResponseCode == 0) {
                    $data['makePolicy'] = 1;
                    $data['draftPolicy'] = MotorCalculationData::find($request->policy_id ?? '');
//                dd($data);
                    Session::forget('motorCalcId');
                    Session::put('motorCalcId', $request->policy_id);
                    $date = Carbon::now();
                    if (!ImepayResponse::where('RefId', '=', $request['RefId'])->count()) {
                        ImepayResponse::create([
                            "MerchantCode" => $request['MerchantCode'],
                            "ImeTxnStatus" => $data['paymentInfo']->ResponseCode,
                            "ResponseDescription" => $data['paymentInfo']->ResponseDescription,
                            "Msisdn" => $data['paymentInfo']->Msisdn,
                            "TransactionId" => $data['paymentInfo']->TransactionId,
                            "RefId" => $data['paymentInfo']->RefId,
                            "TranAmount" => $data['draftPolicy']->TOTALNETPREMIUM,
                            "TokenId" => $data['paymentInfo']->TokenId,
                            "RequestDate" => $date,
                            "ResponseDate" => $date,
                        ]);
                    } else {
                        ImepayResponse::where('RefId', '=', $request['RefId'])->update([
                            "ImeTxnStatus" => $data['paymentInfo']->ResponseCode,
                        ]);
                    }
                    $storeDta = [
                        'payment_url' => json_encode($response) ?? '',
                        'status' => 1,
                        'payment_method' => "imepay",
                    ];
                    if (Session::get('motorCalcId') && $data['paymentInfo']->ResponseCode == 0) {
                        $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update($storeDta);
                    }
                } else {
                    return redirect()->back()->withErrorMessage($data['paymentInfo']->ResponseDescription ?? "Something went wrong");
                }
                return view('Backend.NonLife.bike.payment-result', $data);
            }
        } catch (\Exception $e) {
            $data['data'] = $e->getMessage();
            return redirect()->back()->withErrorMessage($e->getMessage());
        }
    }

    public function makeOnlinePolicy(Request $request)
    {
        try {
            $output = ["Something went wrong"];
            if (Session::get('motorCalcId')) {
                $calcData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
//                dd($calcData);
                $policy = CustomerPolicy::where('reference_number', $calcData->payment_ref_id)->first();
//                dd($policy);
                if (isset($policy) && $calcData->payment_ref_id == $policy->reference_number) {
                    $data['data'] = "Policy Already Made. Please Start Again to Add new policy. To see policy click below.";
//                    dd($data);
                    return view('Backend.NonLife.bike.policy', $data);
                }
            }
            if ($calcData->payment_method == "imepay") {
                $check = $this->finalPaymentCheck($calcData);
                if ($check == false) {
                    $calcData = MotorCalculationData::find(Session::get('motorCalcId'))->update([
                        'status' => 2
                    ]);
                    $data['data'] = "Payment Validation Error. Please check the payment transaction from draft policies or start payment procedure again.";
                    return view('Backend.NonLife.bike.policy', $data);
                }
            }
//            dd($check);
//            dd($calcData);
            $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $url = GeneralSetting::where('key', 'api_url')->first()->value;
//            "AGENTCODE" => Auth::user()->role_id == 5 ? '0738' : '0000',
            if ($calcData && $calcData['TYPECOVER'] == "TP") {
                $agentCode = "0000";
            } else {
                $agentCode = Auth::User()->userAgents->first()->liscence_number ?? '0000';
            }
//            dd($agentCode);
            ini_set('serialize_precision', '-1');
            $datas = array(
                "KYCID" => $calcData->customer->KYCID ?? "0000",
                "BRANCHID" => $calcData['BRANCHID'] ?? 1,
                "DEPTID" => $calcData['DEPTID'] ?? 1,
                "HASAGENT" => $calcData['HASAGENT'] ?? 0,
                "AGENTCODE" => $agentCode,
                "INCLUDE_TOWING" => $calcData['INCLUDE_TOWING'] ?? 0,
                "ISGOVERNMENT" => $calcData['ISGOVERNMENT'] ?? 0,
                "EXCLUDE_POOL" => $calcData['EXCLUDE_POOL'] ?? 0,
                "CLASSID" => $calcData['CLASSID'] ?? 21,
                "CATEGORYID" => $calcData['CATEGORYID'] ?? 0,
                "TYPECOVER" => $calcData['TYPECOVER'] ?? '',
                "BUSSOCCPCODE" => $calcData['BUSSOCCPCODE'] ?? 0,
                "MODEUSE" => $calcData['MODEUSE'] ?? '',
                "MAKEVEHICLEID" => $calcData['MAKEVEHICLEID'] ?? 0,
                "MAKEVEHICLE" => "cfO;",
                "HAS_TRAILOR" => $calcData['HAS_TRAILOR'] ?? 0,
                "MAKEMODELID" => $calcData['MAKEMODELID'] ?? 0,
                "MODEL" => $calcData['MODEL'] ?? '',
                "VEHICLENAMEID" => $calcData['VEHICLENAMEID'] ?? 0,
                "NAMEOFVEHICLE" => ":^f/ :sn a",
                "YEARMANUFACTURE" => $calcData['YEARMANUFACTURE'] ?? 0,
                "CCHP" => $calcData['CCHP'] ?? 0,
                "CARRYCAPACITY" => $calcData['CARRYCAPACITY'] ?? 0,
                "REGDATE" => $calcData['REGISTRATIONDATE'] ?? null,
                "EXPUTILITIESAMT" => $calcData['EXPUTILITIESAMT'] ?? 0,
                "UTILITIESAMT" => 0,
                "OGCPU" => true,
                "VEHICLENO" => $calcData['VEHICLENO'] ?? '',
                "RUNNINGVEHICLENO" => "",
                "EVEHICLENO" => $calcData['VEHICLENO'] ?? '',
                "ERUNNINGVEHICLENO" => "",
                "ENGINENO" => $calcData['ENGINENO'] ?? '',
                "CHASISNO" => $calcData['CHASISNO'] ?? '',
                "EODAMT" => $calcData['EODAMT'] ?? 0,
                "NOOFVEHICLES" => "0",
                "NCDYR" => $calcData['NCDYR'] ?? 0,
                "PADRIVER" => $calcData['PADRIVER'] ?? 0,
                "NOOFEMPLOYEE" => $calcData['NOOFEMPLOYEE'] ?? 0,
                "PACONDUCTOR" => $calcData['PACONDUCTOR'] ?? 0,
                "PACLEANER" => 0,
                "NOOFPASSENGER" => $calcData['NOOFPASSENGER'] ?? 0,
                "Driver" => $calcData['Driver'] ?? 0,
                "PASSCAPACITY" => $calcData['PASSCAPACITY'] ?? 0,
                "PAPASSENGER" => $calcData['PAPASSENGER'] ?? 0,
                "ESTCOST" => $calcData['EXPUTILITIESAMT'] ?? 0,
                "OTHERSI" => 0.00,
                "OTHERSIDESC" => "",
                "SHOWROOM" => 0,
                "Vehicleage" => 0,
                "BASICPREMIUM_A" => $calcData['BASICPREMIUM_A'] ?? 0,
                "THIRDPARTYPREMIUM_B" => $calcData['THIRDPARTYPREMIUM_B'] ?? 0,
                "DRIVERPREMIUM_C" => $calcData['DRIVERPREMIUM_C'] ?? 0,
                "HELPERPREMIUM_D" => $calcData['HELPERPREMIUM_D'] ?? 0,
                "PASSENGERPREM_E" => $calcData['PASSENGERPREM_E'] ?? 0,
                "RSDPREMIUM_F" => $calcData['RSDPREMIUM_F'] ?? 0,
                "PAIDAMT" => $calcData['TOTALNETPREMIUM'] ?? 0,
                "STAMPDUTY" => $calcData['stamp'] ?? 0,
                "VATRATE" => $calcData['VAT'] ?? 0,
                "VATAMT" => $calcData['VATAMT'] ?? 0,
                "COD" => $calcData['compulsaryexcessamount'] ?? 0,
                "MERCHANT_TRANS_NO" => $calcData['payment_ref_id'] ?? 0,
                "TRANS_DATE" => Carbon::now()->format('d-M-Y'),
                "MERCHANT_CODE" => /*$calcData['payment_method'] ? $calcData['payment_method']."@lmtrading" :*/
                    "imepay@lmtrading",
                "MERCHANT_PASSWORD" => "",
                "TOTAL_PREMIUM_BEFORE_VAT" => $calcData['NETPREMIUM'] ?? 0,
                "customer_id" => $calcData->customer_id ?? 0,
                "status" => $calcData->status ?? 0,
                "user_id" => $calcData['user_id'] ?? 0,
                "FOCODE" => '0738',
                "payment_method" => $calcData['payment_method'] ?? 'n/a'
            );

            $dt = array(
                'motorDetail' => json_encode($datas),
                'token' => $this->makeSignature($datas),
            );
//            dd(json_encode($dt));

            $curl = curl_init();
            $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/api/Policy/MakeOnlinePolicy",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($dt),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $apiUser,
                ),
            ));
//dd($curl);
            $output = json_decode(curl_exec($curl));
//            dd($output);
            $data['output'] = $output ?? $output->message;
            $data['data'] = $output->data ?? $output->message;
//            dd($data);
            curl_close($curl);

            if ($output->data != null && $output->data[0]->FLAG == "SUCCESS") {
                $datas['output'] = json_encode($data['data']);
                $datas['reference_number'] = $calcData['payment_ref_id'] ?? '0';
                $datas['newCstPolicy'] = CustomerPolicy::create($datas);
                $data['PaymentFrom'] = $calcData['payment_method'] ?? 'N/A';
                $calcData->update(["payment_output" => json_encode($output)]);
                return redirect()->route('nonLife.calculator.policy.done', $data);
            } else {
                if (isset($calcData)) {
                    $calcData->update(["payment_output" => json_encode($output)]);
                }
                return redirect(route('nonLife.calculator.policy.done', $data));
            }
        } catch (\Exception $e) {
//            dd($e->getMessage());
//            return $e->getMessage();
            $data['data'] = json_encode($output) ?? $e->getMessage();
            $data['output'] = $e->getMessage();
            if (isset($calcData)) {
                $calcData->update(["payment_output" => json_encode($output)]);
            }
//            $datas['reference_number'] = $calcData['payment_ref_id'] ?? '0';
//            $datas['newCstPolicy'] = CustomerPolicy::create($datas);
            return redirect(route('nonLife.calculator.policy.done', $data));
        }
    }

    private function finalPaymentCheck($policy)
    {
        $paymentDetails = json_decode(json_decode($policy->payment_token_details ?? array()));
        $curl = curl_init();
        $url = GeneralSetting::where('key', 'ime_pay_recheck')->first()->value;
        $module = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
        $imeUser = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
        $merchantCode = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
        $data['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
        $date = Carbon::now();
        $datas = array(
            "MerchantCode" => $data['merchantCode'],
            "RefId" => $paymentDetails->RefId,
            "TokenId" => $paymentDetails->TokenId
        );
//dd(json_encode($datas));
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Basic ' . $imeUser,
                'Module: ' . $module
            ],
        ));
        $response = curl_exec($curl);
//        dd($response);
        curl_close($curl);
        if ($response === false) {
            return false;
        } else {
            $data['paymentInfo'] = json_decode($response);
            if ($data['paymentInfo'] && $data['paymentInfo']->ResponseCode == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    private function makeSignature($data)
    {
//        dd($data);
        try {
            $apiUser = GeneralSetting::where('key', 'api_username')->first()->value;
            $apiPassword = GeneralSetting::where('key', 'api_user_password')->first()->value;
            $secret_key = GeneralSetting::where('key', 'api_secret_key')->first()->value;

            $dta = array(
                "A_USERID" => $apiUser,
                "B_PASSWORD" => $apiPassword,
                "C_KYCID" => $data['KYCID'],
                "D_CLASSID" => $data['CLASSID'],
                "E_AMOUNT" => $data['TOTAL_PREMIUM_BEFORE_VAT'],
            );
//            dd(json_encode($dta));
            return hash_hmac('sha512', json_encode($dta), $secret_key);
//            Signature in header
        } catch (Exception $e) {
            return ($e->getMessage());
        }
    }

    public function policyDone(Request $request)
    {
//       dd($request->all());
        try {
            if (Session::get('motorCalcId')) {
                $url = GeneralSetting::where('key', 'api_url')->first()->value;
                $calcData = MotorCalculationData::whereId(Session::get('motorCalcId'))->first();
                $policy = CustomerPolicy::where('reference_number', $calcData->payment_ref_id)->first();
//            return $calcData;
                $curl = curl_init();
                $customer_phone = $calcData->customer ? $calcData->customer->MOBILENO : '';
                if ($policy && GeneralSetting::where('key', 'sms_activation')->first()->value && GeneralSetting::where('key', 'sms_activation')->first()->value == 1 && $customer_phone) {
                    $customer_name = strtok($request->data[0]['insured'], " ");
//                    $policy = str_replace('%20', '', preg_replace('/\s+/', '%20', $request->data[0]['ClassName']));
                    // $smsMessage = SMSTemplate::where('key','policy-done')->where('status',1)->first();
                    // $message=str_replace('{{customer_name}}',$customer_name, $smsMessage->Messsage);
                    // $message1=str_replace('{{tpPremium}}',$request->data[0]['tpPremium'], $message);
                    // $message2=str_replace('{{policyNo}}',$request->data[0]['policyNo'], $message1);
                    $message = 'Hi%20' . $customer_name . ',%0aYou%20have%20successfully%20purchased%20a%20policy%20of%20NRs.%20' . $request->data[0]['tpPremium'] . '%20for%20policy%20number:' . $request->data[0]['policyNo'] . '.%0aThank%20you%20for%20choosing%20Ebeema.';
//                        dd($message);
                    $apiBasicUser = base64_encode("lmtrdnghtp:[D}Rp2pSD");
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://sms.isplservices.com/smpp/sendsms?to=977" . $customer_phone . "&from=ISPL&text=" . $message,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 10,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            'Authorization: Basic ' . $apiBasicUser
                        ),
                    ));
//dd($curl);
                    $smsReply = curl_exec($curl);
//            dd ($smsReply);
                    if ($smsReply) {
                        $smsDta['customer_id'] = $calcData->customer_id ?? 'N/A';
                        $smsDta['reference_number'] = $calcData->payment_ref_id ?? 'N/A';
                        $smsDta['message'] = $message ?? 'N/A';
                        $smsDta['response'] = $smsReply ?? 'N/A';
                        SmsResponse::create($smsDta);
                    }
                }
                curl_close($curl);

//                return $calcData;

                // Send mail
                $liveMail = GeneralSetting::where('key', 'mail_nonlife_policy')->first()->value ?? 1;
                if ($liveMail == 1) {
                    $customer_mail = $calcData->customer ? $calcData->customer->EMAIL : '';
                    if ($policy && $customer_mail) {
                        Mail::to($customer_mail)->cc(Auth::user()->email ?? '')->queue(new NonLifePolicy($policy));
                    } else {
                        Mail::to(Auth::user()->email ?? '')->queue(new NonLifePolicy($policy));
                    }
                } else {
                    Mail::to("azizdulal.ad@gmail.com")
                        ->queue(new NonLifePolicy($policy));
                }

                if ($policy && $calcData->bluebook_image || $calcData->bike_image) {
                    $datas = array(
                        'PolicyDetail' => json_encode([
                            "POLICYNO" => $request->data[0]['AcceptanceNo'] ?? '0'
                        ]),
                    );
                    if ($calcData->bluebook_image) {
                        $datas['bluebook'] = new CURLFILE($calcData->bluebook_image);
                    }
                    if ($calcData->bike_image) {
                        $datas['bike'] = new CURLFILE($calcData->bike_image);
                    }
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url . "/Api/Policy/SavePolicyDocument",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 10,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_POSTFIELDS => $datas,
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: multipart/form-data'
                        ),
                    ));
                    $document = json_decode(curl_exec($curl));
//                    dd($document);
                    curl_close($curl);

                }
                if ($document) {
                    $calcData->update(array('image_upload_status' => 1));
                    if ($policy) {
                        $policy->update(array('image_upload_status' => 1));
                    }
                }

//                $calcData->delete();
            }
//            Session::put('motorCalcId', 118);
            Session::forget('motorCalcId');
            $data['output'] = $request->output;
            $data['data'] = $request->data;
            $data['paymentFrom'] = $request->PaymentFrom ?? 'N/A';
            return view('Backend.NonLife.bike.policy', $data);
        } catch (\Exception $e) {
//            dd($e->getMessage());
            $data['output'] = $request->output;
            $data['data'] = $request->data;
            $data['paymentFrom'] = $request->PaymentFrom ?? 'N/A';
            return view('Backend.NonLife.bike.policy', $data);
        }
    }

    public function documentUpload(Request $request)
    {
        try {
            $url = GeneralSetting::where('key', 'api_url')->first()->value;
            $calcData = MotorCalculationData::where('payment_ref_id', $request->ref_id)->first();
            $policy = CustomerPolicy::where('reference_number', $calcData->payment_ref_id)->first();
//        return json_decode($policy->output)[0]->AcceptanceNo;
//        return $calcData;
            if ($policy && $calcData->bluebook_image || $calcData->bike_image) {
                $datas = array(
                    'PolicyDetail' => json_encode([
                        "POLICYNO" => json_decode($policy->output)[0]->AcceptanceNo ?? '0'
                    ]),
                );

                if ($calcData->bluebook_image) {
                    if (function_exists('curl_file_create')) { // php 5.5+
                        $cFile = curl_file_create($calcData->bluebook_image);
                    } else { //
                        $cFile = '@' . realpath($calcData->bluebook_image);
                    }
                    $datas['bluebook'] = $cFile;
                }
                if ($calcData->bike_image) {
                    if (function_exists('curl_file_create')) { // php 5.5+
                        $cFile = curl_file_create($calcData->bike_image);
                    } else { //
                        $cFile = '@' . realpath($calcData->bike_image);
                    }
                    $datas['bike'] = $cFile;
                }
//                    dd($datas);
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url . "/Api/Policy/SavePolicyDocument",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => $datas,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: multipart/form-data'
                    ),
                ));
                $document = json_decode(curl_exec($curl));
//                    dd($document);
                curl_close($curl);

            }
            if (isset($document) && $document->response_message == "SUCCESS") {
                $calcData->update(array('image_upload_status' => 1));
                if ($policy) {
                    $policy->update(array('image_upload_status' => 1));
                }
                return redirect()->back()->withSuccessMessage('Document uploaded successfully.');
            }
            return redirect()->back()->withErrorMessage('Something went wrong!' . curl_error($curl));
        } catch (\Exception $e) {
            return redirect()->back()->withErrorMessage("Error : " . $e->getMessage());
        }
    }

    public
    function getDebitNote(Request $request)
    {
        try {
            $data['data'] = MotorCalculationData::find($request->motorId);
//        return $data;
            $pdf = PDF::loadView('Backend.NonLife.debit-note', $data);
            return $pdf->download('Draft-policy.pdf');
//        return view('Backend.NonLife.debit-note', $data);
        } catch (\Exception $e) {
            echo('Something went wrong, Cannot download PDF');
        }
    }

    public
    function loadPdf(Request $request)
    {
        try {
//        Log::channel('errors')->warning('Something could be going wrong.'.$request);

            $url = GeneralSetting::where('key', 'api_url')->first()->value;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/api/Reports/PreviewDocument?docid=" . $request->docid,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $pdf = json_decode(curl_exec($curl));
//        dd($pdf);

            curl_close($curl);
            if ($pdf != null && $pdf->filePath != "Error") {
                $filename = 'NB-' . $request->proformano . '.pdf';
                $tempImage = tempnam(sys_get_temp_dir(), $filename);
                copy($url . $pdf->filePath, $tempImage);

                return response()->download($tempImage, $filename);
            }
            return redirect()->back()->withErrors('Cannot Download PDF.');
//        return response('Cannot Download PDF');
//        return Redirect::to($url.$pdf->filePath);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Cannot Download PDF.' . $e->getMessage());
//            dd($e->getMessage());
        }
    }

    public function bikeCalculator(Request $request)
    {
        if (GeneralSetting::where('key', 'non_life_maintainance_mode')->first()->value == 1) {
            return view('frontend.maintainance');
        }
        $data['classId'] = 21;
        Session::forget('motorCalcId');
        $this->CalculatorData($data['classId'] ?? '');
        $data = $this->variable ?? [];
        return view('Backend.NonLife.bike.bikeCalculator', $data);
    }

    public
    function carCalculator(Request $request)
    {
        if (GeneralSetting::where('key', 'non_life_maintainance_mode')->first()->value == 1) {
            return view('frontend.maintainance');
        }
        $data['classId'] = 23;
        Session::forget('motorCalcId');
        $this->CalculatorData($data['classId'] ?? '');
        $data = $this->variable;
//        dd($data);
        return view('Backend.NonLife.Car.carCalculator', $data);
    }

    public
    function commercialCarCalculator(Request $request)
    {
        if (GeneralSetting::where('key', 'non_life_maintainance_mode')->first()->value == 1) {
            return view('frontend.maintainance');
        }
        $data['classId'] = 22;
        Session::forget('motorCalcId');
        $this->CalculatorData($data['classId'] ?? '');
        $data = $this->variable;
        return view('Backend.NonLife.Car.commercialCarCalculator', $data);
    }

    public function viewPolicy(CustomerPolicyDataTable $dataTable)
    {
        return $dataTable->render('Backend.NonLife.policy-list');
    }

    public function updateStatus(Request $request)
    {
//        dd($request->all());
        try {
            $policy = CustomerPolicy::findOrFail($request->id);
            $policy->policy_status = $request->status;
            $policy->save();
            return response()->json([
                'status' => 'success',
                'message' => "status changed Successfully"
            ], 200);
        } catch (\Exception $e) {
//            return $e->getMessage();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 200);
        }
    }

    public function viewDraftPolicy(DraftPolicyDataTable $dataTable)
    {
        return $dataTable->render('Backend.NonLife.DraftPolicies.index');
    }

    public function filter(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $policies = CustomerPolicy::where('created_at', '>=', $request->from)->where('created_at', '<=', $request->to)->get();
        // return redirect()->route('policy.view')->with($policies);
        return view('Backend.NonLife.policy-list', compact('policies'));
    }

    private function generateConnectipsToken($tokenMessage)
    {
        $certificate = \Illuminate\Support\Facades\Storage::get('/public/certificates/connectips.pfx');

        if (openssl_pkcs12_read($certificate, $cert_info, '123')) {
            $private_key = openssl_pkey_get_private($cert_info['pkey']);
        } else {
            throw new \Exception('Unable to read certificate.');
        }

        if (openssl_sign($tokenMessage, $signature, $private_key, 'sha256WithRSAEncryption')) {
            $hash = base64_encode($signature);
            openssl_free_key($private_key);
        } else {
            throw new \Exception('Unable to sign certificate.');
        }

        return $hash;
    }

}
