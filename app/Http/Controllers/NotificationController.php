<?php

namespace App\Http\Controllers;

use App\Models\user_activity_logs;
use App\Notifications\CategoryCreated;
use App\Notifications\LeadRemarksCreated;
use App\Notifications\PaymentSuccess;
use App\Notifications\PremiumPaid;
use App\Notifications\PremiumUpdate;
use App\Notifications\UserActivated;
use App\Notifications\NewLeadCreated;
use DB;
use Auth;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(DatabaseManager $databaseManager)
    {
        $loggedInUser = \Illuminate\Support\Facades\Auth::user();
        $notifications = $databaseManager->table('notifications')
            ->select(
                'data',
                'created_at',
                'type',
            )->when($loggedInUser->designation !== 'Super Admin', function ($query) {
                $query->where('notifications.notifiable_id', \Illuminate\Support\Facades\Auth::user()->id);
            })->latest()
            ->limit(5)
            ->get();

        return response()->json([
            'notifications' => $notifications->map(function ($notification) {
                $notification->data = json_decode($notification->data);
                $notification->message = $this->getMessage($notification);

                return $notification;
            }),
            'hasUnreadNotification' => $loggedInUser->unreadNotifications->count() > 0
        ]);
    }

    private function getMessage($notification)
    {
        if ($notification->type === LeadRemarksCreated::class) {
            return 'Lead remarks: ' . $notification->data->remark . ' was made';
        }
        if ($notification->type === PremiumPaid::class) {
            return 'Premium Paid of NRs.' . $notification->data->premium_amount . ' at date ' . $notification->data->paid_date;
        }
        if ($notification->type === PremiumUpdate::class) {
            return 'Premium paid updated for NRs.' . $notification->data->premium_amount . ' at date ' . $notification->data->paid_date;
        }
        if ($notification->type === PaymentSuccess::class) {
            return 'Payment through ' . $notification->data->paymentMethod . ' was successful';
        }
        if ($notification->type === UserActivated::class) {
            return 'User: ' .$notification->data->user->name. ' status changed successfully';
        }
        if($notification->type = NewLeadCreated::class){
            return 'Lead : New Lead has been created';
        }

    }

    public function readAll(Request $request)
    {
        $loggedInUser = $request->user();
        $loggedInUser->unreadNotifications->markAsRead();

        return response()->json([
            'message' => 'Successfully read all the notifications'
        ]);
    }
}
