<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\PaybackSchedule;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use App\DataTables\PaybackScheduleDataTable;
class PaybackScheduleController extends Controller
{
    public function index(PaybackScheduleDataTable $dataTable)
    {
        $data['products'] = Product::all();
        return $dataTable->render('paybackSchedule.view',$data);
    }
    public function create()
    {
        $products = Product::all();
        return view('paybackSchedule.create', compact('products'));
    }
    public function store(Request $request)
    {
        $data = $request->input();
        // dd($data);
        try {
            $pay = new PaybackSchedule();
            $pay->term_year = $data['term_year'];
            $pay->payback_year = $data['payback_year'];
            $pay->rate = $data['rate'];
            $pay->company_id = $data['company_id'];
            $pay->product_id = $data['product_id'];
            if ($pay->payback_year <= $pay->term_year) {
                // dd($pay);
                # code...
                $pay->save();
                return redirect()->route('admin.payback.view')->withSuccessMessage('Payment Schedule added successfully!');
            } else {
                # code...
                return back()->withErrors('Failed to create Pay Back Schedule');
            }
        } catch (Exception $e) {
            return back()->withErrors('Failed to create Pay Back Schedule');
        }
    }
    public function edit(Request $request, $id)
    {
        $payback = PaybackSchedule::find($id);
        $products = Product::all();
        return view('paybackSchedule.edit', compact('payback', 'products'));
    }
    public function update(Request $request, $id)
    {
        $data = $request->input();
        try {
            $pay = PaybackSchedule::findorFail($id);
            $pay->term_year = $data['term_year'];
            $pay->payback_year = $data['payback_year'];
            $pay->rate = $data['rate'];
            $pay->company_id = $data['company_id'];
            $pay->product_id = $data['product_id'];
            $pay->save();
            return redirect()->route('admin.payback.index')->withSuccessMessage('Payment Schedule updated successfully!');
        } catch (Exception $e) {
            return back()->with('failed', 'Failed to create Pay Back Schedule');
        }
    }
    public function destroy($id)
    {
        $pay = PaybackSchedule::findOrfail($id);
        $pay->delete();

        if ($pay) {
            return response()->json([
                'type' => 'success',
                'message' => 'Payment Schedule is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'Payment Schedule can not be deleted.'
        ], 422);
    }
}
