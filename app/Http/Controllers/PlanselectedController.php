<?php

namespace App\Http\Controllers;

use App\Models\Planselected;
use App\Mail\Planselect;
use App\Models\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanselectedController extends Controller
{


    public function index()
    {
            $data['selected'] = Planselected::orderBY('id','desc')->get();
            return view('Backend.planselected.index', $data);

    }

    public function updateStatus(Request $request)
    {
//        dd($request->all());
        try {
            $plan = Planselected::findOrFail($request->id);
            $plan->status = $request->status;
            $plan->save();
            return response()->json([
                'status' => 'ok',
                'user' => $plan,
                'message' => "status changed Successfully"
            ], 200);
        } catch (\Exception $e) {
//            return $e->getMessage();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 200);
        }
    }

    //
    public function store(Request $request)
    {
        try {

            $data = $request->except('front_citizen', 'back_citizen');
            $front_citizen=$request->front_citizen;
            $back_citizen=$request->back_citizen;

            $destinationpath = 'uploads/citizen/';

            if($front_citizen){
                 $extension = strrchr($front_citizen->getClientOriginalName(), '.');
                 $new_file_name = str_replace(' ','_',$request->name).'_front' . time();
                 $front_image = $front_citizen->move($destinationpath, $new_file_name . $extension);
                 $data['citizen_front'] = isset($front_image) ? $new_file_name . $extension : null;
            }
            if($back_citizen){
                 $extension = strrchr($back_citizen->getClientOriginalName(), '.');
                 $new_file_name = str_replace(' ','_',$request->name).'_back' . time();
                 $back_image = $back_citizen->move($destinationpath, $new_file_name . $extension);
                 $data['citizen_back'] = isset($back_image) ? $new_file_name . $extension : null;
            }


            $selected = Planselected::create($data);
            $leadId=\Session::get('LEAD_ID');
            $lead=Lead::find($leadId)->update([
                'maturity_period'=>$data['term'],
                'sum_insured'=>$data['sum_assured'],
                'age'=>$data['age'],
                'category'=> $data['category'],
                'insurance_company_name' => $data['company_id'],
                'product_id' =>$data['product_id']
            ]);
            if ($selected) {
//                dd($selected);
                \Mail::to(Auth::user())
                    ->send(new Planselect($data));
                return response()->json([
                    'status' => 'ok',
                    'user' => $selected,
                    'message' => 'Plan is added successfully.'
                ], 200);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Plan can not be selected.'
                ], 422);
            }
        }
        catch (\Exception $e){
//            dd($e->getMessage());
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 422);
        }
    }
}
