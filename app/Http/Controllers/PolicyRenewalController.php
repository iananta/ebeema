<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\GeneralSetting;
use App\Models\PolicyRenewal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\OtpCodeMail;

class PolicyRenewalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        if ($request/*->PROVINCECODE && $request->VEHICLELOTNO && $request->VEHICLERUNNINGNO || $request->POLICYNO*/) {
            $this->getDocList($request->PROVINCECODE, $request->VEHICLELOTNO, $request->VEHICLERUNNINGNO, $request->POLICYNO,$request->PHONENO,$request->cat_id);
            $data = $this->data;
        }
//        $this->MakeOtpCode(4);
        return view('Backend.PolicyRenewal.index', $data);
    }

    public function getDocList($province, $lotNo, $runningNo, $policyNo, $MOBILENO,$catID)
    {
//        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $url = "203.78.165.19:9198";
        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $curl = curl_init();

//        $datas = array(
//            "PROVINCECODE" =>  "",
//            "VEHICLELOTNO" =>  "",
//            "VEHICLERUNNINGNO" =>  "6425",
//            "POLICYNO" =>  "",
//            "CATEGORYID" =>  "",
//            "MOBILENO" => "9813004705",
//            "PageNumber" =>  1,
//            "PageSize" =>  10
//        );
        $datas = array(
            "PROVINCECODE" => $province ?? '',
            "VEHICLELOTNO" => $lotNo ?? '',
            "VEHICLERUNNINGNO" => $runningNo ?? '',
            "POLICYNO" => $policyNo ?? '',
            "CATEGORYID" => $catID,
            "MOBILENO" => $MOBILENO ?? '',
            "PageNumber" => 1,
            "PageSize" => 10
        );
//        dd($datas);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/Motor/GetMotorDocumentList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $apiUser
            ),
        ));

        $docList = json_decode(curl_exec($curl));
//        dd($docList);
        curl_close($curl);
        $data['documents'] = $docList->data ?? array();
        return $this->data = $data;
    }


    private function MakeOtpCode($length_of_string)
    {
        // String of all alphanumeric character
        $str_result = '0123456789';

        // Shufle the $str_result and returns substring of specified length
        $code = substr(str_shuffle($str_result),
            0, $length_of_string);

        Session::put("OtpCode", $code);

        $details = [
            'title' => 'Otp Code For Policy Renewal Details',
            'body' => 'Please use this OTP for getting details of the Policy : ' . $code
        ];

        Mail::to("azizdulal.ad@gmail.com")
            ->queue(new OtpCodeMail($details));

        return ("Email is Sent.");
    }

    function policyDetails($id)
    {
        $url = "203.78.165.19:9198";
        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $curl = curl_init();

        $datas = array(
            "DOCID" => $id ?? ''
        );
//        dd($datas);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/Motor/GetMotorInfo",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ' . $apiUser
            ),
        ));

        $docList = json_decode(curl_exec($curl));
//        dd($docList);
        curl_close($curl);
        $data['policyDetails'] = $docList->data ?? array();
        $data['docId'] = $id;
//        dd($data);
        return view('Backend.PolicyRenewal.policyDetails', $data);
    }

    public function motorRateCalculation(Request $request)
    {

        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $datas = json_decode($request->data);
//        dd(json_encode($datas));

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/Calculator/CalculateMotorPremium",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $data['output'] = json_decode(curl_exec($curl));
//        dd($data);

        $data['PremDetails'] = $data['output']->PremDetails ?? array();
        $data['requestedDatas'] = $datas ?? array();
        $data['stamp'] = 10;
//        dd($data);
        curl_close($curl);
        return view('Backend.PolicyRenewal.newRate', $data);

    }
}
