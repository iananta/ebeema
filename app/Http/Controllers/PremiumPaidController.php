<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\PremiumPaidByCustomer;
use App\DataTables\PremiumCollectionDataTable;
class PremiumPaidController extends Controller
{
    

    public function __construct(){
       
    }

    public function index(PremiumCollectionDataTable $dataTable){
        return $dataTable->render('Backend.premiumpaid.index');
    }

    public function getPremiumCollection(Request $request){
        $data['user']=User::with('premiumPaidByCustomer')->findOrFail($request->id);
        $premiumDetails=\View::make('Backend.premiumpaid.includes.premium-details')->with($data)->render();
        return response()->json(['premiumDetails'=>$premiumDetails],200);
    }

    public function premiumVerify(Request $request){
        $premium=PremiumPaidByCustomer::where('id',$request->id)->update($request->except('_token','id'));
        if($premium){
            $message=$request->verify == 0 ? 'Premium amount changed to not verified' : 'Premium amount changed to verified';
        }
        return response()->json(['message'=>$message],200);
    }

}
