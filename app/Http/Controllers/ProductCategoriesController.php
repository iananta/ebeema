<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\User;
use App\Notifications\CategoryCreated;
use App\Notifications\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class ProductCategoriesController extends Controller
{
    public function index(){
        $data['categories'] = ProductCategory::all();
        return view('Backend.ProductCategories.index', $data);
    }

    public function create()
    {
        return view('Backend.ProductCategories.create');
    }

    public function store(Request $request)
    {
        try{
        $request->validate([
            'name'=>'required|min:3',
        ]);

        $category = ProductCategory::create($request->all());
        return redirect()->route('admin.category')->withSuccessMessage('Product Category created Successfully');
        }catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function edit($id)
    {
        $data['category'] = ProductCategory::findOrFail($id);
        return view('Backend.ProductCategories.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|min:3',
        ]);

        $category = ProductCategory::findOrFail($id);
        $category->update($request->all());
        return redirect()->route('admin.category')->withSuccessMessage('Product Category is updated Successfully');
    }

    public function delete($id)
    {

        $category = ProductCategory::findOrfail($id);
        $category->delete();

        if ($category) {
            return response()->json([
                'type' => 'success',
                'message' => 'Product Categories is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'Product Categories can not be deleted.'
        ], 422);
    }

    public function changeStatus($id)
    {
        try {
            $pd = ProductCategory::findOrFail($id);
            $pd->status == '0' ? $pd->status = '1' : $pd->status = '0';
            $pd->save();
            $message = ($pd->status == '1') ? "Product Category is activated successfully." : "Product Category is deactivated successfully.";
            return response()->json([
                'status' => 'ok',
                'cat' => $pd,
                'message' => $message
            ], 200);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
