<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use App\Models\Lead;
use App\Models\SMSTemplate;
use Dropbox\Exception;
use Illuminate\Http\Request;
use App\Models\SmsResponse;
class SMSTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sms'] = SMSTemplate::all();
        return view('Backend.smsTemplate.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.smsTemplate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            "default_body" => $request->body
        ]);
//        dd($request->all());
        try {
            $sms = SMSTemplate::create($request->all());

            return redirect()->route('admin.smsTemplate.index')->withSuccessMessage('SMS Template is added successfully.');
        }catch (Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public function changeStatus($id)
    {
        try {
            $sms = SMSTemplate::findOrFail($id);
            $sms->status == '0' ? $sms->status = '1' : $sms->status = '0';
            $sms->save();
            $message = ($sms->status == '1') ? "SMS is activated successfully." : "SMS is deactivated successfully.";
            return response()->json([
                'status' => 'ok',
                'sms' => $sms,
                'message' => $message
            ], 200);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SMSTemplate  $sMSTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(SMSTemplate $sMSTemplate)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SMSTemplate  $sMSTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sms = SMSTemplate::findorFail($id);
        return view('Backend.smsTemplate.edit', compact('sms'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SMSTemplate  $sMSTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'title' => 'required',
            ]);
            $data['title'] = $request->title;
            $data['message'] = $request->message;
            $sms = SMSTemplate::where('id', $id)->update($data);
            if ($sms) {
                return redirect()->route('admin.smsTemplate.index')->withSuccessMessage('SMS Template is Updated successfully.');
            }
            return redirect()->back()->withInput()->withWarningMessage('SMS Template can not be updated.');
        }catch (Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function resetMail($id){
        $sms = SMSTemplate::findOrFail($id);
        $sms->body = $sms->default_body;
        $sms->save();
        return redirect()->back()->withSuccessMessage('The form was reset!');
    }

    public function smsSend($id){
        $data['smsTemp'] = SMSTemplate::findOrFail($id);
        $data['customers'] = Lead::orderBy('created_at', 'desc')->get();
        return view('Backend.Sms.send', $data);
    }
    public function smsToCustomer(Request $request)
    {
        $curl = curl_init();
        $apiBasicUser = base64_encode("demohtptestint:Demoapi@2022");
        $customerId=[];
        $phone_numberArr=[];
        $message=$request->message;
        // $messages = str_replace("&nbsp;", "", $message);
        $messages = str_replace('%20', '', preg_replace('/\s+/', '%20', $message));
        // $messages = preg_replace("/\s/",'',$message);
        foreach($request->customers as $customer){
            $arr=explode('-',$customer);
            $customerId[]=$arr[0];
            $phone_numberArr[]=$arr[1];
        }
//        dd($customer_phone);
        if (GeneralSetting::where('key', 'sms_activation')->first()->value && GeneralSetting::where('key', 'sms_activation')->first()->value == 1 && $phone_numberArr[0]) {
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://sms.isplservices.com/smpp/sendsms?to=977" . $phone_numberArr[0] . "&from=ISPL&text=" . $messages,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic ' . $apiBasicUser
                ),
            ));
            $smsReply = curl_exec($curl);
            if ($smsReply) {
                $smsDta['customer_id'] = $customerId[0] ?? null;
                $smsDta['phone_number'] = $phone_numberArr[0] ?? null;
                $smsDta['reference_number'] = "premium-pay";
                $smsDta['message'] = $message ?? 'N/A';
                $smsDta['response'] = $smsReply ?? 'N/A';
                //dd($smsDta);
                $created=SmsResponse::create($smsDta);
            }
        }
        curl_close($curl);
        return redirect()->back()
        ->withSuccessMessage('Sms Send successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SMSTemplate  $sMSTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(SMSTemplate $sMSTemplate)
    {
        //
    }
}
