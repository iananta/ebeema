<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SeoForm;
use App\DataTables\SeoFormDatatable;
class SeoFormController extends Controller
{   
    private $seoform;
    public function __construct(SeoForm $seoform){
        $this->seoform=$seoform;
    }


    public function index(SeoFormDatatable $dataTable){
        return $dataTable->render('Backend.seoform.index');
    }

    public function create(){
        return view('Backend.seoform.form');
    }

    public function store(Request $request){
        $validate=$request->validate([
            'form_name'=>'required',
            'form_id'=>'required|unique:seo_forms'
        ]);

        $this->seoform->create($validate);
        return redirect()->route('admin.seoForm')->with('message','Seo Form details created successfully');
    }

    public function edit($id){
        $data['seoform']=$this->seoform->findOrFail($id);
        return view('Backend.seoform.form')->with($data);
    }

    public function update(Request $request,$id){
        $validate=$request->validate([
            'form_name'=>'required',
            'form_id'=>'required|unique:seo_forms,form_id,'.$id
        ]);
        $this->seoform->where('id',$id)->update($validate);
        return redirect()->route('admin.seoForm')->with('message','Seo Form details updated successfully');

    }

    public function delete(Request $request){
        $this->seoform->where('id',$request->id)->delete();
        return response()->json(['message'=>'Seo Form details deleted successfully'],200);
    }
}
