<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscriptionplan;
use App\Models\Subscriptionusage;
use App\Models\GeneralSetting;
use App\Models\ImepayResponse;
use Carbon\Carbon;
class SubscriptionController extends Controller
{
    private $subscriptionplan;
    public function __construct(Subscriptionplan $subscriptionplan){
        $this->subscriptionplan=$subscriptionplan;
    }

    public function index(){
        $data['subscriptionplan']=$this->subscriptionplan->get();
        return view('subscription.subscriptionplan')->with($data);
    }

    public function subscriptionInvoice(Request $request){
        $amount=/*$request->amount*/ 1.50 ;
        $planId=\Session::put('subscriptionplan_id',$request->plan_id);
        $duration=\Session::put('subsctioinplan_duration',$request->duration);
        $url=GeneralSetting::where('key','ime_pay_token')->first()->value;

        $imeAuth=$this->imeAuthentication();
        $merchantCode=$this->imeMerchantCode();
        $refId='REF-'.$this->random_strings(5).'-' . Carbon::now()->format('dmy');
        $ime_pay_checkout_url = GeneralSetting::where('key', 'ime_pay_checkout')->first()->value;
        $curl=curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"MerchantCode\":\"$merchantCode\", \"Amount\":\"$amount\", \"RefId\":\"$refId\"} ",
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Authorization: Basic ' . $imeAuth['imeUser'],
                'Module: ' . $imeAuth['module']
            ],
        ));
            $response = curl_exec($curl);
            $paymentInfo = json_decode($response);
            curl_close($curl);
        return view('subscription.subscriptioninvoice')->with(['amount'=>$amount,'planId'=>$planId,'paymentInfo'=>$paymentInfo,'ime_pay_checkout_url'=>$ime_pay_checkout_url,'merchantCode'=>$merchantCode]);
    }

    public function paymentSuccess(Request $request){
       $subscriptionusage=auth()->user()->subscriptionusage;
       $imeAuth=$this->imeAuthentication();
       $url=$this->imeConfirmUrl();
       $merchantCode=$this->imeMerchantCode();
       $date = Carbon::now();
       $response = base64_decode($request->data);
       $pieces = explode("|", $response);
       $subscriptionusage=auth()->user()->subscriptionusage;
       $imeAuth=$this->imeAuthentication();
       $url=$this->imeConfirmUrl();
       $merchantCode=$this->imeMerchantCode();
       $date = Carbon::now();
       $response = base64_decode($request->data);
       $pieces = explode("|", $response);
        $datas = array(
            "MerchantCode" => $merchantCode,
            "RefId" => $pieces[4],
            "TokenId" => $pieces[6],
            "TransactionId" => $pieces[3],
            "Msisdn" => $pieces[2],
        );
         try{
             \DB::connection('mysql')->beginTransaction();
                $curl=curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($datas),
                    CURLOPT_HTTPHEADER => [
                        'Content-Type: application/json',
                        'Authorization: Basic ' . $imeAuth['imeUser'],
                        'Module: ' . $imeAuth['module'],
                    ],
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                if ($response === false) {
                    $failed = "Payment Validation Error";
                    return redirect()->route('subscription.plan')->with('error',$failed);

                }else{
                    $data['paymentInfo'] = json_decode($response);
                       if (!ImepayResponse::where('RefId', '=', $pieces[4])->count()) {
                            \DB::table('imepay_responses')->insert([
                                "MerchantCode" => $merchantCode,
                                "ImeTxnStatus" => $pieces[0],
                                "Msisdn" => $pieces[2],
                                "TransactionId" => $pieces[3],
                                "RefId" => $pieces[4],
                                "TranAmount" => $pieces[5],
                                "TokenId" => $pieces[6],
                                "RequestDate" => $date,
                                "ResponseDate" => $date,
                            ]);
                        }
                    $storeData=[
                        'payment_url' => json_encode($request->data) ?? '',
                        'subscriptionplan_id'=>\Session::get('subscriptionplan_id'),
                        'user_id'=>auth()->user()->id,
                        'amount'=>$pieces[5],
                        'ref_id'=>$pieces[4],
                        'paid_date'=>$date
                    ];
                    if($subscriptionusage){
                        $subscriptionusage->update([
                            'paid_date'=>$date,
                            'end_date'=>date('Y-m-d', strtotime('+'.\Session::get('subsctioinplan_duration').' month', strtotime($date)) )
                        ]);
                    }else{
                        \DB::table('subscriptionusages')->insert([
                            'user_id'=>auth()->user()->id,
                            'subscriptionplan_id'=>\Session::get('subscriptionplan_id'),
                            'paid_date'=>$date,
                            'end_date'=>date('Y-m-d', strtotime('+'.\Session::get('subsctioinplan_duration').' month', strtotime($date)) )
                        ]);
                    }
                    \DB::table('subscriptionpayment_infos')->insert($storeData);
                }
                \DB::connection('mysql')->commit();
                return redirect()->route('subscription.plan')->with('message','Payment Successful');
            }
            catch(ValidationException $e){
                \DB::connection('mysql')->rollBack();
                return redirect()->route('subscription.plan')->with('error',$e->getMessage());
            }
            catch(Exception $e){
                \DB::connection('mysql')->rollBack();
               return redirect()->route('subscription.plan')->with('error',$e->getMessage());
            }
        
    }

    public function canclePayment(Request $request){
       return redirect()->route('subscription.plan');
    }

    public function random_strings($length_of_string)
    {
        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            // Shufle the $str_result and returns substring
            // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);
    }

    protected function imeAuthentication(){
        $data=[
            'module' => base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value),
            'imeUser' => base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value),
        ];
        return $data;
    
    }

    protected function imeMerchantCode(){
        return GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
    }

    protected function imeConfirmUrl(){
        return GeneralSetting::where('key', 'ime_pay_confirm')->first()->value;
    }
}
