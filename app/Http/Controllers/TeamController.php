<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (control('frontend-dynamics')) {
            $data['teams'] = Team::orderBy('position', 'asc')->where('status', 1)->get();
            return view('Backend.Team.index', $data);
        } else {
            return view('layouts.backend.denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $position = (Team::max('position')) + 1;
            $image = $request->file('image');
            $file = new Team();

            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/testimonial'), $imageName);
            $file->create([
                'image' => $imageName,
                'name' => $request['name'],
                'designation' => $request['designation'],
                'phone' => $request['phone'],
                'email' => $request['email'],
                'position' => $position,
                'fb' => $request['fb'],
                'twitter' => $request['twitter'],
                'linkedin' => $request['linkedin'],
                'status' => true,

            ]);
            return redirect()->back()->withSuccess_message('Team Added Successfully !');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $image = $request->file('image');
            $file = Team::find($id);
            if ($image) {
                $path = $file['image'];
                if (file_exists($path)) {
                    unlink($path);
                }
                $imageName = time() . ('.' . $image->getClientOriginalExtension());
                $image->move(public_path('images/testimonial'), $imageName);
                $file->image = $imageName;
            }
            $file->name = $request['name'];
            $file->designation = $request['designation'];
            $file->phone = $request['phone'];
            $file->email = $request['email'];
            $file->position = $request['position'];
            $file->fb = $request['fb'];
            $file->twitter = $request['twitter'];
            $file->linkedin = $request['linkedin'];
            $file->save();
            return redirect()->back()->withSuccess_message('Team Updated Successfully !');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $team = Team::findorfail($id);

            $path = $team['image'];
            if (file_exists($path)) {
                unlink($path);
            }
            $team->delete();

            return response()->json([
                'type' => 'success',
                'testimonial' => $team,
                'message' => 'Team deteled successfully.'
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'type' => 'error',
                'testimonial' => $team,
                'message' => 'Team can not deleted.'
            ], 422);
        }
    }

    public function statusChange($id)
    {
        try {
            $team = Team::find($id);
//            dd($team);
            if ($team->status == 1) {
                $team->status = 0;
            } else {
                $team->status = 1;
            }
            $team->update();

            return response()->json([
                'type' => 'success',
                'testimonial' => $team,
                'message' => 'Active status changed successfully.'
            ], 200);
        } catch (\Exception $e) {
//            return $e->getMessage();
            return response()->json([
                'type' => 'error',
                'testimonial' => $team,
                'message' => 'Status can not be changed.'
            ], 422);
        }
    }

    public function sort(Request $request)
    {
        foreach ($request->teams as $team) {
            $career = Team::find($team['id']);
            $career->position = $team['position'];
            $career->save();
        }
        return json_encode(['status' => 'success', 'value' => 'Successfully reordered.'], 200);
    }
}
