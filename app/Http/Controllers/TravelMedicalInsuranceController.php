<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\CoverPackageType;
use App\Models\CoverType;
use App\Models\CoverTypePlans;
use App\Models\GeneralSetting;
use App\Models\KYC;
use App\Models\PlanArea;
use App\Models\TravelMedicalInsurance;
use App\Models\TravelMedicalPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;
use App\Http\Controllers\ImePayController;
use App\Http\Controllers\KYCController;
use Exception;
use Illuminate\Support\Facades\Storage;

class TravelMedicalInsuranceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function TravelDatas()
    {
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $curl = curl_init();

//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $url . "/Api/TMICalculator/TMICoverType",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 5,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'GET',
//            CURLOPT_POSTFIELDS => '',
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json',
//                'Authorization: Basic ' . $apiUser,
//            ),
//        ));
//
//        $data['coverTypes'] = json_decode(curl_exec($curl));

        $data['coverTypes'] = CoverType::where('is_active', 1)->get();

        $datas = array(
            "CountryCode" => "USD"
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/NRB/GetForeignExchangeRates",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $data['rates'] = json_decode(curl_exec($curl));
        curl_close($curl);
        $data['countries'] = Country::all();
//        dd($data);
        return $this->variable = $data;
    }

    public function getPlans(Request $request)
    {
//        return $request;
//        $url = GeneralSetting::where('key', 'api_url')->first()->value;
//        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

//        $curl = curl_init();
//        $datas = array(
//            "CoverId" => $request->CoverId,
//        );
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $url . "/Api/TMICalculator/GetPlanType",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 5,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS => json_encode($datas),
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json',
////                'Authorization: Basic ' . $apiUser,
//            ),
//        ));
//
//        $tmiPlans = json_decode(curl_exec($curl));
//        curl_close($curl);
        $planIds = CoverTypePlans::where('cover_type_id', $request->CoverId)->pluck('plan_area_id')->unique();
        $tmiPlans = PlanArea::whereIn('id', $planIds)->where('is_active', 1)->get();
        if ($tmiPlans) {
            return response()->json([
                'type' => 'success',
                'tmiPlans' => $tmiPlans,
                'message' => 'Calculated successfully.'
            ], 200);
        } else {
            return response()->json([
                'type' => 'success',
                'tmiPlans' => $tmiPlans,
                'message' => 'Calculation error.'
            ], 500);
        }
    }

    public function getPackages(Request $request)
    {
////        return $request;
//        $url = GeneralSetting::where('key', 'api_url')->first()->value;
////        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);
//
//        $curl = curl_init();
//        $datas = array(
//            "CoverId" => $request->CoverId,
//            "PlanId" => $request->PlanId
//        );
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => $url . "/Api/TMICalculator/GetPackageType",
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => '',
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 5,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => 'POST',
//            CURLOPT_POSTFIELDS => json_encode($datas),
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json',
////                'Authorization: Basic ' . $apiUser,
//            ),
//        ));
//        $data['tmiPackages'] = json_decode(curl_exec($curl));
//        curl_close($curl);
        $planIds = CoverTypePlans::where('cover_type_id', $request->CoverId)->where('plan_area_id', $request->PlanId)->pluck('package_type_id')->unique();
        $data['tmiPackages'] = CoverPackageType::whereIn('id', $planIds)->where('is_active', 1)->get();
        $plan = PlanArea::where('id', $request->PlanId)->first();
        if ($plan->exclude_country == 1) {
            $data['countries'] = Country::where('plan_id', 6)->whereNotIn('id', [231, 38])->get();
        } else {
            $data['countries'] = Country::where('plan_id', $request->PlanId)->get();
        }
        if ($data) {
            return response()->json([
                'type' => 'success',
                'data' => $data,
                'message' => 'Calculated successfully.'
            ], 200);
        } else {
            return response()->json([
                'type' => 'success',
                'data' => $data,
                'message' => 'Calculation error.'
            ], 500);
        }
    }

    public function getPremium(Request $request)
    {
//        return $request;
        $url = GeneralSetting::where('key', 'api_url')->first()->value;
//        $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

        $curl = curl_init();
        $datas = array(
            "COVERTYPE" => $request->COVERTYPE,
            "COVERTYPECODE" => $request->COVERTYPECODE,
            "PLAN" => $request->PLAN,
            "PLANCODE" => $request->PLANCODE,
            "PACKAGE" => $request->PACKAGE,
            "PACKAGECODE" => $request->PACKAGECODE,
            "Age" => $request->Age ?? 0,
            "Day" => $request->Day ?? 0,
            "ISANNUALTRIP" => $request->ISANNUALTRIP ?? '0',
            "ISLINKCODEENABLE" => 1
        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . "/Api/TMICalculator/CalculateTMIPremium",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datas),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
//                'Authorization: Basic ' . $apiUser,
            ),
        ));

        $tmiPremium = json_decode(curl_exec($curl));
        curl_close($curl);
        if ($tmiPremium) {
            return response()->json([
                'type' => 'success',
                'tmiPackages' => $tmiPremium,
                'message' => 'Calculated successfully.'
            ], 200);
        } else {
            return response()->json([
                'type' => 'success',
                'tmiPackages' => $tmiPremium,
                'message' => 'Calculation error.'
            ], 500);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        Session::put('tmiIds', [36,37,38]);
//        Session::forget('tmiIds');
//        return ('hello');
        $this->TravelDatas();
        $data = $this->variable;
        $data['customerData'] = [];
        $ids = Session::get('tmiIds');
        if ($ids) {
            $data['customerData'] = TravelMedicalInsurance::whereIn('id', $ids)->get();
        }
//        dd($data);
        return view('Backend.Travel.travel', $data);
    }

    public function clearAll()
    {
        $ids = Session::get('tmiIds');
        if ($ids) {
            TravelMedicalInsurance::whereIn('id', $ids)->delete();
            Session::forget('tmiIds');
            return redirect()->back();
        }
        return redirect()->back();
    }

    public function customerDelete($id)
    {
        try {
            $customer = TravelMedicalInsurance::findOrFail($id);
            $customer->delete();
            return response()->json([
                'status' => 'ok',
                'message' => 'Customer is deleted successfully.'
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Customer can not be deleted.'
            ], 422);
        }
    }

    public function store(Request $request)
    {
//        return $request;
//        $this->validate(
//            $request,
//            [
//                'COVERTYPE' => 'required',
//                'PLAN' => 'required',
//                'PACKAGE' => 'required',
//                'DOB' => 'required',
//                'DTFROM' => 'required',
//                'DTTO' => 'required',
//                'PASSPORTNO' => 'required',
//                'VISITPLACE' => 'required',
//                'CONTACTNO' => 'exclude_if:phone_number,null|digits_between:10,13',
//            ],
//            [
//                'COVERTYPE.required' => 'Category Cannot Be Empty!!',
//                'PLAN.required' => 'Plan Cannot Be Empty!!',
//                'PACKAGE.required' => 'Package Cannot Be Empty!!',
//                'DOB.required' => 'Date Of Birth Cannot Be Empty!!',
//                'DTFROM.required' => 'From Date Cannot Be Empty!!',
//                'DTTO.required' => 'To Date Cannot Be Empty!!',
//                'PASSPORTNO.required' => 'Passport Number Cannot Be Empty!!',
//                'VISITPLACE.required' => 'Visiting Place Cannot Be Empty!!',
//                'CONTACTNO.required' => 'Mobile number Cannot Be Empty!!',
//            ]
//        );
        if (Session::get('tmiIds')) {
            $typeIds = TravelMedicalInsurance::whereIn('id', Session::get('tmiIds'))->pluck('COVERTYPE');
            if (in_array($request->COVERTYPE, $typeIds->toArray(), FALSE)) {
                return response()->json([
                    'type' => 'error',
                    'message' => "Cannot add users with different categories !!",
                ], 500);
            }
        }
        if ($request->COVERTYPE != 2 && Session::get('tmiIds') && count(Session::get('tmiIds')) >= 1) {
            return response()->json([
                'type' => 'error',
                'message' => "Cannot Add more user in Individual OR Student !!",
            ], 500);
        }
//        return $request;
        try {
            if ($request->file('attachment_image')) {
                $path = $request->attachment_image->store('TMI', 'public');
                $imgFile = Storage::disk('public')->url($path);
                $request->merge([
                    'image' => $imgFile,
                    'VISITPLACE' => json_encode($request->VISITPLACE),
                ]);
            }
            $travelvData = TravelMedicalInsurance::create($request->all());
            $ids = [$travelvData->id];
            if (!Session::get('tmiIds')) {
                Session::put('tmiIds', $ids);
            } else {
                Session::push('tmiIds', $travelvData->id);
            }
            $ids = Session::get('tmiIds');
            $data['customerData'] = [];
            if ($ids) {
                $data['customerData'] = TravelMedicalInsurance::whereIn('id', $ids)->get();
            }
            return view('Backend.Travel.customerData', $data);
//        return response()->json([
//            'type' => 'success',
//            'message' => 'Successfully Added !',
//            'data' => $data ?? '',
//        ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'type' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function packageSelect(Request $request)
    {
        try {
//        return $request->country[0];
            $country = Country::where('name', $request->country[0])->first();
            $plan = $country->plan_id;

            return response()->json([
                'type' => 'success',
                'message' => 'Package Returned!',
                'plan_id' => $plan ?? '',
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'type' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function customerSelect(Request $request)
    {
//        return $request;
        Session::forget('travelDatas');
        Session::put('travelDatas', $request->except('_token'));
        $kyc = new KYCController();
        $kyc->ajaxData();
        $data = $kyc->variable;
        $data['formData'] = $request->only('TOTALNETPREMIUM', 'STAMP', 'TVPREM', 'TVAT', 'TVATAMT', 'TOTALPREM');
        $data['makePolicy'] = 1;
        $data['customers'] = KYC::where('customer_id', '!=', '')->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
        return view('Backend.Travel.customerSelect', $data);
    }

    public function customerPayment(Request $request)
    {
//        return $request;
        Session::put('customer_id', $request->customer_id);
        try {
            $kyc = KYC::where('customer_id', $request->customer_id)->first();
            if ($kyc) {
                $imePay = new ImePayController();
                $dt['module'] = base64_encode(GeneralSetting::where('key', 'ime_module')->first()->value);
                $dt['imeUser'] = base64_encode(GeneralSetting::where('key', 'ime_pay_username')->first()->value . ':' . GeneralSetting::where('key', 'ime_pay_user_password')->first()->value);
                $dt['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
                $dt['merchantName'] = GeneralSetting::where('key', 'ime_merchant_name')->first()->value;
                $dt['pricePayable'] = "10.00";
                $imePay->getPayment($request->all(), $dt);
                $data = $imePay->variable;
                $calcData = '';
//             $storeDta = [
//            'customer_id' => $$reqData->customer_id,
//            'payment_ref_id' => $data['paymentInfo']->RefId ?? '',
//            'payment_token_details' => json_encode($response) ?? '',
//        ];
                $data['RespUrl'] = route('travel.calculator.imepay.success');
                $data['CancelUrl'] = route('travel.calculator.imepay.cancil');
//                dd($data);
                return view('Backend.Travel.payment', $data);
            } else {
                return response()->json([
//                    'type' => 'error',
                    'message' => 'Kyc not Added ! Please Add the Kyc for the customer.'
                ], 200);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function MakePolicy(Request $request)
    {
        try {
//        dd($request);
            $data['customerData'] = [];
            $output = [];
            $ids = Session::get('tmiIds');
            $travelDt = Session::get('travelDatas');
//        dd($travelDt);
            if ($ids) {
                $customerData = TravelMedicalInsurance::whereIn('id', $ids)->get();
//            dd($customerData);
                ini_set('serialize_precision', '-1');
                $datas = array(
                    "KYCID" => customerKyc(Session::get('customer_id'))->KYCID ?? '0000',
                    "BRANCHID" => $travelDt['BRANCHID'],
                    "DEPTID" => $travelDt['DEPTID'],
                    "CLASSID" => $travelDt['CLASSID'],
                    "STAMPDUTY" => $travelDt['STAMP'],
                    "VATAMOUNT" => $travelDt['TVATAMT'],
                    "VATRATE" => "13",
                    "TOTALPAYABLEBYCLIENT" => floatval($travelDt['TOTALPREM']),
                    "MERCHANT_TRANS_NO" => $travelDt['reference_id'],
                    "MERCHANT_CODE" => "igi@imepay",
                    "MERCHANT_TRANS_DATE" => Carbon::now()->format('d-M-Y'),
                    "token" => $this->makeSignature($travelDt),
                );
//                dd($datas);
                $cstmr = [];
                foreach ($customerData as $dta) {
                    $customers = array(
                        "INSURED" => customerKyc(Session::get('customer_id'))->INSUREDNAME_ENG ?? 'n/a',
                        "INSUREDADDRESS" => customerKyc(Session::get('customer_id'))->ADDRESS ?? 'n/a',
                        "PASSPORTNO" => $dta->PASSPORTNO,
                        "DOB" => $dta->DOB,
                        "TOTALDOLLARPREMIUM" => $dta->TOTALDOLLARPREMIUM,
                        "DOLLARRATE" => $dta->DOLLARRATE,
                        "CURRENCY" => $dta->CURRENCY ?? "USD",
                        "TOTALNC" => $dta->TOTALNC,
                        "DTFROM" => $dta->DTFROM,
                        "DTTO" => $dta->DTTO,
                        "PERIODOFINSURANCE" => $dta->PERIODOFINSURANCE,
                        "VISITPLACE" => $dta->VISITPLACE && is_array($dta->VISITPLACE) ? implode(', ', json_decode($dta->VISITPLACE)) : $dta->VISITPLACE,
                        "CONTACTNO" => $dta->CONTACTNO,
                        "OCCUPATION" => $dta->OCCUPATION,
                        "REMARKS" => $dta->REMARKS,
                        "CAREOF" => $dta->CAREOF ?? "care of",
                        "AGE" => $dta->AGE,
                        "ISDEPENDENT" => $dta->ISDEPENDENT,
                        "RELATION" => $dta->REMARKS,
                        "COVIDCHARGEPREMIUM" => $dta->COVIDCHARGEPREMIUM ?? '0.00',
                        "COVIDRATE" => $dta->COVIDRATE ?? '0.00',
                        "DIRECTDISCOUNTRATE" => $dta->DIRECTDISCOUNTRATE ?? '0.00',
                        "DIRECTDISCOUNT" => $dta->DIRECTDISCOUNT ?? '0.00',
                        "HASDIRECTDISCOUNT" => $dta->HASDIRECTDISCOUNT ?? '0.00',
                        "COVERTYPE" => $dta->COVERTYPE,
                        "COVERTYPECODE" => $dta->COVER_TYPE_CODE,
                        "PLAN" => $dta->PLAN,
                        "PLANCODE" => $dta->PLAN_CODE,
                        "PACKAGE" => $dta->PACKAGE,
                        "PACKAGECODE" => $dta->PACKAGE_CODE,
                        "ISANNUALTRIP" => $dta->ISANNUALTRIP,
                        "ISLINKCODEENABLE" => 1
                    );
                    array_push($cstmr, $customers);
                }
            }
            $datas['tmiList'] = $cstmr;

//            dd($datas);
//            dd(json_encode($datas, JSON_PRESERVE_ZERO_FRACTION));
            $curl = curl_init();
            $data['merchantCode'] = GeneralSetting::where('key', 'ime_merchant_code')->first()->value;
            $url = GeneralSetting::where('key', 'api_url')->first()->value;
            $apiUser = base64_encode(GeneralSetting::where('key', 'api_username')->first()->value . ':' . GeneralSetting::where('key', 'api_user_password')->first()->value);

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url . "/api/Policy/MakeTMIPolicy",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($datas, JSON_PRESERVE_ZERO_FRACTION),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $apiUser,
                ),
            ));
//dd($curl);
            $output = json_decode(curl_exec($curl));
//            dd($output);
            $data['output'] = $output ?? ($output->message ?? 'something went wrong');
            $data['data'] = $output->data ?? ($output->message ?? 'something went wrong');
//            dd($data);
            curl_close($curl);

            $upload = [
                "customer_list_id" => Session::get('tmiIds') ? json_encode(Session::get('tmiIds')) : null,
                "proformano" => $data['data'][0]->proformano ?? '0000',
                "insured" => $data['data'][0]->insured ?? '0000',
                "className" => $data['data'][0]->className ?? '0000',
                "Suminsured" => $data['data'][0]->Suminsured ?? '0000',
                "tpPremium" => $data['data'][0]->tpPremium ?? '0000',
                "documentNo" => $data['data'][0]->documentNo ?? '0000',
                "receiptNo" => $data['data'][0]->receiptNo ?? '0000',
                "receiptDate" => $data['data'][0]->receiptDate ?? '0000',
                "effectiveDate" => $data['data'][0]->effectiveDate ?? '0000',
                "expiryDate" => $data['data'][0]->expiryDate ?? '0000',
                "policyNo" => $data['data'][0]->policyNo ?? '0000',
                "TransactionStatus" => $data['data'][0]->TransactionStatus ?? '0000',
                "AcceptanceNo" => $data['data'][0]->AcceptanceNo ?? '0000',
                "user_id" => Auth::user()->id,
                "customer_id" => Session::get('customer_id'),
                "KYCID" => customerKyc(Session::get('customer_id'))->KYCID ?? '0000',
                "reference_number" => $travelDt['reference_id'] ?? '0',
                "status" => 1,
                "payment_status" => 1
            ];
//            dd($upload);
            $data['newCstPolicy'] = TravelMedicalPolicy::create($upload);
            Session::forget(['tmiIds', 'travelDatas', 'customer_id']);
//            dd($data);
            return redirect()->route('travel.calculator.policy.done', $data);
//            return view('Backend.Travel.travel-policy', $data);
        } catch (\Exception $e) {
//            dd($e->getMessage());
//            return $e->getMessage();
            $data['data'] = json_encode($output) ?? $e->getMessage();
            $data['output'] = $e->getMessage();
            return redirect()->route('travel.calculator.policy.done', $data);
        }
    }

    private function makeSignature($data)
    {
//        dd($data);
        try {
            $apiUser = GeneralSetting::where('key', 'api_username')->first()->value;
            $apiPassword = GeneralSetting::where('key', 'api_user_password')->first()->value;
            $secret_key = GeneralSetting::where('key', 'api_secret_key')->first()->value;

            $dta = array(
                "A_USERID" => $apiUser,
                "B_PASSWORD" => $apiPassword,
                "C_KYCID" => intVal(customerKyc(Session::get('customer_id'))->KYCID ?? '0000'),
                "D_CLASSID" => intVal($data['CLASSID']),
                "E_AMOUNT" => floatval($data['TOTALPREM']),
            );
//            dd(json_encode($dta, JSON_PRESERVE_ZERO_FRACTION));
            return hash_hmac('sha512', json_encode($dta, JSON_PRESERVE_ZERO_FRACTION), $secret_key);
//            Signature in header
        } catch (Exception $e) {
            return ($e->getMessage());
        }
    }

    public function policyDone(Request $request)
    {
        try {
//            dd($request->data);
            $data['output'] = $request->output;
            $data['data'] = $request->data;
            return view('Backend.Travel.travel-policy', $data);
        } catch (\Exception $e) {
//            dd($e->getMessage());
            $data['output'] = $request->output;
            $data['data'] = $request->data;
            return view('Backend.Travel.travel-policy', $data);
        }
    }

    public function loadPdf(Request $request)
    {
//        return $request;
//        Log::channel('errors')->warning('Something could be going wrong.'.$request);

        $pdfUrl = GeneralSetting::where('key', 'api_url')->first()->value;
//        return $pdfUrl;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $pdfUrl . "/api/Reports/PreviewDocument?docid=" . $request->docid,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $pdf = json_decode(curl_exec($curl));
//        dd($pdf);

        curl_close($curl);
        if ($pdf != null && $pdf->filePath != "Error") {
            $filename = 'NB-' . $request->proformano . '.pdf';
            $tempImage = tempnam(sys_get_temp_dir(), $filename);
            copy($pdfUrl . $pdf->filePath, $tempImage);

            return response()->download($tempImage, $filename);
        }
        return redirect()->back()->withErrors('Cannot Download PDF.');
//        return response('Cannot Download PDF');
//        return Redirect::to($pdfUrl.$pdf->filePath);
    }

    public function viewTmiPolicy(Request $request)
    {
        if ($request->from_date && $request->to_date) {
            $this->validate($request, [
                'from_date' => 'required|date|before_or_equal:to_date',
                'to_date' => 'required|date',
            ]);
            $from = $request->from_date;
            $to = $request->to_date;
//            dd($request->all());
            $policies = TravelMedicalPolicy::whereDate('created_at', '>=', $from . ' 00:00:00')->whereDate('created_at', '<=', $to . ' 00:00:00');
//            dd($policies->get());
        } else {
            $policies = TravelMedicalPolicy::orderByDesc('id');
        }
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) {
            $data['policies'] = $policies->latest()->get();
        } else {
            $data['policies'] = $policies->where('user_id', Auth::user()->id)->orderByDesc('id')->get();
        }
        return view('Backend.Travel.policy-list', $data);
    }

}
