<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UploadChooseFile;
use Illuminate\Support\Str;
use App\Mail\CompareResult;
use Illuminate\Support\Facades\Mail;
use App\Mail\Upload;
use File;


class UploadChooseFileController extends Controller
{
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data['uploads'] = UploadChooseFile::orderBy('created_at','desc')->get();
        return view('Backend.Upload.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data = ['foo' => 'bar'];
        // Mail::send('emails.welcome', $data, function($message)
        // {
        //     $message->to('shrebiju06@gmail.com', 'Biju Shrestha')->subject('Welcome!');
        // });

        // $Customer = new UploadChooseFile();
        // Mail::to($Customer->email)->send(new UploadChooseFile());
        return view('Backend.Upload.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadChooseFile  $UploadChooseFile)
    {
     
      $data =[
          'title' => $request->title,
          'file' => $request->file('attachment')
      ];
        $to = 'shrebiju06@gmail.com';
        \Mail::to($to)->send(new \App\Mail\Upload($data));
        // $request->validate([
        //     // 'email' => 'required|email|unique:users,email',
        //     'attachment' => 'required|mimes:pdf,xlx,csv|max:2048',
        // ]); 
        // $file = $request->attachment;
        // $data['clientEmail'] = "shrebiju06@gmail.com";
        // $title =$request->title;
        // $destinationpath = 'uploads/products/';
        // $extension = strrchr($request->file('attachment')->getClientOriginalName(), '.');
        // $new_file_name = Str::random(15).'_'. time();
        // $uploaded_file = $file->move($destinationpath, $new_file_name . $extension);
        // $details = $request->all();
        // unset($details['attachment']);
        // $UploadChooseFile->fill($details)->forceFill([ 
        //     'attachment' => $new_file_name . $extension,
        // ])->save();
        // Mail::to($data['clientEmail'])
        //                ->send(new Upload($file,$data));
        $data=new UploadChooseFile();
        $attachment=$request->attachment;
        $filename=time().'.'.$attachment->getClientOriginalExtension();
        $request->attachment->move('uploads/products/',$filename);
        $data->attachment=$filename;
        $data->title=$request->title;
        $data->save();
        return redirect()->back()->withSuccessMessage('Email Link created successfully!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $supoertvideo = UploadChooseFile::findorFail($id);
        return view('Backend.Upload.view')->with('supoertvideo', $supoertvideo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $link = UploadChooseFile::findOrFail($id);
          
            $link->delete();
                return response()->json([
                    'type'=>'success',
                    'testimonial' => $link,
                    'message'=>'Upload Form deleted successfully.'
                ], 200);
            }
            catch (\Exception $e) {
    //            return $e->getMessage();
                return response()->json([
                    'type'=>'error',
                    'testimonial' => $link,
                    'message'=>'Upload Form can not be deleted.'
                ], 422);
            }
        
    }
}
