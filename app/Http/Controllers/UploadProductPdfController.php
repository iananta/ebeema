<?php

namespace App\Http\Controllers;

use App\Constants\ProductType;
use App\Models\Company;
use App\Models\Product;
use App\Models\SupoortDetail;

use App\Models\UploadProductPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UploadProductPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['uploads'] = UploadProductPdf::orderBy('created_at','desc')->get();
        $data['companies'] = Company::select('id', 'name')->where('is_active', '1')->orderBy('name', 'asc')->get();
        $data['products'] = Product::query()->select( 'name', 'id')->get();
        $data['support'] = SupoortDetail::orderBy('created_at','desc')->get();
        $data['types'] = collect(ProductType::types)->map(function ($type, $index) {
            return [
                'id' => $index,
                'value' => $type,
            ];
        })->values();

        return view('Backend.uploadPM.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UploadProductPdf  $uploadProductPdf)
    {
        $request->validate([
            'title' => 'required',
            'code' => 'required',
            'company_id' => 'required|exists:company,id',
            'product_id' => 'required|exists:product,id',
            'type' => 'required|in:'.implode(',', array_keys(ProductType::types)),
            'attachment' => 'required|file',
        ]);

        $file = $request->file('attachment');
        $destinationpath = 'uploads/products/';
        $extension = strrchr($request->file('attachment')->getClientOriginalName(), '.');
        $new_file_name = Str::random(15).'_'. time();
        $uploaded_file = $file->move($destinationpath, $new_file_name . $extension);

        $details = $request->all();
        unset($details['attachment']);
        $uploadProductPdf->fill($details)->forceFill([
            'attachment' => $new_file_name . $extension,
            'product_id' => $details['product_id']
        ])->save();
        return redirect()->back();
    }

    public function form()
    {
        $data['uploads'] = SupoortDetail::orderBy('created_at','desc')->get();
        return view('Backend.supply.index',$data);
    }

    public function support()
    {
        return view('Backend.supply.create');
    }

    public function supply(Request $request)
    {
        $data=new SupoortDetail();
		$data->name=$request->name;
		$data->phone_number=$request->phone_number;
        $data->save();
        return redirect()->back()->withSuccessMessage('Video created successfully!');

    }
    public function replace($id)
    {
        $supoertdetail = SupoortDetail::findorFail($id);

        //$blogcates = Blogcate::all();
        return view('Backend.supply.edit')->with('supoertdetail', $supoertdetail);

    }
    public function modernize(Request $request, $id)
    {
        // $this->validate($request, array(
        //     'name' => 'required',
        //     'phone_number' => 'required',
        // ));
        $data['name'] = $request->name;
        $data['phone_number'] = $request->phone_number;
        $supoertdetail = SupoortDetail::whereId($id)->update($data);
        return redirect()->route('admin.support.create', $supoertdetail)->withSuccessMessage('Support updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UploadProductPdf  $uploadProductPdf
     * @return \Illuminate\Http\Response
     */
    public function show(UploadProductPdf $uploadProductPdf)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UploadProductPdf  $uploadProductPdf
     * @return \Illuminate\Http\Response
     */
    public function edit(UploadProductPdf $uploadProductPdf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UploadProductPdf  $uploadProductPdf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UploadProductPdf $uploadProductPdf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UploadProductPdf  $uploadProductPdf
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upload = UploadProductPdf::findOrfail($id);
        $upload->delete();

        if ($upload) {
            return response()->json([
                'type' => 'success',
                'message' => 'File uploaded in product management is deleted successfully.'
            ], 200);
        }

        return response()->json([
            'type' => 'error',
            'message' => 'File uploaded in product management can not be deleted.'
        ], 422);
    }

    public function download(int $uploadPdfId, UploadProductPdf $uploadProductPdf)
    {
        $uploadPdf = $uploadProductPdf->findOrNew($uploadPdfId);
        $attachment = $uploadPdf->attachment;
        return response()->download('uploads/products/'. $attachment);
    }

}
