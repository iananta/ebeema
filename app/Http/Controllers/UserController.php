<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use App\Models\UserAgent;
use App\Notifications\UserActivated;
use App\Notifications\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Mockery\Exception;
use App\Models\City;
use App\Models\District;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (control('create-user')) {
            $data['districts']=$this->districts();
            $data['provinces']=$this->provinces();
            $data['roles'] = Role::all();
            $data['users'] = User::all();
            return view('Backend.User.index', $data);
        } else {
            return view('layouts.backend.denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        // return view('configuration::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @param $citizen_front
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'username' => 'required',
                'password' => 'required|min:6',
                'email' => 'required|email|unique:users,email',
                'phone_number' => 'exclude_if:phone_number,null|digits_between:10,13|unique:users,phone_number'
            ],
            [
                'username.required' => 'Name Cannot Be Empty!!',
                'password.required' => 'Password Cannot Be Empty!!',
                'email.required' => 'Email Cannot Be Empty!!',
            ]
        );

        $data['username'] = $request->username;
        $data['designation'] = $request->designation;
        $data['email'] = $request->email;
        $data['phone_number'] = $request->phone_number;
        $data['is_active'] = $request->is_active;
        $data['ZONEID'] = $request->ZONEID;
        $data['DISTRICTID'] = $request->DISTRICTID;
        $data['role_id'] = $request->role_id;
        $data['password'] = Hash::make($request->password);
        $data['address'] = $request->address;
        $data['email_unofficial'] = $request->email_unofficial;
        $data['phone_number_unofficial'] = $request->phone_number_unofficial;

        //        return $data;
        $user = User::Create($data);

        if ($user) {
            return response()->json([
                'status' => 'ok',
                'user' => $user,
                'message' => 'User is added successfully.'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'User can not be added.'
            ], 422);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return response()->json([
            'status' => 'ok',
            'user' => $user
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        $districts=$this->districts();
        $provinces=$this->provinces();
        return view('Backend.User.edit',compact('districts','provinces'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function modify(Request $request, $id)
    {

        // $district=District::where('district_name',$request->id);
        try {
            $this->validate(
                $request,
                [
                    'username' => 'required',
                    'email' => 'required',
                    'phone_number' => 'exclude_if:phone_number,null|digits_between:8,13'
                ],
                [
                    'username.required' => 'Name Cannot Be Empty!!',
                    'email.required' => 'Email Cannot Be Empty!!',
                ]
            );
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            $data['designation'] = $request->designation;
            $data['address'] = $request->address;
            $data['phone_number'] = $request->phone_number;
            $data['province'] = $request->province;
            $data['district'] = $request->district;
            $data['email_unofficial'] = $request->email_unofficial;
            $data['phone_number_unofficial'] = $request->phone_number_unofficial;
            $profile_img = $request->file('image_icon');
            $front_citizen = $request->file('citizen_front');
            $back_citizen = $request->file('citizen_back');
            $destinationpath = 'uploads/user/';
            if ($profile_img) {
                $extension = strrchr($profile_img->getClientOriginalName(), '.');
                $new_file_name = 'user' . str_replace(' ', '_', $request->name) . '_profile' . time();
                $profile_img = $profile_img->move($destinationpath, $new_file_name . $extension);
                $data['image_icon'] = isset($profile_img) ? $new_file_name . $extension : null;
            }
//            dd($data);
            if ($front_citizen) {
                $extension = strrchr($front_citizen->getClientOriginalName(), '.');
                $new_file_name = str_replace(' ', '_', $request->name) . '_front' . time();
                $front_image = $front_citizen->move($destinationpath, $new_file_name . $extension);
                $data['citizen_front'] = isset($front_image) ? $new_file_name . $extension : null;
            }
            if ($back_citizen) {
                $extension = strrchr($back_citizen->getClientOriginalName(), '.');
                $new_file_name = str_replace(' ', '_', $request->name) . '_back' . time();
                $back_image = $back_citizen->move($destinationpath, $new_file_name . $extension);
                $data['citizen_back'] = isset($back_image) ? $new_file_name . $extension : null;
            }
            if ($request->password) {
                $data['password'] = Hash::make($request->password);
            }

            $user = User::where('id', $id)->update($data);

            if ($user) {
                return redirect()->back()->withSuccessMessage('User updated Successfully');
            }
            return redirect()->back()->withErrors('Something went wrong');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function update(Request $request, $id)
    {

        $this->validate(
            $request,
            [
                'username' => 'required',
                'email' => 'required',
                'role_id' => 'required',
                'phone_number' => 'exclude_if:phone_number,null|digits_between:10,13'
            ],
            [
                'username.required' => 'Name Cannot Be Empty!!',
                'email.required' => 'Email Cannot Be Empty!!',
                'role_id.required' => 'Role Cannot Be Empty!!',
            ]
        );
        try {
            $data['phone_number'] = $request->phone_number;
            $data['username'] = $request->username;
            $data['designation'] = $request->designation;
            $data['email'] = $request->email;
            $data['is_active'] = $request->is_active;
            $data['role_id'] = $request->role_id;

            $data['email_unofficial'] = $request->email_unofficial;
            $data['phone_number_unofficial'] = $request->phone_number_unofficial;
            if ($request->password) {
                $data['password'] = Hash::make($request->password);
            }
            $user = User::where('id', $id)->update($data);
            if ($user) {
                return response()->json([
                    'status' => 'ok',
                    'user' => $user,
                    'message' => 'User is updated successfully.'
                ], 200);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'User can not be updated.'
                ], 422);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json([
                'status' => 'ok',
                'message' => 'User is deleted successfully.'
            ], 200);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'User can not be deleted.'
            ], 422);
        }
    }

    /**
     * Change status of the specified user from storage.
     *
     * @param int $id
     * @return string
     */
    public function changeStatus($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->is_active == '0' ? $user->is_active = '1' : $user->is_active = '0';
            $user->save();
            $message = ($user->is_active == '1') ? "User is activated successfully." : "User is deactivated successfully.";
//            dd($user);
            Notification::send(Auth::user(), new UserActivated($user));

            return response()->json([
                'status' => 'ok',
                'user' => $user,
                'message' => $message
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'user' => null,
                'message' => $e->getMessage()
            ], 500);
//            return $e->getMessage();
        }
    }

    public function users(Request $request)
    {
        return response()->json([
            'status' => 'ok',
            'users' => User::all(),
        ], 200);
    }

    public function manuallogin($id)
    {
        if (control('create-user')) {

            $user = User::find($id);

            Auth::login($user);
            return redirect('/dashboard');
        } else {
            return view('layouts.backend.denied');
        }
    }

    public function downloadExcel(Request $request)
    {
        return Excel::download(new UsersExport, 'user-list.xlsx');
    }

    public function showCompanies($id)
    {
        if (control('create-user')) {

            $data['user'] = User::findOrFail($id);
            $usedCompanies = User::findOrFail($id)->userAgents->pluck('company_id');
            $data['companies'] = Company::whereNotIn('id', $usedCompanies)->latest()->get();
            return view('Backend.User.companyDetails', $data);
        } else {
            return view('layouts.backend.denied');
        }
    }
    protected function districts()
    {
        return \DB::table('districts')->get();
    }

    protected function provinces()
    {
        return \DB::table('provinces')->get();
    }


    public function storeCompanies($id, Request $request)
    {
        if (control('create-user')) {
//        return $request;
            UserAgent::where('user_id',$id)->delete();
            $user = User::findOrFail($id);
            if ($request->company_id && $user) {
                foreach ($request->company_id as $key => $comp) {
                    if ($comp != null) {
                        $ref = $request->liscence_number[$key] ?? '';
                        $agent = UserAgent::create([
                            'user_id' => $id,
                            'company_id' => $comp,
                            'liscence_number' => $ref
                        ]);
                    }
                }
            }
            return redirect()->back()->withSuccessMessage('SuccessFully Updated');
        } else {
            return view('layouts.backend.denied');
        }
    }
}
