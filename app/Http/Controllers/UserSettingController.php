<?php

namespace App\Http\Controllers;

use App\Models\GeneralSetting;
use App\Models\UserSettings;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $settings = UserSettings::all();
        return view('Backend.UserSetting.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('Backend.UserSetting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'key' => 'required|unique:user_settings',
            'value' => 'required',
            'type' => 'required',
        ]);

        UserSettings::create([
            'key' => $request->key,
            'value' => $request->value,
            'type' => $request->type,
        ]);
//        return $request;
        return redirect()->route('page-setting.index')->withSuccessMessage('Key created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function edit($id)
    {
        $setting = UserSettings::findOrFail($id);

        return view('Backend.UserSetting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'key' => 'required',
            'value' => 'required',
            'type' => 'required',
        ]);

        $setting = UserSettings::findOrFail($id);
        $setting->update([
            'key' => $request->key,
            'value' => $request->value,
            'type' => $request->type,
        ]);
        return redirect()->route('page-setting.index')->withSuccessMessage('Key updated successfully!');;
    }
}
