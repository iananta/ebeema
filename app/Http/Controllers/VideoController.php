<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use DB;
use Auth;
use App\DataTables\VideoDataTable;
use App\DataTables\UserNotificationDataTable;
class VideoController extends Controller
{
    public function userlogs(VideoDataTable $dataTable)
    {
        return $dataTable->render('Backend.video.index');
    }

    public function index(VideoDataTable $dataTable)
    {
        return $dataTable->render('Backend.video.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=new Video();
		$file=$request->file;
	    $filename=time().'.'.$file->getClientOriginalExtension();
		$request->file->move('assets',$filename);
     
		$data->file=$filename;
		$data->title=$request->title;
        $data->save();
        return redirect()->back()->withSuccessMessage('Video created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supportvideo = Video::findorFail($id);
        return view('Backend.video.view')->with('supportvideo', $supportvideo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tape = Video::findOrFail($id);
          
            $tape->delete();
                return response()->json([
                    'type'=>'success',
                    'testimonial' => $tape,
                    'message'=>'Video deleted successfully.'
                ], 200);
            }
            catch (\Exception $e) {
    //            return $e->getMessage();
                return response()->json([
                    'type'=>'error',
                    'testimonial' => $tape,
                    'message'=>'Video  can not be deleted.'
                ], 422);
            }
        
    }
}