<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\UserActivityLog;
use Auth;
use Illuminate\Support\Facades\Log;
use Session;
use App\Exceptions\Userlog;
class Activityuserlog
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request,Closure $next)
    {
        $response = $next($request);
        if (!in_array($request->path(),$this->without()) && (Auth::check() && $request->method() == 'PATCH' || $request->method() == "PUT" || $request->method() == 'POST' || $request->method() == 'DELETE')) {
            if($request->ajax()){
                $message=json_decode($response->getContent(),true) ?? '';
            }
            $message=$message['message'] ?? $request->session()->get('success_message');
            $error =$request->session()->get('error') ?? $response->exception ?? '';
            $data=$this->getData($request,$message,$error);
            UserActivityLog::create($data);
        }elseif(isset($response->exception) && $response->exception){
            $data=$this->getData($request,NULL,$response->exception);
            UserActivityLog::create($data);
        }
        return $response; 
    }
    public function getData($request,$message=null,$error=null){
        $data = [
                'user_id' => Auth::user()->id ?? '',
                'path' => substr($request->path(), 0, 255),
                'method' => $request->method(),
                'ip' => $request->getClientIp(),
                'user_agent' => $request->header('User-Agent'),
                'input' => json_encode($request->input()),
                'output' =>$error ?? '',
                'RESPONSE' => $message ?? '',
        ];
        return $data;
    }

    public function without(){
        $prefix='admin';
        return[
            $prefix.'/lead/getProduct',
            $prefix.'/lead/getCity',
            $prefix.'/lead/getProduct',
            $prefix.'/getspecificLead',
            $prefix.'/lead/findUser',
            $prefix.'/get-premium-collection-user'
        ];
    }
}
