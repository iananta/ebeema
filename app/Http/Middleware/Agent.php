<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Agent
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next, $categ = null)
    {
        $companyID = count(Auth::user()->userAgents) ? Auth::user()->userAgents->pluck('company_id') : '';
        if ($companyID && Auth::user()->role_id != 1 && Auth::user()->role_id != 2 && Auth::user()->role_id != 7 && Auth::user()->role_id != 6) {
            $cat = DB::table('companies')->whereIn('id', $companyID)->pluck('type');
            $cat = $cat->toArray();
        } else {
            $cat = array('all');
        }
        if (in_array('NonLife', $cat) || in_array('all', $cat) || in_array('Life', $cat)) {
            return $next($request);
        }
        return response()->view('layouts.backend.denied');
    }

//
//    public function handle($request, Closure $next, $categ = null)
//    {
//        $companyID = count(Auth::user()->userAgents) ? Auth::user()->userAgents->pluck('company_id') : '';
//        if ($companyID && Auth::user()->role_id != 1 && Auth::user()->role_id != 2 && Auth::user()->role_id != 7 && Auth::user()->role_id != 6) {
//            $cat = strtoupper(DB::table('companies')->whereIn('id', $companyID)->pluck('type'));
//            $cat = json_decode($cat);
//        } else {
//            $cat = array('ALL');
//        }
////        if (in_array('NONLIFE', $cat) || in_array('ALL', $cat) || in_array('LIFE', $cat)) {
//        if (in_array(strtoupper($categ), $cat) || in_array('ALL', $cat)) {
//            return $next($request);
//        }
//        return response()->view('layouts.backend.denied');
//    }
}
