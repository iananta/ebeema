<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $AUTH_USER = \App\Models\GeneralSetting::where('key', 'auth_user')->first()->value ?? 'ebeema';
        $AUTH_PASS = \App\Models\GeneralSetting::where('key', 'auth_password')->first()->value ?? 'Ebeema@1234##';
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            return Response(["response_code" => 401, 'success' => false, 'message' => "You are not authorized."], 401);
            exit;
        }
        return $next($request);

        return response('You are not authorized!', 401, ['WWW-Authenticate' => 'Basic']);
    }
}
