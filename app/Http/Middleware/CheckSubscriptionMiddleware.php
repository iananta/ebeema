<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Subscriptionusage;
class CheckSubscriptionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(control('subscription-plan ')){
            $user=auth()->user();
            if($user->subscriptionusage){
                $subscrition=$user->subscriptionusage;
                $paidDate=$subscrition->paid_date;
                $endDate=$subscrition->end_date;
                $currentDate=date('Y-m-d');
                if($currentDate > $endDate){
                     return redirect()->route('subscription.plan')->with('message','Your Subscription period has been expired');
                }
                return $next($request);
            }
            else{
                $userRegisterDate= date_create(date('Y-m-d', strtotime($user->created_at)));
                $currentDate=date_create(date('Y-m-d'));
                $days=date_diff($userRegisterDate, $currentDate)->days;
                if($days > 365){
                    return redirect()->route('subscription.plan')->with('message','Your trial period has been expired');
                }
                return $next($request);
            }
        }
        return $next($request);
    }
}
