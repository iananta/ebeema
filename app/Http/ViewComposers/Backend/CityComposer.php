<?php

namespace App\Http\ViewComposers\Backend;

use Illuminate\View\View;
use App\Models\City;


class CityComposer
{
    public function compose(View $view)
    {
        $cities=City::all();
        $view->with('cities',$cities);
    }
}
