<?php
namespace App\Http\ViewComposers\Backend;

use Illuminate\View\View;
use App\Models\Company;
class CompanyComposer
{

    public function compose(View $view)
    {
        $companies=Company::where(['is_active'=>1,'type'=>'Life'])->get(['id','name']);
        $view->with('companies',$companies);
    }
}
