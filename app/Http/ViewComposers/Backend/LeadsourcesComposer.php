<?php

namespace App\Http\ViewComposers\Backend;

use Illuminate\View\View;
use App\Models\LeadSource;


class LeadsourcesComposer
{

    public function compose(View $view)
    {
        $leadsource=LeadSource::where('is_active', '=', '1')->get();
        $view->with('lead_sources',$leadsource);
    }
}
