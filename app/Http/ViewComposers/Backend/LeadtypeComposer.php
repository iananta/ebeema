<?php

namespace App\Http\ViewComposers\Backend;

use Illuminate\View\View;
use App\Models\LeadType;


class LeadtypeComposer
{

    public function compose(View $view)
    {
        $leadtypes=LeadType::where('is_active', '=', '1')->get();
        $view->with('lead_types',$leadtypes);
    }
}
