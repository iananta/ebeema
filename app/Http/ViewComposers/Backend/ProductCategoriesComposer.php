<?php

namespace App\Http\ViewComposers\Backend;

use Illuminate\View\View;
use App\Models\ProductCategory;


class ProductCategoriesComposer
{

    public function compose(View $view)
    {
        $productcategories=ProductCategory::where('status', '1')->get();
        $view->with('policy_sub_categories',$productcategories);
    }
}
