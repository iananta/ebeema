<?php

namespace App\Http\ViewComposers\Backend;

use Illuminate\View\View;
class RoleComposer
{

    public function compose(View $view)
    {
        $roles=\DB::table('roles')->whereNotIn('id',['1','2','7'])->get(['name','id']);
        $view->with('roles',$roles);
    }
}
