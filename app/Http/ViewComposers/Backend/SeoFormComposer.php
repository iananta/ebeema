<?php

namespace App\Http\ViewComposers\Backend;
use Illuminate\View\View;
use App\Models\SeoForm;
class SeoFormComposer
{

    public function compose(View $view)
    {
        $seoForms=SeoForm::get();
        $view->with('seoForms',$seoForms);
    }
}
