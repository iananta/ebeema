<?php

namespace App\Http\ViewComposers\Backend;

use App\Models\User;
use Illuminate\View\View;

class UserListComposer
{
    public function compose(View $view)
    {
        if(auth()->user()->role_id == 1){
            $users= User::whereNotIn('username', ['superadmin', 'admin'])->where('role_id',['6','3'])->get();
        }else{
            $users= User::where('role_id',auth()->user()->role_id)->get();
        }
        $view->with('users', $users);
    }
}
