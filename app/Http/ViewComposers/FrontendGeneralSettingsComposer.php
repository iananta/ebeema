<?php

namespace App\Http\ViewComposers;

use App\Models\UserSettings;
use Illuminate\View\View;

class FrontendGeneralSettingsComposer
{
    public function compose(View $view)
    {
        $data = UserSettings::select('key', 'value')
            ->get();

        $user_settings = [];

        foreach ($data as $item) {
            $user_settings[$item->key] = $item->value;
        }

        $view->with('user_settings', $user_settings);
    }
}
