<?php

namespace App\Http\ViewComposers;


use App\Models\MetaData;
use Illuminate\View\View;

class MetaDataComposer
{
    public function compose(View $view)
    {
        $data = MetaData::select('page_name', 'meta_title', 'meta_description', 'status')
            ->where('status', 1)
            ->get();
        $view->with('seoData', $data);
    }
}
