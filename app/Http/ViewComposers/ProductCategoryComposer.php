<?php

namespace App\Http\ViewComposers;

use App\Models\ProductCategory;
use Illuminate\View\View;

class ProductCategoryComposer
{
    public function compose(View $view)
    {
        $prodCat = ProductCategory::where('status', 1)->get();
        $view->with('productCategories', $prodCat);
    }
}
