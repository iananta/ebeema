<?php

namespace App\Http\ViewComposers;
use App\Models\Province;
use Illuminate\View\View;

class ProvinceComposer{
	  public function compose(View $view){
	  	$data=Province::all();
	  	return $view->with('provinces',$data);
	  }
}