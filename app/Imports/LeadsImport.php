<?php

namespace App\Imports;

use App\Models\Lead;
use App\Models\LeadSource;
use App\Models\LeadType;
use App\Models\Remark;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LeadsImport implements ToModel, WithHeadingRow
{
    protected $file;

    public function __construct(array $file)
    {
        $this->file = $file;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
//        dd($row);
        $lead =  Lead::query()->create([
            'customer_name' => $row['prospect'],
            'phone' => $row['number'],
            'city' => $row['address'],
            'email' => $row['email'],
            'proposed_plan' => $row['proposed_plan'],
            'sum_insured' => $row['est_sum_assured'],
            'leadsource_id' => (LeadSource::query()->firstOrCreate([
                'name' => $row['lead_source']
            ]))->id,
            'leadtype_id' => (LeadType::query()->firstOrCreate([
                'type' => $row['lead_status']
            ]))->id
        ]);
        if ($row['last_call_update']) {
            $lead->remark()->create([
                'remark' => $row['last_call_update'],
                'follow_up_date' => $row['last_call_date'] ?? NULL,
            ]);
        }

        if ($row['next_step']) {
            $lead->remark()->create([
                'remark' => $row['next_step'],
                'follow_up_date' => $row['next_step_deadline'] ?? NULL,
            ]);
        }

    }
}
