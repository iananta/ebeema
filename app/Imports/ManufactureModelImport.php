<?php

namespace App\Imports;

use App\Models\MotorManufactureCompanyModel;
use App\Models\MotorManufactureCompany;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;
use Auth;
use Carbon\Carbon;
use Exception;


class ManufactureModelImport implements ToModel,WithHeadingRow 
{
    protected $rateFile;

    public function __construct(array $rateFile)
    {
        
        $this->rateFile = $rateFile;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $user = MotorManufactureCompany::where('id',$row['sn'])->first();
        return new MotorManufactureCompanyModel([
            'mmc_id'=> $row['mmc_id'],
            'vehicle_type'=>$row['vehicle_type'],
            'model'=>$row['model'],
            'engine_capacity'=>$row['engine_capacity'],
            'manufacture_country'=>$row['manufacture_country'],
            'unit'=>$row['unit'],
            'price_per_vehicle'=>$row['price_per_vehicle'],
            'selling_price'=>$row['selling_price'],
         ]);
   }    
} 
 

 

