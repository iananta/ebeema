<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;

class TestImport implements ToModel, WithHeadingRow
{
    protected $rateFile;

    public function __construct(array $rateFile)
    {
        $this->rateFile = $rateFile;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    public function model(array $row)
    {
        dd($row);
    }

}
