<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Auth;
use Illuminate\Queue\SerializesModels;

class CompareResult extends Mailable
{
    use Queueable, SerializesModels;

//    public $products;
//    public $clientName;
//    public $clientEmail;
    public $data;
    public $pdf;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$pdf)
    {
//        dd($pdf);
//        dd($clientEmail, $clientName, $products);
        $this->data = $data;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        dd($this->data['clientEmail']);
        $mail = MailTemplate::where('key','life-compare-result')->where('status',1)->first();
        $body= str_replace('{{customerName}}',$this->data['clientName'], $mail->body);
        $to = $mail->to ? explode(',',str_replace(' ', '',$mail->to)) : '';
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';
        $mail = $this->subject($mail->subject)->markdown('emails.compare.result',['mail' => $body]);
//        dd($mail);
        if($to){
            $mail=$mail->to($to);
        }
        if($this->data['clientEmail']){
            $mail=$mail->cc($this->data['clientEmail']);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        if($this->pdf){
            $mail=$mail->attachData($this->pdf, 'compare-results.pdf', [
                'mime' => 'application/pdf',
            ]);
        }
        return $mail;
//        return $this->subject('Compare Result')
//            ->cc([Auth::user()->email])
//            ->attachData($this->pdf, 'compare-results.pdf', [
//                'mime' => 'application/pdf',
//            ])
//            ->markdown('emails.compare.result');
    }
}
