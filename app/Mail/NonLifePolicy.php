<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NonLifePolicy extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $policy;

    public function __construct($policy)
    {
        $this->policy = $policy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $policy = $this->policy;
//        dd($policy);
        $mail = MailTemplate::where('key','policy-done')->where('status',1)->first();
        $body=str_replace('{{customer}}',$policy->customer->customer_name, $mail->body);
        $body2=str_replace('{{vehicleNo}}',$policy->VEHICLENO, $body);
        $body3=str_replace('{{paidAmt}}',$policy->PAIDAMT, $body2);
        $body4=str_replace('{{transDate}}',$policy->TRANS_DATE, $body3);
        $body5=str_replace('{{policyNo}}',json_decode($policy->output)[0]->policyNo, $body4);
        $body6=str_replace('{{referenceNumber}}',$policy->reference_number, $body5);
        $to = $mail->to ? explode(',',str_replace(' ', '',$mail->to)) : '';
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';
        $mail = $this->subject($mail->subject)->markdown('emails.nonLife.policy',['mail' => $body6]);
        if ($to){
            $mail=$mail->to($to);
        }
        if($policy->customer->email){
            $mail=$mail->cc($policy->customer->email);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        return $mail;
//        return $this->subject($mail->subject)
//            ->markdown('emails.nonLife.policy');
    }
}
