<?php

namespace App\Mail;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;
class Planselect extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $data['plan_selected']=Product::where('id',$data['product'])->pluck('name')->first();
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $planSelected = $this->data;
        $mail = MailTemplate::where('key','plan-selected')->where('status',1)->first();
//        dd($planSelected);
        $body= str_replace('{{customerName}}',$planSelected['name'], $mail->body);
        $to = $mail->to ? explode(',',str_replace(' ', '',$mail->to)) : '';
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';
        $mail = $this->subject($mail->subject)->markdown('emails.plan.planselected',['mail' => $body]);
        if ($to){
            $mail=$mail->to($to);
        }
        if($this->data['email']){
            $mail=$mail->cc($this->data['email']);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        return $mail;

//        return $this->from($this->data['email'], '')
//            ->subject('Plan Selected')
//            ->markdown('emails.plan.planselected');
    }
}
