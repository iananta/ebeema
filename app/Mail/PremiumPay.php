<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PremiumPay extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;
    public $dayy;

    public function __construct($data,$dayy)
    {
        $this->data = $data;
        $this->dayy = $dayy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['premium'] = $this->data;
        $data['day'] = $this->dayy;
        return $this->subject(ucfirst($data['premium']->planselected->category)." Renewal" ?? '')
            ->markdown('emails.premium.premiumPay',$data);
    }
}
