<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

    protected $fillable=['title','image','description','status'];



    public function setImageAttribute($value){
        $destination='uploads/banner/';
        $imageName = time().'_'.$value->getClientOriginalName();  
        $value->move($destination,$imageName);
        $this->attributes['image'] =$imageName;
    }
}
