<?php

namespace App\Models;

use App\Models\Blogcate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = 'blogs';
    protected $fillable = [
        'blogcate_id',
        'title',
        'description',
        'image',
        'slug',
        'meta_title',
        'meta_description'
    ];

    public function category(){
        return $this->hasOne(Blogcate::class,'id','blogcate_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
