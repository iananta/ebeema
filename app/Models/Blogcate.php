<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Blog;

class Blogcate extends Model
{
    use HasFactory;
    public $timestamps = true;
    protected $table = 'blogcates';
    protected $fillable = [
        'name',
    ];
    public function blog()
    {
        return $this->hasMany(Blog::class,'blogcate_id');
    }
  
}
