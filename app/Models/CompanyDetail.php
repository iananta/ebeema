<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyDetail extends Model
{
    use HasFactory;

    protected $fillable = ['company_id', 'api_url','api_details', 'imepay_credentials', 'esewa_credentials', 'khalti_credentials'];

    public function company()
    {
        return $this->belongsTo(Company::class,"company_id",'id');
    }
}
