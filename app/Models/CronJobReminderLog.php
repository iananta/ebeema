<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CronJobReminderLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'type','sms_sent','mail_sent','output'
    ];
}
