<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;
    protected $guarded = [
        'id',
        'province_id',
        'district_name',
    ];
    public function province()
    {
       return $this->belongsTo(Province::class);
    }
    public function user()
    {
    	return $this->hasMany(User::class,'district_name','province_id');
    }
   
}
