<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
class Lead extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'leads';
    protected $fillable = [
        'lead_id',
        'leadsource_id',
        'leadtype_id',
        'user_id',
        'customer_name',
        'province',
        'city',
        'street_ward',
        'phone',
        'email',
        'province',
        'dob',
        'age',
        'profession',
        'insurance_company_name',
        'category',
        'policy_type',
        'sum_insured',
        'maturity_period',
        'premium',
        'lead_transfer_req',
        'is_active',
        'mop',
        'is_user',
        'sales_person_name',
        'product_id',
        'fb_lead_form_id',
        'fb_lead_id',
        'proposed_plan',
        'branch',
        'form_details',
        'organization_name',
        'size_of_employee',
        'message'
    ];

    protected $hidden = ['created_at', 'updated_at','created_by','updated_by','deleted_at'];



    public function leadsource(){
        return $this->belongsTo(LeadSource::class);
    }
    public function leadtype(){
        return $this->belongsTo(LeadType::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function policytype(){
        return $this->belongsTo(PolicyType::class,'policy_type');
    }

    public function company(){
        return $this->belongsTo(Company::class,'insurance_company_name');
    }

    public function remark(){
        return $this->hasMany(Remark::class,'lead_id','id');
    }

    public function getCity(){
        return $this->belongsTo(City::class,'city');

    }

     public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

    public function getProvince(){
        return $this->belongsTo(Province::class,'province');
    }

    public function seoform(){
        return $this->belongsTo(SeoForm::class,'fb_lead_form_id','form_id');
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function meetings(){
        return $this->hasMany(Meeting::class);
    }

    public function planSelecteds()
    {
        return $this->hasMany(Planselected::class,'lead_id','id')->with('premiumPaidByCustomer');
    }

    public function document(){
        return $this->hasMany(Leadsdocs::class,'lead_id','id');
    }

    public function lifeClaims(){
        return $this->hasMany(LifeClaim::class);
    }

    public function premiumPaidByCustomer(){
        return $this->hasMany(PremiumPaidByCustomer::class,'lead_id','id');
    }

    public static function getLeads(){
        $roleId=auth()->user()->role_id;
        $todayDate=Carbon::create(date('Y-m-d'));
        $query=self::with(['product','getCity','getProvince','leadtype','leadsource','user','document','remark'=>function($query) use($todayDate){
                    $query->orderBy('follow_up_date','desc');
                }]);
        if($roleId == 9){
            $query=$query->whereIn('company_id',\Auth::user()->userAgents->pluck('company_id'));
        }
        if($roleId!=1 && $roleId!=7){
            $query=$query->where('user_id',auth()->user()->id);
        }else if($roleId == 7){
            $query=$query->where('policy_type',1);
        }
        return $query;
    }

    public static function getleadCustomer(){
       $roleId=auth()->user()->role_id;
       $todayDate=Carbon::create(date('Y-m-d'));
       $query=self::withSum('premiumPaidByCustomer','premium_amount')->with(['getCity','getProvince','leadtype','leadsource','user','document'])->where(['is_user'=>1,'verify' => 1]);
        if( $roleId!= 1 && $roleId != 7 ){
         $query->where('user_id',auth()->user()->id);
        }else if($roleId == 7){
            $query->where('policy_type',1);
        }
       return $query;
    }

    public static function leadFollowup(){
        $todayDate=Carbon::create(date('Y-m-d'));
        $roleId=auth()->user()->role_id;
        $query=self::whereHas('remark',function ($sql) use ($todayDate) {
                $sql->where('follow_up_date','>=',$todayDate->toDateString())->orderBy('follow_up_date','asc');
            });
        if($roleId!= 1 && $roleId!=7){
          $query=$query->where('user_id',auth()->user()->id);
        }else if($roleId==7){
           $query=$query->where('policy_type',1);
        }
        $query=$query->groupBy('customer_name');
        return $query;

    }

    public static function leadsCount($date=null){
        $query=self::getLeads();
        if($date){
            $query=$query->whereDate('created_at','<=',$date);
        }
        return $query->count();
    }
     public static function customersCount($date=null){
        $query=self::getleadCustomer();
        if($date){
            $query=$query->whereDate('created_at','<=',$date);
        }
        return $query->count();
    }



    protected static function boot(){
        parent::boot();
        static::creating(function (Lead $lead) {
            $lead->created_by = auth()->user()->id ?? '';
        });
    }

}
