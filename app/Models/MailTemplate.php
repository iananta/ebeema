<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MailTemplate extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'key', 'to', 'cc', 'bcc', 'subject', 'body', 'default_body', 'image', 'status', 'notes'
    ];
}
