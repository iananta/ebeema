<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorManufactureCompany extends Model
{           
    use HasFactory;         
    use SoftDeletes;           
                                               
    protected $fillable = [       
        'category', 'name', 'code', 'status'   
    ];   
    // public function manufacturingcompany()     
    // {            
    //    return $this->hasMany(MotorManufactureCompanyModel::class,'mmc_id');        
    // }            
    // public function manufacturingcompany(): HasOne                         
    // {         
    //     return $this->belongsTo(MotorManufactureCompanyModel::class,'mmc_id');
    // }
    public function manufacturingcompany()
    {
        return $this->hasMany(MotorManufactureCompanyModel::class,'mmc_id','id');
    }
    public function category()
    {
        return $this->hasMany(MotorManufactureCompanyModel::class,'category','id');
    }
    public function name()
    {
        return $this->hasMany(MotorManufactureCompanyModel::class,'name','id');
    }
    public function code()
    {
        return $this->hasMany(MotorManufactureCompanyModel::class,'code','id');
    }

}
