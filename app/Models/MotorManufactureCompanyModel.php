<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotorManufactureCompanyModel extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'mmc_id', 'vehicle_details', 'vehicle_type', 'model', 'engine_capacity', 'manufacture_country', 'unit', 'price_per_vehicle', 'selling_price', 'code', 'status'
    ]; 
  
  
    // public function manufacturingcompanymodel()
    // {
    //     return $this->belongsTo(MotorManufactureCompanyModel::class,'mmc_id');
    // }  
    // public function category()
    // {
    //     return $this->belongsTo(MotorManufactureCompanyModel::class,'category');
    // }  
    // public function name()
    // {
    //     return $this->belongsTo(MotorManufactureCompanyModel::class,'name');
    // }  
    // public function code()
    // {
    //     return $this->belongsTo(MotorManufactureCompanyModel::class,'code');
    // }  
    public function getmmc(){
        return $this->hasMany(MotorManufactureCompany::class,'id','mmc_id');
    }
        
}
   