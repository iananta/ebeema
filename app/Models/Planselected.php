<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Planselected extends Model
{
    use HasFactory;
    protected $table='planselected';

    protected $fillable = [
        'name', 'category', 'product_id', 'company_id', 'lead_id', 'date-type', 'birth_date', 'birth_month', 'birth_year', 'age', 'child_birth_date', 'child_birth_month', 'child_birth_year', 'child_age', 'proposer_birth_date', 'proposer_birth_month', 'proposer_birth_year', 'proposer_age', 'husband_birth_date', 'husband_birth_month', 'husband_birth_year', 'husband_age', 'wife_birth_date', 'wife_birth_month', 'wife_birth_year', 'wife_age', 'term', 'sum_assured', 'invest', 'mop', 'feature', 'premium', 'bonus', 'return', 'net_gain', 'email', 'phone', 'citizen_front', 'citizen_back', 'status','issued_date','policy_number'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function lead()
    {
        return $this->belongsTo(Lead::class,'lead_id','id');
    }

    public function premiumPaidByCustomer(){
        return $this->hasMany(PremiumPaidByCustomer::class,'planselected_id','id');
    }


}
