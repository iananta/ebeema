<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PremiumPaidByCustomer extends Model
{
	protected $table='premium_paid_by_customer';
    protected $fillable = [
		'premium_amount','paid_date','planselected_id','user_id','lead_id','status','verify','next_paid_date'
	];
	protected $hidden = ['created_at', 'updated_at'];

	public function planselected(){
		return $this->belongsTo(Planselected::class,'planselected_id','id');
	}

	public function lead(){
		return $this->belongsTo(Lead::class,'lead_id','id');
	}

	public function user(){
		return $this->belongsTo(User::class,'user_id','id');
	}

	public static function totalSales($date=null){
		$query=new self();
		if($date){
			$query=$query->whereDate('created_at','<=',$date);
		}

		if(!control('view-all-lead-table')){
			$query=$query->where('user_id',auth()->user()->id);
		}
		return $query->sum('premium_amount');
	}
}
