<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remark extends Model
{
    use HasFactory;
    protected $fillable = [ 'lead_id',
        'remark',
        'remark_date',
        'follow_up_date',
        'follow_up_time',
        'created_by',
    ];

    public function lead()
    {
        return $this->belongsTo(Lead::class);
    }
}
