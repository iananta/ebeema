<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SMSTemplate extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'key', 'message', 'default_message', 'image', 'status', 'notes'
    ];
}
