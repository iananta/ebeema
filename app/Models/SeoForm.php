<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SeoForm extends Model
{
    protected $fillable=['form_name','form_id'];

}
