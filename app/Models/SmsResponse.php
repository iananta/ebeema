<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsResponse extends Model
{
    use HasFactory;

    protected $fillable = ['reference_number','customer_id','message','response','phone_number'];

    public function customer(){
        return $this->belongsTo(Lead::class,'customer_id','id');
    }
}
