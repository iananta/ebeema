<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelMedicalInsurance extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id', 'customer_id', 'reference_number', 'KYCID', 'BRANCHID', 'DEPTID', 'CLASSID', 'INSURED', 'INSUREDADDRESS', 'PASSPORTNO', 'DOB', 'TOTALDOLLARPREMIUM', 'DOLLARRATE', 'CURRENCY', 'TOTALNC', 'DTFROM', 'DTTO', 'PERIODOFINSURANCE', 'VISITPLACE', 'CONTACTNO', 'OCCUPATION', 'REMARKS', 'CAREOF', 'AGE', 'ISDEPENDENT', 'RELATION', 'COVIDCHARGEPREMIUM', 'COVIDRATE', 'DIRECTDISCOUNTRATE', 'DIRECTDISCOUNT', 'HASDIRECTDISCOUNT', 'COVERTYPE', 'PLAN', 'PACKAGE', 'COVER_TYPE_CODE', 'PLAN_CODE', 'PACKAGE_CODE', 'ISANNUALTRIP', 'STAMPDUTY', 'VATAMOUNT', 'TOTALPAYABLEBYCLIENT', 'MERCHANT_TRANS_NO', 'MERCHANT_CODE', 'MERCHANT_TRANS_DATE', 'status', 'status_step', 'output','image'
    ];
}
