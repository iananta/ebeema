<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TravelMedicalPolicy extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'customer_id', 'customer_list_id', 'reference_number', 'KYCID', 'proformano', 'insured', 'className', 'Suminsured', 'tpPremium', 'documentNo', 'receiptNo', 'receiptDate', 'effectiveDate', 'expiryDate', 'policyNo', 'TransactionStatus', 'AcceptanceNo', 'status', 'payment_status'];

    public function customer()
    {
        return $this->belongsTo(Lead::class, 'customer_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
