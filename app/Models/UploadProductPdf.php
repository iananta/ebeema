<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadProductPdf extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'code', 'description', 'type', 'attachment', 'product_id', 'company_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
