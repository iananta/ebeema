<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'role_id',
        'designation',
        'is_active',
        'image_icon',
        'agent',
        'ref_id',
        'phone_number',
        'company_details',
        'address',
        'citizen_front',
        'citizen_back',
        'phone_number_unofficial',
        'email_unofficial',
        'ZONEID',
        'DISTRICTID',
        'province',
        'district',
    ];

    protected static $logFillable = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'created_at', 'updated_at','deleted_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get role of the user
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function district(){
        return $this->belongsTo(District::class,'district_name');
    }
    public function province(){
        return $this->belongsTo(Province::class,'province');
    }

    public function leads(){
        return $this->hasMany(Lead::class,'user_id','id');
    }

    public function customers(){
        return $this->hasMany(Lead::class,'user_id','id')->where(['is_user'=>1,'verify'=>1]);
    }
    public function premiumPaidByCustomer(){
        return $this->hasMany(PremiumPaidByCustomer::class,'user_id','id');
    }

    public function kyc()
    {
        return $this->hasOne(KYC::class, 'user_id', 'id');
    }

    public function outlookCredentials()
    {
        return $this->hasOne(UserOutlookCredential::class, 'user_id', 'id');
    }

    public function userAgents()
    {
        return $this->hasMany(UserAgent::class, 'user_id', 'id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d F Y');
    }

    public static function totalUser($date=null){
        $query=new self();
        if($date){
            $query=$query->whereDate('created_at','<=',$date);
        }
        return $query->count();
    }

    public function subscriptionusage(){
        return $this->hasOne(Subscriptionusage::class,'user_id','id');
    }


}
