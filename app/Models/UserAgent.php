<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAgent extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','company_id','liscence_number'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
