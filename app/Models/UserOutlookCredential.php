<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOutlookCredential extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id','access_token','refresh_token','token_expires','user_name','user_email','user_time_zone','OAUTH_APP_ID','OAUTH_APP_SECRET','status'
    ];
}
