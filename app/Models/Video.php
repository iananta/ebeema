<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;
    protected $guarded = [];
    // public function user()
    // {
    //     return $this->belongsTo(User::class, 'user_id', 'id');
    // }
    public static function videofollowup(){
        $todayDate=Carbon::create(date('Y-m-d'));
        $roleId=auth()->user()->role_id;
        if($roleId!= 1 && $roleId!=7){
          $query=$query->where('title',auth()->user()->id);
        }else if($roleId==7){
           $query=$query->where('title',1);
        }
        $query=$query->groupBy('file');
        return $query;

    }
}
