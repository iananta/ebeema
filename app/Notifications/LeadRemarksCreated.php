<?php

namespace App\Notifications;

use App\Models\MailTemplate;
use App\Models\Remark;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\App;

class LeadRemarksCreated extends Notification
{
    use Queueable;

    private $remark;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($remark)
    {
        $this->remark = $remark;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $remark = $this->remark;
        $lead = $remark->lead;
        $mail = MailTemplate::where('key','remark-lead')->where('status',1)->first();
        $body=str_replace('{{remark}}',$remark->remark, $mail->body);
        $body2=str_replace('{{created_at}}',$remark->created_at, $body);
        $to=auth()->user()->email;
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';

        $mail=(new MailMessage)
            ->subject($mail->subject)
            ->view('emails.lead.remarks',['mail' => $body2]);
        if($lead->email){
            $mail=$mail->cc($lead->email);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        $mail=$mail->replyTo($to);
        return $mail;
    }

    public function toDatabase($notifiable)
    {
        return [
          'id' => $this->remark->id,
            'remark' => $this->remark->remark,
            'follow_up_date' => $this->remark->follow_up_date,
            'lead_id' => $this->remark->lead_id
        ];
    }

    public function toBroadcast($notifiable){
        return new BroadcastMessage([
            'message' => 'lead remark: ' . $this->remark->remark . ' was made at ' . $this->remark->follow_up_date
        ]);
    }

    public function broadcastOn()
    {
        return ['user.'.auth()->user()->id];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
