<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use App\Models\Lead;

class NewLeadCreated extends Notification implements ShouldQueue
{
    use Queueable;
    private $lead;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($lead)
    {
        $this->lead=$lead;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
       
        return [
            'lead'=>[
                "is_user" => "0",
                "dob" => date('Y-m-d', strtotime($this->lead->dob)),
                'age' => $this->lead->age ?? '',
                "customer_name" => $this->lead->name ?? '',
                "phone" => $this->lead->phone ?? '',
                "email" => $this->lead->email ?? '',
                "province"=>$this->lead->province ?? '',
                'sum_insured'=>$this->lead->sumassured ?? '',
                'maturity_period'=>$this->lead->term ?? '',
                "leadsource_id" => 1,
                "leadtype_id" => 2,
                'policy_type' => 1,
                'category'=>$this->lead->category ?? ''
            ]
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => 'Lead: New Lead has been added '.$this->lead->created_at
        ]);
    }

    public function broadcastOn()
    {
        return ['notify-channel'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
