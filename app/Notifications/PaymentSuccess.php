<?php

namespace App\Notifications;

use App\Models\MailTemplate;
use App\Models\MotorCalculationData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PaymentSuccess extends Notification
{
    use Queueable;
    private $paySuccess;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($paySuccess)
    {
        $this->paySuccess = $paySuccess;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $paySuccess = $this->paySuccess;
//        dd($paySuccess);
        $mail = MailTemplate::where('key','payment-success')->where('status',1)->first();
//        dd($mail);
        $body=str_replace('{{paymentMethod}}',$paySuccess['paymentMethod'] , $mail->body);
        $body1=str_replace('{{amount}}',$paySuccess['payableAmount'], $body);
        $body2=str_replace('{{referenceId}}',$paySuccess['referenceId'], $body1);
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';

        $mail=(new MailMessage)
            ->subject($mail->subject)
            ->view('emails.nonLife.paymentSuccess',['mail' => $body2]);
        if($cc){
            $mail=$mail->cc($cc);
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        return $mail;
    }

    public function toDatabase($notifiable) {
        return[
            'paymentMethod' => $this->paySuccess['paymentMethod'],
        ];
    }

    public function toBroadcast($notifiable){
        return new BroadcastMessage([
            'message' => 'Payment through ' . $this->paySuccess['paymentMethod'] . ' was successful! '
        ]);
    }

    public function broadcastOn()
    {
        return ['user.'.auth()->user()->id];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
