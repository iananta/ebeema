<?php

namespace App\Notifications;

use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PremiumPaid extends Notification
{
    use Queueable;

    private $premium;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($premium)
    {
//        dd($premium);
        $this->premium = $premium;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $premium = $this->premium;
        $lead = $premium->lead;
        $planselected = $premium->planselected;
        $mail = MailTemplate::where('key','premium-paid')->where('status',1)->first();
        $body=str_replace('{{customer}}',$premium->lead->customer_name, $mail->body);
        $body1=str_replace('{{planName}}',$premium->planselected->name, $body);
        $body2=str_replace('{{premiumAmount}}',$premium->premium_amount, $body1);
        $body3=str_replace('{{paidDate}}',$premium->paid_date, $body2);
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';

        $mail=(new MailMessage)
            ->subject($mail->subject)
            ->view('emails.lead.premium',['mail' => $body3]);
        if($lead->email){
            $mail=$mail->cc($lead->email);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        return $mail;
    }

    public function toDatabase($notifiable){
        return [
            'id' => $this->premium->id,
            'planselected_id' => $this->premium->planselected_id,
            'premium_amount' => $this->premium->premium_amount,
            'paid_date' => $this->premium->paid_date
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => 'Premium paid of NRs.' . $this->premium->premium_amount . ' at date' . $this->premium->paid_date . ' at ' . $this->premium->created_at
        ]);
    }

    public function broadcastOn()
    {
        return ['user.'.auth()->user()->id];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
