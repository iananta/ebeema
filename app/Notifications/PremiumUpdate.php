<?php

namespace App\Notifications;

use App\Models\MailTemplate;
use App\Models\PremiumPaidByCustomer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PremiumUpdate extends Notification
{
    use Queueable;
    private $premiumUpdate;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($premiumUpdate)
    {

        $this->premiumUpdate=$premiumUpdate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $premiumUpdate = $this->premiumUpdate;
        $lead = $premiumUpdate->lead;
        $planselected = $premiumUpdate->planselected;
        $mail = MailTemplate::where('key','premium-update')->where('status',1)->first();
        $body=str_replace('{{customer}}',$premiumUpdate->lead->customer_name, $mail->body);
        $body1=str_replace('{{planName}}',$premiumUpdate->planselected->name, $body);
        $body2=str_replace('{{premiumAmount}}',$premiumUpdate->premium_amount, $body1);
        $body3=str_replace('{{paidDate}}',$premiumUpdate->paid_date, $body2);
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';

        $mail=(new MailMessage)
            ->subject($mail->subject)
            ->view('emails.lead.premiumUpdate',['mail' => $body3]);
        if($lead->email){
            $mail=$mail->cc($lead->email);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        return $mail;
    }

    public function toDatabase($notifiable){
        return [
            'id' => $this->premiumUpdate->id,
            'planselected_id' => $this->premiumUpdate->planselected_id,
            'premium_amount' => $this->premiumUpdate->premium_amount,
            'paid_date' => $this->premiumUpdate->paid_date
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => 'Premium paid updated for NRs.' . $this->premiumUpdate->premium_amount . ' at date' . $this->premiumUpdate->paid_date . ' at ' . $this->premiumUpdate->created_at
        ]);
    }

    public function broadcastOn()
    {
        return ['user.'.auth()->user()->id];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
