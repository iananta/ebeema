<?php

namespace App\Notifications;

use App\Models\MailTemplate;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserActivated  extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
//        dd($this->user);
        $user = $this->user;
        $message = ($user->is_active == '1') ? " activated successfully! Now you can log into the system" : " deactivated! You cannot log into the system anymore";
//        dd($message);
        $mail = MailTemplate::where('key','user-activated')->where('status',1)->first();
        $body=str_replace('{{user}}',$user->username, $mail->body);
        $body1=str_replace('{{phoneNumber}}',$user->phone_number, $body);
        $body2=str_replace('{{status}}',$message, $body1);
        $cc = $mail->cc ? explode(',',str_replace(' ', '',$mail->cc)) : '';
        $bcc =$mail->bcc ? explode(',',str_replace(' ', '',$mail->bcc)) : '' ;
        $cc  = $cc != null ? array_values($cc) : '';
        $bcc = $bcc != null ? array_values($bcc) : '';

        $mail=(new MailMessage)
            ->subject($mail->subject)
            ->view('emails.user.userStatus',['mail' => $body2]);
        if($user->email){
            $mail=$mail->cc($user->email);
            if($cc){
                $mail=$mail->cc($cc);
            }
        }
        if($bcc){
            $mail=$mail->bcc($bcc);
        }
        return $mail;
    }

    public function toDatabase($notifiable){
        return[
            'user' => [
                'id' => $this->user->id,
                'name' => $this->user->username,
                'email' => $this->user->email,
                'is_active' => $this->user->is_active
            ],
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => 'User: ' . $this->user->username . ' status changed successfully! at ' . $this->user->created_at
        ]);
    }

    public function broadcastOn()
    {
        return ['user.'.auth()->user()->id];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
