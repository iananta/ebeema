<?php

namespace App\Providers;

use App\Http\ViewComposers\ContactInfoComposer;
use App\Http\ViewComposers\FrontendGeneralSettingsComposer;
use App\Http\ViewComposers\GeneralSettingsComposer;
use App\Http\ViewComposers\MetaDataComposer;
use App\Http\ViewComposers\SocialLinkComposer;
use App\Http\ViewComposers\ProvinceComposer;
use App\Http\ViewComposers\Backend\UserListComposer;
use App\Http\ViewComposers\Backend\LeadsourcesComposer;
use App\Http\ViewComposers\Backend\LeadtypeComposer;
use App\Http\ViewComposers\Backend\ProductCategoriesComposer;
use App\Http\ViewComposers\Backend\CityComposer;
use App\Http\ViewComposers\Backend\CompanyComposer;
use App\Http\ViewComposers\Backend\RoleComposer;
use App\Http\ViewComposers\Backend\SeoFormComposer;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            [
                'frontend.layouts.header',
                'frontend.layouts.footer',
            ],
            SocialLinkComposer::class
        );

        View::composer(
            [
                'frontend.contact',
            ],
            ContactInfoComposer::class
        );

        View::composer(
            [
                'frontend.*',
            ],
            GeneralSettingsComposer::class
        );
        View::composer(
            [
                '*',
            ],
            FrontendGeneralSettingsComposer::class
        );
        View::composer(
            [
                '*',
            ],
            MetaDataComposer::class
        );
        View::composer(
            [
                '*',
            ],
            'App\Http\ViewComposers\ProductCategoryComposer'
        );


        view()->composer(
            [
                'layouts.backend.sidebar'
            ],
            'App\Http\ViewComposers\Backend\SidebarComposer'
        );

        view()->composer(
            [
                'layouts.backend.breadcrumb'
            ],
            'App\Http\ViewComposers\Backend\BreadcrumbComposer'
        );

        view()->composer(
            [
                'frontend.*',
                'Backend.Dashboard.index',
                'Backend.Leads.*'
            ],
            ProvinceComposer::class
        );

        view()->composer(
            [
                'Backend.Leads.AllLeads.includes.lead-edit',
                'Backend.Leads.Leads._addleadmodule',
                'Backend.Leads.AllLeads.includes.generate-edit-html',
                'Backend.Leads.AllLeads.leads'
                
            ],
            UserListComposer::class
        );

        view()->composer(
            [
                'Backend.Leads.AllLeads.includes.lead-edit',
                'Backend.Leads.Leads._addleadmodule',
                'Backend.Leads.AllLeads.includes.generate-edit-html',
            ],
            LeadsourcesComposer::class
        );

        view()->composer(
            [
                'Backend.Leads.AllLeads.includes.lead-edit',
                'Backend.Leads.Leads._addleadmodule',
                'Backend.Leads.AllLeads.includes.generate-edit-html',
                'Backend.Leads.AllLeads.leads'
            ],
            LeadtypeComposer::class
        );
        view()->composer(
            [ 
                'Backend.Leads.AllLeads.leads'
            ],
            SeoFormComposer::class
        );

        view()->composer(
            [
                'Backend.Leads.AllLeads.includes.lead-edit',
                'Backend.Leads.Leads._addleadmodule',
                'Backend.Leads.AllLeads.includes.generate-edit-html',
                'Backend.Leads.AllLeads.leads'
            ],
            ProductCategoriesComposer::class
        );
        view()->composer(
            [
                'Backend.Leads.AllLeads.includes.lead-edit',
                'Backend.Leads.Leads._addleadmodule',
                'Backend.Leads.AllLeads.includes.generate-edit-html',
            ],
            CityComposer::class
        );

        view()->composer(
            [
                'Backend.Leads.AllLeads.includes.lead-edit',
                'Backend.Leads.Leads._addleadmodule',
                'Backend.Leads.AllLeads.includes.generate-edit-html',
                'Backend.Leads.AllLeads.includes.convert-to-policy',
                'paybackSchedule.*',
            ],
            CompanyComposer::class
        );
         view()->composer(
            [
                'Backend.Leads.AllLeads.includes.generate-edit-html',
            ],
            RoleComposer::class
        );

        // view()->composer(
        //     [
        //         'frontend.header',
        //         'frontend.container',
        //         'frontend.donateUs'
        //     ],
        //     'App\Http\ViewComposers\HeaderComposer'
        // );

        // view()->composer(
        //     [
        //         'frontend.footer'
        //     ],
        //     'App\Http\ViewComposers\FooterComposer'
        // );

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
