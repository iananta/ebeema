<?php

use App\Models\KYC;
use App\Models\User;
use App\Models\Company;
use App\Models\Feature;
use App\Models\Lead;
use App\Models\MotorCalculationData;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ProductFeature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

function control($permission)
{
    $permissions_exist = 0;
    $user_id = Auth::user()->id ?? '';
    $permission_id = DB::table('permissions')->where('name', $permission)->first();
    $role_id = Auth::user()->role_id ?? '';
    if (($permission_id != null || $permission_id != '') && ($role_id != '' || $role_id != null))
        $permissions_exist = DB::table('permission_role')->where([
            ['permission_id', '=', $permission_id->id], ['role_id', '=', $role_id]
        ])->count();
    //    if($user_id == 1){
    //        return true;
    //    }
    if (isset($permissions_exist) && $permissions_exist > 0) {
        return true;
    } else {
        return false;
    }
}

function getPrefix()
{
    if (Auth::check()) {
        $user_id = Auth::user()->id;
        $role_id = DB::table('role_user')->where('user_id', $user_id)->first();
        $roles = DB::table('roles')->where('id', $role_id->role_id)->first();
        if ($role_id) {
            return STR::slug($roles->display_name);
        } else {
            return "admin";
        }
    }
}

function AgentCat()
{
    $companyID = count(Auth::user()->userAgents) ? Auth::user()->userAgents->pluck('company_id') : '';
    if ($companyID && Auth::user()->role_id != 1 && Auth::user()->role_id != 2 && Auth::user()->role_id != 7 && Auth::user()->role_id != 6) {
        $cat = Company::whereIn('id', $companyID)->pluck('type');
        return $cat->toArray();
    } else {
        $a = array('all');
        return $a;
    }
}

function getProductName($prodId)
{
    return Product::where('id', $prodId)->first()->name ?? 'N/A';
}

function featureName($code)
{
    if (isset($code)) {
        return Feature::where('code', $code)->first()->name;
    } else {
        null;
    }
}

function featureCode($code)
{
    if (isset($code)) {
        return Feature::where('code', $code)->first();
    } else {
        null;
    }
}


function findcatFeature($category)
{
    $products = Product::where('category', $category)->pluck('id');
    $featureIds = ProductFeature::whereIn('product_id', $products->toArray())->pluck('feature_id');

    return $feature = Feature::whereIn('id', $featureIds)->get();

    // return Response()::json(array('success' => true, 'data' => $data));
}

function customerKyc($id)
{
    return KYC::where('customer_id', $id)->first();
}

function customerLead($id)
{
    return Lead::where('id', $id)->first() ?? '';
}

function getProductList($prodId)
{
    return Product::whereIn('id', $prodId)->get();
}

function getProductId($prodId)
{
    return Product::where('id', $prodId)->first()->id;
}

function getBluebookImage($refId)
{
//    return $refId;
    $image = MotorCalculationData::withTrashed()->where('payment_ref_id', $refId)->first();
    return $image ? $image['bluebook_image'] : "#";
}

function getBikeImage($refId)
{
    $image = MotorCalculationData::withTrashed()->where('payment_ref_id', $refId)->first();
    return $image ? $image['bike_image'] : "#";
}

function AgentCategories()
{
    $companyID = count(Auth::user()->userAgents) ? Auth::user()->userAgents->unique('company_id')->pluck('company_id') : '';
    //    dd( $companyID);
    if ($companyID) {
        return $companyID->toArray();
    } else {
        $a = array('all');
        return $a;
    }
}

function findCompanyType($compId)
{
    return Company::whereIn('id', $compId)->get();
}

// limit the no of characters
function text_limit($x, $length)
{
    if (strlen($x) <= $length) {
        return $x;
    } else {
        return substr($x, 0, $length) . ' ...';
    }
}


// return role of the user
function getRole($user_id)
{
    return User::findOrFail($user_id)->role_id;
}


function convertCurrency($number)
{
    // Convert Price to Crores or Lakhs or Thousands
    $length = strlen($number);
    $currency = '';

    if ($length == 4 || $length == 5) {
        // Thousand
        $number = $number / 1000;
        $number = round($number, 2);
        $ext = "Thousand";
        $currency = $number . " " . $ext;
    } elseif ($length == 6 || $length == 7) {
        // Lakhs
        $number = $number / 100000;
        $number = round($number, 2);
        $ext = "Lac";
        $currency = $number . " " . $ext;

    } elseif ($length == 8 || $length == 9) {
        // Crores
        $number = $number / 10000000;
        $number = round($number, 2);
        $ext = "Cr";
        $currency = $number . ' ' . $ext;
    }

    return $currency;
}

if (!function_exists('months')) {
    function months()
    {
        return [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
    }
}

if (!function_exists('nepali_months')) {
    function nepali_months()
    {
        return [
            '01' => 'Baishak',
            '02' => 'Jestha',
            '03' => 'Ashar',
            '04' => 'Shrawan',
            '05' => 'Bhadra',
            '06' => 'Ashoj',
            '07' => 'Kartik',
            '08' => 'Mangsir',
            '09' => 'Poush',
            '10' => 'Magh',
            '11' => 'Falgun',
            '12' => 'Chaitra',
        ];
    }
}


if (!function_exists('lead_action_button')) {
    function lead_action_button($lead, $from)
    {
        $html = '
         <button data-id="' . $lead->id . '" data-from="' . $from . '" title="Edit Lead"
            class="btn btn-primary btn-sm edit-modal-btn action-btn"><i class="fa fa-edit"></i></button>
        <button data-id="' . $lead->id . '" class="btn btn-success btn-sm view-detail-lead"><i class="fa fa-eye"></i></button>&nbsp;
        <a href="/customer-kyc/' . $lead->id . '" data-toggle="tooltip"data-placement="top" title="Create Customer KYC" class="btn btn-primary btn-sm action-btn"><i class="fa fa-user"></i>
        </a>
        <a href="javascript:;" title="Delete leadsource"
        class="btn btn-danger delete-lead btn-sm action-btn"
        id="' . $lead->id . '"><i class="fa fa-trash"></i>
        </a>';
        $html .= view('Backend.Leads.AllLeads.includes.lead-details')->with('lead', $lead)->render();
        return $html;
    }
}


if(!function_exists('admin_role_id')){
    function admin_role_id(){
        return \Auth::user()->role_id;
    }
}