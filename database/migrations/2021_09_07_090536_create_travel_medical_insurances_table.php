<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelMedicalInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_medical_insurances', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('customer_id')->nullable();
            $table->string('reference_number')->nullable();
            $table->bigInteger('KYCID')->nullable();
            $table->integer('BRANCHID');
            $table->integer('DEPTID');
            $table->integer('CLASSID');
            $table->string('INSURED')->nullable();
            $table->string('INSUREDADDRESS')->nullable();
            $table->string('PASSPORTNO')->nullable();
            $table->string('DOB')->nullable();
            $table->double('TOTALDOLLARPREMIUM',15,3)->nullable();
            $table->double('DOLLARRATE',15,3)->nullable();
            $table->string('CURRENCY')->nullable();
            $table->double('TOTALNC',15,3)->nullable();
            $table->string('DTFROM')->nullable();
            $table->string('DTTO')->nullable();
            $table->bigInteger('PERIODOFINSURANCE')->nullable();
            $table->string('VISITPLACE')->nullable();
            $table->string('CONTACTNO')->nullable();
            $table->string('OCCUPATION')->nullable();
            $table->longText('REMARKS')->nullable();
            $table->string('CAREOF')->nullable();
            $table->integer('AGE')->nullable();
            $table->boolean('ISDEPENDENT')->default(0);
            $table->string('RELATION')->nullable();
            $table->double('COVIDCHARGEPREMIUM',15,3)->nullable();
            $table->double('COVIDRATE',15,3)->nullable();
            $table->double('DIRECTDISCOUNTRATE',15,3)->nullable();
            $table->double('DIRECTDISCOUNT',15,3)->nullable();
            $table->boolean('HASDIRECTDISCOUNT')->default(0)->nullable();
            $table->string('COVERTYPE')->nullable();
            $table->string('PLAN')->nullable();
            $table->string('PACKAGE')->nullable();
            $table->integer('ISANNUALTRIP')->default(0);
            $table->integer('STAMPDUTY')->nullable();
            $table->double('VATAMOUNT',15,3)->default(0);
            $table->double('TOTALPAYABLEBYCLIENT',15,3)->default(0);
            $table->string('MERCHANT_TRANS_NO')->nullable();
            $table->string('MERCHANT_CODE')->nullable();
            $table->string('MERCHANT_TRANS_DATE')->nullable();
            $table->integer('status')->default(0);
            $table->integer('status_step')->default(0);
            $table->longText('output')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_medical_insurances');
    }
}
