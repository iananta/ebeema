<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelMedicalPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_medical_policies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('customer_id')->nullable();
            $table->string('customer_list_id')->nullable();
            $table->string('reference_number')->nullable();
            $table->bigInteger('KYCID')->nullable();
            $table->string('proformano')->nullable();
            $table->string('insured')->nullable();
            $table->string('className')->nullable();
            $table->string('Suminsured')->nullable();
            $table->string('tpPremium')->nullable();
            $table->string('documentNo')->nullable();
            $table->string('receiptNo')->nullable();
            $table->string('receiptDate')->nullable();
            $table->string('effectiveDate')->nullable();
            $table->string('expiryDate')->nullable();
            $table->string('policyNo')->nullable();
            $table->string('TransactionStatus')->nullable();
            $table->string('AcceptanceNo')->nullable();
            $table->integer('status')->default(0);
            $table->integer('payment_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_medical_policies');
    }
}
