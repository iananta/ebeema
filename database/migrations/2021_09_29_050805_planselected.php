<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Planselected extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planselected', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('category');
            $table->integer('product_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('lead_id')->nullable();
            $table->string('date-type')->nullable();
            $table->integer('birth_date')->nullable();
            $table->integer('birth_month')->nullable();
            $table->integer('birth_year')->nullable();
            $table->integer('age')->nullable();

            $table->integer('child_birth_date')->nullable();
            $table->integer('child_birth_month')->nullable();
            $table->integer('child_birth_year')->nullable();
            $table->integer('child_age')->nullable();

            $table->integer('proposer_birth_date')->nullable();
            $table->integer('proposer_birth_month')->nullable();
            $table->integer('proposer_birth_year')->nullable();
            $table->integer('proposer_age')->nullable();

            $table->integer('husband_birth_date')->nullable();
            $table->integer('husband_birth_month')->nullable();
            $table->integer('husband_birth_year')->nullable();
            $table->integer('husband_age')->nullable();

            $table->integer('wife_birth_date')->nullable();
            $table->integer('wife_birth_month')->nullable();
            $table->integer('wife_birth_year')->nullable();
            $table->integer('wife_age')->nullable();

            $table->double('term',15,4);
            $table->double('sum_assured',15,4);
            $table->double('invest',15,4)->nullable();
            $table->string('mop');
            $table->text('feature')->nullable();
            $table->double('premium',15,4);
            $table->double('bonus',15,4)->nullable();
            $table->double('return',15,4)->nullable();
            $table->double('net_gain',15,4)->nullable();
            $table->string('email')->nullable();
            $table->string('phone');
            $table->string('citizen_front')->nullable();
            $table->string('citizen_back')->nullable();
            $table->integer('status')->default(0)->comment('0: requested, 1:pending,2:Done,3:rejected');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
