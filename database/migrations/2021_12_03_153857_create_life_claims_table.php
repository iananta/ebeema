<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLifeClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('life_claims', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('policy_no');
            $table->foreignId('company_id')->constrained('companies');
            $table->integer('phone_no');
            $table->integer('status')->default(0);
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('life_claims');
    }
}
