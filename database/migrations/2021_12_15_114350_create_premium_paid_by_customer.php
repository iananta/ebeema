<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiumPaidByCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premium_paid_by_customer', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('planselected_id');
            $table->integer('premium_amount');
            $table->date('paid_date');
            $table->index(['planselected_id']);
            $table->timestamps();
            $table->foreign('planselected_id')->references('id')->on('planselected')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premium_paid_by_customer');
    }
}
