<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNextPaidDateToPremiumPaidByCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('premium_paid_by_customer', function (Blueprint $table) {
            $table->date('next_paid_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('premium_paid_by_customer', function (Blueprint $table) {
            $table->dropColumn('next_paid_date');
        });
    }
}
