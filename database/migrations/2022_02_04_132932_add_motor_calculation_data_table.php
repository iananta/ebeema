<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMotorCalculationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motor_calculation_data', function (Blueprint $table) {
         
            $table->integer('INSUREDTYPE')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motor_calculation_data', function (Blueprint $table) {
            $table->dropColumn('INSUREDTYPE');
        });
    }
}
