<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionpaymentInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptionpayment_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('subscriptionplan_id');
            $table->integer('user_id');
            $table->decimal('amount');
            $table->string('payment_url');
            $table->string('ref_id');
            $table->date('paid_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptionpayment_infos');
    }
}
