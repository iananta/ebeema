<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorManufactureCompanyModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motor_manufacture_company_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mmc_id')->comment('mmc: motor manufacture company');
            $table->string('vehicle_details')->nullable()->unique();
            $table->string('vehicle_type')->nullable();
            $table->string('model')->nullable();
            $table->string('engine_capacity')->nullable();
            $table->string('manufacture_country')->nullable();
            $table->string('unit')->nullable();
            $table->double('price_per_vehicle', 10, 2)->nullable();
            $table->double('selling_price', 10, 2)->nullable();
            $table->string('code')->nullable()->unique();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('mmc_id')->references('id')->on('motor_manufacture_companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motor_manufacture_company_models');
    }
}
