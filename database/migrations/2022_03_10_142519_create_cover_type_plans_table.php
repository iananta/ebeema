<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoverTypePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cover_type_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cover_type_id')->nullable()->constrained("cover_types")->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('plan_area_id')->nullable()->constrained("plan_areas")->cascadeOnUpdate()->nullOnDelete();
            $table->foreignId('package_type_id')->nullable()->constrained("cover_package_types")->cascadeOnUpdate()->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cover_type_plans');
    }
}
