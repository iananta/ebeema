<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategoryCodeToTravelMedicalInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_medical_insurances', function (Blueprint $table) {
            $table->string('COVER_TYPE_CODE')->nullable()->after('COVERTYPE');
            $table->string('PLAN_CODE')->nullable()->after('PLAN');
            $table->string('PACKAGE_CODE')->nullable()->after('PACKAGE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_medical_insurances', function (Blueprint $table) {
            $table->dropColumn('COVER_TYPE_CODE');
            $table->dropColumn('PLAN_CODE');
            $table->dropColumn('PACKAGE_CODE');
        });
    }
}
