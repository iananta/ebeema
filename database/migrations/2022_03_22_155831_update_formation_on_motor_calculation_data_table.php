<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFormationOnMotorCalculationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('motor_calculation_data', function (Blueprint $table) {
            $table->string('FORMATION')->nullable()->after('VEHICLENAMEID');
            $table->string('MANUFACTURE')->nullable()->after('MAKEVEHICLEID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('motor_calculation_data', function (Blueprint $table) {
            $table->dropColumn('FORMATION');
            $table->dropColumn('MANUFACTURE');
        });
    }
}
