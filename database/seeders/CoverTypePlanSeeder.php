<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CoverTypePlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cover_type_plans')->insert([

            [
                'cover_type_id' => 1,
                'plan_area_id' => 5,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 1,
                'plan_area_id' => 6,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 1,
                'plan_area_id' => 11,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 1,
                'plan_area_id' => 12,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 2,
                'plan_area_id' => 5,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 2,
                'plan_area_id' => 6,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 2,
                'plan_area_id' => 11,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 2,
                'plan_area_id' => 12,
                'package_type_id' => 2,
            ],
            [
                'cover_type_id' => 3,
                'plan_area_id' => 5,
                'package_type_id' => null,
            ],
            [
                'cover_type_id' => 3,
                'plan_area_id' => 6,
                'package_type_id' => null,
            ],

        ]);
    }
}
