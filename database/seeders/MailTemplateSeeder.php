<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MailTemplate;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_templates')->insert([
            [
                'id' => '1',
                'title' => 'Lead Remarks Mail',
                'key' => 'remark-lead',
                'to' => '',
                'cc' => 'azizdulal.ad@gmail.com, superadmin@ebeema.com',
                'bcc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'subject' => 'Remarks created',
                'body' => '<p>Dear customer,</p><p>The remark: {{remark}} has been created at: {{created_at}}.</p> <p>Thank you for choosing,</p><p>&nbsp;</p>',
                'default_body' => '<p>Dear customer,</p><p>The remark: {{remark}} has been created at: {{created_at}}.</p> <p>Thank you for choosing,</p><p>&nbsp;</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{remark}}</li><li>{{created_at}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '2',
                'title' => 'Leads premium paid',
                'key' => 'premium-paid',
                'to' => '',
                'cc' => 'azizdulal.ad@gmail.com, superadmin@ebeema.com',
                'bcc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'subject' => 'Premium paid for lead',
                'body' => '<p>Dear {{customer}},</p><p>You have successfully paid for policy: {{planName}} of NRs. {{premiumAmount}} on {{paidDate}}.</p><p>Thank you for choosing,</p>',
                'default_body' => '<p>Dear {{customer}},</p><p>You have successfully paid for policy: {{planName}} of NRs. {{premiumAmount}} on {{paidDate}}.</p><p>Thank you for choosing,</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{customer}}</li><li>{{planName}}</li><li>{{premiumAmount}}</li><li>{{paidDate}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'id' => '3',
                'title' => 'Lead premium updated',
                'key' => 'premium-update',
                'to' => '',
                'cc' => 'azizdulal.ad@gmail.com, superadmin@ebeema.com',
                'bcc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'subject' => 'Premium paid for lead was updated',
                'body' => '<p>Dear {{customer}},</p><p>You have successfully updated premium payment for policy: {{planName}} of NRs. {{premiumAmount}} on {{paidDate}} .</p><p>Thank you for choosing,</p>',
                'default_body' => '<p>Dear {{customer}},</p><p>You have successfully updated premium payment for policy: {{planName}} of NRs. {{premiumAmount}} on {{paidDate}} .</p><p>Thank you for choosing,</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{customer}}</li><li>{{planName}}</li><li>{{premiumAmount}}</li><li>{{paidDate}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'id' => '4',
                'title' => 'Payment Success',
                'key' => 'payment-success',
                'to' => '',
                'cc' => 'azizdulal.ad@gmail.com, superadmin@ebeema.com',
                'bcc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'subject' => 'payment was successful',
                'body' => '<p>Dear Customer,</p><p>Your payment through {{paymentMethod}} was successfully completed for amount {{amount}} through Reference Id: {{referenceId}}.</p><p>Thank you for choosing,</p>',
                'default_body' => '<p>Dear Customer,</p><p>Your payment through {{paymentMethod}} was successfully completed for amount {{amount}} through Reference Id: {{referenceId}}.</p><p>Thank you for choosing,</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{paymentMethod}}</li><li>{{amount}}</li><li>{{referenceId}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'id' => '5',
                'title' => 'Payment Failed',
                'key' => 'payment-failed',
                'to' => '',
                'cc' => 'azizdulal.ad@gmail.com, superadmin@ebeema.com',
                'bcc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'subject' => 'payment was failed',
                'body' => '<p>Dear Customer,</p><p>Your payment through {{paymentMethod}} was failed for amount {{amount}} through Reference Id: {{referenceId}}.</p><p>Thank you for choosing,</p>',
                'default_body' => '<p>Dear Customer,</p><p>Your payment through {{paymentMethod}} was failed for amount {{amount}} through Reference Id: {{referenceId}}.</p><p>Thank you for choosing,</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{paymentMethod}}</li><li>{{amount}}</li><li>{{referenceId}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'id' => '6',
                'title' => 'Policy Done',
                'key' => 'policy-done',
                'to' => '',
                'cc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'bcc' => 'azizdulal.ad@gmail.com,superadmin@ebeema.com',
                'subject' => 'Policy made successfully',
                'body' => '<p>Dear {{customer}},</p><p>You have successfully purchased policy for vehicle number: {{vehicleNo}} of NRs. {{paidAmt}} on {{transDate}} .</p><p>The policy number is : {{policyNo}}.</p><p>The reference number is : {{referenceNumber}}.</p><p>Thank you for choosing,</p>',
                'default_body' => '<p>Dear {{customer}},</p><p>You have successfully purchased policy for vehicle number: {{vehicleNo}} of NRs. {{paidAmt}} on {{transDate}} .</p><p>The policy number is : {{policyNo}}.</p><p>The reference number is : {{referenceNumber}}.</p><p>Thank you for choosing,</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{customer}}</li><li>{{vehicleNo}}</li><li>{{paidAmt}}</li><li>{{transDate}}</li><li>{{policyNo}}</li><li>{{referenceNumber}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'id' => '7',
                'title' => 'Life Insurance Plan Selected (frontend compare)',
                'key' => 'plan-selected',
                'to' => '',
                'cc' => '',
                'bcc' => '',
                'subject' => 'Life Insurance Plan Selected',
                'body' => '<p>Dear {{customerName}} Ji,&nbsp;</p><p>&nbsp;Please find the details of the life insurance plan selected&nbsp;below.</p><p>Thank you for choosing ebeema in order to find the best insurance policy for yourself. I was very happy to hear that you were looking for insurance for yourself. I think it is a really good option for you.</p><p>Regards,</p><p><strong>Dhiraj Thapa</strong></p><p><strong>Business Operation and Sales Manager</strong><br />Kamaladi, Kathmandu<br />Tel: 01 - 4429240<br />Cell: +977-9802322097<br /><a href="http://www.ebeema.com">www.ebeema.com</a></p>',
                'default_body' => '<p>Dear {{customerName}} Ji,&nbsp;</p><p>&nbsp;Please find the details of the life insurance plan selected&nbsp;below.</p><p>Thank you for choosing ebeema in order to find the best insurance policy for yourself. I was very happy to hear that you were looking for insurance for yourself. I think it is a really good option for you.</p><p>Regards,</p><p><strong>Dhiraj Thapa</strong></p><p><strong>Business Operation and Sales Manager</strong><br />Kamaladi, Kathmandu<br />Tel: 01 - 4429240<br />Cell: +977-9802322097<br /><a href="http://www.ebeema.com">www.ebeema.com</a></p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{customerName}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],[
                'id' => '8',
                'title' => 'Life Insurance Comparison (admin compare module)',
                'key' => 'life-compare-result',
                'to' => '',
                'cc' => '',
                'bcc' => '',
                'subject' => 'Life insurance comparison result',
                'body' => '<p>Dear {{customerName}} Ji,&nbsp;</p><p>Thank you for choosing ebeema in order to find the best insurance policy for yourself. I was very happy to hear that you were looking for insurance for yourself. I think it is a really good option for you.</p><p>Regards,</p><p><strong>Dhiraj Thapa</strong></p><p><strong>Business Operation and Sales Manager</strong><br />Kamaladi, Kathmandu<br />Tel: 01 - 4429240<br />Cell: +977-9802322097<br /><a href="http://www.ebeema.com">www.ebeema.com</a></p>',
                'default_body' => '<p>Dear {{customerName}} Ji,&nbsp;</p><p>Thank you for choosing ebeema in order to find the best insurance policy for yourself. I was very happy to hear that you were looking for insurance for yourself. I think it is a really good option for you.</p><p>Regards,</p><p><strong>Dhiraj Thapa</strong></p><p><strong>Business Operation and Sales Manager</strong><br />Kamaladi, Kathmandu<br />Tel: 01 - 4429240<br />Cell: +977-9802322097<br /><a href="http://www.ebeema.com">www.ebeema.com</a></p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{customerName}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '9',
                'title' => 'User Activation',
                'key' => 'user-activated',
                'to' => '',
                'cc' => '',
                'bcc' => '',
                'subject' => 'User status changed!',
                'body' => '<p>Dear {{user}},</p><p>The status of your ebeema login of phone number: {{phoneNumber}} has been {{status}}.</p><p>Thank you for choosing,</p>',
                'default_body' => '<p>Dear {{user}},</p><p>The status of your ebeema login of phone number: {{phoneNumber}} has been {{status}}.</p><p>Thank you for choosing,</p>',
                'status' => '1',
                'notes' => '<p>In this mail template, <strong>DO NOT Modify:</strong></p><ul><li>{{user}}</li><li>{{phoneNumber}}</li><li>{{status}}</li></ul>',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

        ]);
    }
}
