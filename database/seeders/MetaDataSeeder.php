<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetaDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meta_data')->insert([
            [
                'id' => '1',
                'page_name' => 'homepage',
                'meta_title' => 'ebeema',
                'meta_description' => 'ebeema homepage',
                'note' => '<p>Ebeema home page SEO required Meta Data</p>',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '2',
                'page_name' => 'contact-page',
                'meta_title' => 'ebeema contact',
                'meta_description' => 'ebeema contact address',
                'note' => '<p>Contact page of ebeema SEO Meta Data</p>',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '3',
                'page_name' => 'about-page',
                'meta_title' => 'about ebeema',
                'meta_description' => 'ebeema related information',
                'note' => '<p>Ebeema SEO Meta data for about us page</p>',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '4',
                'page_name' => 'blog-page',
                'meta_title' => 'ebeema blog',
                'meta_description' => 'ebeema related blogs',
                'note' => '<p>Ebeema SEO Meta data for blog page</p>',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '5',
                'page_name' => 'calculator-page',
                'meta_title' => '	Insurance Calculator',
                'meta_description' => 'ebeema insurance calculator',
                'note' => '<p>Ebeema SEO Meta data for life calculator</p>',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => '6',
                'page_name' => 'nonlife-calculator-page',
                'meta_title' => 'Non Life Calculator',
                'meta_description' => 'Non Life calculator',
                'note' => '<p>Ebeema SEO Meta data for non-life calculator</p>',
                'status' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

        ]);
    }
}
