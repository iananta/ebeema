<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PackageType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cover_package_types')->insert([
            [
                'name' => 'A to B',
                'code' => 'PT001',
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'A to C',
                'code' => 'PT002',
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'A to I',
                'code' => 'PT003',
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'A to J',
                'code' => 'PT004',
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'A to N',
                'code' => 'PT005',
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Covid-19 Global Assistance Cover (Travel Medical Insurance)',
                'code' => 'PT006',
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],

        ]);

    }
}

