<?php

namesPAce Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PlanArea extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plan_areas')->insert([
            [
                'name' => 'Asian Country Ex. SAARC',
                'code' => 'PA001',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'WW ex USA & Canada',
                'code' => 'PA002',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'WW in USA & Canada',
                'code' => 'PA003',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'For SAARC Countries',
                'code' => 'PA004',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'STANDARD PLAN: WW ex USA & Canada',
                'code' => 'PA006',
                'exclude_country' => 1,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'STANDARD PLAN: WW in USA & Canada',
                'code' => 'PA007',
                'exclude_country' => 0,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'GOLD PLAN: WW ex USA & Canada',
                'code' => 'PA008',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'GOLD PLAN: WW in USA & Canada',
                'code' => 'PA009',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'PLATINUM PLAN: WW ex USA & Canada',
                'code' => 'PA010',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'PLATINUM PLAN: WW in USA & Canada',
                'code' => 'PA011',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'PLAN : ASIAN COUNTRIES',
                'code' => 'PA012',
                'exclude_country' => 0,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'PLAN : SAARC COUNTRIES',
                'code' => 'PA013',
                'exclude_country' => 0,
                'is_active' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'India|Bangladesh|Bhutan|SriLanka',
                'code' => 'PA014',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'WorldWide',
                'code' => 'PA015',
                'exclude_country' => 0,
                'is_active' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);

    }
}
