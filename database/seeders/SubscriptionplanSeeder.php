<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SubscriptionplanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('subscriptionplans')->insert([
            [
                'id' => '1',
                'title' => 'Monthly Plan',
                'price' => 150,
                'description' => '',
                'period_in_month'=>'1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => '2',
                'title' => 'Six Months Plan',
                'price' =>500,
                'description' => '',
                'period_in_month'=>'6',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => '3',
                'title' => 'Yearly Plan',
                'price' =>1000,
                'description' => '',
                'period_in_month'=>'12',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
