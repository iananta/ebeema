this.initial=function(){
	let _this=this
	this.mainMenuDropDown=function(){
		let selector=$('.mob-mnu');
		let width=$(window).width();
		let navBar=$('.navbar-toggle')
		
		if(width < 1100){
			navBar.on('click',function(){
				setTimeout(function(){
					$(this).toggleClass('active-nav')
				},600)
			})
		}
		if(width <=800){
			navBar.on('click',function(){
				$('html').toggleClass('no-scroll')
			})
		}
	},
	this.mobileMenuDropDown=function(){
		let selector=$('.mobile-menu-item a')
		selector.on('click',function(){
			let child=$(this).next()
			let parent=$(this).parent()
				parent.toggleClass('active')
			if(child.hasClass('mobile-child-drop')){
				let chervon=$(this).find('.mobile-drop-icon img')
				let chervonParent=$(this).find('.mobile-drop-icon')
					if(parent.hasClass('active'))
					{
						chervon.css('transform','rotate(180deg)')
						chervonParent.css('background','#f5f5f5')
						chervonParent.css('border-radius','4px')
						chervonParent.css('padding','3px')
						child.slideDown()
						
					}else{
						chervon.css('transform','rotate(0deg)')
						chervonParent.removeAttr('style')
						child.slideUp()
					}
			}
		})
	},
	this.bannerSliderListener=function(){
		let banner=$('.banner-slider-wrapper')
			banner.owlCarousel({
				autoplay:true,
				loop:true,
				nav:["",""],
				dots:true,
				margin:60,
				animateOut: 'slideOutUp',
     			animateIn: 'slideInUp',
				autoplayTimeout:6000,
				responsiveClass: true,
				responsive:{
					300: {
				      items: 1
				    },
					380: {
				       items: 1,
				      
				    },
					600:{
						items: 1,
					},
					1024:{
						items: 1
					},
					1200:{
						items:1
					}
				}
			});
	},
	this.videopopup=function(){
		let selector=$('.video-play-button a')
		selector.on('click',function(){
			 $('.video-pop-block').show()
		})
		$(document).on('click',function(event){
			if (!$(event.target).closest(".video-play-button a,.video-pop-wrapper").length) {
                   $('.video-pop-block').hide()
            }
		})
	},
	this.popupCloseonClickBody=function(){
		$(document).on('click',function(event){
			if (!$(event.target).closest(".view-all-product-button,.corporate-plan,.popup-form-wrapper,.pop-calculator-btn,.side-fixed-contact,.navbar-toggle,.mobile-menu-item,.oveflowHidden,.video-play-button").length) {
                   $('.popup-parent-wrapper').hide()
                   $('html').removeClass('no-scroll')
                   if($('body').hasClass('modal-open')){
                   		$('body').removeClass('modal-open')
                   }
                   if($('.popup-parent-wrapper').hasClass('in')){
                   		$(".modal ").modal('hide');
                   }
            }
		})
	},
	this.insurancepolicySlider=function(){
		let width=$(window).width();
		let selector=$('#insurance-policy')
		if(width > 800){
			selector.addClass('owl-carousel')
			selector.owlCarousel({
				items:7,
				autoplay:true,
				loop:true,
				nav:["",""],
				dots:false,
				margin:20,
				autoplayTimeout:4000,
				responsive:{
					800:{
						items: 4
					},
					1024:{
						items: 5
					},
					1100:{
						items: 6
					},
					1200:{
						items:7
					}
				}
			});
		}
	},
	this.associationSlider=function(){
		let width=$(window).width();
		let lifeselector=$('.life-slider')
		let nonlifeselector=$('.nonlife-slider')
			lifeselector.owlCarousel({
				autoplay:true,
				loop:true,
				nav:["",""],
				dots:false,
				margin:60,
				autoplayTimeout:6000,
				responsiveClass: true,
				responsive:{
					300: {
				      items: 1
				    },
					380: {
				       items: 2,
				       margin:10,
				    },
					600:{
						items: 2,
						margin:20,
					},
					1024:{
						items: 3
					},
					1200:{
						items:4
					}
				}
			});
			nonlifeselector.owlCarousel({
				autoplay:true,
				loop:true,
				nav:["",""],
				dots:false,
				margin:60,
				autoplayTimeout:6000,
				responsiveClass: true,
				responsive:{
					300: {
				      items: 1
				    },
					380: {
				      items: 2,
				      margin:10,
				    },
					600:{
						items: 2,
						margin:20,

					},
					1024:{
						items: 3
					},
					1200:{
						items:3
					}
				}
			});
	},
	this.compareRedirectionListener=function(){
		let selector=$('.insurance-product-compare')
		selector.on('submit',function(e){
			e.preventDefault();
			let data=$('.insurance-lists').val()
			let url=location.href
			window.location.href=url+'calculator?category='+data
		})
	},
	this.corporateFormPop=function(){
		let selectorCorporate='.corporate-plan'
		let targetCorporate=$('.corporate-plan-popup-form')
		let closeCorporate='.cross-btn'

		let selectornonlife='.nonlife-plan'
		let targetNonlife=$('.nonlife-popup-form')
		let closeNonlife='.nonlife-cross-btn'

		let selectorInsuranceCategory='.insurance-category-pop'
		let targetCategory=$('.insurance-category-pop-block')
		let closeCategory='.pop-cross-btn'

		formpopup(selectorCorporate,targetCorporate,closeCorporate)
		formpopup(selectornonlife,targetNonlife,closeNonlife)
		formpopup(selectorInsuranceCategory,targetCategory,closeCategory)
		
		function formpopup(selector,target,close){
			$(document).on('click',selector,function(){
				let slug=$(this).data('title')
				if(slug){
					let category=$(this).find('.ins-policy-title').html()
					let description=$('.'+slug+'-description').html()
					$('.insurance-cat-title').html(category)
					$('.category-pop-p-content').html('')
					$('.category-pop-p-content').html(description)
					let host = window.location.origin;
					$('.pop-foot-btn a').attr('href',host+'/calculator?category='+slug)

				}
				
				let animi=target.find('.popup-wrapper')
				if(animi){
					$('html').addClass('no-scroll')
					target.css('display','flex')
					animi.addClass('popup-zoom-in')
					animi.removeClass('popup-zoom-out')
				}
			})
			$(document).on('click',close,function(){
				let animi=target.find('.popup-wrapper')
				animi.removeClass('popup-zoom-in')
				animi.addClass('popup-zoom-out')
				setTimeout(function(){
					$('html').removeClass('no-scroll')
					target.css('display','none')

				},600)
			})
			
		}
		
		$('.view-plan').on('click',function(){
			let target=$(this).data('target')
			$(target).show({
				start: function() {
			        $(this).css('display', 'flex');
			    }
			})
		})
	},
	this.init=function(){
		$(document).ready(function(){
			_this.bannerSliderListener()
			_this.mainMenuDropDown();
			_this.mobileMenuDropDown()
			_this.popupCloseonClickBody()
			_this.associationSlider()
			_this.videopopup()
			$('.action-button-previous').on('click',function(e){
				e.preventDefault()
				$('html, body').animate({scrollTop:0}, '300');
			})
			$('.action-button').on('click',function(e){
				e.preventDefault()
				$('html, body').animate({scrollTop:0}, '300');
			})
			_this.compareRedirectionListener();
			_this.corporateFormPop()
			$('.moreless-button').click(function() {
				$('.moretext').slideToggle();
				if ($('.moreless-button').text() == "Read more") {
					$(this).text("Read less")
				} else {
					$(this).text("Read more")
				}
			});
			$('.moretext p').each(function(){
				if($(this).html()==' ' || $(this).html()=='&nbsp;'){
					$(this).remove()
				}
			})
			let width=$(window).width();
			if(width <= 1133){
				let selector=$('.mobile-filter-button button')
				$(document).on('click','.mobile-filter-button button',function(){
					$('.compare-calc-lists .col-sm-3').toggleClass('active')
					$('body').addClass('no-scroll')
				})
				$(document).on('click','.mb-filter-close',function(){
					$('body').removeClass('no-scroll')

					$('.compare-calc-lists .col-sm-3').removeClass('active')
				})
			}

			$(document).on('click','.benefits-button',function(){
				let parent=$(this).parent().parent()
				if($(this).hasClass('active')){
					$(this).removeClass('active')
					parent.find('td:nth-child(5)').slideUp()
				}else{
					$(this).addClass('active')
					parent.find('td:nth-child(5)').slideDown()
				}

			})
		})
	}
}
let obj=new initial();
obj.init();

this.calculationForm=function(){
	let _this=this
	this.customDropdown=function(){
		let selected=document.querySelector('.selected-item')
		let option_wrapper=document.querySelector('.option-items')
		var option_item=document.querySelectorAll('.option-item')
		selected.addEventListener("click", () => {
			option_wrapper.classList.toggle("active");
		});
		option_item.forEach(option => {
			option.addEventListener("click", () => {
				selected.querySelector('p').innerHTML = option.querySelector("p").innerHTML;
				option_wrapper.classList.remove("active");
			});
		});
	}
	this.checkEventListener=function(parent){
		let items=parent.querySelectorAll('.check-item')
		if(items){
			items.forEach(function(el,key){
				el.addEventListener("click",function(){
					el.classList.add("active");
					items.forEach(function(ell, els){
						if(key !== els) {
							ell.classList.remove('active');
						}
					});  
				})
			})
		}
		
	},
	this.compareFilterCheckboxListener=function(){
		function checkedBox(parent){
			let items=parent.find('.checkbox-item')
			let total=items.length
			if(total > 6){
				let loop=total - 6
				for(i=6;i<total;i+=loop){
					let options=items.slice(i, i+loop).wrapAll("<div class='more-filter-options'></div>");
				}
			}
			items.each(function(){
				let _this=$(this)
				_this.on('click',function(){
					$(this).toggleClass('active')
					if(_this.hasClass('active')){
						$(this).find('input').prop('checked',true)
					}else{
						$(this).find('.filter-times').remove()
					}
				})
			})
		}
		let policy=$('.policy-company')
		let benifits=$('.benifits')
		checkedBox(policy)
		checkedBox(benifits)
		if($('.policy-company .checkbox-item').length > 6)
			if($('.benifits .checkbox-item').length > 6)
				$('<div class="filter-option-show-more"><button type="button"><strong>&plus;</strong>&nbsp;&nbsp;<span>Show More</span></button></div>').insertAfter('.benifits .more-filter-options');
			$(document).on('click','.filter-option-show-more',function(){
				let target=$(this).parent().find('.more-filter-options')
				target.toggleClass('active')
				if(target.hasClass('active')){
					target.slideDown()
					$(this).find('strong').html('&minus;')
					$(this).find('button span').html('Show Less')
				}else{
					target.slideUp()
					$(this).find('strong').html('&plus;')
					$(this).find('button span').html('Show More')
				}
			})

		},

		this.init=function(){
        	//_this.customDropdown()
        	let items=document.querySelectorAll('.check-item')
        	if(items){
        		items.forEach(function(el,key){
        			el.addEventListener('click',function(){
        				let parent=el.parentNode;
        				_this.checkEventListener(parent)	
        			})
        		})
        	}
        	_this.compareFilterCheckboxListener()
        }   
    }
    let obj2=new calculationForm()
    obj2.init();

    this.setupCookie=function(){
    	let _this=this
    	this.acceptCookies=function(){
    		let cookieBox = document.querySelector('.cookie-pop');
			// Get the <span> element that closes the cookiebox
			let closeCookieBox = document.getElementById("cookieBoxok");
			closeCookieBox.onclick = function() {
				cookieBox.style.display = "none";
			};
			document.getElementById('cookie-dismiss').onclick=function(){
				cookieBox.style.display = "none";
			};
			function createCookie(name, value, days, path) {
				let expires = "";
				if (days) {
					let date = new Date();
					date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
					expires = "; expires=" + date.toGMTString();
				}
				else expires = "";
				document.cookie = name + "=" + value + expires + "; path=" + path;
			}

			function readCookie(name) {
				let nameEQ = name + "=";
				let ca = document.cookie.split(';');
				for (let i = 0; i < ca.length; i++) {
					let c = ca[i];
					while (c.charAt(0) == ' ') c = c.substring(1, c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
				}
				return null;
			}
			let cookieExpiry = 30;
			let cookiePath = "/";
			document.getElementById("cookieBoxok").addEventListener('click', function () {
				createCookie('cookiepopup', 'yes', cookieExpiry, cookiePath);
			});
			document.getElementById("cookie-dismiss").addEventListener('click', function () {
				createCookie('cookiepopup', 'yes', cookieExpiry, cookiePath);
			});
			let cookiePopup = readCookie('cookiepopup');
			if (cookiePopup != null && cookiePopup == 'yes') {
				cookieBox.style.display = 'none';
			} else {
				cookieBox.style.display = 'block';
			}
		},
		this.init=function(){
			_this.acceptCookies();
		}
	}
	let obj3=new setupCookie()
	obj3.init()

	$(document).ready(function(){
		$(document).on('click','.oveflowHidden',function(){
			$('html').addClass('no-scroll')
		})
		$(document).on('click','.oveflowVisible',function(){
			$('html').removeClass('no-scroll')
		})
		let width=$(window).width()
		if(width < 800){
			$('.insurance-drop').on('click',function(){
				$('.plans-cat').toggleClass('active')

			})
		}
    	$('.dropdown-nav-item:first-child li').each(function(){
    		if($(this).hasClass('active')){
    			let list=$(this).find('.final-dropdown').html()
    			$('.dropdown-nav-item:nth-child(2)').html(list)
    		}
    		$(this).on('click',function(){
    			menuAction($(this))
    		})
    		$(this).on('mouseenter',function(){
    			menuAction($(this))
    		})
    		function menuAction(_this){
    			if(width > 800){
    				$('.dropdown-nav-item:first-child li.active').removeClass('active')
    				_this.addClass('active')
    				let list=_this.find('.final-dropdown')
    				let li=list.html()
    				$('.dropdown-nav-item:nth-child(2)').html(li)
    			}else{
    				_this.toggleClass('active')
    			}
    		}
    		replaceImgActive($('.dropdown-nav-item li.active span.nav-heart img'));
    		$('.dropdown-nav-item li').each(function(){
    			$(this).hover(function(){
    				let img=$(this).find('a').find('.nav-heart').find('img')
    				replaceImgActive(img)
    			})
    		}).on('mouseout',function(){
    			let img=$(this).find('a').find('.nav-heart').find('img')
    			let attr=img.attr('src')
    				attr.replace('nav-heart','nonhover-heart')
    				attr=attr.replace('nav-heart','nonhover-heart')

    				img.attr('src',attr)
    		})
    		function replaceImgActive(img){
    			let attr=img.attr('src')
    				attr.replace('nonhover-heart','nav-heart')
    				attr=attr.replace('nonhover-heart','nav-heart')
    				img.attr('src',attr)

    		}
    	})


    	setTimeout(function(){
    		removeLoader()
    	},1300)
    	function removeLoader() {
    		 $("#preloader").fadeOut(500)
    	}

    	$(window).on('scroll',function(){
    		let scrollheight=$(this).scrollTop()
    		if(scrollheight > 100){
    			$('.main-navigation').addClass('sticky-header')

    		}else{
    			$('.main-navigation').removeClass('sticky-header')
    		}
    	})
	});