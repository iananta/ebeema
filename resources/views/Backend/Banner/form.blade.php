@extends('layouts.backend.containerlist')
@section('title')
    {{isset($banner) ? 'Update Banner' : 'Create Banner'}}
@endsection

@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{isset($banner) ? route('admin.banner.edit',['id'=>$banner->id])  : route('admin.banner.create')}}" enctype="multipart/form-data">
                	@csrf
                	<div class="custom-row">
                		<div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Title<label>
                            </div>
                            <div class="form-input">
        						<input data-validation="required" type="text" class="input-control"  name="title" placeholder="Title" value="{{isset($banner) ? $banner->title : old('title')}}">
                                   @error('title')
                                   	<div class="validation_error">{{ $message }}</div>
                                   @enderror
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Image<label>
                                @if(isset($banner))
                                    <a href="{{asset('uploads/banner/'.$banner->image)}}" target="_blank"><img src="{{asset('uploads/banner/'.$banner->image)}}" style="height:60px"></a>
                                @endif
                            </div>
                            <div class="form-input">
        						<input data-validation="required" type="file" class="input-control"  name="image" value="">

        						@error('image')
                                   	<div class="validation_error">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="form-input-wrapper">
                        	<div class="form-label">
                        		<label>Description</label>
                        	</div>
                        	<div class="form-input">
                        		<textarea data-validation="required" class="input-control" name="description" rows="5" placeholder="Description">{{isset($banner) ? $banner->description : old('description')}}</textarea>
                        		@error('description')
                                   	<div class="validation_error">{{ $message }}</div>
                                @enderror
                        	</div>
                        </div>
                         <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Submit</button>
                            </div>
                        </div>
                	</div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_js')
 @include('Backend.globalScripts.form-validation')
@endsection