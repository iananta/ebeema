@extends('layouts.backend.containerlist')
@section('title')
    Banners
@endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="table-action-wrapper">
            <div class="action-button-list">
                <a href="{{route('admin.banner.create')}}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create</a>
            </div>
        </div>
        <table class="dataTable">
            <thead>
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Title</th>
                        <th>Image</th>

                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($banners as $key=>$banner)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$banner->title}}</td>
                            <td><img src="{{asset('uploads/banner/'.$banner->image)}}" alt="{{$banner->image}}" style="height:100px"></td>
                            <td>{{$banner->description}}</td>
                            <td>
                                <a href="{{route('admin.banner.edit',['id'=>$banner->id])}}" class="btn btn-primary btn-sm edit-modal-btn action-btn"><i class="fa fa-edit"></i></a>
                                <a href="{{route('admin.banner.delete',['id'=>$banner->id])}}" class="btn btn-danger delete-lead btn-sm action-btn"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </thead>
        </table>
    </div>
</div>
@endsection