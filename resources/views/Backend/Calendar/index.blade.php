@extends('layouts.backend.containerform')

@section('title')
   Calendar
@endsection
@section('footer_js')
@endsection
@section('dynamicdata')
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container">
            <a href="/" class="navbar-brand">Outlook Calendar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a href="/dashboard" class="nav-link {{$_SERVER['REQUEST_URI'] == '/' ? ' active' : ''}}">Home</a>
                    </li>
                    @if(isset($userName))
                        <li class="nav-item" data-turbolinks="false">
                            <a href="{{--/user/calendar/view--}}/admin/full-calender-meeting" class="nav-link{{$_SERVER['REQUEST_URI'] == '/calendar' ? ' active' : ''}}">Calendar</a>
                        </li>
                    @endif
                </ul>
                <ul class="navbar-nav justify-content-end">
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="https://docs.microsoft.com/graph/overview" target="_blank">--}}
{{--                            <i class="fa fa-external-link mr-1"></i>Docs--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    @if(isset($userName))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                @if(isset($user_avatar))
                                    <img src="{{ $user_avatar }}" class="rounded-circle align-self-center mr-2" style="width: 32px;">
                                @else
                                    <i class="fa fa-user-circle fa-lg rounded-circle align-self-center mr-2" style="width: 32px;"></i>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <h5 class="dropdown-item-text mb-0">{{ $userName }}</h5>
                                <p class="dropdown-item-text text-muted mb-0">{{ $userEmail }}</p>
                                <div class="dropdown-divider"></div>
                                <a href="/signout" class="dropdown-item">Sign Out</a>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a href="/signin" class="nav-link">Sign In</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <main role="main" class="container">
        @if(session('error'))
            <div class="alert alert-danger" role="alert">
                <p class="mb-3">{{ session('error') }}</p>
                @if(session('errorDetail'))
                    <pre class="alert-pre border bg-light p-2"><code>{{ session('errorDetail') }}</code></pre>
                @endif
            </div>
    @endif
            <div id="accordion">
                <div class="card">
                    <div class="card-header" data-toggle="collapse" data-target="#UserOutlookDetails" aria-expanded="true"
                         aria-controls="UserOutlookDetails" id="OutlookDetails">
                        <h5 class="mb-0 text-capitalize btn-link">
                            Click here to change Outlook Details   <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                        </h5>
                    </div>

                    <div id="UserOutlookDetails" class="collapse" aria-labelledby="OutlookDetails" data-parent="#accordion">
                        <div class="card-body">
                            <form class="p-2" method="POST" action="{{route('user.outlook.details')}}"
                                  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="oc_id" value="{{Auth::user()->outlookCredentials->id ?? ''}}">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id ?? ''}}">
                                <div class="form-group">
                                    <label for="OAUTH_APP_ID">OAUTH APP ID</label>
                                    <input type="text" name="OAUTH_APP_ID" value="{{Auth::user()->outlookCredentials->OAUTH_APP_ID ?? ''}}"
                                           class="form-control" placeholder="Enter oauth app id">
                                </div>
                                <div class="form-group">
                                    <label for="OAUTH_APP_SECRET">OAUTH APP SECRET</label>
                                    <input type="text" name="OAUTH_APP_SECRET" value="{{Auth::user()->outlookCredentials->OAUTH_APP_SECRET ?? ''}}"
                                           class="form-control" placeholder="Enter oauth app secret key">
                                </div>
                                <div style="clear: both">
                                    <button type="submit" class="btn btn-primary">Save Details</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="jumbotron">
                <h1>Outlook Calendar</h1>
                <p class="lead">This shows all the Microsoft Outlook Calendar Tasks and generate.</p>
                @if(isset($userName))
                    <h4>Welcome {{ $userName }}!</h4>
                    <p><a class="btn btn-primary btn-sm" href="{{--/user/calendar/view--}}/admin/full-calender-meeting">View Calendar</a></p>
                @else
                    <a href="/signin" class="btn btn-primary btn-large">Click here to sign in</a>
                @endif
            </div>
@endsection
