@extends('layouts.backend.containerlist')

@section('title')
    Calender Lists
@endsection
<style>
    input.form-control.form-control-sm {
        border: 1px solid black !important;
        width: 40% !important;
    }
</style>

@section('footer_js')
    <script>
        $(document).ready(function() {
            $('#calendar').DataTable( {
                "scrollX": true,
                "ordering": false
            } );
        } );
    </script>
@endsection

@section('dynamicdata')
    <section class="p-5">
        <div class="text-center">
        <h1>Outlook Calendar</h1>
        <h2>{{ $dateRange }}</h2>
        </div>
        <a class="btn btn-primary btn-sm mb-3" href={{route('user.calendar.new.event')}}>New event</a>
                @include('layouts.backend.alert')
        <table id="calendar" class="table">
            <thead>
            <tr>
                <th class="font-weight-bold" scope="col">Organizer</th>
                <th class="font-weight-bold" scope="col">Subject</th>
                <th class="font-weight-bold" scope="col">Start</th>
                <th class="font-weight-bold" scope="col">End</th>
                <th class="font-weight-bold" scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @isset($events)
                @foreach($events as $event)
                    <tr>
                        <td>{{ $event->getOrganizer()->getEmailAddress()->getName() }}</td>
                        <td>{{ $event->getSubject() }}</td>
                        <td>{{ \Carbon\Carbon::parse($event->getStart()->getDateTime())->format('n/j/y g:i A') }}</td>
                        <td>{{ \Carbon\Carbon::parse($event->getEnd()->getDateTime())->format('n/j/y g:i A') }}</td>
                        <td>
                            <form method="post" action="{{route('user.calendar.event.delete')}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$event->getId()}}">
                                <button type="submit" onclick="return confirm('Delete entry?')" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </section>
@endsection
