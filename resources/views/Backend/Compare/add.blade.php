<div class="form-row align-items-center company-wrapper" id="option_{{ $id }}">
        <div class="form-group">
            <label for="company_{{ $id }}" class="font-weight-bold">Companies</label>
            <select
                id="company_{{ $id }}"
                name="companies[]"
                class="custom-select search-select input-control"
                onchange="findProducts({{ $id }})"
                data-validation="required"
            >
                <option selected value="">Select a company</option>
                @foreach($companies as $company)
                    @if(count($company->products) > 0)
                        <option
                            value="{{ $company->id }}"
                            data-products="{{ $company->products }}"
                            data-categories="{{ $company->products->pluck('category') }}"
                        >
                            {{ $company->name }}
                        </option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="product_{{ $id }}" class="font-weight-bold">Policies</label>
            <select id="product_{{ $id }}" name="products[]" class="custom-select search-select searchCompFeature input-control policy-product" data-validation="required">
                <option selected value="">Select a product</option>
            </select>
        </div>
        <div class="form-group">
            <button
                class="btn btn-danger mt-2 w-100"
                data-id="{{ $id }}"
                onclick="removeRow({{ $id }})"
            >&minus;
            </button>
        </div>
</div>
