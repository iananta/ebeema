@extends('layouts.backend.containerlist')

@php
    $amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
    $amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
@endphp

@section('footer_js')
@endsection

@section('header_css')
    <style>
        .table td, .table th {
            padding: .50rem !important;
        }

        .compare_header {
            display: flex;
            justify-content: space-between;
        }

        .compare_person_detail table, tr, td {
            border: 1px solid rgba(108, 155, 209, 0.2) !important;
        }

        .compare_header > .compare_tagline {
            font-style: italic;
        }

        .compare_person_detail {
            margin-top: 2rem;
            width: 60%;
            font-size: 12px;
        }

        .cell_highlighted {
            background: linear-gradient(0deg, rgba(108, 155, 209, 0.1), rgba(108, 155, 209, 0.1));
        }

        .compare_result_table {
            margin-top: 2rem;
        }

        .compare_result_table table th {
            background: #6C9BD1;
            box-shadow: inset -0.5px 0px 0px rgba(0, 0, 0, 0.25);
            color: #ffffff;
            text-align: center;
            font-weight: normal;
        }

        .compare_result_table table td {
            text-align: center;
        }

        .compare_footer {
            display: flex;
            justify-content: space-evenly;
            margin-top: 5rem;
            align-items: center;
            font-size: 12px;
        }

        .compare_footer div {
            display: flex;
        }

        .compare_footer div i {
            width: 20px;
            height: 20px;

            border: 0.5px solid rgba(33, 33, 33, 0.2);
            box-sizing: border-box;
            border-radius: 24px;

            display: flex;
            justify-content: center;
            align-items: center;
            margin-right: 5px;
        }

        .compare_footer .fa-facebook {
            color: #019BFF;
        }

        .compare_footer .fa-instagram {
            color: #9802AF;
        }

        .compare_footer .fa-twitter {
            color: #3378E5;
        }
        .companylogo{
            text-align: center;
        }
    </style>
@endsection

@section('title')
    <p class="h4 align-center mb-0">t</p>
@endsection

@section('dynamicdata')
    <div class="container">
        <div class="compare_content">
            <div class="compare_header">
                <div class="compare_tagline">Discover the Best Insurance Plans for you and your family!</div>
                <div class="compare_date">{{ now()->format('d F, Y') }}</div>
            </div>

            <div class="compare_person_detail">
                <table class="table table-bordered">
                    <tr>
                        <td>Name</td>
                        <td class="cell_highlighted">{{ $clientName }}</td>
                        <td class="cell_highlighted">Mode of Payment</td>
                        <td class="cell_highlighted">Plan Recommended</td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td class="cell_highlighted">{{ $selectedAge }} Years</td>
                        <td rowspan="3" class="text-center">Yearly</td>
                        <td rowspan="3" class="text-center">Plan</td>
                    </tr>
                    <tr>
                        <td>Est. Sum Insured</td>
                        <td class="cell_highlighted">{{ $amountFormatter->format($selectedSumAssured) }}</td>
                    </tr>
                    <tr>
                        <td>Term</td>
                        <td class="cell_highlighted">{{ $selectedTerm }} Years</td>
                    </tr>
                </table>
            </div>

            <div class="compare_result_table">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Insurer</th>
                        <th>Policy Name</th>
                        <th>Age & Term</th>
                        <th>Premium (Investment)</th>
                        <th>Bonus</th>
                        <th>Return at Maturity</th>
                        <th>Net Gain</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td class="companylogo">
                                <img
                                    src="{{public_path($product->company->logo)}}"
                                    alt="{{$product->company->name}}"
                                    width="90"
                                >
                            </td>
                            <td>{{$product->company->name}}</td>
                            <td>{{ $product->currentAge }}Y & {{ $product->currentTerm }}Y</td>
                            <td>Rs. {{ $amountFormatter->format($product->premiumAmountWithBenefit) }}</td>
                            <td>Rs. {{ $amountFormatter->format($product->bonus) }}</td>
                            <td class="cell_highlighted">Rs. {{ $amountFormatter->format($product->totalPremiumAmount) }}</td>
                            <td>Rs. {{ $amountFormatter->format($product->netGain) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="compare_footer">
                <img src="{{ public_path('uploads/ebeema_logo.png') }}" height="50" alt="LOGO">
                <div>Kamaladi Marg, Kamaladi Kathmandu</div>
                <div>+977-9803136585</div>
                <div>hi@ebeema.com</div>
                <div><i class="fa fa-twitter"></i> ebeema_</div>
                <div><i class="fa fa-instagram"></i>ebeema.nepal</div>
                <div><i class="fa fa-facebook"></i>Ebeema Official</div>
            </div>
        </div>

    </div>
@endsection
