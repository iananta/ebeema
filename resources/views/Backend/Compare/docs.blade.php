<!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252s">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>Compare PDF</title>

        @php
        $amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
        $amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
        $amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
        @endphp

        <style>
            @page {
                size: A4 landscape;
            }
            @font-face {
                font-family: 'Open Sans';
                src: url("/frontend/fonts/open-sans.regular.ttf") format('truetype');
            }
            @font-face {
                font-family: 'karla';
                src: url("/frontend/fonts/karla.regular.ttf") format('truetype');
            }

            html{
              font-family: 'Open Sans';
          }
          .page-break {
            page-break-after: always;
        }
        .table-bg{
            background: #F0F5FA;
        }
   /* .border-wrapper{
        border: 0.5px solid #6C9BD1;
        }*/
        table{
            border-collapse: collapse;
            border:1px solid rgba(108, 155, 209, 0.2);
            font-family: 'Open Sans' !important;
        }
        tr,td,th{
            border:1px solid rgba(108, 155, 209, 0.2);
            font-family: 'Open Sans' !important;
        }
        .top-header{
            width: 100%;
            border: 0px;
        }
        .top-header tr td .compare_tagline,.top-header tr td .compare_date{
            font-size: 16px;
            color: #212121;
            font-family: karla;
            font-weight: normal;

        }
        .top-header tr td .compare_tagline{
            font-style: italic;
        }

        .client-info-table {
            width: 70%;
            border-collapse: collapse;
            border:1px solid rgba(108, 155, 209, 0.2);

        }
        .client-info-table tbody tr, .client-info-table tbody tr td {
            border: 1px solid rgba(108, 155, 209, 0.2);
            padding: 7px 15px;
            font-family: 'Open Sans';
        }
        .client-info-table tbody tr th {
            text-align: left;
            padding: 7px 15px;
            font-size: 14px;
            width: 20%;
            font-family: 'Open Sans';
        }
        .client-info-table tbody tr td{
            font-size: 14px;
        }
        .plan-list {
            padding-left: 0;
            list-style: none;
        }
        .plan-list li{
            color: #2E74B5;
            font-weight: 700;
        }
        .company-list{
            width: 100%;
            margin-top: 30px;
            border-collapse: collapse;
            border: 1px solid #ccc;
            font-family: 'Open Sans' !important;
        }
        .company-list thead tr th {
            background: #6C9BD1;
            color: #fff;
            font-size: 14px;
            font-weight: 600;
            line-height: 15px;
            border: 1px solid #ccc;
            padding: 7px 8px;
            font-family: 'Open Sans';
        }
        .company-list tbody tr td{
            padding: 7px 8px;
            border: 1px solid #ccc;
            font-size: 14px;
            line-height: 22px;
            font-family: 'Open Sans';
        }
        .footer_table {
            margin-top: 5rem;
            font-size: 12px !important;
            width: 100%;
            bottom: 5%;
        }

        .footer_table, .footer_table tr, .footer_table td {
            border: none !important;
        }
        .footer_table tr td{
            font-size: 14px;
            color: #343434;
        }
        .footer_table tr td div{
            font-size: 14px;
        }
        .footer_table tr td div a span{
            color: #343434;
            font-size:14px;
        }

        .product_benefits {
            font-size: 13px;
        }

        #watermark{
            position: fixed;
            width: 30cm;
            height: 30cm;
        }
        .product_benefits ul{
            font-size: 16px;
            padding-left:0px;
        }
        .product_benefits ul li{
            position: relative;
            margin-left: 15px;
        }
       .product_benefits ul li::before {
            position: absolute;
            content: '';
            background: #111;
            width: 6px;
            height: 6px;
            left: -15px;
            top: 10px;
            border-radius: 50%;
        }
        li {
            display: flex;
            line-height: 25px;
            align-items: center;
            font-size: 15px;
            list-style: circle;
        }
        li p{
            margin: 0px;
            font-size: 15px;
        }
        .product_benefits.position-relative h4 {
            font-size: 21px;
        }
        .product_benefits.position-relative p strong {
            font-size: 18px;
        }
        .invoice-table {
            width: 100%;
        }
        .invoice-table thead tr th{
            background: #6C9BD1;
            color: #fff;
        }
        .invoice-table thead tr th,.invoice-table tfoot tr th{
            font-size: 14px;
            font-weight: 600;
            padding: 7px 8px;
        }
        {
            text-align: center;
            font-size: 14px;
            padding: 7px 0px;
        }
         .invoice-table tr {
            border: 1px solid #f4f4f4;
        }
        .totalling {
            background: #fafafa;
        }
        .totalling td {
            font-weight: 600;
            font-size: 14px !important;
        }
    </style>

</head>
<body style="font-family: 'Open Sans';">
@php
    $match =1;
    $rowspan='';
    if($selectedMop == 'half_yearly'){
        $rowspan=2;
    }elseif($selectedMop == 'quarterly'){
        $rowspan=4;
    }elseif($selectedMop == 'monthly'){
        $rowspan=12;
    }else{
        $rowspan=1;
    }
@endphp
    <div class="pdf-wrapper">
        <div class="border-wrapper">
            <div class="pdf-header-wrapper" style="display: block;overflow:hidden;width: 100%;margin-bottom:30px">
                <table class="top-header" style="border:0px">
                    <tr style="border:0px">
                        <td style="float:left;width:80%;border:0px">
                            <div class="compare_tagline">Discover the Best Insurance Plans for you and your family!</div>
                        </td>
                        <td style="float:right;width:20%;border:0px">
                            <div class="compare_date"style="text-align:right">{{ now()->format('d F, Y') }}</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="personal-information">
             <table class="client-info-table">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <td class="table-bg" style="width:20%">{{ $clientName }}</td>
                        <td class="table-bg" style="width:20%">Mode Of Payment</td>
                        <td class="table-bg">Plan Recommended</td>
                    </tr>
                    <tr>
                        <th>Age</th>
                        <td class="table-bg">{{ $selectedAge }} Years</td>
                        <td class="center" rowspan="3">{{ str_replace('_', ' ', ucfirst($selectedMop)) }}</td>
                        <td rowspan="3">
                            <ul class="plan-list">
                                <li>{{  ucfirst($selectedCategory) }}</li>
                            </ul>
                        </td>

                    </tr>
                    <tr>
                        <th>Est.Sum Insured</th>
                        <td class="table-bg">{{ $amountFormatter->format($selectedSumAssured) }}</td>
                    </tr>
                    <tr>
                        <th>Term</th>
                        <td class="table-bg">{{$selectedTerm }} Years</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br>
        <div class="compare-list">
          <table class="company-list">
            <thead>

                <tr>
                    <th class="center" style="width:19%">Insurer</th>
                    <th class="left" style="width:15%">Policy Name</th>
                    <th class="center" style="width:2%">Term</th>
                    <th class="left" style="width:10%">Premium (Investment)</th>
                    <th class="left" style="width:10%">Bonus</th>
                    <th class="left" style="width:27%">Return at Maturity</th>
                    <th class="left" style="width:27%">Net Gain</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                <tr>
                    <td class="center" style="text-align:center;padding:10px 0px;">
                        @if(file_exists(asset('images/company/'.$product->company->logo)))
                            <img
                            src="{{asset('images/company/'.$product->company->logo)}}"
                            alt="{{$product->company->name}}" style="width:120px;height: 75px;margin-top:10px;"
                            >
                        @else
                            {{$product->company->name}}
                        @endif
                        </td>
                            <td >
                             {{$product->name}} {{($features) ? '('.implode(',',$features).')' : ''}}
                         </td>
                         <td class="center">{{ $product->currentTerm }}Y</td>
                         <td>Rs. {{ $amountFormatter->format($product->premiumAmountWithBenefit) }}</td>
                         <td>Rs. {{ $amountFormatter->format($product->bonus) }}</td>
                         <td class="table-bg"> Rs. {{ $amountFormatter->format($product->totalPremiumAmount) }}</td>
                         <td>Rs. {{ $amountFormatter->format($product->netGain)}}</td>

                     </tr>
                     @endforeach
                 </tbody>

             </table>
         </div>
   </div>
</div>
@foreach ($products as $product)
    <div class="page-break" style="page-break-after: always;"></div>
    @if($selectedCategory == 'money-back')
       <h4 style="font-size:18px">Invoice for {{$product->name}}</h4>
        @include('Backend.Compare.includes.invoice-table')
    @endif
    @if($product->benefit_details)
    <div class="product_benefits position-relative">
        <h4 style="font-size:18px">Benefit of {{ $product->name }}:</h4>
        {!! $product->benefit_details !!}
       
    </div>
    @endif
@endforeach
 <div class="pdf-footer" style="position:fixed;bottom:-50px;left:0px;width:100%">
            <table class="footer_table position-absolute" style="border:0px;width:100%;">
                <tr  style="border:0px">
                    <td style="border:0px;width:20%">
                        <img src="{{ public_path('uploads/ebeema_logo.png') }}" style="width:95%;margin-top:-30px"  alt="LOGO">
                    </td>
                    <td style="border:0px;width:20%;text-align:center">
                        <div style="margin-top:20px;font-size:14px;border-right:1px solid #c4c4c4 !important;line-height:18px;"><span style="margin-right:9px">Kamaladi Marg, Kamaladi Kathmandu</span></div>
                    </td>
                    <td style="border:0px;width:12%;text-align:center">
                        <div style="margin-top:20px;font-size:14px;border-right:1px solid #c4c4c4 !important;line-height:18px;"><span style="margin-left:8px!important;;margin-right:8px!important;">+01-4429240</span></div>
                    </td>
                    <td style="border:0px;width:12%;text-align:center">
                        <div style="margin-top:20px;font-size:14px;"><span >hi@ebeema.com</span></div>
                    </td>
                    <td style="border:0px;text-align:right">
                        <div style="height:15px;vertical-align: middle"><a href="" class="instagram"><img src="{{asset('frontend/social/Twitter.png')}}" style="margin-right:12px;margin-top:6px"><span style="margin-right:13px">ebeema.nepal</span></a></div>
                    </td>
                    <td style="border:0px;text-align:right">
                        <div style="height:15px"><a href="" class="instagram"><img src="{{asset('frontend/social/Instagram.png')}}" style="margin-right:12px;margin-top:6px"><span style="margin-right:13px">ebeema.nepal</span></a></div>
                    </td>
                    <td style="border:0px;text-align:right">
                       <div style="height:15px"><a href="" class="instagram"><img src="{{asset('frontend/social/Facebook.png')}}" style="margin-right:12px;margin-top:6px"><span>ebeema.nepal</span></a></div>
                   </td>
               </tr>
           </table>
       </div>
</body>
</html>

