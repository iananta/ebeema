<html>
<head>
    <title>Compare Policy</title>
</head>
<body>
<div class="pdf-wrapper">
    <div class="border-wrapper">
        <div class="pdf-header-wrapper" style="display: block;overflow:hidden;width: 100%;margin-bottom:30px">
            <table class="top-header" style="border:0px">
                <tr style="border:0px">
                    <td style="float:left;width:80%;border:0px">
                        <div class="compare_tagline">Discover the Best Insurance Plans for you and your family!</div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="personal-information">
            <table class="client-info-table">
                <tbody>
                <tr>
                    <th>Name</th>
                    <td class="table-bg">{{ $clientName }}</td>
                    <td class="table-bg">Mode Of Payment</td>
                    <td class="table-bg">Plan Recommended</td>
                </tr>
                <tr>
                    <th>Age</th>
                    <td class="table-bg">{{ $selectedAge }} Years</td>
                    <td rowspan="3">{{ $selectedMop }}</td>
                    <td rowspan="3">
                        <ul class="plan-list">
                            <li>{{ $selectedCategory }}</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>Est.Sum Insured</th>
                    <td class="table-bg">{{ $selectedSumAssured }}</td>
                </tr>
                <tr>
                    <th>Term</th>
                    <td class="table-bg">{{ $selectedTerm }} Years</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="compare-list">
            <table class="company-list">
                <thead style="background:red;">

                    <tr>
                        <th style="background:red;">Insurer</th>
                        <th>Policy Name</th>
                        <th>Term</th>
                        <th>Premium (Investment)</th>
                        <th>Bonus</th>
                        <th>Return at Maturity</th>
                        <th>Net Gain</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>
                           {{--<img
                               src="{{asset('images/company/'.$product->company->logo)}}"
                               alt="{{$product->company->name}}" />--}}
                       </td>
                        <td>
                            {{$product->name}} {{($features) ? '('.implode(',',$features).')' : ''}}
                        </td>
                        <td>{{ $product->currentTerm }}Y</td>
                        <td>Rs. {{ $product->premiumAmountWithBenefit }}</td>
                        <td>Rs. {{ $product->bonus }}</td>
                        <td> Rs. {{ $product->totalPremiumAmount }}</td>
                        <td>Rs. {{ $product->netGain}}</td>

                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
@foreach ($products as $product)
    @if($product->benefit_details)
        <div class="page-break"></div>
        <div class="product_benefits position-relative">
            <h4>Benefit of {{ $product->name }}:</h4>
            {!! $product->benefit_details !!}
        </div>
    @endif
@endforeach
</body>
</html>

