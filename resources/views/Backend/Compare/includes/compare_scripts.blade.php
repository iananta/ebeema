<script>
	let i = 1;
	function onCategorySelection() {
		const category = $('option:selected', `#category`).val();
		let ageDiv = $('.age_container');
		let coupleDiv = $('.couple_container');
		let childDiv = $('.child_container');
		let companyDiv=$('.company_container')
		const style = 'd-none'
		if(category){
			companyDiv.removeClass(style)
		}else{
			companyDiv.addClass(style)
		}
		switch (category) {
			case 'couple':
			ageDiv.addClass(style)
			coupleDiv.removeClass(style)
			childDiv.addClass(style)
			break;
			case 'children':
			ageDiv.addClass(style)
			coupleDiv.addClass(style)
			childDiv.removeClass(style)
			break;
			case 'education':
			ageDiv.addClass(style)
			coupleDiv.addClass(style)
			childDiv.removeClass(style)
			break;
			default:
			ageDiv.removeClass(style)
			coupleDiv.addClass(style)
			childDiv.addClass(style)
			break;
		}

		$.ajax({
			type: "GET",
			url: "{{ route('admin.policy.features') }}",
			dataType: 'json',
			data: {
				'category': category,
			},
			success: function (response) {

				if (response.data.length > 0) {
					let html = '';
					for (let j = 0; j < response.data.length; j++) {
						html += ' <div class="form-check">' +
						'<input type="checkbox" name="features[]" class="form-check-input" value="' + response.data[j].code + '">' +
						'<label class="form-check-label text-capitalize">' + response.data[j].name + '</label>' +
						'</div>'
					}
					$('#featuresListing').html(html);
					$('#featuresRow').removeClass('d-none')

				} else {
					$('#featuresRow').addClass('d-none')
				}
			}
		});

		$.ajax({
			type: "GET",
			url: "{{ route('admin.policy.category') }}",
			dataType: 'json',
			data: {
				'category': category,
			},
			success: function (response) {
				if (response.id) {

					let minAge = response.min_age ? response.min_age : 1;
					let maxAge = response.max_age ? response.max_age : 70;

					let minTerm = response.min_term ? response.min_term : 1;
					let maxTerm = response.max_term ? response.max_term : 70;

					let ages = "<option value='' selected>Select Age</option>";
					for (let i = minAge; i <= maxAge; i++) {
						ages += "<option value='" + i + "'>" + i + "</option>";
					}

					if (category === 'children' || category === 'education') {
						$('#proposer_age').html(ages);
					} else if (category === 'couple') {
						$('#wife_age').html(ages);
						$('#husband_age').html(ages);
					} else {
						$('#age').html(ages);
					}

					let terms = "<option value='' selected>Select Term</option>";
					for (let j = minTerm; j <= maxTerm; j++) {
						terms += "<option value='" + j + "'>" + j + "</option>";
					}
					$('#term').html(terms);
				}
			}
		});
	}
	function removeRow(id) {
		$(`#option_${id}`).remove();
	}

	$('#add_more_btn').click(function (e) {
		let products = $("select[name='products[]']")
		.map(function () {
			return $(this).val();
		}).get();

		$.ajax({
			url: '/admin/compare/add',
			data: {id: i, products: JSON.stringify(products)},
			type: 'get',
			success: function (res) {
				let error=$('.append_fields .validation_error')
				if(error){
					error.remove()
				}
				$('.append_fields').append(res)
				i++;
			},
			error: function (err) {
				console.log(err);
			}
		})
	});

	function findProducts(id) {
		let data = $('option:selected', `#company_${id}`).attr('data-products');
		let cat = $('option:selected', `#company_${id}`).attr('data-categories');
		let category = $('option:selected', `#category`).val();
		if(data){
			data = JSON.parse(data);
		}
		if(cat){
			categories = JSON.parse(cat);
		}

            let html = '';
            if (categories.includes(category)) {
            	html += '<option selected disabled>Select a Product</>';
            	for (let i = 0; i < data.length; i++) {
            		if (category === data[i].category) {
            			html += '<option ' +
            			'value="' + data[i].id +
            			'" data-category="' + data[i].category +
            			'"' +
            			'>'
            			+ data[i].name + '</option>'
            		}
            	}
            } else {
            	html += '<option selected disabled>No Product Found</>';
            }
            $(`#product_${id}`).html(html);
        }

        $(document).on('change','.policy-product',function(){
        	let target=$(this)
        	let parent=target.parent().parent()
        	if(parent.find('.product-required-feature')){
				parent.find('.product-required-feature').remove()
        	}
        	let id=target.val()
        	$.ajax({
        		url: '{{route("admin.findProduct.feature")}}',
				data:{
					_token:"{{csrf_token()}}",
					id:id
				},
				type: 'post',
				success:function(response){
					parent.append('<p class="product-required-feature">Features : '+response.features+'</p>')
				}
        	})
        })


    </script>