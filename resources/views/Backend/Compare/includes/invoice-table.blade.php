<style>
    .invoice-pdf{
        vertical-align: middle !important
    }
</style>
@php
$match =1;
$rowspan='';
    if($selectedMop == 'half_yearly'){
        $rowspan=2;
    }elseif($selectedMop == 'quarterly'){
        $rowspan=4;
    }elseif($selectedMop == 'monthly'){
        $rowspan=12;
    }else{
        $rowspan=1;
    }
@endphp
<?php $totalBackAmount=0;$totalPercentageBack=0;  ?>
<table class="table invoice-table" id="payback-table">
                            <thead>
                            <tr>
                                <th colspan="6">Term</th>
                                <th  colspan="3" class="text-capitalize">{{ str_replace('_', ' ', ucfirst($selectedMop)) }} Premium</th>
                                @if($product->paybackSchedules->count() > 0)
                                    <th colspan="3">Money Back</th>
                                @endif
                                <th colspan="3">Bonus</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for($i=1;$i<=$product->currentTerm;$i++)
                                @for($j=1;$j <= $subTerms;$j++)
                                    <tr >
                                        <td colspan="6">
                                            {{  str_pad($i, 2, '0', STR_PAD_LEFT) }}
                                            {{-- {{ $i }}--}}
                                            @if($subTerms > 1)
                                                @php
                                                    $match=$j;
                                                @endphp
                                                <small>
                                                    {{  str_pad($j, 2, '0', STR_PAD_LEFT) }}
                                                    {{--{{ $j }}--}}
                                                </small>
                                            @endif
                                        </td>
                                        <td colspan="3">
                                            @php
                                                if($match == 1){
                                                    $amount = 0;
                                                }
                                            @endphp
                                            @if ($isChildPlan && $i == 1)
                                                 @php   
                                                    $amount +=round(($product->premiumAmountWithBenefit / $subTerms) + $product->crcAmount);
                                                 @endphp
                                                {{ $amountFormatter->format(($product->premiumAmountWithBenefit / $subTerms) + $product->crcAmount) }}
                                            @else
                                                @if($product->hasPayingTerm)
                                                    @if($i <= $product->payingTerm)
                                                        @php
                                                            $amount+=round($product->premiumAmountWithBenefit / $subTerms);
                                                        @endphp
                                                        {{ $amountFormatter->format($product->premiumAmountWithBenefit / $subTerms) }}
                                                    @else
                                                        -
                                                    @endif
                                                @else
                                                    @php
                                                        $amount+=round($product->premiumAmountWithBenefit / $subTerms);
                                                    @endphp
                                                    {{ $amountFormatter->format($product->premiumAmountWithBenefit / $subTerms) }}
                                                @endif
                                            @endif

                                        </td>
                                        @if($product->paybackSchedules->count() > 0)
                                            <td colspan="3">
                                                @if($product->paybackSchedules->contains('payback_year', $i)  && $j == 1)

                                                    @foreach($product->paybackSchedules as $schedule)
                                                        @if($schedule->payback_year == $i)
                                                            <?php 
                                                                $totalPercentageBack=$totalPercentageBack + $schedule->rate;
                                                                $totalBackAmount=$totalBackAmount + $schedule->amount ;  
                                                            ?>
                                                            {{ $amountFormatter->format($schedule->amount) }}
                                                        @endif
                                                    @endforeach
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        @endif
                                        @if($match == 1)

                                        <td class="invoice-pdf" colspan="3" rowspan="{{$rowspan}}" style="vertical-align: middle !important"><div class="bonus-amount">{{ $amountFormatter->format($product->bonusYearly) }}</div></td>
                                        @endif
                                    </tr>
                                    @if($match == $rowspan && $selectedMop != 'yearly')
                                    <tr class="totalling">
                                        <td>Total</td>
                                        <td colspan="12">Rs {{$amountFormatter->format($amount)}}</td>
                                    </tr>
                                    @endif
                                @endfor

                            @endfor

                            </tbody>
                            <tfoot>
                                               {{-- <tr>
                                                    <th colspan="6">Total Premium</th>
                                                    <th colspan="{{ $product->paybackSchedules->count() > 0 ? 6 : 3 }}"></th>
                                                     <th colspan="3">Rs.{{$amountFormatter->format($product->actualPremium)}}</th>
                                                </tr> --}}
                                                @if($product->paybackSchedules->count() > 0 && $totalPercentageBack < 100)
                                                <tr>
                                                    <th colspan="6">Left over Money back</th>
                                                    <th colspan=" 3"></th>
                                                    <th> Rs.{{$selectedSumAssured - $totalBackAmount}}</th>
                                                </tr>
                                                @endif
                                                <tr>
                                                    <th colspan="6">Total Bonus at Maturity</th>
                                                    <th colspan="{{ $product->paybackSchedules->count() > 0 ? 6 : 3 }}"></th>
                                                    <th colspan="3">Rs.{{ $amountFormatter->format($product->bonus) }}</th>
                                                </tr>
                                                <tr>
                                                     <th colspan="6">Sum Insured</th>
                                                    <th colspan="{{ $product->paybackSchedules->count() > 0 ? 6 : 3 }}"></th>
                                                    <th colspan="3">Rs.{{$selectedSumAssured}}</th>
                                                </tr>
                                                {{--@if($product->paybackSchedules->count() > 0)
                                                <tr>
                                                    <th colspan="6">Pay Back</th>
                                                    <th colspan="{{ $product->paybackSchedules->count() > 0 ? 6 : 3 }}"></th>
                                                    <th colspan="3">Rs.{{ $amountFormatter->format($product->paybackSchedules->sum('amount')) }}</th>
                                                </tr>
                                                @endif --}}
                                               {{-- <tr>
                                                    <?php
                                                        $bonus=$product->bonus;
                                                        $investment=$product->actualPremium;
                                                        $payback=$product->paybackSchedules->count() > 0 ? $product->paybackSchedules->sum('amount') : 0;
                                                    ?>
                                                    <th colspan="6">Total Return</th>
                                                    <th colspan="{{ $product->paybackSchedules->count() > 0 ? 6 : 3 }}"></th>
                                                    <th colspan="3">Rs{{ $amountFormatter->format($bonus + $selectedSumAssured) }}</th>
                                                </tr> 
                                                <tr>
                                                    <?php
                                                        $totalreturn=$bonus + $selectedSumAssured;

                                                    ?>
                                                  <!--   <th colspan="6">Net Return </th>
                                                    <th colspan="{{ $product->paybackSchedules->count() > 0 ? 6 : 3 }}"></th>
                                                    <th colspan="3">Rs{{ $amountFormatter->format($totalreturn - $investment) }}</th> -->
                                                </tr> --}}
                                            </tfoot>
                        </table>