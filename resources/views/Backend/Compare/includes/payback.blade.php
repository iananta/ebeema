
@foreach($products as $product)
    @if($product->paybackSchedules)
        <div class="modal fade popup-parent-wrapper" id="paybackSchedules{{$product->id}}{{$loop->iteration}}" tabindex="-1"
             aria-labelledby="paybackSchedules{{$product->id}}{{$loop->iteration}}Label" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content popup-form-wrapper">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Payback Schedule</h5>
                        <button type="button" class="close oveflowVisible" data-dismiss="modal"
                                aria-label="Close"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="frontend payback schedule"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @include('Backend.Compare.includes.invoice-table')
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach
