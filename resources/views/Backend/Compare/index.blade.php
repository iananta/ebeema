@extends('layouts.backend.containerlist')
@section('title')
    Policy Compare
@endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
         @include('layouts.backend.alert')
         <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data policy-compare-form" action="{{ route('admin.policy.compare') }}" method="POST">
                    @csrf
                    <div class="custom-row">
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Client Name</label>
                            </div>
                             <div class="form-input">
                                <input placeholder="Client Name" type="text" name="full_name" id="full_name" class="input-control" data-validation="required">
                             </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Email Address</label>
                            </div>
                            <div class="form-input">
                                <input placeholder="Email Address" type="email" name="email" id="email" class="input-control" data-validation="required">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Category</label>
                            </div>
                            <div class="form-input">
                                <select name="category" id="category" class="input-control" onchange="onCategorySelection()" data-validation="required">
                                    <option value="">Select Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->category_code }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Sum Assured</label>
                            </div>
                            <div class="form-input">
                                <input type="number" id="sum_assured" name="sum_assured" class="input-control"
                                       placeholder="Enter Sum to be Assured" data-validation="required">
                            </div>
                        </div>
                        <div class="form-input-wrapper age_container">
                            <div class="form-label">
                                <label>Age</label>
                            </div>
                            <div class="form-input">
                                <select id="age" name="age" class="input-control" data-validation="required">
                                    <option value="" selected>Select Age</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper couple_container d-none">
                            <div class="form-label">
                                <label>Husband Age</label>
                            </div>
                            <div class="form-input">
                                <select name="husband_age" id="husband_age" class="input-control" data-validation="required">
                                    <option value="">Select Age</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper couple_container d-none">
                            <div class="form-label">
                                <label>Wife Age</label>
                            </div>
                            <div class="form-input">
                                <select name="wife_age" id="wife_age" class="input-control"  data-validation="required">
                                    <option value="">Select Age</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper child_container d-none">
                            <div class="form-label">
                                <label>Child Age</label>
                            </div>
                            <div class="form-input">
                                <select name="child_age" id="child_age" class="input-control"  data-validation="required">
                                    <option value="">Select Age</option>
                                    @for($i = 0; $i <= 18; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper child_container d-none">
                            <div class="form-label">
                                <label>Proposer's Age</label>
                            </div>
                            <div class="form-input">
                                <select name="proposer_age" id="proposer_age" class="input-control"  data-validation="required">
                                    <option value="">Select Age</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Term</label>
                            </div>
                            <div class="form-input">
                                <select id="term" name="term" class="input-control" data-validation="required">
                                     <option value="" selected>Select an Term</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Mode Of Payment</label>
                            </div>
                            <div class="form-input">
                                 <select id="mop" name="mop" class="input-control" data-validation="required">
                                    <option value="" selected>Select MOP</option>
                                    @foreach($mops as $mop)
                                        <option value="{{ $mop }}" class="text-capitalize">
                                            {{ str_replace('_', ' ', ucfirst($mop)) }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper d-none company_container">
                            <div class="form-label">
                                <button type="button" id="add_more_btn" class="add-company-btn">Add Company</button>
                            </div>
                            <div class="form-input">
                                <div class="append_fields">

                                </div>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label></label>
                            </div>
                            <div class="form-input">
                                 <div id="featuresRow" class="form-group mt-4 d-none">
                                    <div id="featuresListing">

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" class="form-submit-btn">Submit</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="form-input-details-wrapper">

            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_js')

    @include('Backend.Compare.includes.compare_scripts')
    @include('Backend.globalScripts.form-validation')
    <script>
        $('.policy-compare-form').on('submit',function(e){
            let company=$('.append_fields .company-wrapper');
            let message='<p class="validation_error">You have to add company for compare policy</p>'
            console.log(company.length)
            if(company.length == 0){
                $('.append_fields').append(message)
                e.preventDefault()
            }
        })
    </script>
@endsection
