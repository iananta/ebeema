@extends('layouts.backend.containerlist')
@section('title')
    Policy Compare Result
@endsection
@php
$amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
$amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
$amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
@endphp
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        <div class="request-wrapper">
            <ul>
                <li><strong>Proposer's Name </strong><span>{{$clientName}}</span></li>
                <li><strong>Proposer's Email</strong><span>{{$clientEmail}}</span></li>
                @if($isCouplePlan)
                <li><strong>Husband Age</strong><span>{{ $selectedHusbandAge }}</span></li>
                <li><strong>Wife Age</strong><span>{{ $selectedWifeAge }}</span></li>
                <li><strong>Average Age</strong><span>{{ $averageAge }}</span></li>
                @elseif($isChildPlan)
                 <li><strong>Child Age</strong><span>{{ $selectedChildAge }}</span></li>
                <li><strong>Proposer's Age</strong><span>{{ $selectedProposersAge }}</span></li>
                @else
                <li><strong>Proposer's Age</strong><span>{{ $selectedAge }}</span></li>
                @endif
                <li><strong>Selected Term</strong><span>{{$selectedTerm}}</span></li>
                <li><strong>Sum Assured</strong><span>{{$amountFormatter->format($selectedSumAssured) }}</span></li>
                <li><strong>Mode of Payment</strong><span>{{ str_replace('_', ' ', ucfirst($selectedMop)) }}</span></li>
                <li><strong>Plan Selected</strong><span>{{ str_replace('_', ' ', ucfirst($selectedCategory)) }}</span></li>
            </ul>
        </div>
        <div class="action-info-wrapper">
            <div class="excel-action">
                 <form action="/admin/compare" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="mail_pdf" value="1">
                    <input type="hidden" name="full_name" value="{{Request::input('full_name') ?? ''}}">
                    <input type="hidden" name="email" value="{{Request::input('email') ?? ''}}">
                    <input type="hidden" name="category" value="{{Request::input('category') ?? ''}}">
                    <input type="hidden" name="sum_assured" value="{{Request::input('sum_assured') ?? ''}}">
                    <input type="hidden" name="age" value="{{Request::input('age') ?? ''}}">
                    <input type="hidden" name="child_age" value="{{Request::input('child_age') ?? ''}}">
                    <input type="hidden" name="proposer_age" value="{{Request::input('proposer_age') ?? ''}}">
                    <input type="hidden" name="term" value="{{Request::input('term') ?? ''}}">
                    <input type="hidden" name="mop" value="{{Request::input('mop') ?? ''}}">
                    <input type="hidden" name="companies[]"
                    value="{{json_encode(Request::input('companies') ?? '')}}">
                    <input type="hidden" name="products" value="{{json_encode(Request::input('products') ?? '')}}">
                    <input type="hidden" name="features" value="{{json_encode(Request::input('features') ?? '')}}">
                    <button type="submit" class="">Email PDF</button>
                </form>
            </div>
            <div class="excel-action">
                 <form class="ml-2" action="/admin/compare" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="download_pdf" value="1">
                    <input type="hidden" name="full_name" value="{{Request::input('full_name') ?? ''}}">
                    <input type="hidden" name="email" value="{{Request::input('email') ?? ''}}">
                    <input type="hidden" name="category" value="{{Request::input('category') ?? ''}}">
                    <input type="hidden" name="sum_assured" value="{{Request::input('sum_assured') ?? ''}}">
                    <input type="hidden" name="age" value="{{Request::input('age') ?? ''}}">
                    <input type="hidden" name="child_age" value="{{Request::input('child_age') ?? ''}}">
                    <input type="hidden" name="proposer_age" value="{{Request::input('proposer_age') ?? ''}}">
                    <input type="hidden" name="term" value="{{Request::input('term') ?? ''}}">
                    <input type="hidden" name="mop" value="{{Request::input('mop') ?? ''}}">
                    <input type="hidden" name="companies[]"
                    value="{{json_encode(Request::input('companies') ?? '')}}">
                    <input type="hidden" name="products" value="{{json_encode(Request::input('products') ?? '')}}">
                    <input type="hidden" name="features" value="{{json_encode(Request::input('features') ?? '')}}">
                    <button type="submit" class="">Export Pdf</button>
                </form>
            </div>
            <div class="word-action">
                <form class="ml-2" action="/admin/compare" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="download_word" value="1">
                    <input type="hidden" name="full_name" value="{{Request::input('full_name') ?? ''}}">
                    <input type="hidden" name="email" value="{{Request::input('email') ?? ''}}">
                    <input type="hidden" name="category" value="{{Request::input('category') ?? ''}}">
                    <input type="hidden" name="sum_assured" value="{{Request::input('sum_assured') ?? ''}}">
                    <input type="hidden" name="age" value="{{Request::input('age') ?? ''}}">
                    <input type="hidden" name="child_age" value="{{Request::input('child_age') ?? ''}}">
                    <input type="hidden" name="proposer_age" value="{{Request::input('proposer_age') ?? ''}}">
                    <input type="hidden" name="term" value="{{Request::input('term') ?? ''}}">
                    <input type="hidden" name="mop" value="{{Request::input('mop') ?? ''}}">
                    <input type="hidden" name="companies[]"
                    value="{{json_encode(Request::input('companies') ?? '')}}">
                    <input type="hidden" name="products" value="{{json_encode(Request::input('products') ?? '')}}">
                    <input type="hidden" name="features" value="{{json_encode(Request::input('features') ?? '')}}">
                    <button type="submit" class="">Export Word</button>
                </form>
            </div>
        </div>
        <div class="table-wrapper">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Insurer</th>
                        <th>Policy Name</th>
                        <th>Age & Term</th>
                        <th class="text-capitalize">{{ str_replace('_', ' ', $selectedMop) }} Premium (Investment)</th>
                        <th>Total Bonus (as per today's rate)</th>
                        <th>Total Return on Maturity</th>
                        <th>Total Investment</th>
                        @if($selectedCategory != 'money-back')
                        <th>Net Gain</th>
                        @endif
                    </tr>
                </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td style="width: 20%;text-align:center;" class="compare-parts line-rht-cmp" scope="row">
                                    <img style="width:120px;height:75px" src="{{asset('/images/company/'.$product->company->logo)}}">
                                    <p class="cont-plan">{{$product->company->name}}</p>
                                </td>
                                <td>
                                    {{ $product->name }}
                                    <div class="download-wrapper">
                                            @foreach($product->uploadProductPdf as $pdf)
                                           <p> <a target="_blank" href="{{ route('admin.product.downloadProductFile', ['productFileId' => $pdf->id]) }}">{{$pdf->type == 'form' ? 'Form' : 'Brochure'}}</a></p>
                                            @endforeach
                                    </div>
                                </td>
                                <td>Age: {{ $product->currentAge }}Y Term: {{ $product->currentTerm }}Y</td>
                                <td>
                                    @if($selectedMop == 'yearly')
                                    {{ $amountFormatter->format($product->premiumAmountWithBenefit) }}
                                    @elseif($selectedMop == 'half_yearly')
                                    {{ $amountFormatter->format($product->premiumAmountWithBenefit/2) }}
                                    @elseif($selectedMop == 'quarterly')
                                    {{ $amountFormatter->format($product->premiumAmountWithBenefit/4) }}
                                    @elseif($selectedMop == 'monthly')
                                    {{ $amountFormatter->format($product->premiumAmountWithBenefit/12) }}
                                    @endif
                                    @if(count($product->features) > 0)
                                    <br>
                                    <a
                                    class="text-primary mt-2"
                                    data-toggle="collapse"
                                    href="#benefits{{$product->id}}{{$loop->iteration}}"
                                    role="button"
                                    aria-expanded="false"
                                    aria-controls="benefits{{$product->id}}{{$loop->iteration}}"
                                    >
                                    <small class="small-feature">Features</small>
                                </a>
                                <div class="collapse" id="benefits{{$product->id}}{{$loop->iteration}}">
                                    <ul class="feature-list">
                                        @foreach($product->features as $feature)
                                        <li class="text-primary text-capitalize">
                                            @if($product->has_loading_charge_on_features)
                                            <small class="">
                                                {{ featureName($feature['code']) }} with MOP
                                                : {{ $feature['mopAmount'] }}
                                            </small>
                                            @else
                                            <small>
                                                {{ featureName($feature['code']) }}
                                                : {{ $feature['amount'] }}
                                            </small>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </td>
                            <td>{{ $amountFormatter->format($product->bonus) }}</td>
                            <td>
                                {{ $amountFormatter->format($product->totalPremiumAmount) }}
                                @if($product->paybackSchedules)
                                <br>
                                <a
                                type="button"
                                class="text-primary mt-2"
                                data-toggle="modal"
                                data-target="#paybackSchedules{{$product->id}}{{$loop->iteration}}"
                                >
                                <small class="view-pay">View Payback Schedule</small>
                            </a>
                            @endif
                        </td>

                        <td>{{ $amountFormatter->format($product->actualPremium) }}</td>
                        @if($selectedCategory != 'money-back')
                        <td class="d-table-cell">{{ $amountFormatter->format($product->netGain) }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('Backend.Compare.includes.payback')
@endsection
