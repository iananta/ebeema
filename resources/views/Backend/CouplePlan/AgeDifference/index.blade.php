@extends('layouts.backend.containerlist')

@section('title')
    Age Difference in Couple Plan
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-header with-border c-btn-right d-flex-row ">
        <div class="justify-content-end list-group list-group-horizontal ">
            <a href="{{route('admin.age-difference.create')}}"><button
                    class="btn btn-primary c-primary-btn add-modal shadow-none mx-2"><img
                        src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                </button></a>
        </div>
    </div>
    <div class="box-body px-4">
        <div class="dataTables_wrapper dt-bootstrap4">

            @include('layouts.backend.alert')

            <table id="example1" class="table table-bordered table-hover role-table">
                <thead>
                    <tr>
                        <th>Age Difference Between Husband and Wife</th>
                        <th>Additional Age for Small Age</th>
                        <th>Company</th>
                        <th>Product</th>
                        <th class="dt-center">Actions</th>
                    </tr>
                </thead>
                <tbody id="tablebody">

                    @foreach ($ageDifference as $index => $ageDiff)
                    <tr class="gradeX" id="row_{{ $ageDiff->id }}">
                        <td class="name">
                            {{ $ageDiff->age_difference }}
                        </td>
                        <td class="name">
                            {{ $ageDiff->add_age }}
                        </td>
                        <td class="name">
                            {{ $ageDiff->company->name }}
                        </td>
                        <td class="name">
                            {{ $ageDiff->product->name }}
                        </td>
                        <td class="justify-content-center">
                            <a href="{{route('admin.age-difference.edit', $ageDiff->id)}}" id="{{ $ageDiff->id }}"
                                title="Edit">
                                <button class="btn btn-primary btn-flat">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>&nbsp;

                            <a href="javascript:;" title="Delete" class="delete-ageDifference" id="{{ $ageDiff->id }}">
                                <button type="button" class="btn btn-danger btn-flat">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('footer_js')
<script type="text/javascript">
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    });
    $('#tablebody').on('click', '.delete-ageDifference', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (response) {
            if (response.isConfirmed == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/age-difference') }}" + "/" + id,
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        new swal(
                            'Success',
                            response.message,
                            'success'
                        )
                        $object.parent().parent().remove()
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        }).catch(swal.noop);
    });
</script>
@endsection
