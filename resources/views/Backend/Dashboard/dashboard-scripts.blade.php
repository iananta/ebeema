<script type="text/javascript">
    $(document).on('click', '.add-modal', function () {
            $('#addModal').modal('show');
            $('.modal-title').text('Add Customer');
        });
    $('.close').on('click', function () {
                $('div.modal.show').removeClass("show");
                $('body').removeClass("modal-open");
                $('div.modal-backdrop').removeClass("modal-backdrop");
                $('div.modal.fade').css("display", "none");
    });

    $('.date_filter_btn').on('click',function(){
        let endDate=$('.customer_date').val()
        $("#recentlyaddedcustomerdatatable_table").on('preXhr.dt', function (e, settings, data) {
            data.endDate =endDate;
        }).data({
            "serverSide": true,
            "processing": true,
            "ajax": {
                url: "{{route('dashboard')}}",
                type: "GET"
            }
        });
        LaravelDataTables.recentlyaddedcustomerdatatable_table.ajax.reload()
    })

    $(document).on('click',function(event){
        if (!$(event.target).closest(".follow-up-form,.follow_up_edit").length) {
            $('.followup-form-wrapper').slideUp();
            $('html').removeAttr('style')
        }
    })
    $(document).on('click','.follow_up_edit',function(){
        let date=new Date($(this).data('follow'))
        let id=$(this).data('id')
        $('.follow_up_date').val(new Date(date.getTime()-date.getTimezoneOffset()*60000).toISOString().substring(0,19))
        $('.follow_up_id').val(id)
        $('html').css('overflow','hidden')
        $('.followup-form-wrapper').slideDown({
            start:function(){
                $(this).css('display','flex')
            }
        })
    })
    $('.follow-dismis').on('click',function(){
        $('.followup-form-wrapper').slideUp()
        $('html').removeAttr('style')

    })
    $('.follow-form-date').on('submit',function(e){
        e.preventDefault()
        let formData=$(this).serializeArray()
         $.ajax({
            type:'post',
            dataType: 'json',
            url:'{{route("follow.update")}}',
            data:formData,
            success:function(response){
                $('.followup-form-wrapper').slideUp()
                setTimeout(function(){
                    new swal('Success',response.message,'success')
                    LaravelDataTables.followupdatatable_table.ajax.reload()
                },500)
            },error:function(response){
                new swal('Error','Somthing were wrong','error')
            }

        })

    })
    this.filterInit=function(){
        this.filterListener=function(){
            $('.filter-button').on('click',function(){
                let target=$(this)
                let date=$(this).data('date');
                let leadNumber=$('.leads_number'),
                    leadPercentage=$('.leads_per')
                    customerNumber=$('.customers_number')
                    customersPercentage=$('.customers_per')
                    usersNumber=$('.users_number')
                    customersPercentage=$('.users_per'),
                    salesNumber=$('.sales_number'),
                    salesPercentage=$('.sales_percentage')
                $.ajax({
                    type:'post',
                    dataType: 'json',
                    url:'{{route("filter.revenue")}}',
                    data:{
                        _token:"{{csrf_token()}}",
                        date:date
                    },
                    success:function(response){
                        let data=response.data
                        $('.filter-button').removeClass('active')
                        target.addClass('active')
                        leadNumber.html(data.totalCalculation.totalLeads)
                        customerNumber.html(data.totalCalculation.totalCustomers)
                        usersNumber.html(data.totalCalculation.totalUsers)
                        salesNumber.html(data.totalCalculation.totalSales)

                        leadPercentage.html(data.percentageCalculation.percentageLeads)
                    }
                })
                $('.filter-button').removeClass('active')

            })
        },
        this.init=function(){
            this.filterListener()
        }
    }
    let filterObj=new filterInit()
        filterObj.init()
  </script>
