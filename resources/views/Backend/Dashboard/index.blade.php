@extends('layouts.backend.container')
@section('title')
    Dashboard
@endsection
{{-- @section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection --}}
@section('dynamicdata')
    @if(!Auth::User())
    <div class="row mt-2">
        <!-- leads -->
        <div class="col-lg-6 col-sm-12">
            <div class="info-box">
            <span class="info-box-icon bg-org user-img">
                <img style="max-width: 100%" src="/uploads/user/{{Auth::User()->image_icon != null ? Auth::User()->image_icon : 'avatar.png'}}">
            </span>
                <div class="info-box-content mr-1">
                    <span class="info-box-text">{{__("Welcome To eBeema")}}</span>
                    <h3>Hello, {{Auth::User()->username}}</h3>
                    <p style="margin-top: 30px;">
                        <a href="{{ route('user.kyc.entry') }}">KYC<i class="fa fa-long-arrow-right"></i></a>
                        @if (Auth::User()->kyc && Auth::User()->kyc->is_verified == 1)
                            <span class="badge badge-pill badge-success p-2">Verified</span>
                        @else
                            <span class="badge badge-pill badge-danger p-2">Not Verified</span>
                        @endif
                    </p>
                </div>
            </div>
        </div>
        <!-- leads -->

    @if(control('create-leads'))
        <!-- leads -->
            <div class="col-lg-6 col-sm-12">
                <div class="info-box">
            <span class="info-box-icon bg-org">
                    <i class="fa fa-users"></i>
            </span>
                    <div class="info-box-content mr-1">

                        <span class="info-box-text">Total  Leads</span>
                        <h3 >{{isset($leadsCount) ?? '' }}</h3>

                        <p style="margin-top: 30px;">
                            <a href="{{ route('admin.leads.index') }}">Quick Link<i class="fa fa-long-arrow-right"></i></a>
                        </p>
                    </div>
                </div>
            </div>
            <!-- leads -->
        @endif
    </div>
    @else
     <section class="dashboard-numbers">
         <div class="table-heading">
            <div class="table-title">
                <h2>Revenue</h2>
            </div>
            <div class="table-action">
                <button class="today-button filter-button active" data-date="">Today</button>
                <button class="yesterday-button filter-button" data-date="{{date('Y-m-d',strtotime('yesterday'))}}">Yesterday</button>

            </div>
        </div>
        <div class="dashboard-number-wrapper">
            <div class="dashboard-number-item">
                <div class="number-content-wrapper">
                    <p>Total Leads</p>
                    <div class="number-content">
                       <div class="number">
                           <h1 class="leads_number">{{$getTotalCalculation['totalLeads']}}</h1>
                       </div>
                       <!-- <div class="up-down up-green">
                           <span class="leads_per">{{@$getPercentageCalculation['percentageLeads']}}</span><i class="fa fa-arrow-up"></i>
                       </div> -->

                    </div>
                </div>
            </div>
            <div class="dashboard-number-item">
                <div class="number-content-wrapper">
                    <p>Total Customer</p>
                    <div class="number-content">
                       <div class="number">
                           <h1 class="customers_number">{{$getTotalCalculation['totalCustomers']}}</h1>
                       </div>
                      <!--  <div class="up-down up-green">
                           <span class="customers_per">11%</span><i class="fa fa-arrow-up"></i>
                       </div>
 -->
                    </div>
                </div>
            </div>
            <div class="dashboard-number-item">
                <div class="number-content-wrapper">
                    <p>Total Sales</p>
                    <div class="number-content">
                       <div class="number">
                           <h1 class='sales_number'>{{$getTotalCalculation['totalSales']}}</h1>
                       </div>
                     <!--   <div class="up-down up-green">
                           <span class="sales_per">11%</span><i class="fa fa-arrow-up"></i>
                       </div> -->

                    </div>
                </div>
            </div>
            <div class="dashboard-number-item">
                <div class="number-content-wrapper">
                    <p>Total User</p>
                    <div class="number-content">
                       <div class="number">
                           <h1 class="users_number">{{$getTotalCalculation['totalUsers']}}</h1>
                       </div>
                      <!--  <div class="up-down up-green">
                           <span class="users_per">11%</span><i class="fa fa-arrow-up"></i>
                       </div> -->

                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(control('life-leadership-board'))
    <section class="leadership-section">
         <div class="table-heading">
            <div class="table-title">
                <h2>Life Insurance Leadership Board</h2>
            </div>
        </div>
        {!! $lifeLeadershipDataTable->table() !!}
        
    </section>
    @endif
   <!--  @if(control('nonlife-leadership-board'))
     <section class="leadership-section">
         <div class="table-heading">
            <div class="table-title">
                <h2>Nonlife Insurance Leadership Board</h2>
            </div>
        </div>
        {!! $nonlifeLeadershipDataTable->table() !!}
        
    </section>
    @endif -->
    <section class="recently-added-customer">
        <div class="table-heading">
            <div class="table-title">
                <h2>Follow Up</h2>
            </div>
        </div>
        {!! $followDataTable->table() !!}
    </section>
    @if(Auth::user())
    <section class="recently-added-customer">
        <div class="table-heading">
            <div class="table-title">
                <h2>Recently added Customer</h2>
            </div>
            <div class="table-action">
                <a href="javascript:;" class="create-action add-modal" data-toggle="modal"
                        data-target="#addModal"><i class="fa fa-user-plus"></i> <span>Add new user</span></a>
                <a href="{{ url('admin/customers')}}" class="view-action"><i class="fa fa-eye"></i><span>View All</span></a>
                <div class="date_filter_wrapper">
                    <input type="date" name="date_added" class="customer_date" id="customer_date">
                    <button class="date_filter_btn" id="date_filter">Filter</button>
                </div>
            </div>

        </div>
       {!! $recentlyCustomer->table() !!}
    </section>
    <div class="followup-form-wrapper">
        <div class="follow-up-form">
            <div class="follow-title">
                <h1>Update Follow Up Date</h1>
                <button class='follow-dismis'>&times;</button>
            </div>
            <form method="post" class="follow-form-date">
                @csrf
                <input type="hidden" name="follow_up_id" class="follow_up_id">
                <input type="datetime-local" name="follow_up_date" class="follow_up_date">
                <button type="submit" class='follow-submit'>Submit</button>
            </form>
        </div>
    </div>
     @include('Backend.Leads.Leads._addleadmodule')
     @include('Backend.Leads.AllLeads.includes.lead-edit')
    @endif
    @endif
@endsection
@section('foot_js')
   
   {!! $recentlyCustomer->scripts() !!}
   {!! $followDataTable->scripts() !!}
   {!! $lifeLeadershipDataTable->scripts() !!}
   {!! $nonlifeLeadershipDataTable->scripts() !!}
   @include('Backend.Dashboard.dashboard-scripts')
   @include('Backend.Leads.AllLeads.includes.leads-script')
   <script type="text/javascript">
        /*function addPositionImage(){
             $("#lifeleadershipdatatableTable").on('preXhr.dt', function (e, settings, data) {
                }).data({
                   "initComplete": function(settings, json) {
                        if($('.leadership-section table tr:first-child td:first-child')){
                             $('.leadership-section table tr:first-child td:first-child').append('<span class="position-image"><img src="{{asset("backend/img/first.svg")}}"  /></span>')
                        }
                        if( $('.leadership-section table tr:nth-child(2) td:first-child')){
                            $('.leadership-section table tr:nth-child(2) td:first-child').append('<span class="position-image"><img src="{{asset("backend/img/second.png")}}"  /></span>')
                        }
                        if( $('.leadership-section table tr:nth-child(3) td:first-child')){
                             $('.leadership-section table tr:nth-child(3) td:first-child').append('<span class="position-image third-img"><img src="{{asset("backend/img/first.svg")}}"  /></span>')
                        }
                    }
                });
        }
        addPositionImage()*/
   </script>
@endsection
