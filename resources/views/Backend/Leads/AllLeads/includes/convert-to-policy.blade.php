
<li class="convert-policy-item">
    <strong>Company</strong>
    <select class="lead-edit-insurance_company_name"  name='company_idArr[]' required="required">
        <option value="" select>Select</option>
        @foreach($companies as $company)
        <option value="{{$company->id}}" {{(@$plan->company_id  == $company->id) ? 'selected' : ''}}>{{$company->name}}</option>
        @endforeach
    </select>
</li>
<li class="@if(isset($plan)){{!empty(@$plan->product_id) ? '' : 'd-none'}}@endif convert-policy-item">
    <strong>Product</strong>
    <select id="lead-edit-product_edit" class="lead_product_id"  name='product_idArr[]' required=required>
        <option value="" selected>Select</option>
    </select>
</li>
@if(isset($from) && $from== 'lead')
 <input type="hidden" name="is_user" value="1">
 <input type="hidden" name="customer_update" value="1">
@else
<li class="convert-policy-item">
    <strong>Term</strong>
    <select name="termArr[]" required="required">
        <option selected value="">Select</option>
        @for($i=1;$i<=50;$i++)
        <option value="{{$i}}" {{(@$plan->term == $i) ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select>
</li>
@endif
<li class="convert-policy-item">
    <strong>Premium</strong>
    <input type="number" name="premiumArr[]" required="required" placeholder="Enter Premium" value="{{@$plan->premium ?? @$lead->premium }}">
</li>
<li class="convert-policy-item">
    <strong>MOP</strong>
    <select name="mopArr[]" required>
        <option value="">Select Mop</option>
        <option value="yearly" {{(@$plan->mop == 'yearly') ? 'selected' : ''}}>Yearly</option>
        <option value="half_yearly" {{(@$plan->mop == 'half_yearly') ? 'selected' : ''}}>Half Yearly</option>
        <option value="quarterly" {{(@$plan->mop == 'quarterly') ? 'selected' : ''}}>Quarterly</option>
        <option value="monthly" {{(@$plan->mop == 'monthly') ? 'selected' : ''}}>Monthly</option>
    </select>
</li>
<li class="convert-policy-item">
    <strong>Issued Date</strong>
    <input type="date" name="issued_dateArr[]" placeholder="Enter Issued Date" required='required' value="{{@$plan->issued_date}}">
</li>
<li class='convert-policy-item'><strong>Policy Number</strong><input type="number" name="policy_numberArr[]" value="{{@$plan->policy_number}}" placeholder="Enter Policy Number" required="required"></li>