@csrf

@if($from=='lead')
<ul>
    @if(control('view-all-lead-table'))
        <li>
            <strong>User Role</strong>
            <select class="role-change">
                <option selected>Select</option>
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
        </li>
        <li class="d-none">
            <strong>Sales Person</strong>
            <select class="lead-edit-user_id" id="lead-edit-user_id" name="user_id">
                <option selected>Select</option>
            </select>
        </li>
    @endif
    <li>
        <strong>Lead Source</strong>
        <select class="" id="lead-edit-leadsource_id" name="leadsource_id">
            <option>Select</option>
                @foreach($lead_sources as $leadsource)
                    <option value="{{$leadsource->id}}" {{($lead->leadsource_id == $leadsource->id) ? 'selected' : '' }}>{{$leadsource->name}}</option>
                @endforeach
        </select>
    </li>
    <li>
        <strong>Lead Type</strong>
        <select id="lead-edit-leadtype_id" name="leadtype_id">
            <option >Select</option>
            @foreach($lead_types as $leadtype)
                <option value="{{$leadtype->id}}" {{($lead->leadtype_id == $leadtype->id) ? 'selected' : '' }}>{{$leadtype->type}}</option>
            @endforeach
        </select>
    </li>
    <li>
        <strong>Policy Type</strong>
        <select id="lead-edit-policy_type" name="policy_type" required="required">
            <option value="" selected>Select</option>
            <option value="1" {{$lead->policy_type == 1 ? 'selected' : ''}}>Life</option>
            <option value="2" {{$lead->policy_type == 2 ? 'selected' : ''}}>Non-Life</option>
            <option value="3" {{$lead->policy_type == 3 ? 'selected' : ''}}>Corporate Plan</option>

        </select>
    </li>                                
    <li>
        <strong>Category</strong>
        <select id="lead-edit-category" name="category">
            <option value="">Select</option>
            @foreach($policy_sub_categories as $category)
                <option value="{{$category->category_code}}" {{($lead->category == $category->category_code) ? 'selected' : ''}}>{{ucfirst($category->name)}}</option>
            @endforeach
    </select>
    </li> 
    <li>
        <strong>Maturity Period</strong>
            <select id="lead-edit-maturity_period" name="maturity_period" required="required">
                    <option>Select</option>
                    @for($i=1;$i<=50;$i++)
                        <option value="{{$i}}" {{$lead->maturity_period == $i ? 'selected' : ''}}
                    >{{$i}}</option>
                    @endfor
            </select>
    </li>
    <li><strong>Sum Insured</strong><input id="lead-edit-sum_insured" type="number" name="sum_insured" value="{{$lead->sum_insured}}" placeholder="Enter Sum Insured"></li> 
    
        <li class="customer-btn-wrapper">
            <strong></strong>
            <button type="button" class="edit-lead-bttn">Submit</button>
            @if(count($lead->planSelecteds) == 0)
            <button type="button" class="convert-policy-btn" data-category='{{$lead->category}}'>Convert to Policy</button>
                @endif
        </li>

</ul>
@else
<ul>
    @if(control('view-all-lead-table'))
      <li>
            <strong>User Role</strong>
            <select class="role-change">
                <option selected>Select</option>
                @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
        </li>
        <li>
            <strong>Sales Person</strong>
            <select class="lead-edit-user_id" id="lead-edit-user_id" name="user_id">
                <option selected>Select</option>
            
            </select>
        </li>
    @endif
    <li>
        <strong>Lead Source</strong>
        <select id="lead-edit-leadsource_id" name="leadsource_id">
            <option>Select</option>
                @foreach($lead_sources as $leadsource)
                    <option value="{{$leadsource->id}}" {{($lead->leadsource_id == $leadsource->id) ? 'selected' : '' }}>{{$leadsource->name}}</option>
                @endforeach
        </select>
    </li>
    <li>
        <strong>Lead Type</strong>
        <select id="lead-edit-leadtype_id" name="leadtype_id">
            <option >Select</option>
            @foreach($lead_types as $leadtype)
                <option value="{{$leadtype->id}}" {{($lead->leadtype_id == $leadtype->id) ? 'selected' : '' }}>{{$leadtype->type}}</option>
            @endforeach
        </select>
    </li>
    <li>
        <strong>Policy Type</strong>
        <select id="lead-edit-policy_type" name="policy_type">
            <option selected>Select</option>
            <option value="1" {{$lead->policy_type == 1 ? 'selected' : ''}}>Life</option>
            <option value="2" {{$lead->policy_type == 2 ? 'selected' : ''}}>Non-Life</option>
        </select>
    </li>
    @if($plans=$lead->planSelecteds)
    <div class="my-plan-wrapper">
        @foreach($plans as $key=>$plan)
            <div class="my-plan-item-wrapper">
                <div class="my-plan-btn">
                    <h1>{{$lead->customer_name}} (Plan {{$key + 1}})</h1>
                    <button type="button" class='toggle-my-plan'><i class="fa fa-arrow-circle-o-down"></i></button>
                </div>
                <div class="my-plan-details">
                    <li>
                        <strong>Category</strong>
                        <select id="policy_sub_cat" name="categoryArr[]" required="required">
                            <option selected value="">Select</option>
                            @foreach($policy_sub_categories as $category)
                                <option value="{{$category->category_code}}" {{($plan->category == $category->category_code) ? 'selected' : ''}}>{{ucfirst($category->name)}}</option>
                            @endforeach
                        </select>
                    </li>
                    @include('Backend.Leads.AllLeads.includes.convert-to-policy')
                    <input type="hidden" name="plan_idArr[]" value="{{$plan->id}}">
                    <input type="hidden" name="plan_selected" value="1">
                    <button type="button" class="premium_paid_btn">Add Premium Paid Details</button>
                    <div class="premium_paid_details_form_wrapper">
                        @foreach($plan->premiumPaidByCustomer as $premium)
                            <div class="premium_form">
                                 <div class="premium-group">
                                    <label>Premium Amount</label>
                                    <input id="premium_amount_{{$premium->id}}" type="number" name="premium_amount" required="required" value="{{$premium->premium_amount}}">
                                </div>
                                <div class="premium-group">
                                    <label>Paid Date <button class="update-premium-btn" type ="button"  data-id="{{$premium->id}}">Edit</button></label>
                                    <input id="paid_date_{{$premium->id}}" type="date" name="paid_date" required="required"  value="{{$premium->paid_date}}">
                                </div>
                                <div class="premium-group">
                                    <label>Status</label>
                                    <select name="status" id="status_{{$premium->id}}">
                                        <option value="pending" {{$premium->status == 'pending' ? 'selected' : ''}}>Pending</option>
                                        <option value="paid" {{$premium->status == 'paid' ? 'selected' : ''}}>Paid</option>

                                    </select>
                                </div>
                                <input type="hidden" name="paid_id" value="{{$premium->id}}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div> 
    @endif  
    <li class="customer-btn-wrapper">
            <strong></strong>
            <button type="button" class="edit-lead-bttn">Submit</button>
    </li>      
</ul>
@endif
<input type="hidden" name="id" value="{{$lead->id}}">