
    <div id="leads-info-{{$lead->id}}" class="leads-info">
        <div class="leads-info-wrapper">
             <div class="form-loader"><i class="fa fa-spinner fa-spin"></i></div>
            <div class="lead-info-header">
                <div class="lead-name">
                    <h5>{{$lead->customer_name}}</h5>
                </div>
                <div class="lead-action-button">
                    <button class="lead-dismiss"><img class="dismiss-icon" src="{{asset('backend/img/dismiss.png')}}"></button>
                </div>
            </div>
            <div class="lead-content">
                <div class="lead-tab-title">
                    <ul>
                        <li data-target="customer-info" class="active lead-tab"><a href="#">Customer Info</a></li>
                        <li data-target="insurance-details" class="lead-tab"><a href="#">Insurance Details</a></li>
                        <li data-target="document-details" class="lead-tab"><a href="#">Document</a></li>
                        <li data-target="lead-remark" class="lead-tab"><a href="#">{{\Request::is('admin/leads') ? 'Remarks' : 'Plan Details' }}</a></li>
                    </ul>
                </div>
                <div class="lead-tab-content">
                    <div id="customer-info" class="lead-customer lead-tab-wrap active">
                        <div class="info-wrapper info-overflow">
                            <ul>
                                <li><strong>Full Name</strong><span>{{$lead->customer_name}}</span></li>
                                <li><strong>Phone No</strong><span>{{$lead->phone}}</span></li>
                                <li><strong>Email</strong><span>{{$lead->email}}</span></li>
                                <li><strong>Profession</strong><span>{{$lead->profession}}</span></li>
                                <li><strong>Province</strong><span>{{@$lead->getProvince->province_name}}</span></li>
                                <li><strong>City</strong><span>{{@$lead->getCity->city_name}}</span></li>
                                <li><strong>Street</strong><span>{{@$lead->street_ward}}</span></li>
                                <li><strong>Age</strong><span>{{@$lead->age}} {{(!empty($lead->dob)) ? '('.@$lead->dob.')' : ''}}</span></li>
                                @if($lead->policy_type == 3)
                                    <li><strong>Organization Name</strong><span>{{@$lead->organization_name}}</span></li>
                                    <li><strong>Size Of Employee</strong><span>{{@$leads->size_of_employee}}</span></li>
                                @endif
                                <li><strong>Customer</strong><span>{{ ($lead->is_user == 1 ? 'Yes' : 'No')}}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div id="insurance-details" class="lead-insurance lead-tab-wrap">
                        <div class="info-wrapper info-overflow">
                            <ul>
                                <li><strong>Sales Person</strong><p>@if(!empty($lead->user->username)) {{ucfirst($lead->user->username)}} @else  <span class="non-assign">Not Assign</span>@endif</p></li>
                                <li><strong>Lead Source</strong><p>{{@$lead->leadsource->name}}</p></li>
                                <li><strong>Lead Type</strong><p><span class="lead-{{@$lead->leadtype->type}}">{{@$lead->leadtype->type}}</span></p></li>
                                <li><strong>Policy Type</strong><p>@if($lead->policy_type==1) Life @elseif($lead->policy_type==2) Non Life  @else Not selected @endif</p></li>
                                @if(\Request::is('admin/leads'))
                                <li><strong>Category</strong><p>{{ucfirst($lead->category)}}</p></li> 
                                <li><strong>Maturity Period</strong><p>{{$lead->maturity_period}}</p></li>
                                <li><strong>Sum Insured</strong><p>{{$lead->sum_insured}}</p></li> 
                                @else
                                    @if($plans=$lead->planSelecteds)
                                        <div class="my-plan-wrapper">
                                            @foreach($plans as $key=>$plan)
                                                    <div class="my-plan-item-wrapper">
                                                        <div class="my-plan-btn">
                                                            <h1>{{$lead->customer_name}} (Plan {{$key + 1}})</h1>
                                                            <button type="button" class='toggle-my-plan'><i class="fa fa-arrow-circle-o-down"></i></button>
                                                        </div>
                                                        <div class="my-plan-details">
                                                            <li>
                                                                <strong>Category</strong>
                                                                <p>{{ucfirst($plan->category)}}</p>
                                                            </li>
                                                            <li>
                                                                <strong>Company</strong>
                                                                <p>{{ $plan->company->name ?? 'Not selected' }}</p>
                                                            </li>
                                                            <li>
                                                                <strong>Product</strong>
                                                                <p>{{$plan->product->name ?? 'Not Selected'}}</p>
                                                            </li>
                                                            <li>
                                                                <strong>Term</strong>
                                                                <p>{{$plan->term ?? 'No Data Found'}}</p>
                                                            </li>
                                                            <li>
                                                                <strong>Premium</strong>
                                                                <p>{{$plan->premium ?? 'No Data Found'}}</p>
                                                            </li>
                                                             <li>
                                                                <strong>MOP</strong>
                                                                <p>{{$plan->mop ?? 'No Data Found'}}</p>
                                                            </li>
                                                            <li>
                                                                <strong>Issued Date</strong>
                                                                <p>{{$plan->issued_date ?? 'No Data Found'}}</p>
                                                            </li>
                                                            <li>
                                                                <strong>Policy Number</strong>
                                                                <p>{{$plan->policy_number ?? 'No Data Found'}}</p>
                                                            </li>
                                                            <div class="premium-paid-details">
                                                                 <?php $premiumDetails=json_decode($plan->premiumPaidByCustomer); ?>
                                                                @if(!empty($premiumDetails))
                                                                    @foreach($premiumDetails as $premium)
                                                                        <div class="premium-paid-item">
                                                                            <h1>Paid For {{$premium->paid_date}}</h1>
                                                                            <li>
                                                                                <strong>Premium Amount</strong>
                                                                                <p>Rs.{{$premium->premium_amount}}</p>
                                                                                <p class="premium-status"><span class="status-{{$premium->status}}">{{ucfirst($premium->status)}}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="verify_{{$premium->verify == 0 ? 'red' : 'green'}}">{{$premium->verify ==0 ? 'Not Verified' : 'Verified'}}</span></p>

                                                                            </li>
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <p class="non-found">No Premium Details Found</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endforeach
                                        </div> 
                                    @endif        
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div id="document-details" class="document lead-tab-wrap">
                        <div class="document-wrapper">
                            <!-- Upload  -->
                            <form data-lead="{{$lead->id}}"  id="file-upload-form " class="uploader document-form document-form-{{$lead->id}}" enctype="multipart/form-data" novalidate>
                                @csrf
                                  <input multiple class="document-file"  id="file-upload" type="file" name="documents[]" accept="" required/>
                                  <label for="file-upload" id="file-drag">
                                    <div id="start">
                                      <i class="fa fa-download" aria-hidden="true"></i>
                                      <div>Select a file or drag here</div>

                                      <span id="file-upload-btn" class="btn btn-primary">Select a file</span>
                                  </div>
                                </label>
                                <button type="button" class="document-submit" name="Submit">Submit</button>
                            </form>
                            @if($lead->document)
                                @foreach($lead->document as $document)
                                    <div class="document-preview">
                                        <a target="_blank" href="{{asset('uploads/leads/'.$lead->id.'/'.$document->docs)}}" class="preview-doc">{{$document->docs}} </a><button data-id="{{$document->id}}" class="delete-docs"><i class="fa fa-times"></i></button>
                                    </div>
                                @endforeach
                            @endif
                </div>
            </div>
            <div id="lead-remark" class="remark lead-tab-wrap">
                @if(\Request::is('admin/leads'))
                <div class="remark-wrapper">
                    @foreach($lead->remark as $remark)
                           <div class="info-wrapper">
                            <div class="content-remark">
                                <p>{!! $remark->remark !!}</p>
                                <span class="create-date">{{$remark->created_at->format('d M Y')}}</span>
                            </div>
                            <div class="remark-footer">
                                <div class="metting">
                                    <?php 
                                    $date = strtotime($remark->follow_up_date);
                                    $followDate=date('d M Y', $date);

                                    ?>

                                    <span class="span-followdate">Follow On: {{$followDate}}</span>
                                    <span class="span-followtime">{{$remark->follow_up_time}}</span>
                                </div>
                                <div class="person-name">
                                    <h5>{{auth()->user()->username}}</h5>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
           
                <div class="remark-form">
                    <form class="remark-form-details">
                        @csrf
                        <input name="lead_id" type="hidden" value="{{$lead->id}}">
                    </form>
                </div>
                <a class="remark-btn" href="javascript:;"><i class="fa fa-plus-circle"></i>&nbsp;Add Remark</a>
                @else
                    @if($plans=$lead->planSelecteds)
                        <div class="info-wrapper info-overflow">
                            <ul>
                                <div class="my-plan-wrapper">
                                    @foreach($plans as $key=>$plan)
                                        <div class="my-plan-item-wrapper">
                                            <div class="my-plan-btn">
                                                <h1>{{$lead->customer_name}} (Plan {{$key + 1}})</h1>
                                                    <button type="button" class='toggle-my-plan'><i class="fa fa-arrow-circle-o-down"></i></button>
                                            </div>
                                            <div class="my-plan-details">
                                                <li>
                                                    <strong>Category</strong>
                                                    <p>{{ucfirst($plan->category)}}</p>
                                                </li>
                                                <li>
                                                    <strong>Company</strong>
                                                    <p>{{ $plan->company->name ?? 'Not selected' }}</p>
                                                </li>
                                                <li>
                                                    <strong>Product</strong>
                                                    <p>{{$plan->product->name ?? 'Not Selected'}}</p>
                                                </li>
                                                <li>
                                                    <strong>Term</strong>
                                                    <p>{{$plan->term ?? 'No Data Found'}}</p>
                                                </li>
                                                <li>
                                                    <strong>Premium</strong>
                                                    <p>{{$plan->premium ?? 'No Data Found'}}</p>
                                                </li>
                                                <li>
                                                    <strong>MOP</strong>
                                                    <p>{{$plan->mop ?? 'No Data Found'}}</p>
                                                </li>
                                                <li>
                                                    <strong>Issued Date</strong>
                                                    <p>{{$plan->issued_date ?? 'No Data Found'}}</p>
                                                </li>
                                                <li>
                                                    <strong>Policy Number</strong>
                                                    <p>{{$plan->policy_number ?? 'No Data Found'}}</p>
                                                </li>
                                                
                                                <div class="premium-paid-details">
                                                    <?php $premiumDetails=json_decode($plan->premiumPaidByCustomer); ?>
                                                    @if(!empty($premiumDetails))
                                                        @foreach($premiumDetails as $premium)
                                                            <div class="premium-paid-item">
                                                                <h1>Paid For {{$premium->paid_date}}</h1>
                                                                <li>
                                                                    <strong>Premium Amount</strong>
                                                                    <p>Rs.{{$premium->premium_amount}}</p>
                                                                    <p><span class="status-{{$premium->status}}">{{ucfirst($premium->status)}}</span></p>
                                                                </li>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <p class="non-found">No Premium Details Found</p>
                                                    @endif
                                                </div>
                                              
                                            </div>
                                        </div>
                                    @endforeach
                                </div> 
                            </ul>
                        </div>
                    @endif
                @endif
            </div>

        </div>
    </div>
</div>
</div>