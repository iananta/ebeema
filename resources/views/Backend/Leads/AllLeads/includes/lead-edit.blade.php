    <div id="leads-info" class="leads-info lead-edit-view">
            
            <div class="leads-info-wrapper">
                <div class="form-loader"><i class="fa fa-spinner fa-spin"></i></div>
                <div class="lead-info-header">
                    <div class="lead-name">
                        <h5 ></h5>
                    </div>
                    <div class="lead-action-button">
                        <button class="lead-dismiss"><img class="dismiss-icon" src="{{asset('backend/img/dismiss.png')}}"></button>
                        
                    </div>
                </div>
                <div class="lead-content">
                    <div class="lead-tab-title">
                        <ul>
                            <li data-target="customer-info" class="active lead-tab"><a href="javascript:;">Customer Info</a></li>
                            <li data-target="insurance-details" class="lead-tab"><a href="javascript:;">Insurance Details</a></li>
                        </ul>
                    </div>
                    <div class="lead-tab-content lead-edit-tab-content">
                        <div id="customer-info" class="lead-customer lead-tab-wrap active">
                            <div class="info-wrapper info-overflow">
                                <form method="post" id="lead-edit-form" class="customer_info_form info_form">
                                     @csrf
                                    <ul>
                                        <li><strong>Full Name</strong> <input id="lead-edit-customer_name" type="text" name="customer_name" placeholder="Enter Full Name"></li>
                                        <li><strong>Phone No</strong> <input id="lead-edit-phone" type="text" name="phone" placeholder="Enter Phone No"></li>
                                        <li><strong>Email</strong><input id="lead-edit-email" type="text" name="email" placeholder="Enter Email"></li>
                                        <li>
                                            <strong>Profession</strong>
                                            <select id="lead-edit-profession" name="profession">
                                                <option selected value="">Select</option>
                                                <option value="Private jobs">Private jobs</option>
                                                <option value="Public Service">Public Service</option>
                                                <option value="Non Profit Organization">Non Profit Organization</option>
                                                <option value="Student">Student</option>
                                                <option value="Entrepreneur">Entrepreneur</option>
                                                <option value="Home maker">Home maker</option>
                                                <option value="Small Business">Small Business</option>
                                            </select>
                                        </li>
                                        <li>
                                            <strong>Province</strong>
                                            <select id="lead-edit-province" class="changeProvince" name="province">
                                                    <option selected value="">Select</option>
                                                    @foreach($provinces as $province)
                                                        <option value="{{$province->id}}">{!!$province->province_name!!}</option>
                                                    @endforeach
                                            </select>
                                        </li>
                                        <li>
                                            <strong>City</strong>
                                            <select name="city" class="city-select"  id="lead-edit-city">
                                                <option selected value="">Select</option>

                                            </select>
                                        </li>
                                        <li><strong>Street</strong><input type="text" name="street_ward" id="lead-edit-street_ward" placeholder="Enter Street & Ward"></li>
                                        <li><strong>Age</strong><input type="date" name="dob" id="lead-edit-dob" placeholder="Enter DOB"></li>
                                         <li class="customer-btn-wrapper">
                                            <strong></strong>
                                             <button type="button" class="edit-lead-bttn">Submit</button>
                                        </li>
                                    </ul>
                                    <input type="hidden" name="id" id="lead-edit-id">
                                </form>
                               
                            </div>
                        </div>
                        <div id="insurance-details" class="lead-insurance lead-tab-wrap">
                            <div class="info-wrapper info-overflow">
                                <form method="post" class="insurance_info_form info_form edit-wrap">
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        
    </div>