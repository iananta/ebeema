<div class="lead-import-wrapper">
	<div class="lead-form-wrapper">
		<div class="lead-import-header">
			<h1>Leads Import</h1>
			<button class="import-dismiss">&times;</button>
		</div>
		<form class="import-form" action="{{ route('admin.leads.import') }}" method="POST"
                    enctype="multipart/form-data">
            @csrf
            <div class="import-file-wrapper">
            	<div class="import-text">
            		<label for="formFile" class="form-label">
            			<i class="fa fa-download" aria-hidden="true"></i></label>
            		<div>Select a file or drag here</div>
            	</div>
            	<div class="import-input">
            		<input class="form-control" name="file" type="file" id="formFile" required>
            	</div>
            	<div class="import-button">
            		<button type="submit" class="btn btn-primary c-primary-btn shadow-none mx-2">&nbsp;Submit
            		</button>
            	</div>
            </div>
        </form>
	</div>
</div>