<script>
    $(document).ready(function(){
        $(document).on('click','.view-detail-lead',function(){
            let id=$(this).data('id')
            localStorage.setItem('parent_id',id)
            let parent_id=localStorage.getItem('parent_id')
            let parent=$('#leads-info-'+parent_id)
            let tabSelector=parent.find('.lead-tab')
            let leadWrap=parent.find('.lead-tab-wrap')
            let remarkWrapper=parent.find('.remark-form')
            let remarkSelector=remarkWrapper.parent().find('.remark-btn')
            let documentButton=parent.find('.document-form').find('.document-submit');
            $('#leads-info').hide()
            parent.css('display','flex');
            parent.find('.leads-info-wrapper').addClass('popup-zoom-in')
            tabSelector.on('click',function(){
                let target=$(this).data('target')
                let targetDiv=parent.find('#'+target)
                tabSelector.removeClass('active')
                $(this).addClass('active')
                leadWrap.removeClass('active')
                targetDiv.addClass('active')
            })
            //remark
            remarkSelector.on('click',function(){
                let form="<div class='remark-form-wrapper'>\
                <div class='remark-group-row'><div class='form-group'><input type='text' id='follow_up_date' name='follow_up_date' class='follow_update follow_up_date'></div><div class='form-group'><input type='time' name='follow_up_time' class='follow_update follow_up_title'></div></div>\
                <textarea id='remark' name='remark' class='form-control remarks-text' rows='5' placeholder='Remarks'></textarea><div class='remark-remove'>\
                <a href='#' class='remove-btn'>Cancel</a>\
                <button type='submit' class='remark-submit'>Submit</button>\
                </div>"
                let remark=remarkWrapper.find('.remark-form-wrapper')
                if(remark.length == 0 ){
                    let parent=remarkWrapper.find('.remark-form-details')
                    parent.append(form)
                    let followData=parent.find('.remark-form-wrapper .remark-group-row .form-group').find('#follow_up_date')
                    followData.datepicker({
                        autoclose: true,
                        todayHighlight: true
                    }).datepicker('update', new Date());

                }


            })
            $(document).on('click','.remove-btn',function(){
                $(this).parent().parent().remove()
            })


        })
        //document upload

        $(document).on('click','.document-submit',function(e){
            e.preventDefault()
            let _this=$(this)
            let form=_this.parent()
            let lead=form.data('lead')
            $('#lead-id-document').val(lead)
            let input=form.find('#file-upload').clone()
            $('.document-upload-form').append(input)
            $('.document-upload-form').submit()
        })
        $(document).on('submit','.document-upload-form',function(e){
            e.preventDefault()
            let formData=new FormData(this);
            let filelength=$('.document-file')[0].files.length
            formData.append('filelength', filelength);
            let files=$('.document-file')[0]
            for(let i=0; i<filelength; i++){
                formData.append('documents[]',files.files[i])
            }
            $.ajax({
                type: "POST",
                dataType: 'json',
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                cache: false,
                url:"{{route('admin.leads.document')}}",
                data:formData,
                beforeSend:function(){
                  $('.form-loader').css('display','flex')
              },
              success:function(response){
                 setTimeout(function(){
                    $('.form-loader').hide()
                    $('.document-form')[0].reset()
                    new swal('Success', response.message, 'success');
                },1500)
             },error:function(response){
                setTimeout(function(){
                  $('.form-loader').hide()
                  new swal('error',response.responseJSON.message, 'error');
              },1500)
            },complete:function(){
                $('.document-upload-form').find('.document-file').remove()
            }
        })
        })
        $(document).on('click','.delete-docs',function(){
            let _this=$(this)
            let id = $(this).data('id')
            let formData=new FormData()
            formData.append('id',id)
            $.ajax({
                type: "POST",
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false,
                url:"{{route('admin.leads.documentDelete')}}",
                data:formData,
                success:function(response){
                 _this.parent().remove()
                 new swal('Success', response.message, 'success');
             },error:function(response){
                 new swal('error',response.responseJSON.message, 'error');
             }
         })
        })
        //doucment form
        $(document).on('click','.remark-submit',function(e){
            e.preventDefault()
            let _this=$(this)
            let formTag=_this.parent().parent().parent()
            let formData=formTag.serializeArray()
            let textarea=formTag.find('textarea[name="remark"]')
            $('.custom-validation-message-error').remove()
            if(textarea.val()=='' || textarea.val()== null){
             textarea.parent().append('<p class="custom-validation-message-error">Remark field is required</p>')
             return
         }
         _this.attr('disabled','disabled');

         $.ajax({
            type: "POST",
            dataType: 'json',
            url:"{{route('admin.leads.Addremark')}}",
            data: formData,
            beforeSend:function(response){
            },
            success: function (response) {
                let lead=response.lead
                let date=new Date(lead.created_at)
                let followData=new Date(lead.follow_up_date)
                const options = {  year: 'numeric', month: 'short', day: 'numeric' };
                formTag.parent().parent().find('.remark-wrapper').prepend('<div class="info-wrapper"><div class="content-remark"><p>'+lead.remark+'<span class="create-date">'+date.toLocaleDateString('en-US', options)+'</span></p></div><div class="remark-footer"><div class="metting"><span class="span-followdate">Follow Up:'+followData.toLocaleDateString('en-US', options)+'<span><span class="span-followtime">'+lead.follow_up_time+'</span></div><div class="person-name"><h5>{{ucfirst(auth()->user()->username)}}</h5></div></div>');
                $('.remark-form-wrapper').remove()
                setTimeout(function(){
                    _this.removeAttr('disabled');
                },3000)

            },error:function(response){
                setTimeout(function(){
                    _this.removeAttr('disabled');
                },3000)

            }
        })
     })

        $(document).on('click',function(event){
            if (!$(event.target).closest(".leads-info-wrapper,.day,.remove-btn,.view-detail-lead,.swal2-confirm,.edit-modal-btn,.remove-premium-form,.import-button").length || $(event.target).closest('.dismiss-icon').length) {
                $('.leads-info').hide();
                $('html').removeAttr('style')
            }
            if($(event.target).closest(".leads-info-wrapper,.day,.remove-btn,.view-detail-lead,.edit-modal-btn,.remove-premium-form,.import-button").length && !$(event.target).closest('.dismiss-icon').length){
                $('html').css('overflow','hidden')
            }

        })
        $(document).on('click','.lead-dismiss',function(event){
            event.preventDefault()
            $('.leads-info').hide();
            $('html').removeAttr('style')

        })
        $(document).on('click','.edit-modal-btn',function(e){
            e.preventDefault()
            let target=$(this)
            let editModal=$('.lead-edit-view')
            let id=target.data('id')
            let from=target.data('from')
            $('.custom-validation-message-error').remove()
            $.ajax({
                type:'POST',
                dataType:'json',
                url:"{{route('admin.leads.getspecificLead')}}",
                data:{
                    id:id,
                    from:from,
                    _token:"{{csrf_token()}}"
                },
                success:function(response){
                    let lead=response.lead
                    editModal.removeAttr('style')
                    editModal.css('display','flex')
                    editModal.find('.leads-info-wrapper').addClass('popup-zoom-in')
                    $('.lead-edit-view .lead-name h5').html(lead.customer_name)
                    $('.edit-wrap').html(response.html)
                    let provinceId=lead.province
                    let productIds=lead.plan_selecteds
                    let userId=lead.user_id
                    if(provinceId){
                        getCity(provinceId,lead.city)
                    }
                    if(productIds){
                        $.each(productIds,function(key,value){
                            getProduct($('.lead-edit-insurance_company_name'),productIds[key].company_id,productIds[key].product_id)
                        })
                    }
                    if(userId){
                        getUser($('.role-change'),localStorage.getItem('role_id'),userId)
                    }
                    for (const [key, value] of Object.entries(lead)) {
                        $('#lead-edit-'+key).val(value)

                    }

                }
            })
        })
        $('.lead-edit-view .lead-tab').on('click',function(){
            let target=$(this)
            $('.lead-edit-view .lead-tab').removeClass('active')
            target.addClass('active')
        })
        //get city
        function getCity(id,cityId){
            $.ajax({
                type:'POST',
                dataType:'json',
                url:'{{route("admin.leads.getCity")}}',
                data:{
                    id:id,
                    _token:'{{csrf_token()}}'
                },success:function(response){
                    let cities=response.cities
                    let selected
                    let html='<option value="">Select</option>';
                    if(cities != undefined){
                        for(let i=0;i<cities.length;i++){
                            if(cityId){
                                if(cityId == cities[i].id ){
                                    selected='selected'
                                }else{
                                    selected=''
                                }
                            }

                            html+='<option '+selected+' value="'+cities[i].id+'">'+cities[i].city_name+'</option>'
                        }
                        $('.city-select').html(html)
                    }
                }
            })
        }

        $('.changeProvince').on('change',function(e){
            e.preventDefault()
            let id=$(this).val()
            getCity(id,null)

        })
        $('.lead-edit-view .lead-tab-title ul li').on('click',function(){
           $('.lead-edit-view .lead-tab-wrap').hide()
           let id=$(this).data('target')
           $('.lead-edit-view #'+id).show()
           $('.info_form').removeAttr('id')
           $('.lead-edit-view #'+id).find('form').attr('id','lead-edit-form')
       })
        $(document).on('click','.edit-lead-bttn',function(event){
            $('#lead-edit-form').submit()
        })

        $(document).on('submit','#lead-edit-form',function(e){
            e.preventDefault()
            let formData=$(this).serializeArray()
            let requiredInput = document.querySelectorAll('#lead-edit-form input[required]');
            let requiredSelect = document.querySelectorAll('#lead-edit-form select[required]');
            let count=0
            let customers=LaravelDataTables.customerlist_table
            let leads=LaravelDataTables.leads_table
            Array.prototype.forEach.call(requiredInput, function(element){
                if(!element.value){
                    element.style.border = '1px solid #ff0000';
                    count++
                }
            });
            Array.prototype.forEach.call(requiredSelect, function(element){
                if(!element.value){
                    element.style.border = '1px solid #ff0000';
                    count++
                }
            });
            if(count==0){
                $.ajax({
                    type:'POST',
                    url:"{{route('admin.lead.update')}}",
                    data:formData,
                    beforeSend:function(){
                        $('.form-loader').css('display','flex')
                    },
                    success:function(response){
                        setTimeout(function(){
                            $('.form-loader').hide()
                            $('#lead-edit-form input').removeAttr('style')
                            $('#lead-edit-form select').removeAttr('style')
                            new swal(
                                'Success',
                                response.message,
                                'success'
                                )
                        },2000)

                    },complete:function(){
                        setTimeout(function(){
                            if(leads){
                                leads.ajax.reload(null,false)
                            }
                            if(customers){
                                customers.ajax.reload(null,false)
                            }
                        },2100)
                    },error:function(response){
                       setTimeout(function(){
                        $('.form-loader').hide()
                        let res=response.responseJSON
                        let errorObj = res.errors
                        $('.custom-validation-message-error').remove()
                        if(errorObj){
                            for (const [key, value] of Object.entries(errorObj)) {
                                $('#lead-edit-'+key).parent().append('<p class="custom-validation-message-error">' + value + '</p>')

                            }
                        }
                    },2000)
                   }
               })
            }
        })

        //get product
        function getProduct(target,companyId,productId){
            let parent=target.parent()
            let html
            $.ajax({
                type:'POST',
                url:"{{route('admin.leads.getProduct')}}",
                data:{
                    _token:"{{csrf_token()}}",
                    companyId:companyId,
                },
                success:function(response){
                    let product=response.product
                    let selected
                    let html='<option value="">Select</option>';
                    for(let i=0;i<product.length;i++){
                        if(productId){
                            if(productId == product[i].id ){
                                selected='selected'
                            }else{
                                selected=''
                            }
                        }

                        html+='<option '+selected+' value="'+product[i].id+'">'+product[i].name+'</option>'
                    }

                    if(parent.next().hasClass('d-none')){
                        $('.lead-edit-product_id').html(html)
                        parent.next().removeClass('d-none')
                    }else{
                        $('.lead_product_id').html(html)
                    }

                }
            })
        }
        $(document).on('change','.lead-edit-insurance_company_name',function(){
            let target=$(this)
            let companyId=target.val()
            console.log(companyId)
            getProduct(target,companyId,null)
        })


        $(document).on('click','.toggle-my-plan',function(){
            let target=$(this)
            let wrap=target.parent().next()
            if(target.hasClass('active')){
                wrap.slideUp()
            }else{
                wrap.slideDown()
            }
            target.toggleClass('active')

        })
        $(document).on('change','.role-change',function(){
            let target=$(this)
            let id=target.val()
            localStorage.setItem('role_id',id)
            getUser(target,id,null)
        })

        function getUser(target,id,userId){
            let parent=target.parent()
            let html
            $.ajax({
                type:'POST',
                url:"{{route('admin.leads.findUser')}}",
                data:{
                    _token:"{{csrf_token()}}",
                    id:id,
                },
                success:function(response){
                    let users=response.users
                    let selected
                    let html='<option value="">Select</option>';
                    for(let i=0;i<users.length;i++){
                        if(userId){
                            if(userId == users[i].id ){
                                selected='selected'
                            }else{
                                selected=''
                            }
                        }

                        html+='<option '+selected+' value="'+users[i].id+'">'+users[i].username+'</option>'
                    }
                    if(parent.next().hasClass('d-none')){
                        $('.lead-edit-user_id').html(html)
                        parent.next().removeClass('d-none')
                    }else{
                        $('.lead-edit-user_id').html(html)
                    }
                }
            })
        }


        $(document).on('click','.premium_paid_btn',function(){
            let form='<div class="premium_form new_premium_form">\
            <div class="premium-group">\
            <label>Premium Amount</label>\
            <input type="number" name="premium_amount_new" required="required">\
            </div>\
            <div class="premium-group">\
            <label>Paid Date</label>\
            <input type="date" name="paid_date_new" required="required">\
            </div>\
            <div class="premium-group">\
            <label>Status</label><select name="status"><option value="pending">Pending</option><option value="paid">Paid</option></select></div>\
            <input type="hidden" value="1" name="add_premium_paid" >\
            <div class="premium-group"><button type="button" class="remove-premium-form">&minus;</button></div>\
            </div>'
            let findnext=$(this).next()
            if(findnext.hasClass('premium_paid_details_form_wrapper') && $('.new_premium_form').length == 0){
                findnext.append(form)
            }

        })
        $(document).on('click','.update-premium-btn',function(){
            let id=$(this).data('id')
            let formData={
                _token:"{{csrf_token()}}",
                id:id,
                paid_date:$('#paid_date_'+id).val(),
                premium_amount:$('#premium_amount_'+id).val(),
                status:$('#status_'+id).val()
            }
            $.ajax({
                type:'POST',
                url:"{{route('admin.leads.updatePremiumPaidByCustomer')}}",
                data:formData,
                success:function(response){
                    new swal('Success',response.message,'success')
                }
            })
        })
        $(document).on('click','.remove-premium-form',function(){
            $(this).parent().parent().remove()
        })
        $(document).on('click', '.delete-lead', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if(response.isConfirmed == true){
                    $.ajax({
                        type: "DELETE",
                        url: "{{ url('/admin/leads/') }}" + "/" + id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        dataType: 'json',
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            new swal(
                                'Success',
                                response.message,
                                'success'
                                )
                            $object.parent().parent().remove()
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }


            }).catch(swal.noop);
        });
        $(document).on('change','.policy_change',function(){
            if($(this).val()!=' '){
                window.location.href='{{ \Request::is("admin/leads") ? url("admin/leads") :url("admin/customers")}}?policy_type='+$(this).val()
            }
            if($(this).val()== ''){
                window.location.href='{{ \Request::is("admin/leads") ? url("admin/leads") :url("admin/customers")}}'
            }
        })
        $(document).on('click','.import-button',function(){
            $('.lead-import-wrapper').show({
                start: function() {
                    $(this).css('display', 'flex');
                }
            })
            $('.import-dismiss').on('click',function(){
                $('.lead-import-wrapper').hide()
            })
        })



        $(document).on('click','.convert-policy-btn',function(){
            let btn=$(this)
            $.ajax({
                type:'get',
                url:"{{route('admin.leads.getformConvertToPolicy')}}",
                success:function(response){
                    btn.removeClass('convert-policy-btn')
                    btn.addClass('remove-convert-policy-btn')
                    btn.html('Cancle Policy')
                    $('#lead-edit-category').attr('required','required')
                    $(response.form).insertBefore('.customer-btn-wrapper')
                }
            })
        })
        $(document).on('click','.remove-convert-policy-btn',function(){
            let btn=$(this)
            $('.convert-policy-item').remove()
            btn.removeClass('remove-convert-policy-btn')
            btn.addClass('convert-policy-btn')
            btn.html('Convert to Policy')
            $('#lead-edit-category').removeAttr('required')

        })
        $(document).on('click','.customer-status-change-button',function(){
            let target=$(this)
            let id=target.data('id')
            let verify=target.data('verify')
            let name=target.data('name')
            let phone=target.data('phone')
            let email=target.data('email')
            let category=target.data('category')
            let company=target.data('company')
            let product=target.data('product')
            let suminsured=target.data('suminsured')
            let premium=target.data('premium')
            let parent=target.parent()
            let policyType=target.data('policytype')

            let type=null;
            if(policyType === 1){
                type="Life"
            }else if(policyType == 0){
                type="Non Life"
            }else{
                type="Other"
            }
            if(verify == 0){
                $('.confirm-action').html('Change to Not Verify')
                $('.confirm-action').addClass('red-btn')
                $('.confirm-action').removeClass('green-btn')
            }else{
                $('.confirm-action').html('Change to Verify')
                $('.confirm-action').addClass('green-btn')
                $('.confirm-action').removeClass('red-btn')

            }
            $('.confirm-customer-id').val(id)
            $('.confirm-customer-verify').val(verify)
            $('.confirm-name').html(name)
            $('.confirm-phone').html(phone)
            $('.confirm-email').html(email)
            $('.confirm-category').html(category)
            $('.confirm-company').html(company)
            $('.confirm-product').html(product)
            $('.confirm-premium').html(premium)
            $('.confirm-suminsured').html(suminsured)
            $('.confirm-policy').html(type)

            $('.confirm-popup').fadeIn({
                start:function(){
                    $(this).css('display','flex')
                }
            })
        })
        $(document).on('click','.confirm-action',function(){
            let target=$(this)
            let id=$('.confirm-customer-id').val()
            let verify=$('.confirm-customer-verify').val()
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.customerVerify') }}",
                    data:{
                        _token:"{{csrf_token()}}",
                        id:id,
                        verify:verify,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                     $('.confirm-popup').fadeOut()
                      new swal('Success', response.message, 'success');
                       LaravelDataTables.leads_table.ajax.reload()
                  },
                  error: function (e) {
                      $('#preloader').hide();
                    new swal('Oops...', 'Something went wrong!', 'error');
                }
            });
        })

        $(document).on('click','.confirm-dismiss',function(){
            $('.confirm-popup').fadeOut()
        })

        $(document).on('submit','.filter-form',function(e){
            e.preventDefault()
            let formData=$(this).serializeArray();
            let table=$('.dataTable').attr('id')
            $('#'+table).on('preXhr.dt',function(e,setting,data){
                data.customFilter=formData
            }).data({
                "serverSide":true,
                "processing":true,
                "ajax":{
                    url: "{{url('admin/leads')}}",
                    type:"GET"
                }
            })
            if(table =='leads_table'){
                LaravelDataTables.leads_table.ajax.reload()
            }else{
                LaravelDataTables.customerlist_table.ajax.reload()
            }
        })
        $(document).on('click','.filter-reset',function(e){
            e.preventDefault()
            let table=$('.dataTable').attr('id')
            $('.filter-form')[0].reset()
             $('#'+table).on('preXhr.dt',function(e,setting,data){
                data.customFilter=[]
            }).data({
                "serverSide":true,
                "processing":true,
                "ajax":{
                    url: "{{url('admin/leads')}}",
                    type:"GET"
                }
            })
            if(table =='leads_table'){
                LaravelDataTables.leads_table.ajax.reload()
            }else{
                LaravelDataTables.customerlist_table.ajax.reload()
            }
        })
        $(document).on('click','.apply-filter',function(){
                let filterBlock=$('.filter-block')
                filterBlock.toggleClass('active')
                if(filterBlock.hasClass('active')){
                    filterBlock.slideDown();
                    $(this).html('<i class="fa fa-filter"> </i>&nbsp;Cancle Filter')
                }else{
                     filterBlock.slideUp();
                     $(this).html('<i class="fa fa-filter"> </i>&nbsp;Apply Filter')
                }
        })
    })
</script>
