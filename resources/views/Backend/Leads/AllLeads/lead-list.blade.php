<table id="leadLists" class="table table-bordered table-striped role-table leads-table">
    <thead class="text-nowrap">
    <tr>
        <th><b>SN</b></th>
        <th><b>Sales Person Name</b></th>
        <th><b>Customer Name</b></th>
        <th><b>Profession</b></th>
        <th><b>Address</b></th>
        <th><b>Phone No.</b></th>
        <th><b>Email</b></th>
        <th><b>Lead Source</b></th>
        <th><b>Lead Type</b></th>
        <th><b>Policy Category</b></th>
        <th><b>Policy Sub-Category</b></th>
        <th><b>Policy type</b></th>
        <th><b>Maturity Amount</b></th>
        <th><b>Maturity Period</b></th>
        <th><b>Remarks</b></th>
        <th><b>Meetings</b></th>
    </tr>
    </thead>
    <tbody id="tablebody">
    @foreach($leads as $lead)

        <tr>
            <td>{{ $lead->id }}</td>
            <td>{{ $lead->sales_person_name }}</td>
            <td>{{ $lead->customer_name }}</td>
            <td>{{ $lead->profession }}</td>
            <td>@isset($lead->leadsource){{ $lead->address }} @endisset</td>
            <td>{{ $lead->phone }}</td>
            <td>{{ $lead->email }}</td>
            <td>@isset($lead->leadsource){{ $lead->leadsource->name }} @endisset</td>
            <td>
                @isset($lead->leadtype)
                    {{$lead->leadtype->type}}
                @endisset
            </td>
            <td>
                @if($lead->policy_cat==1)
                    <p>Life</p>
                @else
                    <p>Non Life</p>
                @endif
            </td>
            <td>
                @isset($lead->policy_sub_cat){{ $lead->policy_subcat->subcat_name ?? '' }} @endisset
            </td>
            <td>
                @isset($lead->policytype)
                    {{ $lead->policytype->type }} @endisset
            </td>
            <td>
                {{$lead->sun_insured}}
            </td>
            <td>
                {{$lead->maturity_period}}
            </td>
            <td>
                @forelse ($lead->events()->orderBy('created_at','desc')->take(1)->get() as $remark)
                    <p>
                        {{$remark->title}}</p>
                @empty
                    <p>No remarks.</p>
                @endforelse
            </td>
            <td>
                @forelse ($lead->meetings()->orderBy('created_at','desc')->take(1)->get() as $remark)
                    <p>
                        {{$remark->title}}</p>
                @empty
                    <p>No meeting sheduled.</p>
                @endforelse
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th><b>SN</b></th>
        <th><b>Sales Person Name</b></th>
        <th><b>Customer Name</b></th>
        <th><b>Profession</b></th>
        <th><b>Address</b></th>
        <th><b>Phone No.</b></th>
        <th><b>Email</b></th>
        <th><b>Lead Source</b></th>
        <th><b>Lead Type</b></th>
        <th><b>Policy Category</b></th>
        <th><b>Policy Sub-Category</b></th>
        <th><b>Policy type</b></th>
        <th><b>Maturity Amount</b></th>
        <th><b>Maturity Period</b></th>
        <th><b>Remarks</b></th>
        <th><b>Meetings</b></th>
    </tr>
    </tfoot>
</table>

