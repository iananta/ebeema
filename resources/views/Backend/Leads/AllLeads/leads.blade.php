@extends('layouts.backend.containerlist')

@section('title')
    {{\Request::is('admin/customers') ? 'Customers' : 'Leads'}}
@endsection
@section('css')

@endsection
@section('header_css')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
@endsection
@section('footer_js')
    <script type="text/javascript">
        // $(document).ready(function () {}
        $(document).on('click', '.add-modal', function () {
            $('#addModal').modal('show');
            $('.modal-title').text('{{\Request::is("admin/customers") ? "Add Customer" : "Add Lead"}}');
        });
        $(document).on('change','.excel_customizer',function(){
            let value=$(this).val();
            $("#leads_table").on('preXhr.dt', function (e, settings, data) {
                data.excelCustomizer =value;
            }).data({
                "serverSide": true,
                "processing": true,
                "ajax": {
                    url: "{{route('admin.leads.index')}}",
                    type: "GET"
                }
            });
            LaravelDataTables.leads_table.ajax.reload()

        })
    </script>

    <script type="text/javascript">
        @if(Session::has('message'))
            swal('Success','{{Session::get('message')}}','success')
        @endif
          @if(Session::has('error'))
            swal('Error','{{Session::get('error')}}','error')
        @endif

    </script>
    @include('Backend.Leads.AllLeads.includes.leads-script')

    {!! $dataTable->scripts() !!}

@endsection


@section('dynamicdata')
    <div class="box">
        <div class="box-body">
            @include('layouts.backend.alert')
            <div class="filter-block leads-filter">
                <div class="filter-wrapper">
                    <div class="filter-heading">
                        <h1>Filter</h1>
                    </div>
                    <form class="filter-form">
                        <div class="filter-form-wrapper">
                            <div class="form-filter-item">
                                <label>Date Added</label>
                                <input type="date" class="filter-input" name="created_at">
                            </div>
                            <div class="form-filter-item">
                                <label>Policy Type</label>
                                <select class="filter-input" name="policy_type">
                                    <option value="">Select one</option>
                                    <option value="1">Life</option>
                                    <option value="2">Non Life</option>
                                    <option value="3">Corporate Plan</option>
                                </select>
                            </div>
                            <div class="form-filter-item">
                                <label>Category</label>
                                <select class="filter-input" name="category">
                                    <option value="">Select One</option>
                                    @foreach($productCategories as $category)
                                         <option value="{{$category->category_code}}">{{ucfirst($category->name)}}
                                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-filter-item">
                                <label>Sum Insured</label>
                                <input type="number" name="sum_insured" class="filter-input" placeholder="Sum Insured">
                            </div>
                            <div class="form-filter-item">
                                <label>Lead Status</label>
                                <select class="filter-input"  name="leadtype_id">
                                    <option value="">Select One</option>
                                    @foreach($lead_types as $leadtype)
                                    <option value="{{$leadtype->id}}">{{$leadtype->type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-filter-item">
                                <label>Fb Leads</label>
                                <select name="fb_lead_form_id" class="filter-input">
                                    <option value="">Select One</option>
                                    @foreach($seoForms as $seo)
                                    <option value="{{$seo->form_id}}">{{$seo->form_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if(control('view-all-lead-table'))
                            <div class="form-filter-item">
                                <label>Sales Person</label>
                                <select class="filter-input" name="user_id">
                                    <option value="">Select One</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{ucfirst($user->username)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="form-filter-item filter-button-wrapper">
                                <button  class="filter-submit">Filter</button>
                            </div>
                            <div class="form-filter-item filter-button-wrapper">
                                <button type="reset"  class="filter-reset">Reset</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
           {!! $dataTable->table() !!}
                 @include('Backend.Leads.AllLeads.includes.lead-import')

        </div>
    </div>
    <div class="confirm-popup">
        <div class="confirm-wrapper">
            <div class="confirm-header">
                <h1 class="confirm-name"></h1>
                <button type="button" id="confirm-action" class="confirm-action"></button>
                <input type="hidden" value="" class="confirm-customer-id">
                <input type="hidden" class="confirm-customer-verify">
                <button type="button"  class="confirm-dismiss">&times;</button>
            </div>
            <div class="confirm-info-wrapper">
                <ul>

                    <li><strong>Name</strong><span class="confirm-name"></span></li>
                    <li><strong>Phone No</strong><span class="confirm-phone"></span></li>
                    <li><strong>Email</strong><span class="confirm-email"></span></li>
                    <li><strong>Policy Type</strong><span class="confirm-policy"></span></li>
                    <li><strong>Category</strong><span class="confirm-category"></span></li>
                    <li><strong>Company</strong><span class="confirm-company"></span></li>
                    <li><strong>Product</strong><span class="confirm-product"></span></li>
                    <li><strong>Sum Insured</strong><span class="confirm-suminsured"></span></li>
                    <li><strong>Premium</strong><span class="confirm-premium"></span></li>
                </ul>
            </div>
        </div>
    </div>
    @include('Backend.Leads.Leads._addleadmodule')
    @include('Backend.Leads.AllLeads.includes.lead-edit')
    <form class="document-upload-form" enctype="multipart/form-data" novalidate>
        @csrf
        <input type="hidden" id="lead-id-document" name="lead">
    </form>
@endsection
