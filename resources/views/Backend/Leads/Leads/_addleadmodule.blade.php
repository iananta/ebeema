<div class="modal center fade" id="addModal" tabindex="addLead" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal--header d-flex">
                <h4 class="modal-title">Add Lead</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-add-leads"  role="form" method="POST"
                      action="{{route('admin.leads.store')}}" enctype="multipart/form-data">
                    @csrf
                        <input type="hidden" name="lead_id" id="lead_id" value="">
                                <div class="container leads-tab-content">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Customer Full Name</label>
                                                <input type="customer_name" class="form-control"
                                                    id="customer_name" placeholder="Full name here" name="customer_name" placeholder="Enter Customer Name" required>
                                            </div>
                                            
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Profession</label>
                                                    <select class="form-control" id="profession" name="profession" type="profession" required>
                                                        <option selected disabled>Select Profession</option>
                                                        <option>Private jobs</option>
                                                        <option>Public Service</option>
                                                        <option>Non Profit Organization</option>
                                                        <option>Student</option>
                                                        <option>Entrepreneur</option>
                                                        <option>Home maker</option>
                                                        <option>Small Business</option>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Lead Source</label>
                                                <select class="form-control" id="leadsource_id" name="leadsource_id">
                                                    <option selected disabled>Select</option>
                                                    @foreach($lead_sources as $leadsource)
                                                <option value="{{$leadsource->id}}">{{$leadsource->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Lead Status</label>
                                                <select class="form-control" id="leadtype_id" name="leadtype_id">
                                                    <option selected disabled>Select</option>
                                                    @foreach($lead_types as $leadtype)
                                                    <option value="{{$leadtype->id}}">{{$leadtype->type}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 d-inline-flex p-0">
                                            <div class="col-6 no-gutter">
                                                <div class="form-group">
                                                    <label for="province">Province</label>
                                                    <select class="form-control changeProvince" id="province" name="province">
                                                        <option selected disabled>Select</option>
                                                        @foreach($provinces as $province)
                                                            <option value="{{$province->id}}">{!!$province->province_name!!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="CustomerFullName" class="form-label">City</label>
                                                    <select name="city" class="form-control city-select" id="city">
                                                        <option>Select</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="CustomerFullName" class="form-label">Street and Ward
                                                    Name</label>
                                                <input type="text" class="form-control" id="street_ward" name="street_ward" placeholder="Enter Street & Ward">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="phoneNumber" class="form-label">Phone Number</label>
                                                <input type="number" class="form-control" id="phone" name="phone" placeholder="Enter Phone Number">
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email" class="form-label">E-mail</label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder='Enter Email'>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="insureedob" class="form-label">Date of Birth
                                                    (Insuree)</label>
                                                <input type="date" class="form-control" id="dob" name="dob" placeholder="Enter DOB">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Sales Person Name</label>
                                                <select class="form-control" id="sales_person_name" name="user_id">
                                                    <option selected>Select Sales Person Name</option>

                                                    @foreach($users as $user)
                                                    <option value="{{$user->id}}" @if(Auth::user()->id == $user->id) selected @endif>{{$user->username}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Container end -->

                                <div class="container leads-tab-content lower-form-contents">
                                    
                                    <div class="row">
                                         <div class="col-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Policy Type</label>
                                                <select class="form-control" id="policy_type" name="policy_type">
                                                    <option selected>Select</option>
                                                    <option value="1">Life</option>
                                                    <option value="2">Non-Life</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="insurancecompany">Category</label>
                                                <select class="form-control" id="policy_sub_cat" name="category">
                                                    <option selected>Select</option>
                                                    @foreach($policy_sub_categories as $category)
                                                    <option value="{{$category->category_code}}">{{ucfirst($category->name)}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                    @if(\Request::is('admin/customers') || \Request::is('dashboard'))
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="company">Company</label>
                                                    <select class="form-control lead-edit-insurance_company_name"  name='insurance_company_name' required>
                                                        <option selected>Select</option>
                                                        @foreach($companies as $company)
                                                            <option value="{{$company->id}}">{{$company->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="company">Product</label>
                                                    <select class="form-control lead_product_id"  name='product_id' required>
                                                        <option selected>Select</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Premium</label>
                                                    <input type="number" name="premium" class="form-control" required placeholder="Enter Premium">
                                                </div>
                                            </div>
                                             <div class="col-6">
                                                 <div class="form-group">
                                                    <label>MOP</label>
                                                    <select class="form-control" name="mop">
                                                            <option value="">Select Mop</option>
                                                            <option value="yearly">Yearly</option>
                                                            <option value="half_yearly">Half Yearly</option>
                                                            <option value="quarterly">Quarterly</option>
                                                            <option value="monthly">Monthly</option>
                                                    </select>
                                                 </div>
                                             </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Issued Date</label>
                                                    <input type="date" name="issued_date" class="form-control" required placeholder="Enter Issued Date">
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="estimated sum insure" class="form-label">Est Sum
                                                    Insured</label>
                                                <input type="number" class="form-control" id="sun" name="sum_insured" placeholder="Enter Sum Insured">
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="insurancecompany">Term</label>
                                                <select class="form-control" id="maturity" name="maturity_period">
                                                    <option selected>Select</option>
                                                    @for($i=1;$i<=50;$i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                       <!--  @if(auth()->user()->role_id == 1)
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="insurancecompany">Lead Transfer Request</label>
                                                <select class="form-control" id="transfer" name="lead_transfer_req">
                                                    <option selected disabled>Select</option>
                                                   @foreach($users as $user)
                                                    <option>{{$user->username}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @endif -->
                                        @if(\Request::is('admin/leads'))
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="makeuser">Make Customer</label>
                                                <select class="form-control" id="is_user" name="is_user">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        @else
                                            <input type="hidden" name="is_user" value="1">
                                        @endif
                                    </div>
                                </div>
                                <div class="leadsform-action-btn">
                                       @if(!control('view-all-lead-table'))
                                            <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                                        @endif
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                </form>
            </div>
        </div>
    </div>
</div>
