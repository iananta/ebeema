@extends('layouts.backend.containerlist')

@section('title')
    Life Calculator
@endsection
@php
    $oldCompany = old('company');
    $oldProduct = old('product');
    $oldTerm = old('term');
    $oldAge = old('age');
@endphp
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{ route('calculator.life.calculate') }}" id="life-calculator-form">
                @csrf
                    @if(session('errorMessage'))
                    <div class="form-group ">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('errorMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="custom-row">
                        <!--company -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Company<label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" class="input-control search-select  validate-check" id="companies" name="company"
                                        onchange="findProducts()">
                                    <option selected value="">Select a company</option>
                                    @foreach($companies as $comp)
                                        <option value="{{ $comp->id }}" data-products="{{ $comp->products }}"
                                                @if(old('company')==$comp->id) selected @endif
                                        >
                                            {{ $comp->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback d-block" id="companyError"></div>

                            </div>
                        </div>
                          <!-- Products-->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Product</label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" class="input-control search-select  validate-check" id="products" name="product"
                                onchange="findTermsAndAges()">
                                <option selected value="">Select a product</option>
                                </select>
                                <div class="invalid-feedback d-block" id="productError"></div>
                            </div>
                        </div>
                        <!--age -->
                        <div class="form-input-wrapper" id="ages_container">
                            <div class="form-label">
                                <label>Age</label>
                            </div>
                            <div class="form-input">
                                <select
                                    data-validation="required"
                                    name="age"
                                    id="ages"
                                    class="input-control validate-check search-select {{ session()->has('ageError') ? 'is-invalid' : '' }}"
                                >
                                    <option selected value="">Select an age</option>
                                </select>
                                <div class="invalid-feedback d-block" id="ageError"></div>
                            </div>
                        </div>
                        <!-- huband age -->
                        <div class="form-input-wrapper d-none" id="husband_age_container">
                            <div class="form-label">
                                <label>Husband Age</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="text" class="input-control validate-check" id="husband_age" name="husband_age"
                                   placeholder="Enter Husband Age (in year)"
                                   {{ old('husband_age') }}>
                                <div class="invalid-feedback d-block" id="coupleAgeError"></div>
                                <div class="invalid-feedback d-block" id="husbandAgeError"></div>
                            </div>
                        </div>
                        <!--wife age -->
                        <div class="form-input-wrapper d-none" id="wife_age_container">
                            <div class="form-label">
                                <label>Wife Age</label>
                            </div>
                            <div class="form-input">
                                 <input data-validation="required" type="text" class="input-control validate-check" id="wife_age" name="wife_age"
                                   placeholder="Enter Wife Age (in year)" {{ old('wife_age') }} disabled="disabled">
                               <div class="invalid-feedback d-block" id="wifeAgeError"></div>
                            </div>
                        </div>
                        <!--child age -->
                        <div class="form-input-wrapper d-none" id="child_ages_container">
                            <div class="form-label">
                                <label>Child Age</label>
                            </div>
                            <div class="form-input">
                                <select
                                    data-validation="required"
                                    name="child_age"
                                    id="child_ages"
                                    class="input-control validate-check search-select {{ session()->has('ageError') ? 'is-invalid' : '' }}"
                            
                                >
                                    <option selected value="">Select an age</option>
                                </select>
                            </div>
                        </div>
                        <!--Proposer's Age -->
                        <div class="form-input-wrapper d-none" id='proposer_age_container'>
                            <div class="form-label">
                                <label>Proposer's Age</label>
                            </div>
                            <div class="form-input">
                                <select
                                    data-validation="required"
                                    name="proposer_age"
                                    id="proposer_age"
                                    class="input-control validate-check search-select"
                            
                                >
                                    <option selected  value="">Select Proposer's Age</option>
                                </select>
                            </div>
                        </div>
                        <!--term -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Term</label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" name="term" id="terms"
                                        class="input-control validate-check search-select {{ session()->has('termError') ? 'is-invalid' : '' }}">
                                    <option selected value="">Select a term</option>
                                </select>

                                <div class="invalid-feedback d-block mb-3 mx-2 mt-n2" id="maturityAgeError"></div>
                                 <div class="invalid-feedback d-block mb-3 mx-2 mt-n2" id="tableRateError"></div>
                            </div>
                        </div>
                        <!--sum assured -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Sum Assured</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="number" class="input-control validate-check" id="sum_assured" name="sum_assured"
                                   placeholder="Enter Sum Assured" value="{{ old('sum_assured') }}">
                                <div class="invalid-feedback d-block" id="sumAssuredError"></div>
                            </div>
                        </div>
                        <!--mop -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Mode of Payment</label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" name="loading_charge" id="loading_charge"
                                    class="input-control search-select validate-check">
                                        <option selected value="">Select a MoP</option>
                                        <option value="yearly" @if(old('loading_charge')=='yearly' ) selected @endif>
                                            Yearly
                                        </option>
                                        <option value="half_yearly" @if(old('loading_charge')=='half_yearly' ) selected @endif>
                                            Half Yearly
                                        </option>
                                        <option value="quarterly" @if(old('loading_charge')=='quarterly' ) selected @endif>
                                            Quarterly
                                        </option>
                                        <option value="monthly" @if(old('loading_charge')=='monthly' ) selected @endif>
                                            Monthly
                                        </option>
                                </select>
                                <div class="invalid-feedback d-block" id="mopError"></div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="form-input-wrapper d-none" id="featureRow">
                            <div class="form-label">
                                <label>Features</label>
                            </div>
                            <div class="form-input">
                                <div id="featuresTab">

                                </div>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Submit</button>
                            </div>
                        </div>
                    </div>

                <!-- Features -->
                </form>
            </div>
            <div class="form-input-details-wrapper">
                    
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_js')
 @include('Backend.globalScripts.form-validation')
 @include('Backend.Life.includes.lifecalculator_script')
@endsection