
<tr class="gradeX text-center">
    <th colspan="2">
        <h6>Bonus Calculation</h6>
        @if($product->bonusRate == 0)
            <span class="invalid-feedback d-block">Bonus rate not available. Please insert bonus
                            rate.</span>
        @endif
    </th>
</tr>
<tr class="gradeX">
    <th>Bonus Rate</th>
    <td class="index">
        {{ $product->bonusRate }}
    </td>
</tr>
<tr class="gradeX">
    <th>Bonus per Year</th>
    <td class="index">
        {{ $amountFormatter->format(round($product->bonusYearly, 0)) }}
    </td>
</tr>
<tr class="gradeX">
    <th>Bonus at the End of the Year</th>
    <td class="index">
        {{ $amountFormatter->format(round($product->bonus, 0)) }}
    </td>
</tr>

<tr class="gradeX">
    <th>Total Payment at the End of Insured Period</th>
    <td class="index">
        {{ $amountFormatter->format(round($product->totalPremiumAmount, 0)) }}
    </td>
</tr>
