<tr class="gradeX text-center">
    <th colspan="2">
        <h6>One Time Charge</h6>
    </th>
</tr>

<tr>
    <th>CRC Rate (One Time Charge)</th>
    <td class="index">
        {{ $product->crcRate }}
    </td>
</tr>
<tr>
    <th>CRC Amount (One Time Charge)</th>
    <td class="index">
        {{ $amountFormatter->format($product->crcAmount) }}
    </td>
</tr>

@if($selectedMop == 'yearly')
    <tr>
        <th>Annual Premium</th>
        <td class="index">
            {{ $amountFormatter->format($product->premiumAmountWithBenefit + $product->crcAmount) }}
        </td>
    </tr>
@elseif($selectedMop == 'half_yearly')
    <tr>
        <th>One time Premium is paid once so it is calculated in first Premium so H/Y</th>
        <td class="index">
            {{ $amountFormatter->format($product->premiumAmountWithBenefit/2 + $product->crcAmount) }}
        </td>
    </tr>
@elseif($selectedMop == 'quarterly')
    <tr>
        <th>One time Premium is paid once so it is calculated in first Premium so Quarterly</th>
        <td class="index">
            {{ $amountFormatter->format($product->premiumAmountWithBenefit/4 + $product->crcAmount) }}
        </td>
    </tr>
@elseif($selectedMop == 'monthly')
    <tr>
        <th>One time Premium is paid once so it is calculated in first Premium so Monthly</th>
        <td class="index">
            {{ $amountFormatter->format($product->premiumAmountWithBenefit/12 + $product->crcAmount) }}
        </td>
    </tr>
@endif
