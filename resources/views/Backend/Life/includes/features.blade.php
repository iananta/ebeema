@foreach($product->features as $feature)
    <tr>
        <th class="text-capitalize">{{ featureName($feature['code']) }} Rate</th>
        <td class="index">
            {{ $feature['rate'] }}
        </td>
    </tr>
    <tr>
        <th class="text-capitalize">{{ featureName($feature['code']) }} Amount</th>
        <td class="index">
            {{$amountFormatter->format($feature['amount']) }}
        </td>
    </tr>
    @if($product->has_loading_charge_on_features)
        <tr style="background-color: aliceblue;">
            <th class="text-capitalize">Loading Charge
                on {{ featureName($feature['code']) }}</th>
            <td class="index">
                {{ $feature['mopRate'] }}
            </td>
        </tr>
        <tr  style="background-color: aliceblue;">
            <th class="text-capitalize">{{ featureName($feature['code']) }} After Loading Charge
            </th>
            <td class="index">
                {{ $amountFormatter->format($feature['mopAmount']) }}
            </td>
        </tr>
    @endif
@endforeach
