 <script type="text/javascript">
        $(document).ready(function () {
            let oldCompany = parseInt({{$oldCompany}})
            if (oldCompany) {
                findProducts()
            }
        });
        let oldTerm = parseInt({{ $oldTerm }})
        let oldAge = parseInt({{ $oldAge }})
        let oldProduct = parseInt({{ $oldProduct }})
        function findProducts() {
            let data = $('option:selected', '#companies').attr('data-products');
            data = JSON.parse(data);

            let html = '';
            html += '<option selected value=" ">Select a Product</>';
            for (let i = 0; i < data.length; i++) {

                if (oldProduct == data[i].id) {
                    html += '<option ' +
                        'value="' + data[i].id +
                        '" data-category="' + data[i].category +
                        '" data-terms="' + JSON.stringify(data[i].terms) +
                        '" data-ages="' + JSON.stringify(data[i].ages) +
                        '" selected' +
                        '>'
                        + data[i].name + '</option>'
                } else {
                    html += '<option ' +
                        'value="' + data[i].id +

                        '" data-category="' + data[i].category +
                        '" data-terms="' + JSON.stringify(data[i].terms) +
                        '" data-ages="' + JSON.stringify(data[i].ages) +
                        '" data-proposer-ages="' + JSON.stringify(data[i].proposer_ages) +
                        '"' +
                        '>'
                        + data[i].name + '</option>'
                }
            }
            $('#products').html(html);
            if (oldProduct) {
                findTermsAndAges()
            }
        }

        function showInput(input) {
            $(`#${input}_container`).removeClass('d-none');
        }

        function hideInput(input) {
            $(`#${input}_container`).addClass('d-none');
        }

        function findTermsAndAges() {
            let plan = $('option:selected', '#products').attr('data-category');
            switch (plan) {
                case 'couple':
                    showInput('husband_age')
                    showInput('wife_age')
                    hideInput('child_ages')
                    hideInput('proposer_age')
                    hideInput('ages')

                    break;
                case 'children':
                    hideInput('husband_age')
                    hideInput('wife_age')
                    showInput('child_ages')
                    showInput('proposer_age')
                    hideInput('ages')

                    break;
                case 'education':
                    hideInput('husband_age')
                    hideInput('wife_age')
                    showInput('child_ages')
                    showInput('proposer_age')
                    hideInput('ages')

                    break;
                default:
                    hideInput('husband_age')
                    hideInput('wife_age')
                    hideInput('child_ages')
                    hideInput('proposer_age')
                    showInput('ages')

                    break;
            }

            let data = $('option:selected', '#products').attr('data-terms');
            data = JSON.parse(data);

            data = data.sort(function (a, b) {
                return a - b;
            });

            let html = '';
            html += '<option selected value="">Select a term</>';
            for (let i = 0; i < data.length; i++) {
                if (oldTerm == data[i]) {
                    html += '<option value="' + data[i] + '" selected>' + data[i] + '</option>'
                } else {
                    html += '<option value="' + data[i] + '">' + data[i] + '</option>'
                }
            }
            $('#terms').html(html);

            if (plan !== 'couple') {
                let ageData = $('option:selected', '#products').attr('data-ages');
                ageData = JSON.parse(ageData);

                ageData = ageData.sort(function (a, b) {
                    return a - b;
                });

                let html2 = '';
                html2 += '<option selected value="">Select an age</>';
                for (let i = 0; i < ageData.length; i++) {
                    if (oldAge == ageData[i]) {
                        html2 += '<option value="' + ageData[i] + '" selected>' + ageData[i] + '</option>'
                    } else {
                        html2 += '<option value="' + ageData[i] + '">' + ageData[i] + '</option>'
                    }
                }
                if (plan === 'children' || plan === 'education') {
                    $('#child_ages').html(html2);
                } else {
                    $('#ages').html(html2);
                }
            }
            if (plan === 'children' || plan === 'education') {
                let ageData = $('option:selected', '#products').attr('data-proposer-ages');
                ageData = JSON.parse(ageData);

                // console.log(ageData)

                ageData = ageData.sort(function (a, b) {
                    return a - b;
                });

                let html2 = '';
                html2 += '<option selected value="">Select proposers age</>';
                for (let i = 0; i < ageData.length; i++) {
                    if (oldAge == ageData[i]) {
                        html2 += '<option value="' + ageData[i] + '" selected>' + ageData[i] + '</option>'
                    } else {
                        html2 += '<option value="' + ageData[i] + '">' + ageData[i] + '</option>'
                    }
                }

                $('#proposer_age').html(html2);
            }


            let productId = $('#products').find(":selected").val();
            $.ajax({
                type: "GET",
                url: "{{ route('admin.get.feature.list') }}",
                dataType: 'json',
                data: {
                    'id': productId,
                },
                success: function (response) {
                    if (response.data.length > 0) {
                        let html = '';
                        for (let j = 0; j < response.data.length; j++) {

                            if (response.data[j].is_compulsory == 1) {
                                html += ' <div class="form-check">' +
                                    '<input type="hidden" name="features[]" class="form-check-input" value="' + response.data[j].code + '">' +
                                    '<input type="checkbox" name="features[]" class="form-check-input" value="' + response.data[j].code + '" checked >' +
                                    '<label class="form-check-label text-capitalize">' + response.data[j].name + '</label>' +
                                    '</div>'
                            } else {
                                html += ' <div class="form-check">' +
                                    '<input type="checkbox" name="features[]" class="form-check-input" value="' + response.data[j].code + '">' +
                                    '<label class="form-check-label text-capitalize">' + response.data[j].name + '</label>' +
                                    '</div>'
                            }


                        }
                        $('#featuresTab').html(html);
                        $('#featureRow').removeClass('d-none')

                    } else {
                        $('#featureRow').addClass('d-none')
                    }
                },
                error: function (error) {
                    $('#featureRow').addClass('d-none')
                }
            });

        }
        $('.validate-check').change(function (e) {
            validateData(false)
        })
         $('#submit-btn').click(function (e) {
            if(localStorage.getItem('validationCount') == 0 ){
                validateData(true)
            }
        })
       

        function validateData(shouldSubmit) {
            const form = $('#life-calculator-form');
            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "/admin/life/validate",
                data: form.serialize(),
                success: function (response) {
                    resetErrors();
                    if (shouldSubmit) {
                        form.submit();
                    }
                },
                error: function (error) {
                    resetErrors();
                    const type = error.responseJSON.type;
                    const message = error.responseJSON.message;
                    switch (type) {
                        case 'company':
                            $('#companyError').text(message);
                            break;
                        case 'product':
                            $('#productError').text(message);
                            break;
                        case 'age':
                            $('#ageError').text(message);
                            break;
                        case 'husband_age':
                            $('#husbandAgeError').text(message);
                            break;
                        case 'wife_age':
                            $('#wifeAgeError').text(message);
                            break;
                        case 'couple_age':
                            $('#coupleAgeError').text(message);
                            break;
                        case 'child_age':
                            $('#childAgeError').text(message);
                            break;
                        case 'term':
                            $('#termError').text(message);
                            break;
                        case 'sum_assured':
                            $('#sumAssuredError').text(message);
                            break;
                        case 'table_rate':
                            $('#tableRateError').text(message);
                            break;
                        case 'mop':
                            $('#mopError').text(message);
                            break;
                        case 'maturity_age':
                            $('#maturityAgeError').text(message);
                            break;
                    }
                },
            })
        }
        function resetErrors() {
            $('#companyError').text('');
            $('#productError').text('');
            $('#ageError').text('');
            $('#husbandAgeError').text('');
            $('#wifeAgeError').text('');
            $('#coupleAgeError').text('');
            $('#childAgeError').text('');
            $('#termError').text('');
            $('#sumAssuredError').text('');
            $('#maturityAgeError').text('');
            $('#tableRateError').text('');
            $('#mopError').text('');
        }
      
        // end of validate data


 </script>