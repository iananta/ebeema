<table class="table">
    <thead>
    <tr class="gradeX text-center">
        <th colspan="3">
            <h6>Payment Money Back Schedule </h6>
            @if(count($product->paybackSchedules) < 1) <span class="invalid-feedback d-block">Payback Schedule not
                            available. Please insert appropriate payback schedule data.</span>
            @endif
        </th>
    </tr>
    @if(count($product->paybackSchedules) > 1)
        <tr>
            <th scope="col">Payback Years</th>
            <th scope="col">Payback Percentage</th>
            <th scope="col">Payback Amount</th>
        </tr>
    </thead>
    <tbody>

    @foreach ($product->paybackSchedules as $payback)
        <tr>
            <td>{{ $payback->payback_year }}</td>
            <td>{{ round($payback->rate, 0) }} %</td>
            <td>{{ $amountFormatter->format(round($payback->amount, 0)) }}</td>
        </tr>
    @endforeach
    @endif
    </tbody>

</table>
