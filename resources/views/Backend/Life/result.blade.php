@extends('layouts.backend.containerlist')

@section('title')
    Life Calculator
@endsection
@php
    $oldCompany = old('company');
    $oldProduct = old('product');
    $oldTerm = old('term');
    $oldAge = old('age');

    $amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
    $amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
    $amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
    

@endphp
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
             <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{ route('calculator.life.calculate') }}" id="life-calculator-form">
                @csrf
                    @if(session('errorMessage'))
                    <div class="form-group ">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('errorMessage') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    @endif
                    <div class="custom-row">
                        <!--company -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Company<label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" class="input-control search-select  validate-check" id="companies" name="company"
                                        onchange="findProducts()">
                                    <option selected value="">Select a company</option>
                                    @foreach($companies as $comp)
                                        <option value="{{ $comp->id }}" data-products="{{ $comp->products }}"
                                                @if(old('company')==$comp->id) selected @endif
                                        >
                                            {{ $comp->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback d-block" id="companyError"></div>

                            </div>
                        </div>
                          <!-- Products-->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Product</label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" class="input-control search-select  validate-check" id="products" name="product"
                                onchange="findTermsAndAges()">
                                <option selected value="">Select a product</option>
                                </select>
                                <div class="invalid-feedback d-block" id="productError"></div>
                            </div>
                        </div>
                        <!--age -->
                        <div class="form-input-wrapper" id="ages_container">
                            <div class="form-label">
                                <label>Age</label>
                            </div>
                            <div class="form-input">
                                <select
                                    data-validation="required"
                                    name="age"
                                    id="ages"
                                    class="input-control validate-check search-select {{ session()->has('ageError') ? 'is-invalid' : '' }}"
                                >
                                    <option selected value="">Select an age</option>
                                </select>
                                <div class="invalid-feedback d-block" id="ageError"></div>
                            </div>
                        </div>
                        <!-- huband age -->
                        <div class="form-input-wrapper d-none" id="husband_age_container">
                            <div class="form-label">
                                <label>Husband Age</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="text" class="input-control validate-check" id="husband_age" name="husband_age"
                                   placeholder="Enter Husband Age (in year)"
                                   {{ old('husband_age') }}>
                                <div class="invalid-feedback d-block" id="coupleAgeError"></div>
                                <div class="invalid-feedback d-block" id="husbandAgeError"></div>
                            </div>
                        </div>
                        <!--wife age -->
                        <div class="form-input-wrapper d-none" id="wife_age_container">
                            <div class="form-label">
                                <label>Wife Age</label>
                            </div>
                            <div class="form-input">
                                 <input data-validation="required" type="text" class="input-control validate-check" id="wife_age" name="wife_age"
                                   placeholder="Enter Wife Age (in year)" {{ old('wife_age') }} disabled="disabled">
                               <div class="invalid-feedback d-block" id="wifeAgeError"></div>
                            </div>
                        </div>
                        <!--child age -->
                        <div class="form-input-wrapper d-none" id="child_ages_container">
                            <div class="form-label">
                                <label>Child Age</label>
                            </div>
                            <div class="form-input">
                                <select
                                    data-validation="required"
                                    name="child_age"
                                    id="child_ages"
                                    class="input-control validate-check search-select {{ session()->has('ageError') ? 'is-invalid' : '' }}"
                            
                                >
                                    <option selected value="">Select an age</option>
                                </select>
                            </div>
                        </div>
                        <!--Proposer's Age -->
                        <div class="form-input-wrapper d-none" id='proposer_age_container'>
                            <div class="form-label">
                                <label>Proposer's Age</label>
                            </div>
                            <div class="form-input">
                                <select
                                    data-validation="required"
                                    name="proposer_age"
                                    id="proposer_age"
                                    class="input-control validate-check search-select"
                            
                                >
                                    <option selected  value="">Select Proposer's Age</option>
                                </select>
                            </div>
                        </div>
                        <!--term -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Term</label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" name="term" id="terms"
                                        class="input-control validate-check search-select {{ session()->has('termError') ? 'is-invalid' : '' }}">
                                    <option selected value="">Select a term</option>
                                </select>

                                <div class="invalid-feedback d-block mb-3 mx-2 mt-n2" id="maturityAgeError"></div>
                                 <div class="invalid-feedback d-block mb-3 mx-2 mt-n2" id="tableRateError"></div>
                            </div>
                        </div>
                        <!--sum assured -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Sum Assured</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="number" class="input-control validate-check" id="sum_assured" name="sum_assured"
                                   placeholder="Enter Sum Assured" value="{{ old('sum_assured') }}">
                                <div class="invalid-feedback d-block" id="sumAssuredError"></div>
                            </div>
                        </div>
                        <!--mop -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Mode of Payment</label>
                            </div>
                            <div class="form-input">
                                <select data-validation="required" name="loading_charge" id="loading_charge"
                                    class="input-control search-select validate-check">
                                        <option selected value="">Select a MoP</option>
                                        <option value="yearly" @if(old('loading_charge')=='yearly' ) selected @endif>
                                            Yearly
                                        </option>
                                        <option value="half_yearly" @if(old('loading_charge')=='half_yearly' ) selected @endif>
                                            Half Yearly
                                        </option>
                                        <option value="quarterly" @if(old('loading_charge')=='quarterly' ) selected @endif>
                                            Quarterly
                                        </option>
                                        <option value="monthly" @if(old('loading_charge')=='monthly' ) selected @endif>
                                            Monthly
                                        </option>
                                </select>
                                <div class="invalid-feedback d-block" id="mopError"></div>
                            </div>
                        </div>
                        <!-- feature -->
                        <div class="form-input-wrapper d-none" id="featureRow">
                            <div class="form-label">
                                <label>Features</label>
                            </div>
                            <div class="form-input">
                                <div id="featuresTab">

                                </div>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Submit</button>
                            </div>
                        </div>
                    </div>

                <!-- Features -->
                </form>
            </div>
            <div class="form-input-details-wrapper">
                     @if ($product->rate)
                <table id="tablebody" class="table table-bordered table-hover role-table life-policy-calculation">
                    <thead>
                    <tr class="gradeX">
                        <th class="text-dark">Properties</th>
                        <th class="text-dark index">Values</th>
                    </tr>
                    <tr class="gradeX">
                        <th>Company</th>
                        <td class="index">{{ $product->company->name }}</td>
                    </tr>
                    <tr class="gradeX">
                        <th>Product</th>
                        <td class="index">{{ $product->name }}</td>
                    </tr>
                    @if($product['category'] == 'couple')
                        <tr class="gradeX">
                            <th>Husband Age</th>
                            <td class="index">
                                {{ $selectedHusbandAge }}
                            </td>
                        </tr>
                        <tr class="gradeX">
                            <th>Wife Age</th>
                            <td class="index">
                                {{ $selectedWifeAge }}
                            </td>
                        </tr>
                        <tr class="gradeX">
                            <th>Average Age</th>
                            <td class="index">
                                {{ $averageAge }}
                            </td>
                        </tr>
                    @elseif($product->isChildPlan)
                        <tr class="gradeX">
                            <th>Child Age</th>
                            <td class="index">
                                {{ $selectedChildAge }}
                            </td>
                        </tr>
                        <tr class="gradeX">
                            <th>Proposer's Age</th>
                            <td class="index">
                                {{ $selectedProposerAge }}
                            </td>
                        </tr>
                    @else
                        <tr class="gradeX">
                            <th>Age</th>
                            <td class="index">
                                {{ $selectedAge }}
                            </td>
                        </tr>
                    @endif
                    @if($product['category'] == 'children' && $product->isLic)
                        <tr>
                            <th>Term</th>
                            <td class="index">
                                {{ $selectedTerm }}
                            </td>
                        </tr>
{{--                        <tr>--}}
{{--                            <th>Actual Term (According to child age)</th>--}}
{{--                            <td class="index">--}}
{{--                                {{ $selectedTerm - $selectedChildAge }}--}}
{{--                            </td>--}}
{{--                        </tr>--}}
                    @else
                        <tr>
                            <th>Term</th>
                            <td class="index">
                                {{ $selectedTerm }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th>Sum Assured</th>
                        <td class="index">
                            {{ $amountFormatter->format($selectedSumAssured) }}
                        </td>
                    </tr>
                    <tr>
                        <th>Mode of Payment</th>
                        <td class="index text-capitalize">
                            {{ str_replace('_', ' ', $selectedMop) }}
                        </td>
                    </tr>
                    <tr>
                        <th>Rate</th>
                        <td class="index">
                            {{ $product->rate }}
                        </td>
                    </tr>
                    @if($product->caseLic)
                        <tr>
                            <th>Loading Charge on MoP</th>
                            <td class="index">
                                {{ $product->mop }}
                            </td>
                        </tr>
                    @endif

                    @if(!$product->isNational && $product->discountRate != 0 )
                        <tr>
                            <th>Discount on Sum Assured</th>
                            <td class="index">
                                {{ $product->discountRate }}
                            </td>
                        </tr>
                    @endif

                    <tr>
                        <th>New Rate</th>
                        <td class="index">
                            {{ $product->newRate }}
                        </td>
                    </tr>

                    @if($product->discountOnPremium)
                        <tr>
                            <th>Premium Before Discount</th>
                            <td class="index">
                                {{ $amountFormatter->format($product->premiumBeforeDiscount) }}
                            </td>
                        </tr>
                        <tr>
                            <th>Discount on Premium</th>
                            <td class="index">
                                {{ $amountFormatter->format($product->discountOnPremium) }}
                            </td>
                        </tr>
                    @endif

                    @if (!$product->isLic)
                        <tr>
                            <th>Premium Amount (Before Discount/Charge on MoP)</th>
                            <td class="index">
                                {{ $amountFormatter->format($product->premiumBeforeCharge) }}
                            </td>
                        </tr>
                        <tr>
                            <th>Discount/Charge on MoP</th>
                            <td class="index">
                                {{ $amountFormatter->format(abs($product->mopAmount)) }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th>Premium Amount (after MoP)</th>
                        <td class="index">
                            {{ $amountFormatter->format($product->premiumAmount) }}
                        </td>
                    </tr>

                    @if($product->features && count($product->features) > 0)
                        @include('Backend.Life.includes.features')
                    @endif

                    <tr>
                        <th>Total Premium Amount</th>
                        <td class="index">
                            {{ $amountFormatter->format($product->premiumAmountWithBenefit) }}
                        </td>
                    </tr>

                    @if($selectedMop == 'half_yearly')
                        <tr>
                            <th>Half Yearly Premium Amount</th>
                            <td class="index">
                                {{ $amountFormatter->format($product->premiumAmountWithBenefit/2) }}
                            </td>
                        </tr>
                    @elseif($selectedMop == 'quarterly')
                        <tr>
                            <th>Quarterly Premium Amount</th>
                            <td class="index">
                                {{ $amountFormatter->format($product->premiumAmountWithBenefit/4) }}
                            </td>
                        </tr>
                    @elseif($selectedMop == 'monthly')
                        <tr>
                            <th>Monthly Premium Amount</th>
                            <td class="index">
                                {{ $amountFormatter->format($product->premiumAmountWithBenefit/12) }}
                            </td>
                        </tr>
                    @endif

                    @if($product->crcRate !== 0)
                        @include('Backend.Life.includes.crc')
                    @endif

                    @include('Backend.Life.includes.bonus')

                    @if($selectedCategory != 'money-back')
                        <tr class="gradeX">
                            <th>Net Gain</th>
                            <td class="index">
                                {{ $amountFormatter->format(round($product->netGain, 0)) }}
                            </td>
                        </tr>
                    @endif
                    </thead>
                </table>

                @if(isset($product->paybackSchedules) && $selectedCategory == 'money-back')
                    @include('Backend.Life.includes.payback')
                @endif

                @if($product->paying_term()->where('term_id', $term->id)->first())
                    <table class="table table-bordered table-hover role-table">
                        <thead>
                        <tr class="gradeX text-center">
                            <th colspan="2">
                                <h6>Premium Paying Term</h6>
                            </th>
                        </tr>
                        <tr class="gradeX">
                            <th> PremiumPaying Term</th>
                            <td class="index">
                                {{ $product->paying_term()->where('term_id', $term->id)->first()->paying_year }}
                            </td>
                        </tr>
                        </thead>
                    </table>
                @endif

            @else
                No Data Found
            @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_js')
 @include('Backend.globalScripts.form-validation')
 @include('Backend.Life.includes.lifecalculator_script')
@endsection