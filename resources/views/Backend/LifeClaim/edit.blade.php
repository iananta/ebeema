@extends('layouts.backend.containerlist')


@section('dynamicdata')

    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Add Claims</h3>
        </div>
        <div class="box-body">
            @include('layouts.backend.alert')
            <form action="{{ route('life.claim.update', $claim->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <p class="card-inside-title">Name</p>
                    <div class="form-line">
                        <input class="form-control form-control-inline input-medium" name="name" size="16" type="text"
                               id="name" value="{!! $claim->name !!}" placeholder="Claim Name" />
                    </div>
                </div>

                <div class="form-group">
                    <p class="card-inside-title">Policy No.</p>
                    <div class="form-line">
                        <input class="form-control form-control-inline input-medium" name="policy_no" size="16" type="text"
                               id="policy_no" value="{!! $claim->policy_no !!}" placeholder="Policy Number" />
                    </div>
                </div>

                <div class="form-line">
                    <label for="exampleFormControlSelect1">Insurance Company</label>
                    <select class="form-control" id="company_id" name="company_id">
                        <option selected disabled>Select a company</option>
                        @foreach($companies as $company)
                        <option
                            value="{{ $company->id }}"
                            @if ($company->id == $claim->company_id) selected @endif>
                            {{ $company->name }}
                        </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group ">
                    <p class="card-inside-title">Image</p>
                    <div class="form-line">
                        <input type="file" class="form-control form-control-inline input-medium" name="image" />
                        <img style="max-width : 10%" src="/images/claims/{{$claim->image}}">
                    </div>
                </div>

                <div class="form-group">
                    <p class="card-inside-title">Phone No.</p>
                    <div class="form-line">
                        <input class="form-control form-control-inline input-medium" name="phone_no" size="16" type="text"
                               id="phone_no" value="{!! $claim->phone_no !!}" placeholder="Phone Number" />
                    </div>
                </div>

                <div class="form-group">
                    <p class="card-inside-title">Claim Status:</p>
                    <div class="form-line">
                        <select name="status" class="form-control">
                            <option value="0" @if($claim->status == 0) selected @endif>Inactive</option>
                            <option value="1" @if($claim->status == 1) selected @endif>Active</option>
                            <option value="2" @if($claim->status == 2) selected @endif>Processing</option>
                            <option value="3" @if($claim->status == 3) selected @endif>Accepted</option>
                            <option value="4" @if($claim->status == 4) selected @endif>Rejected</option>
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit"  class="btn btn-info waves-effect">
                    <span>Submit
                        <i class="fa fa-check"></i>
                    </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
@stop
