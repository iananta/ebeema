@extends('layouts.backend.containerlist')

@section('title')
Claims
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-body">
        <div class="table-action-wrapper">
            <div class="justify-content-end list-group list-group-horizontal ">
                <a href="{{route('life.claim.add')}}"><button
                    class="btn btn-primary c-primary-btn add-modal shadow-none"><img
                    src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                </button></a>
            </div>
        </div>
        <div class="dataTables_wrapper dt-bootstrap4">
            @include('layouts.backend.alert')

            <table id="example1" class="table table-bordered table-hover role-table">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Name</th>
                    <th>Company Name</th>
                    <th>Image</th>
                    <th>Policy No.</th>
                    <th>Phone No.</th>
                    <th>Status</th>
                    <th class="dt-center">Actions</th>
                </tr>
                <tbody id="tablebody">

                @foreach($lifeClaim as $index=>$claim)
                <tr class="gradeX" id="row_{{ $claim->id }}">
                    <td class="index">
                        {{ ++$index }}
                    </td>
                    <td class="name">
                        {{ $claim->name }}
                    </td>
                    <td class="name">
                        {{ $claim->company->name }}
                    </td>
                    <td style="width: 20%" class="compare-parts line-rht-cmp" scope="row">
                        <img style="max-width:100%" src="/images/claims/{{$claim->image}}">
                    </td>
                    <td class="name">
                        {{ $claim->policy_no }}
                    </td>
                    <td class="name">
                        {{ $claim->phone_no }}
                    </td>
                    <td class="name">
                        @if ($claim->status == '0')  Inactive @endif
                        @if ($claim->status == '1') Active @endif
                        @if ($claim->status == '2') Processing @endif
                        @if ($claim->status == '3') Accepted @endif
                        @if ($claim->status == '4') Rejected @endif
                    </td>
                    <td class="name">
                        <a href="{{ route('life.claim.edit', $claim->id) }}" id="{{ $claim->id }}"
                           title="Edit "><button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button></a>&nbsp;

                        <a href="javascript:;" title="Delete Claim" class="delete-claim"
                           id="{{ $claim->id }}">
                            <button type="button" class="btn btn-danger btn-flat"><i
                                    class="fa fa-trash"></i></button>
                        </a>

                    </td>
                </tr>
                @endforeach
                </tbody>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('footer_js')
<script type="text/javascript">
    $('#tablebody').on('click', '.delete-claim', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (response) {
            if (response.isConfirmed == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/lifeClaim') }}" + "/" + id,
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        new swal(
                            'Success',
                            response.message,
                            'success'
                        )
                        $object.parent().parent().remove()
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        }).catch(swal.noop);
    });</script>
@endsection
