@extends('layouts.backend.containerlist')

@section('title')
    Private Motor Premium Calculator
@endsection
@section('footer_js')

    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
    <script type="text/javascript">

        $('#contactForm').on('submit', function (event) {
            event.preventDefault();

            let age = $('#age').val();
            let term = $('#term').val();
            let sum_assured = $('#sum_assured').val();
            // let subject = $('#subject').val();
            //let message = $('#message').val();


            $.ajax({
                url: "/dhanBristi",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    age: age,
                    term: term,
                    sum_assured: sum_assured,
                    //  subject:subject,
                    //message:message,
                },
                success: function (response) {
                    swal('success', response.message, 'success');
                    $('#tablebody').find('.index').text(
                        response.age);

                },
            });
        });
        $('.excessCalculate').on('change', function () {
            var TYPECOVER = $("#TYPECOVERVal").val();
            var CATEGORYID = $("#categoryListId").val();
            var YEARMANUFACTURE = $("#manufYear").val();

            $.ajax({
                type: "POST",
                url: "{{ route('nonLife.calculator.motor.compulsary.excess') }}",
                data: {
                    TYPECOVER: TYPECOVER,
                    CATEGORYID: CATEGORYID,
                    YEARMANUFACTURE: YEARMANUFACTURE
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response.CompulsoryExcess);
                    if (response.CompulsoryExcess != 0) {
                        $('.cmp-excess-amt').val(response.CompulsoryExcess);
                    } else {
                        $('.cmp-excess-amt').val('N/A');
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });
        });
        $('.excess-damage').on('change', function () {
            var TYPECOVER = $("#TYPECOVERVal").val();
            var CATEGORYID = $("#categoryListId").val();
            var CLASSCODE = $("#CLASSCODE").val();
            var VEHICLETYPE = 0;

            $.ajax({
                type: "POST",
                url: "{{ route('nonLife.calculator.motor.excess.damage') }}",
                data: {
                    TYPECOVER: TYPECOVER,
                    CATEGORYID: CATEGORYID,
                    CLASSCODE: CLASSCODE,
                    VEHICLETYPE: VEHICLETYPE
                },
                dataType: 'json',
                success: function (response) {
                    if (response.excessdamages) {
                        $('#ExcessOwnDamage').empty();
                        for (var i = 0; i < response.excessdamages.length; i++) {
                            $('#ExcessOwnDamage').append($('<option>', {
                                value: response.excessdamages[i].TARIFF,
                                text: response.excessdamages[i].TARIFF
                            }));
                        }
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });
        });
        $(".ncdCheck").on('change', function (){
            var currentYear = new Date().getFullYear();
            var selectedYear = $(".fcManuYear").val();
            var noClaimDisc = $("#NCDYR").val();
            if(currentYear && selectedYear && currentYear == selectedYear && noClaimDisc >= 1){
                $("#NCDYR").val(0).change();
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error',
                    html: 'Something went wrong: No Claim Discount cannot be taken for this current year of manufacture.',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                });
            }
            // console.log(selectedYear,currentYear,noClaimDisc);
        });
    </script>
@endsection
@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection

@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Private Motor Calculation</h3>
        </div>
        <div class="box-body">
            @empty ($categories)
                <div class="alert alert-warning alert-dismissible">
                    <button class="close close-sm" data-dismiss="alert" type="button" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4>
                        <i class="icon fa fa-warning">
                        </i>
                        Warning!
                    </h4>
                    Something wrong with data please <a  href="#" onclick="window.location.reload(true);">reload page </a> or contact administrative.
                </div>
            @endif
            @include('layouts.backend.alert')
            <ul class="nav nav-tabs calculation-nav">
                <li><a class="active" data-toggle="tab" href="#firstParty">Comprehensive</a></li>
                <li><a data-toggle="tab" href="#thirdParty">Third Party</a></li>
            </ul>

            <div class="tab-content">
                <div id="firstParty" class="tab-pane fade in active show">
                    <form class="calc-from" id="calculationForm"
                          action="{{route('nonLife.calculator.motor.premium.calculate')}}" method="POST">
                        @csrf
                        <input type="hidden" id="CLASSCODE" name="CLASSID" value="{{$classId}}">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label class="sr-only" for="CATEGORYID">Category</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Category</div>
                                    </div>
                                    <select class="form-control excessCalculate excess-damage"
                                            id="categoryListId"
                                            name="CATEGORYID"
                                            required>
                                        <option selected disabled>Select the Category</option>
                                        @if(isset($categories))
                                        @foreach ($categories->data as $cat)
                                            <option value="{{ $cat->CATEGORYID }}">
                                                {{ $cat->CATEGORYNAME }}
                                            </option>
                                        @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12 d-none">
                                <label class="sr-only" for="inlineFormInputGroupUsername2">Type of cover</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Type of cover</div>
                                    </div>
                                    <select class="form-control excessCalculate"
                                            id="TYPECOVERVal"
                                            name="TYPECOVER"
                                            required>
{{--                                        <option selected disabled>Select the type cover [Comprehensive (CM), Third Party (TP)]</option>--}}
                                        @if(isset($typeCovers->data))
                                            @foreach($typeCovers->data as $tc)
                                                <option
                                                    {{$tc->CODE == 'CM' ? 'selected' : ''}} value="{{$tc->CODE}}">{{$tc->CODE}}</option>
                                            @endforeach
                                        @else
                                            <option>CM</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">Year Manufacture</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Year Manufacture</div>
                                    </div>
                                    <select class="form-control excessCalculate excess-damage ncdCheck fcManuYear"
                                            id="manufYear"
                                            name="YEARMANUFACTURE"
                                            required>
                                        <option selected disabled>Select Manufactured Year</option>
                                        @if(isset($makeYearLists))
                                        @foreach($makeYearLists->data as $year)
                                            <option value="{{$year->Manu_Year}}">{{$year->Manu_Year}}</option>
                                        @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="CC">Cubic Capacity (cc)</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Cubic Capacity (cc) / KW</div>
                                    </div>
                                    <input type="number" class="form-control" id="CCHP"
                                           placeholder="Car Cubic Capacity e.g. 1200"
                                           name="CCHP" required min="0" step="any"
                                           value="{{ $_GET['CCHP']??'' }}">
                                </div>
                            </div>

                            <div class="form-group col-md-12 d-none">
                                <label class="sr-only" for="inlineFormInputGroupUsername2">Goods Carrying
                                    cap/Tons/Hp</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Goods Carrying cap/Tons/Hp</div>
                                    </div>
                                    <input type="number" class="form-control" id="CARRYCAPACITY"
                                           placeholder="Please enter goods carrying"
                                           name="CARRYCAPACITY" min="0"
                                           value="{{ $_GET['CARRYCAPACITY']??'0' }}">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="EXPUTILITIESAMT">Vehicle Cost</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Vehicle Cost</div>
                                    </div>
                                    <input type="number" class="form-control" id="EXPUTILITIESAMT"
                                           placeholder="Please enter Vehicle Cost"
                                           name="EXPUTILITIESAMT" required min="1"
                                           value="{{ $_GET['EXPUTILITIESAMT']??'' }}">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">Voluntary  Excess</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Voluntary  Excess</div>
                                    </div>
                                    <select class="form-control form-control-inline input-medium" id="ExcessOwnDamage"
                                            name="EODAMT">
                                        @if(isset($excessdamages))
                                        @foreach($excessdamages->data as $damage)
                                            <option value="{{$damage->TARIFF}}">{{$damage->TARIFF}}</option>
                                        @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="sr-only" for="ncdyr">NCD Year</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">NCD (No Claim Discount) Year</div>
                                    </div>
                                    <select class="form-control form-control-inline input-medium ncdCheck" id="NCDYR"
                                            name="NCDYR">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
{{--                                        <option value="2">2</option>--}}
{{--                                        <option value="3">3</option>--}}
{{--                                        <option value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12 d-none ncd-check-btn"></div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">P.A. to Driver</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">P.A. to Driver</div>
                                    </div>
                                    <input type="text" class="form-control" id="PADRIVER"
                                           placeholder="Please enter payble amount."
                                           name="PADRIVER" value="500000" readonly>
                                    <input type="hidden" name="Driver" value="1" readonly>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">No. of Passenger</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">No. of Passenger</div>
                                    </div>
                                    <input type="number" class="form-control" id="NOOFPASSENGER"
                                           placeholder="Please enter No. of Passengers." min="0"
                                           name="NOOFPASSENGER" value="{{ $_GET['NOOFPASSENGER']??'4' }}">
                                    <input type="text" class="form-control" id="PAPASSENGER"
                                           placeholder="Please enter payble amount."
                                           name="PAPASSENGER" value="500000" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">Compulsary Excess</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Compulsary Excess</div>
                                    </div>
                                    <input type="text" class="form-control cmp-excess-amt" id="compulsaryExcessAmount"
                                           placeholder="Compulsary excess amount."
                                           name="compulsaryexcessamount" value="0" readonly>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Include Towing Charge</div>
                                    <div class="input-group-text radio-box">
                                        <input class="form-check" style="height: 15px; width: 15px"
                                               aria-label="Checkbox for Towing Charge"
                                               type="checkbox" value="1" id="INCLUDE_TOWING" name="INCLUDE_TOWING">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Exclude Pool Premium</div>
                                    <div class="input-group-text radio-box">
                                        <input class="form-check" style="height: 15px; width: 15px"
                                               aria-label="Checkbox for Pool Premium"
                                               type="checkbox" value="" id="pool_premium" name="pool_premium">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6 d-none">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Own Goods Carrying/Private Rent</div>
                                    <div class="input-group-text radio-box">
                                        <input class="form-check" style="height: 15px; width: 15px"
                                               aria-label="Checkbox for Own Goods"
                                               type="checkbox" value="1" id="PRIVATE_USE" name="PRIVATE_USE">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Is government vehicle !</div>
                                    <div class="input-group-text radio-box">
                                        <input class="form-check" style="height: 15px; width: 15px"
                                               aria-label="Checkbox If it is government vehicle"
                                               type="checkbox" value="1" id="ISGOVERNMENT" name="ISGOVERNMENT">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-6 d-none">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Has Agent !</div>
                                    <div class="input-group-text radio-box">
                                        @if(Auth::user()->role_id == 5)
                                            <input type="hidden" value="1" id="HASAGENT" name="HASAGENT">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary mb-2 submit">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div id="thirdParty" class="tab-pane fade">
                    <form class="calc-from" id="calculationForm"
                          action="{{route('nonLife.calculator.motor.premium.calculate')}}" method="POST">
                        @csrf
                        <input type="hidden" name="user_Id" value="{{Auth::user()->id}}">
                        <input type="hidden" id="CLASSCODE" name="CLASSID" value="{{$classId}}">
                        <input type="hidden" id="EXPUTILITIESAMT" name="EXPUTILITIESAMT" value="0">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label class="sr-only" for="CATEGORYID">Category</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Category</div>
                                    </div>
                                    <select class="form-control excessCalculate"
                                            id="categoryListId"
                                            name="CATEGORYID"
                                            required>
                                        <option selected disabled>Select the Category</option>
                                        @if(isset($categories))
                                        @foreach ($categories->data as $cat)
                                            <option value="{{ $cat->CATEGORYID }}">
                                                {{ $cat->CATEGORYNAME }}
                                            </option>
                                        @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12 d-none">
                                <label class="sr-only" for="inlineFormInputGroupUsername2">Type of cover</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Type of cover</div>
                                    </div>
                                    <select class="form-control excessCalculate"
                                            id="TYPECOVERVal"
                                            name="TYPECOVER"
                                            required>
{{--                                        <option selected disabled>Select the type cover [Comprehensive (CM), Third Party (TP)]</option>--}}
                                        @if(isset($typeCovers->data))
                                        @foreach($typeCovers->data as $tc)
                                            <option {{$tc->CODE == "TP" ? 'selected' : ''}} value="{{$tc->CODE}}">{{$tc->CODE}}</option>
                                        @endforeach
                                        @else
                                            <option>TP</option>
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">Year Manufacture</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Year Manufacture</div>
                                    </div>
                                    <select class="form-control excessCalculate"
                                            id="manufYear"
                                            name="YEARMANUFACTURE"
                                            required>
                                        <option selected disabled>Select Manufactured Year</option>
                                        @if(isset($makeYearLists->data))
                                        @foreach($makeYearLists->data as $year)
                                            <option value="{{$year->Manu_Year}}">{{$year->Manu_Year}}</option>
                                        @endforeach
                                            @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="sr-only" for="CC">Cubic Capacity (cc)</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Cubic Capacity (cc) / KW</div>
                                    </div>
                                    <input type="number" class="form-control" id="CCHP"
                                           placeholder="Car Cubic Capacity e.g. 1200"
                                           name="CCHP" required min="0" step="any"
                                           value="{{ $_GET['CCHP']??'' }}">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">P.A. to Driver</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">P.A. to Driver</div>
                                    </div>
                                    <input type="text" class="form-control" id="PADRIVER"
                                           placeholder="Please enter payble amount."
                                           name="PADRIVER" value="500000" readonly>
                                    <input type="hidden" name="Driver" value="1" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="sr-only" for="inlineFormInputName2">No. of Passenger</label>
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">No. of Passenger</div>
                                    </div>
                                    <input type="number" class="form-control" id="NOOFPASSENGER"
                                           placeholder="Please enter No. of Passengers." min="0"
                                           name="NOOFPASSENGER" value="{{ $_GET['NOOFPASSENGER']??'4' }}">
                                    <input type="text" class="form-control" id="PAPASSENGER"
                                           placeholder="Please enter payble amount."
                                           name="PAPASSENGER" value="500000" readonly>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary mb-2 submit">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>


    <!-- /.box -->
    </div>

    <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
