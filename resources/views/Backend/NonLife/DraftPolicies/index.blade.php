@extends('layouts.backend.containerlist')

@section('title')
    Draft Policies
@endsection
@section('footer_js')
    {!! $dataTable->scripts() !!}
@endsection
@section('dynamicdata')


    <div class="box">
        @include('layouts.backend.alert')
        <div class="box-body">
            {!! $dataTable->table() !!}
        </div>

    </div>
@stop
