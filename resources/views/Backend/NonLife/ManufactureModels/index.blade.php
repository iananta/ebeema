@extends('layouts.backend.containerlist')

@section('title')
    Nonlife Manufacture Models
@endsection
@section('footer_js')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var oTable = $('#manuTable').dataTable();
        $('#tablebody').on('click', '.delete-item', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {

                    $.ajax({
                        type: "post",
                        url: "{{ url('/motor/manufacture/company/model/delete/') }}" + "/" + id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            oTable.fnDeleteRow(nRow);
                            new swal('success', response.message, 'success').catch(swal.noop);
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error').catch(swal.noop);
                        }
                    });
                }
            }).catch(swal.noop);
        });

        $('#tablebody').on('click', '.change-status', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You are going to change the status!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('/motor/manufacture/company/model/status/') }}" + "/" + id,
                        data: {
                            'id': id,
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            new swal('Success', response.message, 'success');
                            if (response.manufacture.status == 1) {
                                $($object).children().removeClass('bg-red').html(
                                    'InActive');
                                $($object).children().addClass('bg-blue').html(
                                    'Active');
                                $($object).attr('title', 'Deactivate');
                            } else {
                                $($object).children().removeClass('bg-blue').html(
                                    'Active');
                                $($object).children().addClass('bg-red').html(
                                    'InActive');
                                $($object).attr('title', 'Activate');
                            }
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });
    </script>
@endsection


@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')
            <div class="justify-content-end list-group list-group-horizontal ">
                <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button"
                   aria-expanded="false" aria-controls="multiCollapseExample1">
                    <i class="fa fa-chevron-down"></i>
                    Import excel
                </a>
            </div>
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <div class="card card-body">
                    <form class="form-data" action="{{ route('motor.manufacture.company.model.upload.excel') }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group p-2">
                            <label for="excelUpload">Excel File Upload</label>
                            <input name="excel_file" type="file" class="form-control-file" id="excelUpload"
                                   data-validation="required">
                        </div>
                        <button type="submit" class="btn btn-primary">Upload Excel</button>
                    </form>
                </div>
            </div>
            <br>
            <div class="blank-page">
                <table class="table" id="manuTable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Category</th>
                        <th scope="col">Name</th>
                        <th scope="col">Vehice detail</th>
                        <th scope="col">Engine Capacity (CC)</th>
                        <th scope="col">Selling Price(SP)</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($models as $model)
                    @foreach($model->getmmc as $mmccat)
                        <tr class="row1" data-id="{{ $model->id }}">
                            <th scope="row">{{$loop->iteration}}</th>
                            <th>
                                @if($mmccat['category'] == 21) MotorCycle @elseif($mmccat['category'] == 22) Commercial
                                Vehicle @elseif($mmccat['category'] == 23) Private Vehicle @endif
                            </th>
                            <th value="{{$mmccat->id}}">{{ $mmccat->name }}</th>
                            <th>{{$model->vehicle_type}}</th>
                            <th>{{$model->engine_capacity}}</th>
                            <th>{{$model->selling_price}}</th>
                            <th><a href="javascript:;" class="change-status ml-3"
                                   title="{{ $model->status == 0 ? "Click to activate Category." : "Click to De-active Category." }}"
                                   id="{{ $model->id }}">
                                    @if($model->status == 1)
                                        <button type="button"
                                                class="btn bg-blue btn-primary btn-flat ">
                                            Active
                                        </button>
                                    @else
                                        <button type="button"
                                                class="btn bg-red btn-primary btn-flat">
                                            InActive
                                        </button>
                                    @endif
                                </a>
                            </th>
                            <th class="justify-content-center">
                                <a href="javascript:;" title="Delete" class="delete-item" id="{{ $model->id }}">
                                    <button type="button" class="btn btn-danger btn-flat">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>
                            </th>
                        </tr>
                        @endforeach
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

