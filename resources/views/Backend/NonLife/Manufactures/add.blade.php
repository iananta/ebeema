@section('header_css')
    <style>
        .img-responsive{
            max-width: 100%;
        }
        .gallery-img{
            height: 150px;
            width: 150px;
        }
    </style>
@endsection

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Add Manufacture Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                    <form method="POST" action="{{route('motor.manufacture.company.list.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Category</label>
                            <select class="form-control" name="category">
                                <option selected disabled>Select Motor Category</option>
                                <option value="21">Motorcycle</option>
                                <option value="22">Commercial Vehicle</option>
                                <option value="23">Private vehicle</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Name</label>
                            <input type="text" value="{{old('name')}}" name="name" class="form-control" placeholder="Enter Manufacture Category Name" required>
                        </div>
                        <div class="form-group">
                            <label for="title">Manufacture Category Code</label>
                            <input type="text" value="{{old('code')}}" name="code" class="form-control" placeholder="Enter Manufacture Category Code">
                        </div>

                        <div style="clear: both">
                            <br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

