@section('header_css')
    <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">
    <style>
        .rates {
            float: left;
            height: 46px;
            padding: 0 10px;
        }
        .rates:not(:checked) > input {
            position:absolute;
            top:-9999px;
        }
        .rates:not(:checked) > label {
            float:right;
            width:1em;
            overflow:hidden;
            white-space:nowrap;
            cursor:pointer;
            font-size:38px;
            color:#ccc;
        }
        .rates:not(:checked) > label:before {
            content: '★ ';
        }
        .rates > input:checked ~ label {
            color: #f79633;
        }
        .rates:not(:checked) > label:hover,
        .rates:not(:checked) > label:hover ~ label {
            color: #f79633;
        }
        .rates > input:checked + label:hover,
        .rates > input:checked + label:hover ~ label,
        .rates > input:checked ~ label:hover,
        .rates > input:checked ~ label:hover ~ label,
        .rates > label:hover ~ input:checked ~ label {
            color: #f79633;
        }
        .img-responsive{
            max-width: 100%;
        }
        .gallery-img{
            height: 150px;
            width: 150px;
        }
    </style>
@endsection
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4>Edit Team</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{route('motor.manufacture.company.list.update',$manu->id)}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="title">Category</label>
                    <select class="form-control" name="category" required>
                        <option @if($manu-> category == 21) selected @endif value="21">Motorcycle</option>
                        <option @if($manu-> category == 22) selected @endif value="22">Commercial Vehicle</option>
                        <option @if($manu-> category == 23) selected @endif value="23">Private vehicle</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Name</label>
                    <input type="text" value="{{$manu->name ?? ''}}" name="name" class="form-control" placeholder="Enter Manufacture Category Name" required>
                </div>
                <div class="form-group">
                    <label for="title">Manufacture Category Code</label>
                    <input type="text" value="{{$manu->code ?? ''}}" name="code" class="form-control" placeholder="Enter Manufacture Category Code">
                </div>

                <div style="clear: both">
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
