@extends('layouts.backend.containerlist')

@section('title')
    Nonlife Manufacture
@endsection
@section('footer_js')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var oTable = $('#manuTable').dataTable();
        $('#tablebody').on('click', '.delete-item', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {

                    $.ajax({
                        type: "post",
                        url: "{{ url('/motor/manufacture/company/manufacture/delete/') }}" + "/" + id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            oTable.fnDeleteRow(nRow);
                            new swal('success', response.message, 'success').catch(swal.noop);
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error').catch(swal.noop);
                        }
                    });
                }
            }).catch(swal.noop);
        });

        $('#tablebody').on('click', '.change-status', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You are going to change the status!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('/motor/manufacture/company/manufacture/status/') }}" + "/" + id,
                        data: {
                            'id': id,
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            new swal('Success', response.message, 'success');
                            if (response.manufacture.status == 1) {
                                $($object).children().removeClass('bg-red').html(
                                    'InActive');
                                $($object).children().addClass('bg-blue').html(
                                    'Active');
                                $($object).attr('title', 'Deactivate');
                            } else {
                                $($object).children().removeClass('bg-blue').html(
                                    'Active');
                                $($object).children().addClass('bg-red').html(
                                    'InActive');
                                $($object).attr('title', 'Activate');
                            }
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });
    </script>
@endsection


@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')
            <div class="justify-content-end list-group list-group-horizontal ">
                <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#addModal">
                    Add Manufacture
                </button>
                @include('Backend.NonLife.Manufactures.add')
            </div>
            <br>
            <div class="blank-page">
                <table class="table" id="manuTable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Category</th>
                        <th scope="col">Name</th>
                        <th scope="col">Code</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($manufactures as $manu)
                        <tr class="row1" data-id="{{ $manu->id }}">

                            <th scope="row">{{$loop->iteration}}</th>
                            <td>
                                @if($manu['category'] == 21) MotorCycle @elseif($manu['category'] == 22) Commercial Vehicle @elseif($manu['category'] == 23) Private Vehicle @endif
                            </td>
                            <td>{{$manu['name']}}</td>
                            <td>{{$manu['code']}}</td>
                            <td><a href="javascript:;" class="change-status ml-3"
                                   title="{{ $manu->status == 0 ? "Click to activate Category." : "Click to De-active Category." }}"
                                   id="{{ $manu->id }}">
                                    @if($manu->status == 1)
                                        <button type="button"
                                                class="btn bg-blue btn-primary btn-flat ">
                                            Active
                                        </button>
                                    @else
                                        <button type="button"
                                                class="btn bg-red btn-primary btn-flat">
                                            InActive
                                        </button>
                                    @endif
                                </a>
                            </td>
                            <td class="justify-content-center">
                                <button data-toggle="modal" data-target="#editModal{{$manu['id']}}"
                                        class="btn btn-primary btn-flat">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="javascript:;" title="Delete" class="delete-item" id="{{ $manu->id }}">
                                    <button type="button" class="btn btn-danger btn-flat">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>
                            </td>
                            <div class="modal fade" id="editModal{{$manu['id']}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel"
                                 aria-hidden="true" style="display: none;">
                                @include('Backend.NonLife.Manufactures.edit')
                            </div>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

