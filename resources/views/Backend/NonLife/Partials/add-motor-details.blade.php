<div class="modal fade" id="AddMotorDetails" tabindex="-1" role="dialog" aria-labelledby="AddMotorDetailsLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AddMotorDetailsLabel">Add Motor Details</h5>
                <button type="button" class="close clsDtls" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="calc-from" id="motorCalculationForm" action="{{ route('nonLife.calculator.update.motor.info') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="VEHICLENO">Vehicle No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Vehicle No.</div>
                                </div>
                                <input type="text" class="form-control text-uppercase" id="VEHICLENO"
                                       placeholder="Please enter Vehicle Number"
                                       name="VEHICLENO" required
                                       value="{{ $formData['VEHICLENO']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="ENGINENO">Engine No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Engine No.</div>
                                </div>
                                <input type="text" class="form-control text-uppercase" id="ENGINENO"
                                       placeholder="Please enter Engine Number"
                                       name="ENGINENO" required
                                       value="{{ $formData['ENGINENO']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="CHASISNO">Chasis No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Chasis No.</div>
                                </div>
                                <input type="text" class="form-control text-uppercase" id="CHASISNO"
                                       placeholder="Please enter Chasis Number"
                                       name="CHASISNO" required
                                       value="{{ $formData['CHASISNO']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="MODEUSE">Mode Of Use</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Mode Of Use</div>
                                </div>
                                <input type="text" class="form-control" id="MODEUSE"
                                       placeholder="Please enter Mode Of Use"
                                       name="MODEUSE" required
                                       value="{{ $formData['MODEUSE']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="MAKEVEHICLEID">Manufacturer Company</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Manufacturer Company</div>
                                </div>
                                <select class="form-control"
                                        id="MAKEVEHICLEID"
                                        name="MAKEVEHICLEID" required>
{{--                                    <option selected disabled>Select the Manufacturer</option>--}}
                                    @foreach($manufacturers->data as $manu)
                                        <option value="{{$manu->ID}}" {{$formData['MAKEVEHICLEID'] == $manu->ID ? "selected" : ''}}>{{$manu->ENGNAME}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="MANUFACTURE" id="manufactureName">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="MAKEMODELID">Model</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Model</div>
                                </div>
                                <select class="form-control"
                                        id="MAKEMODELID"
                                        name="MAKEMODELID" required>
{{--                                    <option selected disabled>Select Manufacture to load the Model</option>--}}
                                    @foreach($makeModels->data as $model)
                                        <option value="{{$model->ID}}" {{$formData['MAKEMODELID'] == $model->ID ? "selected" : ''}}>{{$model->ENGNAME}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="MODEL" id="modelName">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="VEHICLENAMEID">formation</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">formation</div>
                                </div>
                                <select class="form-control"
                                        id="VEHICLENAMEID"
                                        name="VEHICLENAMEID" required>
                                    <option selected disabled>Select the formation</option>
                                    @foreach($vehicleNames->data as $vcl)
                                        <option value="{{$vcl->ID}}" {{$formData['VEHICLENAMEID'] == $vcl->ID ? "selected" : ''}}>{{$vcl->ENGNAME}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="FORMATION" id="formationName">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="occupation">Occupation</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Occupation</div>
                                </div>
                                <select class="form-control"
                                        id="BUSSOCCPCODE"
                                        name="BUSSOCCPCODE" required>
                                    <option selected disabled>Select the Occupation</option>
                                    @foreach($occupationLists->data as $occ)
                                        <option value="{{$occ->BUSSOCCPCODE}}" {{$formData['BUSSOCCPCODE'] == $occ->BUSSOCCPCODE ? "selected" : ''}}>{{$occ->DESCRIPTION}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-12" id="editDateDisplay"></div>
                        <div class="form-group col-md-12">
                            <label class="sr-only" for="RegistrationDate">Registration Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Registration Date</div>
                                </div>
                                <input type="text" class="form-control edit-datepicker" name="NEPALIREGISTRATIONDATE"
                                       placeholder="Select Nepali Date" required>
                                <input class="form-control" type="text" id="edit-english-date" name="REGISTRATIONDATE"
                                       placeholder="Shows English Date" value="{{ $formData['REGISTRATIONDATE']??'' }}" readonly/>
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
{{--                            <small class="help-block">* File Must Be In Image Format. Insert Image of size less than 2 MB</small>--}}
                            <div class="custom-file">
{{--                                <input type="file" class="custom-file-input" name="bluebook_images" id="bluebook_image">--}}
                                <label class="custom-file-labels" for="bluebook_images">Browse Blue Book Image</label>
                                <span class="custom-file-image" for="images_view">
                                    <img style="max-width: 50%; margin-top: 10px;" src="{{ $formData['bluebook_image']??'' }}">
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
{{--                            <small class="help-block">* File Must Be In Image Format. Insert Image of size less than 2 MB</small>--}}
                            <div class="custom-file">
{{--                                <input type="file" class="custom-file-input" name="bike_images" id="bike_image">--}}
                                <label class="custom-file-labels" for="bike_images">Browse Motor Image</label>
                                <span class="custom-file-image" for="images_view">
                                    <img style="max-width: 50%; margin-top: 10px;" src="{{ $formData['bike_image']??'' }}">
                                </span>
                            </div>
                        </div>
{{--                        <div class="form-group col-md-12">--}}
{{--                            <button type="submit" id="updateMotorInfo" class="btn btn-primary">Update Motor Info</button>--}}
{{--                            <button type="button" data-dismiss="modal" aria-label="close" class="btn btn-secondary clsDtls">Close--}}
{{--                            </button>--}}
{{--                        </div>--}}
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary clsDtls" data-dismiss="modal">Close</button>
                <button type="button" id="updateMotorInfo" class="btn btn-primary">Update changes</button>
            </div>
        </div>
    </div>
</div>
