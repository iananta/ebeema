<div class="modal fade" id="AddCustomer" tabindex="-1" role="dialog" aria-labelledby="AddCustomerLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl select-cust-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AddCustomerLabel">Add New Customer</h5>
                <button type="button" class="close closeMe" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="calc-from" id="calculationForm" action="{{route('user.kyc.entry')}}"
                      method="POST">
                    @csrf
                    <input type="hidden" name="is_ajax" value="1">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="sr-only" for="CATEGORYID">Category *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Category *</div>
                                </div>
                                <select class="form-control" id="categoryListId" name="CATEGORYID"
                                        required>
                                    <option selected disabled>Select the Category</option>
                                    @foreach($kycCategories as $cat)
                                        <option
                                            value="{{$cat->ID}}">{{$cat->GROUPNAME ?? $cat->GroupName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="inlineFormInputGroupUsername2">Insured
                                Type</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Insured Type *</div>
                                </div>
                                <select class="form-control" id="INSUREDTYPEID" name="INSUREDTYPE"
                                        required>
                                    <option selected disabled>Select the Insured Type *</option>
                                    @foreach($insuredTypes as $insType)
                                        <option
                                            value="{{$insType->INSUREDTYPEID}}">{{$insType->INSUREDTYPE}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="inlineFormInputName2">KYC Calssification</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">KYC Calssification</div>
                                </div>
                                <select class="form-control" id="kycclassificationID"
                                        name="kycclassification" required>
                                    <option selected disabled>Select Kyc Classification</option>
                                    @foreach($kycClassifications as $kycCls)
                                        <option
                                            value="{{$kycCls->ID}}">{{$kycCls->CLASSIFICATIONNAME}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="riskcat">KYC Risk Category *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">KYC Risk Category *</div>
                                </div>
                                <select class="form-control" id="KYCRiskCategory" name="KYCRiskCategory"
                                        required>
                                    <option selected disabled>Select Risk Category</option>
                                    @foreach($riskCategories as $rskcat)
                                        @if (!$loop->first)
                                        <option value="{{$rskcat->ID}}">{{$rskcat->DESCRIPTION ?? $rskcat->Description}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="CC">Full Name *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Full Name *</div>
                                </div>
                                <input type="text" class="form-control" id="TITLE"
                                       placeholder="Enter Name in Full Name"
                                       name="INSUREDNAME_ENG"
                                       value="" required>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="CC">Full Name (Nepali) *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Full Name (Nepali) *</div>
                                </div>
                                <input type="text" class="form-control" id="INSUREDNAME_NEP"
                                       placeholder="Enter Name in Nepali"
                                       name="INSUREDNAME_NEP"
                                       value="" required>
                            </div>
                            <small class="form-text text-muted"><a class="btn btn-primary btn-sm"
                                    href="https://www.ashesh.com.np/nepali-unicode.php" target="_blank">Nepali Unicode</a> [Please use this link to convert to Nepali text]</small>
                        </div>

                        <div class="form-group col-md-3">
                            <label class="sr-only" for="Province">Province *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Province *</div>
                                </div>
                                <select class="form-control" id="provinceId" name="ZONEID" required>
                                    <option selected disabled>Select Province</option>
                                    @foreach($provinces as $province)
                                        <option
                                            value="{{$province->PROVINCECODE}}">{{$province->EPROVINCE}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="sr-only" for="Province">District *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">District *</div>
                                </div>
                                <select class="form-control" id="districtId" name="DISTRICTID" required>
                                    <option selected disabled>Select District</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label class="sr-only" for="mnu">MNU</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">MNU</div>
                                </div>
                                <select class="form-control" id="MNUId" name="MUNICIPALITYCODE"
                                        required>
                                    <option selected disabled>Select MNU</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="sr-only" for="vdc">VDC</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">VDC</div>
                                </div>
                                <select class="form-control" id="VDCId" name="VDCCODE" required>
                                    <option selected disabled>Select VDC</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="address">Address *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Address *</div>
                                </div>
                                <input type="text" class="form-control" id="ADDRESS"
                                       placeholder="Eg: Bagmati Pradesh, Kathmandu- jadibuti - 32"
                                       name="ADDRESS"
                                       value="" required>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="inlineFormInputName2">Address (Nepali) *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Address (Nepali) *</div>
                                </div>
                                <input type="text" class="form-control" id="ADDRESSNEPALI"
                                       placeholder="Please enter address in nepali."
                                       name="ADDRESSNEPALI"
                                       value="" required>
                            </div>
                            <small class="form-text text-muted"><a class="btn btn-primary btn-sm"
                                                                   href="https://www.ashesh.com.np/nepali-unicode.php" target="_blank">Nepali Unicode</a> [Please use this link to convert to Nepali text]</small>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="wardno">Ward No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Ward No.</div>
                                </div>
                                <input type="number" class="form-control" id="WARDNO"
                                       placeholder="Please enter Ward no." name="WARDNO" min="0"
                                       value="" required">
                            </div>
                        </div>

                        <div class="form-group col-md-3">
                            <label class="sr-only" for="houseno">House No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">House No.</div>
                                </div>
                                <input type="number" class="form-control" id="houseno"
                                       placeholder="Please enter house No." name="HOUSENO" min="0"
                                       value="">
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="sr-only" for="plotno">Plot No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Plot No.</div>
                                </div>
                                <input type="number" class="form-control" id="plotno"
                                       placeholder="Please enter plot no." min="0"
                                       value="" name="PLOTNO">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="address">Temp. Address </label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Temp. Address</div>
                                </div>
                                <input type="text" class="form-control"
                                       placeholder="Please enter temporary address."
                                       value=""
                                       name="TEMPORARYADDRESS">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="inlineFormInputName2">Temp. Address
                                (Nepali)</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Temp. Address (Nepali)</div>
                                </div>
                                <input type="text" class="form-control"
                                       placeholder="Please enter temporary address in nepali."
                                       value=""
                                       name="NTEMPORARYADDRESS">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="HomeTelNo">Home Tel. No.</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Home Tel. No.</div>
                                </div>
                                <input type="tel" class="form-control" id="telno"
                                       placeholder="Please enter Telephone Number." name="HOMETELNO"
                                       value="">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="mbno">Mobile No. *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Mobile No. *</div>
                                </div>
                                <input type="tel" class="form-control" id="mbno"
                                       placeholder="Please enter Mobile Number." name="MOBILENO"
                                       value="">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="plotno">Email Address</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Email Address</div>
                                </div>
                                <input type="text" class="form-control" id="email"
                                       placeholder="Please enter email address."
                                       value="" name="EMAIL">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="occupation">Occupation</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Occupation</div>
                                </div>
                                <select class="form-control" id="occupation" name="OCCUPATION" required>
                                    <option selected disabled>Select Occupation</option>

                                    @foreach($kycOccupations as $occ)
                                        <option
                                            value="{{$occ->BUSSOCCPCODE}}">{{$occ->DESCRIPTION}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="INCOMESOURCE">Income Source</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Income Source</div>
                                </div>
                                <select class="form-control" id="INCOMESOURCE" name="INCOMESOURCE"
                                        required>
                                    <option selected disabled>Select Income Source</option>
                                    @foreach($kycincomesources as $incSrc)
                                        <option
                                            value="{{$incSrc->ID}}">{{$incSrc->EDESC}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="incomesrc">Pan Number</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Pan Number</div>
                                </div>
                                <input type="number" class="form-control" id="panno"
                                       placeholder="Please enter Pan Number." min="0"
                                       value="" name="PANNO">
                            </div>
                        </div>
                        <div class="form-group col-md-12" id="issuedDateDisplay"></div>
                        <div class="form-group col-md-12" id="dateDisplay"></div>

                        <div class="form-group col-md-12">
                            <p class="font-weight-bold">In case of insured type : individual</p>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sr-only" for="gender">Gender *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Gender *</div>
                                </div>
                                <select class="form-control" id="gender" name="GENDER" required>
                                    <option selected disabled>Select Gender</option>
                                    <option
                                        value="Male">
                                        Male
                                    </option>
                                    <option
                                        value="Female">
                                        Female
                                    </option>
                                    <option
                                        value="Others">
                                        Others
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sr-only" for="MARITALSTATUS">Marital Status</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Marital Status</div>
                                </div>
                                <select class="form-control" id="MARITALSTATUS" name="MARITALSTATUS"
                                        required>
                                    <option selected disabled>Select Marital Status</option>
                                    <option
                                        value="0">
                                        Unmarried
                                    </option>
                                    <option
                                        value="1">
                                        Married
                                    </option>
                                    <option
                                        value="2">
                                        Divorced
                                    </option>
                                    <option value="4">
                                        Widow
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sr-only" for="gender">Date Of Birth *</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Date Of Birth *</div>
                                </div>
                                <input type="text" class="form-control dob-datepicker"
                                       name="NEPALIDATEOFBIRTH" placeholder="Select Nepali Date">
                                <span style="margin-left: -25px;padding: 5px;z-index:999;"
                                      class="fa fa-calendar mt-2"></span>
                                <input class="form-control" type="text" id="dob-english-date"
                                       name="DATEOFBIRTH" placeholder="Shows English Date" readonly/>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sr-only" for="CITIZENSHIPNO">Citizenship no. *</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Citizenship no. *</div>
                                </div>
                                <input type="text" class="form-control" id="ctznno"
                                       placeholder="Please enter citizenship no."
                                       value=""
                                       name="CITIZENSHIPNO">
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sr-only" for="issdis">Issued District</label>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Issued District</div>
                                </div>
                                <select class="form-control" id="ISSUE_DISTRICT_ID_LIST"
                                        name="ISSUE_DISTRICT_ID"
                                        required>
                                    <option selected disabled>Select Issued District</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="sr-only" for="issdate">Issued Date</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Issued Date</div>
                                </div>
                                <input type="text" class="form-control issued-datepicker"
                                       name="NEPALIISSUEDATE" placeholder="Select Nepali Date">
                                <span style="margin-left: -25px;padding: 5px;z-index:999;"
                                      class="fa fa-calendar mt-2"></span>
                                <input class="form-control" type="text" id="issued-date"
                                       name="ISSUEDATE" placeholder="Shows English Date" readonly/>
                                {{-- <input class="form-control datepicker" data-date-format="yyyy-mm-dd"
                                         name="ISSUEDATE"
                                         data-provide="datepicker"
                                         value="">
                                <span style="margin-left: -20px; z-index:999;"
                                      class="fa fa-calendar mt-2"></span> --}}
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="fathername">Father Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Father Name</div>
                                </div>
                                <input type="text" class="form-control" id="FATHERNAME"
                                       placeholder="Please enter fathers name."
                                       value="" name="FATHERNAME">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="mothername">Mother Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Mother Name</div>
                                </div>
                                <input type="text" class="form-control" id="MOTHERNAME"
                                       placeholder="Please enter mothers name."
                                       value="" name="MOTHERNAME">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="grandfather">Grand Father Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Grand Father Name</div>
                                </div>
                                <input type="text" class="form-control" id="GRANDFATHERNAME"
                                       placeholder="Please enter Grand fathers name."
                                       value=""
                                       name="GRANDFATHERNAME">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="grandmother">Grand Mother Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Grand Mother Name</div>
                                </div>
                                <input type="text" class="form-control" id="GRANDMOTHERNAME"
                                       placeholder="Please enter Grand mothers name."
                                       value=""
                                       name="GRANDMOTHERNAME">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="husband">Husband Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Husband Name</div>
                                </div>
                                <input type="text" class="form-control" id="HUSBANDNAME"
                                       placeholder="Please enter your husband name."
                                       value="" name="HUSBANDNAME">
                                <input type="hidden" class="form-control" name="NFATHERNAME"
                                       value="usersfather">
                                <input type="hidden" class="form-control" name="NGRANDFATHERNAME"
                                       value="usergrandfather">
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="sr-only" for="wife">Wife Name</label>
                            <div class="input-group">
                                <div class="input-group-prepend date">
                                    <div class="input-group-text">Wife Name</div>
                                </div>
                                <input type="text" class="form-control" id="WIFENAME"
                                       placeholder="Please enter your wife name."
                                       value="" name="WIFENAME">
                            </div>
                        </div>

                        <div class="form-group col-md-12 border-attachment">
                            <span class="make-border-text">Attachments</span>
                            <div class="row">
                                <div class="form-group col-md-6 images-part">
                                    <small class="help-block">* Photo Must Be In Image Format. Insert
                                        Image of size less than 2 MB</small>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input photoInput" name="photos"
                                               id="photos">
                                        <label class="custom-file-label" for="photos">Browse
                                            Photo</label>
                                        <span class="custom-file-image" for="images_view"></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 images-part">
                                    <small class="help-block"> Citizenship (front) image Must Be In
                                        Image Format. Insert
                                        Image of size less than 2 MB</small>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input citFrntImg" name="citfrnt"
                                               id="citfront">
                                        <label class="custom-file-label" for="citfrnt">Browse
                                            Citizenship
                                            (Front)</label>
                                        <span class="custom-file-image" for="images_view"></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 images-part">
                                    <small class="help-block"> Citizenship (back) image Must Be In
                                        Image Format. Insert
                                        Image of size less than 2 MB</small>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input citBackImg" name="citback"
                                               id="citback">
                                        <label class="custom-file-label" for="ctbck">Browse Citizenship
                                            (back)</label>
                                        <span class="custom-file-image" for="images_view"></span>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 images-part cpmreg">
                                    <small class="help-block"> Registration / Certificate image Must Be
                                        In Image Format. Insert
                                        Image of size less than 2 MB</small>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input panImg" name="cpmreg"
                                               id="cpmreg">
                                        <label class="custom-file-label" for="cpmreg">Browse Company
                                            Registration & PAN
                                            Certificate</label>
                                        <span class="custom-file-image" for="images_view"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="BRANCHCODE" value="01">
                        <input type="hidden" name="BRANCHID" value="1">
                        <input type="hidden" name="ACCOUNTNAMECODE">
                        <input type="hidden" name="AREAID" value="0">
                        <input type="hidden" name="TOLEID" value="0">
                        <div class="chgValNull">
                            <input type="hidden" name="KYCNO">
                            <input type="hidden" name="customer_id" value="">
                            <input type="hidden" name="kyc_id" value="">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="makeModalClose" class="btn btn-secondary closeMe"
                        data-dismiss="modal">Close
                </button>
                <button type="button" id="AddKycCustomer" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
