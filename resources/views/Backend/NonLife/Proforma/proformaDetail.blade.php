@extends('layouts.backend.containerlist')

@section('title')
Motor Document List
@endsection
@section('footer_js')
<!-- formValidation -->
<script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
<script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#policy-table').dataTable({
            "ordering": false,
            "scrollX": true
        });
    });
</script>
@endsection
@section('dynamicdata')

<div class="box">
    <div class="text-right mr-3">
        <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.proforma.payment')}}">Proforma Detail</a>
    </div>
    <div class="box-body">
        <table id="-table" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>proformaNo</th>
                <th>Insured</th>
                <th>Class Name</th>
                <th>Vehicle No.</th>
                <th>Sum Insured</th>
                <th>Subsidized Premium</th>
                <th>Total Premium</th>
            </tr>
            </thead>
            <tbody id="tablebody">
            @foreach($data as $index=>$proforma)
            <tr class="gradeX">
                <td>{{ $proforma['proformaNo'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['insured'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['className'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['VehicleNo'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['sumInsured'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['subsidizedPremium'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['totalPremium'] }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@stop
