@extends('layouts.backend.containerlist')

@section('title')
Motor Document List
@endsection
@section('footer_js')
<!-- formValidation -->
<script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
<script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#policy-table').dataTable({
            "ordering": false,
            "scrollX": true
        });
    });
</script>
@endsection
@section('dynamicdata')

<div class="box">
    <div class="box-body">
        <table id="-table" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>proformaNo</th>
                <th>Payment ID</th>
            </tr>
            </thead>
            <tbody id="tablebody">
            @foreach($data as $index=>$proforma)
            <tr class="gradeX">
                <td>{{ $proforma['proformaNo'] }}</td>
            </tr>
            <tr class="gradeX">
                <td>{{ $proforma['insured'] }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@stop
