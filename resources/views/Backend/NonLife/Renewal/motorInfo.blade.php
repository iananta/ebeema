@extends('layouts.backend.containerlist')

@section('title')
    Motor Info
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.motor.list')}}">Go Back</a>
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <table class="table table-bordered table-hover">
                <tbody id="tablebody">
                    @foreach($data as $index=>$motorInfo)
                        <tr class="gradeX mb-3 mt-3">
                            <th>Category Name</th>
                            <td>{{$motorInfo['CATEGORYNAME']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Category ID</th>
                            <td>{{$motorInfo['CATEGORYID']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Type Cover</th>
                            <td>{{$motorInfo['TYPECOVER']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Business Occupation Code</th>
                            <td>{{$motorInfo['BUSSOCCPCODE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Occupation</th>
                            <td>{{$motorInfo['OCCUPATION']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Mode Use</th>
                            <td>{{$motorInfo['MODEUSE']}}</td>
                        </tr><tr class="gradeX mb-3 mt-3">
                            <th>Make Vehicle</th>
                            <td>{{$motorInfo['MAKEVEHICLE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Make Vehicle ID</th>
                            <td>{{$motorInfo['MAKEVEHICLEID']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Model ID</th>
                            <td>{{$motorInfo['MAKEMODELID']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Model</th>
                            <td>{{$motorInfo['MODEL']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Vehicle Name</th>
                            <td>{{$motorInfo['NAMEOFVEHICLE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Vehicle Name ID</th>
                            <td>{{$motorInfo['VEHICLENAMEID']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Year Manufacture</th>
                            <td>{{$motorInfo['YEARMANUFACTURE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>CCHP</th>
                            <td>{{$motorInfo['CCHP']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Pass Capacity</th>
                            <td>{{$motorInfo['PASSCAPACITY']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Carry Capacity</th>
                            <td>{{$motorInfo['CARRYCAPACITY']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Registration Date</th>
                            <td>{{$motorInfo['REGDATE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Sum Insured Amount</th>
                            <td>{{$motorInfo['SUMINSUREDAMOUNT']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Other Cost</th>
                            <td>{{$motorInfo['OTHERCOST']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Vehicle No.</th>
                            <td>{{$motorInfo['VEHICLENO']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Running Vehicle No.</th>
                            <td>{{$motorInfo['RUNNINGVEHICLENO']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>E-Vehicle No.</th>
                            <td>{{$motorInfo['EVEHICLENO']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>E-Running Vehicle No.</th>
                            <td>{{$motorInfo['ERUNNINGVEHICLENO']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Engine No.</th>
                            <td>{{$motorInfo['ENGINENO']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Chasis No.</th>
                            <td>{{$motorInfo['CHASISNO']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>EODAMT</th>
                            <td>{{$motorInfo['EODAMT']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>PA Driver</th>
                            <td>{{$motorInfo['PADRIVER']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>No. of Employee</th>
                            <td>{{$motorInfo['NOOFEMPLOYEE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>PA Conductor</th>
                            <td>{{$motorInfo['PACONDUCTOR']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>No. of Passanger</th>
                            <td>{{$motorInfo['NOOFPASSENGER']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>PA Passenger</th>
                            <td>{{$motorInfo['PAPASSENGER']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>EST Cost</th>
                            <td>{{$motorInfo['ESTCOST']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Other Sum Insured</th>
                            <td>{{$motorInfo['OTHERSUMINSURED']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Private Use</th>
                            <td>{{$motorInfo['PRIVATE_USE']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Exclude Pool</th>
                            <td>{{$motorInfo['EXCLUDE_POOL']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Include Towing</th>
                            <td>{{$motorInfo['INCLUDE_TOWING']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Has Trailor</th>
                            <td>{{$motorInfo['HAS_TRAILOR']}}</td>
                        </tr>
                        <tr class="gradeX mb-3 mt-3">
                            <th>Vehicle Age</th>
                            <td>{{$motorInfo['VEHICLEAGE']}}</td>
                        </tr><tr class="gradeX mb-3 mt-3">
                            <th>NCD Year</th>
                            <td>{{$motorInfo['NCDYEAR']}}</td>
                        </tr>

                    @endforeach
                </tbody>


            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
