@extends('layouts.backend.containerlist')

@section('title')
    Motor Document List
@endsection
@section('footer_js')
<!-- formValidation -->
<script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
<script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('#policy-table').dataTable({
            "ordering": false,
            "scrollX": true
        });
    });
</script>
@endsection
@section('dynamicdata')

<div class="box">
    <div class="box-body">
        <table id="-table" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Document No.</th>
                <th>Insured Name</th>
                <th>Address</th>
                <th>Mobile No.</th>
                <th>E-Vehicle No.</th>
                <th>Issued Date</th>
                <th>Effective Date</th>
                <th>Expiry Date</th>
                <th>Remaining Days</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="tablebody">
            @foreach($data as $index=>$motorClaim)
            <tr class="gradeX">
                <td>{{ $motorClaim['Documentno'] }}</td>
                <td>{{ $motorClaim['InsuredName'] }}</td>
                <td>{{ $motorClaim['Address'] }}</td>
                <td>{{ $motorClaim['MobileNo'] }}</td>
                <td>{{ $motorClaim['EVehicleNo'] }}</td>
                <td>{{ $motorClaim['IssuedDate'] }}</td>
                <td>{{ $motorClaim['Effective_Date'] }}</td>
                <td>{{ $motorClaim['ExpiryDate'] }}</td>
                <td>{{ $motorClaim['RemainingDays'] }}</td>
                <td>
                    <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.motor.info')}}">Info</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@stop
