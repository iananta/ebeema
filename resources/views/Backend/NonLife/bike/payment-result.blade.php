@extends('layouts.backend.containerform')

@section('title')
    Non Life Motor Calculation Payment {{ isset($pieces) ? 'cancelled' : 'Recheck'}}
@endsection
@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection

@section('footer_js')
{{--    <script>--}}

{{--        $(document).ready(function () {--}}
{{--            $('.search-select').select2();--}}
{{--            $("#makePolicySbtn").submit();--}}
{{--        });--}}
{{--    </script>--}}
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="result-show container">
            <h3>Payment {{ isset($pieces) ? 'cancelled' : 'Recheck'}} - {{Session::get('calculationFor') ? Session::get('calculationFor') : 'Motor'}} Premium</h3>
            <br>
            @include('layouts.backend.alert')
            <div class="row">
                <div class="col-sm-3"></div>
                <div style="text-align:center;border: 1px solid #000000;padding: 50px 0" class="col-sm-6">
                    <div>
{{--                        <p><span class="badge badge-pill badge-danger p-2">{{json_encode($paymentInfo)}}</span></p>--}}
                        <p class="text-bold">Merchant Payed: <span class="text-blue"> {{$merchantName}}</span></p>
                        <p class="text-bold">Your Reference Id : <span class="text-blue"> {{$paymentInfo->RefId ?? ''}}</span></p>
                        <p class="text-bold">Your Amount : <span class="text-blue"> Rs. {{number_format($draftPolicy->TOTALNETPREMIUM, 2, '.','')}}</span></p>
                    </div>
                    <h3 class="text-bold">Payment From : <span class="text-blue"> {{ucfirst($draftPolicy->payment_method)}}</span></h3>
                    <h3 class="text-bold">Payment Status : <span class="text-blue"> {{isset($pieces) ? $pieces[1] : $paymentInfo->ResponseDescription}}</span></h3>
                    <br>
                    <hr>

                    @if(isset($makePolicy))
                        <form id="makePolicySbtn" action="{{route('nonLife.calculator.motor.make.policy')}}" method="get"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="merchant_name" value="{{$merchantName}}">
                            <input type="hidden" name="paymnet_ref_id" value="{{isset($pieces) ? $pieces[4] : ''}}">
                            <button class="btn btn-primary" type="submit">Make Policy</button>
                        </form>
                    @endif
                    <a href="{{route('nonLife.calculator.draft.policy.view')}}" class="btn btn-primary fa fa-arrow-left mt-4"> Draft Policies</a>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
