@extends('layouts.backend.containerform')

@section('title')
    Non Life Motor Calculation Review
@endsection
@section('footer_js')
    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
    <script>
        $('.changeRefId').on('click', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "Are you sure to change the payment reference code! <br> Payment or error might have occured on this one.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if(response.isConfirmed==true) {
                    $.ajax({
                        type: "GET",
                        url: "?regenerate=1&&customer_id=" + id,
                        dataType: 'json',
                        success: function (response) {
                            window.location = "?customer_id=" + id;
                        },
                        error: function (e) {
                            Swal.fire('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            });
        });
        $("#AddMotorDetail").on('click', function () {
            var cForm = $('#motorCalculationForm');
            var dateOfBirth = $('#edit-english-date').val();
            // console.log(dateOfBirth);
            $("#AddMotorDetails").modal("show");
            cForm.find('[name="NEPALIREGISTRATIONDATE"]').val(convDateBS(dateOfBirth));
            cForm.find('[name="MAKEMODELID"]').change();
        });
        $("#updateMotorInfo").on('click', function (e) {
            e.preventDefault();
            var frm = $('#motorCalculationForm');
            var formData = new FormData(frm[0]);
            // console.log(formData);
            $("#updateMotorInfo").prop('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{ route('nonLife.calculator.update.motor.info') }}",
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#preloader').show();
                },
                success: function (response) {
                    $('#preloader').hide();
                    $("#updateMotorInfo").prop('disabled', false);
                    // console.log(response.message);
                    if (response.data) {
                        $('#AddMotorDetails').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            html: response.message ?? "Motor Info updated Successfully",
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                        location.reload();
                        // Swal.fire("Congratulations!", response.message ?? "Customer Added Successfully", "success");
                    }
                    // console.log(response);
                },
                error: function (response) {
                    $('#preloader').hide();
                    $("#updateMotorInfo").prop('disabled', false);
                    var err = '';
                    if (response.responseJSON && response.responseJSON.errors) {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.errors)) {
                            err += '-' + value + '<br>';
                        }
                    } else {
                        var msg = response.responseJSON;
                        err = msg.message;
                    }
                    // console.log(err);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: typeof msg != 'object' && msg !== null ? msg.message : 'Error',
                        html: err ?? 'Something went wrong',
                        showConfirmButton: false,
                        timer: 2500,
                        timerProgressBar: true,
                    });
                }
            });
        });
        $("#MAKEVEHICLEID").on('change', function(){
            $("#manufactureName").val($("#MAKEVEHICLEID option:selected").text());
        });
        $("#MAKEMODELID").on('change', function(){
            $("#modelName").val($("#MAKEMODELID option:selected").text());
        });
        $("#VEHICLENAMEID").on('change', function(){
            $("#formationName").val($("#VEHICLENAMEID option:selected").text());
        });
        $('.clsDtls').one('click', function () {
            $('#AddMotorDetails').modal('hide');
        });
        $('#provinceId').on('change', function () {
            var provinceID = $(this).val();
            // console.log(provinceID);
            $.ajax({
                type: "POST",
                url: "{{ route('province.district') }}",
                data: {
                    provinceID: provinceID
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $('#districtId').empty().append($('<option>', {
                        text: "Select District",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#districtId').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION
                        }));
                    }

                    $('#ISSUE_DISTRICT_ID_LIST').empty().append($('<option>', {
                        text: "Select Issued District",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#ISSUE_DISTRICT_ID_LIST').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION
                        }));
                    }
                    if ($('#calculationForm').find('[name="ZONEID"]').val()) {
                        getSelectedDistrict();
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });

        });
        $('#districtId').on('change', function () {
            var DistrictID = $(this).val();
            $.ajax({
                type: "POST",
                url: "{{ route('district.mnuvdc') }}",
                data: {
                    DistrictID: DistrictID
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $('#MNUId').empty().append($('<option>', {
                        text: "Select Municipality ",
                        disabled: '',
                        selected: ''
                    }));
                    for (var i = 0; i < response.mnu.length; i++) {
                        $('#MNUId').append($('<option>', {
                            value: response.mnu[i].MNUCODE,
                            text: response.mnu[i].MNU
                        }));
                    }

                    $('#VDCId').empty().append($('<option>', {
                        text: "Select VDC ",
                        disabled: '',
                        selected: ''
                    }));
                    for (var j = 0; j < response.vdc.length; j++) {
                        $('#VDCId').append($('<option>', {
                            value: response.vdc[j].VDCCODE,
                            text: response.vdc[j].VDC
                        }));
                    }
                    if ($('#calculationForm').find('[name="DISTRICTID"]').val()) {
                        getSelectedMnuVdc();
                        // console.log('im on for mnuVdc');
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });

        });
        window.onload = function () {
            $('.dob-datepicker').nepaliDatePicker({
                container: '#dateDisplay',
                ndpYear: true,
                ndpMonth: true,
                ndpEnglishInput: 'dob-english-date'
            });
            $('.issued-datepicker').nepaliDatePicker({
                container: '#issuedDateDisplay',
                ndpYear: true,
                ndpMonth: true,
                ndpEnglishInput: 'issued-date'
            });
        };
        $(".dob-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#dob-english-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
        $(".issued-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#issued-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
        $('.edit-datepicker').nepaliDatePicker({
            container: '#editDateDisplay',
            ndpYear: true,
            ndpMonth: true,
            ndpEnglishInput: 'edit-english-date'
        });
        $(".edit-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#edit-english-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
        $(".closeMe").click(function () {
            $("#AddCustomer").modal("hide");
        });
        $("#EditCustomerDetails").on('click', function () {
            var selectedCustomer = <?php echo $customers ?>;
            var cForm = $('#calculationForm');
            $("#AddCustomer").modal("show");
            $('input[name=INSUREDNAME_ENG]').val(selectedCustomer.INSUREDNAME_ENG);
            cForm.find('[name="CATEGORYID"]').val(selectedCustomer.CATEGORYID);
            cForm.find('[name="INSUREDTYPE"]').val(selectedCustomer.INSUREDTYPE);
            cForm.find('[name="kycclassification"]').val(selectedCustomer.kycclassification);
            cForm.find('[name="KYCRiskCategory"]').val(selectedCustomer.KYCRiskCategory);
            cForm.find('[name="INSUREDNAME_NEP"]').val(selectedCustomer.INSUREDNAME_NEP);
            cForm.find('[name="ZONEID"]').val(selectedCustomer.ZONEID);
            cForm.find('[name="ZONEID"]').change();
            cForm.find('[name="ADDRESS"]').val(selectedCustomer.ADDRESS);
            cForm.find('[name="ADDRESSNEPALI"]').val(selectedCustomer.ADDRESSNEPALI);
            cForm.find('[name="WARDNO"]').val(selectedCustomer.WARDNO);
            cForm.find('[name="HOUSENO"]').val(selectedCustomer.HOUSENO);
            cForm.find('[name="PLOTNO"]').val(selectedCustomer.PLOTNO);
            cForm.find('[name="TEMPORARYADDRESS"]').val(selectedCustomer.TEMPORARYADDRESS);
            cForm.find('[name="NTEMPORARYADDRESS"]').val(selectedCustomer.NTEMPORARYADDRESS);
            cForm.find('[name="HOMETELNO"]').val(selectedCustomer.HOMETELNO);
            cForm.find('[name="MOBILENO"]').val(selectedCustomer.MOBILENO);
            cForm.find('[name="EMAIL"]').val(selectedCustomer.EMAIL);
            cForm.find('[name="MOBILENO"]').val(selectedCustomer.MOBILENO);
            cForm.find('[name="OCCUPATION"]').val(selectedCustomer.OCCUPATION);
            cForm.find('[name="INCOMESOURCE"]').val(selectedCustomer.INCOMESOURCE);
            cForm.find('[name="PANNO"]').val(selectedCustomer.PANNO);
            cForm.find('[name="GENDER"]').val(selectedCustomer.GENDER);
            cForm.find('[name="MARITALSTATUS"]').val(selectedCustomer.MARITALSTATUS);
            cForm.find('[name="NEPALIDATEOFBIRTH"]').val(convDateBS(selectedCustomer.DATEOFBIRTH));
            cForm.find('[name="NEPALIISSUEDATE"]').val(convDateBS(selectedCustomer.ISSUEDATE));
            cForm.find('[name="DATEOFBIRTH"]').val(selectedCustomer.DATEOFBIRTH);
            cForm.find('[name="ISSUEDATE"]').val(selectedCustomer.ISSUEDATE);
            cForm.find('[name="CITIZENSHIPNO"]').val(selectedCustomer.CITIZENSHIPNO);
            cForm.find('[name="ISSUE_DISTRICT_ID"]').val(selectedCustomer.ISSUE_DISTRICT_ID);
            cForm.find('[name="FATHERNAME"]').val(selectedCustomer.FATHERNAME);
            cForm.find('[name="MOTHERNAME"]').val(selectedCustomer.MOTHERNAME);
            cForm.find('[name="GRANDFATHERNAME"]').val(selectedCustomer.GRANDFATHERNAME);
            cForm.find('[name="GRANDMOTHERNAME"]').val(selectedCustomer.GRANDMOTHERNAME);
            cForm.find('[name="HUSBANDNAME"]').val(selectedCustomer.HUSBANDNAME);
            cForm.find('[name="WIFENAME"]').val(selectedCustomer.WIFENAME);
            cForm.find('[name="KYCNO"]').val(selectedCustomer.KYCNO);
            cForm.find('[name="customer_id"]').val(selectedCustomer.customer_id);
            cForm.find('[name="kyc_id"]').val(selectedCustomer.id);
            if (selectedCustomer.photo) {
                $(".photoInput").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.photo + '">');
            }
            if (selectedCustomer.citfrntimg) {
                $(".citFrntImg").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.citfrntimg + '">');
            }
            if (selectedCustomer.citbackimg) {
                $(".citBackImg").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.citbackimg + '">');
            }
            if (selectedCustomer.cpmregimg) {
                $(".panImg").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.cpmregimg + '">');
            }
            // console.log(selectedCustomer);
        });

        $('#AddKycCustomer').on('click', function (e) {
            e.preventDefault();
            var frm = $('#calculationForm');
            var formData = new FormData(frm[0]);
            $("#AddKycCustomer").prop('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{ route('user.kyc.entry') }}",
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#preloader').show();
                },
                success: function (response) {
                    $('#preloader').hide();
                    $('#calculationForm')[0].reset();
                    $("#AddKycCustomer").prop('disabled', false);
                    // console.log(response.message);
                    if (response.data) {
                        $("#makeModalClose").trigger("click");
                        $('#CustomerList').append($('<option>', {
                            value: response.data.customer_id,
                            text: response.data.INSUREDNAME_ENG + ' : ' + response.data.MOBILENO,
                            selected: true,
                        }));
                        $('#CustomerList').find(":selected").data('details', response.data);
                        $('#printCustomerDetails').empty().append('<div class="col-sm-6"><ul id="custmrList"><li>Customer Name: {{$customers->INSUREDNAME_NEP}}</li><li>Address: {{$customers->ADDRESSNEPALI}} </li><li>Mobile Number: {{$customers->MOBILENO}} </li><li>Email: {{$customers->EMAIL}} </li></ul></div>');
                        $('.cst-detail').removeClass('d-none');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            html: response.message ?? "Customer Added Successfully",
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                        // swal("Congratulations!", response.message ?? "Customer Added Successfully", "success");
                    }
                    // console.log(response);
                },
                error: function (response) {
                    $('#preloader').hide();
                    $("#AddKycCustomer").prop('disabled', false);
                    var err = '';
                    if (response.responseJSON && response.responseJSON.errors) {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.errors)) {
                            err += '-' + value + '<br>';
                        }
                    } else {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.message)) {
                            err += '-' + value + '<br>';
                        }
                    }
                    // console.log(err);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: typeof msg != 'object' && msg !== null ? msg.message : 'Error',
                        html: err ?? 'Something went wrong',
                        showConfirmButton: false,
                        timer: 2500,
                        timerProgressBar: true,
                    });
                }
            });
        });

        function convDateBS(date) {
            var dobDt = NepaliFunctions.ConvertToDateObject(date, "YYYY-MM-DD");
            var convDate = NepaliFunctions.AD2BS(dobDt);
            return NepaliFunctions.ConvertDateFormat(convDate);
        }

        function getSelectedDistrict() {
            var selectedCustomer = <?php echo $customers ?>;
            var cForm = $('#calculationForm');
            // console.log(selectedCustomer);
            cForm.find('[name="DISTRICTID"]').val(selectedCustomer.DISTRICTID);
            cForm.find('[name="DISTRICTID"]').change();
            cForm.find('[name="ISSUE_DISTRICT_ID"]').val(selectedCustomer.ISSUE_DISTRICT_ID);
        }

        function getSelectedMnuVdc() {
            var selectedCustomer = <?php echo $customers ?>;
            var cForm = $('#calculationForm');
            // console.log(selectedCustomer);
            cForm.find('[name="MUNICIPALITYCODE"]').val(selectedCustomer.MUNICIPALITYCODE);
            cForm.find('[name="VDCCODE"]').val(selectedCustomer.VDCCODE);
        }
    </script>
@endsection
@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection

@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="result-show m-5 p-5">
            <a class="btn btn-primary btn-sm debit-note"
               {{--target="_blank"--}} href="{{route('nonLife.calculator.motor.make.debit.note',['motorId'=> Session::get('motorCalcId') ?? ''])}}">Download
                Debit Note</a>
            <h3>Review / Payment Procedure - {{Session::get('calculationFor') ? Session::get('calculationFor') : 'motor'}}
                Premium</h3>
            <br>
            <div class="alert alert-warning alert-dismissible">
                <button class="close close-sm" data-dismiss="alert" type="button" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4>
                    <i class="icon fa fa-warning">
                    </i>
                    Important Notice!
                </h4>
                Please verify your details before making payments !
            </div>
            @include('layouts.backend.alert')
            <div class="row">
                <div class="col-sm-8">
                    <div style="text-align:left;border: 1px solid #000000;padding: 20px 15px;margin-bottom:10px">
                        <h3 class="text-bold">Customer Details</span></h3>
                        <a href="#" class=" btn btn-primary btn-sm float-right" id="EditCustomerDetails"><i class="fa fa-edit"></i></a>
                        <hr>
                        <div class="row" id="printCustomerDetails">
                            @if($customers)
                                <div class="col-sm-6">
                                    <ul id="custmrList">
                                        <li>Customer Name: {{$customers->INSUREDNAME_NEP}}</li>
                                        <li>Address: {{$customers->ADDRESSNEPALI}} </li>
                                        <li>Mobile Number: {{$customers->MOBILENO}} </li>
                                        <li>Email: {{$customers->EMAIL}} </li>
                                    </ul>
                                </div>
{{--                                <div class="col-sm-6">--}}
{{--                                    <ul id="custmrList">--}}
{{--                                        <li>Fathers Name: {{$customers->FATHERNAME}} </li>--}}
{{--                                        <li>Grandfather Name: {{$customers->GRANDFATHERNAME}} </li>--}}
{{--                                        <li>Grandmother Name: {{$customers->GRANDMOTHERNAME}} </li>--}}
{{--                                        <li>Mobile Number: {{$customers->MOBILENO}} </li>--}}
{{--                                        <li>Mothers Name: {{$customers->MOTHERNAME}} </li>--}}
{{--                                        <li>Temporary Address: {{$customers->TEMPORARYADDRESS}} </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
                            @else
                                <p>N/A</p>
                            @endif
                        </div>
                    </div>
                    <div style="text-align:left;border: 1px solid #000000;padding: 20px 15px">
                        <h3 class="text-bold">Policy Details</span></h3>
                        <a href="#" class="btn btn-primary btn-sm float-right" id="AddMotorDetail" title="edit motor details"><i class="fa fa-edit"></i></a>
                        <hr>
                        <div class="row">
                            <div class="col-sm-6">
                                <ul>
                                    <br>
                                    <li>Category : @if($formData['CLASSID'] == 21)
                                            Bike @elseif($formData['CLASSID'] == 22) Commercial Vehicle @else Private
                                            Vehicle @endif</li>
                                    <li> Vehicle Cost : Rs. {{number_format($formData['EXPUTILITIESAMT'], 2, '.', ',')}}</li>
{{--                                    <li>Model : {{$formData['MODEL'] ?? 'N/A'}}</li>--}}
{{--                                    <li>Premium Type : {{$formData['TYPECOVER'] ?? 'N/A'}}</li>--}}
                                    <li>Vehicle Number : {{$formData['VEHICLENO'] ?? 'N/A'}}</li>
                                    <li>Chasis Number : {{$formData['CHASISNO'] ?? 'N/A'}}</li>
                                    <li>Formation : {{$formData['FORMATION'] ?? 'N/A'}}</li>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul>
                                    <br>
                                    <li>Year Of Manufacture : {{$formData['YEARMANUFACTURE'] ?? 'N/A'}}</li>
                                    <li>CC / HP : {{$formData['CCHP'] ?? 'N/A'}}</li>
                                    <li>Engine Number : {{$formData['ENGINENO'] ?? 'N/A'}}</li>
                                    <li>Manufacturing Company : {{$formData['MANUFACTURE'] ?? 'N/A'}}</li>
                                    <li>Model : {{$formData['MODEL'] ?? 'N/A'}}</li>

{{--                                    <li><span class="text-bold small strong">Sum Insured</span> :--}}
{{--                                        Rs. {{number_format($formData['EXPUTILITIESAMT'], 2, '.', ',')}}--}}
{{--                                    </li>--}}
{{--                                    <li><span class="text-bold small strong">Period From </span>: <span--}}
{{--                                            class="strong">{{$formData['created_at']->format('d/M/y')}} <b>&nbsp;  TO  &nbsp;</b> {{$formData['created_at']->addYear(1)->format('d/M/y')}}</span>--}}
{{--                                    </li>--}}
{{--                                    <li><span class="text-bold small strong">Insurance </span>: <span--}}
{{--                                            class="strong"> Rs. {{number_format($formData['EXPUTILITIESAMT'], 2, '.', ',')}} </span>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <span class="text-bold small strong">Net Premium </span>: <span--}}
{{--                                            class="strong"> Rs. {{number_format($formData['NETPREMIUM'], 2, '.', ',')}} </span>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <span class="text-bold small strong">Stamp Duty</span>: <span--}}
{{--                                            class="strong"> Rs. {{number_format($formData['stamp'], 2, '.', ',')}} </span>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <span class="text-bold small strong">Total Vatable Premium (Rs.)</span>: <span--}}
{{--                                            class="strong"> Rs. {{number_format($formData['NETPREMIUM'] + $formData['stamp'], 2, '.', ',')}} </span>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <table class="ml-5 strong text-center">
                            <tbody>
                            <th class="text-uppercase text-left">{{('Premium Payable :') }}</th>
                            <tr>
                                <th class="text-bold text-left">{{('Sub Total') }}</th>
                                <td class="currency">
                                    Rs. {{number_format($formData['NETPREMIUM'] + $formData['stamp'], 2, '.', ',')}}</td>
                            </tr>
                            <tr class="border-bottom">
                                <th class="text-bold text-left">VAT 13%</th>
                                <td class="currency">Rs. {{number_format($formData['VATAMT'], 2, '.', ',')}}</td>
                            </tr>
                            <tr class="border-bottom">
                                <th class="text-bold text-left strong">Total</th>
                                <td class="currency">
                                    Rs. {{number_format($formData['TOTALNETPREMIUM'], 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <th class="text-bold text-left strong">Rupees :</th>
                                @php
                                    $f = new \NumberFormatter( locale_get_default(), \NumberFormatter::SPELLOUT );
                                    $word = $f ? $f->format($formData['TOTALNETPREMIUM']) : 'N/A';
                                @endphp
                                <th class="text-left text-capitalize">{{$word}}.</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div style="text-align:center;border: 1px solid #000000;padding: 50px 0">
                    <div>
                        <p>
                            {{--                        {{dd($paymentInfo)}}--}}
                        </p>
                        <p class="text-bold">Merchant To Pay: <span class="text-blue"> {{$merchantName}}</span></p>
                        <p class="text-bold">Your Reference Id : <span
                                class="text-blue"> {{$paymentInfo->RefId != null ? $paymentInfo->RefId : 'N/A'}}</span>
                            <a class="ml-3 btn btn-secondary btn-sm rounded changeRefId"
                               id="{{Request::get('customer_id')}}" title="generate reference code again">
                                <i class="fa fa-refresh"></i></a>
                        </p>
                        <p class="text-bold">Your Amount To Be Paid : <span
                                class="text-blue"> Rs. {{number_format($paymentInfo->Amount, 2, '.', '')}}</span></p>
                    </div>
                    <h3>PAY WITH</h3>
                    <small>[ Click image to pay ]</small>
                    <hr>

                    <form class="one-l" action="{{$ime_pay_checkout_url}}" method="post">
                        <input type="hidden" name="TokenId" value="{{$paymentInfo->TokenId}}">
                        <input type="hidden" name="MerchantCode" value="{{$merchantCode}}">
                        <input type="hidden" name="RefId" value="{{$paymentInfo->RefId}}">
                        <input type="hidden" name="TranAmount" value="{{($paymentInfo->Amount)}}">
                        <input type="hidden" name="Method" value="GET">
                        <input type="hidden" name="RespUrl" value="{{route('nonLife.calculator.imepay.success')}}">
                        <input type="hidden" name="CancelUrl" value="{{route('nonLife.calculator.imepay.cancil')}}">
                        <button style="background: transparent;border: none" type="submit">
                            <img src="{{asset('backend/img/SVG RED Logo.svg')}}" width="100" height="100"> </img>
                        </button>
                    </form>
                    @if(isset($showEsewa) && $showEsewa == 1)
                        <p class="one-l">OR</p>
                        <form class="one-l" id="paymentEsewa"
                              action="{{env('ESEWA_MERCHNANT_MAIN_URL') ? env('ESEWA_MERCHNANT_MAIN_URL')  : 'https://uat.esewa.com.np/epay/main'}}"
                              method="POST">
                            <input value="{{$esewa['tAmt']}}" name="tAmt" type="hidden">
                            <input value="{{$esewa['amt']}}" name="amt" type="hidden">
                            <input value="{{$esewa['txAmt']}}" name="txAmt" type="hidden">
                            <input value="{{$esewa['psc']}}" name="psc" type="hidden">
                            <input value="{{$esewa['pdc']}}" name="pdc" type="hidden">
                            <input value="{{$esewa['scd']}}" name="scd" type="hidden">
                            <input value="{{$esewa['pid']}}" name="pid" type="hidden">
                            <input
                                value="{{route('nonLife.calculator.esewa.success',['type'=>'esewa','ref'=>$esewa['pid']])}}"
                                type="hidden" name="su">
                            <input
                                value="{{route('nonLife.calculator.esewa.failed',['type'=>'esewa','ref'=>$esewa['pid']])}}"
                                type="hidden" name="fu">
                            <button style="background: transparent;padding:4px;border: 1px solid red" type="submit">
                                <img src="{{asset('backend/img/esewa_epay_logo.png')}}" width="100%"
                                     height="100%"> </img>
                            </button>
                        </form>
                    @endif

                    @if(isset($showConnectIps) && $showConnectIps == 1)
                        <p class="one-l">OR</p>
                        <form class="one-l" action="https://uat.connectips.com/connectipswebgw/loginpage" method="post">
                            <input type="hidden" name="MERCHANTID" id="MERCHANTID"
                                   value="{{$connectips['merchantId']}}"/>
                            <input type="hidden" name="APPID" id="APPID" value="{{$connectips['appId']}}"/>
                            <input type="hidden" name="APPNAME" id="APPNAME" value="{{$connectips['appName']}}"/>
                            <input type="hidden" name="TXNID" id="TXNID" value="{{$connectips['txnId']}}"/>
                            <input type="hidden" name="TXNDATE" id="TXNDATE" value="{{$connectips['txnDate']}}"/>
                            <input type="hidden" name="TXNCRNCY" id="TXNCRNCY" value="{{$connectips['txnCurrency']}}"/>
                            <input type="hidden" name="TXNAMT" id="TXNAMT" value="{{$connectips['txnAmount']}}"/>
                            <input type="hidden" name="REFERENCEID" id="REFERENCEID"
                                   value="{{$connectips['referenceId']}}"/>
                            <input type="hidden" name="REMARKS" id="REMARKS" value="{{$connectips['remarks']}}"/>
                            <input type="hidden" name="PARTICULARS" id="PARTICULARS"
                                   value="{{$connectips['particulars']}}"/>
                            <input type="hidden" name="TOKEN" id="TOKEN" value="{{$connectips['token']}}"/>
                            <button style="background: transparent;border: 1px solid red" type="submit">
                                <img src="{{asset('backend/img/connectIPSlogo.png')}}" width="100%"
                                     height="50px"> </img>
                            </button>
                        </form>
                    @endif

                    <hr>
                    <div class="">
                    <p>Is Payment already done from Ime Pay ?</p>
                    @php
                        $policyPayment = json_decode($policy->payment_token_details ?? '');
                    @endphp
                    <form action="{{(route('nonLife.calculator.check.policy.payment'))}}" method="POST">
                        @csrf
                        <input type="hidden" name="RefId" value="{{$policy->payment_ref_id ?? ''}}">
                        <input type="hidden" name="MerchantCode" value="{{$merchantCode}}">
                        <input type="hidden" name="TokenId" value="{{json_decode($policyPayment)->TokenId ?? '0'}}">
                        <input type="hidden" name="policy_id" value="{{$policy->id ?? ''}}">
                        <input type="hidden" name="status" value="{{$policy->status ?? ''}}">
                        <button type="submit" class="btn btn-primary btn-sm">Check IME Payment</button>
                    </form>
                    {{--                <p><span class="badge badge-pill badge-danger p-2">{{$output}}</span></p>--}}
                    {{--                <img src="{{asset('/uploads/imepay.png')}}">--}}
                    <br>
                    <a href="{{route('nonLife.calculator.draft.policy.view')}}"
                       class="btn btn-primary fa fa-arrow-left"> Draft Policies</a>
                    </div>
                </div>
                </div>
                <!-- add user Modal -->
            @include('Backend.NonLife.Partials.add-new-customer')

                <!-- add motor details Modal -->
                @include('Backend.NonLife.Partials.add-motor-details')
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
