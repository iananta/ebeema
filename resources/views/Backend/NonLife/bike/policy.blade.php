@extends('layouts.backend.containerform')

@section('title')
    Motor Premium Result
@endsection
@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="result-show container">
            <h3>Policy Generated Electronically - Motor Premium</h3>
            <br>
            <div class="row">
                <div class="col-sm-3"></div>
                <div style="text-align:center;border: 1px solid #000000;padding: 50px 0" class="col-sm-6">
                    @include('layouts.backend.alert')

                @if(is_array($data))
                        @foreach($data as $dt)
                            <div>
                                {{--   <p><span class="badge badge-pill badge-danger p-2">{{(dd($output))}}</span></p>--}}
                                <p class="text-bold">Product Type: <span class="text-blue">{{$dt['ClassName']}}</span></p>
                                <p class="text-bold">Insured Name : <span class="text-blue"> {{$dt['insured'] ?? ''}}</span></p>
                                <p class="text-bold">Policy Number : <span class="text-blue">{{$dt['policyNo']}} </span>
                                </p>
                                <p class="text-bold">VAT Invoice Number : <span
                                        class="text-blue">{{$dt['proformano']}} </span></p>
                                <p class="text-bold">Receipt Number : <span class="text-blue">{{$dt['receiptNo']}} </span>
                                </p>
                                <p class="text-bold">Paid Premium Amount : <span
                                        class="text-blue">Rs. {{$dt['tpPremium']}} </span></p>
                            </div>
                            <h3 class="text-bold">Payment From : <span
                                    class="text-blue">{{ucfirst($paymentFrom) ?? 'N/A' }} </span></h3>
                            <h3 class="text-bold">Payment Status : <span
                                    class="text-blue">{{$dt['succFailMsg']   }} </span></h3>
                            <h3 class="text-bold">Policy Status : <span
                                    class="text-blue">Electronically generated policy</span></h3>

                            <form action="{{route('nonLife.calculator.pdf.load')}}" method="GET"
                                  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="docid" value="{{$dt['AcceptanceNo']}}">
                                <input type="hidden" name="proformano" value="{{$dt['proformano']}}">
                                <button class="btn btn-primary" type="submit">Pdf Download</button>
                            </form>
                        @endforeach
                    @else
                        <p>Error : <b>{{$data}}</b></p>
                    @endif
                        <a href="{{route('nonLife.calculator.policy.view')}}" class="mt-3 btn btn-primary fa fa-arrow-left"> Policy List</a><br>
                        <a href="{{route('nonLife.calculator.draft.policy.view')}}" class="mt-3 btn btn-primary fa fa-arrow-left"> Draft Policies</a>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
