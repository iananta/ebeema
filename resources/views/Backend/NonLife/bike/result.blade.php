@extends('layouts.backend.containerform')

@section('title')
    Motor Premium Calculation
@endsection
{{--<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
{{--<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>--}}
@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection

@section('footer_js')
    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
    <script type="text/javascript">
        $('#MAKEVEHICLEID').on('change', function () {
            var mfComp = $("#MAKEVEHICLEID option:selected").val();
            $.ajax({
                type: "POST",
                url: "{{ route('nonLife.calculator.motor.manufacture.model') }}",
                data: {
                    mfComp: mfComp
                },
                dataType: 'json',
                success: function (response) {
                    var data = response.output;
                    // console.log(data);
                    $('#MAKEMODELID').empty().append($('<option>', {
                        text: "Select Model",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < data.length; i++) {
                        $('#MAKEMODELID').append($('<option>', {
                            value: data[i].ID,
                            text: data[i].ENGNAME
                        }));
                    }
                },
                error: function (e) {
                    console.log('something went wrong' + this.error);
                }
            });
        });
        $(".nepali-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#english-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });

        $("#MAKEVEHICLEID").on('change', function(){
            $("#manufactureName").val($("#MAKEVEHICLEID option:selected").text());
        });
        $("#MAKEMODELID").on('change', function(){
            $("#modelName").val($("#MAKEMODELID option:selected").text());
        });
        $("#VEHICLENAMEID").on('change', function(){
            $("#formationName").val($("#VEHICLENAMEID option:selected").text());
        });

        // function validateSize(input) {
        //     const fileSize = input.files[0].size / 1024 / 1024; // in MiB
        //     console.log(fileSize)
        //     if (fileSize > 5) {
        //         alert('File size exceeds 2 MiB');
        //         $(this)[0].val(''); //for clearing with Jquery
        //     } else {
        //         // Proceed further
        //     }
        // }

    </script>
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    @if(isset($output->data))
        <div class="box box-success">
            <h3>Calculated Result - {{Session::get('calculationFor') ? Session::get('calculationFor') : 'motor'}}
                calculation</h3>

            <div class="result-show container-fluid table-wrapper-scroll-y my-custom-scrollbar" id="calculationResult">
                <table class="table table-bordered table-striped mb-0">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Particulars</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Amount</th>
                    </tr>
                    </thead>
                    <tbody class="scroll-view">
                    @foreach($output->data as $dta)
                        {{--                    @if ($loop->first || $loop->last)--}}
                        <tr>
                            <td>{{$dta->ROWNUMBER}}</td>
                            <td>{{$dta->RskDescription}}</td>
                            <td>{{$dta->Rate}}</td>
                            <td>RS. {{$dta->Amount}}</td>
                        </tr>
                        {{--                    @endif--}}
                    @endforeach
                    </tbody>
                </table>
                {{--    <h4 class="text-center mt-5"><strong>No Data Found</strong></h4>--}}
            </div>
            <hr>
            {{--    {{dd($PremDetails)}}--}}
            <form class="calc-from" id="calculationForm" action="{{route('nonLife.calculator.motor.customer.select')}}"
                  method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="BRANCHID" value="1">
                <input type="hidden" name="CLASSID" value="{{$requested_data[0]['CLASSID']}}">
                <input type="hidden" name="DEPTID" value="2">
                <input type="hidden" name="HAS_TRAILOR" value="0">
                <input type="hidden" name="BASICPREMIUM_A" value="{{ $PremDetails->BASICPREMIUM ?? '0.00' }}">
                <input type="hidden" name="THIRDPARTYPREMIUM_B" value="{{ $PremDetails->THIRDPARTYPREMIUM ?? '0.00' }}">
                <input type="hidden" name="DRIVERPREMIUM_C" value="{{ $PremDetails->DRIVERPREMIUM ?? '0.00' }}">
                <input type="hidden" name="HELPERPREMIUM_D" value="{{ $PremDetails->HELPERPREMIUM ?? '0.00' }}">
                <input type="hidden" name="PASSENGERPREM_E" value="{{ $PremDetails->PASSENGERPREMIUM ?? '0.00' }}">
                <input type="hidden" name="RSDPREMIUM_F" value="{{ $PremDetails->POOLPREMIUM ?? '0.00' }}">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="VEHICLENO">Net Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Net Premium (Rs.)</div>
                            </div>
                            <input type="text" class="form-control"
                                   placeholder="Net Premium Amount"
                                   name="NETPREMIUM"
                                   value="{{ $PremDetails->NETPREMIUM ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6 d-none">
                        <label class="sr-only" for="thirdprty">Third party Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Third party Premium(Rs.)</div>
                            </div>
                            <input type="hidden" class="form-control"
                                   placeholder="Third party Premium"
                                   name="THIRDPARTYPREMIUM"
                                   value="{{ $PremDetails->THIRDPARTYPREMIUM ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="stmp">Stamp</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Stamp (Rs.)</div>
                            </div>
                            <input type="text" class="form-control"
                                   placeholder="Stamp"
                                   name="stamp"
                                   value="{{ $_GET['stamp']?? $stamp }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6 d-none">
                        <label class="sr-only" for="OTHERPREMIUM">Other Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Other Premium (Rs.)</div>
                            </div>
                            <input type="hidden" class="form-control" id="OTHERPREMIUM"
                                   placeholder="Other Premium"
                                   name="OTHERPREMIUM"
                                   value="{{ $PremDetails->OTHERPREMIUM ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="tvp">Total Vatable Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Total Vatable Premium (Rs.)</div>
                            </div>
                            <input type="text" class="form-control" id="tvp"
                                   placeholder="Total Vatable Premium"
                                   name="TOTALVATABLEPREMIUM"
                                   value="@php($totalVatable = $PremDetails->NETPREMIUM + $stamp){{ $totalVatable ?? '0.00' }}"
                                   readonly>
                        </div>
                    </div>
                    @php($vaTtotal = $totalVatable * 0.13)
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="inlineFormInputName2">VAT (%)</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">VAT (%)</div>
                            </div>
                            <input type="number" class="form-control" id="VAT"
                                   placeholder="VAT"
                                   name="VAT" value="{{ $_GET['VAT']??'13' }}" readonly>
                            <span class="p-2">Rs.</span><input type="text" class="form-control" id="VATAMT"
                                                               name="VATAMT"
                                                               value="{{ round($vaTtotal,2) ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="anp">Total Net Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Total Net Premium (Rs.)</div>
                            </div>
                            <input type="text" class="form-control" id="anp"
                                   placeholder="Actual Net Premium"
                                   name="TOTALNETPREMIUM"
                                   value="@php($totalNet = $totalVatable + round($vaTtotal,2)){{ $totalNet ?? '0.00' }}"
                                   readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="VEHICLENO">Vehicle No.</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Vehicle No.</div>
                            </div>
                            <input type="text" class="form-control text-uppercase" id="VEHICLENO"
                                   placeholder="Please enter Vehicle Number"
                                   name="VEHICLENO" required
                                   value="{{ $_GET['VEHICLENO']??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="ENGINENO">Engine No.</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Engine No.</div>
                            </div>
                            <input type="text" class="form-control text-uppercase" id="ENGINENO"
                                   placeholder="Please enter Engine Number"
                                   name="ENGINENO" required
                                   value="{{ $_GET['ENGINENO']??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="CHASISNO">Chasis No.</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Chasis No.</div>
                            </div>
                            <input type="text" class="form-control text-uppercase" id="CHASISNO"
                                   placeholder="Please enter Chasis Number"
                                   name="CHASISNO" required
                                   value="{{ $_GET['CHASISNO']??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="MAKEVEHICLEID">Manufacturer Company</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Manufacturer Company</div>
                            </div>
                            <select class="form-control form-control-inline input-medium search-selection" id="MAKEVEHICLEID"
                                    name="MAKEVEHICLEID" required>
                                <option selected disabled value=" ">Select the Manufacturer</option>
                                @foreach($manufacturers->data as $manu)
                                    <option value="{{$manu->ID}}">{{$manu->ENGNAME}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="MANUFACTURE" id="manufactureName">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="MAKEMODELID">Model</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Model</div>
                            </div>
                            <select class="form-control form-control-inline input-medium search-selection" id="MAKEMODELID"
                                    name="MAKEMODELID" required>
                                <option selected disabled value="">Select Manufacture to load the Model</option>
                            </select>
                            <input type="hidden" name="MODEL" id="modelName">
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="sr-only" for="VEHICLENAMEID">formation</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">formation</div>
                            </div>
                            <select class="form-control form-control-inline input-medium search-selection" id="VEHICLENAMEID"
                                    name="VEHICLENAMEID" required>
                                <option selected disabled>Select the formation</option>
                                @foreach($vehicleNames->data as $vcl)
                                    <option value="{{$vcl->ID}}">{{$vcl->ENGNAME}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="FORMATION" id="formationName">

                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="occupation">Occupation</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Occupation</div>
                            </div>
                            <select class="form-control form-control-inline input-medium search-selection" id="BUSSOCCPCODE"
                                    name="BUSSOCCPCODE" required>
                                <option selected disabled>Select the Occupation</option>
                                @foreach($occupationLists->data as $occ)
                                    <option value="{{$occ->BUSSOCCPCODE}}">{{$occ->DESCRIPTION}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="MODEUSE">Mode Of Use</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Mode Of Use</div>
                            </div>
                            <input type="text" class="form-control" id="MODEUSE"
                                   placeholder="Please enter Mode Of Use"
                                   name="MODEUSE" required
                                   value="{{ $_GET['MODEUSE']??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="RegistrationDate">Registration Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend date">
                                <div class="input-group-text">Registration Date</div>
                            </div>
                            <input type="text" class="form-control nepali-datepicker" name="NEPALIREGISTRATIONDATE" placeholder="Select Nepali Date" required>
                            <input class="form-control" type="text" id="english-date"  name="REGISTRATIONDATE" placeholder="Shows English Date" readonly/>
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <small class="help-block">* File Must Be In Image Format. Insert Image of size less than 2 MB</small>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="bluebook_images" id="bluebook_image"
                                   required>
                            <label class="custom-file-label" for="bluebook_images">Browse Blue Book Image</label>
                            <span class="custom-file-image" for="images_view"></span>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <small class="help-block">* File Must Be In Image Format. Insert Image of size less than 2 MB</small>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="bike_images" id="bike_image" required>
                            <label class="custom-file-label" for="bike_images">Browse Motor Image</label>
                            <span class="custom-file-image" for="images_view"></span>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary">Proceed To Customers</button>
                    </div>
                </div>
            </form>
            <!-- /.box -->
        </div>
    @else
        <div class="text-center">
            <h3>Calculated Result - Error in calculation</h3>
            <br>
            <br>
            <a href="{{route('nonLife.calculator.bike')}}" class="btn btn-primary fa fa-arrow-left"> Go
                Back</a>
        </div>
    @endif
    <!-- /.col (right) -->
    <!-- /.row -->
@stop
