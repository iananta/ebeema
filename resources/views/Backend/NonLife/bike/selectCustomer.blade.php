@extends('layouts.backend.containerform')

@section('title')
    Motor Premium Calculation Customer Selection
@endsection

@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection

@section('footer_js')
    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
    <script>

        $(document).ready(function () {
            $('.search-select').select2();
            // getDetails();
        });

        function getDetails() {
            var details = $('.search-select').find(':selected').data('details');
            // console.log(details);
            if (details) {
                $('#printCustomerDetails').empty().append('<div class="col-sm-6"> <ul id="custmrList"><li>Customer Name: ' + details.INSUREDNAME_ENG + ' | ' + details.INSUREDNAME_NEP + ' </li><li>Address: ' + details.ADDRESS + '</li><li>Citizenship No.: ' + details.CITIZENSHIPNO + '</li><li>Date Of Birth: ' + details.DATEOFBIRTH + '</li><li>Email: ' + details.EMAIL + '</li></ul></div><div class="col-sm-6"><ul id="custmrList"><li>Fathers Name: ' + details.FATHERNAME + '</li><li>Grandfather Name: ' + details.GRANDFATHERNAME + '</li><li>Grandmother Name: ' + details.GRANDMOTHERNAME + '</li><li>Mobile Number: ' + details.MOBILENO + '</li><li>Mothers Name: ' + details.MOTHERNAME + '</li><li>Temporary Address: ' + details.TEMPORARYADDRESS + '</li></ul></div>');
                $('.cst-detail').removeClass('d-none');
            } else {
                $('#printCustomerDetails').empty().append('<ul id="custmrList"><li>N/A</li></ul>');
                $('.cst-detail').addClass('d-none');
            }
        };

        $('#provinceId').on('change', function () {
            var provinceID = $(this).val();
            // console.log(provinceID);
            $.ajax({
                type: "POST",
                url: "{{ route('province.district') }}",
                data: {
                    provinceID: provinceID
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $('#districtId').empty().append($('<option>', {
                        text: "Select District",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#districtId').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION
                        }));
                    }

                    $('#ISSUE_DISTRICT_ID_LIST').empty().append($('<option>', {
                        text: "Select Issued District",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#ISSUE_DISTRICT_ID_LIST').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION
                        }));
                    }
                    if ($('#calculationForm').find('[name="ZONEID"]').val() && $('#CustomerList').find(":selected").data('details')) {
                        getSelectedDistrict();
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });

        });
        $('#districtId').on('change', function () {
            var DistrictID = $(this).val();
            $.ajax({
                type: "POST",
                url: "{{ route('district.mnuvdc') }}",
                data: {
                    DistrictID: DistrictID
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $('#MNUId').empty().append($('<option>', {
                        text: "Select Municipality ",
                        disabled: '',
                        selected: ''
                    }));
                    for (var i = 0; i < response.mnu.length; i++) {
                        $('#MNUId').append($('<option>', {
                            value: response.mnu[i].MNUCODE,
                            text: response.mnu[i].MNU
                        }));
                    }

                    $('#VDCId').empty().append($('<option>', {
                        text: "Select VDC ",
                        disabled: '',
                        selected: ''
                    }));
                    for (var j = 0; j < response.vdc.length; j++) {
                        $('#VDCId').append($('<option>', {
                            value: response.vdc[j].VDCCODE,
                            text: response.vdc[j].VDC
                        }));
                    }
                    if ($('#calculationForm').find('[name="DISTRICTID"]').val() && $('#CustomerList').find(":selected").data('details')) {
                        getSelectedMnuVdc();
                        // console.log('im on for mnuVdc');
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });

        });

        $('#AddKycCustomer').on('click', function (e) {
            e.preventDefault();
            var frm = $('#calculationForm');
            var formData = new FormData(frm[0]);
            $("#AddKycCustomer").prop('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{ route('user.kyc.entry') }}",
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#preloader').show();
                },
                success: function (response) {
                    $('#preloader').hide();
                    $('#calculationForm')[0].reset();
                    $("#AddKycCustomer").prop('disabled', false);
                    // console.log(response.message);
                    if (response.data) {
                        $("#makeModalClose").trigger("click");
                        $('#CustomerList').append($('<option>', {
                            value: response.data.customer_id,
                            text: response.data.INSUREDNAME_ENG + ' : ' + response.data.MOBILENO,
                            selected: true,
                        }));
                        $('#CustomerList').find(":selected").data('details', response.data);
                        $('#printCustomerDetails').empty().append('<div class="col-sm-6"><ul id="custmrList"><li>Customer Name: ' + response.data.INSUREDNAME_ENG + '|' + response.data.INSUREDNAME_NEP + '</li><li>Address: ' + response.data.ADDRESS + '</li><li>Citizenship No.: ' + response.data.CITIZENSHIPNO + '</li><li>Date Of Birth: ' + response.data.DATEOFBIRTH + '</li><li>Email: ' + response.data.EMAIL + '</li></ul></div><div class="col-sm-6"><ul id="custmrList"> <li>Fathers Name: ' + response.data.FATHERNAME + '</li><li>Grandfather Name: ' + response.data.GRANDFATHERNAME + '</li><li>Grandmother Name: ' + response.data.GRANDMOTHERNAME + '</li><li>Mobile Number: ' + response.data.MOBILENO + '</li><li>Mothers Name: ' + response.data.MOTHERNAME + '</li><li>Temporary Address: ' + response.data.TEMPORARYADDRESS + '</li></ul></div>');
                        $('.cst-detail').removeClass('d-none');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            html: response.message ?? "Customer Added Successfully",
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                        // swal("Congratulations!", response.message ?? "Customer Added Successfully", "success");
                    }
                    // console.log(response);
                },
                error: function (response) {
                    $('#preloader').hide();
                    $("#AddKycCustomer").prop('disabled', false);
                    var err = '';
                    if (response.responseJSON && response.responseJSON.errors) {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.errors)) {
                            err += '-' + value + '<br>';
                        }
                    } else {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.message)) {
                            err += '-' + value + '<br>';
                        }
                    }
                    // console.log(err);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: typeof msg != 'object' && msg !== null ? msg.message : 'Error',
                        html: err ?? 'Something went wrong',
                        showConfirmButton: false,
                        timer: 2500,
                        timerProgressBar: true,
                    });
                }
            });
        });
        window.onload = function () {
            $('.dob-datepicker').nepaliDatePicker({
                container: '#dateDisplay',
                ndpYear: true,
                ndpMonth: true,
                ndpEnglishInput: 'dob-english-date'
            });
            $('.issued-datepicker').nepaliDatePicker({
                container: '#issuedDateDisplay',
                ndpYear: true,
                ndpMonth: true,
                ndpEnglishInput: 'issued-date'
            });
        };
        $(".dob-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#dob-english-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
        $(".issued-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#issued-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
        // $('.edit-datepicker').nepaliDatePicker({
        //     container: '#editDateDisplay',
        //     ndpYear: true,
        //     ndpMonth: true,
        //     ndpEnglishInput: 'edit-english-date'
        // });
        // $(".edit-datepicker").on("input", function (e) {
        //     var nepDate = $(this).val();
        //     var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
        //     var EngDate = NepaliFunctions.BS2AD(Dt);
        //     $('#edit-english-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
        //     // console.log(EngDate);
        // });
        $('#INSUREDTYPEID').on('change', function () {
            var tp = $(this).val();
            $('#panno').attr("required", "false");
            if (tp != 2) {
                $('#panno').attr("required", "true");
            }
        });
        // Hide as insured type
        $('#INSUREDTYPEID').on('change', function () {
        if (this.value === '1'){
            $(".cpmreg").show();
        } else {
            $(".cpmreg").hide();
        }
        });        
        $('#MAKEVEHICLEID').on('change', function () {
            var mfComp = $("#MAKEVEHICLEID option:selected").val();
            $.ajax({
                type: "POST",
                url: "{{ route('nonLife.calculator.motor.manufacture.model') }}",
                data: {
                    mfComp: mfComp
                },
                dataType: 'json',
                success: function (response) {
                    var data = response.output;
                    // console.log(data);
                    $('#MAKEMODELID').empty().append($('<option>', {
                        text: "Select Model",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < data.length; i++) {
                        $('#MAKEMODELID').append($('<option>', {
                            value: data[i].ID,
                            text: data[i].ENGNAME
                        }));
                    }
                },
                error: function (e) {
                    console.log('something went wrong' + this.error);
                }
            });
        });
        $(".closeMe").click(function () {
            $("#AddCustomer").modal("hide");
        });
        // $("#EditCustomerDetails").on('click', function () {
        //     var selectedCustomer = $('#CustomerList').find(":selected").data('details');
        //     var cForm = $('#calculationForm');
        //     $("#AddCustomer").modal("show");
        //     $('input[name=INSUREDNAME_ENG]').val(selectedCustomer.INSUREDNAME_ENG);
        //     cForm.find('[name="CATEGORYID"]').val(selectedCustomer.CATEGORYID);
        //     cForm.find('[name="INSUREDTYPE"]').val(selectedCustomer.INSUREDTYPE);
        //     cForm.find('[name="kycclassification"]').val(selectedCustomer.kycclassification);
        //     cForm.find('[name="KYCRiskCategory"]').val(selectedCustomer.KYCRiskCategory);
        //     cForm.find('[name="INSUREDNAME_NEP"]').val(selectedCustomer.INSUREDNAME_NEP);
        //     cForm.find('[name="ZONEID"]').val(selectedCustomer.ZONEID);
        //     cForm.find('[name="ZONEID"]').change();
        //     cForm.find('[name="ADDRESS"]').val(selectedCustomer.ADDRESS);
        //     cForm.find('[name="ADDRESSNEPALI"]').val(selectedCustomer.ADDRESSNEPALI);
        //     cForm.find('[name="WARDNO"]').val(selectedCustomer.WARDNO);
        //     cForm.find('[name="HOUSENO"]').val(selectedCustomer.HOUSENO);
        //     cForm.find('[name="PLOTNO"]').val(selectedCustomer.PLOTNO);
        //     cForm.find('[name="TEMPORARYADDRESS"]').val(selectedCustomer.TEMPORARYADDRESS);
        //     cForm.find('[name="NTEMPORARYADDRESS"]').val(selectedCustomer.NTEMPORARYADDRESS);
        //     cForm.find('[name="HOMETELNO"]').val(selectedCustomer.HOMETELNO);
        //     cForm.find('[name="MOBILENO"]').val(selectedCustomer.MOBILENO);
        //     cForm.find('[name="EMAIL"]').val(selectedCustomer.EMAIL);
        //     cForm.find('[name="MOBILENO"]').val(selectedCustomer.MOBILENO);
        //     cForm.find('[name="OCCUPATION"]').val(selectedCustomer.OCCUPATION);
        //     cForm.find('[name="INCOMESOURCE"]').val(selectedCustomer.INCOMESOURCE);
        //     cForm.find('[name="PANNO"]').val(selectedCustomer.PANNO);
        //     cForm.find('[name="GENDER"]').val(selectedCustomer.GENDER);
        //     cForm.find('[name="MARITALSTATUS"]').val(selectedCustomer.MARITALSTATUS);
        //     cForm.find('[name="NEPALIDATEOFBIRTH"]').val(convDateBS(selectedCustomer.DATEOFBIRTH));
        //     cForm.find('[name="NEPALIISSUEDATE"]').val(convDateBS(selectedCustomer.ISSUEDATE));
        //     cForm.find('[name="DATEOFBIRTH"]').val(selectedCustomer.DATEOFBIRTH);
        //     cForm.find('[name="ISSUEDATE"]').val(selectedCustomer.ISSUEDATE);
        //     cForm.find('[name="CITIZENSHIPNO"]').val(selectedCustomer.CITIZENSHIPNO);
        //     cForm.find('[name="ISSUE_DISTRICT_ID"]').val(selectedCustomer.ISSUE_DISTRICT_ID);
        //     cForm.find('[name="FATHERNAME"]').val(selectedCustomer.FATHERNAME);
        //     cForm.find('[name="MOTHERNAME"]').val(selectedCustomer.MOTHERNAME);
        //     cForm.find('[name="GRANDFATHERNAME"]').val(selectedCustomer.GRANDFATHERNAME);
        //     cForm.find('[name="GRANDMOTHERNAME"]').val(selectedCustomer.GRANDMOTHERNAME);
        //     cForm.find('[name="HUSBANDNAME"]').val(selectedCustomer.HUSBANDNAME);
        //     cForm.find('[name="WIFENAME"]').val(selectedCustomer.WIFENAME);
        //     cForm.find('[name="KYCNO"]').val(selectedCustomer.KYCNO);
        //     cForm.find('[name="customer_id"]').val(selectedCustomer.customer_id);
        //     cForm.find('[name="kyc_id"]').val(selectedCustomer.id);
        //     if (selectedCustomer.photo) {
        //         $(".photoInput").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.photo + '">');
        //     }
        //     if (selectedCustomer.citfrntimg) {
        //         $(".citFrntImg").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.citfrntimg + '">');
        //     }
        //     if (selectedCustomer.citbackimg) {
        //         $(".citBackImg").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.citbackimg + '">');
        //     }
        //     if (selectedCustomer.cpmregimg) {
        //         $(".panImg").siblings(".custom-file-image").empty().append('<img style="max-width: 50%; margin-top: 10px;" src="' + selectedCustomer.cpmregimg + '">');
        //     }
        //     // console.log(selectedCustomer);
        // });

        // function convDateBS(date) {
        //     var dobDt = NepaliFunctions.ConvertToDateObject(date, "YYYY-MM-DD");
        //     var convDate = NepaliFunctions.AD2BS(dobDt);
        //     return NepaliFunctions.ConvertDateFormat(convDate);
        // }
        //
        // function getSelectedDistrict() {
        //     var selectedCustomer = $('#CustomerList').find(":selected").data('details');
        //     var cForm = $('#calculationForm');
        //     // console.log(selectedCustomer);
        //     cForm.find('[name="DISTRICTID"]').val(selectedCustomer.DISTRICTID);
        //     cForm.find('[name="DISTRICTID"]').change();
        //     cForm.find('[name="ISSUE_DISTRICT_ID"]').val(selectedCustomer.ISSUE_DISTRICT_ID);
        // }
        //
        // function getSelectedMnuVdc() {
        //     var selectedCustomer = $('#CustomerList').find(":selected").data('details');
        //     var cForm = $('#calculationForm');
        //     // console.log(selectedCustomer);
        //     cForm.find('[name="MUNICIPALITYCODE"]').val(selectedCustomer.MUNICIPALITYCODE);
        //     cForm.find('[name="VDCCODE"]').val(selectedCustomer.VDCCODE);
        // }

        $('.addCustBtn').on('click', function () {
            var cForm = $('#calculationForm');
            cForm[0].reset();
            $('.chgValNull input[type=hidden]').val('');
            $("#CustomerList").select2('destroy').val("").select2();
            $(".custom-file-input").siblings(".custom-file-image").empty();
            // $('#calculationForm').
            // getDetails();
        });

        {{--$("#AddMotorDetail").on('click', function () {--}}
        {{--    var cForm = $('#motorCalculationForm');--}}
        {{--    var dateOfBirth = $('#edit-english-date').val();--}}
        {{--    // console.log(dateOfBirth);--}}
        {{--    $("#AddMotorDetails").modal("show");--}}
        {{--    cForm.find('[name="NEPALIREGISTRATIONDATE"]').val(convDateBS(dateOfBirth));--}}
        {{--    cForm.find('[name="MAKEMODELID"]').change();--}}
        {{--});--}}
        {{--$("#updateMotorInfo").on('click', function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    var frm = $('#motorCalculationForm');--}}
        {{--    var formData = new FormData(frm[0]);--}}
        {{--    // console.log(formData);--}}
        {{--    $("#updateMotorInfo").prop('disabled', true);--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{ route('nonLife.calculator.update.motor.info') }}",--}}
        {{--        data: formData,--}}
        {{--        headers: {--}}
        {{--            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
        {{--        },--}}
        {{--        processData: false,--}}
        {{--        contentType: false,--}}
        {{--        beforeSend: function () {--}}
        {{--            $('#preloader').show();--}}
        {{--        },--}}
        {{--        success: function (response) {--}}
        {{--            $('#preloader').hide();--}}
        {{--            $("#updateMotorInfo").prop('disabled', false);--}}
        {{--            // console.log(response.message);--}}
        {{--            if (response.data) {--}}
        {{--                $('#AddMotorDetails').modal('hide');--}}
        {{--                Swal.fire({--}}
        {{--                    position: 'top-end',--}}
        {{--                    icon: 'success',--}}
        {{--                    title: 'Congratulations!',--}}
        {{--                    html: response.message ?? "Motor Info updated Successfully",--}}
        {{--                    showConfirmButton: false,--}}
        {{--                    timer: 2000,--}}
        {{--                    timerProgressBar: true,--}}
        {{--                });--}}
        {{--                location.reload();--}}
        {{--                // swal("Congratulations!", response.message ?? "Customer Added Successfully", "success");--}}
        {{--            }--}}
        {{--            // console.log(response);--}}
        {{--        },--}}
        {{--        error: function (response) {--}}
        {{--            $('#preloader').hide();--}}
        {{--            $("#updateMotorInfo").prop('disabled', false);--}}
        {{--            var err = '';--}}
        {{--            if (response.responseJSON && response.responseJSON.errors) {--}}
        {{--                var msg = response.responseJSON;--}}
        {{--                for (let value of Object.values(msg.errors)) {--}}
        {{--                    err += '-' + value + '<br>';--}}
        {{--                }--}}
        {{--            } else {--}}
        {{--                var msg = response.responseJSON;--}}
        {{--                err = msg.message;--}}
        {{--            }--}}
        {{--            // console.log(err);--}}
        {{--            Swal.fire({--}}
        {{--                position: 'top-end',--}}
        {{--                icon: 'error',--}}
        {{--                title: typeof msg != 'object' && msg !== null ? msg.message : 'Error',--}}
        {{--                html: err ?? 'Something went wrong',--}}
        {{--                showConfirmButton: false,--}}
        {{--                timer: 2500,--}}
        {{--                timerProgressBar: true,--}}
        {{--            });--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}
        // $("#MAKEMODELID").on('change', function () {
        //     $("#modelName").val($("#MAKEMODELID option:selected").text());
        // });
        // $('.clsDtls').one('click', function () {
        //     $('#AddMotorDetails').modal('hide');
        // });
    </script>
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="result-show container">
            <h3>Select Customer - {{Session::get('calculationFor') ? Session::get('calculationFor') : 'motor'}}
                Premium</h3>
            <br>
            @include('layouts.backend.alert')
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-5">
                    <div style="text-align:center;border: 1px solid #000000;padding: 20px 0">
                        <h3 class="text-bold">Select Customer</span></h3>
                        <hr>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-sm addCustBtn" data-toggle="modal"
                                data-target="#AddCustomer">
                            Add New Customer
                        </button>
                        <p class="mt-2">OR</p>
                        <hr>

                        @if(isset($makePolicy))
                            <form action="{{route('nonLife.calculator.motor.payment')}}"
                                  id="selectCustomerForm" method="GET"
                                  enctype="multipart/form-data">
                                {{--                                @csrf--}}
                                <div class="form-group">
                                    <label class="" for="userSelect">Select <a href="{{route('admin.leads.customers')}}"
                                                                               target="_blank" data-toggle="tooltip"
                                                                               data-placement="top"
                                                                               title="Customer List">Customer</a></label>
                                    <div class="col-sm-12 mx-auto">
                                        <select class="form-control search-select" onchange="getDetails()"
                                                name="customer_id" id="CustomerList"
                                                required>
                                            <option hidden="" disabled="disabled" selected="selected" value="">Click
                                                Here To Select Customer
                                            </option>
                                            @foreach($customers as $user)
                                                <option data-details="{{json_encode($user)}}"
                                                        value="{{$user->customer_id}}" @if(isset($formData['customer_id'])){{($formData['customer_id'] == $user->customer_id) ? "selected" : ''}}@endif >{{$user->INSUREDNAME_ENG .' : '.$user->MOBILENO}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary selectCstmer" type="submit">Proceed To Payment</button>
                            </form>
                        @endif
                    </div>
                    <a href="{{route('nonLife.calculator.bike')}}"
                       class="mt-2 btn btn-primary fa fa-arrow-left"> Back</a>
                </div>
                <div class="col-sm-1"></div>
{{--                <div class="col-sm-7">--}}
{{--                    <div style="text-align:left;border: 1px solid #000000;padding: 20px 15px;margin-bottom:10px">--}}
{{--                        <h3 class="text-bold">Selected Customer Details</span></h3>--}}
{{--                        <hr>--}}
{{--                        <div class="row" id="printCustomerDetails"><p>N/A</p></div>--}}
{{--                        <div class="cst-detail d-none">--}}
{{--                            <hr>--}}
{{--                            <a href="#" class=" btn btn-primary" id="EditCustomerDetails"><i class="fa fa-edit"></i>--}}
{{--                                Edit KYC</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div style="text-align:left;border: 1px solid #000000;padding: 20px 15px">--}}
{{--                        <h3 class="text-bold">Policy Details</span></h3>--}}
{{--                        <hr>--}}
{{--                        <a class="btn btn-primary btn-sm"--}}
{{--                           --}}{{----}}{{--target="_blank"--}}{{----}}{{-- href="{{route('nonLife.calculator.motor.make.debit.note',['motorId'=> Session::get('motorCalcId') ?? ''])}}">Download--}}
{{--                            Debit Note</a>--}}
{{--                        <hr>--}}
{{--                        <a href="#" class="btn btn-primary" id="AddMotorDetail"><i class="fa fa-edit"></i> Edit Motor--}}
{{--                            Details</a>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-6">--}}
{{--                        <ul>--}}
{{--                            <br>--}}
{{--                            <li>Motor Premium for: @if($formData['CLASSID'] == 21)--}}
{{--                                    Bike @elseif($formData['CLASSID'] == 22) Commercial Vehicle @else Private--}}
{{--                                    Vehicle @endif</li>--}}
{{--                            <li>Model : {{$formData['MODEL'] ?? 'N/A'}}</li>--}}
{{--                            <li>Premium Type : {{$formData['TYPECOVER'] ?? 'N/A'}}</li>--}}
{{--                            <li>Year Of Manufacture : {{$formData['YEARMANUFACTURE'] ?? 'N/A'}}</li>--}}
{{--                            <li>CC / HP : {{$formData['CCHP'] ?? 'N/A'}}</li>--}}
{{--                            <li>Engine Number : {{$formData['ENGINENO'] ?? 'N/A'}}</li>--}}
{{--                            <li>Chasis Number : {{$formData['CHASISNO'] ?? 'N/A'}}</li>--}}
{{--                            <li>Mode Of Use : {{$formData['MODEUSE'] ?? 'N/A'}}</li>--}}
{{--                            <li>Registration Date : {{$formData['REGISTRATIONDATE'] ?? 'N/A'}}</li>--}}
{{--                        </ul>--}}
{{--                            </div>--}}
{{--                            <div class="col-sm-6">--}}
{{--                        <ul>--}}
{{--                            <br>--}}
{{--                            <li><span class="text-bold small strong">Sum Insured</span> :--}}
{{--                                Rs. {{number_format($formData['EXPUTILITIESAMT'], 2, '.', ',')}}--}}
{{--                            </li>--}}
{{--                            <li><span class="text-bold small strong">Period From </span>: <span--}}
{{--                                    class="strong">{{$formData['created_at']->format('d/M/y')}} <b>&nbsp;  TO  &nbsp;</b> {{$formData['created_at']->addYear(1)->format('d/M/y')}}</span>--}}
{{--                            </li>--}}
{{--                            <li><span class="text-bold small strong">Insurance </span>: <span--}}
{{--                                    class="strong"> Rs. {{number_format($formData['EXPUTILITIESAMT'], 2, '.', ',')}} </span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <span class="text-bold small strong">Net Premium </span>: <span--}}
{{--                                    class="strong"> Rs. {{number_format($formData['NETPREMIUM'], 2, '.', ',')}} </span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <span class="text-bold small strong">Stamp Duty</span>: <span--}}
{{--                                    class="strong"> Rs. {{number_format($formData['stamp'], 2, '.', ',')}} </span>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <span class="text-bold small strong">Total Vatable Premium (Rs.)</span>: <span--}}
{{--                                    class="strong"> Rs. {{number_format($formData['NETPREMIUM'] + $formData['stamp'], 2, '.', ',')}} </span>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <hr>--}}
{{--                        <table class="small strong">--}}
{{--                            <tbody>--}}
{{--                            <tr>--}}
{{--                                <th class="text-bold text-left">{{('Sub Total') }}</th>--}}
{{--                                <td class="currency">Rs. {{number_format($formData['NETPREMIUM'] + $formData['stamp'], 2, '.', ',')}}</td>--}}
{{--                            </tr>--}}
{{--                            <tr class="border-bottom">--}}
{{--                                <th class="text-bold text-left">VAT 13%</th>--}}
{{--                                <td class="currency">Rs. {{number_format($formData['VATAMT'], 2, '.', ',')}}</td>--}}
{{--                            </tr>--}}
{{--                            <tr class="border-bottom">--}}
{{--                                <th class="text-bold text-left strong">Total</th>--}}
{{--                                <td class="currency">--}}
{{--                                    Rs. {{number_format($formData['TOTALNETPREMIUM'], 2, '.', ',')}}</td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th class="text-bold text-left strong">Rupees.</th>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                @php--}}
{{--                                    $f = new \NumberFormatter( locale_get_default(), \NumberFormatter::SPELLOUT );--}}
{{--                                    $word = $f ? $f->format($formData['TOTALNETPREMIUM']) : 'N/A';--}}
{{--                                @endphp--}}
{{--                                <th class="text-left text-capitalize">{{$word}}.</th>--}}
{{--                            </tr>--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <!-- add user Modal -->
            @include('Backend.NonLife.Partials.add-new-customer')

            <!-- add motor details Modal -->
{{--                @include('Backend.NonLife.Partials.add-motor-details')--}}
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
