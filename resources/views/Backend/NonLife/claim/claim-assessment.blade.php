@extends('layouts.backend.containerlist')

@section('title')
    Claim Assessment
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.claim.kyc')}}">Go Back</a>
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <table id="policy-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Claim ID</th>
                    <th>Loss Type</th>
                    <th>Sub Loss Type</th>
                    <th>Amount Claimed</th>
                    <th>Amount Assess</th>
                    <th>Net Amount Assess</th>
                    <th>Amount Deprc</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                        <tr class="gradeX" id="row_{{ $claim['CLAIMID'] }}">
                            <td>
                                <b># {{  $claim['CLAIMID'] }}</b>
                            </td>
                            <td>
                                {{  $claim['LOSSTYPE'] }}
                            </td><td>
                                {{  $claim['SUBLOSSTYPE'] }}
                            </td><td>
                                {{  $claim['AMTCLAIMED'] }}
                            </td><td>
                                {{  $claim['AMTASSESS'] }}
                            </td><td>
                                {{  $claim['AMTNETASSES'] }}
                            </td><td>
                                {{  $claim['AMTDEPRC'] }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
