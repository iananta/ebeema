@extends('layouts.backend.containerlist')

@section('title')
    Claim Intimation Document
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.claim.kyc')}}">Go Back</a>
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <table id="policy-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Document No.</th>
                    <th>Document ID</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                        <tr class="gradeX" id="row_{{ $claim['DOCID'] }}">
                            <td>
                                <b># {{  $claim['SN'] }}</b>
                            </td>
                            <td>
                                {{  $claim['DOCUMENTNO'] }}
                            </td>
                            <td>
                                {{  $claim['DOCID'] }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
