@extends('layouts.backend.containerlist')

@section('title')
    Claim By KYC ID
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="box-body">
            <table id="-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Claim ID</th>
                    <th>Claim No.</th>
                    <th>Document Number</th>
                    <th>Own Vehicle No.</th>
                    <th>Registered Date</th>
                    <th>Document ID</th>
                    <th>Status ID</th>
                    <th>Claim Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                        <tr class="gradeX" id="row_{{ $claim['CLAIMID'] }}">
                            <td>
                                <b># {{  $claim['CLAIMID'] }}</b>
                            </td>
                            <td>
                                {{  $claim['CLAIMNO'] }}
                            </td>
                            <td>
                                {{  $claim['DOCUMENTNO'] }}
                            </td>
                            <td>
                                {{ $claim['OWNVEHICLENO'] }}
                            </td>
                            <td>
                                {{ $claim['REGD_DATE'] }}
                            </td>
                            <td>
                                {{ $claim['DOCID'] }}
                            </td>
                            <td>
                                {{ $claim['STATUSID'] }}
                            </td>
                            <td>
                                {{ $claim['CLAIMSTATUS'] }}
                            </td>
                            <td>
                                <form action="" method="post">

                                </form>
                                <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.claim.status', ['claim_number' => $claim['CLAIMNO']])}}">Claim Status</a>
                                <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.claim.assessment', ['claim_number' => $claim['CLAIMNO']])}}">Claim Assessment Summary</a>
                                <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.claim.survey', ['claim_number' => $claim['CLAIMNO']])}}">Claim Surveyor</a>
                                <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.intimate.list', ['claim_number' => $claim['CLAIMNO']])}}">Intimate Claim List</a>
                                <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.claim.doc', ['claim_number' => $claim['CLAIMNO']])}}">Claim Document</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
