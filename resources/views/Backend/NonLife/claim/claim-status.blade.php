@extends('layouts.backend.containerlist')

@section('title')
    Claim Status
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.claim.kyc')}}">Go Back</a>
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <table id="policy-table" class="table table-bordered table-hover">

                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Claim No. {{$index}}</th>
                    <td>{{$claim['CLAIMNO']}}</td>
                </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Place of Occur</th>
                    <td>{{$claim['PLCOFOCCUR']}}</td>
                    </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Date of occur</th>
                    <td>{{$claim['DATEOFOCCUR']}}</td>
                </tr>
                <tr class="gradeX" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Aff Property</th>
                    <td>{{$claim['AFFPROPERTY']}}</td>
                </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Vehicle Own Number</th>
                    <td>{{$claim['VEHICLENOOWN']}}</td>
                    </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Vehicle No TP</th>
                    <td>{{$claim['VEHICLENOTP']}}</td>
                </tr>
                <tr class="gradeX" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Insured Name</th>
                    <td>{{$claim['INSUREDNAME']}}</td>
                </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Assigned Date</th>
                    <td>{{$claim['ASSIGNEDDATE']}}</td>
                </tr>
                <tr class="gradeX" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Username</th>
                    <td>{{$claim['USERNAME']}}</td>
                </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>User ID</th>
                    <td>{{$claim['USERID']}}</td>
                    </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Mobile No.</th>
                    <td>{{$claim['MOBILENO']}}</td>
                    </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Has Surveyour?</th>
                    <td>{{$claim['HASSURVEYOUR']}}</td>
                </tr>
                <tr class="gradeX" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Claim Status</th>
                    <td>{{$claim['CLAIMSTATUS']}}</td>
                </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Registered Date</th>
                    <td>{{$claim['REGDATE']}}</td>
                    </tr>
                    <tr class="gradeX mb-3 mt-3" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Estimated Amount</th>
                    <td>{{$claim['ESTAMOUNT']}}</td>
                </tr>
                <tr class="gradeX" id="row_{{ $claim['CLAIMNO'] }}">
                    <th>Claim Causes</th>
                    <td>{{$claim['CLAIMCAUSES']}}</td>
                </tr>
                    @endforeach
                </tbody>


            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
