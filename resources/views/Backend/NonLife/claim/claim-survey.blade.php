@extends('layouts.backend.containerlist')

@section('title')
    Claim Surveyor Data
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.claim.kyc')}}">Go Back</a>
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <table id="policy-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Appointed Date</th>
                    <th>Report Date</th>
                    <th>Surveyor Name</th>
                    <th>Surveyor Address</th>
                    <th>Telephone No.</th>
                    <th>Has Report Submitted?</th>
                    <th>Username</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                        <tr class="gradeX" id="row_{{ $claim['ID'] }}">
                            <td>
                                <b># {{  $claim['ID'] }}</b>
                            </td>
                            <td>
                                {{  $claim['APPOINTDT'] }}
                            </td>
                            <td>
                                {{  $claim['REPORTDT'] }}
                            </td> <td>
                                {{  $claim['SURVEYORNAME'] }}
                            </td> <td>
                                {{  $claim['SURVEYORADDRESS'] }}
                            </td> <td>
                                {{  $claim['TELNO'] }}
                            </td><td>
                                {{  $claim['HASREPORTSUBMITTED'] }}
                            </td><td>
                                {{  $claim['USERNAME'] }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
