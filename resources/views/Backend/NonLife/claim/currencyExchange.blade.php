@extends('layouts.backend.containerlist')

@section('title')
    Foreign Currency Exchange
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="box-body">
            <table id="-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Country Code</th>
                </tr>
                </thead>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
