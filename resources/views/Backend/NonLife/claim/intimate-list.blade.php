@extends('layouts.backend.containerlist')

@section('title')
    Claim Intimation List
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary m-3" href="{{route('nonLife.calculator.claim.kyc')}}">Go Back</a>
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <table id="policy-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Policy No.</th>
                    <th>User ID</th>
                    <th>Name</th>
                    <th>Contact No.</th>
                    <th>Vehicle No.</th>
                    <th>Province ID</th>
                    <th>District ID</th>
                    <th>MNU/VDC</th>
                    <th>Ward No.</th>
                    <th>Address</th>
                    <th>Remarks</th>
                    <th>Date of Loss</th>
                    <th>Intimation Date</th>
                    <th>Intimation File</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                        <tr class="gradeX" id="row_{{ $claim['ID'] }}">
                            <td>
                                <b># {{  $claim['ID'] }}</b>
                            </td>
                            <td>
                                {{  $claim['PolicyNo'] }}
                            </td>
                            <td>
                                {{  $claim['UserId'] }}
                            </td> <td>
                                {{  $claim['Name'] }}
                            </td> <td>
                                {{  $claim['ContactNo'] }}
                            </td> <td>
                                {{  $claim['VehicleNo'] }}
                            </td><td>
                                {{  $claim['ProvinceId'] }}
                            </td><td>
                                {{  $claim['DistrictId'] }}
                            </td><td>
                                {{  $claim['MNUVDC'] }}
                            </td><td>
                                {{  $claim['WardNo'] }}
                            </td><td>
                                {{  $claim['Address'] }}
                            </td><td>
                                {{  $claim['Remarks'] }}
                            </td><td>
                                {{  $claim['DateOfLoss'] }}
                            </td><td>
                                {{  $claim['IntimationDate'] }}
                            </td>
                            <td>
                                <a class="btn btn-primary btn-sm mb-3" type="submit" href="{{route('nonLife.calculator.claim.intimate')}}">Intimate Claim</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
