@extends('layouts.backend.containerlist')

@section('title')
    Claim By KYC ID
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
@endsection
@section('dynamicdata')

    <div class="box">
        <div class="box-body">
            <table id="-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Policy Issue Date</th>
                    <th>Policy No.</th>
                    <th>Document No.</th>
                    <th>Effective Date</th>
                    <th>Expiry Date</th>
                    <th>VAT Invoice No.</th>
                    <th>Credit Code No.</th>
                    <th>Insured Name</th>
                    <th>Insured Address</th>
                    <th>Sum Insured</th>
                    <th>Net Premium</th>
                    <th>Stamp Duty</th>
                    <th>VAT Rate</th>
                    <th>VAT Amount</th>
                    <th>Total Payable Premium</th>
                    <th>Document Name</th>
                    <th>E-Client Name</th>
                    <th>Endrse Desc</th>
                    <th>E-Vehicle No.</th>
                    <th>Engine No.</th>
                    <th>Chasis No.</th>
                    <th>Collection Date</th>
                    <th>Fiscal Year</th>
                    <th>Department Name</th>
                    <th>Category Name</th>
                    <th>Care Of </th>
                    <th>KYC No.</th>
                    <th>Gov. Anudan</th>
                    <th>PA Sum Insured</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                    @foreach($data as $index=>$claim)
                        <tr class="gradeX">
                            <td>
                                {{  $claim['PolicyIssueDate'] }}
                            </td>
                            <td>
                                {{  $claim['PolicyNo'] }}
                            </td>
                            <td>
                                {{  $claim['DocumentNo'] }}
                            </td>
                            <td>
                                {{ $claim['EffectiveDate'] }}
                            </td>
                            <td>
                                {{ $claim['ExpiryDate'] }}
                            </td>
                            <td>
                                {{ $claim['VatInvNo'] }}
                            </td>
                            <td>
                                {{ $claim['CreditNoteNo'] }}
                            </td>
                            <td>
                                {{ $claim['InsuredName'] }}
                            </td>
                            <td>
                                {{ $claim['InsuredAddress'] }}
                            </td>
                            <td>
                                {{ $claim['Suminsured'] }}
                            </td>
                            <td>
                                {{ $claim['NetPremium'] }}
                            </td>
                            <td>
                                {{ $claim['StampDuty'] }}
                            </td>
                            <td>
                                {{ $claim['VatRate'] }}
                            </td>
                            <td>
                                {{ $claim['VatAmount'] }}
                            </td>
                            <td>
                                {{ $claim['TotalPayablePremium'] }}
                            </td>
                            <td>
                                {{ $claim['DocName'] }}
                            </td>
                            <td>
                                {{ $claim['ECLIENTNAME'] }}
                            </td>
                            <td>
                                {{ $claim['EClientAddress'] }}
                            </td>
                            <td>
                                {{ $claim['endrseDesc'] }}
                            </td>
                            <td>
                                {{ $claim['EVehicleNo'] }}
                            </td>
                            <td>
                                {{ $claim['EngineNo'] }}
                            </td>
                            <td>
                                {{ $claim['ChasisNo'] }}
                            </td>
                            <td>
                                {{ $claim['ReceiptNo'] }}
                            </td>
                            <td>
                                {{ $claim['collectiondt'] }}
                            </td>
                            <td>
                                {{ $claim['fiscalyear'] }}
                            </td>
                            <td>
                                {{ $claim['DEPTNAME'] }}
                            </td>
                            <td>
                                {{ $claim['CategoryName'] }}
                            </td>
                            <td>
                                {{ $claim['CAREOF_ENG'] }}
                            </td>
                            <td>
                                {{ $claim['govanudan'] }}
                            </td>
                            <td>
                                {{ $claim['PASuminsured'] }}
                            </td>



                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
