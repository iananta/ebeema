@extends('layouts.backend.containerlist')

@section('title')
    Policies
@endsection
@section('dynamicdata')
 @include('layouts.backend.alert')
    <div class="box">
        <div class="box-body">
             <div class="filter-block">
                <div class="filter-wrapper">
                    <div class="filter-heading">
                        <h1>Filter</h1>
                    </div>
                    <div class="filter-form-wrapper">
                        <div class="form-filter-item">
                             <label>From</label>
                            <input id="from" name="from" type="date" class="filter-input">
                        </div>
                        <div class="form-filter-item">
                            <label>To</label>
                            <input name="to" id="to" type="date" class="filter-input">
                        </div>
                        <div class="form-filter-item filter-button-wrapper">
                            <button  class="filter-submit datafilter">Filter</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! $dataTable->table() !!}
        </div>
    </div>
@endsection
@section('footer_js')
    {!! $dataTable->scripts() !!}
    <script type="text/javascript">
               function sendStatusToServer(id) {
                var p_id = id;
                var status_value = document.getElementById("statusValue").value;
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: "{{ route('nonLife.calculator.motor.policy.status.change') }}",
                    data: {
                        id: p_id, status: status_value,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (response) {
                            // console.log(response);
                            if (response.status == "success") {
                               new swal({
                                icon: "success",
                                text: response.message,
                            });
                           } else {
                               new swal({
                                icon: "error",
                                text: response.message,
                            });
                                // console.log(response);
                            }
                        }
                    });

            }
            $('.datafilter').on('click',function(e){
                e.preventDefault()
                let from=$('#from').val()
                let to=$('#to').val()
                $("#customerPolicydataTableTable").on('preXhr.dt', function (e, settings, data) {
                    data.from =from;
                    data.to=to;
                }).data({
                    "serverSide": true,
                    "processing": true,
                    "ajax": {
                        url: "{{route('nonLife.calculator.policy.view')}}",
                        type: "GET"
                    }
                });
                LaravelDataTables.customerPolicydataTableTable.ajax.reload()
            })
            $(document).on('change','.excel_customizer',function(){
                let value=$(this).val();
                   $("#customerPolicydataTableTable").on('preXhr.dt', function (e, settings, data) {
                    data.excelCustomizer =value;
                    }).data({
                        "serverSide": true,
                        "processing": true,
                        "ajax": {
                            url: "{{route('nonLife.calculator.policy.view')}}",
                            type: "GET"
                        }
                    });
                    LaravelDataTables.customerPolicydataTableTable.ajax.reload()

            })
    </script>
@endsection