@extends('layouts.backend.containerlist')

@section('title')
    Renew Policy
@endsection
@section('footer_js')
    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
    <script type="text/javascript">
        $('#viewBtn').on('click', function (e) {
            e.preventDefault();
            var phnNo = $('#verifyPhnNo').val();
            var realNo = $('#number').val();
            if (phnNo == realNo) {
                window.location = this.href;
                // $("#makeModalClose").trigger("click");
                // $('#tablebody').removeClass('d-none');
            } else {
                new swal({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error',
                    html: 'Phone number not matched / Enter Phone Number',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                });
            }
        });
    </script>
@endsection


@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')
            <div class="justify-content-end list-group list-group-horizontal ">

            </div>
            <br>
            <div class="blank-page">
                <form class="renewal-from" id="renewalSearch">
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    <p class="text-bold">Please Enter Any Among Three Inputs Below</p>
                    <br>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Vehicle Type</div>
                                </div>
                                <select class="form-control" id="cat_id" name="cat_id">
                                    <option selected disabled>Select</option>
                                    <option value="21"
                                            @if(isset($_GET['cat_id']) && $_GET['cat_id'] == "21") selected @endif>
                                        Motorcycle
                                    </option>
                                    <option value="23"
                                            @if(isset($_GET['cat_id']) && $_GET['cat_id'] == "23") selected @endif>
                                        Private vehicle
                                    </option>
                                    <option value="22"
                                            @if(isset($_GET['cat_id']) && $_GET['cat_id'] == "22") selected @endif>
                                        Commercial Vehicle
                                    </option>
                                </select>
                            </div>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Province</div>
                                </div>
                                <select class="form-control" id="PROVINCECODE" name="PROVINCECODE">
                                    <option selected disabled>Select</option>
                                    <option value="BA"
                                            @if(isset($_GET['PROVINCECODE']) && $_GET['PROVINCECODE'] == "BA") selected @endif>
                                        BA(Bagmati)
                                    </option>
                                    <option value="RA"
                                            @if(isset($_GET['PROVINCECODE']) && $_GET['PROVINCECODE'] == "RA") selected @endif>
                                        RA(Rapti)
                                    </option>
                                    <option value="ME"
                                            @if(isset($_GET['PROVINCECODE']) && $_GET['PROVINCECODE'] == "ME") selected @endif>
                                        ME(Mechi)
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Lot Number</div>
                                </div>
                                <input type="text" class="form-control text-uppercase" id="motorNumber"
                                       placeholder="EG: 44"
                                       name="VEHICLELOTNO" value="{{ $_GET['VEHICLELOTNO']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Running Number</div>
                                </div>
                                <input type="number" class="form-control text-uppercase" id="motorRunningNumber"
                                       placeholder="EG: 1234"
                                       name="VEHICLERUNNINGNO" value="{{ $_GET['VEHICLERUNNINGNO']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <p class="text-bold">OR,</p>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Policy Number</div>
                                </div>
                                <input type="text" class="form-control text-uppercase" id="POLICYNO"
                                       placeholder="EG: BTM/MC-TP/F/00172/077/078"
                                       name="POLICYNO" value="{{ $_GET['POLICYNO']??'' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <p class="text-bold">OR,</p>
                            <div class="input-group mb-2 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Phone Number</div>
                                </div>
                                <input type="text" class="form-control" id="PHONENO"
                                       placeholder="Enter Phone Number"
                                       name="PHONENO" value="{{ $_GET['PHONENO']??'' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary mb-2">Search</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="result-show container-fluid table-wrapper-scroll-y my-custom-scrollbar"
                     id="calculationResult">
                    <table class="table table-bordered table-striped mb-0">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Document Number</th>
                            <th scope="col">Insured Name</th>
                            <th scope="col">Address</th>
                            <th scope="col">Vehicle Number</th>
                            <th scope="col">Issued Date</th>
                            <th scope="col">Effective Date</th>
                            <th scope="col">Expiry Date</th>
                            <th scope="col">Remaining Days</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody class="scroll-view">
                        @if(isset($documents) && count($documents))
                            @foreach($documents as $doc)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$doc->DOCUMENTNO}}</td>
                                    <td>{{$doc->INSUREDNAME}}</td>
                                    <td>{{$doc->ADDRESS}}</td>
                                    <td>{{$doc->EVEHICLENO}}</td>
                                    <td>{{$doc->ISSUEDDATE}}</td>
                                    <td>{{$doc->EFFECTIVE_DATE}}</td>
                                    <td>{{$doc->EXPIRYDATE}}</td>
                                    <td>{{$doc->REMAININGDAYS}}</td>
                                    <td>
                                        @php
                                            $number = base64_decode($doc->MOBILENO);
                                            $encNo = substr($number, 0, 2) . "*****" . substr($number, 7, 3);
                                        @endphp
                                        <input type="hidden" id="Documentno" value="{{$doc->DOCUMENTNO}}">
                                        <input type="hidden" id="number" value="{{$number}}">
                                        @if($number)
                                            {{$encNo.'/'.$number}}<br>
                                        @endif
                                    </td>
                                    <td><a href="#" class="btn btn-primary btn-sm" data-toggle="modal"
                                           data-target="#verifyMobileNumber{{$loop->iteration}}">View</a>
                                        <!-- Modal Start -->
                                        <div class="modal fade p-3" id="verifyMobileNumber{{$loop->iteration}}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="numberVerify">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content p-5">
                                                    <div
                                                        class="d-flex justify-content-center align-items-center container">
                                                        <div class="card py-5 px-3">
                                                            <h5 class="m-0">Mobile phone verification</h5><span
                                                                class="mobile-text">Enter the full mobile phone number to continue  <b
                                                                    class="text-danger">@if(isset($number)){{$encNo}}@endif</b></span>
                                                            <div class="mt-5 mb-5">
                                                                <input type="text" class="form-control"
                                                                       name="phone_number" id="verifyPhnNo"
                                                                       placeholder="EG:98........" required>
                                                            </div>
                                                            <a href="{{route('policy.renew.policy.details',$doc->DOCID)}}"
                                                               class="btn btn-primary c-btn-style c-primary-btn mb-3"
                                                               id="viewBtn">Submit</a>
                                                            <button type="button" id="makeModalClose"
                                                                    class="btn btn-outline-secondary c-btn-style c-ghost-btn"
                                                                    data-dismiss="modal">
                                                                Close
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="11">No Data Available</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    {{--    <h4 class="text-center mt-5"><strong>No Data Found</strong></h4>--}}
                    <hr>
                    {{--                    <table id="policy-table" class="table table-bordered table-hover">--}}
                    {{--                        <thead>--}}
                    {{--                        <tr>--}}
                    {{--                            <th>No.</th>--}}
                    {{--                            <th>Reference No.</th>--}}
                    {{--                            <th>Policy No.</th>--}}
                    {{--                            <th>Agent Details</th>--}}
                    {{--                            <th>Customer</th>--}}
                    {{--                            <th>Type</th>--}}
                    {{--                            <th>Sum Insured</th>--}}
                    {{--                            <th>Vehicle No</th>--}}
                    {{--                            <th>Paid Amount</th>--}}
                    {{--                            <th>Transaction Date</th>--}}
                    {{--                            --}}{{--   <th>Policy Details</th>--}}
                    {{--                            <th>Status</th>--}}
                    {{--                            <th>PDF Download</th>--}}
                    {{--                        </tr>--}}
                    {{--                        </thead>--}}
                    {{--                        <tbody id="tablebody" class="d-none">--}}
                    {{--                        <tr class="gradeX" id="row_1">--}}
                    {{--                            <td>--}}
                    {{--                                <b># 1</b>--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                reference number--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                policy number--}}
                    {{--                            </td>--}}

                    {{--                            <td>--}}
                    {{--                                <ul style="list-style: none">--}}
                    {{--                                    <li><b>Name:</b> agent username</li>--}}
                    {{--                                    <li><b>Contact:</b> agent phone number</li>--}}
                    {{--                                    <li><b>Email:</b> agent email</li>--}}
                    {{--                                </ul>--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                <ul style="list-style: none">--}}
                    {{--                                    <li><b>Name:</b> customer_name</li>--}}
                    {{--                                    <li><b>Contact:</b> 1234567890</li>--}}
                    {{--                                </ul>--}}

                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                CM--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                Rs. 1700--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                BA 00 PA 1234--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                Rs. 1700--}}
                    {{--                            </td>--}}
                    {{--                            <td>--}}
                    {{--                                9-20-2021--}}
                    {{--                            </td>--}}
                    {{--                            <td> Policy Active / Policy Expired</td>--}}
                    {{--                            <td>--}}
                    {{--                                <form action="#" method="GET"--}}
                    {{--                                      enctype="multipart/form-data">--}}
                    {{--                                    @csrf--}}
                    {{--                                    <input type="hidden" name="docid" value="1234">--}}
                    {{--                                    <input type="hidden" name="proformano" value="1234">--}}
                    {{--                                    <button class="btn btn-primary btn-sm" type="submit">Pdf Download</button>--}}
                    {{--                                </form>--}}
                    {{--                                --}}{{--                                        N/A--}}

                    {{--                                <a class="btn btn-secondary btn-sm mt-3 ml-2">Renew Policy</a>--}}
                    {{--                            </td>--}}
                    {{--                        </tr>--}}
                    {{--                        </tbody>--}}
                    {{--                    </table>--}}
                </div>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
