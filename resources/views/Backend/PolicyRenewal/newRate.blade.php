@extends('layouts.backend.containerform')

@section('title')
    Motor Premium Calculation- New Rates
@endsection

@section('header_css')
    <link rel="stylesheet" href="{{asset('backend/css/external.css')}}">
@endsection

@section('footer_js')
    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    @if(isset($output->data))
        <div class="box box-success">
            <h3>Calculated Result - {{Session::get('calculationFor') ? Session::get('calculationFor') : 'motor'}}
                calculation</h3>

            <div class="result-show container-fluid table-wrapper-scroll-y my-custom-scrollbar" id="calculationResult">
                <table class="table table-bordered table-striped mb-0">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Particulars</th>
                        <th scope="col">Rate</th>
                        <th scope="col">Amount</th>
                    </tr>
                    </thead>
                    <tbody class="scroll-view">
                    @foreach($output->data as $dta)
                        {{--                    @if ($loop->first || $loop->last)--}}
                        <tr>
                            <td>{{$dta->ROWNUMBER}}</td>
                            <td>{{$dta->RskDescription}}</td>
                            <td>{{$dta->Rate}}</td>
                            <td>RS. {{$dta->Amount}}</td>
                        </tr>
                        {{--                    @endif--}}
                    @endforeach
                    </tbody>
                </table>
                {{--    <h4 class="text-center mt-5"><strong>No Data Found</strong></h4>--}}
            </div>
            <hr>
{{--                {{dd($requestedDatas)}}--}}
            <form class="calc-from" id="calculationForm" action="#"
                  method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="BRANCHID" value="1">
                <input type="hidden" name="CLASSID" value="">
                <input type="hidden" name="DEPTID" value="2">
                <input type="hidden" name="HAS_TRAILOR" value="0">
                <input type="hidden" name="BASICPREMIUM_A" value="{{ $PremDetails->BASICPREMIUM ?? '0.00' }}">
                <input type="hidden" name="THIRDPARTYPREMIUM_B" value="{{ $PremDetails->THIRDPARTYPREMIUM ?? '0.00' }}">
                <input type="hidden" name="DRIVERPREMIUM_C" value="{{ $PremDetails->DRIVERPREMIUM ?? '0.00' }}">
                <input type="hidden" name="HELPERPREMIUM_D" value="{{ $PremDetails->HELPERPREMIUM ?? '0.00' }}">
                <input type="hidden" name="PASSENGERPREM_E" value="{{ $PremDetails->PASSENGERPREMIUM ?? '0.00' }}">
                <input type="hidden" name="RSDPREMIUM_F" value="{{ $PremDetails->POOLPREMIUM ?? '0.00' }}">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="VEHICLENO">Net Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Net Premium (Rs.)</div>
                            </div>
                            <input type="text" class="form-control"
                                   placeholder="Net Premium Amount"
                                   name="NETPREMIUM"
                                   value="{{ $PremDetails->NETPREMIUM ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6 d-none">
                        <label class="sr-only" for="thirdprty">Third party Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Third party Premium(Rs.)</div>
                            </div>
                            <input type="hidden" class="form-control"
                                   placeholder="Third party Premium"
                                   name="THIRDPARTYPREMIUM"
                                   value="{{ $PremDetails->THIRDPARTYPREMIUM ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="stmp">Stamp</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Stamp (Rs.)</div>
                            </div>
                            <input type="text" class="form-control"
                                   placeholder="Stamp"
                                   name="stamp"
                                   value="" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6 d-none">
                        <label class="sr-only" for="OTHERPREMIUM">Other Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Other Premium (Rs.)</div>
                            </div>
                            <input type="hidden" class="form-control" id="OTHERPREMIUM"
                                   placeholder="Other Premium"
                                   name="OTHERPREMIUM"
                                   value="{{ $PremDetails->OTHERPREMIUM ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="tvp">Total Vatable Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Total Vatable Premium (Rs.)</div>
                            </div>
                            <input type="text" class="form-control" id="tvp"
                                   placeholder="Total Vatable Premium"
                                   name="TOTALVATABLEPREMIUM"
                                   value="@php($totalVatable = $PremDetails->NETPREMIUM + $stamp){{ $totalVatable ?? '0.00' }}"
                                   readonly>
                        </div>
                    </div>
                    @php($vaTtotal = $totalVatable * 0.13)
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="inlineFormInputName2">VAT (%)</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">VAT (%)</div>
                            </div>
                            <input type="number" class="form-control" id="VAT"
                                   placeholder="VAT"
                                   name="VAT" value="{{ $requestedDatas->VAT??'13' }}" readonly>
                            <span class="p-2">Rs.</span><input type="text" class="form-control" id="VATAMT"
                                                               name="VATAMT"
                                                               value="{{ round($vaTtotal,2) ?? '0.00' }}" readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="anp">Total Net Premium</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend next-prepend">
                                <div class="input-group-text">Total Net Premium (Rs.)</div>
                            </div>
                            <input type="text" class="form-control" id="anp"
                                   placeholder="Actual Net Premium"
                                   name="TOTALNETPREMIUM"
                                   value="@php($totalNet = $totalVatable + round($vaTtotal,2)){{ $totalNet ?? '0.00' }}"
                                   readonly>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="VEHICLENO">Vehicle No.</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Vehicle No.</div>
                            </div>
                            <input type="text" class="form-control text-uppercase" id="VEHICLENO"
                                   placeholder="Please enter Vehicle Number"
                                   name="VEHICLENO" readonly
                                   value="{{ $requestedDatas->VEHICLENO??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="ENGINENO">Engine No.</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Engine No.</div>
                            </div>
                            <input type="text" class="form-control text-uppercase" id="ENGINENO"
                                   placeholder="Please enter Engine Number"
                                   name="ENGINENO" readonly
                                   value="{{ $requestedDatas->ENGINENO??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="CHASISNO">Chasis No.</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Chasis No.</div>
                            </div>
                            <input type="text" class="form-control text-uppercase" id="CHASISNO"
                                   placeholder="Please enter Chasis Number"
                                   name="CHASISNO" readonly
                                   value="{{ $requestedDatas->CHASISNO??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="MODEUSE">Mode Of Use</label>
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Mode Of Use</div>
                            </div>
                            <input type="text" class="form-control" id="MODEUSE"
                                   placeholder="Please enter Mode Of Use"
                                   name="MODEUSE" readonly
                                   value="{{ $requestedDatas->MODEUSE??'' }}">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label class="sr-only" for="RegistrationDate">Registration Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend date">
                                <div class="input-group-text">Registration Date</div>
                            </div>
{{--                            <input type="text" class="form-control nepali-datepicker" name="NEPALIREGISTRATIONDATE" placeholder="Select Nepali Date">--}}
                            <input class="form-control" type="text" id="english-date"  name="REGISTRATIONDATE" placeholder="Shows English Date" value="{{ $requestedDatas->REGDATE??'' }}" readonly/>
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
{{--                    <div class="form-group col-md-6">--}}
{{--                        <small class="help-block">* File Must Be In Image Format. Insert Image of size less than 2 MB</small>--}}
{{--                        <div class="custom-file">--}}
{{--                            <input type="file" class="custom-file-input" name="bluebook_images" id="bluebook_image"--}}
{{--                                  >--}}
{{--                            <label class="custom-file-label" for="bluebook_images">Browse Blue Book Image</label>--}}
{{--                            <span class="custom-file-image" for="images_view"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-group col-md-6">--}}
{{--                        <small class="help-block">* File Must Be In Image Format. Insert Image of size less than 2 MB</small>--}}
{{--                        <div class="custom-file">--}}
{{--                            <input type="file" class="custom-file-input" name="bike_images" id="bike_image">--}}
{{--                            <label class="custom-file-label" for="bike_images">Browse Motor Image</label>--}}
{{--                            <span class="custom-file-image" for="images_view"></span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary">Proceed To Payment</button>
                    </div>
                </div>
            </form>
            <!-- /.box -->
        </div>
    @else
        <div class="text-center">
            <h3>Calculated Result - Error in calculation</h3>
            <br>
            <br>
            <a href="{{route('nonLife.calculator.bike')}}" class="btn btn-primary fa fa-arrow-left"> Go
                Back</a>
        </div>
    @endif
    <!-- /.col (right) -->
    <!-- /.row -->
@stop
