@extends('layouts.backend.containerlist')

@section('title')
    Renew Policy Details
@endsection
@section('footer_js')

@endsection


@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')
            <div class="blank-page">
                <div class="result-show container-fluid table-wrapper-scroll-y my-custom-scrollbar"
                     id="calculationResult">
                    {{--    <h4 class="text-center mt-5"><strong>No Data Found</strong></h4>--}}
                    <hr>
                    <table id="policy-table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Category Name</th>
                            <th>Type Cover</th>
                            <th>Customer</th>
                            <th>Model</th>
                            <th>Year Of Manufacture</th>
                            <th>CC / HP</th>
                            <th>Sum Insured</th>
                            <th>EOD AMount</th>
                            <th>Engine Number</th>
                            <th>Chasis Number</th>
                            <th>No Claim Discount</th>
                            <th>Status</th>
                            <th>PDF Download</th>
                        </tr>
                        </thead>
                        <tbody id="tablebody">
                        @foreach($policyDetails as $policy)
                        <tr class="gradeX" id="row_1">
                            <td>
                                <b># {{$loop->iteration}}</b>
                            </td>
                            <td>{{$policy->CATEGORYNAME}}</td>
                            <td>{{$policy->TYPECOVER}}</td>
                            <td>
                                <ul style="list-style: none">
                                    <li><b>Name:</b> {{$policy->KYCID}}</li>
                                    <li><b>Contact:</b> {{$policy->KYCNO}}</li>
                                </ul>

                            </td>
                            <td>{{$policy->MODEL}}</td>
                            <td>{{$policy->YEARMANUFACTURE}}</td>
                            <td>{{$policy->CCHP}}</td>
                            <td>{{$policy->SUMINSUREDAMOUNT}}</td>
                            <td>{{$policy->EODAMT}}</td>
                            <td>{{$policy->ENGINENO}}</td>
                            <td>{{$policy->CHASISNO}}</td>
                            <td>{{$policy->EODAMT}}</td>
                            <td>
                                <form action="{{route('policy.renew.policy.new.rate')}}" method="POST"
                                       enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="data" value="{{json_encode($policy)}}">
                                    <button class="btn btn-secondary btn-sm" type="submit">Renew Policy</button>
                                </form>
                            </td>
                            <td>
                                <form action="{{route('nonLife.calculator.pdf.load')}}" method="GET"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="docid" value="{{$docId}}">
                                    <input type="hidden" name="proformano" value="{{$policy->KYCID}}">
                                    <input type="hidden" name="data" value="{{json_encode($policy)}}">
                                    <button class="btn btn-primary btn-sm" type="submit">Pdf Download</button>
                                </form>
                                {{--                                        N/A--}}

                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
