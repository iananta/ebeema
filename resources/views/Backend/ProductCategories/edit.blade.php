@extends('layouts.backend.containerform')

@section('footer_js')

    <script type="text/javascript">
        $(document).ready(function () {

            $(document).ready(function () {
                $('#roleAddForm').formValidation({
                    framework: 'bootstrap',
                    excluded: ':disabled',
                    icon: {
                        valid: 'glyphicon glyphicon-ok',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        role: {
                            // validators: {
                            //   notEmpty: {
                            //     message: 'Role Name field is required.'
                            //   }
                            // }
                        }
                    },
                });
            });

            $('.check-module').on('change', function (e) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').find('.check').prop('checked', true);
                } else {
                    $(this).closest('tr').find('.check').prop('checked', false);
                }
            });

            $('.check').on('change', function (e) {
                var checked = $(this).closest('table').parent().parent().find('.check:checked').length;
                var total = $(this).closest('table').parent().parent().find('.check').length;

                if (checked == 0) {
                    $(this).closest('table').parent().parent().find('.check-module').prop('checked', false);
                } else {
                    $(this).closest('table').parent().parent().find('.check-module').prop('checked', true);
                }
            });

        });
    </script>
@endsection @section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Edit Category</h3>
        </div>
        <div class="box-body">
            @include('layouts.backend.alert')

            <form action="{{ route('admin.category.update', $category->id) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group">
                    <p class="card-inside-title">Category Name:</p>
                    <div class="form-line">
                        <input class="form-control form-control-inline input-medium" name="name" size="16" type="text"
                               placeholder="Category Name" value="{{ $category->name }}"/>
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Category Code:</p>
                    <div class="form-line">
                        <input class="form-control form-control-inline input-medium" name="category_code" size="16"
                               type="text" value="{{ $category->category_code }}"
                               placeholder="Category Code"/>
                    </div>
                </div>

                <div class="form-group">
                    <p class="card-inside-title">Minimum Age:
                        <span class="badge badge-secondary p-2">Minimum age for (Age, Proposer Age , Husband Age & Wife Age)</span>
                    </p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="min_age"
                            type="number"
                            placeholder="Minimum Age"
                            value="{{ $category->min_age }}"
                        />
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Maximum Age:
                        <span class="badge badge-secondary p-2">Maximum age for (Age, Proposer Age , Husband Age & Wife Age)</span>
                    </p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="max_age"
                            type="number"
                            placeholder="Maximum Age"
                            value="{{ $category->max_age }}"
                        />
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Child Age Range:
                        <span
                            class="badge badge-secondary p-2">Minimum age - Maximum age (Use - to separate age range)</span>
                    </p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="child_age_range"
                            type="text"
                            value="{{$category->child_age_range}}"
                            placeholder="Child Age Range Eg: 0-30"
                        />
                    </div>
                </div>
                <div class="form-group d-none">
                    <p class="card-inside-title">Proposer Age Range:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="proposer_age_range"
                            type="text"
                            value="{{$category->proposer_age_range}}"
                            placeholder="Proposer Age Range Eg: 20-60"
                        />
                    </div>
                </div>
                <div class="form-group d-none">
                    <p class="card-inside-title">Husband Age Range:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="husband_age_range"
                            type="text"
                            value="{{$category->husband_age_range}}"
                            placeholder="Husband Age Range Eg: 20-60"
                        />
                    </div>
                </div>
                <div class="form-group d-none">
                    <p class="card-inside-title">Wife Age Range:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="wife_age_range"
                            type="text"
                            value="{{$category->wife_age_range}}"
                            placeholder="Wife Age Range Eg: 20-60"
                        />
                    </div>
                </div>

                <div class="form-group">
                    <p class="card-inside-title">Minimum Term:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="min_term"
                            type="number"
                            placeholder="Minimum Term"
                            value="{{ $category->min_term }}"
                        />
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Maximum Term:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="max_term"
                            type="number"
                            placeholder="Maximum Term"
                            value="{{ $category->max_term }}"
                        />
                    </div>
                </div>

                <div class="form-group">
                    <p class="card-inside-title">Minimum Sum:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="min_sum"
                            type="number"
                            placeholder="Minimum Sum"
                            value="{{ $category->min_sum }}"
                        />
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Maximum Sum:</p>
                    <div class="form-line">
                        <input
                            class="form-control form-control-inline input-medium"
                            name="max_sum"
                            type="number"
                            placeholder="Maximum Sum"
                            value="{{ $category->max_sum }}"
                        />
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-info waves-effect">
                          <span>Submit
                            <i class="fa fa-check"></i>
                          </span>
                    </button>
                </div>

        </div>
    </div>
    <!-- #END# Basic Table -->
    </form>

    <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
    <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
