@extends('layouts.backend.containerlist')

@section('footer_js')

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    });

    $('#tablebody').on('click', '.change-status', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You are going to change the status!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
        }).then(function (response) {
            if(response.isConfirmed == true) {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin/category/status') }}" + "/" + id,
                    data: {
                        'id': id,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        new swal('Success', response.message, 'success');
                        if (response.cat.status == 1) {
                            $($object).children().removeClass('bg-red').html(
                                '<i class="fa fa-ban"></i> Not Active');
                            $($object).children().addClass('bg-green').html(
                                '<i class="fa fa-check"></i> Active');
                            $($object).attr('title', 'Deactivate');
                        } else {
                            $($object).children().removeClass('bg-green').html(
                                '<i class="fa fa-check"></i> Active');
                            $($object).children().addClass('bg-red').html(
                                '<i class="fa fa-ban"></i> Not Active');
                            $($object).attr('title', 'Activate');
                        }
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        });
    });
    $('#tablebody').on('click', '.delete-category', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (response) {
            if (response.isConfirmed == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/category/delete') }}" + "/" + id,
                    dataType: 'json',
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        new swal(
                            'Success',
                            response.message,
                            'success'
                        )
                        $object.parent().parent().remove()
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        }).catch(swal.noop);
    });
</script>
@endsection
@section('title')
    Products Categories
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-header with-border c-btn-right d-flex-row ">
        <div class="justify-content-end list-group list-group-horizontal m-3">
            <a href=" {{ route('admin.category.create') }} ">
                <button class="btn btn-primary c-primary-btn add-modal shadow-none mx-2" data-toggle="modal"
                        data-target="#addNewUserModal">
                    &nbsp; Add New &nbsp;
                </button>
            </a>
        </div>
    </div>

    <div class="box-body">
        <div class="dataTables_wrapper dt-bootstrap4">

            <div class="box-body">

                @include('layouts.backend.alert')
                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Category Name:</th>
                        <th>Minimum Age:</th>
                        <th>Maximum Age:</th>
                        <th>Age Range:</th>
                        <th>Minimum Term:</th>
                        <th>Maximum Term:</th>
                        <th>Minimum Sum:</th>
                        <th>Maximum Sum:</th>
                        <th>Status:</th>
                        <th width="220px">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">

                    @foreach($categories as $category)
                    <tr class="gradeX" id="row_{{ $category->id }}">
                        <td class="index">
                            {{ $category->id }}
                        </td>
                        <td class="name">
                            {{ $category->name }}
                        </td>
                        <td class="name">
                            {{ $category->min_age ? $category->min_age : 'No Limit' }}
                        </td>
                        <td class="name">
                            {{ $category->max_age ? $category->max_age : 'No Limit' }}
                        </td>
                        <td class="name">
                            @if($category->category_code == "children")
                            <b> Child:</b> <br> {{ $category->child_age_range ?? 'N/A' }}
                            <ul>
                                {{--
                                <li>Proposer: {{ $category->proposer_age_range ?? 'N/A' }}</li>
                                --}}
                                {{--
                                <li>Husband: {{ $category->husband_age_range ?? 'N/A' }}</li>
                                --}}
                                {{--
                                <li>Wife: {{ $category->wife_age_range ?? 'N/A' }}</li>
                                --}}
                            </ul>
                            @else
                            N/A
                            @endif
                        </td>
                        <td class="name">
                            {{ $category->min_term ? $category->min_term : 'No Limit' }}
                        </td>
                        <td class="name">
                            {{ $category->max_term ? $category->max_term : 'No Limit' }}
                        </td>
                        <td class="name">
                            {{ $category->min_sum ? $category->min_sum : 'No Limit' }}
                        </td>
                        <td class="name">
                            {{ $category->max_sum ? $category->max_sum : 'No Limit' }}
                        </td>
                        <td>
                            @if($category->status == 1)
                            <a href="javascript:;" class="change-status" title="Deactivate"
                               id="{{ $category->id }}">
                                <button type="button"
                                        class="btn btn-sm bg-green btn-circle waves-effect waves-circle waves-float">
                                    <i class="fa fa-check"></i> Active
                                </button>
                            </a>
                            @else
                            <a href="javascript:;" class="change-status" title="Activate"
                               id="{{ $category->id }}">
                                <button type="button"
                                        class="btn btn-sm bg-red btn-circle waves-effect waves-circle waves-float">
                                    <i class="fa fa-ban"></i> Not Active
                                </button>
                            </a>
                            @endif
                        </td>
                        <td class="justify-content-center action-buttons">
                            <a href="{{ route('admin.category.edit', $category->id) }}" id="{{ $category->id }}"
                               title="Edit ">
                                <button
                                    class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button>
                            </a>&nbsp;


                            <a href="javascript:;" title="Delete category" class="delete-category"
                               id="{{ $category->id }}">
                                <button type="button" class="btn btn-danger btn-flat"><i
                                        class="fa fa-trash"></i></button>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->

            <!-- /.box -->
        </div>
    </div>
</div>
@endsection
