@extends('layouts.backend.containerlist')

@section('title')
    Sms Sent
@endsection
@section('footer_js')
    <script>
        $(document).ready(function() {
            $('#SmsResponse').DataTable( {
                "scrollX": true,
                "ordering": false
            } );
        } );
    </script>
@endsection

@section('dynamicdata')

    <div class="box">

        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')
            <span class="badge badge-success p-2">
              <h5>  Sent Sms : {{text_limit($smsCount,20) ?? "N/A"}}</h5>
            </span>
            <hr>
            <br>
            <div class="blank-page">
                <table class="table" id="SmsResponse">
                    <thead>
                    <tr>
                        <th scope="col">SN</th>
                        <th scope="col">Reference Number</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Message Sent</th>
                        <th scope="col">Response</th>
                        <th scope="col">Time</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($allSms as $sel)
                        <tr id="row_{{ $sel->id }}">
                            <th>#{{$loop->iteration}}</th>
                            <td>{{$sel->reference_number}}</td>
                            <td> {{$sel->customer->customer_name ?? ""}} </td>
                            <td><p> {{text_limit($sel->message,50)}} </p></td>
                            <td> {!! $sel->response !!} </td>
                            <td> {{ $sel->created_at->format('d-M-Y') }} </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
