@extends('layouts.backend.containerlist')
 <link rel="stylesheet" href="http://127.0.0.1:8000/backend/bower_components/select2/dist/css/select2.min.css">
@section('footer_js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script type="text/javascript">
        $(document).ready(function () {


            $('.check-module').on('change', function (e) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').find('.check').prop('checked', true);
                } else {
                    $(this).closest('tr').find('.check').prop('checked', false);
                }
            });

            $('.check').on('change', function (e) {
                var checked = $(this).closest('table').parent().parent().find('.check:checked').length;
                var total = $(this).closest('table').parent().parent().find('.check').length;

                if (checked == 0) {
                    $(this).closest('table').parent().parent().find('.check-module').prop('checked', false);
                } else {
                    $(this).closest('table').parent().parent().find('.check-module').prop('checked', true);
                }
            });

        });
        $(document).ready(function () {
            $('#multi-search').select2({
                multiple:true
            });
            loadProd();
        });

        function loadProd() {
            $(".prod-selected option:selected").each(function () {
                var thisOptionValue = $(this).val();
                var thisOptionText = $(this).text();
                var thisOptionSelect = $(this).data('compulsory') === 1 ? true : false;
                console.log(thisOptionSelect);
                // console.log(thisOptionValue);
                // console.log(thisOptionText);
            });
        };
        $(document).ready(function(){
            var e = document.getElementById("ddlViewBy");
            var strUser = e.options[e.selectedIndex].text;
        })

    </script>
@endsection

@section('title')
    Send SMS
@endsection

@section('dynamicdata')
    <div class="box">
        <div class="box-body">
            @include('layouts.backend.alert')
            <div class="policy-compare-wrapper">
                <div class="form-wrapper">
                    <form class="form-data" method="POST" action="{{ route('admin.smsSend.customer', $smsTemp->id) }}">
                        @csrf
                        <div class="custom-row">
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Title</label>
                                </div>
                                <div class="form-input">
                                    <input type="text" class="form-control" id="title" name="title"
                                           placeholder="Enter title of mail" value="{{ $smsTemp->title }}" />
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Message</label>
                                </div>
                                <div class="form-input">
                                <textarea
                                    class="form-control"
                                    name="message"
                                    id="message"
                                    placeholder="Message of the SMS"
                                    value="{!! $smsTemp->message !!} "
                                    
                                ></textarea>
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Customer</label>
                                </div>

                                <div class="form-input form-line">
                                    <select class="form-control form-control-inline input-medium multi-search prod-selected"
                                onchange="loadProd()" name="customers[]" id="multi-search" multiple="multiple">
                                        @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}-{{ $customer->phone}}">{{$customer->customer_name ?? 'N/A' }} {{$customer->phone ?? 'N/A'}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label"></div>
                                <div class="form-input">
                                    <input type="hidden" name="title" value="{{ $smsTemp->title }}">
                                    <input type="hidden" name="title" value="{{ $smsTemp->message }}">
                                    <button type="submit" id="submit-btn" class="form-submit-btn">Send</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

