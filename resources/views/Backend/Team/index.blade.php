@extends('layouts.backend.containerlist')

@section('title')
    Teams
@endsection
@section('footer_js')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script type="text/javascript">
        var oTable = $('#testimonialTable').dataTable();
        $('#tablebody').on('click', '.delete-item', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {

                    $.ajax({
                        type: "DELETE",
                        url: "{{ url('/admin/team') }}" + "/" + id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            oTable.fnDeleteRow(nRow);
                            new swal('success', response.message, 'success').catch(swal.noop);
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error').catch(swal.noop);
                        }
                    });
                }
            }).catch(swal.noop);
        });

        $('#tablebody').on('click', '.change-status', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You are going to change the status!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('/admin/team/status') }}" + "/" + id,
                        data: {
                            'id': id,
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            new swal('Success', response.message, 'success');
                            if (response.testimonial.status == 1) {
                                $($object).children().removeClass('bg-red').html(
                                    '<i class="fa fa-ban"></i>');
                                $($object).children().addClass('bg-blue').html(
                                    '<i class="fa fa-check"></i>');
                                $($object).attr('title', 'Deactivate');
                            } else {
                                $($object).children().removeClass('bg-blue').html(
                                    '<i class="fa fa-check"></i>');
                                $($object).children().addClass('bg-red').html(
                                    '<i class="fa fa-ban"></i>');
                                $($object).attr('title', 'Activate');
                            }
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });

        $(function () {
            $("#testimonialTable").DataTable();

            $( "#tablebody" ).sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {
                var team = [];
                $('tr.row1').each(function(index,element) {
                    team.push({
                        id: $(this).attr('data-id'),
                        position: index+1
                    });
                });
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ route('team.sort') }}",
                    data: {
                        teams:team,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            new swal({
                                text: response.value,
                                icon: "success",
                            });
                            // console.log(response);
                        } else {
                            new swal({
                                text: response.value,
                                icon: "error",
                            });
                            // console.log(response);
                        }
                    }
                });

            }
        });
    </script>
@endsection


@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')
            <div class="justify-content-end list-group list-group-horizontal ">
               <button type="button" class="btn btn-primary" data-toggle="modal"
                data-target="#addModal">
                Add Team
                </button>
                @include('Backend.Team.add')
            </div>
            <br>
            <div class="blank-page">
                <table class="table" id="testimonialTable">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Name</th>
                        <th scope="col">Designation</th>
                        <th scope="col">Phone No.</th>
                        <th scope="col">Email</th>
                        <th scope="col">Position</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($teams as $testimonial)
                        <tr class="row1" data-id="{{ $testimonial->id }}">

                            <th scope="row">{{$loop->iteration}}</th>
                            <td>
                                <div class="col-md">
                                    <div class="gallery-img">
                                        <a target="_blank" href="{{$testimonial['image']}}"
                                           class="swipebox" title="{{$testimonial['name']}}">
                                            <img class="img-responsive"
                                                 src="{{asset($testimonial->image)}}" alt="">
                                            <span class="zoom-icon"> </span> </a>

                                    </div>
                                </div>
                            </td>
                            <td>
                                {{$testimonial['name']}}
                                <br>

                            </td>
                            <td>{{$testimonial['designation']}}</td>
                            <td>{{$testimonial['phone']}}</td>
                            <td>{{$testimonial['email']}}</td>
                            <td>{{$testimonial['position']}}</td>
                            <td class="justify-content-center">
                                <button data-toggle="modal" data-target="#editModal{{$testimonial['id']}}"
                                        class="btn btn-primary btn-flat">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <a href="javascript:;" title="Delete" class="delete-item" id="{{ $testimonial->id }}">
                                    <button type="button" class="btn btn-danger btn-flat">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>
                                <a href="javascript:;" class="change-status ml-3"
                                   title="{{ $testimonial->status == 0 ? "Click to activate Team." : "Click to De-active Team." }}"
                                   id="{{ $testimonial->id }}">
                                    @if($testimonial->status == 1)
                                        <button type="button"
                                                class="btn bg-blue btn-primary btn-flat ">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    @else
                                        <button type="button"
                                                class="btn bg-red btn-primary btn-flat">
                                            <i class="fa fa-ban"></i>
                                        </button>
                                    @endif
                                </a>
                            </td>
                            <div class="modal fade" id="editModal{{$testimonial['id']}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel"
                                 aria-hidden="true" style="display: none;">
                                @include('Backend.Team.edit')
                            </div>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('footer_js')
@endsection
