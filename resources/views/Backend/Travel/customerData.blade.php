<a class="btn btn-danger mb-3 float-right" onclick="return confirm('Do you want to erase all !')"
   href="{{route('travel.calculator.customer.clear.all')}}">Clear All Data</a>
<table id="travel-users" class="table table-bordered table-striped mb-0">
    <thead>
    <tr>
        <th scope="col">SN</th>
        <th scope="col">Passport No.</th>
        <th scope="col">DOB</th>
        <th scope="col">From</th>
        <th scope="col">To</th>
        <th scope="col">Place Of Visit</th>
        <th scope="col">Contact No.</th>
        {{--        <th scope="col">Occupation</th>--}}
        <th scope="col">Premium</th>
        <th scope="col">Rate</th>
        <th scope="col">Cov. Load Charge</th>
        <th scope="col">Rs(on NPR)</th>
        {{--        <th scope="col">Relation</th>--}}
        <th scope="col">Is Dependent</th>
        <th scope="col">Remarks</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody class="scroll-view" id="travel-body">
    @if(isset($customerData))
        @foreach($customerData as $dta)
            <tr class="gradeX" id="row_{{ $dta->id }}">
                <td>{{$loop->iteration}}</td>
                <td>{{$dta->PASSPORTNO}}</td>
                <td>{{$dta->DOB}}</td>
                <td>{{$dta->DTFROM}}</td>
                <td>{{$dta->DTTO}}</td>
                <td>{{$dta->VISITPLACE != null && is_array($dta->VISITPLACE) ? implode(', ', json_decode($dta->VISITPLACE)) : $dta->VISITPLACE}}</td>
                <td>{{$dta->CONTACTNO}}</td>
                {{--                <td>{{$dta->OCCUPATION}}</td>--}}
                <td>{{$dta->TOTALDOLLARPREMIUM}}</td>
                <td>{{$dta->DOLLARRATE}}</td>
                <td>{{$dta->COVIDCHARGEPREMIUM}}</td>
                <td>{{$dta->TOTALNC}}</td>
                {{--                <td>{{$dta->RELATION}}</td>--}}
                <td>{{$dta->ISDEPENDENT == 1 ? 'YES' : 'NO'}}</td>
                <td>{{$dta->REMARKS}}</td>
                <td>
                    <a class="delete-customer" id="{{ $dta->id }}" title="delete customer">
                        <button type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float">
                            <i class="fa fa-trash"></i>
                        </button>
                    </a>&nbsp;
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<hr>
<form class="calc-from mt-5" id="calculationForm"
      action="{{route('travel.calculator.customer.select')}}" method="POST">
    @csrf
    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
    <input type="hidden" id="BRANCHCODE" name="BRANCHID" value="1">
    <input type="hidden" id="DEPARTMENTCODE" name="DEPTID" value="2">
    <input type="hidden" id="CLASSCODE" name="CLASSID" value="62">
    <div class="form-row">
        @php
            $totalNetPremium = $customerData ? $customerData->sum('TOTALNC') : '0';
            $stamp = $totalNetPremium < 100000 ? 10 : 20;
            $totalVatable = $totalNetPremium + $stamp;
            $vatAmt = $totalVatable * 0.13;
            $totalPrem = $totalVatable + $vatAmt;
        @endphp
        <div class="form-group col-md-6">
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Net Premium</div>
                </div>
                <input type="text" class="form-control bck-wht" id="totalnetprem"
                       placeholder="Net premium" value="{{round($totalNetPremium,2)}}"
                       name="TOTALNETPREMIUM" readonly>
            </div>
        </div>

        <div class="form-group col-md-6">
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Stamp</div>
                </div>
                <input type="text" class="form-control bck-wht" id="stamp"
                       placeholder="Stamp Amount" value="{{$stamp}}"
                       name="STAMP" readonly>
            </div>
        </div>

        <div class="form-group col-md-6">
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Total Vatable Premium</div>
                </div>
                <input type="text" class="form-control bck-wht" id="TVPREM"
                       placeholder="Total vatable Amount"
                       name="TVPREM" value="{{round($totalVatable,2)}}" readonly>
            </div>
        </div>

        <div class="form-group col-md-6">
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">VAT</div>
                </div>
                <input type="text" class="form-control bck-wht" id="TVAT"
                       placeholder="Total vat"
                       name="TVAT" value="13%" readonly>
                <input type="text" class="form-control bck-wht" id="TVATAMT"
                       placeholder="Total vat amount" value="{{round($vatAmt,2)}}"
                       name="TVATAMT" readonly>
            </div>
        </div>

        <div class="form-group col-md-6">
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Total Premium</div>
                </div>
                <input type="text" class="form-control bck-wht" id="TOTALPREM"
                       placeholder="Total Premium Amount" value="{{round($totalPrem,2)}}"
                       name="TOTALPREM" readonly>
            </div>
        </div>

        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-primary mb-2 float-right">Next Step</button>
        </div>

    </div>
</form>
