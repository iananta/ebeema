@extends('layouts.backend.containerform')

@section('title')
    Travel Medical Insurance Customer Select
@endsection
<style>
    .calculation-nav li, .calculation-nav li a {
        padding: 20px 10px;
    }

    .calculation-nav li .active {
        color: #444;
        background: #f7f7f7;
    }

    .calc-from {
        padding: 50px 20px;
    }

    .input-group-prepend {
        width: 20%;
    }

    .next-prepend {
        width: 30%;
    }

    .input-group-text {
        width: 100%;
    }

    .border-attachment {
        border: 1px solid #ced4da;
        padding: 20px 0;
    }

    .make-border-text {
        position: relative;
        top: -32px;
        background: #ffff;
    }

    /*Customer Modal CSS*/

    .calculation-nav li,
    .calculation-nav li a {
        padding: 20px 10px;
    }

    .calculation-nav li .active {
        color: #444;
        background: #f7f7f7;
    }

    .calc-from {
        padding: 50px 20px;
    }

    .input-group-prepend {
        width: 30%;
    }

    .input-group-text {
        width: 100%;
    }

    .border-attachment {
        border: 1px solid #ced4da;
        padding: 20px 0;
    }

    .make-border-text {
        position: relative;
        top: -32px;
        background: #ffff;
    }

    #ndp-nepali-box {
        top: 100px !important;
        right: 55px !important;
        left: auto !important;
    }

    .custom-file {
        height: auto !important;
    }

    @media only screen and (max-width: 1025px) {
        .select-cust-modal {
            max-width: 95% !important;
        }

    }
</style>
@section('footer_js')
    <script src="{{asset('js/sweetalert2@11.js')}}"></script>
    <script>

        $(document).ready(function () {
            $('.search-select').select2();
            getDetails();
        });

        function getDetails() {
            var details = $('.search-select').find(':selected').data('details');
            // console.log(details);
            if (details) {
                $('#printCustomerDetails').empty().append('<ul id="custmrList"><li>Customer Name: ' + details.INSUREDNAME_ENG + '</li><li>Address: ' + details.ADDRESS + '</li><li>Citizenship No.: ' + details.CITIZENSHIPNO + '</li><li>Date Of Birth: ' + details.DATEOFBIRTH + '</li><li>Email: ' + details.EMAIL + '</li><li>Fathers Name: ' + details.FATHERNAME + '</li><li>Grandfather Name: ' + details.GRANDFATHERNAME + '</li><li>Grandmother Name: ' + details.GRANDMOTHERNAME + '</li><li>Mobile Number: ' + details.MOBILENO + '</li><li>Mothers Name: ' + details.MOTHERNAME + '</li><li>Temporary Address: ' + details.TEMPORARYADDRESS + '</li></ul>');
            }
        };

        $('#provinceId').on('change', function () {
            var provinceID = $(this).val();
            // console.log(provinceID);
            $.ajax({
                type: "POST",
                url: "{{ route('province.district') }}",
                data: {
                    provinceID: provinceID
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $('#districtId').empty().append($('<option>', {
                        text: "Select District",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#districtId').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION
                        }));
                    }

                    $('#ISSUE_DISTRICT_ID_LIST').empty().append($('<option>', {
                        text: "Select Issued District",
                        disabled: "disabled",
                        selected: ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#ISSUE_DISTRICT_ID_LIST').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION
                        }));
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });

        });
        $('#districtId').on('change', function () {
            var DistrictID = $(this).val();
            $.ajax({
                type: "POST",
                url: "{{ route('district.mnuvdc') }}",
                data: {
                    DistrictID: DistrictID
                },
                dataType: 'json',
                success: function (response) {
                    // console.log(response);
                    $('#MNUId').empty().append($('<option>', {
                        text: "Select Municipality ",
                        disabled: '',
                        selected: ''
                    }));
                    for (var i = 0; i < response.mnu.length; i++) {
                        $('#MNUId').append($('<option>', {
                            value: response.mnu[i].MNUCODE,
                            text: response.mnu[i].MNU
                        }));
                    }

                    $('#VDCId').empty().append($('<option>', {
                        text: "Select VDC ",
                        disabled: '',
                        selected: ''
                    }));
                    for (var j = 0; j < response.vdc.length; j++) {
                        $('#VDCId').append($('<option>', {
                            value: response.vdc[j].VDCCODE,
                            text: response.vdc[j].VDC
                        }));
                    }
                },
                error: function (e) {
                    console.log('something went wrong');
                }
            });

        });

        $('#AddKycCustomer').on('click', function (e) {
            e.preventDefault();
            var frm = $('#calculationForm');
            var formData = new FormData(frm[0]);
            $("#AddKycCustomer").prop('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{ route('user.kyc.entry') }}",
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#preloader').show();
                },
                success: function (response) {
                    $('#preloader').hide();
                    $('#calculationForm')[0].reset();
                    $("#AddKycCustomer").prop('disabled', false);
                    // console.log(response.message);
                    if (response.data) {
                        $("#makeModalClose").trigger("click");
                        $('#CustomerList').append($('<option>', {
                            value: response.data.customer_id,
                            text: response.data.INSUREDNAME_ENG + ' : ' + response.data.MOBILENO,
                            selected: true,
                        }));
                        $('#CustomerList').find(":selected").data('details', response.data);
                        $('#printCustomerDetails').empty().append('<div class="col-sm-6"><ul id="custmrList"><li>Customer Name: ' + response.data.INSUREDNAME_ENG + '|' + response.data.INSUREDNAME_NEP + '</li><li>Address: ' + response.data.ADDRESS + '</li><li>Citizenship No.: ' + response.data.CITIZENSHIPNO + '</li><li>Date Of Birth: ' + response.data.DATEOFBIRTH + '</li><li>Email: ' + response.data.EMAIL + '</li></ul></div><div class="col-sm-6"><ul id="custmrList"> <li>Fathers Name: ' + response.data.FATHERNAME + '</li><li>Grandfather Name: ' + response.data.GRANDFATHERNAME + '</li><li>Grandmother Name: ' + response.data.GRANDMOTHERNAME + '</li><li>Mobile Number: ' + response.data.MOBILENO + '</li><li>Mothers Name: ' + response.data.MOTHERNAME + '</li><li>Temporary Address: ' + response.data.TEMPORARYADDRESS + '</li></ul></div>');
                        $('.cst-detail').removeClass('d-none');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            html: response.message ?? "Customer Added Successfully",
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                        // swal("Congratulations!", response.message ?? "Customer Added Successfully", "success");
                    }
                    // console.log(response);
                },
                error: function (response) {
                    $('#preloader').hide();
                    $("#AddKycCustomer").prop('disabled', false);
                    var err = '';
                    if (response.responseJSON && response.responseJSON.errors) {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.errors)) {
                            err += '-' + value + '<br>';
                        }
                    } else {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.message)) {
                            err += '-' + value + '<br>';
                        }
                    }
                    // console.log(err);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: typeof msg != 'object' && msg !== null ? msg.message : 'Error',
                        html: err ?? 'Something went wrong',
                        showConfirmButton: false,
                        timer: 2500,
                        timerProgressBar: true,
                    });
                }
            });
        });
        window.onload = function () {
            $('.dob-datepicker').nepaliDatePicker({
                container: '#dateDisplay',
                ndpYear: true,
                ndpMonth: true,
                ndpEnglishInput: 'dob-english-date'
            });
            $('.issued-datepicker').nepaliDatePicker({
                container: '#issuedDateDisplay',
                ndpYear: true,
                ndpMonth: true,
                ndpEnglishInput: 'issued-date'
            });
        };
        $(".dob-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#dob-english-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
        $(".issued-datepicker").on("input", function (e) {
            var nepDate = $(this).val();
            var Dt = NepaliFunctions.ConvertToDateObject(nepDate, "YYYY-MM-DD");
            var EngDate = NepaliFunctions.BS2AD(Dt);
            $('#issued-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
            // console.log(EngDate);
        });
    </script>
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="result-show container">
            <h3>Select Customer - TMI Policy</h3>
            <br>
            @include('layouts.backend.alert')
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <div style="text-align:center;border: 1px solid #000000;padding: 20px 0">
                        <h3 class="text-bold">Select Customer</span></h3>
                        <hr>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                data-target="#AddCustomer">
                            Add New Customer
                        </button>
                        <p class="mt-2">OR</p>
                        <hr>

                        @if(isset($makePolicy))
                            <form action="{{route('travel.calculator.insurance.payment')}}"
                                  id="selectCustomerForm" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="" for="userSelect">Select Customer</label>
                                    <div class="col-sm-12 mx-auto">
                                        <select class="form-control search-select" onchange="getDetails()"
                                                name="customer_id" id="CustomerList"
                                                required>
                                            <option hidden="" disabled="disabled" selected="selected" value="">Click
                                                Here To Select
                                                Customer
                                            </option>
                                            @foreach($customers as $user)
                                                <option data-details="{{json_encode($user)}}"
                                                        value="{{$user->customer_id}}" @if(isset($formData['customer_id'])){{($formData['customer_id'] == $user->customer_id) ? "selected" : ''}}@endif >{{$user->INSUREDNAME_ENG .' : '.$user->MOBILENO}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button class="btn btn-primary selectCstmer" type="submit">Proceed To Payment</button>
                            </form>
                        @endif
                    </div>
                    <a href="{{route('travel.calculator.insurance.index')}}"
                       class="mt-2 btn btn-primary fa fa-arrow-left"> Back</a>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <div style="text-align:left;border: 1px solid #000000;padding: 20px 5px;margin-bottom:10px">
                        <h3 class="text-bold">Selected Customer Details</span></h3>
                        <hr>
                        <div id="printCustomerDetails"><p>N/A</p></div>
                    </div>
                    <div style="text-align:left;border: 1px solid #000000;padding: 20px 5px">
                        <h3 class="text-bold">Policy Details</span></h3>
                        <hr>
                        <ul>
                            @if($formData)
                                @foreach($formData as $key => $dt)
                                    @if($dt)
                                        <li>{{$key." : ".$dt}}</li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>

                <!-- add user Modal -->
                @include('Backend.NonLife.Partials.add-new-customer')
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
