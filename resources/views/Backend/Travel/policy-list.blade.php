@extends('layouts.backend.containerlist')

@section('title')
    TMI Policies
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var oTable = $('#policy-table').dataTable({
                "ordering": false,
                "scrollX": true
            });
        });

        {{--function sendStatusToServer(id) {--}}
        {{--    var p_id = id;--}}
        {{--    var status_value = document.getElementById("statusValue").value;--}}
        {{--    $.ajax({--}}
        {{--        type: "post",--}}
        {{--        dataType: "json",--}}
        {{--        url: "{{ route('nonLife.calculator.motor.policy.status.change') }}",--}}
        {{--        data: {--}}
        {{--            id: p_id, status: status_value,--}}
        {{--            _token: '{{csrf_token()}}'--}}
        {{--        },--}}
        {{--        success: function (response) {--}}
        {{--            // console.log(response);--}}
        {{--            if (response.status == "success") {--}}
        {{--                swal({--}}
        {{--                    icon: "success",--}}
        {{--                    text: response.message,--}}
        {{--                });--}}
        {{--            } else {--}}
        {{--                swal({--}}
        {{--                    icon: "error",--}}
        {{--                    text: response.message,--}}
        {{--                });--}}
        {{--                // console.log(response);--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}

        {{--}--}}
    </script>
@endsection
@section('dynamicdata')


    <div class="box">
        <div class="text-right mr-3">
{{--            <a class="btn btn-primary btn-sm" href="{{route('nonLife.calculator.policy.view',['download'=>1])}}">Download--}}
{{--                Excel</a>--}}
{{--            <a class="btn btn-primary btn-sm" href="{{route('nonLife.calculator.all.document.upload')}}">Upload All Documents</a>--}}
        </div>
        @include('layouts.backend.alert')

        <div class="box-body">
            <form action="" class="p-3" method="get">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>From</label>
                        <input name="from_date" type="date" class="form-control" value="{{Request::get('from_date')}}">
                    </div>
                    <div class="col-auto">
                        <label>To</label>
                        <input name="to_date" type="date" class="form-control" value="{{Request::get('to_date')}}">
                    </div>
                    <div class="col-auto mt-4">
                    <button class="btn btn-primary btn-sm" type="submit">Filter</button>
                </div>
                </div>
            </form>
            <table id="policy-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Reference No.</th>
                    <th>Policy No.</th>
                    <th>Agent Details</th>
                    <th>Customer</th>
                    <th>Effective Date</th>
                    <th>Expiry Date</th>
                    <th>Paid Amount</th>
                    <th>Payment From</th>
                    <th>Status</th>
                    <th>Created Date</th>
                    <th>PDF Download</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                @foreach($policies as $index=>$policy)
                    <tr class="gradeX" id="row_{{ $policy->id }}">
                        <td>
                            <b># {{ $loop->iteration }}</b>
                        </td>
                        <td>
                            <b> {{ $policy->reference_number }}</b>
                        </td>
                        <td>{{$policy->policyNo ?? 'N/A'}}
                        </td>
                        <td>
                            <ul style="list-style: none">
                                <li><b>Name:</b> {{ $policy->agent->username ?? 'N/A'  }}</li>
                                <li><b>Contact:</b> {{ $policy->agent->phone_number ?? 'N/A'  }}</li>
                                <li><b>Email:</b> {{ $policy->agent->email ?? 'N/A'  }}</li>
                            </ul>
                        </td>
                        <td>
                            <ul style="list-style: none">
                                <li><b>Name:</b> {{ $policy->customer->customer_name ?? 'N/A' }}</li>
                                <li><b>Contact:</b> {{ $policy->customer->phone ?? 'N/A' }}
                                    <br> {{ $policy->customer->email ?? '' }}</li>
                            </ul>

                        </td>
                        <td>{{$policy->effectiveDate}}</td>
                        <td>{{$policy->expiryDate}}</td>
                        <td>
                            Rs. {{number_format($policy->tpPremium, 2, '.', ',')}}
                        </td>
                        <td>
                            {{ucfirst('imePay')}}
                        </td>
                        <td>@if($policy->status == 1) Payment Successful @elseif($policy->status == 2) Payment
                            Error @else Pending @endif
                        </td>
                        <td>
                            <b>{{ $policy->created_at }}</b>
                        </td>
                        <td>
                            @if($policy->AcceptanceNo)
                                <form action="{{route('travel.calculator.pdf.load')}}" method="GET"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="docid" value="{{$policy['AcceptanceNo']}}">
                                    <input type="hidden" name="proformano" value="{{$policy['proformano']}}">
                                    <button class="btn btn-primary" type="submit">Pdf Download</button>
                                </form>
                            @else
                                @if($policy->status == 1)
                                    {{--Payment Successful--}}
                                    N/A
                                    {{--<form action="{{(route('nonLife.calculator.make.draft.policy'))}}" method="POST">
                                        @csrf
                                        <label>{{$policy->output}}</label>
                                        <input type="hidden" name="reference_number" value="{{$policy->payment_ref_id}}">
                                        <input type="hidden" name="policy_id" value="{{$policy->id}}">
                                        <button type="submit" class="btn btn-secondary btn-sm">Make Policy</button>
                                    </form>--}}
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop
