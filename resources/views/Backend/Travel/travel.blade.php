@extends('layouts.backend.containerlist')

@section('title')
    Travel Medical Insurance Calculator
@endsection
@section('footer_js')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $('#coverType').on('change', function () {
            var CoverId = $("#coverType option:selected").val();

            $.ajax({
                type: "POST",
                url: "{{ route('travel.calculator.medical.insurance.plans') }}",
                data: {
                    CoverId: CoverId,
                },
                dataType: 'json',
                success: function (response) {
                    if (response.tmiPlans) {
                        $('#planArea').empty().append('<option disabled selected>Please Select</option>');
                        $('#PackageType').empty().append('<option disabled selected>Please Select</option>');
                        for (var i = 0; i < response.tmiPlans.length; i++) {
                            $('#planArea').append($('<option>', {
                                text: response.tmiPlans[i].name,
                                value: response.tmiPlans[i].id,
                                'data-code': response.tmiPlans[i].code
                            }));
                        }
                    }
                },
                error: function (e) {
                    swal.fire("Error!", "something went wrong. Please reload !", "error");
                    console.log('something went wrong');
                }
            });
        });
        $('#planArea').on('change', function () {
            var CoverId = $("#coverType option:selected").val();
            var PlanId = $("#planArea option:selected").val();

            $.ajax({
                type: "POST",
                url: "{{ route('travel.calculator.medical.insurance.package') }}",
                data: {
                    CoverId: CoverId,
                    PlanId: PlanId,
                },
                dataType: 'json',
                success: function (response) {
                    response = response.data;
                    if (response.tmiPackages) {
                        $('#PackageType').empty().append('<option disabled selected>Please Select</option>');
                        for (var i = 0; i < response.tmiPackages.length; i++) {
                            $('#PackageType').append($('<option>', {
                                text: response.tmiPackages[i].name,
                                value: response.tmiPackages[i].id,
                                'data-code': response.tmiPackages[i].code
                            }));
                        }
                    }
                    if (response.countries) {
                        $('#PLACEOFVISIT').empty().append('<option disabled>Please Select</option>');
                        for (var i = 0; i < response.countries.length; i++) {
                            $('#PLACEOFVISIT').append($('<option>', {
                                text: response.countries[i].name,
                                value: response.countries[i].name
                            }));
                        }
                    }
                },
                error: function (e) {
                    swal.fire("Error!", "something went wrong. Please reload !", "error");
                    console.log('something went wrong');
                }
            });
        });
        $('.calc-premium').on('input', function () {
            premiumCalculate();
        });

        function premiumCalculate() {
            // console.log('premium clac applied');
            var COVERTYPE = $("#coverType option:selected").text();
            var COVERTYPECODE = $("#coverType option:selected").data('code');
            var PLAN = $("#planArea option:selected").text();
            var PLANCODE = $("#planArea option:selected").data('code');
            var PACKAGE = $("#PackageType option:selected").text();
            var PACKAGECODE = $("#PackageType option:selected").data('code');
            var Age = $('#AGE').val();
            var Day = $('#Day').val();
            var ISANNUALTRIP = $('#ISANNUALTRIP').is(':checked') === true ? 1 : 0;

            $('#COVER_TYPE_CODE').val(COVERTYPECODE);
            $('#PLAN_CODE').val(PLANCODE);
            $('#PACKAGE_CODE').val(PACKAGECODE);

            if (COVERTYPE && PLAN && PACKAGE && Age >= 1 && Day >= 1) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('travel.calculator.medical.insurance.premium') }}",
                    data: {
                        COVERTYPE: COVERTYPE,
                        COVERTYPECODE: COVERTYPECODE,
                        PLAN: PLAN,
                        PLANCODE: PLANCODE,
                        PACKAGE: PACKAGE,
                        PACKAGECODE: PACKAGECODE,
                        Age: Age,
                        Day: Day,
                        ISANNUALTRIP: ISANNUALTRIP
                    },
                    dataType: 'json',
                    success: function (response) {
                        var result = response.tmiPackages;
                        if (result) {
                            $('#PREMIUM').val(result.PremDetails.premiuminUSD);
                            // console.log(response.tmiPackages);
                            return rateCalculate();
                        }
                    },
                    error: function (e) {
                        swal.fire("Error!", "something went wrong. Please reload !", "error");
                        console.log('something went wrong');
                    }
                });
            }
        }

        $(document).ready(function () {
            $('.select2').select2();
            // $('.dob-datepicker').nepaliDatePicker({
            //     ndpYear: true,
            //     ndpMonth: true,
            //     ndpEnglishInput: 'english-dob',
            //     onChange: function (e) {
            //         // console.log(e);
            //         var EngDate = NepaliFunctions.BS2AD(e.object);
            //         var age = calculate_age(new Date(EngDate.year, EngDate.month, EngDate.day));
            //         if(age <= 69) {
            //             $('#AGE').val(age);
            //             premiumCalculate();
            //         }
            //         else{
            //             new swal("Error!", "something went wrong. Age restriction !", "error");
            //             $('.ndp-nepali-calendar').val('');
            //             $('#english-dob').val('');
            //             $('#AGE').val('');
            //         }
            //     }
            // });
            // $('.fromdate-datepicker').nepaliDatePicker({
            //     ndpYear: true,
            //     ndpMonth: true,
            //     ndpEnglishInput: 'from-date',
            //     onChange: function () {
            //         // alert('hello');
            //         countDays();
            //     }
            // });
            // $('.todate-datepicker').nepaliDatePicker({
            //     ndpYear: true,
            //     ndpMonth: true,
            //     ndpEnglishInput: 'to-date',
            //     onChange: function () {
            //         // alert('hello');
            //         countDays();
            //     }
            // });
            $("#english-dob").on("input", function () {
                var EngDate = $(this).val();
                var Dt = NepaliFunctions.ConvertToDateObject(EngDate, "YYYY-MM-DD");
                // var EngDate = NepaliFunctions.BS2AD(Dt);
                regex_two_digit = /^\d{2}$/;
                regex_four_digit = /^\d{4}$/;

                if (Dt.month && Dt.day && (regex_four_digit.test(Dt.year))) {
                    // $('#english-dob').val(NepaliFunctions.ConvertDateFormat(Dt));
                    // console.log(Dt);
                    var age = calculate_age(new Date(Dt.year, Dt.month, Dt.day));
                    if (age <= 69) {
                        $('#AGE').val(age);
                        premiumCalculate();
                    } else {
                        new swal("Error!", "something went wrong. Age restriction !", "error");
                        $('#AGE').val('');
                        $('#english-dob').val('');
                    }
                }
            });
            $("#from-date").on("input", function () {
                countDays();
            });
            $("#to-date").on("input", function () {
                countDays();
            });
            $("#Day").on("input", function () {
                var day = parseInt($(this).val());
                var frmDt = NepaliFunctions.ConvertToDateObject($('#from-date').val(), "YYYY-MM-DD");
                if (day > 0 && frmDt) {
                    var nepDate = NepaliFunctions.AD2BS(frmDt);
                    var toDt = NepaliFunctions.BsAddDays(nepDate, day - 1);
                    var EngDate = NepaliFunctions.BS2AD(toDt);
                    $('#to-date').val(NepaliFunctions.ConvertDateFormat(EngDate));
                }
            });
        });

        function calculate_age(dob) {
            var diff_ms = Date.now() - dob.getTime();
            var age_dt = new Date(diff_ms);
            return Math.abs(age_dt.getUTCFullYear() - 1970);
        }

        function countDays() {
            var dateFrom = NepaliFunctions.ConvertToDateObject($('#from-date').val(), "YYYY-MM-DD");
            var dateTo = NepaliFunctions.ConvertToDateObject($('#to-date').val(), "YYYY-MM-DD");
            if (dateFrom && dateTo) {
                var diffDate = NepaliFunctions.AdDatesDiff(dateFrom, dateTo);
            }
            $('#Day').val(++diffDate);
            premiumCalculate();
            // console.log(dateFrom);
            // console.log(dateTo);
            // console.log(diffDate);
        };
        $('.calc-prem').on('input', function () {
            rateCalculate();
        });

        function rateCalculate() {
            var premium = $('#PREMIUM').val();
            var Rate = $('#RATE').val();
            if (premium && Rate) {
                var nepPremium = (premium * Rate).toFixed(2);
            }
            $('#RSNPR').val(nepPremium);
            covidRateCalculate();
        }

        $('#AddMedicalInsurer').on('click', function (e) {
            e.preventDefault();
            $("#AddMedicalInsurer").prop('disabled', true);
            // $("#basic-form").validate();
            var frm = $('#calculationForm');
            var formData = new FormData(frm[0]);
            $.ajax({
                type: "POST",
                url: "{{ route('travel.calculator.insurance.store') }}",
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#preloader').show();
                },
                success: function (response) {
                    // console.log(response);
                    // $('#calculationForm')[0].reset();
                    $('#preloader').hide();
                    $("#AddMedicalInsurer").prop('disabled', false);
                    $('#TravelInsuranceResults').html(response);
                    swal.fire("Congratulations!", "User Added Successfully", "success");
                },
                error: function (response) {
                    $('#preloader').hide();
                    $("#AddMedicalInsurer").prop('disabled', false);
                    // console.log(JSON.parse(response.responseText));
                    swal.fire('Oops...', JSON.parse(response.responseText).message ?? "Something went wrong", 'question');
                }
            });
        });

        var oTable = $('#travel-users').dataTable({
            "paging": true,
            "searching": false,
            "ordering": false,
            "info": false
        });
        $('#travel-body').on('click', '.delete-customer', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ url('travel/calculator/insurance/customer/delete') }}" + "/" + id,
                    data: {
                        'id': id,
                    },
                    dataType: 'json',
                    success: function (response) {
                        var nRow = $($object).parents('tr')[0];
                        oTable.fnDeleteRow(nRow);
                        swal.fire('Success', response.message, 'success');
                    },
                    error: function (e) {
                        swal.fire('Oops...', 'Something went wrong!', 'error');
                    }
                });
            });
        });
        $('#includeCovidRate').on('change', function () {
            // alert("it is checked");
            covidRateCalculate();
        });

        function covidRateCalculate() {
            var rate = $("#RATE").val();
            var premium = $("#PREMIUM").val();
            if ($('#includeCovidRate').is(":checked")) {
                var covidAmt = premium * 0.2;
                var totalAmt = parseFloat(premium) + parseFloat(covidAmt);
                $('#COVIDLOADCHARGE').val((covidAmt).toFixed(2));
                $('#RSNPR').val((totalAmt * parseFloat(rate)).toFixed(2));
                // console.log(rate , premium , totalAmt);
            } else {
                // console.log(rate, premium);
                $('#RSNPR').val((parseFloat(premium) * parseFloat(rate)).toFixed(2));
                $('#COVIDLOADCHARGE').val(0);
            }
        }

        $('#PLACEOFVISIT').on('change', function (){
            $object = $(this);
            var country = $object.val();
            if(country != null && country != '') {
                $.ajax({
                    type: "POST",
                    url: "{{ url('travel/calculator/insurance/package/select') }}",
                    data: {
                        'country': country,
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#planArea').val(response.plan_id);
                    },
                    error: function (e) {
                        // console.log(e);
                        swal.fire('Oops...', 'Something went wrong  ,' + e.responseJSON.message, 'error');
                    }
                });
            }
        });
    </script>
@endsection
<style>
    .calculation-nav li, .calculation-nav li a {
        padding: 20px 10px;
    }

    .calculation-nav li .active {
        color: #444;
        background: #f7f7f7;
    }

    .calc-from {
        padding: 20px 20px;
    }

    .input-group-prepend {
        min-width: 20%;
        width: auto;
    }

    .input-group-text {
        width: 100%;
    }

    .check-title {
        width: 50%;
    }

    .radio-box {
        width: 15%;
    }

    .bck-wht {
        background-color: #FFFFFF !important;
    }

    span.select2-selection.select2-selection--single {
        height: auto;
    }
</style>
@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        {{--        <div class="box-header">--}}
        {{--            <h3 class="box-title">TMI (Travel Medical Insurance) Calculation </h3>--}}
        {{--        </div>--}}
        <div class="box-body">
            @include('layouts.backend.alert')
            <ul class="nav nav-tabs calculation-nav">
                <li><a class="active" data-toggle="tab" href="#firstParty">Travel Medical Insurance</a></li>
                {{--                <li><a data-toggle="tab" href="#thirdParty">Third Party</a></li>--}}
            </ul>
            <div class="tab-content">
                <div id="firstParty" class="tab-pane fade in active show">
                    <form class="calc-from" id="calculationForm"
                          action="{{route('travel.calculator.insurance.store')}}" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <input type="hidden" id="BRANCHCODE" name="BRANCHID" value="1">
                        <input type="hidden" id="DEPARTMENTCODE" name="DEPTID" value="2">
                        <input type="hidden" id="CLASSCODE" name="CLASSID" value="62">
                        <input type="hidden" id="COVER_TYPE_CODE" name="COVER_TYPE_CODE">
                        <input type="hidden" id="PLAN_CODE" name="PLAN_CODE">
                        <input type="hidden" id="PACKAGE_CODE" name="PACKAGE_CODE">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Cover Type</div>
                                    </div>
                                    <select class="form-control calc-premium" id="coverType" name="COVERTYPE" required>
                                        <option selected disabled>Please Select</option>
                                        @if(isset($coverTypes))
                                            @foreach ($coverTypes as $cat)
                                                <option value="{{ $cat->id }}"
                                                        data-code="{{$cat->code}}">{{ $cat->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Plan And Area</div>
                                    </div>
                                    <select class="form-control calc-premium" id="planArea" name="PLAN" required>
                                        <option selected disabled>Please Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Package Type</div>
                                    </div>
                                    <select class="form-control calc-premium" id="PackageType" name="PACKAGE"
                                            required>
                                        <option selected disabled>Please Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Place Of Visit</div>
                                    </div>
                                    <select name="VISITPLACE" id="PLACEOFVISIT" multiple class="form-control select2">
                                        <option disabled>Select Plan to get Country</option>
                                        @foreach($countries as $country)
                                            <option>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            {{--                            <div class="form-group col-md-6">--}}
                            {{--                                <div class="input-group-prepend check-title">--}}
                            {{--                                    <div class="input-group-text">Annual Multi Trip !</div>--}}
                            {{--                                    <div class="input-group-text radio-box">--}}
                            {{--                                        <input class="form-check calc-premium" style="height: 15px; width: 15px"--}}
                            {{--                                               aria-label="Checkbox If Its Annual Multi Trip"--}}
                            {{--                                               type="checkbox" value="1" id="ISANNUALTRIP" name="ISANNUALTRIP">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <div class="input-group-prepend date">
                                        <div class="input-group-text">Date Of Birth</div>
                                    </div>
                                    {{--                                    <input type="text" class="form-control dob-datepicker" name="NEPALIDATEOFBIRTH"--}}
                                    {{--                                           placeholder="Select Nepali Date" required>--}}
                                    <input class="form-control" type="date" id="english-dob" name="DOB"
                                           placeholder="Shows English Date"/>
                                    <input class="form-control calc-premium" type="text" name="AGE" id="AGE"
                                           placeholder="Age" readonly/>
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Passport No.</div>
                                    </div>
                                    <input type="text" class="form-control" id="PASSPORTNO"
                                           placeholder="Please enter passport number"
                                           name="PASSPORTNO" required>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="attachment_image"
                                           id="attachment_image"
                                           required>
                                    <label class="custom-file-label" for="attachment_images">Browse Attachment for
                                        Passport Image</label>
                                    <span class="custom-file-image" for="images_view"></span>
                                </div>
                                <small class="help-block">* File Must Be In Image Format. Insert Image of size less than
                                    2 MB</small>
                            </div>

                            {{--                            <div class="form-group col-md-6">--}}
                            {{--                                <div class="input-group mb-2 mr-sm-2">--}}
                            {{--                                    <div class="input-group-prepend">--}}
                            {{--                                        <div class="input-group-text">Occupation</div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <input type="text" class="form-control" id="OCCUPATION"--}}
                            {{--                                           placeholder="Please enter Occupation"--}}
                            {{--                                           name="OCCUPATION" required>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Contact Number</div>
                                    </div>
                                    <input type="text" class="form-control" id="contactNO"
                                           placeholder="Contact number"
                                           name="CONTACTNO">
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend date">
                                        <div class="input-group-text">From</div>
                                    </div>
                                    {{--                                    <input type="text" class="form-control fromdate-datepicker daysCount"--}}
                                    {{--                                           name="NEPALIFROM"--}}
                                    {{--                                           placeholder="Select Nepali Date" >--}}
                                    <input class="form-control" type="date" id="from-date" name="DTFROM"
                                           placeholder="Shows English Date" value="<?php echo date('Y-m-d'); ?>"/>
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend date">
                                        <div class="input-group-text">To</div>
                                    </div>
                                    {{--                                    <input type="text" class="form-control todate-datepicker daysCount" name="NEPALITO"--}}
                                    {{--                                           placeholder="Select Nepali Date" >--}}
                                    <input class="form-control" type="date" id="to-date" name="DTTO"
                                           placeholder="Shows English Date"/>
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">No Of Days</div>
                                    </div>
                                    <input type="number" class="form-control calc-premium" id="Day"
                                           placeholder="Number of stay days"
                                           name="PERIODOFINSURANCE" value="0">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Premium</div>
                                    </div>
                                    <select class="form-control" id="premium" name="CURRENCY" required>
                                        <option value="USD">USD ( $ )</option>
                                        {{-- <option>NPR</option> --}}
                                    </select>
                                    <div style="min-width: auto" class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">$</span>
                                    </div>
                                    <input type="text" class="form-control calc-prem" id="PREMIUM"
                                           placeholder="Premium" title="Dollar Premium"
                                           name="TOTALDOLLARPREMIUM" readonly>
                                    <div style="min-width: auto" class="input-group-prepend">
                                        <span class="input-group-text"
                                              id="basic-addon1">Per Dollar Rate Today (Rs.)</span>
                                    </div>
                                    <input type="text" class="form-control calc-prem" id="RATE"
                                           placeholder="Rate" value="{{$rates->data[0]->Buy ?? 0}}"
                                           name="DOLLARRATE" title="Dollar Rate" readonly>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend check-title">
                                        <div class="input-group-text">Include Covid Load Charge ( 20% )</div>
                                        <div class="input-group-text radio-box">
                                            <input class="form-check" style="height: 15px; width: 15px"
                                                   aria-label="Checkbox to include covid rate"
                                                   type="checkbox" value="1" id="includeCovidRate"
                                                   name="includeCovidRate">
                                        </div>
                                    </div>
                                    <input type="hidden" readonly class="form-control" id="COVIDRATE"
                                           placeholder="Covid Load Rate %"
                                           title="Covid Load Rate is 20 %"
                                           name="COVIDRATE" value="20">
                                    <div style="min-width: auto" class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">$</span>
                                    </div>
                                    <input type="text" readonly class="form-control" id="COVIDLOADCHARGE"
                                           placeholder="Covid Load Charge in USD" title="Covid Load Charge in USD"
                                           name="COVIDCHARGEPREMIUM">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Total Amount in Rs.</div>
                                    </div>
                                    <div style="min-width: auto" class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rs.</span>
                                    </div>
                                    <input type="text" class="form-control" id="RSNPR"
                                           placeholder="Rs In NPR"
                                           name="TOTALNC" readonly>
                                </div>
                            </div>

                            {{--                            <div class="form-group col-md-6">--}}
                            {{--                                <div class="input-group mb-2 mr-sm-2">--}}
                            {{--                                    <div class="input-group-prepend">--}}
                            {{--                                        <div class="input-group-text">Relation</div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <input type="text" class="form-control" id="RELATION"--}}
                            {{--                                           placeholder="Your Relation" name="RELATION">--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <div class="form-group col-md-6">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Is Dependent !</div>
                                    <div class="input-group-text radio-box">
                                        <input class="form-check" style="height: 15px; width: 15px"
                                               aria-label="Checkbox for dependent"
                                               type="checkbox" value="" id="ISDEPENDENT" name="ISDEPENDENT">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Remarks</div>
                                    </div>
                                    <textarea name="REMARKS" class="form-control" id="comment" rows="5"
                                              placeholder="Your Comment"></textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-6 d-none">
                                <div class="input-group-prepend check-title">
                                    <div class="input-group-text">Has Agent !</div>
                                    <div class="input-group-text radio-box">
                                        @if(Auth::user()->role_id == 5)
                                            <input type="hidden" value="1" id="HASAGENT" name="HASAGENT">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="reset" class="btn btn-danger mb-2 ml-2 submit float-right">CLEAR</button>
                                <button type="submit" id="AddMedicalInsurer"
                                        class="btn btn-primary mb-2 submit float-right">Save
                                </button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="result-show container-fluid table-wrapper-scroll-y my-custom-scrollbar"
                         id="TravelInsuranceResults">
                        @include('Backend.Travel.customerData')
                        {{--    <h4 class="text-center mt-5"><strong>No Data Found</strong></h4>--}}
                    </div>
                </div>
                {{--                <div id="thirdParty" class="tab-pane fade">--}}
                {{--                </div>--}}
            </div>
        </div>
        <!-- /.box-body -->

    </div>
@stop
