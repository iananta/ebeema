@extends('layouts.backend.containerlist')

@section('title')
    File
@endsection

@section('footer_js')
    <script type="text/javascript">
        $('#tablebody').on('click', '.delete-link', function (e) { 
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "DELETE",
                        url: "video/"+id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            new swal(
                                'Success',
                                response.message,
                                'success'
                            )
                            $object.parent().parent().remove()
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });
    </script>
@endsection

@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
                <a href="{{route('admin.videoupload.create')}}">
                    <button
                        class="btn btn-primary c-primary-btn add-modal shadow-none mx-2"><img
                            src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                    </button>
                </a>
            </div>
        </div>
        <div class="box-body px-4">
            <div class="dataTables_wrapper dt-bootstrap4">

                @include('layouts.backend.alert')

                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Email</th>
                        <th>Choose File</th>
                        <th class="dt-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($uploads as $index=>$upload)
                                <tr class="gradeX" id="row_{{ $upload->id }}">
                                    <td class="index">
                                        {{ ++$index }}
                                    </td>
                                    <td class="title">
                                        {{ $upload->title }}
                                    </td>
                                    <td class="attachment">
                                        {{ $upload->attachment }}
                                    </td>
                                    <td class="justify-content-center d-flex p-2">
                                            <a href="javascript:;" title="Delete" class="delete-link" id="{{ $upload->id }}">
                                    <button type="button" class="btn btn-danger btn-flat">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>    &nbsp&nbsp   
                                        &nbsp&nbsp  
                                     <a href="{{route('admin.support.show', $upload->id)}}" id="{{ $upload->id }}">
                                    <button type="button" class="btn btn-primary btn-flat">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                </td> 
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
