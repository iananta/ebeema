@extends('layouts.backend.containerlist')

@section('dynamicdata')

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Upload show</h3>
    </div>
    <div class="box-body">
        @include('layouts.backend.alert')
        <form class="px-3" action="{{ route('admin.videoupload.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Tittle</label> 
                <div class="form-input">
                <input class="form-control @error('title') is-invalid @enderror" name="title" 
                value="{{old('name', $supoertvideo->title)}}" readonly> 
            </div>
            </div>
           
            <div class="form-group">
                 <label>Choose File</label>
                 <div class="form-input">
                 <input  class="form-control @error('title') is-invalid @enderror" name="attachment"
                value="{{old('name', $supoertvideo->attachment)}}" readonly> 
              </div>
            </div>
        </form>
    </div>
</div>
@stop
