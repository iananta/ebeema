@extends('layouts.backend.containerlist')

@section('title')
    Edit User Companies
@endsection

@section('dynamicdata')
    <div class="container">
        <br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-6">
                @include('layouts.backend.alert')
                <form role="form" action="{{route('admin.privilege.user.companies.store', $user->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="agentAdd">
                            <hr>
                            <table class="table table-bordered" id="dynamicTable">
                                <tr>
                                    <th>Company</th>
                                    <th>Liscence Number</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($user->userAgents as $agentComp)
                                    <tr>
                                        <td><select class="form-control m-bot15" name="company_id[]">
                                                <option value="{{$agentComp->company_id}}" >{{$agentComp->company->name ?? 'N/A'}}</option>
                                            @foreach($companies as $company)
                                                    <option value="{{$company->id}}" @if($agentComp->company_id == $company->id) selected @endif>{{$company->name}}</option>
                                                @endforeach
                                            </select></td>
                                        <td><input type="text" name="liscence_number[]" min="0" value="{{$agentComp->liscence_number}}"
                                                   placeholder="Enter liscence number" class="form-control"/></td>
                                        <td>
                                            <button type="button" class="btn btn-danger remove-tr">Remove</button>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td><select class="form-control m-bot15" name="company_id[]">
                                            <option value="" selected>Select Company</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" name="liscence_number[]" min="0"
                                               placeholder="Enter liscence number" class="form-control"/></td>
                                    <td>
                                        <button type="button" name="add" id="addCompany"
                                                class="btn btn-success pl-4 pr-4">Add
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">Save</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>
@section('footer_js')
    <script type="text/javascript">
        var i = 0;
        $(document).on('click', '#addCompany', function () {
            // alert('button clicked');
            ++i;

            $("#dynamicTable").append('<tr><td><select class="form-control m-bot15" name="company_id[]"><option value="" selected>Select Company</option>@foreach($companies as $company) <option value="{{$company->id}}">{{$company->name}}</option>@endforeach </select></td> <td><input type="text" name="liscence_number[]" min="0" placeholder="Enter liscence number" class="form-control" /></td> <td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
        });

        $(document).on('click', '.remove-tr', function () {
            $(this).parents('tr').remove();
        });
    </script>
@stop
