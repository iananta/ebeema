@extends('layouts.backend.containerlist')

@section('title')
    Edit Profile
@endsection
@section('footer_js')

@endsection
@section('dynamicdata')
  @php($user = Auth::user())
 <div class="box">
    <div class="box-body">
         @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" role="form" id="userEditForm" action="{{ route('user.profile.modify', $user->id) }}"
                      method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="custom-row">
                        <div class="form-input-wrapper" >
                            <div class="form-label">
                                <label>Profile Image</label>
                            </div>
                            <div class="form-input">
                                <input
                                    type="file"
                                    name="image_icon"
                                    accept="image/*"
                                    class="input-control"
                                >
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Full Name</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="text" name="username" class="input-control username" value="{{$user->username}}" id="username"
                                       placeholder="Enter username" />
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                             <label for="designation">Designation</label>
                            </div>
                            <div class="form-input">
                                <input type="text" name="designation" class="input-control designation" value="{{$user->designation}}" id="designation"
                                       placeholder="Enter designation"/>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Address</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="text" name="address" class="input-control address" value="{{$user->address}}" id="address"
                                       placeholder="Enter address"/>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Phone Number</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="text" name="phone_number" class="input-control phone_number" value="{{$user->phone_number}}" id="phone_number"
                                       placeholder="Enter phone_number" />
                            </div>
                        </div>
                        <!-- <div class="form-input-wrapper">
                            <div class="form-label">
                                <label for="phone_number_unofficial">Phone Number (unofficial)</label>
                            </div>
                            <div class="form-input">
                                <input type="text" name="phone_number_unofficial" class="input-control phone_number_unofficial" value="{{$user->phone_number_unofficial}}" id="phone_number_unofficial"
                                       placeholder="Enter Unofficial Phone Number"/>
                            </div>
                        </div> -->
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label for="email">Email</label>
                            </div>
                            <div class="form-input">
                                <input data-validation="required" type="text" name="email" class="input-control email"
                                       placeholder="Enter Email Address" value="{{$user->email}}" required/>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label for="province">Province *</label>
                            </div>
                            <div class="form-input">
                            <select class="form-control" id="province" name="province" required >
                            <option selected value="">Select</option>
                             @foreach($provinces as $province)
                             <option value="{{old('province_name',$province->province_name)}}">
                                                {{ ($province->province_name)}}
                                            </option>
                             @endforeach
                            </select>      
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label for="disctrict">District *</label>
                            </div>
                            <div class="form-input">
                            <select name="district" id="district" class="form-control" data-validation="required" value="" data-validation-error-msg="Select City">
                                <option selected disabled="">Please Select One...</option>
                                @foreach($districts as $district)
                                            <option value="{{old('district_name',$district->district_name)}}">
                                                {{ ($district->district_name)}}
                                            </option>
                                 @endforeach
                            </select> 
                            </div>
                        </div>


                                                
                        <!-- <div class="form-input-wrapper">
                            <div class="form-label">
                                <label for="email_unofficial">Email (Unofficial)</label>
                            </div>
                            <div class="form-input">
                                  <input type="text" name="email_unofficial" class="input-control email_unofficial"
                                       placeholder="Enter Unofficial Email Address" value="{{$user->email_unofficial}}"/>
                            </div>
                        </div> -->

                       
                      
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Citizenship Front Image @if(Auth::User()->citizen_front)<img style="width:20px" class="user-img" src="/uploads/user/{{Auth::User()->citizen_front}}">@endif</label>
                            </div>
                            <div class="form-input">
                                <input
                                    type="file"
                                    name="citizen_front"
                                    accept="image/*"
                                    class="input-control front-citizen citizen-input"
                                >
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Citizenship Back Image @if(Auth::User()->citizen_back)<img style="width:20px" class="user-img" src="/uploads/user/{{Auth::User()->citizen_back}}">@endif</label>
                            </div>
                            <div class="form-input">
                                <input
                                    type="file"
                                    name="citizen_back"
                                    accept="image/*"
                                    class="input-control back-citizen citizen-input"
                                >
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Password</label>
                            </div>
                            <div class="form-input">
                                <input class="input-control" type="password" id="pasword" name="password"
                                           placeholder="Enter Password">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
             <div class="form-input-details-wrapper">
                <div class="profile-image-wrapper">
                    <div class="profile-image">
                       @if (Auth::User()->image_icon)
                        <img src="{{asset('uploads/avatar.png')}}">
                       @else
                       <img class="user-img" src="{{asset('/images/images.png')}}">
                       @endif
                      
                    </div>
                     <span class="user-stataus-{{$user->is_active == 1 ? 'green' : 'red'}}">
                           {{$user->is_active == 1 ? 'Verified' : 'Not verified'}}
                       </span>
                </div> 
            </div>

        </div>
    </div>
</div>  
@stop

@section('footer_js')
 @include('Backend.globalScripts.form-validation')

@endsection
