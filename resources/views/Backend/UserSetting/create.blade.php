@extends('layouts.backend.containerlist')

@section('dynamicdata')

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Add Setting</h3>
    </div>
    <div class="box-body">
        @include('layouts.backend.alert')
        <form class="px-3" action="{{ route('page-setting.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="key">Key</label>
                <input type="text" class="form-control @error('key') is-invalid @enderror" name="key" id="key"
                    value="{{ old('key') }}" placeholder="Setting Key Eg: mail-from">
                @error('key')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="value">Value</label>
                <textarea
                    class="ckeditor form-control form-control-inline input-medium"
                    name="value"
                    id="value"
                    placeholder="Value Eg: abc@xyz.com"
                >{!! old('value') !!} </textarea>
                @error('value')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="type">Type</label>
                <input type="text" class="form-control @error('type') is-invalid @enderror" name="type" id="type"
                    value="{{ old('type') }}" placeholder="Setting Type Eg: mail-settings">
                @error('type')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@stop
