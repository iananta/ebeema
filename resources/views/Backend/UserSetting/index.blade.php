@extends('layouts.backend.containerlist')

@section('title')
    Settings
@endsection

@section('footer_js')
    <script>
        $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
        })
        });
    </script>
@endsection

@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
                <a href="{{route('page-setting.create')}}">
                    <button
                        class="btn btn-primary c-primary-btn add-modal shadow-none mx-2"><img
                            src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                    </button>
                </a>
            </div>
        </div>
        <div class="box-body px-4">
            <div class="dataTables_wrapper dt-bootstrap4">

                @include('layouts.backend.alert')

                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Key</th>
                        <th width="50%">Value</th>
                        <th>Type</th>
                        <th class="dt-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">

                    @foreach ($settings as $index => $setting)
                        <tr class="gradeX" id="row_{{ $setting->id }}">
                            <td class="index">
                                {{ ++$index }}
                            </td>
                            <td class="name">
                                {{ $setting->key }}
                            </td>
                            <td class="name" style="word-break: break-word">
                                {!! $setting->value  !!}
                            </td>
                            <td class="name">
                                {{ $setting->type }}
                            </td>
                            <td class="justify-content-center">
                                <a href="{{route('page-setting.edit',$setting->id)}}" id="{{ $setting->id }}"
                                   title="Edit">
                                    <button class="btn btn-primary btn-flat">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>&nbsp;
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


