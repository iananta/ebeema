<script>
	this.formValidationInitial=function(){
		this.validationListener=function(){
			let form=document.querySelector('.form-data')
			form.addEventListener('submit',function(e){
				let count=0
				let selector=document.querySelectorAll('.input-control[data-validation]')
				let validationMessage=document.querySelectorAll('.validation_error')
				Array.prototype.forEach.call(validationMessage,function(element){
					element.remove()
				})
				Array.prototype.forEach.call(selector, function(element){
					if(!element.parentNode.parentNode.classList.contains('d-none')){
						let tag=document.createElement('p')
						tag.classList.add('validation_error')
						tag.innerHTML='This field is required'
		                if(!element.value){
		                	element.parentNode.append(tag)
		                    element.style.border = '1px solid #ff0000';
		                    count++
		                }
					}
            	});
            	if(count > 0){
            		e.preventDefault()
            	}
			})
			form.addEventListener('input',function(e){
				let selector=document.querySelectorAll('.input-control[data-validation]')
				Array.prototype.forEach.call(selector, function(element){
					let parent=element.parentNode
						if(element.value){
							element.removeAttribute('style')
							let validationTag= parent.querySelector('.validation_error')
							if(validationTag){
								validationTag.remove()
							}
						}
				})
			})
		},
		this.init=function(){
			this.validationListener();
		}
	}
	let validationOBJ=new formValidationInitial()
		validationOBJ.init()
	
</script>