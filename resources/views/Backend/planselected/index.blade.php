@extends('layouts.backend.containerlist')

@section('title')
    Selected Plans
@endsection

@section('footer_js')
    <script type="text/javascript">
        var oTable = $('#whyTable').dataTable(
            {
                "ordering": false,
                "scrollX": true
            }
        );
        $('#tablebody').on('click', '.delete-item', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/why-us') }}" + "/" + id,
                    data: {
                        'id': id,
                        _method: 'delete'
                    },
                    dataType: 'json',
                    success: function (response) {
                        var nRow = $($object).parents('tr')[0];
                        oTable.fnDeleteRow(nRow);
                        swal('Success', response.message, 'success');
                    },
                    error: function (e) {
                        swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            });
        });

        function sendStatusToServer(id) {
            var p_id = id;
            var status_value = document.getElementById("statusValue").value;
            $.ajax({
                type: "post",
                dataType: "json",
                url: "{{ route('plan.status.change') }}",
                data: {
                    id: p_id, status: status_value,
                    _token: '{{csrf_token()}}'
                },
                success: function (response) {
                    // console.log(response);
                    if (response.status == "success") {
                        swal({
                            icon: "success",
                            text: response.message,
                        });
                    } else {
                        swal({
                            icon: "error",
                            text: response.message,
                        });
                        // console.log(response);
                    }
                }
            });

        }
    </script>
@endsection

@section('dynamicdata')

    <div class="box">

        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
            </div>
        </div>
        <div class="box-body">

            @include('layouts.backend.alert')

            <hr>

            {{--  <div class="justify-content-end list-group list-group-horizontal ">
                  <button type="button" class="btn btn-primary" data-toggle="modal"
                          data-target="#addModal"><img src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon">&nbsp;
                      Add
                  </button>
                  @include('backend.WhyUs.add')
              </div>--}}

            <br>
            <div class="blank-page">
                <table class="table" id="whyTable">
                    <thead>
                    <tr>
                        <th scope="col">SN</th>
                        <th scope="col">Customer Name</th>
                        <th scope="col">Customer E-mail</th>
                        <th scope="col">Customer Phone No.</th>
                        <th scope="col">Customer Citizenship Front</th>
                        <th scope="col">Customer Citizenship Back</th>
                        <th scope="col">Category</th>
                        <th scope="col">Product</th>
                        <th scope="col">Company</th>
                        <th scope="col">Lead</th>
                        <th scope="col">Age</th>
                        <th scope="col">Date of Birth</th>
                        <th scope="col">Child Age</th>
                        <th scope="col">Child Date of Birth</th>
                        <th scope="col">Proposer Age</th>
                        <th scope="col">Proposer Date of Birth</th>
                        <th scope="col">Wife Age</th>
                        <th scope="col">Wife Date of Birth</th>
                        <th scope="col">Husband Age</th>
                        <th scope="col">Husband Date of Birth</th>
                        <th scope="col">Term</th>
                        <th scope="col">Sum Assured</th>
                        <th scope="col">Invest per Year</th>
                        <th scope="col">Mode of Payment</th>
                        <th scope="col">Features Selected</th>
                        <th scope="col">Premium on compare</th>
                        <th scope="col">Bonus at the End of Year</th>
                        <th scope="col">Return at the End of Period</th>
                        <th scope="col">Net Gain</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($selected as $sel)
                        <tr id="row_{{ $sel->id }}">

                            <th scope="row">#{{$loop->iteration}}</th>
                            <td>{{$sel->name}}</td>

                            <td> {{$sel->email}} </td>

                            <td> {{$sel->phone}} </td>

                            <td style="width: 20%" class="compare-parts line-rht-cmp" scope="row">
                                <img
                                    src="{{ asset('uploads/citizen/'.$sel->citizen_front) }}"
                                    width="80"
                                    height="80"
                                    alt="citizen front"
                                    title="citizen front"
                                >
                                {{--                                <img style="max-width:100%" src="/uploads/citizen/{{$sel->citizen_front}}">--}}
                            </td>

                            <td style="width: 20%" class="compare-parts line-rht-cmp" scope="row">
                                <img
                                    src="{{ asset('uploads/citizen/'.$sel->citizen_back) }}"
                                    width="80"
                                    height="80"
                                    alt="citizen back"
                                    title="citizen back"
                                >
                            </td>

                            <td>{{ $sel->category }}</td>

                            <td>{{ $sel->product->name ?? "" }}</td>

                            <td>{{ $sel->company->name ?? "" }}</td>

                            <td>{{@$sel->lead->user->username}}</td>

                            <td>{{$sel->age}}</td>

                            <td>
                                <ul style="list-style: none">
                                    dd/mm/yy:
                                    <li> {{$sel->birth_date}}/{{$sel->birth_month}}/{{$sel->birth_year}}</li>

                                </ul>

                            </td>

                            <td>{{$sel->child_age}}</td>

                            <td>
                                @if ($sel->proposer_age != null)
                                    <ul style="list-style: none">
                                        dd/mm/yy:
                                        <li> {{$sel->child_birth_date}}/{{$sel->child_birth_month}}
                                            /{{$sel->child_birth_year}}</li>

                                    </ul>
                                @else

                                @endif

                            </td>

                            <td>{{$sel->proposer_age}}</td>

                            <td>
                                @if ($sel->proposer_age != null)
                                    <ul style="list-style: none">
                                        dd/mm/yy:
                                        <li> {{$sel->proposer_birth_date}}/{{$sel->proposer_birth_month}}
                                            /{{$sel->proposer_birth_year}}</li>
                                    </ul>
                                @else

                                @endif

                            </td>

                            <td>{{$sel->wife_age}}</td>

                            <td>
                                @if ($sel->wife_age != null)
                                    <ul style="list-style: none">
                                        dd/mm/yy:
                                        <li> {{$sel->wife_birth_date}}/{{$sel->wife_birth_month}}
                                            /{{$sel->wife_birth_year}}</li>
                                    </ul>
                                @else

                                @endif

                            </td>

                            <td>{{$sel->husband_age}}</td>

                            <td>
                                @if ($sel->husband_age != null)
                                    <ul style="list-style: none">
                                        dd/mm/yy:
                                        <li> {{$sel->husband_birth_date}}/{{$sel->husband_birth_month}}
                                            /{{$sel->husband_birth_year}}</li>
                                    </ul>
                                @else

                                @endif
                            </td>

                            <td>{{$sel->term}}</td>

                            <td>Rs. {{is_float($sel->sum_assured) ? round($sel->sum_assured,2) : intval($sel->sum_assured) }}</td>

                            <td>Rs. {{is_float($sel->invest) ? round($sel->invest,2) : intval($sel->invest) }}</td>

                            <td>{{$sel->mop}}</td>

                            <td>{{$sel->feature}}</td>

                            <td>Rs. {{is_float($sel->premium) ? round($sel->premium,2) : intval($sel->premium) }}</td>

                            <td>Rs. {{is_float($sel->bonus) ? round($sel->bonus,2) : intval($sel->bonus) }}</td>

                            <td>Rs. {{is_float($sel->return) ? round($sel->return,2) : intval($sel->return) }}</td>

                            <td>Rs. {{is_float($sel->net_gain) ? round($sel->net_gain,2) : intval($sel->net_gain) }}</td>

                            <td>
                                <select style="width: 135px;" onchange="sendStatusToServer({{ $sel->id }})"
                                        id="statusValue" class="form-control" name="status">
                                    <option value="0"{{$sel->status == 0 ? "selected" : ''}}>Requested</option>
                                    <option value="1"{{$sel->status == 1 ? "selected" : ''}}>Processing</option>
                                    <option value="2"{{$sel->status == 2 ? "selected" : ''}}>Done</option>
                                    <option value="3"{{$sel->status == 3 ? "selected" : ''}}>Rejected</option>
                                </select>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
