<script>
	this.premiumDetailsInitial=function(){
		let _this=this
		let popUp=$('.premium-collection-popup-wrapper')
		this.permiumPopUpListener=function(){
			$(document).on('click','.premium-dismiss',function(){
				popUp.hide()
			})
			$(document).on('click','.view-premium-collection',function(){
				let target=$(this)
				let id=target.data('id')
				
				$.ajax({
					type:'post',
					dataType:'json',
					url:"{{route('admin.getPremiumCollection')}}",
					data:{
						_token:'{{csrf_token()}}',
						id:id
					},
					beforeSend:function(){

					},
					success:function(response){
						popUp.css('display','flex')
						$('.premiumdetails-content-wrapper').html(response.premiumDetails)
					}
				})
			})
		},
		this.premiumAccordianListener=function(){
			$(document).on('click','.toggle-premium',function(){
				let target=$(this)
				let parent=target.parent()
				target.toggleClass('active')
				if(target.hasClass('active')){
					parent.next().slideDown()
				}else{
					parent.next().slideUp()
				}
			})
		},
		this.verifyListener=function(){
			$(document).on('click','.status-change-button',function(){
				let target=$(this)
				let id=target.data('id')
				let verify=target.data('verify')
				let parent=target.parent()
				new swal({
                    title: 'Are you sure?',
                    text: "You want to change the premium amount status",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (response) {
                    if(response.isConfirmed == true){
                        $.ajax({
                            type: "post",
                            url: "{{ route('admin.premiumVerify') }}",
                            data:{
                            	_token:"{{csrf_token()}}",
                            	id:id,
                            	verify:verify
                            },
                            dataType: 'json',
                            success: function (response) {
                              parent.find('button').each(function(){
                              	$(this).removeClass('active')
                              })
                              target.addClass('active')
                              new swal('Success', response.message, 'success');
                            },
                            error: function (e) {
                                new swal('Oops...', 'Something went wrong!', 'error');
                            }
                        });
                    }


                }).catch(swal.noop);
			})
		},
		this.init=function(){
			_this.permiumPopUpListener()
			_this.premiumAccordianListener()
			_this.verifyListener()
		}
	}
	let premiumDetailsObj=new premiumDetailsInitial()
		premiumDetailsObj.init()
</script>