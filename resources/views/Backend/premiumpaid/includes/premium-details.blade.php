@if(count($user->premiumPaidByCustomer))
<div class="accordion-wrapper">
    @foreach($user->premiumPaidByCustomer->groupBy('planselected_id') as $premium)
    <div class="premium-accordian-item">
        <div class="accordian-heading-wrapper">
            <h4>{{$premium[0]->planselected->name}}&nbsp;&nbsp;({{$premium[0]->planselected->phone}})</h4>
            <button type="button" class="toggle-premium"><i class="fa fa-arrow-circle-o-down"></i></button>
        </div>
        <div class="permium-details-wrapper">
            <ul>
                <li><strong>Category</strong><span>{{ucfirst($premium[0]->planselected->category ?? 'Not Selected')}}</span></li>
                <li><strong>Company</strong><span>{{$premium[0]->planselected->company->name ?? 'Not Selected'}}</span></li>
                <li><strong>Product</strong><span>{{$premium[0]->planselected->product->name ?? 'Not Selected'}}</span></li>
                <li><strong>Term</strong><span>{{$premium[0]->planselected->term ?? 'Not Selected'}}</span></li>
                <li><strong>Premium</strong><span>{{$premium[0]->planselected->premium ?? 'Null'}}</span></li>
                <li><strong>MOP</strong><span>{{$premium[0]->planselected->mop ?? 'Not Selected'}}</span></li>
                <li><strong>Issued Date</strong><span>{{$premium[0]->planselected->issued_date ?? 'Null'}}</span></li>
                <li><strong>Policy Number</strong><span>{{$premium[0]->planselected->policy_number ?? "Null"}}</span></li>
                <div class="premium-paid-details">
                    @foreach($premium as $details)
                    <div class="premium-paid-item">
                        <h1>Paid For {{@$details['paid_date']}}</h1>
                        <li>
                            <strong>Premium Amount</strong>
                            <span>Rs.{{$details->premium_amount}}</span>
                            <!-- <span class="premium-status-wrapper"><button data-id="{{$details->id}}" data-verify="1" class="status-change-button green-btn {{$details->verify == 1 ? 'active' : ''}}">Verified</button><button data-id="{{$details->id}}"  data-verify="0" class="status-change-button red-btn {{$details->verify == 0 ? 'active' : ''}}">Not Verify</button></span> -->
                        </li>
                        <li><p class="premium-status"><span class="status-{{$details->status}}">{{ucfirst($details->status)}}</span></p></li>
                    </div>
                    @endforeach

                </div>
            </ul>
        </div>
    </div>
    @endforeach
</div>
@else
    <p class="not-found">No data found</p>
@endif