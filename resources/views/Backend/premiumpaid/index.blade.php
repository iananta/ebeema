@extends('layouts.backend.containerlist')
@section('title')
Life Insurance Staff
@endsection
@section('footer_js')
    {!! $dataTable->scripts() !!} 
    @include('Backend.premiumpaid.includes.premium-details-scripts')
@endsection


@section('dynamicdata')
    <div class="box">
        <div class="box-body">
            {!! $dataTable->table() !!}
        </div>
    </div>
    <div class="premium-collection-popup-wrapper">
        <div class="premium-collection-popup">
            <div class="premium-collection-wrapper">
                <div class="pop-up-heading">
                    <h1>Premium Collection Details</h1>
                    <button type="button" class="premium-dismiss"><img class="dismiss-icon" src="{{asset('backend/img/dismiss.png')}}"></button>
                </div>
            </div>
            <div class="premiumdetails-content-wrapper"></div>
        </div>
    </div>
@endsection
