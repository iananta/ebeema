@extends('layouts.backend.containerlist')
@section('title')
    {{isset($seoform) ? 'Edit' : 'Create' }} Seo Form
@endsection
@section('footer_js')
@endsection
@php
    $url=isset($seoform) ? route('admin.seoForm.edit',['id'=>$seoform->id]) : route('admin.seoForm.store');
@endphp
@section('dynamicdata')
    <div class="box">
        <div class="box-body">
            <div class="policy-compare-wrapper">
                <div class="form-wrapper">
                    <form class="form-data" method="POST" action="{{ $url }}" >
                        @csrf
                        <div class="custom-row">
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Form Name<label>
                                </div>
                                <div class="form-input">
                                    <input type="text" name="form_name" class="input-control"  value="{{isset($seoform) ? $seoform->form_name : old('form_name')}}">
                                    @if ($errors->has('form_name'))
                                        <p class="validation-error"><i class="fa fa-times-circle"></i>&nbsp;{{ $errors->first('form_name') }}</p>
                                    @endif
                                </div>
                            </div>
                             <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Form Id<label>
                                </div>
                                <div class="form-input">
                                    <input type="text" name="form_id" class="input-control" value="{{isset($seoform) ? $seoform->form_id : old('form_id')}}">
                                    @if ($errors->has('form_id'))
                                        <p class="validation-error"><i class="fa fa-times-circle"></i>&nbsp;{{ $errors->first('form_id') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                    <div class="form-label"></div>
                                <div class="form-input">
                                    <button type="submit" id="submit-btn" class="form-submit-btn">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
