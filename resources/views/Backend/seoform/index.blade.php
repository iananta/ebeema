@extends('layouts.backend.containerlist')
@section('title')
    Seo Form
@endsection
@section('footer_js')
        <script type="text/javascript">
            @if(\Session::has('message'))
                new swal('success','{{\Session::get('message')}}','success')
            @endif
        </script>
    
    {!! $dataTable->scripts() !!}
    @include('Backend.seoform.seoform-script')
@endsection


@section('dynamicdata')
    <div class="box">
        <div class="box-body">
           {!! $dataTable->table() !!}
        </div>
    </div>
@endsection
