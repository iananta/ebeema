<script>
	this.seoFormIntial=function(){
		let _this=this
		this.deleteSeoFormListener=function(){
			$(document).on('click','.delete-seo-form',function(){
				let id =$(this).data('id')
				 $object = $(this);
				new swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (response) {
                    if(response.isConfirmed == true){
                        $.ajax({
                            type: "DELETE",
                            url: "{{ route('admin.seoForm.delete') }}",
                            data:{
                            	id:id,
                                '_token': $('meta[name="csrf-token"]').attr('content')
                            },
                            beforeSend: function () {
                                $('#preloader').show();
                            },

                            dataType: 'json',
                            success: function (response) {
                                $('#preloader').hide();
                                var nRow = $($object).parents('tr')[0];
                                new swal(
                                    'Success',
                                    response.message,
                                    'success'
                                )
                                  $object.parent().parent().remove()
                            },
                            error: function (e) {
                                $('#preloader').hide();
                                new swal('Oops...', 'Something went wrong!', 'error');
                            }
                        });
                    }


                }).catch(swal.noop);
			})
		},
		this.init=function(){
			_this.deleteSeoFormListener()
		}
	}
	let seoFormObj=new seoFormIntial()
		seoFormObj.init()
</script>
