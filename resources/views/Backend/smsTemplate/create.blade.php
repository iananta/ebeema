@extends('layouts.backend.containerlist')

@section('title')
SMS Template Add
@endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{ route('admin.smsTemplate.store') }}">
                    @csrf

                    <div class="custom-row">
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Title</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Enter title of SMS" value="{{ old('title') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Code</label>
                            </div>
                            <div class="form-input">
                                <input type="text" data-validation="required" class="form-control" id="key" name="key"
                                       placeholder="Enter key of SMS" value="{{ old('key') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Message</label>
                            </div>
                            <div class="form-input">
                                <textarea
                                    class="form-control"
                                    name="message"
                                    id="message"
                                    placeholder="Message for the sms"
                                    value="{{ old('message') }}"
                                ></textarea>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Status</label>
                            </div>
                            <div class="form-input">
                                <select class="input-control search-select  validate-check" id="status" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Deactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Notes</label>
                            </div>
                            <div class="form-input">
                                <textarea
                                    class="ckeditor form-control"
                                    name="notes"
                                    id="notes"
                                    placeholder="Notes:"
                                    value="{{ old('notes') }}"
                                ></textarea>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="form-input-details-wrapper">

            </div>
        </div>
    </div>
</div>
@endsection
