@extends('layouts.backend.containerlist')

@section('title')
SMS Template Edit
@endsection

@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{ route('admin.smsTemplate.update', $sms->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="custom-row">
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Title</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Enter title of mail" value="{{ $sms->title }}"/>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Message</label>
                            </div>
                            <div class="form-input">
                                <textarea
                                    class="form-control"
                                    name="message"
                                    id="message"
                                    placeholder="Message of the SMS"
                                    value="{{ $sms->message }}"
                                >{!! $sms->message !!}</textarea>
                            </div>
                        </div>

                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                <a href="{{ route('admin.mail.reset', $sms->id) }}" onclick="return resetBody()" class="btn btn-danger float-right mt-2">Reset Form</a>
            </div>
            <div class="form-input-details-wrapper">
                <div class="alert"
                     style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                    <center><strong>{{ ('Note') }}:</strong></center>
                    <br>
                    <p>
                        {{ ('DO NOT Modify data that are inside { } brackets!') }}!
                    </p>
                    <p>{!! $sms->notes !!}</p>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_js')
    <script>
        function resetBody()
        {
            var agree=confirm("Are you sure you wish to continue?");
            if (agree)
                return true ;
            else
                return false ;
        }
    </script>
@endsection

