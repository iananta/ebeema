@extends('layouts.backend.containerlist')

@section('dynamicdata')

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">ADD Detail</h3>
    </div>
    <div class="box-body">
        @include('layouts.backend.alert')
        <form class="px-3" action="{{ route('admin.support.supply') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                    value="{{ old('name') }}" placeholder="Enter Desigination Name">
                @error('name')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
         
            <div class="form-group">
                <label for="phone_number">Phone Number</label>
                <input type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" id="phone_number"
                    value="{{ old('phone_number') }}" placeholder="Enter Phone Number">
                @error('phone_number')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
           
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
@stop
