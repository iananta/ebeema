@extends('layouts.backend.containerlist')

@section('title')
  Social Address
@endsection

@section('footer_js')
    <script type="text/javascript">
    </script>
@endsection
@section('dynamicdata')
    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
                <a href="{{route('admin.support.create')}}">
                    <button
                        class="btn btn-primary c-primary-btn add-modal shadow-none mx-2"><img
                            src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                    </button>
                </a>
            </div>
        </div>
        <div class="box-body px-4">
            <div class="dataTables_wrapper dt-bootstrap4">
                @include('layouts.backend.alert')
                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Designation Name</th>
                        <th>Phone Number</th>
                        <th class="dt-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    @foreach($uploads as $index=>$upload)
                                <tr class="gradeX" id="row_{{ $upload->id }}">
                                    <td class="index">
                                        {{ ++$index }}
                                    </td>
                                    <td class="title">
                                        {{ $upload->name }}
                                    </td>
                                    <td class="attachment">
                                        {{ $upload->phone_number }}
                                    </td>
                                    <td class="justify-content-center d-flex p-2">
                                        <a href="{{ route('admin.product.downloadProductFile', ['productFileId' => $upload->id]) }}"
                                         title="download"><button class="btn btn-primary btn-flat"><i
                                            class="fa fa-download"></i></button></a>&nbsp;
                                            <a href="{{route('admin.support.replace',$upload->id)}}" id="{{ $upload->id }}"
                                   title="support profile">
                                    <button class="btn btn-primary btn-flat"><i
                                            class="fa fa-edit"></i></button>
                                </a>&nbsp;
                                        </td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
