    @extends('layouts.backend.containerlist')

    @section('footer_js')
     @include('Backend.globalScripts.form-validation')
    <script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/js/bootstrap-editable.min.js')}}"></script>
    <style>
    .backend-address {
        position: fixed;
        bottom: 0;
        width: 100%;
    }
    </style>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $('.xedit').editable({
                url: '{{url("admin/update-rate-table/endowment-rate")}}',
                title: 'Update',
                ajaxOptions: {
                    type: 'post'
                },
                success: function (response) {
                        // console.log(response);
                        if (response.success) {
                            swal.fire("Done!", "Updated Successfully !", "success");
                        } else {
                            swal.fire("Error!", "Update error !", "error");
                        }
                    }
                });

        });

        function findProducts() {
            var data1 = $('option:selected', '#company1').data('products');
            data = data1;
                // console.log(data);
                var html = '';
                html += '<option selected value="">Select the Product</>'
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].name + '</option>'
                }
                $('#products1').html(html);
            }

        </script>
        <script type="text/javascript">
            $(function () {
                $('#example1').DataTable()
                $('#example2').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': false,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            });
            $('#tablebody').on('click', '.delete-file', function (e) {
                e.preventDefault();
                $object = $(this);
                var id = $object.attr('id');
                new swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (response) {
                    if (response.isConfirmed == true) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ url('/admin/upload/ProductFile') }}" + "/" + id,
                            data: {
                                '_token': $('meta[name="csrf-token"]').attr('content')
                            },
                            beforeSend: function () {
                                $('#preloader').show();
                            },
                            dataType: 'json',
                            success: function (response) {
                                $('#preloader').hide();
                                var nRow = $($object).parents('tr')[0];
                                new swal(
                                    'Success',
                                    response.message,
                                    'success'
                                    )
                                $object.parent().parent().remove()
                            },
                            error: function (e) {
                                $('#preloader').hide();
                                new swal('Oops...', 'Something went wrong!', 'error');
                            }
                        });
                    }
                }).catch(swal.noop);
            });
            $('.card-header').on('click',function(){
                $(this).toggleClass('active')
                $('.product-file-form-wrapper').toggleClass('active')
                if($('.product-file-form-wrapper').hasClass('active')){
                    $('.product-file-form-wrapper').slideDown()
                }else{
                    $('.product-file-form-wrapper').slideUp()
                }
            })
        </script>


        @endsection
        @section('title')
        Product Management File Upload
        @endsection

        @section('dynamicdata')

        <div class="box">
            <div class="box-body">

                @include('layouts.backend.alert')
                <div class="product-collasp">

                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0 h6"><strong>Click To Import Product Management File</strong></h5>
                            <button class="toggleBtn-product-form"><i class="fa fa-chevron-down"></i></button>
                        </div>
                        <div class="card-body product-file-form-wrapper">
                            <div class="policy-compare-wrapper">
                                <div class="form-wrapper">
                                    <form class="form-data policy-compare-form" action="{{ route('admin.policy.compare') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="custom-row">
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label>Title</label>
                                                </div>
                                                 <div class="form-input">
                                                    <input placeholder="Title" type="text" id="title" name="title" class="input-control" data-validation="required">
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label>Code</label>
                                                    <small>[ Codes should be unique !! ]</small>

                                                </div>
                                                 <div class="form-input">
                                                    <input placeholder="Code" type="text" id="code" name="Code" class="input-control" data-validation="required">
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label>Description</label>
                                                </div>
                                                 <div class="form-input">
                                                     <textarea
                                                        class="input-control"
                                                        name="description"
                                                        id="description"
                                                        ></textarea>
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label class="">Company Name</label>
                                                </div>
                                                 <div class="form-input">
                                                      <select data-validation="required" class="input-control input-medium search-select"
                                                      onchange="findProducts()" id="company1" name="company_id">
                                                      <option selected value="">Select the Company</option>
                                                      @foreach ($companies as $company)
                                                      <option data-products="{{$company->products}}"
                                                        value="{{ $company->id }}">{{ $company->name }}</option>
                                                        @endforeach
                                                    </select>
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label class="">Product</label>
                                                </div>
                                                 <div class="form-input">
                                                       <select class="input-control form-control-inline input-medium search-select"
                                                       id="products1"
                                                       name="product_id" data-validation="required">
                                                       <option selected value="">Select the Product</option>
                                                   </select>
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label class="">Types</label>
                                                </div>
                                                 <div class="form-input">
                                                         <select class="input-control input-medium search-select"
                                                         id="type"
                                                         name="type" data-validation="required">
                                                         <option selected value="" >Select Type</option>
                                                         @foreach($types as $type)
                                                         <option value="{{ $type['id'] }}">{{ $type['value'] }}</option>
                                                         @endforeach
                                                     </select>
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label">
                                                    <label>Choose File</label>

                                                </div>
                                                 <div class="form-input">
                                                         <input class="input-control" name="attachment" type="file" id="formFile" data-validation="required">
                                                 </div>
                                            </div>
                                            <div class="form-input-wrapper">
                                                <div class="form-label"></div>
                                                <div class="form-input">
                                                    <button type="submit" class="form-submit-btn">Submit</button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="form-input-details-wrapper">

                            </div>
                        </div>
                    </div>

                </div>

                <table id="sms-table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Title</th>
                            <th>Code *</th>
                            <th>Company</th>
                            <th>Product</th>
                            <th>Description</th>
                            <th>Type</th>
                            <th>Attachment</th>
                            <th class="dt-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="tablebody">
                            @foreach($uploads as $index=>$upload)

                                <tr class="gradeX" id="row_{{ $upload->id }}">
                                    <td class="index">
                                        {{ ++$index }}
                                    </td>
                                    <td class="title">
                                        {{ $upload->title }}
                                    </td>
                                    <td class="code">
                                        {{ $upload->company->code ?? '' }}
                                    </td>
                                    <td class="company_id">
                                        {{ $upload->company->name ?? ''}}
                                    </td>
                                    <td class="product_id">
                                        {{ $upload->product_id }}
                                    </td>
                                    <td class="descripton">
                                        {{ $upload->description }}
                                    </td>
                                    <td class="type">
                                        {{ $upload->type }}
                                    </td>
                                    <td class="attachment">
                                        {{ $upload->attachment }}
                                    </td>

                                    <td class="justify-content-center d-flex p-2">
                                        <a href="{{ route('admin.product.downloadProductFile', ['productFileId' => $upload->id]) }}"
                                         title="download"><button class="btn btn-primary btn-flat"><i
                                            class="fa fa-download"></i></button></a>&nbsp;

                                            <a href="javascript:;" title="Delete" class="delete-file" id="{{ $upload->id }}">
                                                <button type="button" class="btn btn-danger btn-flat">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </a>
                                        </td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    <div class="box-footer">
    @foreach($support as $upload)
    <div class="backend-address">
        <div class="number">  {{ $upload->name }}
        </div>
        <div class="name">   {{ $upload->phone_number }}
        @endforeach
    </div>
        @endsection
