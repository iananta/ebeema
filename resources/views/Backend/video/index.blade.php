@extends('layouts.backend.containerlist')

@section('title')
  Video
@endsection

@section('footer_js')
    <script type="text/javascript">
         $(document).on('click','.add-video',function(){
        window.location = "{{ route('admin.tape.create')}}"
        });
        $('#tablebody').on('click', '.delete-tape', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id'); 
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "DELETE",
                        url: "tape/"+id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            new swal(
                                'Success',
                                response.message,
                                'success'
                            )
                            $object.parent().parent().remove()
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });
    </script>
         {!! $dataTable->scripts() !!}
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-body">
        {!! $dataTable->table() !!}
    </div>
</div>
@endsection

