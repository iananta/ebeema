@extends('layouts.backend.containerlist')

@section('dynamicdata')

<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Video Upload</h3>
    </div>
    <div class="box-body">
        @include('layouts.backend.alert')
        <form class="px-3" action="{{ route('admin.tape.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Tittle</label>
                <div class="form-input">
                <input type="text" class="form-control @error('title') is-invalid @enderror" 
                value="{{old('name', $supportvideo->title)}}" readonly> 
            </div>
            </div>
            <div class="form-group">
                 <label>Choose Video</label>
                 <div class="form-input">
                 <iframe src="/assets/{{ $supportvideo->file }}"></iframe>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Send</button>
        </form>
    </div>
</div>
@stop
