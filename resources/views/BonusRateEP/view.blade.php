@extends('layouts.backend.containerlist')

@section('footer_js')
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    });

    $('#tablebody').on('click', '.delete-bonus', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (response) {
            if (response.isConfirmed == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/bonusList/delete') }}" + "/" + id,
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        new swal(
                            'Success',
                            response.message,
                            'success'
                        )
                        $object.parent().parent().remove()
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        }).catch(swal.noop);
    });
</script>
@endsection
@section('title')
    Bonus Rate
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-header with-border c-btn-right d-flex-row ">
        <div class="justify-content-end list-group list-group-horizontal ">
            <a href="{{route('admin.bonus.create')}}">
                <button class="btn btn-primary c-primary-btn add-modal shadow-none mx-2" data-toggle="modal"
                        data-target="#addNewUserModal">
                    {{-- <img src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> --}}
                    &nbsp; Add New &nbsp;
                </button>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper dt-bootstrap4">
            <!-- <div class="box-header">
                <h3 class="box-title">ROLES</h3>
                <ul class="header-dropdown m-r--5 pull-right">
                  <li class="dropdown" style="list-style : none;">
                       <a href="{{ route('admin.privilege.role.create') }}"><button type="button" class="btn btn-primary waves-effect">ADD NEW <b>+</b></button></a>
                    </li>
                </ul>
              </div>

            </.box-header -->
            <div class="box-body">

                @include('layouts.backend.alert')

                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Start Year</th>
                        <th>End Year</th>
                        <th>Term Rate</th>
                        <th>Product</th>
                        <th>Company</th>
                        <th width="220px">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">

                    @foreach($bonusRate as $index=>$br)
                    <tr class="gradeX" id="row_{{ $br->id }}">
                        <td class="index">
                            {{ ++$index }}
                        </td>
                        <td class="name">
                            {{ $br->first_year }}
                        </td>
                        <td class="name">
                            {{ $br->second_year }}
                        </td>
                        <td class="name">
                            {{ $br->term_rate }}
                        </td>
                        <td class="name">
                            @if(isset($br->product))
                            {{ $br->product->name }}
                            @endif
                        </td>
                        <td class="name">
                            @isset($br->company)
                            {{ $br->company->name }}
                            @endisset
                        </td>
                        <td class="justify-content-center">
                            <a href="{{ route('admin.bonus.edit', $br->id) }}" id="{{ $br->id }}"
                               title="Edit ">
                                <button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button>
                            </a>&nbsp;
                            <a href="javascript:;" title="Delete Bonus" class="delete-bonus"
                               id="{{ $br->id }}">
                                <button type="button" class="btn btn-danger btn-flat"><i
                                        class="fa fa-trash"></i></button>
                            </a>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->

            <!-- /.box -->

        </div>
    </div>
</div>
@endsection





