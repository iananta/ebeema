@extends('layouts.backend.containerlist')

@section('footer_js')
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    });
</script>
@endsection
@section('title')
    Features Products
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-header with-border c-btn-right d-flex-row ">
        <div class="justify-content-end list-group list-group-horizontal ">
            {{-- <a href="{{route('admin.feature.product.create')}}">--}}
                {{--
                <button class="btn btn-primary c-primary-btn add-modal shadow-none mx-2" data-toggle="modal"
                        data-target="#addNewUserModal">--}}
                    {{-- --}}{{-- <img src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> --}}
                    {{-- &nbsp; Add New &nbsp;Feature Product--}}
                    {{--
                </button>
            </a>--}}
            <a href="{{route('admin.feature.create')}}" class="btn btn-primary">Create Feature</a>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper dt-bootstrap4">
            <!-- <div class="box-header">
        <h3 class="box-title">ROLES</h3>
        <ul class="header-dropdown m-r--5 pull-right">
          <li class="dropdown" style="list-style : none;">
               <a href="{{ route('admin.privilege.role.create') }}"><button type="button" class="btn btn-primary waves-effect">ADD NEW <b>+</b></button></a>
            </li>
        </ul>
      </div>

    </.box-header -->

            <div class="box-body">

                @include('layouts.backend.alert')
                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Feature Name:</th>
                        <th>Product Name</th>


                        <th width="220px">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">

                    @foreach($feature as $index=>$pro)
                    <tr class="gradeX" id="row_{{ $pro->id }}">
                        <td class="index">
                            {{ ++$index }}
                        </td>
                        <td class="name">
                            {{ $pro->name }}
                        </td>
                        <td class="name">
                            @if(count($pro->products))
                            @foreach($pro->products as $pro1)
                            <ul>
                                <li>{{ getProductName($pro1->product_id) }} @if($pro1->is_compulsory === 1)<span
                                        class="badge badge-success p-1">Compulsory</span> @endif
                                </li>
                            </ul>
                            @endforeach
                            @else
                            <span class="pl-4"> N/A</span>
                            @endif
                        </td>

                        <td class="justify-content-center">
                            <a href="featureproEdit/{{$pro->id}}" id="{{ $pro->id }}"
                               title="Edit ">
                                <button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button>
                            </a>&nbsp;

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

