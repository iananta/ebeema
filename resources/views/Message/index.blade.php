    @extends('layouts.backend.containerlist')

@section('title')
    Contact's Messages
@endsection
    @section('footer_js')
        <script>
            $(document).ready(function() {
                $('#contactMessage').DataTable( {
                    "scrollX": true,
                    "ordering": false
                } );
            } );
        </script>
        <script type="text/javascript">
            $('#tablebody').on('click', '.delete-messages', function (e) {
                e.preventDefault();
                $object = $(this);
                var id = $object.attr('id');
                new swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (response) {
                    if (response.isConfirmed == true) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ url('/admin/messages/delete') }}" + "/" + id,
                            dataType: 'json',
                            data: {'_token': $('meta[name="csrf-token"]').attr('content')},
                            beforeSend: function () {
                                $('#preloader').show();
                            },
                            success: function (response) {
                                $('#preloader').hide();
                                var nRow = $($object).parents('tr')[0];
                                new swal(
                                    'Success',
                                    response.message,
                                    'success'
                                )
                                $object.parent().parent().remove()
                            },
                            error: function (e) {
                                $('#preloader').hide();
                                new swal('Oops...', 'Something went wrong!', 'error');
                            }
                        });
                    }
                }).catch(swal.noop);
            });
        </script>
    @endsection

@section('dynamicdata')

    <div class="box">
        <div class="box-body">
            <div class="dataTables_wrapper dt-bootstrap4">

                @include('layouts.backend.alert')

                <table id="contactMessage" class="table table-bordered table-hover role-table">
                    <thead>
                        <tr>
                            <th>S.No.</th>
                            <th>Date</th>
                            <th>Full Name</th>
                            <th>E-Mail</th>
                            <th>Message</th>
                            <th class="dt-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="tablebody">

                        @foreach ($messages as $index => $message)
                            <tr class="gradeX" id="row_{{ $message->id }}">
                                <td class="index">
                                    {{ ++$index }}
                                </td>
                                <td class="created_at">
                                    {{ $message->created_at }}
                                </td>
                                <td class="name">
                                    {{ $message->name }}
                                </td>
                                <td class="name">
                                    {{ $message->email }}
                                </td>
                                <td class="name text-wrap">
                                    {{ $message->message }}
                                </td>
                                <td class="justify-content-center">

                                    <a href="{{ route('message.show', $message->id) }}" id="{{ $message->id }}"
                                        title="Show message"><button class="btn btn-primary btn-flat"><i
                                                class="fa fa-eye"></i></button></a>&nbsp;

                                    <a href="javascript:;" title="Delete message" class="delete-messages"
                                        id="{{ $message->id }}"><button type="button" class="btn btn-danger btn-flat"><i
                                                class="fa fa-trash"></i></button></a>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


