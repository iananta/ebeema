@extends('layouts.backend.containerlist')

@section('footer_js')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
<script>
    $('#tablebody').on('click', '.delete-product', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (response) {
            if(response.isConfirmed == true){
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/productDelete') }}" + "/" + id,
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        new swal(
                            'Success',
                            response.message,
                            'success'
                        )
                        $object.parent().parent().remove()
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        }).catch(swal.noop);
    });
</script>
@endsection
@section('title')
    product Lists
@endsection

@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal m-3">
                <a href="{{route('admin.product.create')}}">
                    <button class="btn btn-primary c-primary-btn add-modal shadow-none mx-2" data-toggle="modal"
                            data-target="#addNewUserModal">
                        {{-- <img src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> --}}
                        &nbsp; Add New &nbsp;
                    </button>
                </a>
                <a href="{{ route('admin.product.deleted') }}" class="btn btn-primary c-primary-btn add-modal shadow-none mx-2">
                        &nbsp; Deleted Products &nbsp;
                </a>
            </div>



        </div>
        <div class="box-body">
            <div class="dataTables_wrapper dt-bootstrap4">
                <div class="box-body">

                    @include('layouts.backend.alert')
                    <table id="example1" class="table table-bordered table-hover role-table">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Product Name:</th>
                            <th>Code:</th>
                            <th>Type:</th>
                            <th>Category:</th>
                            <th>Company Name:</th>
                            <th>Minimum Sum:</th>
                            <th>Maximum Sum:</th>
                            <th>Status:</th>
                            <th width="220px">Actions</th>
                        </tr>
                        </thead>
                        <tbody id="tablebody">

                        @foreach($product as $index=>$pro)
                            <tr class="gradeX" id="row_{{ $pro->id }}">
                                <td class="index">
                                    {{ ++$index }}
                                </td>
                                <td class="name">
                                    {{ $pro->name }}
                                </td>
                                <td class="name">
                                    {{ $pro->code }}
                                </td>
                                <td class="name">
                                    {{ $pro->type }}
                                </td>
                                <td class="name">
                                    {{ $pro->category }}
                                </td>
                                <td class="name">
                                    {{ $pro->company->name }}
                                </td>
                                <td class="name">
                                    {{ $pro->min_sum ? $pro->min_sum : 'No Limit' }}
                                </td>
                                <td class="name">
                                    {{ $pro->max_sum ? $pro->max_sum : 'No Limit' }}
                                </td>
                                <td class="name">
                                    <form
                                        action="{{ route('admin.product.status') }}"
                                        method="POST"
                                    >
                                        @csrf
                                        @method('PATCH')

                                        <input type="hidden" name="id" value="{{ $pro->id }}">

                                        <div class="custom-control custom-switch">
                                            <input
                                                type="checkbox"
                                                class="custom-control-input"
                                                id="activeInactive{{$pro->id}}"
                                                @if($pro->is_active == 1) checked @endif
                                                onchange="this.form.submit()"
                                            >
                                            <label
                                                class="custom-control-label"
                                                for="activeInactive{{$pro->id}}">
                                                {{ $pro->is_active == 1 ? 'Active' : 'Inactive' }}
                                            </label>
                                        </div>
                                    </form>

                                </td>
                                <td class="justify-content-center">
                                    <a href="productEdit/{{$pro->id}}" id="{{ $pro->id }}" title="Edit ">
                                        <button
                                            class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button>
                                    </a>&nbsp;

                                    <a href="javascript:;" title="Delete product" class="delete-product"
                                       id="{{ $pro->id }}"><button type="button" class="btn btn-danger btn-flat"><i
                                                class="fa fa-trash"></i></button></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->

                <!-- /.box -->

            </div>
        </div>
    </div>
@endsection
