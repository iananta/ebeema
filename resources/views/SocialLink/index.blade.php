@extends('layouts.backend.containerlist')

@section('title')
    Social Links
@endsection

@section('footer_js')
    <script type="text/javascript">
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        });
        $('#tablebody').on('click', '.delete-link', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ url('/admin/social-link') }}" + "/" + id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            new swal(
                                'Success',
                                response.message,
                                'success'
                            )
                            $object.parent().parent().remove()
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });

        $('#tablebody').on('click', '.change-status', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You are going to change the status!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if(response.isConfirmed == true) {
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/admin/social/status') }}" + "/" + id,
                        data: {
                            'id': id,
                            _method: 'post',
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            swal.fire('Success', response.message, 'success');
                            if (response.link.is_active == 1) {
                                $($object).children().removeClass('bg-red').html(
                                    '<i class="fa fa-ban"></i> Not Active');
                                $($object).children().addClass('bg-green').html(
                                    '<i class="fa fa-check"></i> Active');
                                $($object).attr('title', 'Deactivate');
                            } else {
                                $($object).children().removeClass('bg-green').html(
                                    '<i class="fa fa-check"></i> Active');
                                $($object).children().addClass('bg-red').html(
                                    '<i class="fa fa-ban"></i> Not Active');
                                $($object).attr('title', 'Activate');
                            }
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });
    </script>
@endsection


@section('dynamicdata')

    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
                <a href="{{route('social-link.create')}}">
                    <button
                        class="btn btn-primary c-primary-btn add-modal shadow-none mx-2"><img
                            src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                    </button>
                </a>
            </div>
        </div>
        <div class="box-body px-4">
            <div class="dataTables_wrapper dt-bootstrap4">

                @include('layouts.backend.alert')

                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Link</th>
                        <th>Position</th>
                        <th>Icon</th>
                        <th>Status</th>
                        <th class="dt-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">

                    @foreach ($links as $index => $link)
                        <tr class="gradeX" id="row_{{ $link->id }}">
                            <td class="index">
                                {{ ++$index }}
                            </td>
                            <td class="name">
                                {{ $link->title }}
                            </td>
                            <td class="name">
                                {{ $link->link }}
                            </td>
                            <td class="name">
                                {{ $link->position }}
                            </td>
                            <td class="name">
                                <i class="{{ $link->icon }}"></i>
                            </td>
                            <td>
                                @if($link->is_active == 1)
                                    <a href="javascript:;" class="change-status" title="Deactivate"
                                       id="{{ $link->id }}">
                                        <button type="button"
                                                class="btn btn-sm bg-green btn-circle waves-effect waves-circle waves-float">
                                            <i class="fa fa-check"></i> Active
                                        </button>
                                    </a>
                                @else
                                    <a href="javascript:;" class="change-status" title="Activate"
                                       id="{{ $link->id }}">
                                        <button type="button"
                                                class="btn btn-sm bg-red btn-circle waves-effect waves-circle waves-float">
                                            <i class="fa fa-ban"></i> Not Active
                                        </button>
                                    </a>
                                @endif
                            </td>
                            <td class="justify-content-center">
                                <a href="{{route('social-link.edit',$link->id)}}" id="{{ $link->id }}" title="Edit">
                                    <button class="btn btn-primary btn-flat">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>&nbsp;

                                <a href="javascript:;" title="Delete" class="delete-link" id="{{ $link->id }}">
                                    <button type="button" class="btn btn-danger btn-flat">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
