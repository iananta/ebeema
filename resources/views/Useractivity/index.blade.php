@extends('layouts.backend.containerlist')

@section('title')
    User Activity Logs
@endsection
@section('footer_js')
    {!! $dataTable->scripts()  !!}
@endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        {!! $dataTable->table() !!}
    </div>
</div>
@endsection