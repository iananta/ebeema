@extends('layouts.backend.containerlist')

@section('title')
    User Activity Notifications
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
    {!! $dataTable->scripts()  !!}
    @endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        {!! $dataTable->table() !!}
    </div>
</div>>
@stop
