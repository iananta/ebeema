@extends('layouts.backend.containerlist')

@section('title')
    <p class="h4 align-center mb-0">User Activity Logs</p>
@endsection
@section('footer_js')
    <!-- formValidation -->
    <script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
    <script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var oTable = $('#policy-table').dataTable({
                "ordering": false,
                "scrollX": true
            });
        });

        function sendStatusToServer(id) {
            var p_id = id;
            var status_value = document.getElementById("statusValue").value;
            $.ajax({
                type: "post",
                dataType: "json",
                url: "{{ route('nonLife.calculator.motor.policy.status.change') }}",
                data: {
                    id: p_id, status: status_value,
                    _token: '{{csrf_token()}}'
                },
                success: function (response) {
                    // console.log(response);
                    if (response.status == "success") {
                        swal({
                            icon: "success",
                            text: response.message,
                        });
                    } else {
                        swal({
                            icon: "error",
                            text: response.message,
                        });
                        // console.log(response);
                    }
                }
            });

        }
    </script>
@endsection
@section('dynamicdata')
    <div class="box">
        <div class="text-right mr-3">
            <a class="btn btn-primary btn-sm" href="{{route('nonLife.calculator.policy.view',['download'=>1])}}">Download
                Excel</a>
{{--            <a class="btn btn-primary btn-sm" href="{{route('nonLife.calculator.all.document.upload')}}">Upload All Documents</a>--}}
        </div>
        @include('layouts.backend.alert')

        <div class="box-body" style="height: 100%;">
            <table id="policy-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>SN.</th>
                    <th>UserName</th>
                    <th>Path</th>
                    <th>Response</th>
                    <th>User Agent</th>
                    <th>IP No.</th>
                    <th>Error</th>
                    <th>Created Date</th>
                </tr>
                </thead>

                @foreach($users as  $key=>$user)
                <tbody id="tablebody">
                    <tr class="gradeX" id="">
                        <td>
                            <b>{{$user->id}}</b>
                        </td>
                        <td>
                            <b>{{$user->user->username}}</b>
                        </td>
                        <td>
                            <b>{{$user->path}}</b>
                        </td>
                        <td>
                            <b>{{$user->RESPONSE}}</b>
                        </td>
                        <td>
                            <b>{{$user->user_agent}}</b>
                        </td>
                        <td>
                            <b>{{$user->ip}}</b>
                        </td>
                        <td>
                            <b>{{$user->input}}</b>
                        </td>
                        <td>
                            <b>{{$user->created_at}}</b>
                        </td>
                </tbody>
            @endforeach
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop
