@extends('layouts.backend.containerform')

@section('footer_js')

    <script>
        $(document).ready(function () {
            $('.search-select').select2();
        });

        function findBlogcate() {
            var data = $('option:selected', '#blogcate').attr('data-products');
            data = JSON.parse(data);
            var html = '';
            html += '<option selected disabled>Select the Blog category</>';
            for (var i = 0; i < data.length; i++) {
                console.log(data[i]);
                html += '<option value="' + data[i].id + '">' + data[i].name + '</option>'
            }
            $('#products').html(html);

        }

    </script>
    <script src="https://cdn.tiny.cloud/1/2afio3obxs8z0uoygcpu91svi55711a2483oveukj73rtaj5/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script>

        tinymce.init({
            selector: '.text-editor',
            menubar: true,
            height: 600,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>

@endsection

@section('title')
    Edit Blog
@endsection
@section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="box-body">
            @include('layouts.backend.alert')
            <div class="policy-compare-wrapper">
                <div class="form-wrapper">
                    <form class="form" action="{{ route('blogs.update',$blogs->id) }}" method="POST"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        @method('PUT')
                        <div class="form-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Slug</div>
                            </div>
                            <input type="text" class="form-control wdth-full" placeholder="blog slug" value="{{$blogs->slug}}"
                                   name="slug">
                        </div>
                        <div class="form-group">
                            <label class="input-group-prepend">
                                <label>SEO Title</label>
                            <input type="text" class="form-control wdth-full" placeholder="meta title" value="{{$blogs->meta_title}}"
                                   name="meta_title">
                        </div>
                        <div class="form-group">
                            <div class="input-group-prepend">
                                <label for="meta_desc">Meta Description</label>
                            </div>
                            <input type="text" class="form-control wdth-full" placeholder="meta description" value="{{$blogs->meta_description}}"
                                   name="meta_description">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Blog Category:</label>
                            <select class="form-control search-select" onchange="findBlogcate()"
                                    id="blogcate"
                                    name="blogcate_id" required>
                                <option selected disabled>Select the Blog category</option>
                                @foreach($blogcates as $blogcate)
                                    <option value="{{ $blogcate->id }}"
                                            @if ($blogcate->id == $blogs->blogcate_id) selected @endif>
                                        {{ $blogcate->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Blog Title</label>
                            <input class="form-control" name="title" type="text" value="{{ $blogs->title }}"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Description:</label>
                            <textarea class="form-control text-editor"
                                      name="description">{{ $blogs->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Image:</label>
                            <small>[ Please use image of size 500*500 ]</small>
                            <div class="form-group">
                                <input type="file" class="form-control"
                                       name="image"/>
                                <br>
                                <img style="width:50%" src="/images/blogCategory/{{$blogs->image}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-submit-btn">Update</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
