@extends('layouts.backend.containerlist')

@section('title')
    All Blogs
@endsection

@section('footer_js')
    <script type="text/javascript">
        $(document).ready(function () {
            var oTable = $('.role-table').dataTable();

            $('#tablebody').on('click', '.delete-blog', function (e) {
                e.preventDefault();
                $object = $(this);
                var id = $object.attr('id');
                new swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (response) {
                    if (response.isConfirmed == true) {
                        $.ajax({
                            type: "DELETE",
                            url: "{{ url('/admin/blogs') }}" + "/" + id,
                            data: {
                                '_token': $('meta[name="csrf-token"]').attr('content')
                            },
                            beforeSend: function () {
                                $('#preloader').show();
                            },
                            dataType: 'json',
                            success: function (response) {
                                $('#preloader').hide();
                                var nRow = $($object).parents('tr')[0];
                                new swal(
                                    'Success',
                                    response.message,
                                    'success'
                                )
                                $object.parent().parent().remove()
                            },
                            error: function (e) {
                                $('#preloader').hide();
                                new swal('Oops...', 'Something went wrong!', 'error');
                            }
                        });
                    }
                }).catch(swal.noop);
            });
        });
    </script>
    <script src="https://cdn.tiny.cloud/1/2afio3obxs8z0uoygcpu91svi55711a2483oveukj73rtaj5/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>

        tinymce.init({
            selector: '.text-editor',
            menubar: true,
            height: 600,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
            content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
        });
    </script>
@endsection
@section('dynamicdata')
    <style>
        .wdth-full {
            width: 100% !important;
        }
    </style>
    <div class="box">
        <div class="box-body">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary float-right list-inline-item" data-toggle="modal"
                    data-target="#addBlog">
                Add New Blog
            </button>
            <!-- Modal -->
            <div class="modal fade" id="addBlog" tabindex="-1" role="dialog" aria-labelledby="addBlogTitle"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addBlogTitle">Add New Blog</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            @include('layouts.backend.alert')
                            <form class="form" action="{{ route('blogs.store') }}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Slug</div>
                                        </div>
                                        <input type="text" class="form-control wdth-full" placeholder="blog slug"
                                               name="slug">
                                    </div>
                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">SEO Title</div>
                                        </div>
                                        <input type="text" class="form-control wdth-full" placeholder="meta title"
                                               name="meta_title">
                                    </div>
                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Meta Description</div>
                                        </div>
                                        <input type="text" class="form-control wdth-full" placeholder="meta description"
                                               name="meta_description">
                                    </div>
                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Blog Category</div>
                                        </div>
                                        <select class="form-control wdth-full" name="blogcate_id" required>
                                            <option selected disabled>Select the Blog category</option>
                                            @foreach($blogcates as $blogcate)
                                                <option value="{{ $blogcate->id }}">{{ $blogcate->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Title</div>
                                        </div>
                                        <input type="text" class="form-control wdth-full" placeholder="title"
                                               name="title">
                                    </div>


                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Description</div>
                                        </div>
                                        <textarea class="form-control text-editor" name="description"
                                                  placeholder="description"></textarea>
                                    </div>

                                    <div class="form-group col-md-12 p-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Image</div>
                                        </div>
                                        <input type="file" class="form-control wdth-full" name="image">
                                        <small>[ Please use image of size 500*500 ]</small>
                                    </div>

                                    <button type="submit" class="btn btn-primary ml-2 mt-2 mb-2">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="dataTables_wrapper dt-bootstrap4">

            <div class="box-body">

                @include('layouts.backend.alert')

                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Blog Category</th>
                        <th>Title</th>
                        <td>Created Date</td>
                        <td>Image</td>
                        <th class="dt-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    <?php $i = 1; ?>
                    @foreach($blogs as $index=>$blog)
                        <tr class="gradeX" id="row_{{ $blog->id }}">
                            <td>{{ $i++ }}</td>
                            {{-- <td> @isset($blog->blogcate){{ $blog->blogcate->name }} @endisset</td> --}}
                            <td class="category-name">
                                {{@$blog->category->name}}
                            </td>
                            <td class="title">
                                {{ $blog->title }}
                            </td>
                            <td>{{ $blog->created_at }}</td>
                            <td style="width: 15%" class="compare-parts line-rht-cmp" scope="row">
                                <img style="max-width:100%" src="/images/blogCategory/{{$blog->image}}">

                            </td>

                            <td class="justify-content-center">

                                <a href="{{route('blogs.edit',$blog->id)}}" id="{{ $blog->id }}"
                                   title="Edit blog">
                                    <button class="btn btn-primary btn-flat"><i
                                            class="fa fa-edit"></i></button>
                                </a>&nbsp;

                                <a href="javascript:;" title="Delete Blogs" class="delete-blog"
                                   id="{{ $blog->id }}">
                                    <button type="button" class="btn btn-danger btn-flat"><i
                                            class="fa fa-trash"></i></button>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->

            <!-- /.box -->
        </div>
    </div>
@endsection
