@extends('layouts.backend.containerform')

@section('footer_js')

<script type="text/javascript">
    $(document).ready(function () {

        $(document).ready(function () {
            $('#roleAddForm').formValidation({
                framework: 'bootstrap',
                excluded: ':disabled',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    role: {
                        // validators: {
                        //   notEmpty: {
                        //     message: 'Role Name field is required.'
                        //   }
                        // }
                    }
                },
            });
        });

        $('.check-module').on('change', function (e) {
            if ($(this).is(':checked')) {
                $(this).closest('tr').find('.check').prop('checked', true);
            } else {
                $(this).closest('tr').find('.check').prop('checked', false);
            }
        });

        $('.check').on('change', function (e) {
            var checked = $(this).closest('table').parent().parent().find('.check:checked').length;
            var total = $(this).closest('table').parent().parent().find('.check').length;

            if (checked == 0) {
                $(this).closest('table').parent().parent().find('.check-module').prop('checked', false);
            } else {
                $(this).closest('table').parent().parent().find('.check-module').prop('checked', true);
            }
        });

    });

</script>


@endsection
@section('title')
<p class="h4 align-center mb-0">Blog Category</p>
@endsection
@section('dynamicdata')

<!-- iCheck -->
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Blog Category</h3>
    </div>
    <div class="box-body">
        @include('layouts.backend.alert')

        <form class="form-inline" action="{{ route('blogcates.update',$blogcates->id) }}" method="POST" enctype="multipart/form-data"> 
        @csrf
        @method('PUT')
     
         <div class="column">
           
                <div class="">
                    
                    <div class="card-inside-title">Name:</div>
                    <input type="text" name="name" value="{{ $blogcates->name }}" class="form-control form-control-inline input-medium" size="50" placeholder="Blog Category">
                </div>
            
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
     
    </form>
        <!-- /.box-body -->
    </div>
</div>

@stop
