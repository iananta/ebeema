@extends('layouts.backend.containerlist')
@section('title')
Blog Category
@endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="box-header">
            <h3 class="box-title">Blog Category</h3>
        </div>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary float-right list-inline-item" data-toggle="modal"
                data-target="#addBlog">
            Add New Blog
        </button>

        <!-- Modal -->
        <div class="modal fade" id="addBlog" tabindex="-1" role="dialog" aria-labelledby="addBlogTitle"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addBlogTitle">Add New Blog</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form class="form-inline" action="{{ route('blogcates.store') }}" method="post">
                            @csrf
                            <div class="form-row">
                                <div class="input-group mb-2 mr-sm-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Name</div>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Blog Category" name="name">
                                </div>
                                <br>
                                <br><br>

                                <button type="submit" class="btn btn-primary ml-2 mt-2 mb-2">Submit</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper dt-bootstrap4">
            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th class="dt-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody id="tablebody">
                    <?php $i = 1; ?>
                    @foreach($blogcates as $index=>$blogcate)
                    <tr class="gradeX" id="row_{{ $blogcate->id }}">
                        <td>{{ $i++ }}</td>
                        <td class="name">
                            {{ $blogcate->name }}
                        </td>
                        <td class="justify-content-center">
                            {{-- <a class="edit-role" href="{{ route('admin.privilege.role.edit', $role->id) }}"
                                    id="{{ $role->id }}"
                                    title="Edit Role">
                                &nbsp;<i class="fa fa-pencil"></i>
                            </a>&nbsp; --}}

                            <a href="{{route('blogcates.edit',$blogcate->id)}}" id="{{ $blogcate->id }}"
                               title="Edit blogcate">
                                <button class="btn btn-primary btn-flat"><i
                                        class="fa fa-edit"></i></button>
                            </a>&nbsp;
                            <a href="javascript:;" title="Delete Blog Category" class="delete-blogCat"
                               id="{{ $blogcate->id }}">
                                <button type="button" class="btn btn-danger btn-flat"><i
                                        class="fa fa-trash"></i></button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <!-- /.box -->
            @endsection
        </div>
    </div>
</div>
@section('footer_js')
<script type="text/javascript">
    $(document).ready(function () {
        var oTable = $('.role-table').dataTable();

        $('#tablebody').on('click', '.delete-blogCat', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ url('/admin/blogcates') }}" + "/" + id,
                        data: {
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        dataType: 'json',
                        success: function (response) {
                            $('#preloader').hide();
                            var nRow = $($object).parents('tr')[0];
                            new swal(
                                'Success',
                                response.message,
                                'success'
                            )
                            $object.parent().parent().remove()
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });
    });

</script>
@endsection
