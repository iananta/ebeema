@extends('layouts.backend.containerform')

@section('footer_js')
    <script type="text/javascript">
        $(".addTableRow").click(function () {
            var $tableBody = $(this).parents('table').find("tbody"),
                $trLast = $tableBody.find("tr:last"),
                $trNew = $trLast.clone();
            $trNew.find('button').text('Remove').removeClass('btn-success addTableRow').addClass('btn-danger remove-tr');
            $trLast.find('input').val('');
            $trLast.before($trNew);
            // $(this).parents('table').append('<tr><td><input type="text" name="imepay_title[]" placeholder="Enter the IMEPAY Title" class="form-control"/></td><td><input type="text" name="imepay_value[]" placeholder="Enter Imepay Title value" class="form-control"/></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
        });

        $(document).on('click', '.remove-tr', function () {
            $(this).parents('tr').remove();
        });

    </script>
@endsection @section('dynamicdata')

    <!-- iCheck -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Company Details</h3>
        </div>
        <div class="box-body">
            @include('layouts.backend.alert')
            <form action="{{ route('admin.company.details.update',$company->id) }}" method="POST" content
                  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="company_id" value="{{$company->id}}">
                <input type="hidden" name="company_details_id" value="{{$company->details->id ?? ''}}">
                <div class="form-group">
                    <p class="card-inside-title">Company Activate/ Deactivate</p>
                    <div class="form-line">
                        <input name="status" type="checkbox" @if($company->details && $company->details->status == 1) checked @endif value="1" title="Tick company to enable"/>
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Company API URL *</p>
                    <div class="form-line">
                        <input class="form-control form-control-inline input-medium" name="company_api" type="text"
                               value="{{$company->details->api_url ?? ''}}" placeholder="Enter company API URL"/>
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">API Details *</p>
                    <div class="form-line">
                        <table class="table table-bordered" id="dynamicTable">
                            <tr>
                                <th>Title*</th>
                                <th>Value*</th>
                                <th>Action</th>
                            </tr>
                            @if(isset($company->details->api_details))
                                @foreach(json_decode($company->details->api_details) as $key => $cred)
                                    <tr>
                                        <td><input type="text" name="api_titles[]"
                                                   placeholder="Enter the API Title" value="{{$key ?? ''}}"
                                                   class="form-control"/></td>
                                        <td><input type="text" name="api_values[]" value="{{$cred ?? ''}}"
                                                   placeholder="Enter API Title value" class="form-control"/></td>
                                        <td>
                                            <button type="button" class="btn btn-danger remove-tr">Remove</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr class="tr_clone">
                                <td><input type="text" name="api_titles[]"
                                           placeholder="Enter the API Title"
                                           class="form-control"/></td>
                                <td><input type="text" name="api_values[]"
                                           placeholder="Enter API Title value" class="form-control"/></td>
                                <td>
                                    <button type="button" name="add" id="add" class="btn btn-success addTableRow">Add
                                        More
                                    </button>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </div>
                </div>
                <div class="form-group">
                    <p class="card-inside-title">Company IME PAY Details *</p>
                    <div class="form-line">
                        <table class="table table-bordered" id="dynamicTable">
                            <tr>
                                <th>Title*</th>
                                <th>Value*</th>
                                <th>Action</th>
                            </tr>
                            @if(isset($company->details->imepay_credentials))
                                @foreach(json_decode($company->details->imepay_credentials) as $key => $cred)
                                    <tr>
                                        <td><input type="text" name="imepay_title[]"
                                                   placeholder="Enter the IMEPAY Title" value="{{$key ?? ''}}"
                                                   class="form-control"/></td>
                                        <td><input type="text" name="imepay_value[]" value="{{$cred ?? ''}}"
                                                   placeholder="Enter Imepay Title value" class="form-control"/></td>
                                        <td>
                                            <button type="button" class="btn btn-danger remove-tr">Remove</button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            <tr class="tr_clone">
                                <td><input type="text" name="imepay_title[]"
                                           placeholder="Enter the IMEPAY Title"
                                           class="form-control"/></td>
                                <td><input type="text" name="imepay_value[]"
                                           placeholder="Enter Imepay Title value" class="form-control"/></td>
                                <td>
                                    <button type="button" name="add" id="add" class="btn btn-success addTableRow">Add
                                        More
                                    </button>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-info waves-effect">
                    <span>Update
                        <i class="fa fa-check"></i>
                    </span>
                    </button>
                </div>


        </div>
    </div>
    <!-- #END# Basic Table -->
    </form>

    <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
    <!-- /.col (right) -->
    </div>
    <!-- /.row -->
@stop
