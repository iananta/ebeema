@extends('layouts.backend.containerlist')

@section('title')
    Companies
@endsection

@section('dynamicdata')

<div class="box">
    <div class="box-header with-border c-btn-right d-flex-row ">
        <div class="justify-content-end list-group list-group-horizontal ">
            <a href="{{route('admin.companies.create')}}"><button
                    class="btn btn-primary c-primary-btn add-modal shadow-none mx-2"><img
                        src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp; Add New &nbsp;
                </button></a>
        </div>
    </div>

    <div class="box-body">
        <div class="dataTables_wrapper dt-bootstrap4">
            <!-- <div class="box-header">
        <h3 class="box-title">ROLES</h3>
        <ul class="header-dropdown m-r--5 pull-right">
          <li class="dropdown" style="list-style : none;">
               <a href="{{ route('admin.privilege.role.create') }}"><button type="button" class="btn btn-primary waves-effect">ADD NEW <b>+</b></button></a>
            </li>
        </ul>
      </div>

    </.box-header -->
            <div class="box-body">

                @include('layouts.backend.alert')

                <table id="example1" class="table table-bordered table-hover role-table">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Company Name</th>
                            <th>code</th>
                            <th>Type</th>
                            <th>Logo</th>
                            <th>Priority</th>
                            <th>Status</th>
                            <th class="dt-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="tablebody">

                        @foreach($companies as $index=>$company)
                        <tr class="gradeX" id="row_{{ $company->id }}">
                            <td class="index">
                                {{ ++$index }}
                            </td>
                            <td class="name">
                                {{ $company->name }}
                            </td>
                            <td class="name">
                                {{ $company->code }}
                            </td>
                            <td class="name">
                                {{ $company->type }}
                            </td>
                            <td style="width: 20%" class="compare-parts line-rht-cmp" scope="row">
                                <img style="max-width:100%" src="/images/company/{{$company->logo}}">
                            </td>
                            <td class="name">
                                {{ $company->priority }}
                            </td>
                            <td class="name">
                                @if($company->status == 1)
                                    <a href="javascript:;" class="change-status" title="Deactivate"
                                       id="{{ $company->id }}">
                                        <button type="button"
                                                class="btn btn-sm bg-green btn-circle waves-effect waves-circle waves-float">
                                            <i class="fa fa-check"></i> Active
                                        </button>
                                    </a>
                                @else
                                    <a href="javascript:;" class="change-status" title="Activate"
                                       id="{{ $company->id }}">
                                        <button type="button"
                                                class="btn btn-sm bg-red btn-circle waves-effect waves-circle waves-float">
                                            <i class="fa fa-ban"></i> Not Active
                                        </button>
                                    </a>
                                @endif

                            </td>
                            <td class="justify-content-center action-buttons">
                                {{-- <a class="edit-role" href="{{ route('admin.privilege.role.edit', $role->id) }}"
                                id="{{ $role->id }}"
                                title="Edit Role">
                                &nbsp;<i class="fa fa-pencil"></i>
                                </a>&nbsp; --}}

                                <a href="{{route('admin.companies.edit',$company->id)}}" id="{{ $company->id }}"
                                title="Edit company"><button class="btn btn-primary btn-flat"><i
                                        class="fa fa-edit"></i></button></a>&nbsp;

                                <a href="{{route('admin.company.details',$company->id)}}" title="Comapany Details" class="btn btn-primary btn-flat mr-1"><i class="fa fa-info"></i></a>

                                <a href="{{route('admin.company.product',$company->id)}}" id="{{ $company->id }}"
                                    title="Comapany Products" class="btn btn-primary btn-flat"><i class="fa fa-file"></i></a>&nbsp;

                                <a href="javascript:;" title="Delete company" class="delete-company"
                                    id="{{ $company->id }}"><button type="button" class="btn btn-danger btn-flat"><i
                                            class="fa fa-trash"></i></button></a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->

            <!-- /.box -->
            @endsection
        </div>
    </div>
</div>
@section('footer_js')
<script type="text/javascript">
    $(document).ready(function() {
        var oTable = $('.role-table').dataTable();

        $('#tablebody').on('click', '.delete-company', function(e){
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function(response) {
            if(response.isConfirmed == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/companies') }}" + "/" + id,
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        oTable.fnDeleteRow(nRow);
                        new swal('success', response.message, 'success').catch(swal.noop);
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error').catch(swal.noop);
                    }
                });
            }
        }).catch(swal.noop);
        });
    });
    $('#tablebody').on('click', '.change-status', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You are going to change the status!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
        }).then(function (response) {
            if(response.isConfirmed == true) {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin/company-update-status') }}" + "/" + id,
                    data: {
                        'id': id,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        new swal('Success', response.message, 'success');
                        if (response.cat.is_active == 1) {
                            $($object).children().removeClass('bg-red').html(
                                '<i class="fa fa-ban"></i> Not Active');
                            $($object).children().addClass('bg-green').html(
                                '<i class="fa fa-check"></i> Active');
                            $($object).attr('title', 'Deactivate');
                        } else {
                            $($object).children().removeClass('bg-green').html(
                                '<i class="fa fa-check"></i> Active');
                            $($object).children().addClass('bg-red').html(
                                '<i class="fa fa-ban"></i> Not Active');
                            $($object).attr('title', 'Activate');
                        }
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        });
    });

</script>
@endsection
