@extends('layouts.backend.containerform')
@section('title')
    CRC Rate
@endsection
@section('footer_js')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        });
    </script>

<script type="text/javascript">
    $(document).ready(function () {

        $(document).ready(function () {
            $('#roleAddForm').formValidation({
                framework: 'bootstrap',
                excluded: ':disabled',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    role: {
                        // validators: {
                        //   notEmpty: {
                        //     message: 'Role Name field is required.'
                        //   }
                        // }
                    }
                },
            });
        });

        $('.check-module').on('change', function (e) {
            if ($(this).is(':checked')) {
                $(this).closest('tr').find('.check').prop('checked', true);
            } else {
                $(this).closest('tr').find('.check').prop('checked', false);
            }
        });

        $('.check').on('change', function (e) {
            var checked = $(this).closest('table').parent().parent().find('.check:checked').length;
            var total = $(this).closest('table').parent().parent().find('.check').length;

            if (checked == 0) {
                $(this).closest('table').parent().parent().find('.check-module').prop('checked', false);
            } else {
                $(this).closest('table').parent().parent().find('.check-module').prop('checked', true);
            }
        });

    });
</script>
<script>

    $(document).ready(function () {
        $('.search-select').select2();
    });

    function findProduct() {
        // console.log('im here');
        var data = $('option:selected', '#company').attr('data-products');
        data = JSON.parse(data);
        // console.log(data);
        var html = '';
        html += '<option selected disabled>Select the Product</>';
        for (var i = 0; i < data.length; i++) {
            console.log(data[i]);
            html += '<option value="' + data[i].id + '">' + data[i].name + '</option>'
        }
// console.log(html);
        $('#products').html(html);

    }
</script>
<script>

    $('#tablebody').on('click', '.delete-crc', function (e) {
        e.preventDefault();
        $object = $(this);
        var id = $object.attr('id');
        new swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function (response) {
            // console.log(response);
            if (response.isConfirmed == true || response.value == true) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/crcrate/delete') }}" + "/" + id,
                    data: {
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: function () {
                        $('#preloader').show();
                    },
                    dataType: 'json',
                    success: function (response) {
                        $('#preloader').hide();
                        var nRow = $($object).parents('tr')[0];
                        new swal(
                            'Success',
                            response.message,
                            'success'
                        )
                        $object.parent().parent().remove()
                    },
                    error: function (e) {
                        $('#preloader').hide();
                        new swal('Oops...', 'Something went wrong!', 'error');
                    }
                });
            }
        }).catch(swal.noop);
    });
</script>

@endsection
@section('dynamicdata')

<!-- iCheck -->
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">CRC rate of child</h3>
    </div>
    <div class="box-body">
        @include('layouts.backend.alert')

        <form class="form-inline" action="{{ route('admin.crcrate.store') }}" method="post">
            @csrf
            <label class="sr-only" for="inlineFormInputGroupUsername2">Age</label>
            <div class="input-group mb-2 mr-sm-2">

                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Age.."
                       name="age_id">
            </div>

            <label class="sr-only" for="inlineFormInputName2">One Time Charge</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2"
                   placeholder="one time charge.." name="one_time_charge">

            <select class="form-control input-medium search-select"
                    onchange="findProduct()" id="company" name="company_id" required>
                <option selected disabled>Select the Company</option>
                @foreach ($companies as $company)
                <option data-products="{{$company->child_products()}}"
                        value="{{ $company->id }}">{{ $company->name }}
                </option>
                @endforeach
            </select>

            <select class="form-control input-medium search-select" id="products" name="product_id" required>
                <option selected disabled>Select the Product</option>
            </select>

            <button type="submit" class="btn btn-primary ml-2 mt-2 mb-2">Submit</button>
        </form>
        <!-- /.box-body -->
    </div>
    <div class="box-header">
        <h3 class="box-title">Previous Crc Rates of Child Endowment</h3>
    </div>
    <table id="example1" class="table table-bordered table-hover role-table">
        <thead>
        <tr>
            <th>Age</th>
            <th>One time charge</th>
            <th>Company name</th>
            <th>Product name</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody id="tablebody">
        @foreach($crcrates as $crcrate)
        <tr class="gradeX" id="row_{{ $crcrate->id }}">
            <td>{{$crcrate->age ? $crcrate->age->age : 'N/A'}}</td>
            <td>{{$crcrate->one_time_charge}}</td>

            <td> @isset($crcrate->company){{ $crcrate->company->name}} @endisset</td>
            <td> @isset($crcrate->product){{ $crcrate->product->name}} @endisset</td>
            <td class="justify-content-center">

                <a href="{{ route('admin.crcrate.edit',$crcrate->id) }}" id="{{ $crcrate->id }}"
                   title="Edit ">
                    <button class="btn btn-primary btn-flat"><i class="fa fa-edit"></i></button>
                </a>&nbsp;

                <a href="javascript:;" title="Delete CRC" class="delete-crc"
                   id="{{ $crcrate->id }}">
                    <button type="button" class="btn btn-danger btn-flat"><i
                            class="fa fa-trash"></i></button>
                </a>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- /.box -->
</div>

<!-- /.col (right) -->
</div>
<!-- /.row -->
@endsection
