@extends('layouts.backend.containerlist')

@section('title')
Mail Template Add
@endsection
@section('footer_js')
    <script>
        ClassicEditor
            .create(document.querySelector('#notes'))
            .catch(error => {
                console.error(error);
            });
    </script>
    @endsection
@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{ route('admin.mail.store') }}">
                    @csrf

                    <div class="custom-row">
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Title</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Enter title of mail" value="{{ old('title') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Code</label>
                            </div>
                            <div class="form-input">
                                <input type="text" data-validation="required" class="form-control" id="key" name="key"
                                       placeholder="Enter key of mail" value="{{ old('key') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>to,</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="to" name="to"
                                       placeholder="Enter mailing address of the recipient" value="{{ old('to') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>C.C.</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="cc" name="cc"
                                       placeholder="Enter mailing address for C.C." value="{{ old('cc') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>B.C.C.</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="bcc" name="bcc"
                                       placeholder="Enter mailing address for B.C.C." value="{{ old('bcc') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Subject</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="subject" name="subject"
                                       placeholder="Enter subject of the mail" value="{{ old('subject') }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Body</label>
                            </div>
                            <div class="form-input">
                                <textarea
                                    class="ckeditor form-control"
                                    name="body"
                                    id="body"
                                    placeholder="Body of the mail"
                                    value="{{ old('body') }}"
                                ></textarea>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Status</label>
                            </div>
                            <div class="form-input">
                                <select class="input-control search-select  validate-check" id="status" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Deactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Notes</label>
                            </div>
                            <div class="form-input">
                                <textarea
                                    class="form-control"
                                    name="notes"
                                    id="notes"
                                    placeholder="Notes:"
                                    value="{{ old('notes') }}"
                                ></textarea>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="form-input-details-wrapper">

            </div>
        </div>
    </div>
</div>
@endsection
