@extends('layouts.backend.containerlist')

@section('title')
Mail Template Edit
@endsection

@section('dynamicdata')
<div class="box">
    <div class="box-body">
        @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
            <div class="form-wrapper">
                <form class="form-data" method="POST" action="{{ route('admin.mail.update', $mail->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="custom-row">
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Title</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Enter title of mail" value="{{ $mail->title }}"/>
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>to,</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="to" name="to"
                                       placeholder="Enter mailing address of the recipient" value="{{ $mail->to }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>C.C.</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="cc" name="cc"
                                       placeholder="Enter mailing address for C.C." value="{{ $mail->cc }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>B.C.C.</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="bcc" name="bcc"
                                       placeholder="Enter mailing address for B.C.C." value="{{ $mail->bcc }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Subject</label>
                            </div>
                            <div class="form-input">
                                <input type="text" class="form-control" id="subject" name="subject"
                                       placeholder="Enter subject of the mail" value="{{ $mail->subject }}">
                            </div>
                        </div>
                        <div class="form-input-wrapper">
                            <div class="form-label">
                                <label>Body</label>
                            </div>
                            <div class="form-input">
                                <textarea
                                    class="ckeditor form-control"
                                    name="body"
                                    id="body"
                                    placeholder="Body of the mail"
                                    value="{{ $mail->body }}"
                                >{!! $mail->body !!}</textarea>
                            </div>
                        </div>

                        <div class="form-input-wrapper">
                            <div class="form-label"></div>
                            <div class="form-input">
                                <button type="submit" id="submit-btn" class="form-submit-btn">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
                <a href="{{ route('admin.mail.reset', $mail->id) }}" onclick="return resetBody()" class="btn btn-danger float-right mt-2">Reset Form</a>
            </div>
            <div class="form-input-details-wrapper">
                <div class="alert"
                     style="color: #004085;background-color: #cce5ff;border-color: #b8daff;margin-bottom:0;margin-top:10px;">
                    <center><strong>{{ ('Note') }}:</strong></center>
                    <br>
                    <p>
                        {{ ('DO NOT Modify data that are inside { } brackets!') }}!
                    </p>
                    <p>{!! $mail->notes !!}</p>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_js')
    <script>
        function resetBody()
        {
            var agree=confirm("Are you sure you wish to continue?");
            if (agree)
                return true ;
            else
                return false ;
        }
    </script>
@endsection

