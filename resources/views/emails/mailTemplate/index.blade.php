@extends('layouts.backend.containerlist')

@section('footer_js')
<!-- formValidation -->
<script src="{{ asset('backend/js/formValidation/formValidation.min.js') }}"></script>
<script src="{{ asset('backend/js/formValidation/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $('#mail-table').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    });
    $('#tablebody').on('click', '.change-status', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You are going to change the status!",
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if(response.isConfirmed == true) {
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/admin/mail/status/') }}" + "/" + id,
                        data: {
                            'id': id,
                            _method: 'post',
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            swal.fire('Success', response.message, 'success');
                            if (response.mail.status == 1) {
                                $($object).children().removeClass('bg-red').html(
                                    '<i class="fa fa-ban"></i> Not Active');
                                $($object).children().addClass('bg-blue').html(
                                    '<i class="fa fa-check"></i> Active');
                                $($object).attr('title', 'Deactivate');
                            } else {
                                $($object).children().removeClass('bg-blue').html(
                                    '<i class="fa fa-check"></i> Active');
                                $($object).children().addClass('bg-red').html(
                                    '<i class="fa fa-ban"></i> Not Active');
                                $($object).attr('title', 'Activate');
                            }
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });

</script>
@endsection

@section('title')
Mail Template
@endsection

@section('dynamicdata')


<div class="box">
    <div class="box-header with-border c-btn-right d-flex-row ">
        <div class="justify-content-end list-group list-group-horizontal ">
            <a href="{{route('admin.mail.create')}}">
            <button class="btn btn-primary btn-sm c-primary-btn add-modal shadow-none mx-2" data-toggle="modal"
                    data-target="#addNewMailModel"><img src="{{ asset('uploads/add-circle-16-Regular.svg') }}"
                                                        alt="Add-icon"> &nbsp; Add New Mail &nbsp;
            </button>
            </a>
        </div>
    </div>

    <div class="box-body">
        <table id="mail-table" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>S.No.</th>
                <th>Title</th>
                <th>Code *</th>
                <th>Subject</th>
                <th>Status</th>
                <th class="dt-center">Actions</th>
            </tr>
            </thead>
            <tbody id="tablebody">
            @foreach($mail as $index=>$mail)
            <tr class="gradeX" id="row_{{ $mail->id }}">
                <td class="index">
                    {{ ++$index }}
                </td>
                <td class="title">
                    {{ $mail->title }}
                </td>
                <td class="key">
                    {{ $mail->key }}
                </td>
                <td class="subject">
                    {{ $mail->subject }}
                </td>
                <td class="status">
                    @if($mail->status == 1)
                    <a href="javascript:;" class="change-status" title="Deactivate" id="{{ $mail->id }}">
                        <button type="button"
                                class="btn btn-sm bg-blue btn-circle waves-effect waves-circle waves-float">
                            <i class="fa fa-check"></i> Active
                        </button>
                    </a>
                    @else
                    <a href="javascript:;" class="change-status" title="Activate" id="{{ $mail->id }}">
                        <button type="button"
                                class="btn btn-sm bg-red btn-circle waves-effect waves-circle waves-float">
                            <i class="fa fa-ban"></i> Not Active
                        </button>
                    </a>
                    @endif
                </td>

                <td class="justify-content-center d-flex p-2">
                    <a href="{{route('admin.mail.edit',$mail->id)}}" id="{{ $mail->id }}"
                       title="Edit mail"><button class="btn btn-primary btn-flat"><i
                                class="fa fa-edit"></i></button></a>&nbsp;
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@stop
