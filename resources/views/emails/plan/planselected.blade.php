<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0;">
    <meta name="format-detection" content="telephone=no"/>
    <style>

body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important;}
body, table, td, div, p, a { -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; border-spacing: 0; }
img { border: 0; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
#outlook a { padding: 0; }
.ReadMsgBody { width: 100%; } .ExternalClass { width: 100%; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

/* Rounded corners for advanced mail clients only */
@media all and (min-width: 560px) {
    .container { border-radius: 8px; -webkit-border-radius: 8px; -moz-border-radius: 8px; -khtml-border-radius: 8px;}
}

/* Set color for auto links (addresses, dates, etc.) */
a, a:hover {
    color: #127DB3;
}
.footer a, .footer a:hover {
    color: #999999;
}

    </style>

    <!-- MESSAGE SUBJECT -->
    <title>Get this responsive email template</title>

</head>
<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
    background-color: #F0F0F0;
    color: #000000;"
    bgcolor="#F0F0F0"
    text="#000000">

<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;" class="background"><tr><td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
    bgcolor="#F0F0F0">

<!-- WRAPPER -->
<!-- Set wrapper width (twice) -->
<table border="0" cellpadding="0" cellspacing="0" align="center"
    width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
    max-width: 560px;" class="wrapper">

    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
            padding-top: 20px;
            padding-bottom: 20px;">
            <div style="display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
            color: #F0F0F0;" class="preheader">
                Available on&nbsp;GitHub and&nbsp;CodePen. Highly compatible. Designer friendly. More than 50%&nbsp;of&nbsp;total email opens occurred on&nbsp;a&nbsp;mobile device&nbsp;— a&nbsp;mobile-friendly design is&nbsp;a&nbsp;must for&nbsp;email campaigns.</div>
            <a target="_blank" style="text-decoration: none;"
                href="{{url('/')}}"><img border="0" vspace="0" hspace="0"
                src="{{asset('frontend/img/logo.png')}}"
                width="150" height="50"
                alt="Logo" title="Logo" style="
                color: #000000;
                font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" /></a>

        </td>
    </tr>

<!-- End of WRAPPER -->
</table>

<!-- WRAPPER / CONTEINER -->
<!-- Set conteiner background color -->

<table border="0" cellpadding="0" cellspacing="0" align="center"
    bgcolor="#FFFFFF"
    width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
    max-width: 560px;width:560px" class="container">

    <!-- HEADER -->
    <!-- Set text color and font family ("sans-serif" or "Georgia, serif") -->
    <tr style="padding: 20px 15px;display: block;overflow: hidden;">
        <td>  <?php echo $mail; ?>
            {{ config('app.name') }}</td>
    </tr>
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
            padding-top: 25px;
            color: #000000;
            font-family: sans-serif;" class="header">
                Plan Selected
        </td>
    </tr>





    <!-- LINE -->
    <!-- Set line color -->
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
            padding-top: 25px;" class="line"><hr
            color="#E0E0E0" align="center" width="100%" size="1" noshade style="margin: 0; padding: 0;" />
        </td>
    </tr>

    <!-- LIST -->
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%;" class="list-item"><table align="center" border="0" cellspacing="0" cellpadding="0" style="width: inherit; margin: 0; padding: 0; border-collapse: collapse; border-spacing: 0;">

            <!-- LIST ITEM -->
            <tr>


            </tr>

            <!-- LIST ITEM -->
            <tr>

                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Full Name</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['name']}}</b>
                </td>


            </tr>
            <tr>
                 <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Phone Number</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['phone']}}</b>
                </td>

            </tr>
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Email</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['email']}}</b>
                </td>
            </tr>
             <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Date of Birth</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['birth_year']}}/{{$data['birth_month']}}/{{$data['birth_date']}}</b>
                </td>
            </tr>
            @if($data['age'])
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Age</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['age'] }} Yrs</b>
                </td>
            </tr>
            @endif
            @if($data['child_age'])
             <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Child Age</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['child_age'] }} Yrs</b>
                </td>
            </tr>
            @endif
            @if($data['proposer_age'])
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Proposer Age</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['proposer_age'] }} Yrs</b>
                </td>
            </tr>
            @endif
            @if($data['husband_age'])
             <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Husband Age</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['husband_age'] }} Yrs</b>
                </td>
            </tr>
            @endif
             @if($data['wife_age'])
             <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Wife Age</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['Wife age'] }} Yrs</b>
                </td>
            </tr>
            @endif
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Plan Selected</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['plan_selected']}}</b>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Term</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">{{$data['term']}} Yrs</b>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Sum Assured</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">Rs.{{$data['sum_assured']}}</b>
                </td>
            </tr>
             <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Bonus</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">Rs.{{$data['bonus']}}</b>
                </td>
            </tr>
             <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Net Gain</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">Rs.{{$data['net_gain']}}</b>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="border-collapse: collapse; border-spacing: 0;
                    padding-top: 30px;
                    padding-right: 20px;font-size:17px;color:#3D538C">Premium</td>

                <td align="left" valign="top" style="font-size: 16px; font-weight: 400; line-height: 160%; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
                    padding-top: 25px;
                    color: #000000;
                    font-family: sans-serif;" class="paragraph">
                        <b style="color: #333333;">Rs.{{$data['premium']}}</b>
                </td>
            </tr>
        </table></td>
    </tr>

    <!-- LINE -->
    <!-- Set line color -->
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
            padding-top: 25px;" class="line"><hr
            color="#E0E0E0" align="center" width="100%" size="1" noshade style="margin: 0; padding: 0;" />
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
            padding-top: 20px;
            padding-bottom: 25px;
            color: #000000;
            font-family: sans-serif;" class="paragraph">
                Have a&nbsp;question? <a href="mailto:hi@ebeema.com" target="_blank" style="color: #127DB3; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 160%;">hi@ebeema.com</a>
        </td>
    </tr>

<!-- End of WRAPPER -->
</table>

<!-- WRAPPER -->
<!-- Set wrapper width (twice) -->
<table border="0" cellpadding="0" cellspacing="0" align="center"
    width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
    max-width: 560px;margin-bottom:30px" class="wrapper">
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 80.5%;
            padding-top: 25px;" class="social-icons"><table
            width="256" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; border-spacing: 0; padding: 0;">
            <tr>

                <!-- ICON 1 -->
                <td align="center" valign="middle" style="margin: 0; padding: 0; border-collapse: collapse; border-spacing: 0;"><a target="_blank"
                    href="#"
                style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
                    color: #000000;"
                    alt="F" title="Facebook"
                    width="44" height="44"
                    src="https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/facebook.png"></a></td>

                <!-- ICON 2 -->
                <td align="center" valign="middle" style="margin: 0; padding: 0; border-collapse: collapse; border-spacing: 0;"><a target="_blank"
                    href="#"
                style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
                    color: #000000;"
                    alt="T" title="Twitter"
                    width="44" height="44"
                    src="https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/twitter.png"></a></td>
                <!-- ICON 3 -->
                <td align="center" valign="middle" style="margin: 0; padding: 0; border-collapse: collapse; border-spacing: 0;"><a target="_blank"
                    href="#"
                style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
                    color: #000000;"
                    alt="I" title="Instagram"
                    width="44" height="44"
                    src="https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/instagram.png"></a></td>

            </tr>
            </table>
        </td>
    </tr>


<!-- End of WRAPPER -->
</table>

<!-- End of SECTION / BACKGROUND -->
</td></tr></table>

</body>
</html>
