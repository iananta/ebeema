@component('mail::message')
    Hello {{$premium->planselected ? $premium->planselected->name : 'User'}} jee,

    Your due date is: {{$premium->next_paid_date ?? 'N/A'}} i.e. {{$day ?? 'N/A'}} for policy renewal of NRS. {{$premium->premium_amount ? number_format($premium->premium_amount, 2, '.','') : 'N/A'}} for policy number : {{$premium->planselected ? $premium->planselected->policy_number : 'N/A'}}.

    Thank you for choosing {{ config('app.name') }}
@endcomponent
