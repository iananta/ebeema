@extends('frontend.layouts.app')

@section('css')
<style type="text/css">
    #error404{
    }
    .error-wrapper{
        display: flex;
        flex-flow:row wrap;
        padding-bottom: 80px;
    }
   .error-wrapper .left-404-info {
        width: 50%;
        display: flex;
        justify-content: center;
    }
   .left-404-info .left-message {
        position: relative;
        display: flex;
        justify-content: center;
    }
    .error-info{
        position: absolute;
        top: 145px;
    }
    .left-message .error-text{
        text-align: center;

    }
    .left-message .error-text p {
        width: 65%;
        margin: 0px auto;
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 19px;
        text-align: center;
        margin-top: 20px;

        color: #535353;
    }
    .error-wrapper .right-404-info{
        width: 50%;
        position: relative;
    }
    .error-wrapper .right-404-info img {
        width: 80%;
        position: absolute;
        right: 0;
        border: 0;
        bottom: 95px;
    }
    .error-text h1{
        font-size: 48px;
        line-height: 56px;
        letter-spacing: 1px;
        color: #F03800;
        font-family: Roboto;
        margin: 0px;

    }
    .return-btn {
        margin-top: 120px;
        text-align: center;
    }
    .return-btn a {
        background: #3D538C;
        padding: 10px 25px;
        font-family: Roboto;
        font-style: normal;
        font-weight: 500;
        font-size: 16px;
        line-height: 19px;
        color: #FFFFFF;
        display: inline-block;
    }
    .return-btn a img{
        margin-right: 15px;
        width: 19px;
    }
    @media screen and (max-width : 800px){
       .right-404-info {
            display: none;
        }
        .error-wrapper {

            padding: 0;
            padding-top: 80px;
        }
        .error-wrapper .left-404-info {
            width: 100%
        }
    }
    @media screen and (max-width : 500px){
        .error-text h1 {
            font-size: 36px;
        }
        .left-message .error-text p {
            font-size: 14px;
        }
    }

</style>

@endsection

@section('content')
    <div id="error404" class="en">
        <div class="container">
            <div class="error-wrapper">
                <div class="left-404-info">
                    <div class="left-message">
                       <div class="light-wrapper">
                            <img src="{{asset('frontend/img/lamp.png')}}">
                        </div>
                        <div class="error-info">
                           <div class="error-text">
                                <h1>ERROR / 404</h1>
                                <p>We can’t seem to find the page you’re looking for.</p>
                            </div>
                            <div class="return-btn">
                                <a href="/"><img src="{{asset('frontend/img/error-home.png')}}">Take me to homepage</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-404-info">
                    <img src="{{asset('frontend/img/sleep.png')}}">
                </div>
            </div>
        </div>
    </div>
@stop
