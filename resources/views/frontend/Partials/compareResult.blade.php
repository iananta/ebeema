@php
$amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
$amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
$amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
@endphp

    {{-- /* . features{
        margin-top:20px;
    } */ --}}

<div class="col-sm-9">
     @php
            $sumassured = str_split((string)$selectedSumAssured);
            @endphp
            <div class="compare-header-info">
                <div class="sumassured-items">
                    <div class="sumassured-title">
                        <h3>Sum Assured</h3>
                    </div>
                    <div class="sumassured-number-wrapper">
                         @foreach($sumassured as $number)
                             <div class="number-item">
                                <p>{{(int)$number}}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="term-items">
                    <div class="term-title">
                        <h3>Term</h3>
                    </div>
                     <div class="term-number-wrapper">
                            <div class="number-item">
                                <p>{{$selectedTerm}}</p>
                            </div>
                     </div>
                </div>
            </div>
    <div class="compare-search-sort">
        <p class="left-sort">{{ucfirst($selectedCategory)}} : <a class="" href="javascript:;">{{count($products)}} Plans match your
        search</a></p>

        <div class="navbar-nav right-sort">
            <form class="sortingForm" action="{{ route('compare.result') }}" method="POST">
                @csrf
                <input type="hidden" name="age" value="{{$selectedAge}}">
                <input type="hidden" name="term" value="{{$selectedTerm}}">
                <input type="hidden" name="category" value="{{$selectedCategory}}">
                <input type="hidden" name="features" value="{{json_encode($features)}}">
                <input type="hidden" name="sum_assured" value="{{$selectedSumAssured}}">
                <input type="hidden" name="mop" value="{{$selectedMop}}">
                <input type="hidden" class="sorting-val" name="sort" value="">
                <div class="sort-wrapper">
                    <div class="sort-inital">
                       <div class="sort-box">
                           <p><img class="filter-icon" src="{{asset('frontend/img/fliter.png')}}" alt="filter icon">&nbsp;&nbsp;
                            <span class="sortChange">Sort by Relevance</span>&nbsp;&nbsp;<img class="sort-down-arrow" src="{{asset('frontend/img/down-arrow.svg')}}" alt="arrow image"></p>
                        </div>
                         <div class="mb-filter">
                            <img src="{{asset('frontend/img/mb-filter.png')}}" alt="filter icon">
                         </div>
                         <div class="sort-dropdown-content">
                             <ul>
                                 <li><input class="sort-radio" type="radio" value="premium_high_low" name="sort-radio"><label>Premium: High to Low</label></li>
                                 <li><input class="sort-radio" type="radio" value="premium_low_high" name="sort-radio"><label> Premium: Low to High</label></li>
                                 <li><input class="sort-radio" type="radio" value="maturity_high_to_low" name="sort-radio"><label>Maturity: High to Low</label></li>
                                 <li><input class="sort-radio" type="radio" value="maturity_low_to_high" name="sort-radio"><label>Maturity: Low to High</label></li>
                             </ul>
                         </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="compare-plans">
        <table class="table compare-result-table">
            {{-- <thead class="thead-dark">
               <tr class="heading-dark" >
                   <th scope="col">Insurer & Plan</th>
                   <th scope="col">Premium</th>
                   <th scope="col">Details</th>
                   <th scope="col">Benefits</th>
                   <th scope="col">Action</th>
               </tr>
           </thead> --}}
           <tbody>
         {{--<tr class="spacer">
            <td></td>
        </tr> --}}
        @foreach($products as $product)
        <tr class="content-compare">
            <td class="compare-parts line-rht-cmp" scope="row">
                <p class="cont-plan"> {{$product->company->name}}</p>
                <img src="/images/company/{{$product->company->logo}}" alt="{{$product->company->name}}">
                <p class="prod-name">{{$product->name}}</p>
            </td>
            <td class="prem-box line-rht-cmp">
                    <!-- <p class="sum-title">Sum Assured</p>
                        <p class="sum-title-val">Rs. {{$amountFormatter->format($selectedSumAssured)}}</p> -->
                        <span class="gap-0"></span>
                        <p class="sum-title">Premium Amount</p>
                        <p class="prem-val val-amount" id="pamount">
                            Rs.
                            @if($selectedMop == 'yearly')
                            {{ $amountFormatter->format($product->premiumAmountWithBenefit) }}
                            @elseif($selectedMop == 'half_yearly')
                            {{ $amountFormatter->format($product->premiumAmountWithBenefit/2) }}
                            @elseif($selectedMop == 'quarterly')
                            {{ $amountFormatter->format($product->premiumAmountWithBenefit/4) }}
                            @elseif($selectedMop == 'monthly')
                            {{ $amountFormatter->format($product->premiumAmountWithBenefit/12) }}
                            @endif
                        </p>
                        <span class="gap-0"></span>
                        <p class="details-title2">Age</p>
                        <p class="details-val2 val-amount">
                            {{$product->currentAge}} Y
                        </p>
                         <p class="details-title2 mb-block">Term</p>
                        <p class="details-val2 val-amount mb-block">
                            {{$product->currentTerm}} Y
                        </p>
                        <p class="details-title2 mb-block">Premium Paying Term</p>
                        <p class="details-val2 val-amount mb-block">
                            {{$product->payingTerm}} Y
                        </p>
                        <p class="payment-schedule-wrapper">
                            <a
                            href="#"
                            class="payment-schedule oveflowHidden"
                            data-toggle="modal"
                            data-target="#paybackSchedules{{$product->id}}{{$loop->iteration}}"
                            >
                            Payment Schedule
                            <i
                            class="fa fa-info-circle"
                            data-toggle="tooltip"
                            data-placement="top"
                            title="Payment Schedule"
                            ></i>
                        </a>
                        <p>

                        </td>

                        <td class="details-box line-rht-cmp">
                       <!--  <p class="details-title">Total Bonus</p>
                        <p class="details-val2">Rs. {{$amountFormatter->format($product->bonus ?? '0')}}</p> -->
                        <span class="gap-0"></span>
                        <p class="details-title2">Estimated Maturity Value</p>
                        <p class="details-val2" id="return"><input type="hidden" value=" {{$product->return}}"
                         name="bonus">
                         Rs. {{ $amountFormatter->format($product->totalPremiumAmount) }}
                     </p>
                     <!-- <span class="gap-0"></span>
                     <p class="details-title2">Net Gain</p>
                     <p class="details-val2">
                        Rs. {{$amountFormatter->format($product->netGain) }}</p> -->
                       

                    </td>
                            <td class="mb-benefits">
                               <div class="benefits-button">
                                <p ><span>Benefits</span> <i class="fa fa-chevron-down" ></i></p>
                            </div>
                        </td>
                        <td class="benefit-box line-rht-cmp">
                            <ul class="term-details mb-none-block ">
                                <li><strong>Term :</strong>&nbsp;<span>{{$product->currentTerm}} Y</span></li>
                                <li><strong>Pay Term :</strong>&nbsp;<span>{{$product->payingTerm}} Y</span></li>
                            </ul>
                            <ul class="benefit-lists features">
                                @foreach($product->availableFeatures as $available)
                                <li class="text-primary text-capitalize">
                                    <p class="result-feature">
                                        <span>{{ $available['name'] }}</span>

                                        @if($product->features->contains('code', $available->code))
                                        <i class="fa fa-check-square-o to-right" aria-hidden="true"></i>
                                        @else
                                        <i class="fa fa-times to-right cross-fa" aria-hidden="true"></i>
                                        @endif
                                    </p>
                                </li>

                                @endforeach
                            </ul>
                        </td>

                        <td>
                            <div class="select-plan-box">
                                <a data-id="{{$product->id}}" href="#" class="view-plan">View Plan</a>
                                <form action="{{route('front.confirm')}}" method="POST" enctype="multipart/form">
                                    @csrf
                                    <input type="hidden" name="age" value="{{$product->currentAge}}">
                                    @if($isChildPlan)
                                    <input type="hidden" name="child_age" value="{{$selectedChildAge}}">
                                    <input type="hidden" name="proposer_age" value="{{$selectedProposersAge}}">
                                    @endif
                                    <input type="hidden" name="birth_date" value="{{$birth_date ?? null}}">
                                    <input type="hidden" name="birth_month" value="{{$birth_month ?? null}}">
                                    <input type="hidden" name="birth_year" value="{{$birth_year ?? null}}">

                                    @if($isCouplePlan)
                                    <input type="hidden" name="wife_age" value="{{$selectedWifeAge}}">
                                    <input type="hidden" name="husband_age" value="{{$selectedHusbandAge}}">
                                    @endif
                                    @php
                                    if($selectedMop == 'yearly'){
                                        $premiumAmt = $product->premiumAmountWithBenefit;
                                    }
                                    elseif($selectedMop == 'half_yearly'){
                                        $premiumAmt = $product->premiumAmountWithBenefit/2;
                                    }
                                    elseif($selectedMop == 'quarterly'){
                                        $premiumAmt = $product->premiumAmountWithBenefit/4;
                                    }
                                    elseif($selectedMop == 'monthly'){
                                        $premiumAmt = $product->premiumAmountWithBenefit/12;
                                    }
                                    else{
                                        $premiumAmt = 0;
                                    }
                                    @endphp
                                    <input type="hidden" name="bonus" value="{{$product->bonus ?? null}}">
                                    <input type="hidden" name="net_gain" value="{{$product->netGain ?? null}}">
                                    <input type="hidden" name="return" value="{{$product->totalPremiumAmount ?? null}}">
                                    <input type="hidden" name="company_id" value="{{$product->company->id ?? null}}">
                                    <input type="hidden" name="invest" value="{{$invest ?? null}}">
                                    <input type="hidden" name="premium" value="{{$premiumAmt ?? null}}">
                                    <input type="hidden" name="term" value="{{$product->currentTerm}}">
                                    <input type="hidden" name="category" value="{{$selectedCategory}}">
                                    <input type="hidden" name="features" value="{{json_encode($features)}}">
                                    <input type="hidden" name="sum_assured" value="{{$selectedSumAssured}}">
                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                    <input type="hidden" name="loading_charge" value="{{$selectedMop}}">
                                    <button type="submit" class="btn btn-primary select-plan">Select Plan</button>
                                </form>
                            </div>
                        </td>
                    </tr>

                    <tr class="spacer">
                        <td></td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
            <div class="dis-clamer">
                <div class="left-disclamer">
                    <p>*Maturity Value is subject to change as per each year's bonus rate published by Beema Samiti. <span>
                    </span> </p>
                </div>
            </div>
        </div>
    </div>

    @include('Backend.Compare.includes.payback')
<div id="view-plan" class="view-plan-block">
            <div class="view-plan-wrapper">
                <div class="view-plan-heading">
                    <h1>View Plan</h1>
                    <div class="close-btn">&times;</div>
                </div>
                <div class="view-plan-content">
                   <div class="view-plan-content-wrapper"></div>
                </div>
            </div>
        </div>
