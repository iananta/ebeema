<form
    id="frontLifeForm"
    action="{{ route('compare.result') }}"
    class="form-inline"
    enctype="multipart/form-data"
    method="POST">
    @csrf
    <div class="form-group life-category">
        <label for="cat">Category&nbsp;&nbsp;&nbsp;<i
                class="fa fa-info-circle"
                data-toggle="tooltip"
                data-placement="top"
                title="Select your category."
            ></i></label>
        <select
            class="form-control form-control-wide ftCalculate"
            id="cat"
            name="category"
            onchange="onCategorySelection()"
            required
        >
            <option value="" selected='selected'>Select Category</option>
            @if(isset($life_categories))
                @foreach($life_categories as $category)

                    <option
                        value="{{ $category }}" {{ Request::get('category') == $category ? 'selected' : '' }}>        @if($category=='retirement-pension')
                            {{ ucfirst(str_replace('-','/',$category)) }}
                        @else
                            {{ ucfirst(str_replace('-',' ',$category)) }}
                        @endif
                    </option>
                @endforeach
            @endif
        </select>
    </div>
    <div id="age_container" class="form-group">
      <h1>  <label>Date of Birth&nbsp;&nbsp;&nbsp;<i
                class="fa fa-info-circle"
                data-toggle="tooltip"
                data-placement="top"
                title="Put your Date of Birth"
            ></i></label> </h1>

        <div class="date-type-wrapper">
            <div class="date-type-selection">
                <select name="date-type" class="form-control form-control-wide date-type">
                    <option value="">Select One</option>
                    <option value="english">English Date</option>
                    <option value="nepali">Nepali Date</option>
                </select>
            </div>
            <div class="date-form-wrapper date-format">
                <div class='date-form-item'>
                    <select class="form-control age-calc" name="birth_date" id="birthDay">
                        <option value="">DD</option>
                    </select>
                </div>
                <div class='date-form-item'>
                    <select class="form-control age-calc" name="birth_month" id="birthMonth">
                        <option value="">MM</option>
                    </select>
                </div>
                <div class='date-form-item'>
                    <select class="form-control age-calc" name="birth_year" id="birthYear">
                        <option value="">YYYY</option>
                    </select>
                </div>
                <div class='date-form-item'>
                    <input class="form-control" id="total-age" type="number" name="age" readonly placeholder="Age">
                </div>
            </div>
        </div>
    </div>

    <!-- Child Age -->
    <div id="child_container" class="col-md-12 calculate-input d-none">
        <!-- Child Age -->
        <div class="form-group">
         <h1>   <label>Child Age&nbsp;&nbsp;&nbsp;
                <i
                    class="fa fa-info-circle"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Select your child age"
                ></i>  </h1>
            </label>
            <div class="checkbox-wrapper" id="child_age">
                @for($i = 0; $i <= 18; $i++)
                    <div class='check-item'>
                        <label>
                            <input type='radio' class='sum-cal' name='child_age' autocomplete='off'
                                   value='{{ $i }}'>{{ $i }}
                        </label>
                    </div>
                @endfor
            </div>
            {{--            <label for="child_age">--}}
            {{--                Child Age in Years--}}
            {{--                <i--}}
            {{--                    class="fa fa-info-circle"--}}
            {{--                    data-toggle="tooltip"--}}
            {{--                    data-placement="top"--}}
            {{--                    title="Put child age in years."--}}
            {{--                ></i>--}}
            {{--            </label>--}}
            {{--            <input--}}
            {{--                class="form-control radio-select-box sidecalc-btn"--}}
            {{--                type="number"--}}
            {{--                name="child_age"--}}
            {{--                id="child_age"--}}
            {{--                min="0"--}}
            {{--                max="100"--}}
            {{--                placeholder="Age"--}}
            {{--            />--}}
        </div>

        <!-- Proposer's Age -->
        <div class="form-group">
            <label for="proposer_age">
                Proposer's Age in Years
                <i
                    class="fa fa-info-circle"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Put proposer age in years."
                ></i>
            </label>
            <div class="checkbox-wrapper" id="proposer_age">
                <p>Please Select a Category</p>
            </div>
            {{--            <input--}}
            {{--                class="form-control radio-select-box sidecalc-btn"--}}
            {{--                type="number"--}}
            {{--                name="proposer_age"--}}
            {{--                id="proposer_age"--}}
            {{--                min="0"--}}
            {{--                max="100"--}}
            {{--                placeholder="Age"--}}
            {{--            />--}}
        </div>
    </div>

    <!-- Couple Age -->
    <div id="couple_container" class="col-md-12 calculate-input d-none">
        <!-- Husband Age -->
        <div class="form-group">
            <label for="husband_age">
                Husband Age in Years
                <i
                    class="fa fa-info-circle"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Put husband age in years."
                ></i>
            </label>
            <div class="checkbox-wrapper" id="husband_age">
                <p>Please Select a Category</p>
            </div>
            {{--            <input--}}
            {{--                class="form-control radio-select-box sidecalc-btn"--}}
            {{--                type="number"--}}
            {{--                name="husband_age"--}}
            {{--                id="husband_age"--}}
            {{--                min="0"--}}
            {{--                max="100"--}}
            {{--                placeholder="Age"--}}
            {{--            />--}}
        </div>

        <!-- Wife Age -->
        <div class="form-group">
            <label for="wife_age">
                Wife Age in Years
                <i
                    class="fa fa-info-circle"
                    data-toggle="tooltip"
                    data-placement="top"
                    title="Put wife age in years."
                ></i>
            </label>
            <div class="checkbox-wrapper" id="wife_age">
                <p>Please Select a Category</p>
            </div>
            {{--            <input--}}
            {{--                class="form-control radio-select-box sidecalc-btn"--}}
            {{--                type="number"--}}
            {{--                name="wife_age"--}}
            {{--                id="wife_age"--}}
            {{--                min="0"--}}
            {{--                max="100"--}}
            {{--                placeholder="Age"--}}
            {{--            />--}}
        </div>
    </div>

    <div class="form-group">
        <label>Term&nbsp;&nbsp;&nbsp;
            <i
                class="fa fa-info-circle"
                data-toggle="tooltip"
                data-placement="top"
                title="Select your Term in years"
            ></i>
        </label>
        <div class="checkbox-wrapper" id="term">
            <p>Please Select a Category</p>
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="5"> 5--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="10"> 10--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="15"> 15--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="20"> 20--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="25"> 25--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="30"> 30--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <div class="check-item">--}}
            {{--                <label>--}}
            {{--                    <input type="radio" class="sum-cal" name="term1" autocomplete="off" value="35"> 35--}}
            {{--                </label>--}}
            {{--            </div>--}}
            {{--            <b>OR</b>--}}
            {{--            <div class="custom-input">--}}
            {{--                <label>--}}
            {{--                    <input type="number" class="sum-cal customize-input" name="term" autocomplete="off" value=""--}}
            {{--                           placeholder="Enter Your Term" required>--}}
            {{--                </label>--}}
            {{--            </div>--}}
        </div>
    </div>
    <div class="form-group">
        <label>Sum Assured&nbsp;&nbsp;&nbsp; <i
                class="fa fa-info-circle"
                data-toggle="tooltip"
                data-placement="top"
                title="Select your invest."
            ></i></label>
        <div class="checkbox-wrapper" id="invest">
            <div class="check-item">
                <label>
                    <input type="radio" name="sum_assured1" class="sum-cal" autocomplete="off" value="500000"> 5 Lakhs
                </label>
            </div>
            <div class="check-item">
                <label>
                    <input type="radio" name="sum_assured1" class="sum-cal" autocomplete="off" value="1000000"> 10 Lakhs
                </label>
            </div>
            <div class="check-item">
                <label>
                    <input type="radio" name="sum_assured1" class="sum-cal" autocomplete="off" value="1500000"> 15 Lakhs
                </label>
            </div>
            <div class="check-item">
                <label>
                    <input type="radio" name="sum_assured1" class="sum-cal" autocomplete="off" value="2000000"> 20 Lakhs
                </label>
            </div>
            <div class="check-item">
                <label>
                    <input type="radio" name="sum_assured1" class="sum-cal" autocomplete="off" value="2500000"> 25 Lakhs
                </label>
            </div>
            <div class="check-item">
                <label>
                    <input type="radio" name="sum_assured1" class="sum-cal" autocomplete="off" value="5000000"> 50 Lakhs
                </label>
            </div>

            <b>OR</b>
            <div class="custom-input">
                <label>
                    <input type="number" name="sum_assured" step="any" class="sum-cal customize-input"
                           autocomplete="off"
                           value="" placeholder="Enter your Sum Assured" required>
                </label>
            </div>


        </div>
    </div>

    <input type="hidden" name="mop" value="yearly">
    {{--    <div class="form-group">--}}
    {{--        <label>Mode of Payment&nbsp;&nbsp;&nbsp; <i--}}
    {{--                    class="fa fa-info-circle"--}}
    {{--                    data-toggle="tooltip"--}}
    {{--                    data-placement="top"--}}
    {{--                    title="Select mop."--}}
    {{--                ></i></label>--}}
    {{--        <select class="form-control form-control-wide" id="mode-of-payment" name="mop">--}}
    {{--            <option value="">Select Mode of Payment</option>--}}
    {{--                @if(isset($mops))--}}
    {{--                    @foreach($mops as $mop)--}}
    {{--                        <option value="{{ $mop }}">{{ str_replace('_', ' ',ucfirst($mop)) }}</option>--}}
    {{--                    @endforeach--}}
    {{--                @endif--}}
    {{--        </select>--}}
    {{--    </div>--}}
    <div class="form-group">
        <label>Invest&nbsp;&nbsp;&nbsp;<i
                class="fa fa-info-circle"
                data-toggle="tooltip"
                data-placement="top"
                title="Select your investment."
            ></i></label>
        <input type=number name="invest" id="invest" step="any" class="form-control form-control-wide"
               placeholder="Enter Your Investment">
    </div>
    <!-- Features -->
    <div class="d-inline form-group col-12 d-none calculate-input" id="featureRow">
      <h2>  <label>Features</label> </h2>
        <div id="featuresTab">
        </div>
    </div>


    <div class="form-group">
        <button class="btn-submit" @if(!Session::get('customerLeadId')) id="data-calc" @endif type="submit">Continue
        </button>
    </div>
</form>
