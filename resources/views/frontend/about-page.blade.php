@extends('frontend.layouts.app')

@section('seo_contents')
    <title>{{$seoData->where('page_name','about-page')->first()->meta_title ?? ''}}</title>
    <meta name="title" content="{{$seoData->where('page_name','about-page')->first()->meta_title ?? ''}}">
    <meta name="description" content="{{$seoData->where('page_name','about-page')->first()->meta_description ?? ''}}">
@endsection

@section('content')

<!-- /About Us section-->

<section class="about-us">
    <div class="container aboutUs">
        <div class="row aboutUs--hero">
            @if($aboutUsContent)
            <div class="col-sm-6 aboutUs--herotitle pr-2">
                <h1 class="about-filt">{{$aboutUsContent->about_us_title}}</h1>
                @if(strlen($aboutUsContent->about_us_content) > 406)
                {!! text_limit($aboutUsContent->about_us_content, 406) !!}
                  <div class="moretext">
                    {!! substr($aboutUsContent->about_us_content,407,strlen($aboutUsContent->about_us_content)) !!}
                 </div>
                 <a class="moreless-button" href="#">Read more</a>
                @else
                    {!! $aboutUsContent->about_us_content !!}
                @endif
            </div>
            <div class="col-sm-6 aboutUs--hero__img justify-content-center">
                <img class="float-right-img" src="{{asset('frontend/img/team.jpg')}}" alt="team image">
            </div>
            @endif
        </div>
    </div>

    <!-- Features of Ebeema with three different features -->
    <div class="container aboutUs--feature">
        <div class="row">
            @if($aboutUs)
            @foreach($aboutUs as $key => $why)
            <div class="card col-sm-4">
                <div class="row  feature--cards">
                    <div class="col-sm-2">
                        <img src="{{$why->image}}" alt="{{$why->title}}" alt="title image">
                    </div>

                    <div class="col-sm-10 feature--cards__text">
                        {{$why->title}}
                        <p>{{$why->description}}</p>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    <!-- Features of Ebeema ends -->
    <div class="container core--value--wrapper">
        <div class="row">
            <img src="{{ asset('backend/img/welcome-bg.png') }}" alt="dot image">
            <div class="col-sm-5 core--wrapper">
                <h3>Mission & Values</h3>
                <p>Mission: is to make insurance simple, seamless, and accessible to everyone and as a result help in developing the country’s insurance and financial sector.
                <br><br>
                Vision: Contribute to nationwide financial security and improved insurance services
                </p>
                <a href="#"><img src="{{ asset('frontend/img/about/CTA-arrow.png') }}" alt="mission arrow"></a>
            </div>

            <div class="col-sm-7 values--wrapper wrap">
                <div class="row">
                    <div class="col values-wrapper__content">
                        <p>01</p>
                        <div class="core--values">
                            <h6>Customer First</h6>
                            <p>Ebeema is a proud company with 100% satisfaction to the customer as of date. We also are leading in after service regarding insurance.</p>
                        </div>
                    </div>
                    <div class="col values-wrapper__content">
                        <p>02</p>
                        <div class="core--values">
                            <h6>Impartial</h6>
                            <p>Customers can choose from any plan they see fit. We do not favor any company but we favor the client.</p>
                        </div>
                    </div>
                    <div class="col values-wrapper__content">
                        <p>03</p>
                        <div class="core--values">
                            <h6>Professional</h6>
                            <p>Ebeema is a pioneer in the digital insurance industry and have amazing state of the art digital platform. We are professional about the way we deal with our clients.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid cta-wrapper aboutUs--cta ">
        <div class="container">
            <div class="row aboutUs--row">
                <div class="col-sm-12 expert">
                    <h2>Talk to an expert</h2>
                    <p>Our expert will call you back <span class="rigthway" ><strong><em>Right
                                    Away</em></strong></span></p>
                    <div class="cta--btn">
                        <a href="{{url('contact')}}" class="button-icon">
                            <span class="button-icon-text">Talk Now
                                <img src="{{ asset('frontend/img/about/cta-arrow.png') }}" alt="talk to an expert">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid aboutUs--team--wriapper">
        <div class="container aboutUs--team--container py-5">
            <h3 class="text-center">Our Expert Team</h3>
            <div class="row">
                @foreach($teams as $team)
                <div class="col-sm-4 ">
                    <div class="row team--member--detail">
                        <img src="{{$team->image}}" alt="ebeema team">
                        <div class="col-sm text-center team--connect">
                                {{$team->name}}
                            <p>{{ $team->designation }}</p>
                            <p>{{ $team->phone }} </p>
                            <p>{{ $team->email }}</p>
                            <ul class="list-group">
                                <li>
                                    <a target="_blank" href="{{ $team->linkedin ?? '#' }}"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a target="_blank" href="{{ $team->fb ?? '#' }}"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a target="_blank" href="{{ $team->twitter ?? '#' }}"><i class="fa fa-twitter"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection
