@extends('frontend.layouts.app')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/nonlife.css') }}">
@endsection

@section('seo_contents')
    <title>{{$blog->meta_title ?? ''}}</title>
    <meta name="title" content="{{$blog->meta_title ?? ''}}">
    <meta name="description" content="{{$blog->meta_description ?? ''}}">
@endsection
@section('content')
<div class="container">
        <div class="single-blog-wapper">
            <div class="single-blog">
                <div class="single-image">
                    <img src="/images/blogCategory/{{ $blog->image }}" alt="blog">
                </div>
                <div class="single-blog-content">
                    <div class="single-blog-heading">
                        <h1>{{ $blog->title }}</h1>
                    </div>
                    <div class="single-blog-info">
                        <div class="single-blog-info-detail">
                            <div class="single-blog-category">
                                {{ $blog->category->name }}
                            </div>
                            <div class="single-blog-date">
                                {{ $blog->created_at }}
                            </div>
                        </div>
                        <div class="single-blog-description">
                           <p> {!! $blog->description !!} </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blog-side-bar">
                <div class="related-fields">
                    <div class="related-field-title">
                        <h3>Related Blogs</h3>
                    </div>
                    <div class="related-blog-links">
                        <ul>
                                @foreach($relatedBlog as $rblog)
                                <a href="{{ route('frontend.blog.view', $blog->slug ?? '') }}" title="View Blog">
                                        <div class="latest-img">
                                            <img src="/images/blogCategory/{{ $rblog->image }}" alt="blog logo">
                                        </div>
                                        <div class="latest-shortdesc">
                                            <div class="latest-time-cat">
                                              <p class="latest-cat"><strong>{{$rblog->title ?? 'N/A'}}</strong><span>{{$rblog->created_at ? $rblog->created_at->format('D/M/Y') : Carbon\Carbon::parse($rblog->created_at)->diffForHumans()}}</span></p>
                                                <p class='desc'>{{$rblog->title}}</p>
                                              <div class="readmore-btn">
                                   <button type="submit" class="readmore-button">Read more</button>
                                    <div class="readmore-arrow">
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </div>
                                    </div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection

