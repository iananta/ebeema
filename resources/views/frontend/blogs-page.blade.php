@extends('frontend.layouts.app')
@section('seo_contents')
     <title>{{$seoData->where('page_name','blog-page')->first()->meta_title ?? ''}}</title> 
    <meta name="title" content="{{$seoData->where('page_name','blog-page')->first()->meta_title ?? ''}}">
    <meta name="description" content="{{$seoData->where('page_name','blog-page')->first()->meta_description ?? ''}}">
@endsection
@section('content')
    <section class="inner-page-blog">
		<div class="container">
			<div class="blog-search">
				<form action="" class="blog-search-form" method="get">
                    <div class="container">
                        <div class="row height d-flex justify-content-center align-items-center">
                            <div class="col-md-12">
                                <div class="search"> <i class="fa fa-search"></i> <input type="text" name="search" value="{{Request::get('search')}}" class="form-control" placeholder="want to search? Type here"> <button class="btn btn-primary">Search</button> </div>
                            </div>
                        </div>
                    </div>
                </form>
			</div>
			<div class="blogs-wrapper text-center">
                @if(count($blogs))
				 @foreach($blogs as $blog)
                    <div class="latest-item">
                        <div class="latest-content">
                            <a href="{{ route('frontend.blog.view', $blog->slug ?? '') }}" title="View Blog">
                            <div class="latest-img">
                                <img src="/images/blogCategory/{{ $blog->image != null ? $blog->image : 'default.png'}}" alt="{{$blog->title}}">
                            </div>
                            <div class="latest-shortdesc">
                                <div class="latest-time-cat">
                                    <p class="latest-cat"><strong>{{$blog->title ?? 'N/A'}}</strong><span>{{$blog->created_at ? $blog->created_at->format('D/M/Y') : Carbon\Carbon::parse($blog->created_at)->diffForHumans()}}</span></p>
                                    <p class='desc'>{{$blog->title}}</p>
                                    <div class="readmore-btn">
                                   <button type="submit" class="readmore-button">Read more</button>
                                    <div class="readmore-arrow">
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                @else
                    <div class="latest-item text-center">
                        <div class="latest-content">
                            <div class="latest-shortdesc">
                            <h1>No Any Blogs Found</h1>
                            </div>
                        </div>
                    </div>
                @endif
			</div>
		</div>
	</section>
@endsection
