@extends('frontend.layouts.app')

@section('css')
    <!-- <style>
        .form-check {
            display: flex;
        }

        .form-check-label {
            padding: 0 10px !important;
        }

        .d-flex {
            display: flex;
        }

        .d-none {
            display: none !important;
        }

        .ml-3 {
            margin-left: 4rem;
        }
    </style> -->
@endsection

@section('script')
    <script>

        $(document).ready(function () {
            $('.search-select').select2();
            categoryCalc();

            $('#ageForm').on('submit', function (e) {
                e.preventDefault();

                let day = $('#day').val();
                let month = $('#month').val();
                let year = $('#year').val();
                $.ajax({
                    url: "/age",
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        day: day,
                        month: month,
                        year: year,

                    },
                    success: function (response) {

                    },
                });
            });
        });
        $('input[name = term1]').on('change', function () {
            $('input[name = term]').val($('input[name = term1]:checked').val());
            claculateSum();
        });
        $('input[name = sum_assured1]').on('change', function () {
            $('input[name = sum_assured]').val($('input[name = sum_assured1]:checked').val());
            claculateSum();
        });

        $('.sum-cal').on('input', function () {
            claculateSum();
        });

        function claculateSum() {
            var term = $('input[name = term]').val();
            var sum = $('input[name = sum_assured]').val();
            if (term && invest) {
                $('input[name = invest]').val((sum / term).toFixed(2));
            }
        }

        $('input[name = invest]').on('input', function () {
            var term = $('input[name = term]').val();
            var invest = $('input[name = invest]').val();
            if (term && invest) {
                $('input[name = sum_assured1]:checked').prop("checked", false);
                $("#invest > .check-item").removeClass('active');
                $('input[name = sum_assured]').val((invest * term).toFixed(2));
            }
        });

        $('.age-calc').on('input', function () {
            var day = $('#birthDay').val();
            var month = $('#birthMonth').val();
            var year = $('#birthYear').val();
            var age = 0;
            if (day && month && year) {
                age = calculate_age(new Date(year, month, day));
            }
            $('#total-age').val(age);
        });

        $('.ftCalculate').on('change', function () {
            categoryCalc();
        });

        function categoryCalc() {
            let plan = $("#cat").val();
            let ageDiv = $('#age_container');
            let coupleDiv = $('#couple_container');
            let childDiv = $('#child_container');
            let parent = $('.life-category')
            const style = 'd-none'
            let message;
            if (plan) {
                $('.short-note').remove()
                switch (plan) {
                    case 'endowment':
                        message = '<p class="short-note">"An investment plan with comparatively lesser premium"</p>';
                        parent.append(message)
                        break;
                    case 'money-back':
                        message = '<p class="short-note">"The insurance plan that payes you back timely after certain year"</p>';
                        parent.append(message)
                        break;
                    case 'whole-life':
                        message = '<p class="short-note">"The insurance that gives coverage till you are 99."</p>'
                        parent.append(message)
                        break;
                    case 'term':
                        message = '<p class="short-note">"The traditional insurance plan that basically provides support to your family after you"</p>'
                        parent.append(message)
                        break;
                    case 'pension':
                        message = '<p class="short-note">"An insurance plan that gives you security at your old age and provide money in the form of salary"</p>'
                        parent.append(message)
                        break;
                    case 'retirement-pension':
                        message = '<p class="short-note">"An insurance plan that gives you security at your old age and provide money in the form of salary"</p>'
                        parent.append(message)
                        break;
                    default:
                        message = '<p class="short-note">"The insurance plan for you and your love one together with dual benefits"</p>'
                        parent.append(message)
                        break;
                }
                switch (plan) {
                    case 'couple':
                        ageDiv.addClass(style)
                        coupleDiv.removeClass(style)
                        childDiv.addClass(style)
                        break;
                    case 'children':
                        ageDiv.addClass(style)
                        coupleDiv.addClass(style)
                        childDiv.removeClass(style)
                        break;
                    case 'education':
                        ageDiv.addClass(style)
                        coupleDiv.addClass(style)
                        childDiv.removeClass(style)
                        break;
                    default:
                        ageDiv.removeClass(style)
                        coupleDiv.addClass(style)
                        childDiv.addClass(style)
                        break;
                }

                $.ajax({
                    type: "GET",
                    url: "{{ route('admin.get.category.feature') }}",
                    dataType: 'json',
                    data: {
                        'id': plan,
                    },
                    success: function (response) {

                        if (response.data.length > 0) {
                            let html = '';
                            for (let j = 0; j < response.data.length; j++) {

                                html += ' <div class="form-check">' +
                                    '<input type="checkbox" name="features[]"  class="form-check-input sum-cal" value="' + response.data[j].code + '">' +
                                    '<label class="form-check-label text-capitalize">' + response.data[j].name + '</label>' +
                                    '</div>'
                            }
                            $('#featuresTab').html(html);
                        } else {
                        }
                    }
                });
            }
        }

        function calculate_age(dob) {
            var diff_ms = Date.now() - dob.getTime();
            var age_dt = new Date(diff_ms);
            return Math.abs(age_dt.getUTCFullYear() - 1970);
        }

        $('#Custlogin').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('frontend.add.lead') }}",
                dataType: 'json',
                data: $('#userInfos').serialize(),
                beforeSend: function () {
                    $('#preloader').show(500);
                },
                success: function (response) {
                    // console.log(response);
                    if (response.data == 1) {
                        // $('#preloader').hide(500);
                        $("#frontLifeForm").submit();
                    } else {
                        $('#preloader').hide(500);
                        swal({
                            icon: "success",
                            text: "something went wrong",
                        });
                    }
                },
                error: function (response) {
                    let parent = $('#verifyCustomer')
                    let formgroup = parent.find('.modal-content form .form-group')
                    let validationTag = $('.custom-validation-message-error')
                    if (validationTag) {
                        validationTag.remove()
                    }
                    $('#preloader').hide();
                    $("#AddKycCustomer").prop('disabled', false);
                    if (response.responseJSON && response.responseJSON.errors) {
                        let res = response.responseJSON
                        let obj = res.errors
                        for (const [key, value] of Object.entries(obj)) {
                            formgroup.find('input[name="' + key + '"]').parent().append('<p class="custom-validation-message-error">' + value + '</p>')
                        }
                    } else {
                        let res = response.responseJSON
                        for (let value of Object.values(res.message)) {
                            parent.find('form').append('<p class="custom-validation-message-error">' + value + '</p>')
                        }
                    }

                }
            });
        });
        $('#data-calc').on('click', function (e) {
            e.preventDefault();
            $('#verifyCustomer').modal('show');
        });
        let fiscal = "1st Shrawan " + nepalidate.year
        document.cookie = "fiscal=" + fiscal;
        $('.date-type').on('change', function () {
            let val = $(this).val()
            $('#birthMonth').html('')
            $('#birthYear').html('')
            $('#birthMonth').append('<option value=" ">MM</option>')
            $('#birthYear').append('<option value=" ">YYYY</option>')
            for (let i = 1; i <= 32; i++) {
                $('#birthDay').append('<option value="' + i + '">' + i + '</option>')
            }
            $('.date-format').css('display', 'flex')
            if (val == 'english') {
                <?php foreach(months() as $key=>$month){ ?>
                $('#birthMonth').append('<option value="<?php echo $key; ?>"><?php echo $month; ?></option>')
                <?php } ?>
                <?php
                $currentYear = date("Y");
                for($i = 0;$i <= 100;$i++) {
                ?>
                $('#birthYear').append('<option value="<?php echo $currentYear - $i ?>"><?php echo $currentYear - $i ?></option>')
                <?php } ?>
            } else if (val == 'nepali') {
                <?php foreach(nepali_months() as $key=>$month){ ?>
                $('#birthMonth').append('<option value="<?php echo $key; ?>"><?php echo $month; ?></option>')
                <?php } ?>
                <?php
                $currentYear = date("Y") + 57;
                for($i = 0;$i <= 100;$i++) {
                ?>
                $('#birthYear').append('<option value="<?php echo $currentYear - $i ?>"><?php echo $currentYear - $i ?></option>')
                <?php } ?>
            } else {
                $('.date-format').hide()
            }
        })

        function onCategorySelection() {
            const category = $("#cat").val();
            $.ajax({
                type: "GET",
                url: "{{ route('admin.policy.category') }}",
                dataType: 'json',
                data: {
                    'category': category,
                },
                success: function (response) {
                    if (response.id) {

                        let minAge = response.min_age ? response.min_age : 1;
                        let maxAge = response.max_age ? response.max_age : 70;

                        let minTerm = response.min_term ? response.min_term : 1;
                        let maxTerm = response.max_term ? response.max_term : 70;

                        let ages = "";
                        let i = minAge

                        if (category === 'children' || category === 'education') {
                            for (i; i <= maxAge; i++) {
                                ages += "<div class='check-item'>"
                                    + "<label>"
                                    + "<input type='radio' class='sum-cal' name='proposer_age' autocomplete='off' value='" + i + "'>" + i
                                    + "</label>"
                                    + "</div>";
                            }
                            $('#proposer_age').html(ages);
                        } else if (category === 'couple') {
                            for (i; i <= maxAge; i++) {
                                ages += "<div class='check-item'>"
                                    + "<label>"
                                    + "<input type='radio' class='sum-cal' name='wife_age' autocomplete='off' value='" + i + "'>" + i
                                    + "</label>"
                                    + "</div>";
                            }
                            $('#wife_age').html(ages);

                            ages = "";
                            i = minAge;
                            for (i; i <= maxAge; i++) {
                                ages += "<div class='check-item'>"
                                    + "<label>"
                                    + "<input type='radio' class='sum-cal' name='husband_age' autocomplete='off' value='" + i + "'>" + i
                                    + "</label>"
                                    + "</div>";
                            }
                            $('#husband_age').html(ages);
                        } else {
                            $('#age').html(ages);
                        }

                        let terms = "";
                        for (let j = minTerm; j <= maxTerm; j++) {
                            terms += "<div class='check-item'>"
                                + "<label>"
                                + "<input type='radio' class='sum-cal' name='term1' autocomplete='off' value='" + j + "'>" + j
                                + "</label>"
                                + "</div>";
                        }
                        $('#term').html(terms);
                    }
                }
            });
        }

    </script>
@endsection
@section('content')
    <section class="calculator-section">
        <div class="container">
            <div class="calculation-form">
                @include('frontend.Partials.premiumCalculatorForm')
            </div>

            <div class="modal fade p-3" id="verifyCustomer" tabindex="-1" role="dialog"
                 aria-labelledby="numberVerify">
                <div class="modal-dialog modal-dialog-center" role="document">
                    <div style="padding: 20px" class="modal-content">
                        <h3 class="m-0">Your Information
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="premium information"></span>
                            </button>
                        </h3>
                      <h1>  <span class="mobile-text">Enter the your information to continue.</span> </h1>
                        <form id="userInfos" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter your full name"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="number" class="form-control" name="phone"
                                       placeholder="Enter your phone number" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" name="email" aria-describedby="emailHelp"
                                       placeholder="Enter your email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                                    anyone else.</small>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" id="Custlogin"
                                        class="btn btn-primary c-btn-style c-primary-btn mb-3">Submit & View result
                                </button>
                                <br>
                                <button style="margin-top: 10px" type="button" id="makeModalClose"
                                        class="btn btn-outline-secondary c-btn-style c-ghost-btn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
