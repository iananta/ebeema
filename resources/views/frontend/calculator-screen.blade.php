@extends('frontend.layouts.app')
@section('seo_contents')
   <title>{{$seoData->where('page_name','calculator-page')->first()->meta_title ?? ''}}</title> 
    <meta name="title" content="{{$seoData->where('page_name','calculator-page')->first()->meta_title ?? ''}}">
    <meta name="description" content="{{$seoData->where('page_name','calculator-page')->first()->meta_description ?? ''}}">
@endsection
@section('content')
    <section class="calculator-section">
        <div class="container">
            <div class="calculation-form">
                @include('frontend.Partials.premiumCalculatorForm')
            </div>
        </div>
    </section>
@endsection
@section('script')
@include('frontend.components.calculator_script')
@endsection
