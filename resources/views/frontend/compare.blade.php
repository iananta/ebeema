@extends('frontend.layouts.app')


@php
$amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
$amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
$amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
@endphp
@section('content')
<section class="compare-screen">
    <div class="top-navigation">
        <a href="/">Home</a> / <a href="{{ route('compare.form') }}">Insurance Calculate</a> / <a class="active"
        href="javascript:;">{{str_replace('_',' ',ucfirst($selectedCategory))}}</a>
    </div>
</section>
<section class="compare-calc-lists">
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-3">
                <form id="policyfilter" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="is_ajax" value="1">
                    <div class="left-filter-cmp">
                        <div class="compare-list-cat">
                         <h1 class="policy-filt">Policy filter</h1>
                            <div class="mb-filter-close"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="filter policy"></div>
                        </div>

                        @if($selectedCategory != 'couple' && $selectedCategory != 'children')
                        <div class="comare-lst">
                          <h3 class="birth-date">Date of birth</h3>
                            <div class="date_selection">
                                <input type="hidden" class="date-type" name="date_type" value="{{$date_type}}">
                                <select class=" age-calc calc-chg" name="birth_date" id="birthDay">
                                    <option value="" disabled>DD</option>
                                    @for($i = 1; $i <= 32; $i++)
                                    <option
                                    value={{ $i }} {{$birth_date && $birth_date == $i ? "selected" : ''}}>{{ $i }}</option>
                                    @endfor
                                </select>
                                <select  class="form-control age-calc calc-chg" name="birth_month" id="birthMonth">
                                    <option value="" disabled>MM</option>
                                    @foreach(($date_type == 'nepali') ? nepali_months() :months() as $key=>$month)
                                    <option
                                    value="{{$key}}" {{$birth_month && $birth_month == $key ? "selected" : ''}}>{{$month}}</option>

                                    @endforeach
                                </select>
                                @php
                                $currentYear= ($date_type == 'nepali') ? date("Y") + 57 : date("Y");
                                @endphp
                                <select class="age-calc calc-chg" name="birth_year" id="birthYear">
                                    <option value="" disabled>YYYY</option>
                                    @for($i=0;$i<=100;$i++)
                                    <option
                                    value={{$currentYear - $i }} {{$birth_year && $birth_year == ($currentYear - $i) ? "selected" : ''}}>{{$currentYear - $i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <input min="11" max="70" type="number" id="total-age" name="age" value="{{$selectedAge}}" readonly
                            class="cmp-datepkr">
                        </div>
                        @endif

                        @if($selectedCategory == 'couple')
                        <div class="comare-lst">
                       <p>Husband </p> x
                            <input type="number" id="age" name="husband_age" value="{{$selectedHusbandAge}}"
                            class="cmp-datepkr calc-chg">
                        </div>

                        <div class="comare-lst">
                        <p>Wife Age</p> 
                            <input type="number" id="age" name="wife_age" value="{{$selectedWifeAge}}"
                            class="cmp-datepkr calc-chg">
                        </div>
                        @endif

                        @if($selectedCategory == 'children')
                        <div class="comare-lst">
                          <p>Child Age</p> 
                            <input type="number" id="age" name="child_age" value="{{$selectedChildAge}}"
                            class="cmp-datepkr calc-chg">
                        </div>

                        <div class="comare-lst">
                       <p>Proposer's Age</p>
                            <input type="number" id="age" name="proposer_age" value="{{$selectedProposersAge}}"
                            class="cmp-datepkr calc-chg">
                        </div>
                        @endif

                        <div class="comare-lst">
                            <p>Terms</p>
                            <select id="term" name="term"
                            class="form-control calc-chg radio-select-box sidecalc-btn" required>
                            <option selected disabled>Select a term</option>
                            @foreach($terms as $term)
                            <option value="{{ $term }}"
                            @if($selectedTerm == $term) selected @endif>{{ $term }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="comare-lst">
                    <p>Sum Assured</p>
                        <input type="number" id="sum_assuredf" name="sum_assured"
                        value="{{$selectedSumAssured}}" class="cmp-datepkr calc-chg">
                    </div>
                    <div class="comare-lst">
                     <p>Mode of Payment</p> 
                        <select id="mop" name="mop"
                        class="custom-select calc-chg form-control radio-select-box sidecalc-btn"
                        autocomplete="off" required>

                        <option selected disabled>Select a mop</option>
                        @if(isset($mops))
                        @foreach($mops as $mop)
                        <option value="{{ $mop }}" @if($selectedMop == $mop) selected
                        @endif class="text-capitalize">
                        {{ str_replace('_', ' ',ucfirst($mop)) }}
                    </option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="comare-lst btn-group btn-group-toggle" data-toggle="buttons">
                <p>Company</p>

                @foreach($products->unique('company_id') as $product)
                <div class="compare-company-listing"><input type="checkbox" multiple="multiple" name="company_id[]"
                autocomplete="off" class="calc-chg "
                value="{{$product->company->id}}"
                @if($product->company->id == $product->company_id) checked @endif> <span>{{$product->company->name}}</span>
                <i class="fa fa-check selected-cmp d-none"></i></div>
            </label>

            @endforeach
        </div>

        <div class="comare-lst btn-group btn-group-toggle" data-toggle="buttons">
            <p>Features</p>

            @foreach($availableFeatures as $feature)
            <div>
                <input
                type="checkbox"
                class="calc-chg company-feature"
                name="features[]"
                value="{{ $feature->code }}"
                >
                <label>{{ $feature->name }}</label>
            </div>
            @endforeach
        </div>

        <input type="hidden" name="category" value="{{$selectedCategory}}">
    </div>
</form>
</div>
<div id="compare-result">
    @include('frontend.Partials.compareResult')
</div>
</div>
</div>
</section>
 <div class="mobile-filter-button"><button>Filter</button></div>
@endsection
@section('css')
<link href="https://ispl.ebeema.com/css\nepali.datepicker.v3.7.min.css" rel="stylesheet" type="text/css"/>
@endsection
@section('script')
    <script src="{{asset('js\nepali.datepicker.v3.7.min.js')}}" type="text/javascript"></script>
    @include('frontend.components.compare_result_script')
@endsection
