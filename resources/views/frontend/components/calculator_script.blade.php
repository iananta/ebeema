<script>
    let nepalidate = NepaliFunctions.GetCurrentBsDate();

    window.onload = function () {
        var mainInput = document.getElementById("nepali-datepicker");
        if (mainInput) {
            mainInput.nepaliDatePicker();
        }
    };

    $(document).ready(function () {
        $("[data-toggle=tooltip]").tooltip();
        $('#benefit').on('submit', function (e) {
            e.preventDefault();

            let feature = $('#feature').val();
            $.ajax({
                url: "/benefit",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    feature: feature,
                },
                success: function (response) {
                    // console.log(response);
                },
            });
        });


    });

    $(document).on('change', '#sumAssuredRange', function () {
        var val = this.value;
        val = parseInt(val).toLocaleString('en-IN');
        $('.tooltip-inner').text('Rs. ' + val);
        $('#sumAmt').text(val);
    });

    function add_one() {
        var val = $('#sumAssuredRange').val();
        if (val < 10000000) {
            val = parseInt(val) + 10000;
        }
        newVal = val.toLocaleString('en-IN');
        $('#sumAssuredRange').val(val);
        $(" #sumAssuredRange").attr('data-original-title', 'Rs. ' + newVal);
        $('#sumAmt').text(newVal);
    }

    function subtract_one() {
        var val = $('#sumAssuredRange').val();
        if (val > 0) {
            val = parseInt(val) - 10000;
        }
        newVal = val.toLocaleString('en-IN');
        $('#sumAssuredRange').val(val);
        $(" #sumAssuredRange").attr('data-original-title', 'Rs. ' + newVal);
        $('#sumAmt').text(newVal);
    }

    $('.nonlife-foot').on('click', function (e) {
        e.preventDefault()
        localStorage.removeItem('state')
        location.href = $(this).attr('href')
    })
</script>

<script>
    $(document).ready(function () {
        $('.search-select').select2();
        $('#ageForm').on('submit', function (e) {
            e.preventDefault();

            let day = $('#day').val();
            let month = $('#month').val();
            let year = $('#year').val();
            $.ajax({
                url: "/age",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    day: day,
                    month: month,
                    year: year,

                },
                success: function (response) {

                },
            });
        });
    });
    $('input[name = term1]').on('change', function () {
        $('input[name = term]').val($('input[name = term1]:checked').val());
        claculateSum();
    });
    $('input[name = sum_assured1]').on('change', function () {
        $('input[name = sum_assured]').val($('input[name = sum_assured1]:checked').val());
        claculateSum();
    });

    $('.sum-cal').on('input', function () {
        claculateSum();
    });

    function claculateSum() {
        var term = $('input[name = term]').val();
        var sum = $('input[name = sum_assured]').val();
        if (term && invest) {
            $('input[name = invest]').val((sum / term).toFixed(2));
        }
    }

    $('input[name = invest]').on('input', function () {
        var term = $('input[name = term]').val();
        var invest = $('input[name = invest]').val();
        if (term && invest) {
            $('input[name = sum_assured1]:checked').prop("checked", false);
            $("#invest > .check-item").removeClass('active');
            $('input[name = sum_assured]').val((invest * term).toFixed(2));
        }
    });
    @if(isset($_GET['category']))
        localStorage.setItem('category',"{{$_GET['category']}}");
    @endif
    $('.cat').on('change',function(){
        localStorage.removeItem('category')
        localStorage.setItem('category',$(this).val());
    })
    $('.age-calc').on('input', function () {
        var cat=localStorage.getItem('category')
        if (cat) {
            switch (cat) {
                case 'children':
                    var dateType = $('input[name = date-type-child][checked=checked]').val();
                    break;
                case 'couple':
                    var dateType = $('input[name = date-type-couple]:checked').val();
                    break;
                default:
                    var dateType = $('input[name = date-type]:checked').val();
                    break;
            }
        }
        calculateAge($(this),dateType)
    });
    function calculateAge(_this,dateType){
        var age = 0;
        var parent = _this.parent().parent();
        var form=parent.parent().parent().parent()
        var totalAgeId=parent.find('#total-age')
        var minAgeId = totalAgeId.attr('min');
        var maxAgeId = totalAgeId.attr('max');
        if(parent.attr('class') =='comare-lst'){
            parent=parent.find('.date_selection')
        }
        var day = parent.find('#birthDay').val()
        var month = parent.find('#birthMonth').val()
        var year =parent.find('#birthYear').val()

        var totalAge=parent.find('.total-age')
        var minAge = totalAge.attr('min');
        var maxAge = totalAge.attr('max');
        if (dateType == "english" && day > 0 && month > 0 && year > 0) {
            age = calculate_age(new Date(year, month, day));
        }
        if (dateType == "nepali" && day > 0 && month > 0 && year > 0) {
            var EngDate = NepaliFunctions.BS2AD({year: year, month: month, day: day});
            age = calculate_age(new Date(EngDate.year, EngDate.month, EngDate.day));
        }
        if(totalAge){
            totalAge.removeAttr('style')
            totalAge.val(age)
            form.find('#age').val(age)
            localStorage.setItem('age',age)
            let dateOfBirth=day+'/'+month+'/'+year
            localStorage.setItem('dateOfBirth',dateOfBirth)
         }
         if(totalAgeId){
            totalAgeId.removeAttr('style')
            totalAgeId.val(age)
         }
        if ((parseInt(minAge) > parseInt(age) || parseInt(maxAge) < parseInt(age) || parseInt(minAgeId) > parseInt(age) || parseInt(maxAgeId) < parseInt(age)) && day!=null  && month !=null && year!=null) {
            if(totalAge){
                 totalAge.css("box-shadow", "0px 0px 2px rgb(0 0 0 / 50%)")
            }
                Swal.fire({
                    position: 'top-end',
                    icon: 'warning',
                    html: "<h5>" + age + " is not compactible age. Age from " + minAge + " to " + maxAge + " is only allowed !</h5>",
                    timerProgressBar: true,
                    timer: 2500
                });

            parent.find('.total-age').css("box-shadow", "3px 2px 13px rgb(255 0 0)");
        }
    }
    $('.ftCalculate').on('change', function () {
        let _this = $(this)
        categoryCalc(_this);
    });
    categoryCalc($('.ftCalculate'))

    function categoryCalc(_this) {
        if (_this != undefined) {
            let plan = _this.val()
            let wrapper = _this.parent().parent()
            let minAge = $('option:selected', _this).attr('data-min-age')
            let maxAge = $('option:selected', _this).attr('data-max-age')
            let childAge = $('option:selected', _this).attr('data-child-age')
            //console.log(parseInt(minAge),parseInt(maxAge),childAge);
            let parent = _this.parent()
            let ageDiv = wrapper.find('#age_container'),
                coupleDiv = wrapper.find('#couple_container'),
                childDiv = wrapper.find('#child_container');
            const style = 'd-none'
            let message;
            if (plan) {

                $('.short-note').remove()
                switch (plan) {
                    case 'endowment':
                        message = '<p class="short-note">"An endowment policy is a policy designed to pay a lump sum after a specific term (on its "maturity") or on death. This plan has low premium and high return and has basic coverage."</p>';
                        parent.append(message)
                        break;
                    case 'money-back':
                        message = '<p class="short-note">“Money Back plan is designed to pay you part of sum assured in intervals during the policy term. Here you will receive the remaining sum assured and bonus at maturity.”</p>';
                        parent.append(message)
                        break;
                    case 'whole-life':
                        message = '<p class="short-note">“Whole life insurance is a policy which gives the insurer coverage for lifetime while paying premium for limited number of years. At maturity accumulated bonus and sum assured is paid back."</p>'
                        parent.append(message)
                        break;
                    case 'term':
                        message = '<p class="short-note">“Term Life Insurance is a type of insurance which provides coverage for a certain period of time or term. This policy does not have return at maturity. If the insurer dies during the time period of the policy a death benefit will be paid."</p>'
                        parent.append(message)
                        break;
                    case 'pension':
                        message = '<p class="short-note">“A pension or a retirement plan is a plan that gives you benefits after the maturity of your policy. At maturity you will receive bonus and get the rest in installments through-out your life (depending on the company).”</p>'
                        parent.append(message)
                        break;
                    case 'retirement-pension':
                        message = '<p class="short-note">“A pension or a retirement plan is a plan that gives you benefits after the maturity of your policy. At maturity you will receive bonus and get the rest in installments through-out your life (depending on the company).”</p>'
                        parent.append(message)
                        break;
                    case 'children':
                        message = '<p class="short-note">“Child plan is a type of insurance that insures the life of a minor. Child policy normally has a proposer instead of a nominee. This gives parents the opportunity to secure the child’s future. In the market this plan is also known as education plan because this plan lets you save money for your child’s further education."</p>'
                        parent.append(message)
                        break;
                    case 'couple':
                        message = '<p class="short-note">“Couple- plan is designed to give coverage to both spouses as well as return on maturity. Here you will be paid sum assured and accumulated bonus at maturity, and if one of the spouses dies during the policy, they will receive death benefits likewise."</p>'
                        parent.append(message)
                        break;
                    default:
                        message = '<p class="short-note">"The insurance plan for you and your love one together with dual benefits"</p>'
                        parent.append(message)
                        break;
                }
                switch (plan) {
                    case 'couple':
                        var totalAge=wrapper.find('#couple_container').find('.total-age')
                        ageDiv.addClass(style)
                        coupleDiv.removeClass(style)
                        childDiv.addClass(style)
                        totalAge.attr({
                            "min": parseInt(minAge),        // min age of the category
                            "max": parseInt(maxAge)          // max age of the category
                        });
                        break;
                    case 'children':
                        var totalAge=wrapper.find('#child_container').find('.total-age')

                        ageDiv.addClass(style)
                        coupleDiv.addClass(style)
                        childDiv.removeClass(style)
                        totalAge.attr({
                            "min": parseInt(minAge),        // min age of the category
                            "max": parseInt(maxAge)          // max age of the category
                        });
                        $(".child-age").attr({
                            "min": parseInt(childAge.split("-")[0]),        // min age of the category
                            "max": parseInt(childAge.split("-")[1])          // max age of the category
                        });
                        break;
                    case 'education':
                        ageDiv.addClass(style)
                        coupleDiv.addClass(style)
                        childDiv.removeClass(style)
                        break;
                    default:
                        var totalAge=wrapper.find('#age_container').find('#total-age')
                        ageDiv.removeClass(style)
                        totalAge.attr({
                            "min": parseInt(minAge),        // min age of the category
                            "max": parseInt(maxAge)          // max age of the category
                        });
                        coupleDiv.addClass(style)
                        childDiv.addClass(style)
                        break;
                }
            }
        }
    }

    function calculate_age(dob) {
        var diff_ms = Date.now() - dob.getTime();
        var age_dt = new Date(diff_ms);
        return Math.abs(age_dt.getUTCFullYear() - 1970);
    }
    $('.birthDay').on('change',function(){
        if($(this).hasClass('storebirthDay')){
            let age=$(this).parent().parent().find('.store-age').val()
            localStorage.setItem('storeage',age)
            localStorage.setItem('birthDay',$(this).val())
        }
    })
    $('.birthMonth').on('change',function(){
        if($(this).hasClass('storebirthMonth')){
            let age=$(this).parent().parent().find('.store-age').val()
            localStorage.setItem('storeage',age)
            localStorage.setItem('birthMonth',$(this).val())
        }
    })
    $('.birthYear').on('change',function(){
        if($(this).hasClass('storebirthYear')){
            let age=$(this).parent().parent().find('.store-age').val()
            localStorage.setItem('storeage',age)
            localStorage.setItem('birthYear',$(this).val())
        }
    })
    $('#Custlogin').on('click', function (e) {
        e.preventDefault();
        let _this=$(this)
        let form=_this.parent().parent()
        let category=localStorage.getItem('category')
        let formData=form.serializeArray()
            formData.push({"name": "category",'value':category})
            formData.push({"name": "term",'value':localStorage.getItem('term')})
            formData.push({"name": "sumassured",'value':localStorage.getItem('sumassured')})
            if(category != 'children' && category!='couple'){
                formData.push({"name": "age",'value':localStorage.getItem('age')})
                formData.push({"name": "dob",'value':localStorage.getItem('dateOfBirth')})
            }else{
                let birthday=localStorage.getItem('birthDay')
                let birthMonth=localStorage.getItem('birthMonth')
                let birthYear=localStorage.getItem('birthYear')
                let dob=birthday+'/'+birthMonth+'/'+birthYear
                formData.push({"name": "age",'value':localStorage.getItem('storeage')})
                formData.push({"name": "dob",'value':dob})
            }

            console.log(formData)
        $.ajax({
            type: "POST",
            url: "{{route('frontend.add.lead')}}",
            data:formData,
            dataType: "json",
            beforeSend: function () {
                $('#preloader').show();
            },
            success: function (response) {
                if (response.data == 1) {
                    // $('#preloader').hide(500);
                    $(".calFormsubmit").submit();
                } else {
                    setTimeout(function(){
                        $('#preloader').hide();
                    },1000)
                    swal({
                        icon: "success",
                        text: "something went wrong",
                    });
                }
            },
            error: function (response) {
                let parent = _this.parent().parent().parent().parent()
                let formgroup = parent.find('.modal-content form .form-group')
                let validationTag = $('.custom-validation-message-error')
                if (validationTag) {
                    validationTag.remove()
                }
                setTimeout(function(){
                    $('#preloader').hide();
                },1000)
                $("#AddKycCustomer").prop('disabled', false);
                if (response.responseJSON && response.responseJSON.errors) {
                    let res = response.responseJSON
                    let obj = res.errors
                    for (const [key, value] of Object.entries(obj)) {
                        formgroup.find('input[name="' + key + '"]').parent().append('<p class="custom-validation-message-error">' + value + '</p>')
                        formgroup.find('select[name="' + key + '"]').parent().append('<p class="custom-validation-message-error">' + value + '</p>')
                    }
                } else {
                    let res = response.responseJSON
                    parent.find('form').append('<p class="custom-validation-message-error">' + res.message + '</p>')
                }

            }
        });
    });

    $(document).on('click','.data-calc',function (e) {
        e.preventDefault();
        let parentForm=$(this).parent().parent()
        let requiredForm = parentForm.find('.checkValidation')
        let requiredVal = []
        requiredForm.each(function(){
            requiredVal.push($(this).val())
        })
        if(requiredVal[0]!='' && requiredVal[1]!='' && requiredVal[2]!=''){
            $(this).addClass('oveflowHidden')
                localStorage.setItem('term',requiredVal[1])
                localStorage.setItem('sumassured',requiredVal[2])

             $('.frontLifeForm').removeClass('calFormsubmit')
            parentForm.addClass('calFormsubmit')
             $('.verifyCustomer').modal('show');
        }else{
            Swal.fire({
                position: 'top-end',
                icon: 'warning',
                html: "<h5>Complete your form details</h5>",
                timerProgressBar: true,
                timer: 2500
            });
        }

    });
    $('.customize-input').on('click',function(){
        $(this).val('');
        let parent=$(this).parent().parent().parent()
        let checkinput=parent.find('.check-item')
            checkinput.removeClass('active')
    })
    let fiscal = "1st Shrawan " + nepalidate.year
    document.cookie = "fiscal=" + fiscal;

    $('.date-type').each(function () {
        $(this).on('change',function(){
            let name=$(this).attr('name')
            $('.date-type[name='+name+']').removeAttr('checked')
            $(this).attr('checked',true)
            dateCondition($(this))
        })
        dateCondition($(this))
    })

    function dateCondition(_this) {
        let parent = _this.parent().parent();
        if (parent.attr('class') == 'form-group') {
            parent.parent().find('.form-group').each(function () {
                parent.parent().find('.total-age').val(0)
                let wrapper = $(this).find('.date-type-wrapper')
                if (wrapper != undefined) {
                    englishnepaliDate(_this, wrapper)
                }
            })
        } else {
            parent.find('.total-age').val(0)
            let wrapper = _this.parent().parent();
            englishnepaliDate(_this, wrapper)

        }
    }

    function englishnepaliDate(_this, wrapper) {
        if (_this != undefined) {
            let val = _this.val()
            let birthMonth = wrapper.find('.date-form-wrapper').find('.date-form-item').find('.birthMonth')
            let birthYear = wrapper.find('.date-form-wrapper').find('.date-form-item').find('.birthYear')
            let birthDay = wrapper.find('.date-form-wrapper').find('.date-form-item').find('.birthDay')
            let dateFormat = wrapper.find('.date-format')
            birthMonth.html('')
            birthYear.html('')
            birthDay.html('')
            birthMonth.append('<option selected disabled value=" ">MM</option>')
            birthYear.append('<option selected disabled value=" ">YYYY</option>')
            birthDay.append('<option selected disabled value=" ">DD</option>')

            for (let i = 1; i <= 32; i++) {
                birthDay.append('<option value="' + i + '">' + i + '</option>')
            }
            dateFormat.css('display', 'flex')
            if (val == 'english') {
                <?php foreach(months() as $key=>$month){ ?>
                birthMonth.append('<option value="<?php echo $key; ?>"><?php echo $month; ?></option>')
                <?php } ?>
                <?php
                $currentYear = date("Y");
                for($i = 0;$i <= 100;$i++) {
                ?>
                birthYear.append('<option value="<?php echo $currentYear - $i ?>"><?php echo $currentYear - $i ?></option>')
                <?php } ?>
            } else if (val == 'nepali') {
                <?php foreach(nepali_months() as $key=>$month){ ?>
                birthMonth.append('<option value="<?php echo $key; ?>"><?php echo $month; ?></option>')
                <?php } ?>
                <?php
                $currentYear = date("Y") + 57;
                for($i = 0;$i <= 100;$i++) {
                ?>
                birthYear.append('<option value="<?php echo $currentYear - $i ?>"><?php echo $currentYear - $i ?></option>')
                <?php } ?>
            } else {
                dateFormat.hide()
            }
        } else {

        }
    }
</script>
