<script>
        // $('#payback-table').dataTable();

        var maxLength = 2;
        var showHideBtns = '<span class="show-button"><i class="glyphicon glyphicon-plus"></i> Show more</span><span class="show-button" style="display:none"><i class="glyphicon glyphicon-minus"></i> Show less</span>';

        $('ul.allcats').each(function () {
            if ($(this).children('li').length > maxLength) {
                $(this).children('li:gt(' + (maxLength - 1) + ')').addClass('toggleable').hide();
                $(this).append(showHideBtns);
            }
        });

        $('.show-button').click(function () {
            $(this).parent('li').siblings('.toggleable').slideToggle();
            $(this).parent('li').children('.show-button').toggle();
        });

        $('.calc-chg').on('change', function (e) {
            e.preventDefault();
            $.ajax({
                url: "/compare/result",
                type: "POST",
                data: $('#policyfilter').serialize(),
                beforeSend: function () {
                   $('#preloader').show();
                },
                success: function (response) {
                   setTimeout(function(){
                        $('#preloader').hide();
                        $('#compare-result').empty().html(response);
                        $('[data-toggle="tooltip"]').tooltip();
                   },1000)
                },
                error: function (e) {
                    $('#preloader').hide(500);
                    console.log('something went wrong');
                }
            });
        });


        $('.feature-list').on('change', function () {
            var checkedVal = [];

            var values = $(".feature-list:checked").each(function () {
                checkedVal.push($(this).val());
            });

            $.ajax({
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "featureCode": checkedVal,
                    "is_ajax": 1,
                    "category": $('#selectedCat').val(),
                    "age": $('#selectedAge').val(),
                    "term": $('#selectedTerm').val(),
                    "sum_assured": $('#selectedSum').val(),
                    "mop": $('#mop').val(),

                    // "sum_assured":<?php echo $selectedSumAssured ?>,
                    // "term" :<?php echo $selectedTerm ?>,
                },
                success: function (response) {

                    var data = response;
                    if (data.length > 0) {

                        for (var i = 0; i < data.length; i++) {

                        }

                    }


                },

                error: function (e) {

                    // $('.sum-title-val').text('2312');
                    //  $('#pamount').text('response.data[j].code');

                    console.log('something went wrong');
                },
            });


        });
        $('.age-calc').on('input', function () {
            var day = $('#birthDay').val();
            var month = $('#birthMonth').val();
            var year = $('#birthYear').val();
            var dateType=$('.date-type').val()
            var age = 0;
            if (dateType == "english" && day > 0 && month > 0 && year > 0) {
                age = calculate_age(new Date(year, month, day));
            }
            if (dateType == "nepali" && day > 0 && month > 0 && year > 0) {
                var EngDate = NepaliFunctions.BS2AD({year: year, month: month, day: day});
                age = calculate_age(new Date(EngDate.year, EngDate.month, EngDate.day));
            }
            $('#total-age').val(age);
        });

        function calculate_age(dob) {
            var diff_ms = Date.now() - dob.getTime();
            var age_dt = new Date(diff_ms);
            return Math.abs(age_dt.getUTCFullYear() - 1970);
        }
        let dropWrapper=$('.sort-dropdown-content')

        $(document).on('click','.sort-inital p',function(){
            $('.sort-dropdown-content').toggleClass('active');
        })
        $(document).on('click','.mb-filter',function(){
            $('.sort-dropdown-content').toggleClass('active');
        })
        let getSortText=localStorage.getItem('sortingText')
        if(getSortText){
            $('.sortChange').html(getSortText)
        }
        $('.sort-dropdown-content ul li').on('click',function(){
            let sortVal=$(this).find('.sort-radio').val()
            let text=$(this).find('label').html()
                $('.sorting-val').val(sortVal)
                localStorage.setItem('sortingText',text)
                dropWrapper.removeClass('active')
                $('.sortingForm').submit();
        })
        $('.company-feature').on('click',function(){
                $('#preloader').show(20);
                setTimeout(function(){
                    $('#preloader').hide();
                },2000)
        })
        let selectorPlan=$('.view-plan')
        let targetPlan=$('.view-plan-block')
        let closePlan=$('.close-btn')
        selectorPlan.on('click',function(){
            let animi=targetPlan.find('.view-plan-wrapper')
            let id=$(this).data('id')
            $.ajax({
                type:'post',
                url:"{{route('compare.viewplan')}}",
                data:{
                    _token:"{{csrf_token()}}",
                    id:id,
                },
                beforeSend:function(){
                    $('.view-plan-content-wrapper').html('')
                    $('html').css('overflow','hidden')
                },
                success:function(response){
                    let html=response.headerPart
                    html+=response.benifit

                    console.log(response)
                    targetPlan.css('display','flex')
                    animi.addClass('popup-zoom-in')
                    animi.removeClass('popup-zoom-out')
                    $('.view-plan-content-wrapper').html(html)
                }
            })
        })
        closePlan.on('click',function(){
            let animi=targetPlan.find('.view-plan-wrapper')
            $('html').removeAttr('style')
            targetPlan.css('display','none')
            animi.removeClass('popup-zoom-in')
            animi.addClass('popup-zoom-out')
        })
    </script>