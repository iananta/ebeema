<script>
        $(document).ready(function () {
            $('.items').slick({
                dots: false,
                nextArrow: document.getElementById('slick-next'),
                prevArrow: document.getElementById('slick-previous'),
                infinite: true,
                speed: 800,
                autoplay: true,
                autoplaySpeed: 2000,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        dots: true
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                },

                ]
            });

            $('.whyItems').slick({
                dots: true,
                infinite: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 2000,
                responsive: [
                {
                    breakpoint: 2600,
                    settings: 'unslick'
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                ]
            });
        });
        //Filled required for captcha
        window.onload = function() {
            var $recaptcha = document.querySelector('#g-recaptcha-response');

            if($recaptcha) {
                $recaptcha.setAttribute("required", "required");
            }
            const $form = document.querySelector('form');
            $form.addEventListener('submit', (event) => {
                event.preventDefault();
                console.log('prevented submit by code for demo');
            });
        };
        $('.video-pop-wrapper').append('{!!@$tutorial->value!!}');
        function createCookie(name, value, days, path) {
            let expires = "";
            if (days) {
                let date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            }
            else expires = "";
            document.cookie = name + "=" + value + expires + "; path=" + path;
        }

        function readCookie(name) {
            let nameEQ = name + "=";
            let ca = document.cookie.split(';');
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
        this.walkthroughInitial=function(){
            let _this=this
            let stepOne=$('#stepOne')
            let stepTwo=$('#stepTwo')
            let left=0;
            let top=0;
            let nextBtn=$('.next-button button')
            let cookieExpiry = 30;
            let cookieStatus =true;
            let getCookie = readCookie('walkthrough');
            this.findScreenWidth=function(){
                return $(window).width()
            }
            this.stepPosition=function(target){
                return target.offset()
            }
            this.stylePosition=function(target,targetPosition,left,top){
               target.css('top',top)
               target.css('left',parseInt(_this.stepPosition(targetPosition).left) + parseInt(left))
           }
           this.nextStepListener=function(){
            nextBtn.on('click',function(){
                let target=$(this)
                let targetDiv=$('#'+target.data('target'))
                let targetPosition=$('#'+target.data('position'))
                if(_this.stepPosition(targetPosition) == undefined){
                    $('html').removeAttr('style')
                    $('.walkthrough-wrapper').removeClass('active')
                    createCookie('walkthrough', 'yes', cookieExpiry, cookieStatus);
                    return
                }
                left=parseInt(targetDiv.data('left'))
                top=parseInt(targetDiv.data('top')) + parseInt(_this.stepPosition(targetPosition).top)

                $('.walkthrough-item').removeClass('active')
                $('.walkthrough-item').removeAttr('style')
                _this.stylePosition(targetDiv,targetPosition,left,top)
                targetDiv.addClass('active')

            })
        }
        this.init=function(){
            @if($walkthrough!= 0)
            if(getCookie == null && _this.findScreenWidth() >=800){
                $('html').css('overflow','hidden')
                $('.walkthrough-wrapper').addClass('active')
                window.scrollTo({ top: 0, behavior: 'smooth' });
            }
            @endif

            if(_this.findScreenWidth() <= 1084){
                left= - 400
                top=$('#topMedia').height() + 60
            }
            else if(_this.findScreenWidth() <= 1233){
                left= - 300
                top=$('#topMedia').height() + 20
            }else{
                top=$('#topMedia').height() + $('.navbar.navbar-default.main-navigation').height() + $('#first-step').height() / 2 + 15
            }
            _this.stylePosition($('#first-step'),$('#stepOne'),left,top)
            _this.nextStepListener()
        }
    }
    let walkThroughObj=new walkthroughInitial()
    walkThroughObj.init()


    $(document).ready(function(){
        let button=$('.view-all-product-button')
        let list=$('.product-list-item')
        let itemWrapper=$('.insurance-product-items-wrapper')
        let popupwrapper=$('.more-product-popup')

        button.on('click',function(){
          let animi=popupwrapper.find('.popup-wrapper')
          if(animi){
            $('html').addClass('no-scroll')
            popupwrapper.css('display','flex')
            animi.addClass('popup-zoom-in')
            animi.removeClass('popup-zoom-out')
        }
    })
        list.on('click',function(){
            let target=$(this).data('target')
            list.removeClass('active')
            itemWrapper.removeClass('active')
            $(this).addClass('active')
            $('#'+target).addClass('active')
        })
        $('.product-cross-btn').on('click',function(){
            let animi=popupwrapper.find('.popup-wrapper')
            animi.removeClass('popup-zoom-in')
            animi.addClass('popup-zoom-out')
            setTimeout(function(){
                $('html').removeClass('no-scroll')
                popupwrapper.css('display','none')

            },600)
        })
    })

</script>