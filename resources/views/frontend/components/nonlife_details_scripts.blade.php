<script type="text/javascript">
    this.nonlifeDetailsInitial=function(){
        let _this=this
        this.progressBarStickyListener=function(){
            let bar=$('.nonlife-progress-bar-wrapper')
            $(window).on('scroll',function(){
               let height= $(window).scrollTop();
               if(height > 110){
                bar.addClass('active')
               }else{
                bar.removeClass('active')
               }
            })
        },
        this.nonlifeHelperListener=function(){
            function getSelectedDistrict() {
                var selectedCustomer = $('#CustomerList').find(":selected").data('details');
                var cForm = $('#calculationForm');
                cForm.find('[name="DISTRICTID"]').val(selectedCustomer.DISTRICTID);
                cForm.find('[name="DISTRICTID"]').change();
                cForm.find('[name="ISSUE_DISTRICT_ID"]').val(selectedCustomer.ISSUE_DISTRICT_ID);
                 cForm.find('[name="ISSUE_DISTRICT_ID"]').change();
            }

            function getSelectedMnuVdc() {
                var selectedCustomer = $('#CustomerList').find(":selected").data('details');
                var cForm = $('#calculationForm');
                // console.log(selectedCustomer);
                cForm.find('[name="MUNICIPALITYCODE"]').val(selectedCustomer.MUNICIPALITYCODE);
                cForm.find('[name="VDCCODE"]').val(selectedCustomer.VDCCODE);
            }
        
        },
        this.getModelListener=function(){
            $('#MAKEVEHICLEID').on('change', function () {
                var mfComp = $("#MAKEVEHICLEID option:selected").val();
                $.ajax({
                    type: "POST",
                    url: "{{ url('Nonlifeinsurance/calculator/manufacture-model') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        mfComp: mfComp
                    },
                    dataType: 'json',
                    success: function (response) {
                        var data = response.output;
                        // console.log(data);
                        $('#MAKEMODELID').empty().append($('<option>', {
                            text: "Select Model",
                            disabled: "disabled",
                            selected: ''
                        }));
                        for (var i = 0; i < data.length; i++) {
                            $('#MAKEMODELID').append($('<option>', {
                                value: data[i].ID,
                                text: data[i].ENGNAME
                            }));
                        }
                    },
                    error: function (e) {
                        console.log('something went wrong' + this.error);
                    }
                });
            })  
            $("#MAKEMODELID").on('change', function(){
            $("#modelName").val($("#MAKEMODELID option:selected").text());
        });
        },
        this.stepNextFormListener=function(target){
            localStorage.setItem('state',target.attr('id'))
            let nextBtn=$('.nonlife-next')
            let current,next
            current=target.parent().parent().parent()
            next=current.next()
            let parent=nextBtn.parent().parent().prev();
            $('#non-life-progressbar li').eq($("fieldset").index(next)).addClass("active")
            $('#non-life-progressbar li').removeClass('current')
            $('#non-life-progressbar li').eq($("fieldset").index(next)).addClass("current")
            next.show();
            current.animate({opacity: 0}, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;
                    current.css({
                        'display': 'none',
                    });
                    next.css({'opacity': opacity});
                },
                duration: 600
            });
            next.addClass('active')
            $(window).scrollTop(0);
        },
        this.stepPrevFormListener=function(){
            let previousBtn=$('.nonlife-previous');
            let current,previous
            previousBtn.on('click',function(){
                current=$(this).parent().parent().parent()
                previous = current.prev();
                //Remove class active
                $("#non-life-progressbar li").eq($("fieldset").index(current)).removeClass("active");
                $('#non-life-progressbar li').removeClass('current')
                $('#non-life-progressbar li').eq($("fieldset").index(previous)).addClass("current")
                //show the previous fieldset
                previous.show();
                //hide the current fieldset with style
                current.animate({opacity: 0}, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current.css({ 
                            'display': 'none',
                        });
                        previous.css({'opacity': opacity});
                    },
                    duration: 600
                });
                $(window).scrollTop(0);
            })
        },
        this.districtApiRequest=function(provinceID =null,districtId=null,ISSUE_DISTRICT_ID_LIST=null ){
               $.ajax({
                type: "POST",
                url: "{{ route('province.district') }}",
                data: {
                    provinceID: provinceID,
                    _token: "{{ csrf_token() }}"
                },
                dataType: 'json',
                success: function(response) {
                    //console.log(response)
                    $('#districtId').empty().append($('<option>', {
                        text: "Select District",
                        value:'',
                        selected: (districtId == null) ? 'selected' : ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        $('#districtId').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION,
                            selected: (districtId != null && response[i].ID == districtId) ? 'selected' : null
                        }));
                    }

                    $('#ISSUE_DISTRICT_ID_LIST').empty().append($('<option>', {
                        text: "Select Issued District",
                        disabled: "",
                        selected: (ISSUE_DISTRICT_ID_LIST == null) ? 'selected' : ''
                    }));
                    for (var i = 0; i < response.length; i++) {
                        //console.log(response[i].ID)
                        $('#ISSUE_DISTRICT_ID_LIST').append($('<option>', {
                            value: response[i].ID,
                            text: response[i].EDESCRIPTION,
                            selected: (ISSUE_DISTRICT_ID_LIST != null && response[i].ID == ISSUE_DISTRICT_ID_LIST) ? 'selected' : null
                        }));
                    }
                    if ($('#calculationForm').find('[name="ZONEID"]').val()) {
                        _this.nonlifeHelperListener().getSelectedDistrict();
                    }
                },
                error: function(e) {
                    console.log('something went wrong');
                }
            });
        },
        this.getProvinceListener=function(){
            let provinceID=$('#provinceId').val();
            $('#provinceId').on('change', function() {
                    var provinceID = $(this).val();
                    _this.districtApiRequest(provinceID)
            });
            if(provinceID){
                 let districtId=$('#districtId').data('selected')
                 let ISSUE_DISTRICT_ID_LIST = $('#ISSUE_DISTRICT_ID_LIST').data('selected')
                _this.districtApiRequest(provinceID,districtId,ISSUE_DISTRICT_ID_LIST)
            }
        },
        this.vdcApiRequest=function(DistrictID=null,MNUId=null,VCDID=null){
               $.ajax({
                type: "POST",
                url: "{{ route('district.mnuvdc') }}",
                data: {
                    DistrictID: DistrictID,
                    "_token": "{{ csrf_token() }}"
                },
                dataType: 'json',
                success: function(response) {
                            // console.log(response);
                            $('#MNUId').empty().append($('<option>', {
                                text: "Select Municipality ",
                                value:'',
                                disabled: '',
                                selected: (MNUId == null) ? 'selected' : ''
                            }));
                            for (var i = 0; i < response.mnu.length; i++) {
                                $('#MNUId').append($('<option>', {
                                    value: response.mnu[i].MNUCODE,
                                    text: response.mnu[i].MNU,
                                    selected: (MNUId != null && response.mnu[i].MNUCODE == MNUId) ? 'selected' : null
                                }));
                            }
                            $('#VDCId').empty().append($('<option>', {
                                text: "Select VDC ",
                                value: '',
                                disabled: '',
                                selected: (VCDID == null) ? 'selected' : ''
                            }));
                            for (var j = 0; j < response.vdc.length; j++) {
                                $('#VDCId').append($('<option>', {
                                    value: response.vdc[j].VDCCODE,
                                    text: response.vdc[j].VDC,
                                    selected:(VCDID != null && response.vdc[j].VDCCODE == VCDID) ? 'selected' : null
                                }));
                               
                            }
                            if ($('#calculationForm').find('[name="DISTRICTID"]').val()) {
                                _this.nonlifeHelperListener().getSelectedMnuVdc();
                            // console.log('im on for mnuVdc');
                        }
                    },
                    error: function(e) {
                        console.log('something went wrong');
                    }
                });
        }
        this.getDistrictListener=function(){
            let DistrictID=$('#districtId').data('selected')
            let ISSUE_DISTRICT_ID = $('ISSUE_DISTRICT_ID_LIST').data('selected')
            $('#districtId').on('change', function() {
                var DistrictID = $(this).val();
                _this.vdcApiRequest(DistrictID)
            });
            $('#ISSUE_DISTRICT_ID_LIST').on ('change', function() {
                var ISSUE_DISTRICT_ID = $(this).val();
            });
            if(DistrictID){
                 let VCDID =$('#VDCId').data('selected')
                 let MNUId=$('#MNUId').data('selected')
                _this.vdcApiRequest(DistrictID,MNUId,VCDID)
            }
        },
        this.validationErrorMessage=function(errorObj){
            let formgroup=$('.non-life-group-wrapper')
            if(errorObj){
                for (const [key, value] of Object.entries(errorObj)) {
                    formgroup.find('input[name="' + key + '"]').css('border','1px solid #e74c3c')  
                    formgroup.find('select[name="' + key + '"]').next().find('.selection .nonlife-details-container').css('cssText','border:1px solid #e74c3c !important')  
                    if(key=='bluebook_images'){
                        formgroup.find('input[name="' + key + '"]').parent().append('<p class="custom-validation-message-error">' + value + '</p>')
                    }
                }
            }
        },
        this.clientSideValidation=function(target){
            let count=0
            let parent=document.querySelector(target)
            let requiredInput=parent.querySelectorAll('input[required]')
            let requiredSelect = parent.querySelectorAll('select[required]');

            Array.prototype.forEach.call(requiredInput, function(element){
                if(!element.value){
                    if(element.getAttribute('type') == 'file'){
                        element.parentNode.querySelector('label').setAttribute('style', 'border:1px solid #ff0000 ') 
                    }else{
                        element.style.border = '1px solid #ff0000';
                    }
                    count++
                }
            });
            Array.prototype.forEach.call(requiredSelect, function(element){
                if(!element.value && element.parentNode.querySelector('.nonlife-details-container')){

                        element.parentNode.querySelector('.nonlife-details-container').setAttribute('style', 'border:1px solid #ff0000  !important')
                        count++
                }
            });
            if(count > 0){
                $(window).scrollTop(0)
            }
            return count
            
        }
        this.vehicleDetailsFormSubmitListener=function(){
            let selector=$('#vechiledetail')
            if(selector){
                selector.on('click', function (e) {
                    e.preventDefault();
                    let target=$(this)
                    var frm = $('#vehicleForm');
                    var formData = new FormData(frm[0]);
                    let count=_this.clientSideValidation('#vehicleForm')
                   if(count==0){
                           $.ajax({
                            type: "POST",
                            url: '{{ url("Nonlifeinsurance/calculator/motor/detail") }}',
                            data: formData,
                            processData: false,
                            contentType: false,
                            beforeSend: function () {
                                $('#preloader').show();
                            },
                            success: function (response) {
                                setTimeout(function(){
                                    $('#preloader').hide();
                                },2000)


                                if (response.data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Congratulations!',
                                        html: response.message ?? "Vehicle Added Successfully",
                                        showConfirmButton: false,
                                        timer: 5000,
                                        timerProgressBar: true,
                                    });                            
                                }
                                _this.stepNextFormListener(target)
                            },
                            error: function (response) {
                                $(window).scrollTop(0);
                                setTimeout(function(){
                                    $('#preloader').hide();
                                },2000)
                                if(response.responseJSON){
                                    let res=response.responseJSON
                                    let errorObj = res.errors
                                    _this.validationErrorMessage(errorObj)
                                }else{
                                   Swal.fire({
                                    position: 'top-end',
                                    icon: 'error',
                                    title: 'Error',
                                    html: response.error ?? "Somthing were wrong",
                                    showConfirmButton: false,
                                    timer: 5000,
                                    timerProgressBar: true,
                                });
                               }
                               
                           }
                       })
                   }
                })
            }
        },
        this.kycFormCatListener=function(){
            let selector=$('.kyc-cat-list li')
            let next =$('.kyc-next-btn')
            let prev=$('.kyc-previous-btn')
           
            function KcyTabOperation(target){
                let count=_this.clientSideValidation('.kyc-form-item.active')
                if(count == 0){
                     let point=target.data('target')
                    let title=target.data('title')
                    $('.kyc-cat-list li').removeClass('active')
                    $('.kyc-form-item').removeClass('active')
                    target.addClass('active')
                    $('#'+point).addClass('active')
                    $('.kyc-cat-title').html(title)
                }
                $(window).scrollTop(0)
            }
            function buttonHideShow(){
               $('.kyc-form-item').each(function(index,value){
                   if($(this).hasClass('active') && index == 1){
                     prev.hide()
                     next.show()

                   }else if($(this).hasClass('active') && index == 0){
                     prev.show()
                     next.show()
                   }else if($(this).hasClass('active') && index == 2){
                     prev.show()
                     next.hide()
                   }
               })
            }
            buttonHideShow()
            selector.on('click',function(){
                KcyTabOperation($(this))
                buttonHideShow()
            })
            next.on('click',function(){
                let count=_this.clientSideValidation('.kyc-form-item.active')
                if(count ==0){
                    let findNext=$('.kyc-cat-list li.active').next()
                    if(findNext.hasClass('kyc-tab-item')){
                       KcyTabOperation(findNext)
                       buttonHideShow()
                    }
                }
            })
            prev.on('click',function(){
                let findPrev=$('.kyc-cat-list li.active').prev()
                if(findPrev.hasClass('kyc-tab-item')){
                    KcyTabOperation(findPrev)
                    buttonHideShow()

                }
            })
   

        },
        this.kycFormsubmitListener=function(){
            let selector=$('#kycsubmit')
            if(selector){
                selector.on('click', function (e) {
                    e.preventDefault();
                    var frm = $('#kycform');
                    var formData = new FormData(frm[0]);
                    let target=$(this)
                    let count=_this.clientSideValidation('#kycform')
                    // var numberOfHindiCharacters = 128;
                    // var unicodeShift = 0x0900;
                    // var hindiAlphabet = [];
                    // for(var i = 0; i < numberOfHindiCharacters; i++) {
                    // hindiAlphabet.push("\\u0" + (unicodeShift + i).toString(16));
                    // }
                    // var regex = new RegExp("(?:^|\\s)["+hindiAlphabet.join("")+"]+?(?:\\s|$)", "g");
                    // var string1 = "abcde वायरस abcde";
                    // var string2 = "abcde abcde";

                    // [ string1.match(regex), string2.match(regex) ].forEach(function(match) {
                    // if(match) {
                    //     console.log("String contains " + match.length + " words with Hindi characters only.");
                    // } else {
                    //     console.log("String does NOT contain any words with Hindi characters only.");
                    // }
                    // });
                    if(count == 0){
                          $.ajax({
                        type: "POST",
                        url: "{{ route('user.kyc.store') }}",
                        data: formData,
                        dataType:'json',
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            setTimeout(function(){
                                    $('#preloader').hide();
                            },2000)
                            if (response.data) {
                                $('.review-confirm-wrapper').remove()
                                $('.review-payment-block-wrapper').remove()
                                $('.review').append(response.data)   
                                location.reload() 
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Congratulations!',
                                    html: response.message ?? "KYC Added Successfully",
                                    showConfirmButton: false,
                                    timer: 5000,
                                    timerProgressBar: true,
                                });
                            }
                            _this.stepNextFormListener(target)

                        },
                        error: function (response) {
                            $(window).scrollTop(0);
                            setTimeout(function(){
                                    $('#preloader').hide();
                            },2000)
                            if(response.responseJSON){
                                let res=response.responseJSON
                                let errorObj = res.errors
                                _this.validationErrorMessage(errorObj)
                            }else{
                                 Swal.fire({
                                    position: 'top-end',
                                    icon: 'error',
                                    title: 'Error',
                                    html: response.error ?? "Somthing were wrong".errorObj,
                                    showConfirmButton: false,
                                    timer: 5000,
                                    timerProgressBar: true,
                                });
                            }

                           
                        }
                    });
                    }
                });
            }
        },
        this.kycAddCustomerListener=function(){
             $('#AddKycCustomer').on('click', function(e) {
                e.preventDefault();
                var frm = $('#calculationForm');
                var formData = new FormData(frm[0]);
                $("#AddKycCustomer").prop('disabled', true);
                $.ajax({
                type: "POST",
                url: "{{ route('user.kyc.entry') }}",
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('#preloader').show();
                },
                success: function (response) {
                    $('#preloader').hide();
                    $('#calculationForm')[0].reset();
                    $("#AddKycCustomer").prop('disabled', false);
                    // console.log(response.message);
                    if (response.data) {
                        $("#makeModalClose").trigger("click");
                        $('#CustomerList').append($('<option>', {
                            value: response.data.customer_id,
                            text: response.data.INSUREDNAME_ENG + ' : ' + response.data.MOBILENO,
                            selected: true,
                        }));
                        $('#CustomerList').find(":selected").data('details', response.data);
                        $('#printCustomerDetails').empty().append('<div class="col-sm-6"><ul id="custmrList"><li>Customer Name: ' + response.data.INSUREDNAME_ENG + '|' + response.data.INSUREDNAME_NEP + '</li><li>Address: ' + response.data.ADDRESS + '</li><li>Citizenship No.: ' + response.data.CITIZENSHIPNO + '</li><li>Date Of Birth: ' + response.data.DATEOFBIRTH + '</li><li>Email: ' + response.data.EMAIL + '</li></ul></div><div class="col-sm-6"><ul id="custmrList"> <li>Fathers Name: ' + response.data.FATHERNAME + '</li><li>Grandfather Name: ' + response.data.GRANDFATHERNAME + '</li><li>Grandmother Name: ' + response.data.GRANDMOTHERNAME + '</li><li>Mobile Number: ' + response.data.MOBILENO + '</li><li>Mothers Name: ' + response.data.MOTHERNAME + '</li><li>Temporary Address: ' + response.data.TEMPORARYADDRESS + '</li></ul></div>');
                        $('.cst-detail').removeClass('d-none');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            html: response.message ?? "Customer Added Successfully",
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                        // swal("Congratulations!", response.message ?? "Customer Added Successfully", "success");
                    }
                    // console.log(response);
                },
                error: function (response) {
                    $(window).scrollTop(0);
                    $('#preloader').hide();
                    $("#AddKycCustomer").prop('disabled', false);
                    var err = '';
                    if (response.responseJSON && response.responseJSON.errors) {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.errors)) {
                            err += '-' + value + '<br>';
                        }
                    } else {
                        var msg = response.responseJSON;
                        for (let value of Object.values(msg.message)) {
                            err += '-' + value + '<br>';
                        }
                    }
                    // console.log(err);
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: typeof msg != 'object' && msg !== null ? msg.message : 'Error',
                        html: err ?? 'Something went wrong',
                        showConfirmButton: false,
                        timer: 2500,
                        timerProgressBar: true,
                    });
                }
            });
            });
  
        },
        this.reviewAccordianListener=function(){
            $(document).on('click','.accordian-button',function(){ 
                let parent=$(this).parent().parent();
                           $(this).toggleClass('active')
                let datawrapper=parent.find('.accordian-data-wrapper')
                    if($(this).hasClass('active')){
                        datawrapper.slideDown()
                    }else{
                        datawrapper.slideUp()

                    }
                    

            })
        },
        this.paymentOptionListener=function(){
            $('.payment-btn').on('click',function(){
                if($(this).hasClass('active')){
                    $('.proceed-payment').slideUp()
                }else{
                    $('.proceed-payment').slideDown()
                }
                 $(this).toggleClass('active')
            })
        },
        this.previewImageListener=function(){
            function readUrlListener(target,jTarget){
                let parent=jTarget.parent()
                let size =parseInt(target.files[0].size);
                if(size > 2000000){
                    new swal('error','The file size should not be greater than 2 MB','error')
                    return 
                }
                if (target.files && target.files[0]) {
                    var reader = new FileReader();
                
                    reader.onload = function (e) {
                        let previewHtml=$('<div />',{
                                            class: 'preview-image-wrapper'
                                        });
                        let img=$('<img />', { 
                                  id: 'pre-img',
                                  src: e.target.result,
                                  alt: e.target.result
                                });
                        let close='<button class="dismiss-preview">&times;</button>'
                        previewHtml.append(img)
                        previewHtml.append(close)
                        previewHtml.insertAfter(parent)
                    };
                    reader.readAsDataURL(target.files[0]);
                }
            }
            $('.custom-upload-file input').on('change',function(){
                readUrlListener(this,$(this))
            })
            $(document).on('click','.dismiss-preview',function(){
                let parent=$(this).parent().parent() 
                parent.find('input').val('')
                $(this).parent().remove()
            })
        },
        
        this.datePickerListener=function(){
                let englishdate=$('#dob-english-date').val()
                if(englishdate){
                    var Dt = NepaliFunctions.ConvertToDateObject(englishdate, "YYYY-MM-DD");
                    var NepDate = NepaliFunctions.AD2BS(Dt);
                    if(NepaliFunctions.ConvertDateFormat(NepDate)){
                         $('#nepaliDatePicker').val(NepaliFunctions.ConvertDateFormat(NepDate))
                    }
                }
                    $('#nepaliDatePicker').nepaliDatePicker({
                        ndpYear: true,
                        ndpMonth: true,
                        ndpEnglishInput: 'dob-english-date'
                    });
                     $('#NEPALIREGISTRATIONDATE').nepaliDatePicker({
                        ndpYear: true,
                        ndpMonth: true,
                        ndpEnglishInput: 'english-date'
                    });
        },
        this.init=function(){
                _this.progressBarStickyListener()
                _this.getModelListener()
                _this.stepPrevFormListener()
                _this.getProvinceListener()
                _this.getDistrictListener()
                _this.vehicleDetailsFormSubmitListener()
                _this.kycFormCatListener()
                _this.kycAddCustomerListener()
                _this.kycFormsubmitListener()
                _this.reviewAccordianListener()
                _this.paymentOptionListener()
                _this.previewImageListener()

                _this.datePickerListener()
               
                $(window).on('load',function(){
                    let state=localStorage.getItem('state')
                    if(state){
                        _this.stepNextFormListener($('#'+state))
                        $('#non-life-progressbar li').each(function(key,value){
                             if($(this).hasClass('current')){
                                $(this).prevAll().addClass('active')
                             }
                         })
                    }else{
                        $('.fieldset-wrapper fieldset:first-child').show()
                    }
                })
                $('.othernonlife-next').on('click',function(){
                    _this.stepNextFormListener($(this))
                })

                
                $('.form-control').on('input',function(){
                        $(this).removeAttr('style')

                })
                $('.form-control').on('change',function(){
                    $(this).next().find('.selection .nonlife-details-container').removeAttr('style')

                })
                $(".select-option").select2({
                    placeholder: "Select One",
                    allowClear: false,
                    width: '40%',
                    containerCssClass: "nonlife-details-container" 
                });

                $(window).on('load',function(){
                    $(window).scrollTop(0);
                })
                $('.pay-button').on('click',function(){
                    if(!$('#imepay').is(':checked')){
                        new swal('Warning','Please select the payment method','warning')
                    }
                })
        }
    }   
    let nonlifeDetailsObj=new nonlifeDetailsInitial();
        nonlifeDetailsObj.init();  
        $(document).ready(function() {
            $('.search-select').select2();
            getDetails();
        });
        function getDetails() {
            var details = $('.search-select').find(':selected').data('details');
            // console.log(details);
            if (details) {
                $('#printCustomerDetails').empty().append('<ul id="custmrList"><li>Customer Name: ' + details
                    .INSUREDNAME_ENG + '</li><li>Address: ' + details.ADDRESS + '</li><li>Citizenship No.: ' + details
                    .CITIZENSHIPNO + '</li><li>Date Of Birth: ' + details.DATEOFBIRTH + '</li><li>Email: ' + details
                    .EMAIL + '</li><li>Fathers Name: ' + details.FATHERNAME + '</li><li>Grandfather Name: ' + details
                    .GRANDFATHERNAME + '</li><li>Grandmother Name: ' + details.GRANDMOTHERNAME +
                    '</li><li>Mobile Number: ' + details.MOBILENO + '</li><li>Mothers Name: ' + details.MOTHERNAME +
                    '</li><li>Temporary Address: ' + details.TEMPORARYADDRESS + '</li></ul>');
            }
        };       
        $('#dropDownId').attr('disabled', true);
        $('#vehicleId').attr('disabled', true);
    </script>