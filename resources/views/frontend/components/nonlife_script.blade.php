<script>
	this.nonlifeInitial=function(){
		let _this=this
		this.nonlifeTab=function(){
			$('.nonlife-insurance-type ul li').on('click',function(){
				let target=$(this).data('target')
				$('.nonlife-insurance-type ul li').removeClass('active')
				$(this).addClass('active')
				$('.nonlife-tab-content').hide()
				$('#'+target).show();
			});
		},
		this.radioButtonListener=function(){
			$(document).on('click','.radio-item',function(){
				let wrapper=$(this).parent()
				radioActiveHelper($(this),wrapper)
			})
			function radioActiveHelper(_this,wrapper){
				let item=wrapper.find('.radio-item')
				item.removeClass('active')
				_this.addClass('active')
				item.find('input').removeAttr('checked')
				_this.find('input').attr('checked','checked')
				if($('input[name="NCDYR"]:checked').val() > 0){
					$('.nonlife-claim').show()
				}else{
					$('.nonlife-claim').hide()

				}
			}
		},
		this.excessDamageListener=function(){
			$('.excess-damage').on('change', function () {
				var TYPECOVER = $("#TYPECOVERVal").val();
				var CATEGORYID = $("#categoryListId").val();
				var CLASSCODE = $("#CLASSCODE").val();
				var VEHICLETYPE = 0;
				console.log(TYPECOVER,CATEGORYID,CLASSCODE,VEHICLETYPE)
				$.ajax({
					type: "POST",
					url: "{{ url('Nonlifeinsurance/calculator/excess-damage') }}",
					data: {
						TYPECOVER: TYPECOVER,
						CATEGORYID: CATEGORYID,
						CLASSCODE: CLASSCODE,
						VEHICLETYPE: VEHICLETYPE,
						_token: "{{ csrf_token() }}"
					},
					dataType: 'json',
					success: function (response) {
						let active;
						if (response.excessdamages) {
							$('#ExcessOwnDamage').empty();
							for (var i = 0; i < response.excessdamages.length; i++) {
								if(i == 0){
									active='active'
								}else{
									active='';
								}
								let excessDiv='<div class="radio-item '+active+'">\
													<label>'+response.excessdamages[i].TARIFF+'</label>\
													<input type="radio" name="EODAMT" value="'+response.excessdamages[i].TARIFF+'">\
												</div>'
								$('#ExcessOwnDamage').append(excessDiv);
							}
							if(CATEGORYID == null || CATEGORYID == ''){
								$('.visibel-none').css('visibility','hidden');
							}else{
								$('.visibel-none').css('visibility','visible');
							}
						}
					},
					error: function (e) {
						console.log('something went wrong');
					}
				})
			})
		},
		this.calculateCompulsaryExcessListener=function(){
			$('.excessCalculate').on('change', function () {
				var TYPECOVER = $("#TYPECOVERVal").val();
				var CATEGORYID = $("#categoryListId").val();
				var YEARMANUFACTURE = $("#manufYear").val();

				$.ajax({
					type: "POST",
					url: "{{ url('Nonlifeinsurance/calculator/compulsary-excess') }}",
					data: {
						TYPECOVER: TYPECOVER,
						CATEGORYID: CATEGORYID,
						YEARMANUFACTURE: YEARMANUFACTURE,
						_token: "{{ csrf_token() }}"
					},
					dataType: 'json',
					success: function (response) {
						console.log(response.CompulsoryExcess);
						if (response.CompulsoryExcess != 0) {
							$('.cmp-excess-amt').val(response.CompulsoryExcess);
						} else {
							$('.cmp-excess-amt').val('0');
						}
					},
					error: function (e) {
						console.log('something went wrong');
					}
				});
			});
		},
		this.ncdyearListener=function(){
			$(".ncdCheck").on('change', function (){
            var currentYear = new Date().getFullYear();
            var selectedYear = $(".fcManuYear").val();
            var noClaimDisc = $("#NCDYR").val();
            if(currentYear && selectedYear && currentYear == selectedYear && noClaimDisc >= 1){
                $("#NCDYR").val(0).change();
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error',
                    html: 'Something went wrong: No Claim Discount cannot be taken for this current year of manufacture.',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                });
            }
           //  console.log(selectedYear,currentYear,noClaimDisc);
        });
		},
		
		this.init=function(){
			_this.nonlifeTab()
			_this.radioButtonListener()
			_this.excessDamageListener()
			_this.calculateCompulsaryExcessListener()
			_this.ncdyearListener()
			$(".select-option").select2({
				placeholder: "Select One",
				allowClear: false,
				width: '100%',
				containerCssClass: "nonlife-type-container" 
			});
		}
	}
	let nonlifeObj=new nonlifeInitial()
		nonlifeObj.init()

		
</script>