@extends('frontend.layouts.app')
@section('content')
    @php
        $amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
        $amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
        $amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
    @endphp
    @php($user = customerLead(Session::get('customerLeadId') ?? ''))
    <div class="container-fluid" id="grad1">
        <div class="row justify-content-center mt-0">
            <div class="col-11 col-sm-9 col-md-7 col-lg-12 text-center p-0 mt-3 mb-2">
                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">

                    <div class="row">
                        <div class="col-md-12 col-12 mx-0">
                            <form
                                method="post"
                                id="msform"
                                enctype="multipart/form-data">
                                @csrf

                                <ul id="progressbar">
                                    <li class="active" id="account"><strong>Confirm</strong></li>
                                    <li id="invoice"><strong>Invoice</strong></li>
                                    <li id="personal"><strong>Personal</strong></li>
                                    <li id="payment"><strong>Payment</strong></li>
                                    <li id="confirm"><strong>Finish</strong></li>
                                </ul>

                                <fieldset>
                                     <h1 class="fs-title step-title">
                                            <input
                                                type="hidden"
                                                name="product"
                                                placeholder="prod"
                                                value="{{$product->id}}"
                                            />
                                            Selected Product: {{$product->name}}({{$product->company->name}})
                                        </h1>

                                    <div class="form-card container">
                                        <!-- Customer Name -->
                                        <label for="name">Name:</label>
                                        <input
                                            type="text"
                                            name="name"
                                            id="name"
                                            value="{{$user->customer_name ?? ''}}"
                                            placeholder="Name"
                                        />
                                        <input type="hidden" name="lead_id" value="{{Session::get('customerLeadId') ?? null}}">

                                        <input type="hidden" name="category" value="{{$selectedCategory ?? null}}">
                                        <input type="hidden" name="product_id" value="{{$selectedProduct ?? null}}">
                                        <input type="hidden" name="bonus" value="{{$bonus ?? null}}">
                                        <input type="hidden" name="net_gain" value="{{$net_gain ?? null}}">
                                        <input type="hidden" name="return" value="{{$return ?? null}}">
                                        <input type="hidden" name="company_id" value="{{$company_id ?? null}}">
                                        <input type="hidden" name="invest" value="{{$invest ?? null}}">
                                        <input type="hidden" name="premium" value="{{$premium ?? null}}">
                                        <input type="hidden" name="birth_date" value="{{$birth_date ?? null}}">
                                        <input type="hidden" name="birth_month" value="{{$birth_month ?? null}}">
                                        <input type="hidden" name="birth_year" value="{{$birth_year ?? null}}">

                                        <!-- Ages -->
                                        <input type="hidden" name="age" value="{{$selectedAge ?? ''}}">
                                        <input type="hidden" name="child_age" value="{{$selectedChildAge ?? ''}}">
                                        <input type="hidden" name="proposer_age" value="{{$selectedProposerAge ?? ''}}">
                                        <input type="hidden" name="husband_age" value="{{$selectedHusbandAge ?? ''}}">
                                        <input type="hidden" name="wife_age" value="{{$selectedWifeAge ?? ''}}">

                                        <!-- Term -->
                                        <label for="term">Term:</label>
                                        <input
                                            type="number"
                                            id="term"
                                            value="{{$selectedTerm ?? ''}}"
                                            disabled
                                        />
                                        <input type="hidden" name="term" value="{{$selectedTerm ?? ""}}"/>

                                        <!-- Sum Assured -->
                                        <label for="sum">Sum Assured:</label>
                                        <input
                                            type="number"
                                            id="sum"
                                            placeholder="Sum Assured"
                                            value="{{$selectedSumAssured ?? ''}}"
                                            disabled
                                        />
                                        <input type="hidden" name="sum_assured" value={{$selectedSumAssured ?? ''}} />

                                        <!-- MOP -->
                                        <label for="mop">Mode of Payment:</label>
                                        <select
                                            id="mop"
                                            name="mop"
                                            class="custom-select radio-select-box sidecalc-btn"
                                            autocomplete="off"
                                            required
                                            disabled
                                        >
                                            <option selected disabled>Select a mop</option>
                                            @if(isset($loadingCharges))
                                                @foreach($loadingCharges as $charge)
                                                    <option
                                                        value="{{ $charge }}"
                                                        @if($charge == $selectedMop ?? '') selected @endif
                                                        class="text-capitalize"
                                                    >
                                                        {{ str_replace('_', ' ', $charge) }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <input type="hidden" name="mop" value="{{$selectedMop ?? ''}}"/>

                                        <!-- Features -->
                                        <label for="benefits">Benefits:</label>
                                        @if(count($product->features) > 0)
                                            <div class="btn-group btn-group-toggle " data-toggle="buttons">
                                                @if(isset($product->features))
                                                    @foreach($product->features as $feature)
                                                        <ul>
                                                            <li>
                                                                {{ featureName($feature['code']) }}
                                                                <input
                                                                    type="hidden"
                                                                    name="feature"
                                                                    value="{{$feature['code']}}"
                                                                >
                                                            </li>
                                                        </ul>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @else
                                            <small>No Benefits selected</small>
                                        @endif
                                    </div>
                                    <input
                                        type="button"
                                        name="next"
                                        class="next action-button confirm"
                                        value="Next Step"
                                    />
                                </fieldset>
                                <fieldset>
                                    <h2 class="fs-title step-title">Invoice</h2>
                                    <div class="form-card">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2">
                                                    <h3 class="font-weight-bold">Policy Timeline</h3>
                                                </th>
                                                <th colspan="2">
                                                    <h3 class="font-weight-bold">
                                                        Pay For <br> {{$selectedTerm ?? ''}} years
                                                    </h3>
                                                </th>
                                                <th colspan="2">
                                                    <h3 class="font-weight-bold">
                                                        Sum Assured: <br> {{convertCurrency($selectedSumAssured ?? '')}}
                                                    </h3>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                        @include('Backend.Compare.includes.invoice-table')
                                    </div>
                                    <input
                                        type="button"
                                        name="previous"
                                        class="previous action-button-previous"
                                        value="Previous"
                                    />
                                    <input
                                        type="button"
                                        name="next"
                                        class="next action-button"
                                        value="Next Step"
                                    />
                                </fieldset>

                                <fieldset id="per-info">
                                        <h2 class="fs-title step-title">Personal Information</h2>

                                    <div class="form-card">

                                        <input
                                            type="email"
                                            name="email"
                                            value="{{$user->email ?? ''}}"
                                            placeholder="Email Address"
                                        />

                                        <input
                                            type="text"
                                            name="phone"
                                            value="{{$user->phone ?? ''}}"
                                            placeholder="Contact No."
                                            autocomplete="off"
                                        />

                                        <div class="citezen_wrapper">
                                            <div class="front-wrapper-citizen">
                                                <label>Citizenship Front Image</label>
                                                <input
                                                    type="file"
                                                    name="front_citizen"
                                                    accept="image/*"
                                                    class="front-citizen citizen-input"
                                                >
                                            </div>
                                            <div class="back-wrapper-citizen">
                                                <label>Citizenship Back Image</label>
                                                <input
                                                    type="file"
                                                    name="back_citizen"
                                                    accept="image/*"
                                                    class="back-citizen citizen-input"
                                                >
                                            </div>
                                        </div>
                                    </div>

                                    <input
                                        type="button"
                                        name="previous"
                                        class="previous action-button-previous"
                                        value="Previous"
                                    />
                                    <input
                                        type="button"
                                        name="next"
                                        class="next action-button"
                                        value="Next Step"
                                    />
                                </fieldset>

                                <fieldset class="con-payment">
                                    <h2 class="fs-title step-title compayent">Request for payment collection</h2>
                                    <div class="form-card compayment">
                                        <div class="form-group payment-method collection">
                                                <button
                                                type="button"
                                                name="next"
                                                class="payment-confirm"

                                                >
                                                Confirm Request
                                            </button>
                                        </div>
                                    </div>

                                    <input
                                        type="button"
                                        name="previous"
                                        class="previous action-button-previous"
                                        value="Previous"
                                    />
                                    <button
                                                type="submit"
                                                name="next"
                                                id="sbtn-policy"
                                                class="next action-button step"
                                                value="Next Step"
                                                >
                                                Confirm Request
                                    </button>
                                </fieldset>

                                <fieldset>
                                    <div class="form-card">
                                        <h2 class="fs-title text-center">Success !</h2> <br><br>
                                        <div class="row justify-content-center">
                                            <div class="col-3 text-center">
                                                <img
                                                     src="https://img.icons8.com/color/96/000000/ok--v2.png"
                                                     class="fit-image icon"
                                                     alt="image"
                                                >
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row justify-content-center">
                                            <div class="col-7 text-center">
                                                <h5>Successfully processed insurance policy from eBeema.</h5>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    @include('frontend.components.confirmation_script')
@endsection
