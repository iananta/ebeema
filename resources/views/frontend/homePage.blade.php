@extends('frontend.layouts.app')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/slick-theme.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/slick.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/walkthrough.css')}}">
@endsection
@section('seo_contents')
<title>{{$seoData->where('page_name','homepage')->first()->meta_title ?? ''}}</title>
<meta name="title" content="{{$seoData->where('page_name','homepage')->first()->meta_title ?? ''}}">
<meta name="description" content="{{$seoData->where('page_name','homepage')->first()->meta_description ?? ''}}">
@endsection
@section('content')
<seciton class="banner-section">
    <div class="container">
        <div class="banner-wrapper">
            <div class="right-banner-wrapper">
                <div class="right-banner-text">
                    <h1>Compare the <b>Best Insurance Policies</b> for you and your family.</h1>
                    <div class="video-block">
                        <div class="video-wrapper">
                            <div class="video-play-button" id="stepTwo">
                                <a href="#" class="oveflowHidden"><img
                                    src="{{asset('frontend/img/playbutton.svg')}}"
                                    alt="tutorial playbutton"></a>
                                </div>
                                <div class="video-text">
                                    <h5>Watch tutorials</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="select-insurance-type" >
                        <form class="insurance-product-compare" method="get" action="#">
                            <select class="insurance-lists">
                                <option value="" selected>Select a insurance type</option>
                                @foreach($productCategories as $category)
                                <option value="{{ $category->category_code }}">
                                    {{ ucfirst($category->name) }}
                                </option>
                                @endforeach
                            </select>
                            <button type="submit" class="insurance-type-submit">Compare&nbsp;&nbsp;<img
                                src="{{asset('frontend/img/calculator.svg')}}" alt="compare calculator" id="stepOne"></button>
                            </form>
                        </div>
                    </div>
                    <div class="left-banner-wrapper">
                        <div class="banner-slider-wrapper owl-carousel">
                            @foreach($banners as $key=>$banner)
                            <div class="banner-item">
                                <div class="banner-item-wrapper">
                                    <div class="banner-content-wrapper">
                                        {{$banner->title}}
                                        <p>{{$banner->description}}</p>
                                    </div>
                                    <div class="banner-image-wrapper">
                                        <img src="{{asset('uploads/banner/'.$banner->image)}}" alt="banner title">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="insurance-product-wrapper">
                 <div id="insurance-policy" class="list-of-insurance-policy">
                    <div class="policy-item">
                        <div class="policy-content">
                            <a href="{{url('calculator')}}?category=endowment">
                                <div class="policy-img"><img src="{{asset('frontend/img/policy/investment.svg')}}"
                                 alt="endowment plan"></div>
                                 <p class="ins-policy-title"><b>Endownment</b> Plan</p>
                             </a>
                         </div>
                     </div>
                     <div class="policy-item">
                        <div class="policy-content">
                            <a href="{{url('calculator')}}?category=money-back">
                                <div class="policy-img"><img src="{{asset('frontend/img/policy/money-back.svg')}}"
                                 alt="money plan"></div>
                                 <p class="ins-policy-title"><b>Money Back</b> Plan</p>
                             </a>
                         </div>
                     </div>
                     <div class="policy-item">
                        <div class="policy-content">
                            <a href="{{url('calculator')}}?category=whole-life">
                                <div class="policy-img"><img src="{{asset('frontend/img/policy/heartbeat.svg')}}"
                                 alt="wholelife plan"></div>
                                 <p class="ins-policy-title"><b>Whole Life</b> Plan</p>
                             </a>
                         </div>
                     </div>
                     <div class="policy-item">
                        <div class="policy-content">
                            <a href="{{url('calculator')}}?category=couple">
                                <div class="policy-img"><img src="{{asset('frontend/img/policy/in-love.svg')}}"
                                 alt="couple plan"></div>
                                 <p class="ins-policy-title"><b>Couple</b> Plan</p>
                             </a>
                         </div>
                     </div>
                     <div class="policy-item">
                        <div class="policy-content">
                            <a href="{{url('calculator')}}?category=children">
                                <div class="policy-img"><img src="{{asset('frontend/img/policy/child.svg')}}"
                                 alt="child plan"></div>
                                 <p class="ins-policy-title"><b>Child</b> Plan</p>
                             </a>
                         </div>
                     </div>
                     <div class="policy-item">
                        <div class="policy-content">
                            <a href="{{url('calculator')}}?category=term">
                                <div class="policy-img"><img src="{{asset('frontend/img/policy/shield.svg')}}"
                                 alt="term life"></div>
                                 <p class="ins-policy-title"><b>Term</b> Life</p>
                             </a>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="list-of-insurance-policy">
                <div class="policy-item">
                    <div class="policy-content">
                        <a class="" href="{{ route('frontend.nonlife.insurance') }}">
                            <div class="policy-img"><img src="{{asset('frontend/img/policy/motorcycle.svg')}}"
                             alt="two wheeler"></div>
                             <p class="ins-policy-title"><b>2 Wheeler</b> Plan</p>
                         </a>
                     </div>
                 </div>
                 <div class="policy-item">
                    <div class="policy-content">
                        <a class="" href="{{ route('private.car.nonlifeinsurance') }}">
                            <div class="policy-img"><img src="{{asset('frontend/img/policy/car.svg')}}"
                             alt="four wheeler"></div>
                             <p class="ins-policy-title"><b>4 Wheeler</b> Plan</p>
                         </a>
                     </div>
                 </div>
                 <div class="policy-item">
                    <div class="policy-content">
                        <a class="view-all-product-button" href="javascript:;">
                            <div class="policy-img"><img src="{{asset('frontend/img/policy/view-menu.svg')}}"
                             alt="four wheeler"></div>
                             <p class="ins-policy-title"><b>View All Product</b></p>
                         </a>
                     </div>
                 </div>
             </div>
         </div>
     </seciton>
     <section class="corporate-plan-section">
        <div class="container">
            <div class="corporate-plan-content-wrapper">
                <div class="corporate-plan-content">
                    <h1>Corporate Plan</h1>
                    <p>If you want to know more about plans for your employees.</p>
                    <div class="get-assistance">
                        <button type="button" class="corporate-plan">Get assistance</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="why-different">
        <div class="why-diff-title">
            <h2 class="text-capitalize text-center">Why eBeema?</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-12 why-different-div">
                    @if($whyDifferentContent)
                    <div class="why-diff-left  text-left">
                        <div class="diff-top">
                           {!!  $whyDifferentContent->why_diff_title !!} <a class="arrow-img" href="#"><img
                                src="{{asset('frontend/img/different-arrow.png')}}" alt="arrow"></a>
                            </div>
                            {!! $whyDifferentContent->why_diff_content !!}
                        </div>
                        @endif
                    </div>
                    <div class="col-sm-7 col-xs-12 why-different-items">
                        <div class="row">
                            @foreach($whyDifferent as $whyD)
                            <div class="col-sm-6 why-diff-right">
                                <img src="{{$whyD->image}}" title="{{$whyD->title}}" alt="titleD image">
                                {{$whyD->title}}
                                <p>{{$whyD->description }}</p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="how-it-works">
            <div class="container-fluid">
                <div class="how-works-title">
                    <h2 class="text-capitalize text-center">How eBeema works</h2>
                </div>
                <div class="row working-bar">
                    <div class="col-sm-2 col-xs-12 resp-width-wrk">
                        <a href="{{url('calculator')}}}">
                            <img src="{{asset('frontend/img/works/visit-ebeema.svg')}}" alt="visit ebeema">
                            <p class="how-first-prag">{{__('Visit Ebeema.com')}}</p>
                        </a>
                        <p>{{__('Visit our website ebeema.com')}}</p>
                    </div>
                    <div class="col-sm-2 col-xs-12 resp-width-wrk">
                        <a href="{{url('calculator')}}">
                            <img src="{{asset('frontend/img/works/compare-price.svg')}}" alt="compare prices">
                            <p class="how-first-prag">{{__('Compare Prices')}}</p>
                        </a>
                        <p>{{__("Compare the prices and returns of different insurance plans.")}}</p>

                    </div>
                    <div class="col-sm-2 col-xs-12 resp-width-wrk">
                        <a href="{{url('calculator')}}">
                            <img src="{{asset('frontend/img/works/buy-online.svg')}}" alt="online buy">
                            <p class="how-first-prag">{{__('Buy online')}}</p>
                        </a>
                        <p>{{__('Buy online and get a call from our support team.')}}</p>
                    </div>
                    <div class="col-sm-2 col-xs-12 resp-width-wrk">
                        <a href="{{url('calculator')}}">
                            <img src="{{asset('frontend/img/works/get-update.svg')}}" alt="policy updated">
                            <p class="how-first-prag">{{__('Get updated')}}</p>
                        </a>
                        <p>{{__('Closely monitor your insurance policy and be updated.')}}</p>

                    </div>
                    <div class="col-sm-2 col-xs-12 resp-width-wrk">
                        <a href="{{url('calculator')}}">
                            <img src="{{asset('frontend/img/works/insure-yourself.svg')}}" alt="secure life">
                            <p class="how-first-prag">{{__('Insure your self')}}</p>
                        </a>
                        <p>{{__('Live life knowing your loved ones are secure.')}}</p>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="why-us d-none">
            <div class="container">
                <div class="row p-5">
                    <div class="col-sm-6 why-us-left-banner">
                        @if($whyUsContent)
                       {{$whyUsContent->why_us_title}} 
                        <br>
                        {!! $whyUsContent->why_us_content !!}
                        <a href="/about" class="btn btn-primary text-uppercase">Learn more</a>
                        @endif
                    </div>
                    <div class="col-sm-6 why-us-right-banner">
                        <div class="row whyItems">
                            @if($whyUs)
                            @foreach($whyUs as $key => $why)
                            <div class="card">
                                <div class="col-sm-6 card-body why-us-card-body @if($loop->even) make-space @endif">
                                    <div class="text-center why-box-right">
                                        <img src="{{$why->image}}" title="{{$why->title}}" alt="title image">
                                        <h3 class="text-uppercase">{{$why->title}}</h3>
                                        <p>{{ $why->description }}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
     <section class="compare-now">
        <div class="row comp-color">
            <div class="col-sm-1"></div>
            <div class="col-sm-4 compare-left-img">
                <img src="{{asset('uploads/compare.png')}}" alt="compare insurance">
            </div>
            <div class="col-sm-7 compare-right text-center">
                <h3>You’ve made it here!</h3>
                <p>We try to make your life as easy as cheese.<br>
                Compare insurance to see which plan fits your needs.</p>
                <a href="{{url('calculator')}}" class="fa fa-calculator compare-btn"> Compare Now</a>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </section>
<section class="testimonials-section">
    <div class="training-title container text-center">
        <h2 class="">What our customers say</h2>
    </div>
    <div class="slider-arrows">
        <div class="container">
            <button class="fa fa-angle-left slider-btn" id="slick-previous"></button>
            <button class="fa fa-angle-right slider-btn" id="slick-next"></button>
        </div>
    </div>
    <div class="items container">
        @foreach($testimonials as $testimonial)
        <div class="card testimonials-card">
            <div class="card-body testimonials-card-body">
                <div class="image-box"><img class="imgtestimonial"
                    src="{{$testimonial->image}}"
                    title="{{$testimonial->name}}" alt="testimonial image"></div>
                    <div class="user-content">
                        {{text_limit($testimonial->comment,180)}}
                    </div>
                    <div class="mx-auto text-center">

                        <div class="star-rating">
                            <ul class="list-inline">
                                @for($i = 1; $i <= $testimonial->rating; $i++)
                                    <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                    @endfor
                                </ul>
                            </div>
                            <div class="client-details text-capitalize">
                                <h5 class="client-name">{{$testimonial->name}}</h5> <span
                                class="client-company-name">{{$testimonial->designation}}</span>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach
            </div>
        </section>
        <section class="container associations">
            <div class="association-title">
                <h2 class="text-capitalize text-center">{{__("Our Association")}}</h2>
            </div>
            <div class="association-row-wrapper">
                <div class="association-slider life-slider owl-carousel">
                    @foreach($associations->where('association_type',1) as $association)
                    <div class="association-item">
                        <img class="association-img" src="{{asset($association->image)}}"
                        title="{{$association->name ? $association->name : 'Our Association'}}"
                        alt="association image">
                    </div>
                    @endforeach
                    @foreach($associations->where('association_type',2) as $association)
                    <div class="association-item">
                        <img class="association-img" src="{{asset($association->image)}}"
                        title="{{$association->name ? $association->name : 'Our Association'}}"
                        alt="association image">
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <div class="video-pop-block popup-parent-wrapper">
            <div class='video-pop-wrapper popup-form-wrapper'>
            </div>
        </div>
        @if($walkthrough!= 0)
        <div class='walkthrough-wrapper'>
            <div class="walkthrough-items">
                <div id="first-step" class="walkthrough-item active">
                    <div class="walkthrough-title">
                        <h2>1.Contact</h2>
                    </div>
                    <div class="walkthrough-content">
                        <p>if you find difficulties in calculation or don’t have time to compare. Don’t worry we got your back.</p>
                        <div class="next-button">
                            <button type="button" data-target="second-step" data-position="stepTwo">Next</button>
                        </div>
                    </div>
                </div>
                <div id="second-step" class="walkthrough-item"  data-position="stepThree" data-left="300" data-top="-80">
                    <div class="walkthrough-title">
                        <h2>2.Contact</h2>
                    </div>
                    <div class="walkthrough-content">
                        <p>if you find difficulties in calculation or don’t have time to compare. Don’t worry we got your back.</p>
                        <div class="next-button">
                            <button type="button" data-target="third-step">Next</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @endif
        <div class="more-product-popup popup-parent-wrapper">
            <div class="more-product-form-wrapper popup-wrapper popup-form-wrapper">
                <div class="more-product-heading">
                    <h2>More Product</h2>
                    <div class="product-cross-btn"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="cross"></div>
                </div>
                <div class="product-wrapper">
                    <div class="more-product-tab-list-wrapper">
                        <ul>
                            <li><a href="#" data-target="life-product" class="active product-list-item">Life Insurance</a></li>
                            <li><a href="#" data-target="nonlife-product" class="product-list-item">Non Life Insurance</a></li>
                        </ul>
                    </div>
                    <div class="insurance-product-items">
                        <div id="life-product" class="insurance-product-items-wrapper active">
                          <div class="insurance-product-wrapper">
                             <div id="insurance-policy" class="list-of-insurance-policy">
                                <div class="policy-item">
                                    <div class="policy-content">
                                        <a href="{{url('calculator')}}?category=endowment">
                                            <div class="policy-img"><img src="{{asset('frontend/img/policy/investment.svg')}}"
                                             alt="endowment plan"></div>
                                             <p class="ins-policy-title"><b>Endownment</b> Plan</p>

                                         </a>
                                     </div>
                                 </div>
                                 <div class="policy-item">
                                    <div class="policy-content">
                                        <a href="{{url('calculator')}}?category=money-back">
                                            <div class="policy-img"><img src="{{asset('frontend/img/policy/money-back.svg')}}"
                                             alt="money plan"></div>
                                             <p class="ins-policy-title"><b>Money Back</b> Plan</p>

                                         </a>
                                     </div>
                                 </div>
                                 <div class="policy-item">
                                    <div class="policy-content">
                                        <a href="{{url('calculator')}}?category=whole-life">
                                            <div class="policy-img"><img src="{{asset('frontend/img/policy/heartbeat.svg')}}"
                                             alt="wholelife plan"></div>
                                             <p class="ins-policy-title"><b>Whole Life</b> Plan</p>
                                         </a>
                                     </div>
                                 </div>
                                 <div class="policy-item">
                                    <div class="policy-content">
                                        <a href="{{url('calculator')}}?category=couple">
                                            <div class="policy-img"><img src="{{asset('frontend/img/policy/in-love.svg')}}"
                                             alt="couple plan"></div>
                                             <p class="ins-policy-title"><b>Couple</b> Plan</p>
                                         </a>
                                     </div>
                                 </div>
                                 <div class="policy-item">
                                    <div class="policy-content">
                                        <a href="{{url('calculator')}}?category=children">
                                            <div class="policy-img"><img src="{{asset('frontend/img/policy/child.svg')}}"
                                             alt="child plan"></div>
                                             <p class="ins-policy-title"><b>Child</b> Plan</p>
                                         </a>
                                     </div>
                                 </div>
                                 <div class="policy-item">
                                    <div class="policy-content">
                                        <a href="{{url('calculator')}}?category=term">
                                            <div class="policy-img"><img src="{{asset('frontend/img/policy/shield.svg')}}"
                                             alt="term life"></div>
                                             <p class="ins-policy-title"><b>Term</b> Life</p>
                                         </a>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div id="nonlife-product" class="insurance-product-items-wrapper">
                        <div class="list-of-insurance-policy">
                            <div class="policy-item">
                                <div class="policy-content">
                                    <a class="" href="{{ route('frontend.nonlife.insurance') }}">
                                        <div class="policy-img"><img src="{{asset('frontend/img/policy/motorcycle.svg')}}"
                                         alt="two wheeler"></div>
                                         <p class="ins-policy-title"><b>2 Wheeler</b> Plan</p>
                                     </a>
                                 </div>
                             </div>
                             <div class="policy-item">
                                <div class="policy-content">
                                    <a class="" href="{{ route('private.car.nonlifeinsurance') }}">
                                        <div class="policy-img"><img src="{{asset('frontend/img/policy/car.svg')}}"
                                         alt="four wheeler"></div>
                                         <p class="ins-policy-title"><b>4 Wheeler</b> Plan</p>
                                     </a>
                                 </div>
                             </div>

                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     @endsection

     @section('script')
     <script type="text/javascript" src="{{asset('frontend/js/slick.min.js')}}"></script>
      @include('frontend.components.home_page_script')
@endsection
