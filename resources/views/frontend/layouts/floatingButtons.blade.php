<ul class="side-fixed-nav">
    <li class="{{--pop-calculator-btn--}}"><a href="{{url('calculator')}}"><span class='side-fixed-img'><img src="{{asset('frontend/img/icon/calculator.svg')}}" alt="calculator icon"></span><span class="side-fixed-text">Calculator <i class="fa fa-caret-right"></i></span></a></li>
    <li class="oveflowHidden side-fixed-contact"><a href="#" data-toggle="modal" data-target="#expertTalk"> <span class="side-fixed-img"><img src="{{asset('frontend/img/icon/phone.svg')}}" alt="phone contact"></span><span class="side-fixed-text">Contact <i class="fa fa-caret-right"></i></span></a></li>
</ul>
<a href="#" class="ph-fixed-contact" data-toggle="modal" data-target="#expertTalk">
    <i class="fa fa-phone"></i>
</a>

{{--<!-- Modal -->--}}
{{--<div class="modal right fade" id="CalculateBox" tabindex="-1" role="dialog" aria-labelledby="CalculateBox">--}}
{{--    <div class="modal-dialog my-prem-calc" role="document">--}}
{{--        <div class="modal-content">--}}

{{--            <div class="modal-header">--}}
{{--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
{{--                    aria-hidden="true">&times;</span>--}}
{{--                </button>--}}
{{--                <h3 class="modal-title" id="talkToExpert">Premium Calculation</h3>--}}
{{--            </div>--}}

{{--            <div class="modal-body">--}}
{{--                @include('frontend.Partials.premiumCalculatorForm')--}}
{{--            </div>--}}

{{--        </div><!-- modal-content -->--}}
{{--    </div><!-- modal-dialog -->--}}
{{--</div><!-- modal -->--}}

<!-- Callback Modal -->

<div class="modal right fade popup-parent-wrapper" id="expertTalk" tabindex="-1" role="dialog" aria-labelledby="talkToExpert">
    <div class="modal-dialog" role="document">
        <div class="modal-content popup-form-wrapper">
            <div class="modal-header">
                <h3 class="modal-title callback" id="talkToExpert">Request Callback</h3>
                <button  type="button" class="close oveflowVisible" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="callback expert"></span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('message'))
                <div class="alert alert-success" role="alert">
                    {{ session('message') }}
                </div>
                @endif
                <form class="expert-talk" action="{{route('contact.message')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="phoneNumberInput text-grey">Phone Number</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" pattern="[0-9]{1}[0-9]{9}" name="phone" id="phone"
                        value="{{ old('phone') }}" placeholder="+977 |" required>
                        @error('phone')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="Name">Full Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="full_name"
                        value="{{ old('name') }}" placeholder="e.g. Radha Krisna" required>
                        @error('name')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email"
                        value="{{ old('email') }}" placeholder="e.g. radha.krisha@email.com" required>
                        @error('email')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                        <small class="form-text text-muted d-none">We'll never share your email with anyone
                        else.</small>
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control @error('message') is-invalid @enderror" name="message" id="message"
                        rows="1" placeholder="Type your message here.">{{ old('message') }}</textarea>
                        @error('message')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="g-recaptcha" data-sitekey="6Le3RNcdAAAAAHGeWj3T3ql-LKc8o_lL3tW1Zaxf"></div>
                   {{-- @if(Session::has('g-recaptcha-response'))
                    <p class="alert {{Session::get('alert-class', 'alert-info')}}">
                    {{Session::get('g-recaptcha-response')}}
                    </p>
                   @endif --}}
                    <div class="form-group text-center">
                        <button type="submit" class="btn-primary">Request Call back</button>
                    </div>
                </form>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->


<div class="corporate-plan-popup-form popup-parent-wrapper">
    <div class="corporate-plan-form-wrapper popup-wrapper popup-form-wrapper">
        <div class="corporate-plan-heading">
            <h1>Corporate Plan</h1>
            <div class="cross-btn"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="cross corporate plan"></div>
        </div>
        <form method="post" action="{{route('lead.corporate')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group full-block">
                <label>Full Name <span class="star">*</span></label>
                <input  type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{old('name')}}" required>
                @error('name')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror

            </div>
            <div class="form-group">
                <label>Phone No <span class="star">*</span></label>
                <input type="text" pattern="[0-9]{1}[0-9]{9}" name="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone No" value="{{old('phone')}}" required>
                @error('phone')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Email</label>
                <input  type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
                @error('email')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label>Name of Organization</label>
                <input type="text" name="organization" class="form-control" placeholder="Name Of Organization">
            </div>
            <div class="form-group">
                <label>Size of Employee</label>
                <input type="text" name="size_of_employee" class="form-control" placeholder="Size of Employee">
            </div>
            <div class="form-group full-block">
                <label>Message</label>
                <textarea rows="5" name="message"  placeholder="Message" class="form-control @error('message') is-invalid @enderror" rows=2>{{ old('message') }}</textarea>
                @error('message')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group full-block">
                <input type="hidden" name="policy_type" value="3">
                <button type="submit" class="btn-corporate">Send</button>
            </div>
        </form>

    </div>
</div>

<div class="nonlife-popup-form popup-parent-wrapper">
    <div class="nonlife-form-wrapper popup-wrapper popup-form-wrapper">
        <div class="nonlife-form-heading">
            <h1>Nonlife Plan</h1>
            <div class="nonlife-cross-btn"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="front nonlifeplan"></div>
        </div>
        <form method="post" action="{{ route('frontend.add.lead') }}">
            @csrf
            <div class="form-group full-block">
                <label>Full Name <span class="star">*</span></label>
                <input  type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{old('name')}}" required>
                @error('name')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror

            </div>
            <div class="form-group full-block">
               <label>Phone No <span class="star">*</span></label>
               <input type="text" pattern="[0-9]{1}[0-9]{9}" name="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Phone No" value="{{old('phone')}}" required>
               @error('phone')
               <div class="invalid-feedback d-block">{{ $message }}</div>
               @enderror
           </div>
           <div class="form-group full-block">
            <label>Email</label>
            <input  type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
            @error('email')
            <div class="invalid-feedback d-block">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group full-block">
            <label>Non Life Plan</label>
            <select class="form-control" name="policy_type">
                <option value='' selected>Select one category</option>
                <option value="2 Wheeler Plan">2 Wheeler Plan</option>
                <option value="4 Wheeler Plan">4 Wheeler Plan</option>
                <option value="Health Plan">Health Plan</option>
                <option value="Travel Plan">Travel Plan</option>
                <option value="Home Plan">Home Plan</option>

            </select>
        </div>

        <div class="form-group full-block">
            <input type="hidden" name="policy_cat" value="2">
            <button type='submit' class="btn-nonlife">Submit</button>
        </div>
    </form>
</div>
</div>



<div class="insurance-category-pop-block">
    <div class="insurance-category-pop-wrapper">
       <div class="insurance-category-heading">
        <h2 class="insurance-cat-title"></h2>
        <div class="pop-cross-btn"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="pop cross"></div>
    </div>
    <div class="category-pop-content">
        <p class='category-pop-p-content'></p>

    </div>
    <div class="pop-foot-btn">
        <a href="">Get Quote</a>
    </div>
</div>
</div>
<div class="all-insurance-category-description">
    <p class="endowment-description">An endowment policy is essentially a life insurance policy which, apart from covering the life of the insured, helps the policy holder save regularly over a specific period of time so that he/she is able to get a lump sum amount on the policy maturity in case he/she survuves the policy term.This maturity amount can be used to meet various financial needs such as funding one's retirement,children's education and/or marriage or buying a house.<br><br>A life insurance endowment policy pays the fill sum assured to the beneficiaries if the insurer dies during the policy term or to the policy holder on maturity of the policy if he/she survives the term.</p>
</div>
<div class='pop-calculator-wrapper popup-parent-wrapper'>
    <div class="pop-calculator-form popup-form-wrapper">
        <div class="pop-calculator-heading">
            <h2>Calculator</h2>
             <div class="pop-calculator-cross-btn"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="pop cross"></div>
        </div>
        <?php


         ?>
        @include('frontend.Partials.premiumCalculatorForm')
    </div>
</div>

<div class="cookie-pop">
        <div class="cookie-content-wrapper">
            <div class="cookie-heading">
                <img src="{{asset('frontend/img/cookie.png')}}" alt="cookie">Cookie
                <button id="cookie-dismiss" class='cookie-dismiss'>&times;</button>
            </div>
            <p>Our site uses cookies which are only used by us and the information they contain is not shared with anyone else. Please note that disabling cookies may affect some features of our website. <a href="#">Learn more</a></span></p>
            <div class="acceptclose">
                <button id="cookieBoxok" class="accept-btn"  data-cookie="{{url('/')}}">I love Cookie</button>
            </div>
        </div>
</div>
  <div class="modal fade p-3 verifyCustomer popup-parent-wrapper" id="verifyCustomer" tabindex="-1" role="dialog"
                 aria-labelledby="numberVerify">
                <div class="modal-dialog modal-dialog-center popup-form-wrapper" role="document">
                    <div class="modal-content information">
                        <h5 class="m-0">Your Information   <button type="button" class="close oveflowVisible" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"><img src="{{asset('frontend/img/pop-cross.svg')}}" alt="pop cross"></span>
                            </button></h5><span class="mobile-text">Enter the your information to continue.</span>
                        <form id="userInfos" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Enter your full name"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input type="number" class="form-control" name="phone"
                                       placeholder="Enter your phone number" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" name="email" aria-describedby="emailHelp"
                                       placeholder="Enter your email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                                    anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="province">Province</label>
                                <select class="form-control" name="province">
                                    <option value="" selected>Select Province</option>
                                    @foreach($provinces as $province)
                                        <option value="{{$province->id}}">{{$province->province_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" id="Custlogin"
                                        class="btn btn-primary c-btn-style c-primary-btn mb-3">Submit & View result
                                </button>
                                <br>
                                <button class="btnmodal"  type="button" id="makeModalClose"
                                        class="btn btn-outline-secondary c-btn-style c-ghost-btn oveflowVisible"
                                        data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
