<section class="footer">
    <img class="top-img" src="{{ asset('frontend/img/footer2.png') }}" alt="footer line">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 footer-logo-part">
                <img class="base-img" src="{{ asset('frontend/img/logo.png') }}" alt="ebeema logo" title="Ebeema">
                {!! $user_settings['footer-text'] ?? "" !!}
{{--                <h1>  <p class="base-logo-text">--}}
{{--                    {{__("A team of determined, passionate, and crazy entrepreneurs, developers, and financial engineers set out on one common mission - To make insurance services accessible, simple, and seamless.")}}--}}
{{--                </p>  </h1>--}}
            </div>
            <div class="col-sm-3 footer-titile footer-second-item">
               <p>{{__('Contact Info')}}</p>
                <ul class="footer-lists foot-contact-info">
                    <li><img src="{{asset('frontend/img/footer/location.png')}}" alt="footer location"><span>{{ $general_settings['iframe-location'] ?? 'Gairidhara, Kathmandu' }}</span></li>
                    <li><img src="{{asset('frontend/img/footer/contact.png')}}"  alt="footer contact"><a href="tel:{{ $general_settings['iframe-call'] ?? '' }}"><span>{{ $general_settings['iframe-call'] ?? '01-4429240 / +977-9802322097' }}</span></a></li>
                    <li><img src="{{asset('frontend/img/footer/message.png')}}"  alt="footer message"><a href="mailto:{{ $general_settings['iframe-mail'] ?? "hiATebeemaDOTcom" }}" onclick="this.href=this.href.replace(/AT/,'&#64;').replace(/DOT/,'&#46;')"><span>Click to Email Us</span></a></li>
                    <li><img src="{{asset('frontend/img/footer/time-circle.png')}}"  alt="footer circle"><span>{{ $general_settings['footer-open-timing'] ?? "Sun - Fri 09:00 AM - 06:00 PM" }}</span></li>
                </ul>
            </div>
             <div class="col-sm-3 footer-titile footer-second-item">
                <p>{{__('Branch Info')}}</p> 
                <ul class="footer-lists foot-contact-info">
                    <li><img src="{{asset('frontend/img/footer/location.png')}}" alt="footer location"><span>{{ $general_settings['footer-regional-office'] ?? "Regional Office - Itahari Dharan Line, Itahari" }}</span></li>
                </ul>
            </div>
            <div class="col-sm-3 footer-titile footer-second-item">
               <p>{{__("Explore")}}</p>
                <ul class="footer-lists">
                    <li class="fa fa-angle-double-right"><a href="{{ route('frontend.about') }}"> {{__("About")}}</a></li>
                    <br>
                    {{-- <li class="fa fa-angle-double-right" ><a href="#"> {{__("Product")}}</a></li>
                    <br> --}}
                    <li class="fa fa-angle-double-right"><a href="{{route('home.calculator')}}"> {{__("Calculator")}}</a></li>
                    <br>
                    {{--<li class="fa fa-angle-double-right"><a class="nonlife-foot" href="{{ route('frontend.nonlife.insurance') }}"> {{__("Non-Life Insurance")}}</a></li>
                    <br>--}}

                    <li class="fa fa-angle-double-right"><a href="{{ route('contact') }}"> {{__("Contact Us")}}</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <hr class="footer-line">
    </div>
    <div class="container">
         <div class="row copyright-text">
        <div class="col-sm-6 footer-item">
            <p>{{__("Copyright")}} &copy;
                {{date("Y")}}
                , {{__('eBeema Pvt. Ltd.')}} | Powered by <a href="#" class="text-primary">Infocom Solutions Pvt. Ltd.</a>
            </p>
        </div>
        <div class="col-sm-3 footer-item">
            <a href="{{url('term-condition')}}">{{__("Terms & Conditions")}} </a> | <a href="{{url('privacy-policy')}}"> {{__("Privacy Policy")}}</a>
        </div>
        <div class="col-sm-3 foot-social-icon">
            @foreach ($links as $link)
            <a class="footer-media-icon" href="{{ $link->link }}" target="_blank" rel="noopener" >
                <i class="{{ $link->icon }}" aria-hidden="true"></i>
            </a>
            @endforeach
            {{-- <a class="footer-media-icon" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            <a class="footer-media-icon" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a class="footer-media-icon" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a class="footer-media-icon" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> --}}
        </div>
    </div>
    </div>
</div>
</section>

@section('script')
@endsection
