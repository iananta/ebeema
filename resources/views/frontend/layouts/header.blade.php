 {{--<div class="top-media-bar" id="topMedia">
    <div class="container-fluid">
        <div class="top-bar-wrapper">
            <ul class="media-icons list-inline">
                @php $countSocialIcon=count($links) - 1;@endphp
                @foreach ($links as $key=>$link)
                <li class="list-inline-item social-top-icn {{($key===$countSocialIcon) ? 'flexSocial' : ''}}">
                    <a class="top-media-icon" href="{{ $link->link }}" target="_blank">
                        <span class="{{ $link->icon }}"></span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>--}}
<!-- Fixed navbar -->
<nav class="navbar navbar-default main-navigation">
    <!-- Top Bar -->
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/" title="Ebeema"><img class="header-logo"
                                                                 src="{{asset('frontend/img/logo.png')}}" alt="ebeema header logo" width="150"
                                                                 height="60"></a>
            <button id="menu-bar" type="button" class="navbar-toggle collapsed mainMenu oveflowHidden" data-toggle="collapse"
                    data-target="#navbarMenu" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbarMenu" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right navbar-menus">
                 <li class="dropdown mob-mnu plans-cat">
                    <a href="#" class="fa fa-heart-o menu-icn" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">{{__('Insurance Plans')}} <span class=" insurance-drop "><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="chervon insurance image"></span></a>
                     <ul class="parent-dropdown">
                            <div class="dropdown-nav-item">
                                <li class="active">
                                    <a href="#"><span class="nav-heart"><img src="{{asset('frontend/img/profits.svg')}}" alt="profit image svg"></span>High Return Plans <span class="drop-chevron"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                                    <ul class="final-dropdown">
                                        <li><a href="{{url('calculator')}}?category=endowment">Investment Plan</a></li>
                                        <li><a href="{{url('calculator')}}?category=money-back">Money Back Plan</a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><span class="nav-heart"><img src="{{asset('frontend/img/wallet.svg')}}" alt="frontend wallet image"></span>Saving Plans <span class="drop-chevron"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                                    <ul class="final-dropdown">
                                        <li><a href="{{url('calculator')}}?category=whole-life">Whole Life Plan</a></li>
                                        <li><a href="{{url('calculator')}}?category=couple">Couple Plan</a></li>
                                        {{--<li><a href="#">Single Premium Plan</a></li>
                                         <li><a href="{{url('calculator')}}?category=children">Child Plan</a></li>--}}

                                    </ul></li>
                                <li><a href="#"><span class="nav-heart"><img src="{{asset('frontend/img/shield.svg')}}" alt="frontend term insurance plans"></span></span>Term Insurance Plans <span class="drop-chevron"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                                     <ul class="final-dropdown">
                                        <li><a href="{{url('calculator')}}?category=term">Term Plan</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><span class="nav-heart"><img src="{{asset('frontend/img/safe.svg')}}" alt="general insurance safe plans"></span>General Insurance Plans <span class="drop-chevron"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                                     <ul class="final-dropdown">
                                        <li><a href="{{url('calculator')}}?category=retirement-pension">Retirement/Pension Plan</a></li>
                                    </ul>
                                </li>
                            </div>
                            <div class="dropdown-nav-item"></div>
                        </ul>
                </li>
                <li class="normal-width"><a href="{{url('about')}}" class="fa fa-heart-o menu-icn">About Us</a></li>
                <li class="normal-width"><a href="{{url('contact')}}" class="fa fa-heart-o menu-icn">Contact</a></li>
                <li class="normal-width"><a href="{{url('all/blogs')}}" class="fa fa-heart-o menu-icn">Blog</a></li>
                @if(Auth::user())
                    <li>
                        <a class="fa fa-heart-o menu-icn" href="/admin"> Dashboard</a>
                        <a class="menu-icn" href=" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" title="Log out"><span class="fa fa-sign-out logout-btn"></span></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    </li>
                @else
                    <li class="auth-wrapper">
                        <a class="fa fa-heart-o menu-icn log-nav-btn" href="/admin">Login</a>
                        <a class="header-login-btn no-display-top" href="/register">Register</a>
                    </li>
                @endif
            </ul>
            <ul class="mobile-menu-navbar">
                <li class="mobile-menu-item"><a href='#' class="parent-menu-text"><span class="drop-text">Insurance Plan</span><span class="mobile-drop-icon"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="chervon menu"></span></a>
                    <ul class="mobile-child-drop">
                        <li>
                            <a href="#"><span class="plan-icon"><img src="{{asset('frontend/img/profits.svg')}}" alt="high return profit plans"></span><span class="drop-text">High Return Plans</span><span class="mobile-drop-icon"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="high return plan mobile"></span></a>
                            <ul class="mobile-child-drop">
                                <li><a href="{{url('calculator')}}?category=endowment">Investment Plan</a></li>
                               <li><a href="{{url('calculator')}}?category=money-back">Money Back Plan</a></li>
                            </ul>

                        </li>
                        <li><a href="#"><span class="plan-icon"><img src="{{asset('frontend/img/wallet.svg')}}" alt="saving wallet plans"></span><span class="drop-text">Saving Plans</span> <span class="mobile-drop-icon"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                            <ul class="mobile-child-drop">
                              <li><a href="{{url('calculator')}}?category=whole-life">Whole Life Plan</a></li>
                              <li><a href="{{url('calculator')}}?category=couple">Couple Plan</a></li>
                            {{--  <li><a href="#">Single Premium Plan</a></li> --}}
                               <li><a href="{{url('calculator')}}?category=children">Child Plan</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span class="plan-icon"><img src="{{asset('frontend/img/shield.svg')}}" alt="term insurance plans"></span><span class="drop-text">Term Inusrance Plans</span> <span class="mobile-drop-icon"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                            <ul class="mobile-child-drop">
                                <li><a href="{{url('calculator')}}?category=term">Term Plan</a></li>
                            </ul>
                        </li>
                        <li><a href="#"><span class="plan-icon"><img src="{{asset('frontend/img/safe.svg')}}" alt="general insurance plans"></span><span class="drop-text">General Inusrance Plans</span> <span class="mobile-drop-icon"><img src="{{asset('frontend/img/chervon-menu.svg')}}" alt="menu frontend image"></span></a>
                            <ul class="mobile-child-drop">
                                <li><a href="{{url('calculator')}}?category=retirement-pension">Retirement/Pension Plan</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="mobile-menu-item"><a href='{{url("about")}}'>About Us</a></li>
                <li class="mobile-menu-item"><a href="{{url('contact')}}">Contact</a></li>
                <li class="mobile-menu-item"><a href="{{url('all/blogs')}}">Blog</a></li>
                @if(Auth::user())
                    <li class="mobile-menu-item">
                        <a class="mobile-register" href="/admin"> Dashboard</a>
                        <a class="mobile-login" href=" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" title="Log out">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    </li>
                @else
                    <li class="mobile-auth-wrapper mobile-menu-item">
                        <a href="/register" class="mobile-register">Register</a>
                        <a href="/admin" class="mobile-login"> Login</a>
                    </li>
                @endif
            </ul>
        </div>
        <!--nav-collapse -->
    </div>
</nav>
