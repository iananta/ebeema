@extends('frontend.layouts.app')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/nonlife.css')}}">
@endsection
@php
    $amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
    $amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
    $amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
@endphp
@section('content')
    <section>
        @if (isset($requested_data))
            <div class="box box-success">
            <!--     <h3>Calculated Result - {{ Session::get('calculationFor') ? Session::get('calculationFor') : 'bike' }}
                    calculation</h3> -->
                <section class="compare-screen">
                    <div class="top-navigation">
                        <a href="/">Home</a> / <a href="">Non-Life Calculated</a> / <a class="active"
                                                                                       href="#">{{ Session::get('calculationFor') ? Session::get('calculationFor') : 'bike' }}
                            calculation</a>
                    </div>
                </section>
                <section class="compare-calc-lists">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-3">
                                <div id="policyfilter" enctype="multipart/form-data">

                                    <input type="hidden" name="is_ajax" value="1">
                                    <div class="left-filter-cmp">
                                        <div class="compare-list-cat">
                                           <p>Non Life Calculator</p> 
                                        </div>
                                        <div class="comare-lst">
                                            <p> For</p> 
                                            <div class="date_selection">
                                                {{ Session::get('calculationFor') ? Session::get('calculationFor') : 'bike' }}
                                            </div>
                                        </div>
                                        <div class="comare-lst">
                                        <p> Type of cover</p> 
                                            <div class="date_selection">
                                                @if($requested_data['TYPECOVER'] == "CM")
                                                    Comprehensive(First Party)
                                                @elseif($requested_data['TYPECOVER'] == "TP")
                                                    Third Party
                                                @endif
                                            </div>
                                        </div>
                                        <div class="comare-lst">
                                          <p> Year Manufacture</p> 
                                            <div class="date_selection">
                                                <input type="number" name="age"
                                                       value="{{ $requested_data['YEARMANUFACTURE'] }}" readonly
                                                       class="cmp-datepkr">
                                            </div>
                                        </div>
                                        <input type="hidden" value="{{$requested_data['PRIVATE_USE'] === 1 ? 1 : 0}}"
                                               name="PRIVATE_USE "/>
                                     
                                        <div class="comare-lst">
                                            <p>Cubic Capacity</p> 
                                            <div class="date_selection">
                                                <input type="number"  name="age"
                                                       value="{{ $requested_data['CCHP'] }}"
                                                       readonly class="cmp-datepkr">
                                            </div>
                                        </div>

                                        <div class="comare-lst">
                                            <p>EXPUTILITIESAMT </p> 
                                            <input type="number" id="" name="DRIVERPREMIUM_C"
                                                   value="{{ $requested_data['EXPUTILITIESAMT'] }}" readonly
                                                   class="cmp-datepkr calc-chg">
                                        </div>
                                        <div class="comare-lst">
                                            <input type="hidden" value="1" name="INCLUDE_TOWING"/>
                                            @if ($requested_data['INCLUDE_TOWING'] === 1)
                                                <input type="hidden" value="1" name="INCLUDE_TOWING"/>
                                                <div>
                                                    <p><span class="span">Include Towing </span> <i
                                                            class="fa fa-check" aria-hidden="true"></i>
                                                    </p>
                                                </div>
                                            @else
                                                <p> Include Towing <i class="fa fa-times" aria-hidden="true"></i></p>
                                            @endif
                                            @if ($requested_data['ISGOVERNMENT'] === 1)
                                                <input type="hidden" value="1" name="ISGOVERNMENT"/>
                                                <div>
                                                    <p><span class="span">ISGOVERNMENT </span> <i
                                                            class="fa fa-check" aria-hidden="true"></i></p>
                                                </div>
                                            @else
                                                <p>ISGOVERNMENT <i class="fa fa-times" aria-hidden="true"></i></p>
                                            @endif
                                            @if (isset($requested_data['PRIVATE_USE']) && $requested_data['PRIVATE_USE'] === 1)
                                                <input type="hidden" value="1" name="ISGOVERNMENT"/>
                                                <div>
                                                    <p><span class="span">PRIVATE USE </span> <i class="fa fa-check"
                                                                                                 aria-hidden="true"></i>
                                                    </p>
                                                </div>
                                            @else
                                                <p>PRIVATE USE <i class="fa fa-times" aria-hidden="true"></i></p>
                                            @endif
                                        </div>
                                        {{-- @if ($requested_data['pool_premium'] === 1)
                                        <input type="hidden" value="1" name="pool_premium" />
                                        <div>
                                         Pool Premium <span class="span"></span>
                                     </div>
                                     @else
                                     Pool Premium
                                     @endif --}}
                                    </div>
                                </div>

                                <input type="hidden" name="category" value="">
                            </div>

                            <div id="compare-result">
                                @include('frontend.nonlifeinsurance.bike.nonlifecompareresult')
                            </div>
                        </div>
                    </div>
            </div>
        @endif
    </section>
@endsection
@section('script')

    @if(Auth::check())
        <script>
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Please select plan and login to proceed',
                // html: err ?? 'Something went wrong',
                showConfirmButton: false,
                showCloseButton: true,
                timer: 4500,
                timerProgressBar: true,
            });
        </script>
    @endif
    <script>
        //   $('.select-plan').on('click',function(e){
        //     e.preventDefault()
        //     localStorage.setItem('state','');
        //     $('.select-plan-form').submit()
        // })
        localStorage.setItem('state','');
        $("#show_hide_password a").on('click', function (event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("fa-eye-slash");
                $('#show_hide_password i').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("fa-eye-slash");
                $('#show_hide_password i').addClass("fa-eye");
            }
        });
            window.onload = function() {
        document.getElementById('AddSelectPlan').style.display = 'block';
        };
        /** toggle password visibility */
        $('#pasword + .glyphicon').on('click', function () {
            $(this).toggleClass('glyphicon-eye-close').toggleClass(
                'glyphicon-eye-open'); // toggle our classes for the eye icon
            $('#pasword').togglePassword(); // activate the hideShowPassword plugin
        });
         $('.nonlife-login-close').on('click',function(){
                    $('.nonlife-login-wrapper').hide()
            })

            // //Form submit of register
            $(document).ready(function(){
            $('#SubmitRegister').submit(function(e){ 
                e.preventDefault();
                $.ajaxSetup({
                    type: "post",
                    url: "{{ route('user.register') }}",
                    data: $('#SubmitRegister').serialize(),
                    async: true,
                    dataType: 'json',
                    beforeSend: function () {
                    },
                    success: function(){
                        $('#SubmitRegister').append("<div class='alert alert-success'><i class='fa fa-check'></i> Registerd successfully added. Please Fill the Login form</div>");
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            html: response.message ?? "Registered Added Successfully",
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                    }
                });
                $.post()
                .done(function(response) {
                    console.log(response);
                })
                .fail(function() {
                    Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: 'Invalid Phone number and Email alerady use!',
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                })
            });
        });
            //end of register


            //form login register
            $(document).ready(function(){
            $('#Registerlogin').submit(function(e){
                event.preventDefault();
                $.ajaxSetup({
                    type: "post",
                    url: "{{ route('auth.login') }}",
                    data: $('#Registerlogin').serialize(),
                    async: true,
                    dataType: 'json',
                    beforeSend: function () {
                            $(".loader").modal('show');
                        },
                    success: function(){
                       // $('#Registerlogin').append("<div class='alert alert-success'><i class='fa fa-check'></i> Registerd successfully added. Please Fill the Login form</div>");
                        // alert(json);
                        window.location.assign("{{ route('frontend.nonlife.insurance.bike.view') }}"),
                        // window.location.href = "/Nonlifeinsurance/calculator/motors";
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Congratulations!',
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                    },complete:function(){
                            $("#loader").modal('hide');
                    }
                });
                $.post()
                .done(function(response) {
                    console.log(response);
                })
                .fail(function() {
                    Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: 'PhoneNumber and password Not matching!',
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: true,
                        });
                })
            });
        });

        

            //end of login register
          
          
          
    </script>
@endsection


