@extends('frontend.layouts.app')
@section('seo_contents')
    <title>{{$seoData->where('page_name','nonlife-calculator-page')->first()->meta_title ?? ''}}</title>
    <meta name="title" content="{{$seoData->where('page_name','nonlife-calculator-page')->first()->meta_title ?? ''}}">
    <meta name="description" content="{{$seoData->where('page_name','nonlife-calculator-page')->first()->meta_description ?? ''}}">
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/nonlife.css')}}">
@endsection
@php
    $nonlifeId=$classId;
@endphp
@section('content')
  <section class="nonlife-calculation-block">
      <div class="container">
           @include('layouts.backend.alert')
          <div class="nonlife-category-listing">
              <ul>
                  <li class="{{$nonlifeId == 21 ? 'active' : ''}}">
                    <a href="{{route('frontend.nonlife.insurance')}}">
                        <span class="nonlife-cat-icon "><img src="{{asset('frontend/img/nonlife/motorcycle.png')}}" alt="frontend motorcycle"></span>
                        <span class="nonlife-name">Two Wheeler</span>
                    </a>
                  </li>
                   <li class="{{$nonlifeId == 23 ? 'active' : ''}}">
                       <a href="{{ route('private.car.nonlifeinsurance')}}">
                             <span class="nonlife-cat-icon"><img src="{{asset('frontend/img/nonlife/car.png')}}" alt="frontend car"></span>
                            <span class="nonlife-name">Private Vehicle</span>
                        </a>
                  </li>
                   <li class="{{$nonlifeId == 22 ? 'active' : ''}}" >
                       <a href="{{ route('commerical.car.calculator.nonlifeinsurance') }}">
                            <span class="nonlife-cat-icon"><img src="{{asset('frontend/img/nonlife/bus.png')}}" alt="frontend bus"></span>
                            <span class="nonlife-name">Commercial Vehicle</span>
                       </a>
                  </li>
                 
              </ul>
          </div>
          <div class="nonlife-insurance-type">
              <ul>
                  <li data-target="comprehensive-form"><a href="#"><span class="nonlife-insurancey-type-name">Comprehensive</span></a></li>
                  <li data-target="third-party-form"><a href="#"><span class="nonlife-insurancey-type-name">Third Party</span></a></li>
              </ul>
          </div>
          <div class="nonlife-calculation-form-wrapper">
              <div id="comprehensive-form" class=" nonlife-tab-content">
                  @include('frontend.nonlifeinsurance.comprehensive')
              </div>
              <div id="third-party-form" class=" nonlife-tab-content">
                  @include('frontend.nonlifeinsurance.thirdparty')
              </div>
          </div>
      </div>
  </section>
@endsection
@section('script')
  @include('frontend.components.nonlife_script')
@endsection
