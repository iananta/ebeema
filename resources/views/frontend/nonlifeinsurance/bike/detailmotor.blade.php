@extends('frontend.layouts.app')
@section('title')
    <p class="h4 align-center mb-0">Add Vechile Detail</p>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/nonlife.css') }}">
    <!-- Nepali Datepicker CSS -->
  
@endsection
<?php
    $user=\Auth::user();
?>
@section('content')
<div class="non-life-details-wrapper">
    <div class="nonlife-progress-bar-wrapper">
        <ul id="non-life-progressbar">
            <li class="active current"><span>Vehicle Details</span></li>
            <li><span>KYC Form</span></li>
            <li><span>Review</span></li>
            <li><span>Confirm</span></li>
        </ul>
    </div>
    <div class="non-life-form-wrapper">
        <div class="container">
            <div class="fieldset-wrapper">
                 <fieldset id="vehicle-details" class="step-fieldset">
                     @include('frontend.nonlifeinsurance.vehicledetailsform')
                 </fieldset>
                 <fieldset id="kyc-form" class="step-fieldset">
                     @include('frontend.nonlifeinsurance.kycform')
                 </fieldset>
                 <fieldset class="review" class="step-fieldset">
                 @include('frontend.nonlifeinsurance.review',['kyc' =>auth()->user()->kyc,'paymentInfo'=>$paymentInfo])
                    <div class="step-action-button">
                            <div class="container">
                                <button type="button" class="nonlife-previous">Previous</button>
                                <button type="submit" class="nonlife-next othernonlife-next"
                                   id="kycreview">Continue</button>
                            </div>
                    </div>    
                 </fieldset>
                 <fieldset class="invoice step-fieldset">
                    @if(isset($data))
                    @include('frontend.nonlifeinsurance.invoice')
                    @endif
                        <div class="step-action-button">
                            <div class="container-invoice"> 
                                <button type="button" class="nonlife-previous">Previous</button>
                                <button type="submit" class="nonlife-next othernonlife-next"
                                   >Finished</button>
                            </div>
                        </div>
                 </fieldset>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
  <script type="text/javascript">
            @if(!Auth::check())
                new swal({
                    icon: "success",
                    text: "PLease Select Plan For Login",
                });
            @endif
            @if(Session::has('submit'));
                new swal({
                    icon: "success",
                    text: "Please fill the Vechile Detail form",
                }); 
            @endif
        let insuredId=[1,3,4,12]
        $("#astricshow").text($("#kycchange option:selected").val());
        $("#INSUREDTYPEID").on('change',function(){
            let _this=$(this)
            let val=_this.val()
            if(insuredId.includes(parseInt(val))){
                $('.astricshow').html('*')
            }else
            $('.astricshow').html('')
        });      
        $('#INSUREDTYPEID').on('change', function () {
        if (this.value === '1'){
            $(".cpmreg").show();
        } else {
            $(".cpmreg").hide();
        }
        });    
        $('#MNUId').on('change', function () {
            if(!$(this).val()){
                $('.vdc').show();
                $('.vdc').attr('required','required')
            } else{
                $('.vdc').hide();
                $('.vdc').removeAttr('required')
            }
        })    
        $('#VDCId').on('change', function () {
            if(!$(this).val()){
                $('.mnu').show();
                $('.mnu').attr('required','required')
            } else{
                $('.mnu').hide();
                $('.mnu').removeAttr('required')
            }
        })    
    </script>
    @include('frontend.components.nonlife_details_scripts')
@endsection
