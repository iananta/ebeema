@php
    $amountFormatter = new NumberFormatter('en_IN', NumberFormatter::CURRENCY);
    $amountFormatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
    $amountFormatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 0);
@endphp

@section('title')
    <p class="h4 align-center mb-0">Non Life Calculation Policy Result</p>
@endsection
@if (isset($premiums))

    <div class="col-sm-9">
        <div class="compare-search-sort">
            <p class="left-sort">{{ count($premiums) }} : <a class="" href=" #"> Plans match your
                    search</a>
            </p>
            <div class="navbar-nav right-sort">
            </div>
        </div>

        <div class="compare-plans">
            <table class="table nonlife-compare-table">
                <thead class="thead-dark">
                <tr class="heading-dark">
                    <th scope="col">Insurance</th>
                    <th scope="col">Calculation Motor</th>
                    <th scope="col">Premimum</th>
                    <th scope="col">ACTION</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($premiums as $premium)
                    @if($premium['data'])
                        <tr class="content-compare nonlife-compare-row">
                            <td  class="compare-parts line-rht-cmp comapny" scope="row">
                                <p>{{ $premium['company_name'] }} </p>
                                <img src="/images/company/{{ $premium['company_image'] }}" alt="IME"/>
                                <p>{{ $premium['company_name'] }}</p>
                            </td>
                            <td class="compare-parts line-rht-cmp company" scope="row">
                                <a data-toggle="modal" data-target="#view-plan-{{ $loop->iteration }}" data-id="{{ $loop->iteration }}" href="#" class="view-plan oveflowHidden ">Risk Lists</a>
                                <div id="view-plan-{{ $loop->iteration }}" class="view-plan-block popup-parent-wrapper modal fade">
                                    <div class="view-plan-wrapper popup-form-wrapper">
                                        <div class="view-plan-heading">
                                            <h1>Risk Lists</h1>
                                            <div class="close-btn" data-dismiss="modal">&times;</div>
                                        </div>
                                        <div class="view-plan-content">
                                            <table class="table table-bordered mb-0">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Particulars</th>
                                                    <th scope="col">Rate</th>
                                                    <th scope="col">Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody class="scroll-view">
                                                @foreach ($premium['data']->data as $dta)
                                                    <tr>
                                                        <td>{{ $dta->ROWNUMBER }}</td>
                                                        <td>{{ $dta->RskDescription }}</td>
                                                        <td>{{ $dta->Rate }}</td>
                                                        <td>RS. {{ $amountFormatter->format($dta->Amount) }}</td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        RS.
                                                        {{ $amountFormatter->format($premium['data']->PremDetails->NETPREMIUM ?? '0.00') }}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                               
                                <p class="sum-title">Basic Premimum </p>
                                <p class="sum-title-val">
                                    RS. {{ $amountFormatter->format($premium['data']->data[0]->Amount) }}</p>
                                <p class="sum-title">THIRDPARTYPREMIUM </p>
                                <p class="sum-title-val">
                                    RS.
                                    {{ $amountFormatter->format($premium['data']->PremDetails->THIRDPARTYPREMIUM ?? '0.00') }}
                                </p>
                                <p class="details-title">Net Premium</p>
                                <p class="details-val2">
                                    Rs.
                                    {{ $amountFormatter->format($premium['data']->PremDetails->NETPREMIUM ?? '0.00') }}
                                </p>
                                <p class="sum-title">Stamp(Rs.) </p>
                                <p class="sum-title-val">
                                    Rs.{{ $amountFormatter->format($requested_data['stamp'] ?? 0) }}
                                </p>
                                <span class="gap-0"></span>
                            </td>
                            <td class="prem-box line-rht-cmp">
                                <p></p>
                                <br>
                                <br>
                                <p class="details-title2">Total Vatable Premium</p>
                                <p class="details-val2" id="return"><input type="hidden"
                                                                           value=" @php($totalVatable = $premium['data']->PremDetails->NETPREMIUM + $requested_data['stamp']){{ $totalVatable ?? '0.00' }}"
                                                                           name="bonus">
                                    Rs.@php($totalVatable = $premium['data']->PremDetails->NETPREMIUM + $requested_data['stamp']){{ $amountFormatter->format($totalVatable ?? '0.00') }}
                                </p>
                                @php($vaTtotal = $totalVatable * 0.13)
                                <span class="gap-0"></span>
                                <p class="sum-title">VAT(13%) </p>
                                <p class="sum-title-val">Rs.{{ $amountFormatter->format($vaTtotal) }} </p>
                                <span class="gap-0"></span>
                                <p class="details-title2">Total Net Premium</p>
                                <p class="details-val2">
                                    Rs.
                                    @php($totalNet = $totalVatable + round($vaTtotal, 2)){{ $amountFormatter->format($totalNet ?? '0.00') }}
                                </p>
                            </td>
                            <td class="nonlife-select-plan">
                                <br>
                                @if(!Auth::check())
                                    <button type="button" class="btn btn-primary select-plan" data-toggle="modal" data-target="#AddSelectPlan">
                                    Select plam
                                    </button>       
                                @endif
                                @if (Auth::check())
                                    @if (Auth::user()->role_id == 4)
                                    <form action="{{ route('fronted.nonlife.details') }}" method="GET" class="select-plan-form">
                                        @csrf
                                        <input type="hidden" name="company_code" value="{{$premium['company_code']}}">
                                        <input type="hidden" name="company_id" value="{{$premium['company_id']}}">
                                        <input type="hidden" name="premium" value="{{json_encode($premium['data']->PremDetails)}}">
                                            <button type="submit" class="btn btn-primary select-plan">Select Plan
                                            </button>
                                    </form>
                                @endif
                                {{-- SelectPlan Button trigger modal  --}}
                                <br>
                                <!-- Button trigger modal -->
                                @if(Auth::check())
                                        @if(Auth::user()->role_id == 1)  
                            <button type="button" class="btn btn-primary select-plan" data-toggle="modal" data-target="#exampleModalLong">
                                Select Plan
                            </button>
  
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content user">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">You Need to login as User</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            @endif
                                    @endif
                                    @if(!Auth::check())           
                                    <button type="button" class="btn btn-primary select-plan" data-toggle="modal"
                                            data-target="#AddSelectPlan">
                                        Select Plan
                                    </button>
                                @endif
                                <br>
                            </td>
                        </tr>
                         <tr class="spacer">
                         <td></td>
                        </tr>
                    @endif
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif
<!--  SelectPlan Button trigger modal -->
@if(!Auth::check())
  


<div class="modal fade nonlife-login-wrapper in"  id="AddSelectPlan" tabindex="-1" role="dialog" aria-labelledby="AddCustomerLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl select-cust-modal" role="document" >
        <div class="modal-content main login-frm">
            <div class="modal-header nonlife-login-header">
                <ul class="nav nav-pills">
                    <li class="active">
                        <a href="#login" data-toggle="tab">
                            LOGIN
                        </a>
                    </li>
                    <li><a href="#register" data-toggle="tab">Register</a>
                    </li>
                    <button type="button" class="close nonlife-login-close" data-dismiss="modal" aria-label="Close">
                        x
                    </button>
                </ul>
            </div>
            <!--FORM OF Login Select Plan -->
            <div class="col-sm">
                <div class="tab-content clearfix">
                    <div class="tab-pane active" id="login">
                        @include('layouts.backend.alert')
                        <form class="nonlife-login-form"  id="Registerlogin">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                   <label for="phone">Phone Number</label> 
                                    <input type="phone_number" class="form-control" id="phone_number"
                                           name="phone_number" placeholder="Enter Phone Number"
                                           value="{!! old('phone_number') !!}" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label> 
                                    <div class="input-group" id="show_hide_password">
                                        <input type="password" class="form-control" name="password" id="password"
                                               placeholder="Password">
                                        <div class="input-group-addon">
                                            <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group forget-rem ">
                                    <div class="nonlife-remember">
                                        <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox"
                                           name="remember">
                                        <label for="demo-form-checkbox" class="">
                                        Remember Me
                                    </label>
                                    </div>
                                    <a class="forgot-paswd" href="{{route('password.request')}}">Forgot Password?</a>
                                </div>
                            <div class="row login--btn">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" id="LoginNewRegister"class="btn btn-primary btn-block btn-flat">Sign In</button>
                                </div>
                            </div>
                                <!-- Image loader -->
                                <!-- <div class="loader"></div> -->
                    <!-- Image loader -->

                    <div class='response'></div>
                        </form>
                    </div>
                    <div class="tab-pane" id="register">
                        @include('layouts.backend.alert')
                        <form role="form" class="nonlife-login-form" id="SubmitRegister">
                            @csrf
                                <div class="form-group">
                                    <div class="form-line">
                                       <label for="phone_number">Phone Number *</label> 
                                        <input type="tel" name="phone_number" class="form-control" required
                                               value="{{old('phone_number')}}"
                                               placeholder="Enter phone number" pattern="[0-9]{1}[0-9]{9}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                       <label for="username"> Name *</label> 
                                        <input type="text" name="username" value="{{ old('username') }}"
                                               class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}"
                                               min="4" required
                                               placeholder="Enter your name"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="password">Password *</label>
                                        <div class="input-group" id="show_hide_password">
                                            <input class="form-control" type="password" id="pasword" name="password"
                                                   placeholder="Enter Password" required>
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="email">Email *</label>
                                        <input type="email" name="email" value="{{old('email')}}" class="form-control"
                                               id="email" placeholder="Enter email" required/>
                                    </div>
                                </div>
                                    <button type="submit" id ="AddRegister" class="btn btn-primary c-btn-style add c-primary-btn">
                                        Register
                                    </button>
                            </div>
                        </form>
                    </div>
                    <div class="forgot--password pt-2 text-center">
                        <a class="nonlife-back" href="{{ route('frontend.nonlife.insurance') }}">Go Back</a>
                    </div>
                    <!-- <div class="forgot--password pt-2 text-center">
                        <span>Need an account?</span>
                    </div> -->
               
                </div>
                </section>
            </div>
        </div>
    </div>
</div>

@endif

{{-- End Of Login Select Plan --}}


