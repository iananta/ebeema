<div class="comprehensive-form-wrapper">
	<form method="post" action="{{ route('frontend.nonlife.insurance.bike.calculation') }}">
		@csrf
		
		<div class="nonlife-row">
			<div class="nonlife-form-group">
				<label>Category</label>
				<select id="categoryListId" class="custom-form-control excess-damage excessCalculate" name="CATEGORYID">
					<option value="" selected>Select Category</option>
					@if(isset($categories) && !empty($categories->data))
						@foreach ($categories->data as $cat)
							<option value="{{ $cat->CATEGORYID }}">
								{{ $cat->CATEGORYNAME }}
							</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="nonlife-form-group">
				<label>Year of Manufacture</label> 
				<select id="manufYear" class="custom-form-control excess-damage excessCalculate fcManuYear ncdCheck" name="YEARMANUFACTURE" requirefd>
					<option value="" selected>Select Manufactured Year</option>
					@if(isset($makeYearLists) && !empty($makeYearLists->data))
						@foreach($makeYearLists->data as $year)
						<option value="{{$year->Manu_Year}}">{{$year->Manu_Year}}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="nonlife-form-group">
				<label>Cubic Capacity (cc)</label> 
				<input class="custom-form-control" type="number" name="CCHP" placeholder="Bike Cubic Capacity e.g. 15">
			</div>
			<div class="nonlife-form-group">
				<label>Vehicle Cost</label>
				<input type="number" class="custom-form-control" name="EXPUTILITIESAMT"  placeholder="Please enter Vehicle Cost">
			</div>
			<div class="nonlife-form-group voluntary-excess visibel-none">
				<label>Voluntary Excess</label>
				<div class="nonlife-radio-button-list">
					<div class="radio-button-row " id="ExcessOwnDamage"></div>
				</div>
			</div>

			<!-- //numfmt_get_error_code	 -->
			<div class="nonlife-form-group voluntary-excess" id="NCDYR">
				<label>NCD Year</label> 
				<div class="nonlife-radio-button-list ncdCheck" id="NCDYR">
					<div class="radio-button-row ncdCheck" id="NCDYR">
						<div class="radio-item active">
							<label>0</label>
							<input type="radio" name="NCDYR" value="0">
						</div>
						<div class="radio-item">
							<label>1</label>
							<input type="radio" name="NCDYR" value="1">
						</div>
					</div>
				</div>
				<p class="nonlife-claim "><input type="checkbox" name="">&nbsp;&nbsp;I thereby accept to bear the consequences if found guilty on No Claim Discount (NCD Year). <span>1 Year</span></p>
			</div>
			<!-- <div class="nonlife-form-group voluntary-excess">
				<label>NCD Year</label>
				<div class="nonlife-radio-button-list">
					<div class="radio-button-row ncdCheck">
					<select class="form-control form-control-inline input-medium ncdCheck" id="NCDYR"
                                            name="NCDYR">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                    </select>
					</div>
				</div>
			</div> -->
			<div class="nonlife-form-group">
				<div class="nonlife-calculate-amount">
					<div class="calculate-item">
						<div class="nonlife-calculate-title">
							<label>Compulsary Excess</label>
						</div>
						<div class="nonlife-calculate-input">
							<input class="cmp-excess-amt" type="number" name="compulsaryexcessamount" id="compulsaryExcessAmount" value="0000" readonly>
						</div>
					</div>
					<div class="calculate-item">
						<div class="nonlife-calculate-title">
							<label>P.A to Driver</label>
						</div>
						<div class="nonlife-calculate-input">
							<input type="number" name="PADRIVER" id="PADRIVER" value="500000" readonly>
						</div>
					</div>
					<div class="calculate-item">
						<div class="nonlife-calculate-title">
							<label>No. of Passenger (@if($nonlifeId == 21)  <input type='number' value="1" name="NOOFPASSENGER" class="noofpassanger" > @else <input type='number' value="4" name="NOOFPASSENGER" class="noofpassanger" > @endif Seat )</label>
						</div>
						<div class="nonlife-calculate-input">
							<input type="number" name="PAPASSENGER" id="PAPASSENGER" value="500000" readonly>
						</div>
					</div>
					<div class="calulate-item full-width">
						<ul class="nonlife-checkbox-list">
							@if($nonlifeId != 21)
								<li><span><input type="checkbox" name="INCLUDE_TOWING" id="INCLUDE_TOWING" value="1"></span>Including Towing Charge</li>
							@endif
							<li><span><input type="checkbox"  name="pool_premium" id="pool_premium" value="1"></span>Excluding Pool Premium</li>
							<li><span><input type="checkbox" name="ISGOVERNMENT" id="ISGOVERNMENT" value="1"></span>Is goverment Vehicle !</li>
						</ul>
					</div>
				</div>      			
			</div>
			<div class="nonlife-form-group full-width">
				<input type="hidden" id="CLASSCODE" name="CLASSID" value="{{$classId}}">
				<input type="hidden" name="TYPECOVER" value="CM" id="TYPECOVERVal">
				<button class="non-life-submit-buttton">Submit</button>
			</div>
		</div>
	</form>
</div>