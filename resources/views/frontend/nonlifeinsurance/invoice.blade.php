@extends('frontend.layouts.app')
@section('title')
<p class="h4 align-center mb-0"></p>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/nonlife.css') }}">

@endsection
@section('content')
<div class="non-life-details-wrapper">
    <div class="nonlife-progress-bar-wrapper">
        <ul id="non-life-progressbar">
            <li class="active" ><span>Vehicle Details</span></li>
            <li class="active"><span>KYC Form</span></li>
            <li class="active"><span>Review</span></li>

            <li class="active current"><span>Confrim</span></li>
        </ul>
    </div>
    <div class="non-life-form-wrapper">
        <div class="container">
            <div class="fieldset-wrapper">
                <div class="invoice-wrapper">
                    <div class="invoice-bg">
                        <div class="invoice-details-block">
                            <div class="invoice-header">

                                @if(is_array($data))
                                @foreach($data as $dt)
                                <h1>Payment Successfully</h1>
                                <form action="{{route('pdf.invoice')}}" method="GET"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="docid" value="{{$dt['AcceptanceNo']}}">
                                <input type="hidden" name="proformano" value="{{$dt['proformano']}}">
                                <button type="submit" class="btn btn-secondary">Pdf Download</button>
                            </form>
                        </div>
                        <div class="invoice-details">
                            @include('layouts.backend.alert')
                            <ul>
                                <li><strong>Product Type</strong>:<span>{{$dt['ClassName']}}</span></li>
                                <li><strong>Insured Name</strong>:<span>{{$dt['insured'] ?? ''}}</span></li>
                                <li><strong>Policy Number</strong>:<span>{{$dt['policyNo']}} </span></li>
                                <li><strong>VAT Invoice Number</strong>:<span>{{$dt['proformano']}} </span></li>
                                <li><strong>Receipt Number</strong>:<span>{{$dt['receiptNo']}}</span></li>
                                <li><strong>Paid Premium Amount</strong>:<span>Rs {{$dt['tpPremium']}} </span></li>
                                <li><strong>Payment Gateway</strong>:<span>{{$paymentFrom}} </span></li>
                                <li><strong>Payment</strong>:<span>{{$dt['FLAG']}} </span></li>
                                <!-- <li><strong>Payment Status</strong>:<span>{{$dt['succFailMsg']   }} </span></li> -->
                                <li><strong>Policy Status</strong>:<span>Electronically generated policy </span></li>
                            </ul>
                            @endforeach
                            @else
                            <h2>Payment UnSuccessfully</h2>
                            <!-- <p>Error : <b>{{$data}}</b></p> -->
                            @endif
                        </div>
                    </div>
                    <div class="finished-button-wrapper">
                        <a href="{{url('/')}}">Go Back to HomePage</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
@endsection


