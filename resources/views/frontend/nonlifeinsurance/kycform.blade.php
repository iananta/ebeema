    <form id="kycform" action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="kyc-form-wrapper">
            <div class="kyc-form-category">
                <h1 class="kyc-cat-title">Insured Type</h1>
                <ul class="kyc-cat-list">
                    <li class="active kyc-tab-item" data-target="kyc-personal-details" data-title="Personal Details"><a href="#">Personal Details</a></li>

                    <li class="kyc-tab-item " data-target="kyc-insured-type" data-title="Insured Type"><a href="#">Insured Type</a></li>
                    <li class="kyc-tab-item" data-target="kyc-document" data-title="Document"><a href="#">Document</a></li>
                </ul>
            </div>

            <div class="kyc-form-details">
                <div class="kyc-form-item" id="kyc-insured-type">
                    <div class="form-group non-life-group-wrapper">
                        <label for="category">Select the Category</label>
                            <select class="form-control  input-medium search-selection select-option"
                            id="categoryListId" name="CATEGORYID" required>
                            <option selected disabled value="">Select the Category</option>
                            @foreach($kycCategories as $cat)
                            <option
                            value="{{$cat->ID}}" {{ $loop->first ? 'selected="selected"' : '' }}>{{$cat->GROUPNAME ?? $cat->GroupName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- <div class="form-group non-life-group-wrapper">
                        <label for="insured type">Insured Type</label>
                        <select class="form-control  input-medium search-selection select-option"
                        id="INSUREDTYPEID" name="INSUREDTYPE" required>
                            <option selected disabled>Insured Type</option>
                            @foreach ($insuredTypes as $insType)
                            <option
                            {{-- @if($insType->INSUREDTYPE == "Individual") selected @endif --}}
                             @if($insuredTypes[4]->INSUREDTYPE) selected @endif --}}
                            {{$user->kyc && $user->kyc['INSUREDTYPE'] == $insType->INSUREDTYPEID ? 'selected' : '' }}
                            value="{{ $insType->INSUREDTYPEID }}">{{ $insType->INSUREDTYPE }}</option>
                            @endforeach
                        </select>
                    </div> -->
                    <div class="form-group non-life-group-wrapper">
                        <label>KYC Classification</label>
                        <select class="form-control form-control-inline input-medium search-selection select-option"
                        id="kycclassificationID" name="kycclassification" required>
                            <option selected disabled>Select Kyc Classification</option>
                            @foreach($kycClassifications as $kycCls)
                            <option
                            @if($loop->first) selected @endif
                            value="{{$kycCls->ID}}">{{$kycCls->CLASSIFICATIONNAME}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group non-life-group-wrapper">
                        <label>KYC Risk Category</label>
                        <select class="form-control form-control-inline input-medium search-selection select-option"
                            id="KYCRiskCategory" name="KYCRiskCategory" required>
                            <option selected disabled>Select Risk Categorys</option>
                            @foreach ($riskCategories as $rskcat)
                                <option
                                @if($rskcat->DESCRIPTION == "Low Risk") selected @endif
                                    {{$user->kyc &&$user->kyc['KYCRiskCategory'] == $rskcat->ID ? 'selected' : '' }}
                                    value="{{ $rskcat->ID }}">{{ $rskcat->DESCRIPTION }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group non-life-group-wrapper">
                        <label>Insured Type</label>
                        <select class="form-control form-control-inline input-medium search-selection select-option"
                        id="INSUREDTYPEID" name="kycclassification" required>
                            <option selected disabled>Select Insured Type</option>
                            @foreach ($insuredTypes as $insType)
                                <option
                                    {{$user->kyc &&$user->kyc['kycclassification'] == $insType->INSUREDTYPEID ? 'selected' : '' }}
                                    value="{{ $insType->INSUREDTYPEID }}">{{ $insType->INSUREDTYPE }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                <div class="manadatory">
                    <div class="form-group non-life-group-wrapper">
                        <label for="gender">Gender</label>
                        <select class="form-control select-option" id="gender" name="GENDER" required>
                                <option disabled>Select Gender</option>
                                <option {{$user->kyc && $user->kyc['GENDER'] == 1 ?'selected' :'' }} value="1" >
                                    Male
                                </option>
                                <option {{$user->kyc && $user->kyc['GENDER'] == 2 ?'selected' :'' }} value="2">
                                    Female
                                </option>
                                <option
                                {{$user->kyc && $user->kyc['GENDER'] == 3 ?'selected' :'' }} value="3">
                                Others
                            </option>
                        </select>
                    </div>
    <div class="form-group non-life-group-wrapper">
        <label for="maritial status"> Marital Status</label>
        <select class="form-control select-option" id="MARITALSTATUS" name="MARITALSTATUS"
        required>
        <option selected disabled>Select Marital Status</option>
        <option value="0"
        {{$user->kyc && $user->kyc['MARITALSTATUS'] == 0 ? 'selected' : ''}} >
        Unmarried
    </option>
    <option value="1"
    {{$user->kyc && $user->kyc['MARITALSTATUS'] == 1 ? 'selected' : ''}}>
    Married
    </option>
    <option value="2"
    {{$user->kyc && $user->kyc['MARITALSTATUS'] == 2 ? 'selected' : ''}}>
    Divorced
    </option>
    <option value="3"  {{$user->kyc && $user->kyc['MARITALSTATUS'] == 3 ? 'selected' : ''}}>
        Widow
    </option>
    </select>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label for="data of birth" for="gender">
            Date Of Birth  <span class="astricshow">*</span>    </label>
            <div class="non-life-form-row nonlife-date-wrapper">
                <div class="form-group">
                    <input id="nepaliDatePicker" type="text" class="form-control dob-datepicker"
                    value="" placeholder="Select Nepali Date">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" value="{{$user->kyc['DATEOFBIRTH'] ?? old('DATEOFBIRTH')}}" id="dob-english-date"
                    name="DATEOFBIRTH" placeholder="Shows English Date" readonly/>
                </div>
            </div>
        </div>

        <div class="form-group non-life-group-wrapper">
            <label for="data of birth">Citizenship no. <span class="red-text astricshow">*</span></label>
            <input type="text" class="form-control" id="ctznno"
            placeholder="Please enter citizenship no."
            value="{{$user->kyc['CITIZENSHIPNO'] ?? ''}}"
            name="CITIZENSHIPNO" required>
        </div>
    </div>
    <!-- <select id="kycchange" style="display:none">
        <option ></option>
        <option ></option>
        <option ></option>
        <option >*</option>
    </select> -->
    <div class="form-group non-life-group-wrapper">
        <label for="issued district">Issued District
           <span class="red-text astricshow">*</span>
        </label>
        <select class="form-control select-option" id="ISSUE_DISTRICT_ID_LIST"
        name="ISSUE_DISTRICT_ID"
        required data-selected="{{$user->kyc['ISSUE_DISTRICT_ID_LIST'] ?? ''}}">
        <option selected disabled value="">Select Issued District <span class="red-text astricshow">*</span></option>
    </select>
    </div>
    <!-- <div class="form-group non-life-group-wrapper">
            <label for="district">District
               <span class="red-text astricshow">*</span>
            </label>
            <select class="form-control  input-medium search-selection select-option" id="districtId"
            name="DISTRICTID" required data-selected="{{$user->kyc['DISTRICTID'] ?? ''}}">
            <option value="">Select the District</option>
        </select>
    </div> -->


    <div class="form-group non-life-group-wrapper">
        <label for="fathername">Father Name<span class="red-text astricshow">*</span></label>
        <input type="text" class="form-control" id="FATHERNAME"
        placeholder="Please enter fathers name."
        value="{{$user->kyc['FATHERNAME'] ?? ''}}" name="FATHERNAME" required>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label for="mothername">Mother Name<span class="red-text astricshow">*</span></label>
        <input type="text" class="form-control" id="MOTHERNAME"
        placeholder="Please enter mothers name."
        value="{{$user->kyc['MOTHERNAME'] ?? ''}}" name="MOTHERNAME" required>
    </div>
    <!-- <div class="form-group non-life-group-wrapper">
        <label for="grandfather">Grand Father Name</label>
        <input type="text" class="form-control" id="GRANDFATHERNAME"
        placeholder="Please enter Grand fathers name."
        value="{{$user->kyc['GRANDFATHERNAME'] ?? ''}}"
        name="GRANDFATHERNAME">
    </div>
    <div class="form-group non-life-group-wrapper">
        <label for="grandmother">Grand Mother Name</label>
        <input type="text" class="form-control" id="GRANDMOTHERNAME"
        placeholder="Please enter Grand mothers name."
        value="{{$user->kyc['GRANDMOTHERNAME'] ?? ''}}"
        name="GRANDMOTHERNAME">
    </div> -->
    <div class="form-group non-life-group-wrapper">
        <label for="husband">Husband Name</label>
        <input type="text" class="form-control" id="HUSBANDNAME"
        placeholder="Please enter your husband name."
        value="{{$user->kyc['HUSBANDNAME'] ?? ''}}" name="HUSBANDNAME">
        <input type="hidden" class="form-control" name="NFATHERNAME"
        value="usersfather">
        <input type="hidden" class="form-control" name="NGRANDFATHERNAME"
        value="usergrandfather">
    </div>
    <div class="form-group non-life-group-wrapper">
        <label for="wife">Wife Name</label>
        <input type="text" class="form-control" id="WIFENAME"
        placeholder="Please enter your wife name."
        value="{{$user->kyc['WIFENAME'] ?? ''}}" name="WIFENAME">
    </div>
    <input type="hidden" name="BRANCHCODE" value="01">
    </div>
    <div class="kyc-form-item active" id="kyc-personal-details">
        <div class="form-group non-life-group-wrapper">
            <label for="Tittle">Full Name (English)
            <span class="red-text astricshow">*</span>
            </label>
            <input required type="text" class="form-control" id="TITLE" placeholder="Enter Name in Full Name"
            name="INSUREDNAME_ENG" value="{{$user->kyc['INSUREDNAME_ENG'] ?? $user->username }}">
        </div>
        <div class="form-group non-life-group-wrapper">
            <label for="Tittle">Full Name (Nepali)
            <span class="red-text astricshow">*</span>
            </label>
            <input required type="text" class="form-control" id="INSUREDNAME_NEP"
            placeholder="Enter Name in Nepali." name="INSUREDNAME_NEP"
            value="{{$user->kyc['INSUREDNAME_NEP'] ?? '' }}" pattern="[\u0900-\u097F]+" required>
            <small class="form-text text-muted"><a class="btn btn-primary btn-sm"
                                                   href="https://www.ashesh.com.np/nepali-unicode.php" target="_blank">Nepali Unicode</a> [Please use this link to convert to Nepali text]</small>
        </div>
        <div class="form-group non-life-group-wrapper">
            <label for="kycclassification">Province
            <span class="red-text astricshow">*</span>
            </label>
            <select class="form-control select-option" id="provinceId" name="ZONEID" required >
                <option selected disabled value="">Select Province</option>
                @foreach($provinces as $province)
                <option
                {{ $user->kyc && $user->kyc['ZONEID'] == $province->id ?'selected' :'' }}
                value="{{$province->id}}">{{$province->province_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group non-life-group-wrapper">
            <label for="district">District
               <span class="red-text astricshow">*</span>
            </label>
            <select class="form-control  input-medium search-selection select-option" id="districtId"
            name="DISTRICTID" required data-selected="{{$user->kyc['DISTRICTID'] ?? ''}}">
            <option value="">Select the District</option>
        </select>
    </div>

    <div class="form-group non-life-group-wrapper mnu">
        <label for="MNU">MNU
           <span class="red-text astricshow">*</span>
        </label>
        <select class="form-control  input-medium search-selection select-option" id="MNUId"
        name="MUNICIPALITYCODE"  data-selected="{{$user->kyc['MUNICIPALITYCODE'] ?? ''}}">
        <option selected disabled value="">MNU</option>
    </select>
    </div>

    <div class="form-group non-life-group-wrapper vdc">
        <label for="VDC">VDC
           <span class="red-text astricshow">*</span>
        </label>
        <select class="form-control  input-medium search-selection select-option vdc" id="VDCId"
        name="VDCCODE" data-selected="{{$user->kyc['VDCCODE'] ?? ''}}" >
        <option selected disabled value="">Select VDC</option>
    </select>
    </div>

    <div class="form-group non-life-group-wrapper">
        <label>Address
           <span class="red-text astricshow">*</span>
        </label>
        <input type="text" class="form-control" id="ADDRESS"
        placeholder="Please enter address." name="ADDRESS"
        value="{{$user->kyc['ADDRESS'] ?? '' }}" required>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Address (Nepali)</label>
        <input type="text" class="form-control" id="ADDRESSNEPALI"
        placeholder="Please enter address in nepali." name="ADDRESSNEPALI"
        value="{{$user->kyc['ADDRESSNEPALI'] ?? '' }}">
        <small class="form-text text-muted"><a class="btn btn-primary btn-sm"
                                               href="https://www.ashesh.com.np/nepali-unicode.php" target="_blank">Nepali Unicode</a> [Please use this link to convert to Nepali text]</small>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Temp. Address (Nepali)</label>
        <input type="text" class="form-control"
        placeholder="Please enter temporary address in nepali." name="NTEMPORARYADDRESS" value="{{$user->kyc['NTEMPORARYADDRESS'] ?? ''}}" min="0">
        <small class="form-text text-muted"><a class="btn btn-primary btn-sm"
                                               href="https://www.ashesh.com.np/nepali-unicode.php" target="_blank">Nepali Unicode</a> [Please use this link to convert to Nepali text]</small>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Temp. Address</label>
        <input type="text" class="form-control" id="TEMPORARYADDRESS"
        placeholder="Please enter temporary address." name="TEMPORARYADDRESS" value="{{$user->kyc['TEMPORARYADDRESS'] ?? ''}}" min="0">
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Ward No.</label>
        <input type="number" class="form-control" id="WARDNO"
        placeholder="Please enter Ward no." name="WARDNO"
        value="{{$user->kyc['WARDNO'] ?? '' }}" min="0">
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>House No.</label>
        <input type="number" class="form-control" id="houseno"
        placeholder="Please enter house No." name="HOUSENO"
        value="{{$user->kyc['HOUSENO'] ?? '' }}" min="0" >
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Plot No.</label>
        <input type="number" class="form-control" id="PLOTNO" placeholder="Please enter plot No."
        name="PLOTNO" value="{{$user->kyc['PLOTNO'] ?? ''}}" min="0" >
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Home Tel. No.</label>
        <input type="tel" class="form-control" id="telno" placeholder="Please enter Telephone Number."
        name="HOMETELNO" value="{{$user->kyc['HOMETELNO'] ?? ''}}" >
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Mobile No.
           <span class="red-text astricshow">*</span>
        </label>
        <input type="tel" class="form-control" id="mbno" placeholder="Please enter Mobile Number."
        name="MOBILENO" value="{{$user->kyc['MOBILENO'] ?? ''}}" required>
    </div>

    <div class="form-group non-life-group-wrapper">
        <label>Email Address</label>
        <input type="text" class="form-control" id="email" placeholder="Please enter email address."
        name="EMAIL" value="{{$user->kyc['EMAIL'] ?? ''}}">
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Occupation</label>
        <select class="form-control  input-medium search-selection select-option" id="occupation" name="OCCUPATION"
        >
        <option selected disabled value="">Select Occupation</option>
        @foreach ($kycOccupations as $occ)
        <option {{$user->kyc &&$user->kyc['OCCUPATION'] == $occ->BUSSOCCPCODE ?'selected' :'' }}
            value="{{$occ->BUSSOCCPCODE}}">{{$occ->DESCRIPTION}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group non-life-group-wrapper">
        <label>Income Source</label>
        <select class="form-control  input-medium search-selection select-option" id="INCOMESOURCE"
        name="INCOMESOURCE" >
        <option selected disabled>Select Income Source</option>
        @foreach($kycincomesources as $incSrc)
        <option {{$user->kyc &&$user->kyc['INCOMESOURCE'] == $incSrc->ID ?'selected' :'' }}
            value="{{$incSrc->ID}}">{{$incSrc->EDESC}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label>Pan Number</label>
        <input type="number" class="form-control form-control-inline input-medium search-selection" id="panno" placeholder="Please enter Pan Number."
        name="PANNO"  value="{{$user->kyc['PANNO']??'' }}" min="0">
    </div>
    </div>
    <div class="kyc-form-item" id="kyc-document">
        <div class="form-group non-life-group-wrapper">
            <label for="Blue Book Image">Browse Photo  <span class="red-text astricshow">*</span></label>
            <div class="custom-upload-file">
            <input type="file" class="custom-file-input" name="photos"
            id="photos">
            <label for="photos" class="custom-upload-label">Upload</label>
        </div>
        <span class="custom-file-image" for="images_view">
                @if(Auth::user()->kyc)
                <img src="{{Auth::user()->kyc['photo'] ?? ''}}" alt="citizenship frontside">
                @endif
            </span>
            <button class="dismiss-previews">&times;</button>
    </div>


    <div class="form-group non-life-group-wrapper">
        <label for="Blue Book Image">Upload Citizen Front</label>
        <div class="custom-upload-file">
            <input type="file" class="custom-file-input" name="citfrnt"
            id="citfront">
            <label class="custom-upload-label" for="citfront">Upload</label>
        </div>
        <span class="custom-file-image" for="images_view">
                @if(Auth::user()->kyc)
                <img src="{{Auth::user()->kyc['citfrntimg'] ?? ''}}" alt="citizenship backside">
                @endif
            </span>
            <button class="dismiss-previews">&times;</button>
    </div>
    <div class="form-group non-life-group-wrapper">
        <label for="Blue Book Image">Upload Citizen Back</label>
        <div class="custom-upload-file">
            <input type="file" class="custom-file-input" name="citback"
            id="citback">
            <label class="custom-upload-label" for="citback"><img src="{{asset('frontend/img/nonlife/upload.png')}}" title="" alt="citizenship back side">Upload</label>

        </div>
    </div>
    <div class="form-group non-life-group-wrapper cpmreg">
        <label for="Blue Book Image">Company registration & Pan Certificate</label>
        <div class="custom-upload-file">
            <input type="file" class="custom-file-input" name="cpmreg"
            id="cpmreg">
            <label class="custom-upload-label" for="cpmreg"><img src="{{asset('frontend/img/nonlife/upload.png')}}" title="" alt="pan certification">Upload</label>
        </div>
    </div>
    <small class="image-note">Photo must be image format and less than 5mb</small>
    </div>
        <div class="kyc-next-button">
            <button type="button" class="kyc-previous-btn">Previous</button>
            <button type="button" class="kyc-next-btn">Next</button>
        </div>
    </div>
    </div>
    <input type="hidden" name="BRANCHCODE" value="01">
    <input type="hidden" name="BRANCHID" value="1">
    <input type="hidden" name="ACCOUNTNAMECODE">
    <input type="hidden" name="AREAID" value="0">
    <input type="hidden" name="TOLEID" value="0">
    <input type="hidden" name="customer_id" id="CustomerList" value="">
    <input type="hidden" name="kyc_id" value="">
    <input type="hidden" name="is_ajax_store" value="1">
    <input type="hidden" name="is_ajax" value="1">
    <input type="hidden" name="frontend_request" value="1">

    {{-- <input type="hidden" name="user_id" value="{{$user ? $user->id : ''}}"> --}}
    <input type="hidden" name="user_id" value="{{Auth::user() ? Auth::user()->id : ''}}">
    @if(count($customers) === 0)
    <input type="hidden" name="KYCNO" value="">
    @else
    <input type="hidden" name="kyc_id" value="{{auth()->user()->kyc->id ?? ''}}">
    <input type="hidden" name="customer_id" id="CustomerList" value="{{$user ? $user->kyc->customer_id : ''}}">
    <input type="hidden" name="KYCNO" value="{{$user ? $user->kyc->KYCNO : ''}}">
    @endif

    </form>
    <div class="step-action-button">
        <div class="container">
            <button type="button" class="nonlife-previous">Previous</button>
            <button type="submit" class="nonlife-next kyccustomer"
            id="kycsubmit">Continue</button>
        </div>
    </div>
