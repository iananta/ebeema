
<!-- <div class="review-confirm-wrapper">
	<div id="insured-type-wrapper" class="accordian-item">
		<div class="accordian-header-wrapper">
			<h1>PERSONAL DETAILS</h1>
			 <button class="accordian-button"><img src="{{asset('frontend/img/nonlife/circle-chervon.png')}}" alt="review dropdwon list"></button> 
		</div>
		<div class="">
            <div class="review-item">
                <h2>Full Name (Nepali)</h2>
                <p>{{$kyc['INSUREDNAME_ENG'] ?? ''}}</p>
            </div>
            <div class="review-item">
                <h2>Full Name (English)</h2>
                <p>{{$kyc['INSUREDNAME_NEP'] ?? ''}}</p>
            </div>
            <div class="review-item">
                <h2>Province</h2>
                @if(isset($kyc['ZONEID']) == 1 ?'selected' : '')
                <p>Provice no 1</p>
                @elseif(isset($kyc['ZONEID']) == 2 ?'selected' : '')
                <p>Province no 2</p>
                @elseif(isset($kyc['ZONEID']) == 3 ?'selected' : '')
                <p>Province no 3</p>
                @elseif(isset($kyc['ZONEID']) == 4 ?'selected' : '')
                <p>Province no 4</p>
                @elseif(isset($kyc['ZONEID']) == 5 ?'selected' : '')
                <p>Province no 5</p>
                @elseif(isset($kyc['ZONEID']) == 6 ?'selected' : '')
                <p>Province no 6</p>
                @endif
            </div>
            <div class="review-item">
                <h2>Address</h2>
                <p>{{$kyc['ADDRESS'] ?? ''}}</p>
            </div>
        </div>
    </div>
    <div id="personal-details-wrapper" class="accordian-item">
      <div class="accordian-header-wrapper">
         <h1>INSURED TYPES</h1>
         <button class="accordian-button"><img src="{{asset('frontend/img/nonlife/circle-chervon.png')}}" alt="review dropdwon list"></button>
     </div>
     <div class="accordian-data-wrapper">
        <div class="review-item">
            <h2>Name</h2>
            <p>{{$kyc['INSUREDNAME_ENG'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2> Citizenship No.</h2>
            <p>{{$kyc['CITIZENSHIPNO'] ?? ''}}</p> 
        </div>
        <div class="review-item">
            <h2>Gender</h2>
            @if(isset($kyc['GENDER']) == 1 ?'selected' : '')
            <p>Male</p>
            @elseif(isset($kyc['GENDER']) == 2 ?'selected' : '')
            <p>Female</p>
            @else
            <p>Other</p>
            @endif
        </div>
        <div class="review-item">
            <h2> MARITALSTATUS </h2>
            @if(isset($kyc['MARITALSTATUS']) == 0)
            <p>Unmarried</p>
            @elseif(isset($kyc['MARITALSTATUS']) == 1)
            <p>Married</p>
            @elseif(isset($kyc['MARITALSTATUS']) == 2)
            <p>Divorced</p>
            @else
            <p>Widow</p>
            @endif
        </div>
        <div class="review-item">
            <h2> Date of Birth </h2>
            <p>{{$kyc['DATEOFBIRTH'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>Mobile No</h2>
            <p>{{$kyc['MOBILENO'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>Father Name</h2>
            <p>{{ $kyc['FATHERNAME'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>MOTHER Name</h2>
            <p>{{ $kyc['MOTHERNAME'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>GRANDFATHER Name</h2>
            <p>{{ $kyc['GRANDFATHERNAME'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>GRANDMOTHER Name</h2>
            <p>{{ $kyc['GRANDMOTHERNAME'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>Husband Name</h2>
            <p>{{ $kyc['HUSBANDNAME'] ?? ''}}</p>
        </div>
        <div class="review-item">
            <h2>Wife Name</h2>
            <p>{{ $kyc['WIFENAME'] ?? ''}}</p>
        </div>
    </div>
</div>
</div> -->


<div class="review-payment-block-wrapper">
    <div class="review-block">
        <div class='review-block-bg'>
            <div class="review-item-wrap">
                <div class="review-title">
                    <h1>Personal Details</h1>
                </div>
                <div class="review-item-details">
                    <div class="review-details-wrapper">
                        <strong>Full Name</strong>
                        <p>
                        {{$kyc['INSUREDNAME_ENG'] ?? ''}}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Name in nepali</strong>
                        <p>
                        {{$kyc['INSUREDNAME_NEP'] ?? ''}}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Address</strong>
                        <p>
                        {{$kyc['ADDRESS'] ?? ''}}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                    <strong>Nepali Address</strong>
                        <p>
                        {{ $kyc['ADDRESSNEPALI'] ?? '' }}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Citizenship No.</strong>
                        <p>
                        {{$kyc['CITIZENSHIPNO'] ?? ''}}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Date of Birth.</strong>
                        <p>
                        {{$kyc['DATEOFBIRTH'] ?? ''}}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Province </strong>
                        @if(@$kyc['ZONEID'] == 1) <p> province no 1 </p>
                        @elseif(@$kyc['ZONEID'] == 2) <p> province no 2</p>
                        @elseif(@$kyc['ZONEID'] == 3) <p> province no 3</p>
                        @elseif(@$kyc['ZONEID'] == 4) <p> province no 4</p>
                        @elseif(@$kyc['ZONEID'] == 5) <p> province no 5</p>
                        @else<p> Province has not been selected</p>
                        @endif
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Mobile Number</strong>
                        <p>
                        {{$kyc['MOBILENO'] ?? ''}}
                        </p>
                    </div>
                </div>

            </div>
            <div class="review-item-wrap">
                <div class="review-title">
                    <h1>Vehicle & Policy Details</h1>
                </div>
                <div class="review-item-details">
                    <!-- <div class="review-details-wrapper">
                        <strong>Policy Type</strong>
                        <p> 
                        @if(@$directData->INSUREDTYPE == 1 ?'selected' : '')
                          <p>Cooperate</p>
                          @else
                          <p>Individual</p>
                           @endif
                        </p>
                    </div> -->
                    <div class="review-details-wrapper">
                        <strong>Engine Number</strong>
                        <p>
                        {{$directData->ENGINENO ?? ''}}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Chassis Number</strong>
                        <p>
                        {{ $directData['CHASISNO'] ?? '' }}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Vehicle Number</strong>
                        <p>
                        {{ $directData['VEHICLENO'] ?? '' }}
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Model</strong>
                        <p>{{$formData['MODEL'] ?? 'N/A'}}</p>
                    </div>
                    <div class="review-details-wrapper dropdown">
                        <strong>Formation </strong>
                        <select class="form-control"
                                        id="vehicleId"
                                        name="VEHICLENAMEID" disabled>
                        @foreach($vehicleNames->data as $vcl)
                        <option value="{{$vcl->ID}}" {{@$formData['VEHICLENAMEID'] == $vcl->ID ? "selected" : ''}}>{{$vcl->ENGNAME}}</option>
                        @endforeach
                        
                        </select>
                        
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Year of ManuFacture</strong>
                        <p>{{$directData->YEARMANUFACTURE ?? ''}}</p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Insured Type</strong>
                        <p> 
                          @if(isset($directData->TYPECOVER) == 'TP')
                          <p>Third Party Only</p>
                          @else
                          <p>First Party Only</p>
                           @endif
                        </p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Cubic Capacity/Tone</strong>
                        <p>{{@$directData->CCHP}}</p>
                    </div>
                    <div class="review-details-wrapper">
                        <strong>Cost of Vehicle </strong>
                        <p>
                        {{$directData->EXPUTILITIESAMT ?? ''}}
                        </p>
                    </div>
                     <div class="review-details-wrapper dropdown">
                        <strong> Manufacturing Company</strong>
                      <select class="form-control"
                                        id="dropDownId"
                                        name="MAKEVEHICLEID" required>
                        @foreach($manufacturers->data as $manu)
                        <option value="" {{@$formData['MAKEVEHICLEID'] == $manu->ID ? "selected" : ''}}>{{$manu->ENGNAME}}</option>
                        @endforeach
                        
                        </select>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    <div class="payment-block">
        <div class='payment-block-bg'>
            <div class="payment-title">
                <h1>Your new policy will be start from 1/November/2021</h1>
            </div>
            <div class="payment-info-wrap">
                <div class="payment-premium-info">
                    <ul class="payment-premium-ul">
                        <li><strong>Ref Id</strong><span class="refId
                        ">{{$paymentInfo->RefId != null ? $paymentInfo->RefId : 'N/A'}}</span></li>
                        <li><strong>Amount</strong><span>Rs. {{number_format($paymentInfo->Amount, 2, '.', '')}}</span></li>
                    </ul>
                </div>
            </div>
            <form action="{{$ime_pay_checkout_url}}"method="post">
            <div class="payment-method-wrap">
                <div class="payment-method-info">
                    <div class="payment-method-title">
                        <h2>Payment Method</h2>
                    </div>
                    <ul class="payment-method-list">
                        <li class="pay-list">
                        <input type="checkbox" name="condition" id="imepay" required>    
                       
                        <label class="paylabel" for="imepay"><img src="{{asset('backend/img/SVG RED Logo.svg')}}"></label>
                        </li>
                    </ul>
                </div>
            </div>
             <div class="payment-info-wrap">
                <div class="payment-premium-info">
                    <div class="total-need-pay">
                        <h2>You Pay</h2>
                        <p class="pay-amount-nonlife">Rs. {{number_format($paymentInfo->Amount, 2, '.', '')}}</p>
                    </div>
                    <div class="payment-term-condition-wrapper">
                        <input type="checkbox" name="condition" required>
                        <label>I agree to the <a href="">Terms & Condition</a> & i confirm that my vehicle has a valid FUC certificate</label>
                    </div>
                </div>
            </div>
            <div class="pay-button-wrapper">
                       <input type="hidden" name="KYCNO" value="{{Auth::user()->kyc ? Auth::user()->kyc->KYCNO: ''}}">
                       <input type="hidden" name="TokenId" value="{{$paymentInfo->TokenId}}">
                       <input type="hidden" name="MerchantCode" value="{{$merchantCode}}">
                       <input type="hidden" name="RefId" value="{{$paymentInfo->RefId}}">
                       <input type="hidden" name="TranAmount" value="{{($paymentInfo->Amount)}}">
                       <input type="hidden" name="Method" value="GET">
                       <input type="hidden" name="RespUrl" value="{{route('successfull.imepay')}}">
                       <input type="hidden" name="CancelUrl" value="{{route('canceled.imepay')}}">
                      <button class="pay-button" type="submit"><i class="fa fa-lock"></i>&nbsp;&nbsp;Pay Securely</button>
                    </form>
            </div>
        </div>
    </div>
</div>