<div class="third-party-form-wrapper">
	<form method="post" action="{{ route('frontend.nonlife.insurance.bike.calculation') }}">
		@csrf
		<div class="nonlife-row">
			<div class="nonlife-form-group">
				<label>Category</label>
				<select id="categoryListId" class="custom-form-control select-option" name="CATEGORYID" required>
					<option value="" selected>Select Category</option>
					@if(isset($categories) && !empty($categories->data))
						@foreach ($categories->data as $cat)
							<option value="{{ $cat->CATEGORYID }}">
								{{ $cat->CATEGORYNAME }}
							</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="nonlife-form-group">
				<label>Year of Manufacture</label> 
				<select class="custom-form-control select-option" name="YEARMANUFACTURE">
					<option value="" selected>Select Manufactured Year</option>
					@if(isset($makeYearLists) && !empty($makeYearLists->data))
						@foreach($makeYearLists->data as $year)
						<option value="{{$year->Manu_Year}}">{{$year->Manu_Year}}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="nonlife-form-group">
				<label>Cubic Capacity (cc)</label>
				<input class="custom-form-control" type="number" name="CCHP" required min="0" placeholder="Bike Cubic Capacity e.g. 15">
			</div>
			<div class="nonlife-form-group full-width">
				@if(auth()->check())
				<input type="hidden" name="user_id" value="{{auth()->user()->id}}">
				@endif
				<input type="hidden" id="CLASSCODE" name="CLASSID" value="{{$nonlifeId}}">
				<input type="hidden" name="TYPECOVER" value="TP" id="TYPECOVERVal">
				<button class="non-life-submit-buttton">Submit</button>
			</div>
		</div>
	</form>
</div>