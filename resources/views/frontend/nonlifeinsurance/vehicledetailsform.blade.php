                        <form id="vehicleForm" method="POST" enctype="multipart/form-data">
                             @csrf
                            @if(isset($premiums))
                            <input type="hidden" name="BRANCHID" value="1">
                            <input type="hidden" name="CLASSID" value="{{$requested_data['CLASSID']}}">
                            <input type="hidden" name="DEPTID" value="2">
                            <input type="hidden" name="HAS_TRAILOR" value="0">
                            <input type="hidden" name="BASICPREMIUM_A" value="{{ $premiums->BASICPREMIUM ?? '0.00' }}">
                            <input type="hidden" name="THIRDPARTYPREMIUM_B" value="{{ $premiums->THIRDPARTYPREMIUM ?? '0.00' }}">
                            <input type="hidden" name="DRIVERPREMIUM_C" value="{{ $premiums->DRIVERPREMIUM ?? '0.00' }}">
                            <input type="hidden" name="HELPERPREMIUM_D" value="{{ $premiums->HELPERPREMIUM ?? '0.00' }}">
                            <input type="hidden" name="PASSENGERPREM_E" value="{{ $premiums->PASSENGERPREMIUM ?? '0.00' }}">
                            <input type="hidden" name="RSDPREMIUM_F" value="{{ $premiums->POOLPREMIUM ?? '0.00' }}">
                            @if(Auth::user()->kyc)
                            <input type="hidden" name="customer_id" value="{{Auth::user()->kyc->customer_id}}">
                            @endif
                            <!-- hidden B-->
                            <input type="hidden"
                              placeholder="Net Premium Amount"
                              name="NETPREMIUM"
                              value="{{ $premiums->NETPREMIUM ?? '0.00' }}">
                            <input type="hidden"
                              placeholder="Third party Premium"
                              name="THIRDPARTYPREMIUM"
                              value="{{ $premiums->THIRDPARTYPREMIUM ?? '0.00' }}">
                            <input type="hidden" id="OTHERPREMIUM"
                                  placeholder="Other Premium"
                                  name="OTHERPREMIUM"
                                  value="{{ $premiums->OTHERPREMIUM ?? '0.00' }}">
                            <input type="hidden" id="tvp"
                                  placeholder="Total Vatable Premium"
                                  name="TOTALVATABLEPREMIUM"
                                  value="@php($totalVatable = $premiums->NETPREMIUM +  $premiums->STAMP){{ $totalVatable ?? '0.00' }}"
                                  > 
                            <!-- end hidden B -->
                             @php($vaTtotal = $totalVatable * 0.13)
                            <input type="hidden" id="VAT"
                                placeholder="VAT" name="VAT" value="{{ $_GET['VAT']??'13' }}">
                            <input type="hidden" id="VATAMT"
                                                      name="VATAMT"
                                                      value="{{ round($vaTtotal,2) ?? '0.00' }}">
                            <input type="hidden" class="form-contramountFormatterol" id="anp"
                                placeholder="Actual Net Premium"
                                name="TOTALNETPREMIUM"
                                value="@php($totalNet = $totalVatable + round($vaTtotal,2)){{ $totalNet ?? '0.00' }}"
                            >
                            @endif
                                <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                            <div class="form-group non-life-group-wrapper">
                                <label for="VEHICLENO">Vehicle No. <span class="red-text">*</span></label>
                                <input type="text" class="form-control text-uppercase" id="VEHICLENO"
                                    placeholder="" name="VEHICLENO"  data-error="Firstname is required."
                                    value="{{ $_GET['VEHICLENO'] ?? old('VEHICLENO')}}" required>     
                            </div>
                            <div class="form-group non-life-group-wrapper">
                                <label for="ENGINENO">Engine No. <span class="red-text">*</span></label>
                                <input type="text" class="form-control text-uppercase" id="ENGINENO"
                                        placeholder="" name="ENGINENO"  data-error="Firstname is required."
                                        value="{{ $_GET['ENGINENO'] ?? '' }}" required>
                            </div>
                            <div class="form-group non-life-group-wrapper">
                                <label for="CHASISNO">Chasis No <span class="red-text">*</span></label>
                                <input type="text" class="form-control text-uppercase" id="CHASISNO"
                                        placeholder="" name="CHASISNO" required
                                        value="{{ $_GET['CHASISNO'] ?? '' }}">
                            </div>
                            <div class="form-group non-life-group-wrapper">
                                <label for="MANUFACTURE">Manufacturer Company <span class="red-text">*</span></label>
                                <select class="form-control  input-medium search-selection select-option"
                                        id="MAKEVEHICLEID" name="MAKEVEHICLEID" required>
                                    <option selected disabled value="">Select the Manufacturer</option>
                                    @if(isset($manufacturers))
                                    @foreach ($manufacturers->data as $manu)
                                        <option value="{{ $manu->ID }}">{{ $manu->ENGNAME }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                             <div class="form-group non-life-group-wrapper">
                                    <label for="MAKEMODEL">Model<span class="red-text">*</span></label>
                                    <select class="form-control  input-medium search-selection select-option"
                                        id="MAKEMODELID" name="MAKEMODELID" required>
                                        <option selected disabled value="">Select the Model</option>
                                        <!-- @if(isset($makeModels))
                                        @foreach ($makeModels->data as $mod)
                                            <option value="{{ $mod->ID }}">{{ $mod->ENGNAME }}</option>
                                        @endforeach
                                        @endif -->
                                    </select>
                                    <input type="hidden" name="MODEL" id="modelName">
                            </div>
                           
                            <div class="form-group non-life-group-wrapper">
                                    <label for="VECHILENAME">Formation<span class="red-text">*</span></label>
                                    <select class="form-control  input-medium search-selection select-option"
                                        id="VEHICLENAMEID" name="VEHICLENAMEID" required>
                                        <option selected disabled value="">Select the formation</option>
                                        @if(isset($vehicleNames))
                                        @foreach ($vehicleNames->data as $vcl)
                                            <option value="{{ $vcl->ID }}">{{ $vcl->ENGNAME }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                            </div>
                            <div class="form-group non-life-group-wrapper">
                                    <label for="OCCUPATION">Occupation</label>
                                    <select class="form-control  input-medium search-selection select-option"
                                        id="BUSSOCCPCODE" name="BUSSOCCPCODE">
                                        <option selected disabled value="">Select the Occupation</option>
                                        @if(isset($occupationLists))
                                        @foreach ($occupationLists->data as $occ)
                                            <option value="{{ $occ->BUSSOCCPCODE }}">{{ $occ->DESCRIPTION }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                            </div>
                            <!-- <div class="form-group non-life-group-wrapper">
                                <label for="INSUREDTYPE">InsuredType<span class="red-text">*</span></label>
                                <select class="form-control inpu-medium search-selection select-option" id="INSUREDTYPEID"
                                        name="INSUREDTYPE" required>
                                    <option selected disabled value="">Select the Insuredtype</option>
                                    <option value="1">Cooperate</option>
                                    <option value="2">Individual</option>
                                </select>
                            </div> -->
                            <div class="form-group non-life-group-wrapper"> 
                                <label for="MODE">Mode of Use <span class="red-text">*</span></label>
                                <input type="text" class="form-control" id="MODEUSE"
                                        placeholder="" name="MODEUSE" required
                                        value="{{ $_GET['MODEUSE'] ?? '' }}">
                            </div>
                            <div class="form-group non-life-group-wrapper">
                                <label for="REGISTRATIONDATE">Registration Date</label>
                                <div class="non-life-form-row nonlife-date-wrapper">
                                    <div class="form-group">
                                        <input type="text" class="form-control nepali-datepicker"
                                                name="NEPALIREGISTRATIONDATE" placeholder="Select Nepali Date"
                                                id="NEPALIREGISTRATIONDATE">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="english-date"
                                                name="REGISTRATIONDATE" placeholder="Shows English Date" readonly/>
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group non-life-group-wrapper">
                                <label for="Blue Book Image">Upload Blue Book Image</label>
                                <div class="custom-upload-file">
                                    
                                    <input type="file" id="bluebookimage" name="bluebook_images" class="bluebook_image">
                                    <label for="bluebookimage" class="custom-upload-label"><img src="{{asset('frontend/img/nonlife/upload.png')}}" alt="nonlife blue book image" title="Blue Book Image">Upload</label>
                                </div>
                            </div>
                             <div class="form-group non-life-group-wrapper">
                                <label for="Blue Book Image">Upload Bike Image</label>
                                <div class="custom-upload-file">
                                    <input type="file" id="bikeimage" name="bike_images" class="bike_images">
                                    <label for="bikeimage" class="custom-upload-label"><img src="{{asset('frontend/img/nonlife/upload.png')}}" alt="nonlife bike image" title="Bike Image">Upload</label>
                                </div>
                            </div>
                        </form>
                       <div class="step-action-button">
                            <div class="container"> 
                                <button type="submit" class="nonlife-next vechiledetail"
                                    id="vechiledetail">Continue</button>
                            </div>
                        </div>
                   