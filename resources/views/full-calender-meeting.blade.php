@extends('layouts.backend.containerlist')
@section('header_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js" defer></script>
    <style>
        .small-fnt {
            font-size: 12px;
        }

        .swal2-container {
            z-index: 20000 !important;
        }
    </style>
@endsection
@section('title')
    Meetings Calendar
@endsection
@section('dynamicdata')
@if(isset($userName))
<span class="badge badge-success small-fnt"><b>Outlook Connected,</b> Welcome {{ $userName }} !</span>
@else
<a href="/signin" class="btn btn-primary btn-sm small-fnt" title="Sign in to access outlook calender.">Sign
    in to outlook</a>
@endif

    <div id="calendar"></div>

    <!-- Modal -->
    <div class="modal fade" id="MeetingCalendar" tabindex="-1" role="dialog" aria-labelledby="MeetingCalendarLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="MeetingCalendarLabel">Meeting Calendar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="meetingForm" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id">
                        <input type="hidden" name="type" value="add">
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="text" class="form-control" name="eventSubject"/>
                        </div>
                        <div class="form-group">
                            <label>Attendees</label>
                            <input type="text" class="form-control" name="eventAttendees"/>
                            <small id="emailHelp" class="form-text text-muted"> Multiple Attendees are added by using
                                semi-colon <br> <b>(EG: hello@outlook.com;hey@outlook.com )</b></small>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Start</label>
                                    <input type="datetime-local" class="form-control" name="eventStart"
                                           id="eventStart"/>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>End</label>
                                    <input type="datetime-local" class="form-control" name="eventEnd" id="eventEnd"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea type="text" class="form-control" id="EvBody" name="eventBody" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2 buttonText">Create</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="deleteMeeting" class="btn btn-danger float-right d-none">Delete</button>
                    </form>
                </div>
                <div class="modal-footer">
                    {{--                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_js')
    <script type="text/javascript">
        $(document).ready(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var calendar = $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay,listWeek'
                },
                timeZone: 'Asia/Kathmandu',
                events: '/admin/full-calender-meeting',
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                selectHelper: true,
                editable: true,
                select: function (start, end, hour, allDay) {
                    var start = $.fullCalendar.formatDate(start, 'YYYY-MM-DD HH:mm:ss');
                    var end = $.fullCalendar.formatDate(end, 'YYYY-MM-DD HH:mm:ss');
                    var newTime = new Date();
                    var now = new Date(start);
                    var next = new Date(end);
                    var mForm = $('#meetingForm');
                    // var title = prompt('Event Title:');
                    // console.log(start,end,newTime);
                    $('#deleteMeeting').addClass('d-none');
                    $('.buttonText').text('Create');
                    mForm.find('[name="id"]').val('');
                    mForm.find('[name="type"]').val("add");
                    mForm.find('[name="eventSubject"]').val('');
                    mForm.find('[name="eventAttendees"]').val('');
                    mForm.find('[name="eventBody"]').text('');
                    $('#eventStart').val(new Date(now.getTime() - now.getTimezoneOffset() * 60000).toISOString().substring(0, 19));
                    $('#eventEnd').val(new Date(next.getTime() - next.getTimezoneOffset() * 60000).toISOString().substring(0, 19));
                    $("#MeetingCalendar").modal("show");
                },
                eventResize: function (event, delta) {
                    alert(info.event.title + " end is now " + info.event.end.toISOString());

                    if (!confirm("is this okay?")) {
                        info.revert();
                    }
                    var start = $.fullCalendar.formatDate(event.start, 'Y-MM-DD HH:mm:ss');
                    var end = $.fullCalendar.formatDate(event.end, 'Y-MM-DD HH:mm:ss');
                    var title = event.title;
                    var attendes = event.eventAttendees;
                    var eventBody = event.eventBody;
                    var id = event.id;
                    $.ajax({
                        url: "/admin/full-calender-meeting/store",
                        type: "POST",
                        data: {
                            eventSubject: title,
                            eventAttendees: attendes,
                            eventStart: start,
                            eventEnd: end,
                            eventBody: eventBody,
                            id: id,
                            type: 'update'
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            calendar.fullCalendar('refetchEvents');
                            alert("Event Updated Successfully");
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            swal.fire('Oops...', 'Something went wrong!', 'error');
                        }
                    })
                },
                eventDrop: function (event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, 'Y-MM-DD HH:mm:ss');
                    var end = $.fullCalendar.formatDate(event.end, 'Y-MM-DD HH:mm:ss');
                    var title = event.title;
                    var attendes = event.eventAttendees;
                    var eventBody = event.eventBody;
                    var id = event.id;
                    $.ajax({
                        url: "/admin/full-calender-meeting/store",
                        type: "POST",
                        data: {
                            eventSubject: title,
                            eventAttendees: attendes,
                            eventStart: start,
                            eventEnd: end,
                            eventBody: eventBody,
                            id: id,
                            type: 'update'
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            calendar.fullCalendar('refetchEvents');
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Congratulations!',
                                html: "Event Updated Successfully",
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: true,
                            });
                            // alert("Event Updated Successfully");
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            swal.fire('Oops...', 'Something went wrong!', 'error');
                        }
                    })
                },

                eventClick: function (event) {
                    $('#deleteMeeting').removeClass('d-none');
                    var mForm = $('#meetingForm');
                    var now = new Date(event.start);
                    var next = new Date(event.end);
                    $('.buttonText').text('Update');
                    $("#MeetingCalendar").modal("show");
                    mForm.find('[name="type"]').val("update");
                    mForm.find('[name="id"]').val(event.id);
                    mForm.find('[name="eventSubject"]').val(event.title);
                    mForm.find('[name="eventAttendees"]').val(event.eventAttendees);
                    mForm.find('[name="eventStart"]').val(new Date(now.getTime()).toISOString().substring(0, 19));
                    mForm.find('[name="eventEnd"]').val(new Date(next.getTime()).toISOString().substring(0, 19));
                    mForm.find('[name="eventBody"]').text(event.eventBody);
                }
            });
            $('#meetingForm').on('submit', function (e) {
                e.preventDefault();
                frmDta = $('#meetingForm').serialize();
                if (frmDta) {
                    // console.log(frmDta);
                    $.ajax({
                        url: "/admin/full-calender-meeting/store",
                        type: "POST",
                        data: frmDta,
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (data) {
                            $('#preloader').hide();
                            $("#MeetingCalendar").modal("hide");
                            $('#meetingForm')[0].reset();
                            calendar.fullCalendar('refetchEvents');
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Congratulations!',
                                html: "Event Created Successfully",
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: true,
                            });
                            // alert("Event Created Successfully");
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            swal.fire('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            });

        $('#deleteMeeting').on('click', function () {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'error',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    // if (confirm("Are you sure you want to remove it?")) {
                    var id = $('#meetingForm').find('[name="id"]').val();
                    $.ajax({
                        url: "/admin/full-calender-meeting/store",
                        type: "POST",
                        data: {
                            id: id,
                            type: "delete"
                        },
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            $("#MeetingCalendar").modal("hide");
                            $('#meetingForm')[0].reset();
                            calendar.fullCalendar('refetchEvents');
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Congratulations!',
                                html: "Event Deleted Successfully",
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: true,
                            });
                            // alert("Event Deleted Successfully");
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            swal.fire('Oops...', 'Something went wrong!', 'error');
                        }
                    })
                }
            });
        });

        });

    </script>

@endsection
