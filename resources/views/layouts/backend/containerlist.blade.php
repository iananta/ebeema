<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{!! csrf_token() !!}"/>
    <title>Dashboard | {!! env('APP_NAME') !!}  @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 4.6.2 -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('backend/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('backend/bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- datatables -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <!--datepicker -->
    {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css"> --}}
    <!--end of date picker -->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('backend/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/dist/css/skins/_all-skins.min.css') }}">
    <!-- Full Calendar css -->
    <link rel="stylesheet" href="{{ asset('backend/css/fullcalendar.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- formValidation -->
    <link href="{{ asset('backend/js/formValidation/formValidation.min.css') }}" rel="stylesheet">

    <!-- sweet alert -->
    <link href="{{ asset('backend/js/sweetalert/dist/sweetalert2.min.css') }}" rel="stylesheet">

    <!-- Custom Style -->
    <link rel="stylesheet" href="{!! asset('css/backend-style.css') !!}"/>

    <!-- Custom css -->
    <link rel="stylesheet" href="{{ asset('backend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/responsive.css') }}">


    @yield('header_css')
    <style>
        .dataTables_scrollHead {
            overflow: auto !important;
        }

        /*Preloader*/
        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 999999;
        }

        #preloader #loader {
            height: 100%;
        }

        #loader .loader-wrapper {
            overflow: visible;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
        }

        #loader .loader-wrapper .loader-logo {
            overflow: visible;
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative;
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div id="preloader" style="display: none;">
    <div id="loader">
        <div class="loader-wrapper">
            <div class="loader-logo">
                <img class="bigsmall-animation" src="{{asset('images/Preload.gif')}}">
            </div>
        </div>
    </div>
</div>
<div class="wrapper">

    <!-- Top Bar -->
@include('layouts.backend.header')
<!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
    @include('layouts.backend.sidebar')
    <!-- #END# Left Sidebar -->

        <!-- Right Sidebar -->

        <!-- #END# Right Sidebar -->
    </section>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @include('layouts.backend.breadcrumb')
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('dynamicdata')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
@include('layouts.backend.footer')

<!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
   immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Google Translator -->
{{-- <script type="text/javascript">
function googleTranslateElementInit() {
new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ne'}, 'google_translate_element');
}
</script> --}}

<!-- CK Editor -->


<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<!-- datatable -->
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<!-- Nepali Datetime Js -->
<script src="{{asset('js\nepali.datepicker.v3.7.min.js')}}" type="text/javascript"></script>
<!-- date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- SlimScroll -->
<script src="{{ asset('backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('backend/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('backend/dist/js/demo.js') }}"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="{{ asset('backend/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>
<!-- sweet alert -->
<script src="{{ asset('backend/js/sweetalert/dist/sweetalert2.min.js') }}"></script>
<script src="{{ asset('backend/js/onload.js') }}"></script>
<!--ckeditor-->
<script src="{{asset('js/ckeditor.js')}}"></script>

<script src="{{asset('js/sweetalert2@11.js')}}"></script>
<script type="text/javascript">
    $('#notification-badge').hide();
    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var name = button.data('name')
        var is_active = button.data('is_active')
        var leadsource_id = button.data('leadsourceid')
        var modal = $(this)
        modal.find('.modal-body #name').val(name);
        modal.find('.modal-body #is_active').val(is_active);
        modal.find('.modal-body #leadsource_id').val(leadsource_id);
    })

    var url = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.sidebar-menu a').filter(function () {
        return this.href == url;
    }).parent().addClass('active');

    // for treeview
    $('ul.treeview-menu a').filter(function () {
        return this.href == url;
    }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');

    $(document).ready(function () {
        $("#show_hide_password a").on('click', function (event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("fa-eye-slash");
                $('#show_hide_password i').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("fa-eye-slash");
                $('#show_hide_password i').addClass("fa-eye");
            }
        });
        $("[data-toggle=tooltip").tooltip();
    });
</script>


@yield('footer_js')
<script type="text/javascript">
    $(document).ready(function () {
        let datatableWrapper = $('.dataTables_wrapper ')
        if (datatableWrapper) {
            let table = datatableWrapper.find('table')
            table.wrapAll('<div class="custom_datatable_wrapper"></div>')
        }
    });

    Pusher.logToConsole = true;

    var pusher = new Pusher('98d64f329997c84ca25c', {
        cluster: 'ap2'
    });

    var channel = pusher.subscribe('user.{{auth()->user()->id}}');
    channel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
        $('#notification-badge').show();
        $('#realtime-notification').prepend(`<div class="dropdown-item c-dropdown"><div class="text-sm"> ${data.message} </div></div>`)
    });


    pusher.subscribe('notify-channel');
    channel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
        $('#notification-badge').show();
        $('#realtime-notification').prepend(`<div class="dropdown-item c-dropdown"><div class="text-sm"> ${data.message} </div></div>`)
    });


    if ($('#ckeditor').length > 0) {
        ClassicEditor
        .create( document.querySelector( '#ckeditor' ) )
        .catch( error => {
            console.error( error );
        } );
    }

    if ($('.ckeditor').length > 0) {
        ClassicEditor
            .create(document.querySelector('.ckeditor'))
            .catch(error => {
                console.error(error);
            });
    }
    $('.treeview-menu .treeview').each(function(){
        $(this).find('.treeview-menu').each(function(){
            if(!$(this).hasClass('active')){
                $(this).css('display','none')
            }
        })
    })
</script>

</body>

</html>
