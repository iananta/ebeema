<!-- jQuery 3 -->
<script src="{{ asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/dist/js/adminlte.min.js') }}"></script>
   <!-- datatable -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
     <script src="{{ asset('backend/js/sweetalert/dist/sweetalert2.min.js') }}"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<script>
    // console.log('footer')
   //  Enable pusher logging - don't include this in production
   //  Pusher.logToConsole = true;

    var pusher = new Pusher('98d64f329997c84ca25c', {
        cluster: 'ap2'
    });

    var channel = pusher.subscribe('user.{{auth()->user()->id}}');
    channel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', function(data) {
        $('#realtime-notification').prepend(`<div class="dropdown-item c-dropdown"><div class="text-sm"> ${data.message} </div></div>`)
    });
</script>
