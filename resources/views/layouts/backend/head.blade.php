<head>
    <link rel="favicon icon" type="image/x-icon" href="{{ asset('images/Ebeema-favicon.png') }}">
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Dashboard | {!! env('APP_NAME') !!}</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
      <!-- Ionicons -->
      <link rel="stylesheet" href="{{ asset('backend/bower_components/Ionicons/css/ionicons.min.css') }}">
      <!-- jvectormap -->
      <link rel="stylesheet" href="{{ asset('backend/bower_components/jvectormap/jquery-jvectormap.css') }}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('backend/dist/css/AdminLTE.min.css') }}">
      <link rel="stylesheet" href="{{ asset('frontend/css/fonts.css') }}">
      <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
            page. However, you can choose any other skin. Make sure you
            apply the skin class to the body tag so the changes take effect. -->
      <link rel="stylesheet" href="{{ asset('backend/dist/css/skins/skin-blue.min.css') }}">
      <!-- datatables -->
          <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
          <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
          <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <!-- Google Font -->
      <link rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

      <!-- Custom Style -->
       {{-- <link rel="stylesheet" href="{!! asset('css/backend-style.css')!!}" />  --}}

       <!-- Custom css -->

       <link rel="stylesheet" href="{{ asset('backend/css/custom.css') }}">
       <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/responsive.css') }}">
       <link href="{{ asset('backend/js/sweetalert/dist/sweetalert2.min.css') }}" rel="stylesheet">
  </head>
