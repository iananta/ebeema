<div class="fixed">
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu thin-scroll" style="height: 100vh; overflow-x: hidden; overflow-y: scroll;"
                data-widget="tree">
                {{-- Dashboard --}}
                <li>
                    <a href="{{ route('dashboard') }}" id="dashboard">
                        <i class="fa fa fa-tachometer"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                {{-- Leads ( List of Customers ) of Respective User Related --}}
                @if(control('marketing-leads'))
                    <li>
                        <a href="{{ route('admin.leads.marketing') }}">
                            <i class="fa fa-user-circle"></i>
                            <span>Marketing Leads</span>
                        </a>
                    </li>
                @endif
                @if(control('create-leads'))
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-user-circle"></i>
                            <span>Leads</span>

                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>

                        </a>

                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('admin.leads.customers') }}">
                                    <i class="fa fa-users"></i>
                                    <span>Customers</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.leads.index') }}">
                                    <i class="fa fa-user-circle"></i>
                                    <span>All Leads</span>
                                </a>
                            </li>
                            {{-- Calendar --}}
                            @if(control('premium-info'))
                                <li>
                                    <a href="{{route('admin.permiumpaid')}}">
                                        <i class="fa fa-user"></i>
                                        <span>Premium Collection</span>
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ route('admin.calender.meeting') }}">
                                    <i class="fa fa-calendar-o"></i>Calendar
                                </a>
                            </li>

                            {{-- End of Calendar --}}
                        </ul>
                    </li>
                @endif
                {{-- End of Leads ( List of Customers ) of Respective User Related --}}
                {{-- Life Insurance --}}
                @if (control('life-calculation'))
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-calculator"></i>
                            <span>Life Calculations</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>

                        <ul class="treeview-menu">
                            {{--                            Compare Policy--}}
                            <li>
                                <a href="{{ route('admin.policy.form') }}">
                                    <i class="fa fa-calculator"></i>
                                    <span>Compare Policy</span>
                                </a>
                            </li>
                            {{--                            end of Compare Policy--}}

                            {{--                          life calculator  --}}
                            <li>
                                <a href="{{ route('calculator.life.calculate') }}">
                                    <i class="fa fa-calculator"></i>
                                    <span>Life Calculator</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('life.claim') }}">
                                    <i class="fa fa-calculator"></i>
                                    <span>Claims</span>
                                </a>
                            </li>
                            {{--                            end of life calculator--}}

                        </ul>
                    </li>
                @endif

                {{--                            selected plans--}}
                @if (control('selected-plans'))
                    <li>
                        <a href="{{ route('selected.plans') }}">
                            <i class="fa fa-calculator"></i>
                            <span>User Selected Plans</span>
                        </a>
                    </li>
                @endif
                {{--                            end of selected plans--}}
                {{-- Non Life Calculator --}}
                {{--                    {{dd(AgentCat())}}--}}
                @if (control('non-life-calculation'))
                    @if(in_array('NonLife',AgentCat()) || in_array('all',AgentCat()))
                        {{-- Life Insurance --}}
                        <li class="treeview">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="fa fa-calculator"></i>
                                <span>Non Life Calculations</span>

                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>

                            </a>

                            <ul class="treeview-menu">
                                <li class="treeview">
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="fa fa-calculator"></i>
                                        <span>Non Life Calculator</span>

                                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>

                                    </a>

                                    <ul class="treeview-menu">
                                        <li>
                                            <a href="{{ route('nonLife.calculator.bike') }}">
                                                <i class="fa fa-motorcycle"> </i> Motor Cycle
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('nonLife.calculator.private.motor') }}">
                                                <i class="fa fa-car"> </i> Private Vehicle
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('nonLife.calculator.commercial.motor') }}">
                                                <i class="fa fa-bus"> </i> Commercial Vehicle
                                            </a>
                                        </li>
                                        @if(control('travel-calculation'))
                                            <li>
                                                <a href="{{ route('travel.calculator.insurance.index') }}">
                                                    <i class="fa fa-plane"> </i> Travel Medical Insurance
                                                </a>
                                            </li>
                                        @endif
                                        @if (control('policy-renewal'))
                                            <li>
                                                <a href="{{route('policy.renew.customer.index')}}">
                                                    <i class="fa fa-refresh"></i>
                                                    <span>Policy Renewal</span>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                                <li class="treeview">
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="fa fa-list"></i>
                                        <span>Policies</span>

                                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li>
                                            <a href="{{route('nonLife.calculator.policy.view')}}">
                                                <i class="fa fa-link"></i>
                                                <span>All Policies </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('nonLife.calculator.draft.policy.view')}}">
                                                <i class="fa fa-link"></i>
                                                <span>Draft Policies</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{route('travel.calculator.tmi.policy.view')}}">
                                                <i class="fa fa-link"></i>
                                                <span>TMI Policies </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                @if (control('ime-life'))
                                    @if(in_array('Life',AgentCat()) || in_array('all',AgentCat()))
                                        <li class="">
                                            <a href="{{ route('admin.ime.life.insurance') }}">
                                                <i class="fa fa-calculator"></i>
                                                <span>IME Life</span>
                                            </a>
                                        </li>
                                        {{-- End of Life Calculator --}}
                                    @endif
                                @endif
                                @if (control('nonlife-claim'))
                                    @if(in_array('Life',AgentCat()) || in_array('all',AgentCat()))
                                        <li class="">
                                            <a href="{{ route('nonLife.calculator.claim.kyc') }}">
                                                <i class="fa fa-calculator"></i>
                                                <span>Claims</span>
                                            </a>
                                        </li>
                                        {{-- End of Life Calculator --}}
                                    @endif
                                @endif
                            </ul>
                        </li>
                    @endif

                @endif
                {{-- End of Non Life Calculator --}}


                {{-- Admin Settings --}}

                <li class="treeview">
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="fa fa-cogs"></i>
                        <span>Settings</span>

                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('user.profile.edit') }}">
                                <i class="fa fa fa-user"></i>
                                <span>Profile Management</span>
                            </a>
                        </li>
                        @if(control('frontend-dynamics'))
                        <li>
                            <a href="{{ route('admin.support.index') }}">
                                <i class="fa fa fa-user"></i>
                                <span>Backend address</span>
                            </a>
                        </li>
                        @endif
                        @if(control('page-seo-settings'))
                            <li>
                                <a href="{{route('admin.seoForm')}}"> <i class="fa fa fa-info"></i>
                                    <span>Seo Form</span></a>
                            </li>
                        @endif
                        @if(control('frontend-dynamics'))
                            <li>
                                <a href="{{ route('sms.index') }}">
                                    <i class="fa fa fa-building"></i>
                                    <span>Sms Management</span>
                                </a>
                            </li>
                        @endif

                        {{-- Company Management --}}
                        @if(control('create-company'))
                            <li>
                                <a href="{{ route('admin.companies.index') }}">
                                    <i class="fa fa fa-building"></i>
                                    <span>Company Management</span>
                                </a>
                            </li>
                        @endif
                        {{-- End of Company Management --}}

                        {{-- Category Management --}}
                        @if(control('create-product'))
                            <li>
                                <a href="{{ route('admin.category') }}">
                                    <i class="fa fa-th-large"></i>
                                    <span>Product Category</span>
                                </a>
                            </li>
                        @endif

                        {{-- Product Management --}}
                        @if(control('create-product'))
                            <li>
                                <a href="{{ route('admin.product.list') }}">
                                    <i class="fa fa-th-large"></i>
                                    <span>Product Management</span>
                                </a>
                            </li>
                        @endif

                        {{-- Lead Related --}}
                        @if(control('create-leads'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-tags"></i>
                                    <span>Lead Settings</span>

                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                </a>

                                <ul class="treeview-menu">
                                    <li>
                                        <a href="{{ route('admin.leadcategories.leadsource.index') }}">
                                            Lead Source
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.leadcategories.leadtypes.index')}}">
                                            Lead Types
                                        </a>
                                    </li>
                                </ul>

                            </li>
                        @endif
                        {{-- End of Lead Related --}}

                        {{-- User Management --}}
                        @if(control('create-user'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-users"></i>
                                    <span>User Management</span>

                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                </a>

                                <ul class="treeview-menu">
                                    <li>
                                        <a href="{{ route('admin.privilege.role.index') }}">
                                            <i class="fa fa-users"></i>Role
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.privilege.permission.index')}}">
                                            <i class="fa fa fa-cog"></i>
                                            <span>Permission</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.privilege.user.index') }}">
                                            <i class="fa fa-user"></i>User
                                        </a>
                                    </li>
                                </ul>

                            </li>
                        @endif
                        {{-- End of User Management --}}

                        @if(control('non-life-calculator-settings'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-cogs"></i>
                                    <span>Non Life Calculator Settings</span>

                                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                                </a>
                                <ul class="treeview-menu">

                                    <li>
                                        <a href="{{ route('motor.manufacture.company.list') }}">
                                            <i class="fa fa fa-credit-card-alt"></i>
                                            <span>Maufactures</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('motor.manufacture.company.models') }}">
                                            <i class="fa fa fa-credit-card-alt"></i>
                                            <span>Models</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if(control('life-calculator-inputs'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-cogs"></i>
                                    <span>Life Calculator Settings</span>

                                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                                </a>
                                <ul class="treeview-menu">

                                    <li>
                                        <a href="{{ route('admin.crcrate.create') }}">
                                            <i class="fa fa fa-credit-card-alt"></i>
                                            <span>CRC Rate</span>
                                        </a>
                                    </li>
                                    {{-- End of Loading Charge --}}

                                    {{-- Discount on SA --}}
                                    <li>
                                        <a href="{{ route('admin.discount.list') }}">
                                            <i class="fa fa-percent"></i>Discount on SA
                                        </a>
                                    </li>
                                    {{-- End of Discount on SA --}}

                                    {{-- Bonus --}}
                                    <li>
                                        <a href="{{ route('admin.bonus.list') }}">
                                            <i class="fa fa-dollar"></i>Bonus
                                        </a>
                                    </li>
                                    {{-- End of Bonus --}}

                                    {{-- Loading Charge --}}
                                    <li>
                                        <a href="{{ route('admin.loadingcharges.create') }}">
                                            <i class="fa fa fa-credit-card-alt"></i>
                                            <span>Mode of Payment</span>
                                        </a>
                                    </li>
                                    {{-- End of Loading Charge --}}

                                    {{--                                        Features--}}
                                    @if (control('life-calculator-inputs'))


                                        <li class="treeview">
                                            <a href="javascript:void(0);" class="menu-toggle">
                                                <i class="fa fa-pause-circle"></i>
                                                <span>Features </span>
                                                <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                      </span>

                                            </a>

                                            <ul class="treeview-menu">
                                                <li>
                                                    <a href="{{ route('admin.feature.list') }}">
                                                        Feature Management
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('admin.feature.product')}}">
                                                        Feature Products
                                                    </a>
                                                </li>
                                            </ul>

                                        </li>
                                    @endif


                                    {{--                                        end of features--}}

                                    {{-- Payback Schedule --}}
                                    <li>
                                        <a href="{{ route('admin.payback.index') }}">
                                            <i class="fa fa fa-money"></i>
                                            <span>Payback Schedule</span>
                                        </a>
                                    </li>
                                    {{-- End of Payback Schedule --}}

                                    {{-- Couple Age Difference --}}
                                    <li>
                                        <a href="{{ route('admin.age-difference.index') }}">
                                            <i class="fa fa fa-minus"></i>
                                            <span>Couple's Age Difference</span>
                                        </a>
                                    </li>
                                    {{-- End of Couple Age Difference --}}

                                    {{-- Paying Term --}}
                                    <li>
                                        <a href="{{ route('admin.paying-term.index') }}">
                                            <i class="fa fa fa-money"></i>
                                            <span>Policy Paying Term</span>
                                        </a>
                                    </li>
                                    {{-- End of Paying Term --}}

                                    <li>
                                        <a href="{{route('admin.api-key.index')}}">
                                            <i class="fa fa fa-key"></i>
                                            <span>Api Key</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        {{-- Product Data Management --}}
                        @if(control('rate-import'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-table"></i>
                                    <span>Rate Table</span>

                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                </a>

                                <ul class="treeview-menu">

                                    <li>
                                        <a href="{{ route('rate.feature.index') }}">
                                            <i class="fa fa-calendar-o"></i>Product Features rate
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('endowment.rate.index') }}">
                                            <i class="fa fa-calendar-o"></i>Table rate
                                        </a>
                                    </li>
                                </ul>

                            </li>
                        @endif
                        {{-- End of Product Data Management --}}

                        {{-- {{UserActivitylogs}} --}}
                        @if(control('access-logs'))
                            <li>
                                <a href="{{route('user.activity')}}">
                                    <i class="fa fa-th-large"></i>
                                    <span>User Activity</span>
                                </a>
                            </li>
                        @endif

                        {{-- End of UserActivityLogs --}}

                        {{-- Policy Categories  --}}
                        @if(control('create-policy'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-pause-circle"></i>
                                    <span>Policy Category</span>

                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                </a>

                                <ul class="treeview-menu">
                                    <li>
                                        <a href="{{ route('admin.policycategories.sub.index') }}">
                                            Policy Sub Categories
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.policycategories.type.index')}}">
                                            Policy Types
                                        </a>
                                    </li>
                                </ul>

                            </li>
                        @endif
                        {{-- End of Policy Categories  --}}
                        {{-- Mail Template --}}
                        @if(control('mail-template'))
                            <li>
                                <a href="{{ route('admin.mail.index') }}">
                                    <i class="far fa fa-envelope"></i>
                                    <span>Mail Template</span>
                                </a>
                            </li>
                        @endif
                        {{-- End of Mail Template --}}
                        {{-- sms Template --}}
                        @if(control('sms-template'))
                            <li>
                                <a href="{{ route('admin.smsTemplate.index') }}">
                                    <i class="fa fa-commenting-o"></i>
                                    <span>SMS Template</span>
                                </a>
                            </li>
                        @endif
                        {{-- End of sms Template --}}

                        {{-- SEO settings     --}}
                        @if (control('page-seo-settings'))


                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-pause-circle"></i>
                                    <span>S.E.O </span>
                                    <span class="pull-right-container">
                                                        <i class="fa fa-angle-left pull-right"></i>
                                                      </span>

                                </a>

                                <ul class="treeview-menu">
                                    <li>
                                        <a href="{{ route('meta.index') }}">
                                            <i class="fa fa fa-bullhorn"></i>
                                            <span>SEO Metas</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{route('admin.seoForm')}}"> <i class="fa fa fa-info"></i>
                                            <span>Seo Form</span></a>
                                    </li>
                                </ul>

                            </li>
                        @endif
                        {{--   end of seo settings   --}}

                        {{-- Frontend Settings --}}

                        @if(control('frontend-dynamics'))
                            <li class="treeview">
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <i class="fa fa-tasks"></i>
                                    <span>Frontend Settings</span>

                                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>

                                </a>

                                <ul class="treeview-menu">
                                    {{-- Home Page --}}
                                    <li class="treeview">
                                        <a href="javascript:void(0);" class="menu-toggle">
                                            <i class="fa fa-home"></i>
                                            <span>Home Page</span>

                                            <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                        </a>

                                        <ul class="treeview-menu">
                                            <li>
                                                <a href="{{route('admin.banner.index')}}">
                                                    <i class="fa fa-photo"></i>
                                                    <span>Banner</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('why-us.index') }}">
                                                    <i class="fa fa-handshake-o"></i>
                                                    <span>Why Us</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('why-different.index') }}">
                                                    <i class="fa fa-th-large"></i>
                                                    <span>Why Different</span>
                                                </a>
                                            </li>
                                            {{-- Blog Category --}}
                                            <li>
                                                <a href="{{ route('blogcates.index') }}">
                                                    <i class="fa fa-list-alt"></i>
                                                    <span>Blog Category</span>
                                                </a>
                                            </li>
                                            {{-- End of Blog Category --}}

                                            {{-- Blog Post --}}
                                            <li>
                                                <a href="{{ route('blogs.index') }}">
                                                    <i class="fa fa-telegram"></i>
                                                    <span>Blog Post</span>
                                                </a>
                                            </li>
                                            {{-- End of Blog Management --}}
                                            <li>
                                                <a href="{{ route('testimonial.index') }}">
                                                    <i class="fa fa-quote-left"></i>
                                                    <span>Testimonials</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('association.index') }}">
                                                    <i class="fa fa-handshake-o"></i>
                                                    <span>Our Association</span>
                                                </a>
                                            </li>
                                        </ul>

                                    </li>
                                    {{-- End of Home Page --}}

                                    {{-- About Us Page --}}
                                    <li class="treeview">
                                        <a href="javascript:void(0);" class="menu-toggle">
                                            <i class="fa fa-info-circle"></i>
                                            <span>About Us Page</span>

                                            <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                        </a>

                                        <ul class="treeview-menu">
                                            <li>
                                                <a href="{{ route('about-us.index') }}">
                                                    <i class="fa fa-info"></i>
                                                    <span>About Us</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ route('values.index') }}">
                                                    <i class="fa fa-handshake-o"></i>
                                                    <span>Core Values</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ route('team.index')}}">
                                                    <i class="fa fa-users"></i>
                                                    <span>Teams</span>
                                                </a>
                                            </li>

                                        </ul>

                                    </li>
                                    {{-- End of About Us Page --}}

                                    {{-- Contact Us Page --}}
                                    <li class="treeview">
                                        <a href="javascript:void(0);" class="menu-toggle">
                                            <i class="fa fa-phone-square"></i>
                                            <span>Contact Us Page</span>

                                            <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>

                                        </a>

                                        <ul class="treeview-menu">
                                            <li>
                                                <a href="{{ route('message.index') }}">
                                                    <i class="fa fa fa-envelope"></i>
                                                    <span>Client's Messages</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ route('general-setting.contact') }}">
                                                    <i class="fa fa fa-cog"></i>
                                                    <span>Contact Settings</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    {{-- End of Contact Us Page --}}

                                    {{-- Social Media --}}
                                    <li>
                                        <a href="{{ route('social-link.index') }}">
                                            <i class="fa fa fa-link"></i>
                                            <span>Social Media Links</span>
                                        </a>
                                    </li>
                                    {{-- End of Social Media --}}
                                    {{--   other settings     --}}
                                    <li>
                                        <a href="{{ route('page-setting.index') }}">
                                            <i class="fa fa fa-cog"></i>
                                            <span>Other Settings</span>
                                        </a>
                                    </li>
                                    {{--                                     end of other settings   --}}
                                </ul>
                            </li>
                        @endif
                        {{-- End of Frontend Settings --}}

                    </ul>
                </li>

                {{-- End of Admin Settings --}}

                {{-- General Settings --}}
                @if(control('general-settings'))
                    <li>
                        <a href="{{ route('general-setting.index') }}">
                            <i class="fa fa fa-cog"></i>
                            <span>General Settings</span>
                        </a>
                    </li>
                @endif
                {{-- End of General Settings --}}

                {{-- User Settings --}}
                @if(control('general-settings'))

                @endif
                {{-- End of General Settings --}}
                {{-- Calendar Settings --}}
                @if(control('calendar-access'))
                    <li>
                        <a href="{{ route('user.calendar.index') }}">
                            <i class="fa fa fa-calendar"></i>
                            <span>Calendar Settings</span>
                        </a>
                    </li>
                @endif
                {{-- End of Calendar Settings --}}
                <!-- Support -->
                @if(control('create-upload'))
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-user-circle"></i>
                            <span>Support</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                        @if(auth()->user()->role_id == 1)
                        <li>
                        <a href="{{ route('admin.product.uploadFile') }}">
                            <i class="fa fa fa-download"></i>
                            <span>Download</span>
                        </a>
                         </li>
                         @endif
                            <li class="treeview">
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="fa fa-list"></i>
                                        <span>Upload</span>

                                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li>
                                        <a href="{{ route('admin.videoupload.index') }}">
                                                <i class="fa fa-link"></i>
                                                <span>File </span>
                                            </a>
                                        </li>
                                        <li>
                                        <a href="{{ route('admin.tape.index') }}">
                                                <i class="fa fa-link"></i>
                                                <span>Video</span>
                                            </a>
                                        </li>
                                      
                                    </ul>
                                </li>
                        </ul>
                    </li>
                    
                @endif
                {{-- old menu --}}
                <br>
                <br>
                <br>
                <br>
                <hr>
            </ul>

            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
</div>
