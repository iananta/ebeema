@extends('layouts.backend.containerlist')

@section('title')
    Meta-data Add
@endsection

@section('dynamicdata')
    <div class="box">
        <div class="box-body">
            @include('layouts.backend.alert')
            <div class="policy-compare-wrapper">
                <div class="form-wrapper">
                    <form class="form-data" method="POST" action="{{ route('meta.store') }}">
                        @csrf

                        <div class="custom-row">
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Page Name</label>
                                </div>
                                <div class="form-input">
                                    <input type="text" class="form-control" id="page_name" name="page_name"
                                           placeholder="Enter page name" value="{{ old('page_name') }}">
                                </div>
                            </div>

                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Meta Title</label>
                                </div>
                                <div class="form-input">
                                    <input type="text" class="form-control" id="meta_title" name="meta_title"
                                           placeholder="Enter meta title" value="{{ old('meta_title') }}">
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Meta Description</label>
                                </div>
                                <div class="form-input">
                                    <textarea
                                        class="form-control"
                                        name="meta_description"
                                        id="meta_description"
                                        placeholder="Enter meta description"
                                        value="{{ old('meta_description') }}"
                                    ></textarea>
                                </div>
                            </div>

                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Note</label>
                                </div>
                                <div class="form-input">
                                    <textarea
                                        class="ckeditor form-control"
                                        name="note"
                                        id="note"
                                        placeholder="Enter the note"
                                        value="{{ old('note') }}"
                                    ></textarea>
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Status</label>
                                </div>
                                <div class="form-input">
                                    <select class="input-control search-select  validate-check" id="status" name="status">
                                        <option value="1">Active</option>
                                        <option value="0">Deactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-input-wrapper">
                                <div class="form-label"></div>
                                <div class="form-input">
                                    <button type="submit" id="submit-btn" class="form-submit-btn">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-input-details-wrapper">

                </div>
            </div>
        </div>
    </div>
@endsection
