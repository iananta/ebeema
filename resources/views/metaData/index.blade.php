@extends('layouts.backend.containerlist')

@section('footer_js')
    <script type="text/javascript">
        $(function () {
            $('#meta-table').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        });
        $('#tablebody').on('click', '.change-status', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You are going to change the status!",
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, change it!'
            }).then(function (response) {
                if(response.isConfirmed == true) {
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/admin/meta/status/') }}" + "/" + id,
                        data: {
                            'id': id,
                            _method: 'post',
                            '_token': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('#preloader').show();
                        },
                        success: function (response) {
                            $('#preloader').hide();
                            swal.fire('Success', response.message, 'success');
                            if (response.meta.status == 1) {
                                $($object).children().removeClass('bg-red').html(
                                    '<i class="fa fa-ban"></i> Not Active');
                                $($object).children().addClass('bg-blue').html(
                                    '<i class="fa fa-check"></i> Active');
                                $($object).attr('title', 'Deactivate');
                            } else {
                                $($object).children().removeClass('bg-blue').html(
                                    '<i class="fa fa-check"></i> Active');
                                $($object).children().addClass('bg-red').html(
                                    '<i class="fa fa-ban"></i> Not Active');
                                $($object).attr('title', 'Activate');
                            }
                        },
                        error: function (e) {
                            $('#preloader').hide();
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });

    </script>
@endsection

@section('title')
    Meta-Data Settings
@endsection

@section('dynamicdata')


    <div class="box">
        <div class="box-header with-border c-btn-right d-flex-row ">
            <div class="justify-content-end list-group list-group-horizontal ">
                <a href="{{route('meta.create')}}">
                    <button class="btn btn-primary btn-sm c-primary-btn add-modal shadow-none mx-2">
                        <img src="{{ asset('uploads/add-circle-16-Regular.svg') }}" alt="Add-icon"> &nbsp;
                        Add New Mail &nbsp;
                    </button>
                </a>
            </div>
        </div>

        <div class="box-body">
            <table id="meta-table" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S.No.</th>
                    <th>Page Name</th>
                    <th>Meta Title</th>
                    <th>Meta Description</th>
                    <th>Note</th>
                    <th>Status</th>
                    <th class="dt-center">Actions</th>
                </tr>
                </thead>
                <tbody id="tablebody">
                @foreach($metas as $index=>$meta)
                    <tr class="gradeX" id="row_{{ $meta->id }}">
                        <td class="index">
                            {{ ++$index }}
                        </td>
                        <td class="page_name">
                            {{ $meta->page_name }}
                        </td>
                        <td class="meta_title">
                            {{ $meta->meta_title }}
                        </td>
                        <td class="meta_description text-wrap">
                            {!! $meta->meta_description !!}
                        </td>
                        <td class="note">
                            {!! $meta->note !!}
                        </td>
                        <td class="status">
                            @if($meta->status == 1)
                                <a href="javascript:;" class="change-status" title="Deactivate" id="{{ $meta->id }}">
                                    <button type="button"
                                            class="btn btn-sm bg-blue btn-circle waves-effect waves-circle waves-float">
                                        <i class="fa fa-check"></i> Active
                                    </button>
                                </a>
                            @else
                                <a href="javascript:;" class="change-status" title="Activate" id="{{ $meta->id }}">
                                    <button type="button"
                                            class="btn btn-sm bg-red btn-circle waves-effect waves-circle waves-float">
                                        <i class="fa fa-ban"></i> Not Active
                                    </button>
                                </a>
                            @endif
                        </td>

                        <td class="justify-content-center d-flex p-2">
                            <a href="{{route('meta.edit',$meta->id)}}" id="{{ $meta->id }}"
                               title="Edit meta"><button class="btn btn-primary btn-flat"><i
                                        class="fa fa-edit"></i></button></a>&nbsp;
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

@stop

