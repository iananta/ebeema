@extends('layouts.backend.containerlist')

@section('footer_js')
   @include('Backend.globalScripts.form-validation')
@endsection
@section('title')
    Create Money Back Schedule
@endsection

@section('dynamicdata')

<div class="box">
     <div class="box-body">
         @include('layouts.backend.alert')
        <div class="policy-compare-wrapper">
                <div class="form-wrapper">
                    <form class="form-data" method="POST" action="{{ route('admin.payback.store') }}" >
                        @csrf
                        <div class="custom-row">
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Term Year<label>
                                </div>
                                <div class="form-input">
                                    <input data-validation="required" class="input-control" name="term_year" size="16" type="text" placeholder="Term Year" />
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Payback Year</label>
                                </div>
                                <div class="form-input">
                                   <input data-validation="required" class="input-control" name="payback_year" size="16" type="text" placeholder="Payback Year" />
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Rate</label>
                                </div>
                                <div class="form-input">
                                    <input data-validation="required" class="input-control" name="rate" size="16" type="text" placeholder="Rate"/>
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Company</label>
                                </div>
                                <div class="form-input">
                                    <select class="input-control" name="company_id" data-validation="required">
                                        <option value="">Select Company</option>
                                        @foreach ($companies as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                <div class="form-label">
                                    <label>Product</label>
                                </div>
                                <div class="form-input">
                                    <select class="input-control" name="product_id" data-validation="required">
                                        <option value="">Select Product</option>
                                        @foreach ($products as $prod)
                                            <option value="{{ $prod->id }}">{{ $prod->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-input-wrapper">
                                    <div class="form-label"></div>
                                <div class="form-input">
                                    <button type="submit" id="submit-btn" class="form-submit-btn">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
     </div>
</div>
@endsection
