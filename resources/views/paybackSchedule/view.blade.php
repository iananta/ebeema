@extends('layouts.backend.containerlist')
@section('footer_js')
    {!! $dataTable->scripts() !!}
    <script>
        $(document).on('click','.payback-create-btn',function(){
            window.location= '{{route("admin.payback.create")}}'
        })
        $(document).on('submit','.filter-form',function(e){
            e.preventDefault()
            let formData=$(this).serializeArray();
            let table=$('.dataTable').attr('id')
            $('#'+table).on('preXhr.dt',function(e,setting,data){
                data.customFilter=formData
            }).data({
                "serverSide":true,
                "processing":true,
                "ajax":{
                    url: "{{url('admin/paybackSchedule')}}",
                    type:"GET"
                }
            })
            LaravelDataTables.paybackscheduledatatableTable.ajax.reload()

        })
        $(document).on('click','.filter-reset',function(e){
            e.preventDefault()
            let table=$('.dataTable').attr('id')
            $('.filter-form')[0].reset()
             $('#'+table).on('preXhr.dt',function(e,setting,data){
                data.customFilter=[]
            }).data({
                "serverSide":true,
                "processing":true,
                "ajax":{
                    url: "{{url('admin/leads')}}",
                    type:"GET"
                }
            })
            LaravelDataTables.paybackscheduledatatableTable.ajax.reload()
        })
        $(document).on('click','.apply-filter',function(){
                let filterBlock=$('.filter-block')
                filterBlock.toggleClass('active')
                if(filterBlock.hasClass('active')){
                    filterBlock.slideDown();
                    $(this).html('<i class="fa fa-filter"> </i>&nbsp;Cancle Filter')
                }else{
                     filterBlock.slideUp();
                     $(this).html('<i class="fa fa-filter"> </i>&nbsp;Apply Filter')
                }
        })
        $(document).on('click', '.delete-payment', function (e) {
            e.preventDefault();
            $object = $(this);
            var id = $object.attr('id');
            new swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (response) {
                if (response.isConfirmed == true) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ url('/admin/paybackSchedule/delete') }}" + "/" + id,
                        dataType: 'json',
                        success: function (response) {
                            var nRow = $($object).parents('tr')[0];
                            new swal(
                                'Success',
                                response.message,
                                'success'
                                )
                            $object.parent().parent().remove()
                        },
                        error: function (e) {
                            new swal('Oops...', 'Something went wrong!', 'error');
                        }
                    });
                }
            }).catch(swal.noop);
        });

            
    </script>

@endsection
@section('title')
    Payment Money Back Schedule
@endsection

@section('dynamicdata')

<div class="box">
     <div class="box-body">
            <div class="filter-block leads-filter">
                <div class="filter-wrapper">
                    <div class="filter-heading">
                        <h1>Filter</h1>
                    </div>
                    <form class="filter-form">
                        <div class="filter-form-wrapper">
                            <div class="form-filter-item">
                                <label>Term Year</label>
                                <select class="filter-input" name="term_year">
                                    <option value="">Select One</option>
                                    @for($i=1;$i<=16;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-filter-item">
                                <label>Company</label>
                                <select class="filter-input" name="company_id">
                                    <option value="">Select One</option>
                                    @foreach ($companies as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                     @endforeach
                                </select>
                            </div>
                            <div class="form-filter-item">
                                <label>Product</label>
                                 <select class="filter-input" name="product_id" >
                                        <option value="">Select One</option>
                                        @foreach ($products as $prod)
                                            <option value="{{ $prod->id }}">{{ $prod->name }}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="form-filter-item filter-button-wrapper">
                                <button  class="filter-submit">Filter</button>
                            </div>
                            <div class="form-filter-item filter-button-wrapper">
                                <button type="reset"  class="filter-reset">Reset</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
         {!! $dataTable->table() !!}
     </div>
</div>
@endsection