@extends('frontend.layouts.app')
@section('content')
<section id="subscription-section">
    <div class="container">
    	<div class="subscription-payment-wrapper">

          <div class="payment-block">
            <div class='payment-block-bg'>
                <div class="payment-title">
                    <h1>Your Subscription will be start from {{date('Y/M/d')}}</h1>
                </div>

                <div class="payment-info-wrap">
                    <div class="payment-premium-info">
                        <ul class="payment-premium-ul">
                            <li><strong>Full Name</strong><span
                                >{{ucfirst(auth()->user()->username)}}</span></li>
                            <li><strong>Phone Number</strong><span>{{auth()->user()->phone_number}}</span></li>
                             <li><strong>Phone Number</strong><span>{{auth()->user()->email}}</span></li>
                            <li><strong>Ref Id</strong><span class="refId
                                ">{{$paymentInfo->RefId}}</span></li>
                                <li><strong>Amount</strong><span>Rs {{$amount}}</span></li>
                            </ul>
                        </div>
                    </div>

                    <form action="{{$ime_pay_checkout_url}}" method="post">
                        <div class="payment-info-wrap">
                            <div class="payment-premium-info">
                                <div class="total-need-pay">
                                    <h2>You Pay</h2>
                                    <p class="pay-amount-nonlife">Rs {{$amount}}</p>
                                </div>
                                <div class="payment-term-condition-wrapper">
                                    <input type="checkbox" name="condition" required>
                                    <label>I agree to the <a href="">Terms & Condition</a></label>
                                </div>
                            </div>
                        </div>
                        <div class="pay-button-wrapper">
                             <input type="hidden" name="TokenId" value="{{$paymentInfo->TokenId}}">
                             <input type="hidden" name="MerchantCode" value="{{$merchantCode}}">
                             <input type="hidden" name="RefId" value="{{$paymentInfo->RefId}}">
                             <input type="hidden" name="TranAmount" value="{{$amount}}">
                             <input type="hidden" name="Method" value="GET">
                             <input type="hidden" name="RespUrl" value="{{route('subscription.payment')}}">
                             <input type="hidden" name="CancelUrl" value="{{route('subscription.cancel')}}">
                         <button class="pay-button" type="submit"><i class="fa fa-lock"></i>&nbsp;&nbsp;Pay Securely</button>
                     </form>
                 </div>
             </div>
         </div>
     </div>   

 </div>
</div>
</section>
@endsection