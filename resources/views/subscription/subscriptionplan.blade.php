@extends('frontend.layouts.app')
@section('content')
<section id="subscription-section">
	<div class="contact_banner_container visible-lg-block">
        <div class="contact_banner">
            <h1>Subscription Plan</h1>
        </div>
    </div>
    <div class="container">
    	<div class='subscription-plan-wrapper'>
    		<div class="subscription-plan-items">
                @foreach($subscriptionplan as $plan)
    			<div class="subscription-plan-item">
    				<div class="subscription-plan-item-wrapper">
                        <div class='subscription-top-wrapper'>
                            <div class="subscription-top-bg"></div>
                            <div class="subscription-icon">
                                <span><i class="fa fa-rocket"></i></span>
                            </div>
                        </div>
    					<div class="subscription-description-wrapper">
    						<div class="subscription-heading">
                                <h1>{{$plan->title}}</h1>                  
                            </div>
                            <div class="suscription-price-wrapper">
                                <span class="suscription-price">Rs {{$plan->price}}</span>
                            </div>
                            <div class="subscription-pay-button-wrapper">
                                <form action="{{route('subscription.invoice')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="plan_id" value="{{$plan->id}}">
                                    <input type="hidden" name="duration" value="{{$plan->period_in_month}}">
                                    <input type="hidden" name="amount" value="{{$plan->price}}">
                                    <button type="submit">Select Plan</button>
                                </form>
                            </div>
    					</div>
    				</div>
    			</div>
                @endforeach
                
    		</div>
    	</div>
    </div>
</section>
@endsection