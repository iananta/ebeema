<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PremiumController;
use App\Http\Controllers\Api\MoneyBackController;
use App\Http\Controllers\Api\WholelifeController;
use App\Http\Controllers\Api\DhanBristiController;
use App\Http\Controllers\Api\LeadController;
use App\Http\Controllers\Api\ReminderController;
use App\Http\Controllers\Api\LifeCalculatorController;


Route::middleware(['api.auth'])->prefix('lead')->group(function () {
    Route::post('store', [LeadController::class, 'storeLead']);
});
Route::middleware(['api.auth'])->prefix('premium')->group(function () {
    Route::post('check', [ReminderController::class, 'PremiumPayableCheck']);
});

Route::middleware(['api.auth'])->prefix('calculator')->group(function () {

    Route::prefix('moneyback')->group(function () {
        Route::get('create', [MoneyBackController::class, 'create']);
        Route::post('store', [MoneyBackController::class, 'store']);
    });

    Route::prefix('dhanbristi')->group(function () {
        Route::get('create', [DhanBristiController::class, 'create']);
        Route::post('store', [DhanBristiController::class, 'store']);
    });

    Route::prefix('premiumrate')->group(function () {
        Route::post('store', [PremiumController::class, 'store']);
        Route::post('child',[PremiumController::class, 'child']);
        Route::post('childjeevanvidya',[PremiumController::class, 'childjeevanvidya']);
        Route::post('wholelife',[WholelifeController::class, 'store']);
    });
});

//life calculation Api
Route::middleware(['cors'])->prefix('life')->group(function () {
    Route::get('catagories', [LifeCalculatorController::class, 'lifeCalculationCategory']);
    Route::post('calculation', [LifeCalculatorController::class, 'lifeCalculation']);
});

//blogs
Route::middleware(['cors'])->prefix('blogs')->group(function () {
    Route::get('all', [\App\Http\Controllers\Api\BlogsController::class, 'blogs']);
    Route::get('detail/{id}', [\App\Http\Controllers\Api\BlogsController::class, 'detail']);
});

//Fall back route
Route::fallback(function () {
    return Response::json(["response_code" => 0, 'success' => false, "message" => "Page Not Found.You are not allowed here !!!"], 404);
});
