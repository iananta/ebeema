<?php

use App\Http\Controllers\CalculationController;
use App\Http\Controllers\CompareController;
use App\Http\Controllers\CouplePlanCalcController;
use App\Http\Controllers\LifeCalculatorController;
use App\Http\Controllers\LifeClaimController;
use App\Http\Controllers\MoneyBackCalcController;
use Illuminate\Support\Facades\Route;

Route::middleware(['access:life-calculation', 'agent:Life', 'cors', 'api'])->prefix('admin')->group(function () {
    Route::get('money-back', [MoneyBackCalcController::class, 'showCalculationForm'])
        ->name('calculator.moneyBack');
    Route::post('money-back', [MoneyBackCalcController::class, 'calculate'])
        ->name('calculator.moneyBack.calculate');

    Route::get('couple-plan', [CouplePlanCalcController::class, 'showCalculationForm'])
        ->name('calculator.couplePlan');
    Route::post('couple-plan', [CouplePlanCalcController::class, 'calculate'])
        ->name('calculator.couplePlan.calculate');

    Route::get('life', [LifeCalculatorController::class, 'index'])->name('calculator.life.index');
//    Route::post('life', [LifeCalculatorController::class, 'calculate'])->name('calculator.life.calculate');
    Route::post('life', [CalculationController::class, 'lifeCalculation'])->name('calculator.life.calculate');

    Route::get('life/validate', [LifeCalculatorController::class, 'validateData'])->name('calculator.life.validate');

    Route::get('compare', [CompareController::class, 'showCompareForm'])->name('admin.policy.form');
    Route::get('design', function () {
        return view('Backend.Compare.design');
    });
    Route::get('compare/add', [CompareController::class, 'addRow'])->name('admin.policy.add');

    Route::get('compare/find-features', [CompareController::class, 'findFeaturesOfSelectedCategory'])->name('admin.policy.features');
    Route::post('compare/findProductFeture',[CompareController::class,'findProductFeature'])->name('admin.findProduct.feature');
//    Route::post('compare', [CompareController::class, 'compare'])->name('admin.policy.compare');
    Route::get('compare/category', [CalculationController::class, 'getCategoryDetails'])->name('admin.policy.category');
    Route::post('compare', [CalculationController::class, 'backendCompare'])->name('admin.policy.compare');
    Route::get('compare/pdf', [CompareController::class, 'generatePdf'])->name('admin.policy.compare.pdf');
    Route::middleware(['userlogs'])->group(function () {
        Route::get('lifeClaim', [LifeClaimController::class, 'index'])->name('life.claim');
        Route::get('lifeClaim/add', [LifeClaimController::class, 'create'])->name('life.claim.add');
        Route::post('lifeClaim/add', [LifeClaimController::class, 'store'])->name('life.claim.store');
        Route::get('lifeClaim/edit/{id}', [LifeClaimController::class, 'edit'])->name('life.claim.edit');
        Route::post('lifeClaim/edit/{id}', [LifeClaimController::class, 'update'])->name('life.claim.update');
        Route::delete('lifeClaim/{id}', [LifeClaimController::class, 'destroy'])->name('life.claim.destroy');
    });
});


//get all selected plans
Route::middleware(['access:selected-plans', 'cors', 'api'])->prefix('admin')->group(function () {
    Route::get('/selected/plans', [App\Http\Controllers\PlanselectedController::class, 'index'])->name('selected.plans');
});

