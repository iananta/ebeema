<?php

use App\Http\Controllers\Frontend\CompareController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Frontend Table Routes
|--------------------------------------------------------------------------
*/

//HomePage
Route::get('lang/set', [App\Http\Controllers\LocalizationController::class, 'languageChange'])->name('setLanguage');

Route::get('/', [App\Http\Controllers\FrontendController::class, 'home'])->name('frontend.home');
Route::get('/calculator', [App\Http\Controllers\FrontendController::class, 'calculatorScreen'])->name('home.calculator');
//age
Route::post('/benefit', [App\Http\Controllers\FrontendController::class, 'benefits'])->name('home.benefits');

Route::post('/compare', [App\Http\Controllers\FrontendController::class, 'compareScreen'])->name('home.compare');
Route::get('/about', [App\Http\Controllers\FrontendController::class, 'about'])->name('frontend.about');

Route::get('/confirmation/{age}/{term}/{sum}/{mop}/{product}', [App\Http\Controllers\FrontendController::class, 'confirm'])->name('front.confirmation');

Route::post('corporateStore',[App\Http\Controllers\FrontendController::class,'corporateStore'])->name('lead.corporate');

Route::post('/confirm', [App\Http\Controllers\CalculationController::class, 'selectedPlanCalculation'])->name('front.confirm');

Route::get('get/cat/featureList', [App\Http\Controllers\FrontendController::class, 'findcatFeature'])->name('admin.get.category.feature');

Route::post('/plan/selected', [App\Http\Controllers\PlanselectedController::class, 'store'])->name('plan.selected');

Route::post('/plan/status/change', [App\Http\Controllers\PlanselectedController::class, 'updateStatus'])->name('plan.status.change');

//Route::get('/compare/result/demo',[App\Http\Controllers\FrontendController::class,'compareResult'])->name('compare.result.demo');

Route::post('frontend/add/lead', [App\Http\Controllers\Frontend\CompareController::class, 'leadCreate'])->name('frontend.add.lead');

Route::get('term-condition',[App\Http\Controllers\FrontendController::class,'termcondition'])->name('frontend.termcondition');
Route::get('privacy-policy',[App\Http\Controllers\FrontendController::class,'privacyPolicy'])->name('frontend.privacyPolicy');

Route::get('all/blogs',[App\Http\Controllers\FrontendController::class,'blogs'])->name('frontend.blogs');
Route::get('blog/{slug}',[App\Http\Controllers\FrontendController::class,'blogView'])->name('frontend.blog.view');

//contact
Route::get('contact', [App\Http\Controllers\FrontendController::class, 'contact'])->name('contact');
Route::post('contact', [App\Http\Controllers\FrontendController::class, 'contactStore'])->name('contact.message');

/**
 * Non-life Insurance Calculation
 */
Route::prefix('Nonlifeinsurance/calculator')->group(function () {
    Route::get('bike', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'nonlifecalculationForm']) ->name('frontend.nonlife.insurance');
    Route::post('motor', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'calculationMotor'])->name('frontend.nonlife.insurance.bike.calculation');
    Route::get('motors', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'calculationMotorview'])->name('frontend.nonlife.insurance.bike.view');
    Route::get('nonlife/detail', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'nonlifedetail'])->name('fronted.nonlife.details');
    Route::get('payment-view', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'paymentview'])->name('fronted.nonlife.payment');
    Route::post('motor/detail', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'detailmotor'])->name('fronted.bike.details');
    Route::post('/choosecustomer', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'choosecustomer'])->name('bike.customer.choose');
    Route::post('compulsary-excess', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'compulsaryExcess'])->name('motor.first.party.compulsary.excess');
    Route::post('excess-damage', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'excessDamage'])->name('bike.excess.damage');
    Route::post('manufacture-model', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'getModel'])->name('motor.first.party.model.manufacture');
    Route::post('make-draft-policy', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'makeDraftPolicy'])->name('construct.draft.policy');
    Route::get('make-policy', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'makeOnlinePolicy'])->name('motor.construct.policy');
    Route::get('/privatecar', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'carCalculator'])->name('private.car.nonlifeinsurance');
    Route::get('commerical-car', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'commercialCarCalculator'])->name('commerical.car.calculator.nonlifeinsurance');
    Route::get('invoice-pdf', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'loadPdf'])->name('pdf.invoice');
    // After KYC
    Route::post('motor-payment', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'paymentBikeFirstParty'])->name('motor.first.party.payment');
    Route::post('compulsary-excess', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'compulsaryExcess'])->name('bike.first.party.compulsary.excess');
    Route::post('excess-damage', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'excessDamage'])->name('motor.excess.damage');
    Route::get('policy-done', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'policyDone'])->name('policy.done');
    // Route::get('policy-view', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'viewPolicy'])->name('policy.detail.view');
    Route::get('draft-policy-view', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'viewDraftPolicy'])->name('policy.draft.view');
    Route::get('successfull/imepay', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'imePaySuccess'])->name('successfull.imepay');
    Route::get('unsuccessfull/imepay', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'imePayCancel'])->name('canceled.imepay');
    // Route::post('category-type', [App\Http\Controllers\Fronted\NonLifeCalculatorController::class, 'getCatType'])->name('bike.category.type');
    Route::get('debit-note', [App\Http\Controllers\Frontend\NonLifeCalculatorController::class, 'getDebitNote'])->name('bike.get.debit.note');
});

/**
 * Compare Routes
 */
Route::prefix('compare')
    ->name('compare.')
    ->group(function () {
        Route::get('', [CompareController::class, 'showCompareForm'])->name('form');
//        Route::post('result', [CompareController::class, 'compare'])->name('result');
        Route::post('viewplan',[CompareController::class,'viewPlan'])->name('viewplan');
        Route::post('result', [\App\Http\Controllers\CalculationController::class, 'frontendCompare'])->name('result');
    });


