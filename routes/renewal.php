<?php

/*
|--------------------------------------------------------------------------
| Policy Renewal Routes
|--------------------------------------------------------------------------
*/


Route::middleware(['access:policy-renewal', 'cors', 'api'])->prefix('policy/renew')->name('policy.renew.')->group(function () {
    Route::resource('customer', \App\Http\Controllers\PolicyRenewalController::class);
    Route::get('motor/details/{id}',[App\Http\Controllers\PolicyRenewalController::class,'policyDetails'])->name('policy.details');
    Route::post('motor/details/new/rate',[App\Http\Controllers\PolicyRenewalController::class,'motorRateCalculation'])->name('policy.new.rate');
});
