<?php

/*
|--------------------------------------------------------------------------
| Travel Medical Insurance Routes
|--------------------------------------------------------------------------
*/


Route::middleware(['access:travel-calculation', 'agent:NonLife', 'cors', 'api'])->prefix('travel/calculator')->name('travel.calculator.')->group(function () {
    Route::resource('insurance', \App\Http\Controllers\TravelMedicalInsuranceController::class);
    Route::post('insurance/package/select', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'packageSelect'])->name('package.select');
    Route::post('insurance/plans', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'getPlans'])->name('medical.insurance.plans');
    Route::post('insurance/package', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'getPackages'])->name('medical.insurance.package');
    Route::post('insurance/premium', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'getPremium'])->name('medical.insurance.premium');
    Route::post('insurance/customer/select', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'customerSelect'])->name('customer.select');
    Route::post('insurance/customer/payment', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'customerPayment'])->name('insurance.payment');
    Route::get('insurance/customer/clearAll', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'clearAll'])->name('customer.clear.all');
    Route::post('insurance/customer/delete/{id}', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'customerDelete'])->name('customer.delete');
    Route::get('insurance/customer/policy/make', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'MakePolicy'])->name('customer.create.policy');
    Route::get('imepay/success', [App\Http\Controllers\ImePayController::class, 'imePaySuccess'])->name('imepay.success');
    Route::get('imepay/unsuccess', [App\Http\Controllers\ImePayController::class, 'imePayCancil'])->name('imepay.cancil');
    Route::get('policy/done', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'policyDone'])->name('policy.done');
    Route::get('pdf/load', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'loadPdf'])->name('pdf.load');
    Route::get('tmi-policy-view', [App\Http\Controllers\TravelMedicalInsuranceController::class, 'viewTmiPolicy'])->name('tmi.policy.view');
});
