<?php

use App\Http\Controllers\ProductCategoriesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImelifeController;
use App\Http\Controllers\PremiumController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\LoadingChargeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['namespace' => 'Auth'], function () {
    Route::get('/admin', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
    Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login.login');
    Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'authenticate'])->name('auth.login');
    Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
    Route::get('register', [App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('register', [App\Http\Controllers\Auth\RegisterController::class, 'adduser'])->name('user.register');
});
Route::middleware(['userlogs', 'subscription'])->group(function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::middleware(['auth', 'access:create-leads'])->prefix('admin')->name('admin.')->group(function () {
        Route::get('full-calender', [App\Http\Controllers\FullCalenderController::class, 'index'])->name('calender.event');
        Route::get('full-calender-meeting', [App\Http\Controllers\FullCalenderController::class, 'meeting'])->name('calender.meeting');

        Route::post('full-calender/store', [App\Http\Controllers\FullCalenderController::class, 'action']);
        Route::post('full-calender-meeting/store', [App\Http\Controllers\FullCalenderController::class, 'actionmeet']);
    });

    Route::middleware(['auth', 'access:non-life-calculation'])->prefix('admin')->name('admin.')->group(function () {

        Route::get('life/rate/validate', [App\Http\Controllers\LifeCalculatorController::class, 'validateRate'])->name('life.rate.validate');

        Route::get('ime-life-insurance', [ImelifeController::class, 'imelife'])
            ->name('ime.life.insurance');
        Route::post('ime-life-insurance/one', [ImelifeController::class, 'postimeOne'])
            ->name('ime.life.insurance.one');
        Route::get('ime-life-insurance/choose', [ImelifeController::class, 'imelifetwo'])
            ->name('ime.life.two');
        Route::post('ime-life-insurance/two', [ImelifeController::class, 'postimeTwo'])
            ->name('ime.life.insurance.two');
        Route::get('ime-life-insurance/confirm', [ImelifeController::class, 'imelifethree'])
            ->name('ime.life.three');
        Route::post('ime-life-insurance/three', [ImelifeController::class, 'postimeThree'])
            ->name('ime.life.insurance.three');
        Route::get('ime-life-insurance/personal-info', [ImelifeController::class, 'imelifefour'])
            ->name('ime.life.four');
        Route::post('ime-life-insurance/four', [ImelifeController::class, 'postimefour'])
            ->name('ime.life.insurance.four');
        Route::get('ime-life-insurance/details-of-plan', [ImelifeController::class, 'imelifefive'])
            ->name('ime.life.five');
        Route::post('ime-life-insurance/final-confirmation', [ImelifeController::class, 'postimefive'])
            ->name('ime.life.insurance.five');
        Route::get('ime-life-insurance/final-details-of-plan', [ImelifeController::class, 'imelifesix'])
            ->name('ime.life.six');
    });

    Route::middleware(['auth', 'access:create-leads'])->prefix('admin/leadcategories')->name('admin.leadcategories.')->group(function () {
        Route::resource('leadsource', \App\Http\Controllers\LeadSourceController::class)->except('show');
        Route::resource('leadtypes', \App\Http\Controllers\LeadTypeController::class)->except('create', 'show');
    });
    Route::middleware(['auth', 'access:marketing-leads'])->prefix('admin')->name('admin.')->group(function () {
        Route::get('lead/marketing', [App\Http\Controllers\LeadController::class, 'marketingLead'])->name('leads.marketing');
    });
    Route::middleware(['auth', 'access:create-leads'])->prefix('admin')->name('admin.')->group(function () {
        Route::resource('leads', \App\Http\Controllers\LeadController::class);
        Route::get('get-form-convert-to-policy', [App\Http\Controllers\LeadController::class, 'getFormConvertToPolicy'])->name('leads.getformConvertToPolicy');
        Route::post('getspecificLead', [App\Http\Controllers\LeadController::class, 'getspecificLead'])->name('leads.getspecificLead');
        Route::post('lead/add/remark', [App\Http\Controllers\LeadController::class, 'addRemarkLead'])->name('leads.Addremark');
        Route::post('lead/getProduct', [App\Http\Controllers\LeadController::class, 'getProduct'])->name('leads.getProduct');

        Route::post('lead/findUser', [App\Http\Controllers\LeadController::class, 'findUser'])->name('leads.findUser');
        Route::post('lead/document', [App\Http\Controllers\LeadController::class, 'leadDocument'])->name('leads.document');
        Route::get('customers', [App\Http\Controllers\LeadController::class, 'customersList'])->name('leads.customers');
        Route::post('leads/document/delete', [App\Http\Controllers\LeadController::class, 'documentDelete'])->name('leads.documentDelete');
        Route::post('lead/getCity', [App\Http\Controllers\LeadController::class, 'getCity'])->name('leads.getCity');
        Route::get('getCityList/{id}', [App\Http\Controllers\LeadController::class, 'getCityList']);
        Route::post('leads/create/client', [App\Http\Controllers\LeadController::class, 'makeUser'])->name('lead.make.user');
        Route::post('leads/update', [App\Http\Controllers\LeadController::class, 'update'])->name('lead.update');
        Route::get('leads/delete/{id}', [App\Http\Controllers\LeadController::class, 'delete'])->name('leads.delete');

        Route::get('lead/getleadremarks/{id}', [App\Http\Controllers\LeadController::class, 'GetRemarkList'])->name('leads.remarklist');
        Route::post('lead/remarks/', [App\Http\Controllers\LeadController::class, 'remark'])->name('leads.remarks');
        Route::post('lead/meetings/', [App\Http\Controllers\LeadController::class, 'meeting'])->name('leads.meetings');
        Route::get('lead/getleadmeetings/{id}', [App\Http\Controllers\LeadController::class, 'GetMeetingList'])->name('leads.meetinglist');

        Route::get('lead/demo-export', [\App\Http\Controllers\LeadController::class, 'export'])->name('leads.exportDemo');
        Route::post('lead/import', [\App\Http\Controllers\LeadController::class, 'importFile'])->name('leads.import');

        Route::post('premiumPaidByCustomer', [App\Http\Controllers\LeadController::class, 'updatePremiumPaidByCustomer'])->name('leads.updatePremiumPaidByCustomer');
        Route::post('customer-verify', [App\Http\Controllers\LeadController::class, 'customerVerify'])->name('customerVerify');
        Route::get('premium-paid-by-customer', [App\Http\Controllers\PremiumPaidController::class, 'index'])->name('permiumpaid');
        Route::post('get-premium-collection-user', [App\Http\Controllers\PremiumPaidController::class, 'getPremiumCollection'])->name('getPremiumCollection');
        Route::post('premium-verify', [App\Http\Controllers\PremiumPaidController::class, 'premiumVerify'])->name('premiumVerify');
    });

    Route::prefix('admin')->name('admin.')->group(function () {
        Route::get('seo-form', [App\Http\Controllers\SeoFormController::class, 'index'])->name('seoForm');
        Route::get('seo-form-create', [App\Http\Controllers\SeoFormController::class, 'create'])->name('seoForm.create');
        Route::post('seo-form/create', [App\Http\Controllers\SeoFormController::class, 'store'])->name('seoForm.store');
        Route::get('seo-form/edit/{id}', [App\Http\Controllers\SeoFormController::class, 'edit'])->name('seoForm.edit');
        Route::post('seo-form/edit/{id}', [App\Http\Controllers\SeoFormController::class, 'update'])->name('seoForm.update');
        Route::delete('seo-form/delete', [App\Http\Controllers\SeoFormController::class, 'delete'])->name('seoForm.delete');
    });

    Route::middleware(['auth', 'access:create-policy'])->prefix('admin/policycategories')->name('admin.policycategories.')->group(function () {
        Route::resource('sub', \App\Http\Controllers\PolicySubCategoryController::class)->except('create', 'show');
        Route::resource('type', \App\Http\Controllers\PolicyTypeController::class)->except('create', 'show');
    });

    Route::middleware(['auth', 'access:create-product'])->prefix('admin')->name('admin.')->group(function () {
        Route::get('productList', [App\Http\Controllers\ProductController::class, 'index'])->name('product.list');
        Route::get('productDeletedList', [App\Http\Controllers\ProductController::class, 'trashedProductsList'])->name('product.deleted');
        Route::get('productDeletedList/{id}', [App\Http\Controllers\ProductController::class, 'restoreProduct'])->name('product.restore');
        Route::get('productCreate', [App\Http\Controllers\ProductController::class, 'create'])->name('product.create');
        Route::post('productCreate', [App\Http\Controllers\ProductController::class, 'store'])->name('product.store');
        Route::get('productEdit/{id}', [App\Http\Controllers\ProductController::class, 'edit'])->name('product.edit');
        Route::post('productEdit/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('product.update');
        Route::delete('productDelete/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->name('product.destroy');
        Route::patch('product-update-status', [App\Http\Controllers\ProductController::class, 'updateProductStatus'])->name('product.status');

        //categories
        Route::get('category', [ProductCategoriesController::class, 'index'])->name('category');
        Route::get('category/create', [ProductCategoriesController::class, 'create'])->name('category.create');
        Route::post('category/store', [ProductCategoriesController::class, 'store'])->name('category.store');
        Route::get('category/edit/{id}', [ProductCategoriesController::class, 'edit'])->name('category.edit');
        Route::post('category/update/{id}', [ProductCategoriesController::class, 'update'])->name('category.update');
        Route::delete('category/delete/{id}', [ProductCategoriesController::class, 'delete'])->name('category.delete');
        Route::post('category/status/{id}', [ProductCategoriesController::class, 'changeStatus'])->name('category.status');

        //Upload product management pdf
        Route::get('uploadProductFile', [App\Http\Controllers\UploadProductPdfController::class, 'index'])->name('product.uploadFile');
        Route::post('uploadProductFile', [App\Http\Controllers\UploadProductPdfController::class, 'store'])->name('product.storeProductFile');
        
        Route::get('support/create',[App\Http\Controllers\UploadProductPdfController::class,'support'])->name('support.create');
        Route::get('support',[App\Http\Controllers\UploadProductPdfController::class,'form'])->name('support.index');
        Route::post('support/supply', [App\Http\Controllers\UploadProductPdfController::class, 'supply'])->name('support.supply');
        Route::get('support/edit/{id}', [App\Http\Controllers\UploadProductPdfController::class, 'replace'])->name('support.replace');
        Route::post('support/update/{id}', [App\Http\Controllers\UploadProductPdfController::class, 'modernize'])->name('support.modernize');
        Route::get('productFile/{productFileId}/download', [App\Http\Controllers\UploadProductPdfController::class, 'download'])->name('product.downloadProductFile');
        Route::delete('upload/ProductFile/{id}', [App\Http\Controllers\UploadProductPdfController::class, 'destroy'])->name('product.uploadFile.delete');

        //  Upload choosefile with mail
        Route::get('videoupload',[App\Http\Controllers\UploadChooseFileController::class,'index'])->name('videoupload.index');
        Route::get('videoupload/create',[App\Http\Controllers\UploadChooseFileController::class,'create'])->name('videoupload.create');
        Route::post('videoupload/store',[App\Http\Controllers\UploadChooseFileController::class,'store'])->name('videoupload.store');
        Route::get('videoupload/show/{id}', [App\Http\Controllers\UploadChooseFileController::class, 'view'])->name('support.show');
        Route::delete('video/{id}', [App\Http\Controllers\UploadChooseFileController::class, 'destroy'])->name('videoupload.delete');
        // Route::resource('videoupload', \App\Http\Controllers\UploadProductPdfController::class);

        //video
         Route::get('tape',[App\Http\Controllers\VideoController::class,'index'])->name('tape.index');
         Route::get('tape/create',[App\Http\Controllers\VideoController::class,'create'])->name('tape.create'); 
         Route::post('tape/store',[App\Http\Controllers\VideoController::class,'store'])->name('tape.store');
         Route::delete('tape/{id}', [App\Http\Controllers\VideoController::class, 'destroy'])->name('tape.delete');
         Route::get('tape/show/{id}', [App\Http\Controllers\VideoController::class, 'show'])->name('tape.show');
        Route::delete('tape/{id}', [App\Http\Controllers\VideoController::class, 'destroy'])->name('tape.delete');
        // Route::get('upload/create',[App\Http\Controllers\VideoController::class,'create'])->name('upload.create');
        // Route::post('upload/store',[App\Http\Controllers\VideoController::class,'store'])->name('upload.store');
        // Route::delete('video/{id}', [App\Http\Controllers\VideoController::class, 'destroy'])->name('upload.delete');

    });

    Route::middleware(['auth', 'access:create-company'])->prefix('admin')->name('admin.')->group(function () {
        Route::resource('companies', CompaniesController::class);
        Route::post('company-update-status/{id}', [App\Http\Controllers\CompaniesController::class, 'updateCompanyStatus'])->name('company.status');
        Route::get('productView/{id}', [App\Http\Controllers\CompaniesController::class, 'product'])->name('company.product');
        Route::get('company-details/{id}', [App\Http\Controllers\CompaniesController::class, 'companyDetails'])->name('company.details');
        Route::post('company-details/update/{id}', [App\Http\Controllers\CompaniesController::class, 'companyDetailsUpdate'])->name('company.details.update');
    });

    Route::middleware(['auth', 'access:life-calculator-inputs'])->prefix('admin')->name('admin.')->group(function () {
//    crc Rate Routes
        Route::resource('crcrate', \App\Http\Controllers\CrcRateController::class);
        Route::delete('crcrate/delete/{id}', [App\Http\Controllers\CrcRateController::class, 'destroy'])->name('crcrate.delete');
//    Discount on SA Routes
        Route::get('discountList', [App\Http\Controllers\SumAssuredController::class, 'index'])->name('discount.list');
        Route::get('discountCreate', [App\Http\Controllers\SumAssuredController::class, 'create'])->name('discount.create');
        Route::post('discountCreate', [App\Http\Controllers\SumAssuredController::class, 'store'])->name('discount.store');
        Route::get('discountEdit/{id}', [App\Http\Controllers\SumAssuredController::class, 'edit'])->name('discount.edit');
        Route::post('discountEdit/{id}', [App\Http\Controllers\SumAssuredController::class, 'update'])->name('discount.update');
        Route::delete('discountList/{id}', [App\Http\Controllers\SumAssuredController::class, 'destroy'])->name('discount.destroy');

//    Bonus Routes
        Route::get('bonusList', [App\Http\Controllers\BonusRateEPController::class, 'index'])->name('bonus.list');
        Route::get('bonusCreate', [App\Http\Controllers\BonusRateEPController::class, 'create'])->name('bonus.create');
        Route::post('bonusCreate', [App\Http\Controllers\BonusRateEPController::class, 'store'])->name('bonus.store');
        Route::get('bonusEdit/{id}', [App\Http\Controllers\BonusRateEPController::class, 'edit'])->name('bonus.edit');
        Route::post('bonusEdit/{id}', [App\Http\Controllers\BonusRateEPController::class, 'update'])->name('bonus.update');
        Route::delete('bonusList/delete/{id}', [App\Http\Controllers\BonusRateEPController::class, 'destroy'])->name('bonus.delete');
//    Mode of Payment Routes
        Route::resource('loadingcharges', LoadingChargeController::class);
        Route::delete('loadingcharges/delete/{id}', [LoadingChargeController::class, 'destroy']);
//    Payback Schedule Routes
        Route::get('paybackSchedule', [\App\Http\Controllers\PaybackScheduleController::class, 'index'])->name('payback.index');
        Route::get('paybackSchedule/create', [\App\Http\Controllers\PaybackScheduleController::class, 'create'])->name('payback.create');
        Route::post('paybackSchedule/create', [\App\Http\Controllers\PaybackScheduleController::class, 'store'])->name('payback.store');
        Route::get('paybackSchedule/edit/{id}', [\App\Http\Controllers\PaybackScheduleController::class, 'edit'])->name('payback.edit');
        Route::post('paybackSchedule/edit/{id}', [\App\Http\Controllers\PaybackScheduleController::class, 'update'])->name('payback.update');
        Route::get('paybackSchedule/delete/{id}', [\App\Http\Controllers\PaybackScheduleController::class, 'destroy'])->name('payback.delete');
        Route::delete('paybackSchedule/delete/{id}', [\App\Http\Controllers\PaybackScheduleController::class, 'destroy'])->name('payback.destroy');
//    Couples Age Difference Routes
        Route::resource('age-difference', \App\Http\Controllers\CoupleAgeDifferenceController::class);
//    Policy Paying Term Routes
        Route::resource('paying-term', \App\Http\Controllers\PayingTermController::class);
//  Api Key Routes
        Route::resource('api-key', \App\Http\Controllers\ApiKeyController::class);
        // Feature Routes
        Route::get('get/featureList', [App\Http\Controllers\LifeCalculatorController::class, 'findFeature'])->name('get.feature.list');

        Route::get('featureList', [App\Http\Controllers\FeatureController::class, 'index'])->name('feature.list');
        Route::get('featureList', [App\Http\Controllers\FeatureController::class, 'index'])->name('feature.list');
        Route::get('featureList', [App\Http\Controllers\FeatureController::class, 'index'])->name('feature.list');
        Route::get('featureCreate', [App\Http\Controllers\FeatureController::class, 'create'])->name('feature.create');
        Route::post('featureCreate', [App\Http\Controllers\FeatureController::class, 'store'])->name('feature.store');
        Route::get('featureEdit/{id}', [App\Http\Controllers\FeatureController::class, 'edit'])->name('feature.edit');
        Route::post('featureEdit/{id}', [App\Http\Controllers\FeatureController::class, 'update'])->name('feature.update');
        Route::delete('featureList/{id}', [App\Http\Controllers\FeatureController::class, 'destroy'])->name('feature.destroy');

        Route::get('featureproductList', [App\Http\Controllers\FeatureController::class, 'fpindex'])->name('feature.product');
        Route::get('featureproductCreate', [App\Http\Controllers\FeatureController::class, 'fpcreate'])->name('feature.product.create');
        Route::post('featureproductCreate', [App\Http\Controllers\FeatureController::class, 'fpstore'])->name('feature.product.store');
        Route::get('featureproEdit/{id}', [App\Http\Controllers\FeatureController::class, 'fpedit'])->name('feature.product.edit');
        Route::post('featureproEdit/{id}', [App\Http\Controllers\FeatureController::class, 'fpupdate'])->name('feature.product.update');
        Route::get('featureproList/{id}', [App\Http\Controllers\FeatureController::class, 'fpdestroy'])->name('feature.product.destroy');
    });

    Route::middleware(['auth', 'access:frontend-dynamics'])->prefix('admin')->name('')->group(function () {
//    Homepage Routes
        Route::resource('why-us', \App\Http\Controllers\WhyUsController::class);
        Route::resource('why-different', \App\Http\Controllers\WhyDifferentController::class);
        Route::resource('blogs', \App\Http\Controllers\BlogController::class);
        Route::resource('blogcates', \App\Http\Controllers\BlogcateController::class);
        Route::delete('blogcates/{id}', [\App\Http\Controllers\BlogcateController::class, 'destroy'])->name('blogcates.delete');
        Route::resource('testimonial', \App\Http\Controllers\TestimonialController::class);
        Route::post('testimonial/status/{id}', [\App\Http\Controllers\TestimonialController::class, 'statusChange'])->name('testimonial.change.status');
        Route::resource('association', \App\Http\Controllers\AssociationController::class);
        Route::post('association/status/{id}', [\App\Http\Controllers\AssociationController::class, 'statusChange'])->name('association.change.status');
//  About Us Routes
        Route::resource('about-us', \App\Http\Controllers\AboutUsController::class);
        Route::resource('values', \App\Http\Controllers\ValuesController::class);
        Route::resource('team', \App\Http\Controllers\TeamController::class);
        Route::get('team/status/{id}', [\App\Http\Controllers\TeamController::class, 'statusChange'])->name('team.change.status');
        Route::post('team/sort', [\App\Http\Controllers\TeamController::class, 'sort'])->name('team.sort');

//    Contact Us Routes
        Route::get('messages', [\App\Http\Controllers\MessagesController::class, 'index'])->name('message.index');
        Route::get('messages/show/{id}', [\App\Http\Controllers\MessagesController::class, 'show'])->name('message.show');
        Route::post('messages/update/{id}', [\App\Http\Controllers\MessagesController::class, 'update'])->name('message.update');
        Route::delete('messages/delete/{id}', [\App\Http\Controllers\MessagesController::class, 'delete'])->name('message.delete');
        Route::get('general-setting/contact', [\App\Http\Controllers\GeneralSettingController::class, 'contactList'])->name('general-setting.contact');
        Route::get('general-setting/contact/create', [\App\Http\Controllers\GeneralSettingController::class, 'contactCreate'])->name('general-setting.contact.create');
        Route::get('general-setting/contact/edit/{id}', [\App\Http\Controllers\GeneralSettingController::class, 'contactEdit'])->name('general-setting.contact.edit');
//    social Media Link Routes
        Route::resource('social-link', \App\Http\Controllers\SocialLinkController::class);
        Route::post('social/status/{id}', [\App\Http\Controllers\SocialLinkController::class, 'changeStatus'])->name('social.status');
        //user settings routes
        Route::resource('page-setting', \App\Http\Controllers\UserSettingController::class);

    });

    Route::middleware(['auth', 'access:page-seo-settings'])->prefix('admin')->name('')->group(function () {
//        Meta Data Routes
        Route::resource('meta', \App\Http\Controllers\MetaDataController::class);
        Route::post('meta/status/{id}', [App\Http\Controllers\MetaDataController::class, 'changeStatus'])->name('meta.status');

    });

    Route::middleware(['auth', 'access:general-settings'])->prefix('admin')->name('')->group(function () {
        Route::resource('general-setting', \App\Http\Controllers\GeneralSettingController::class);
        Route::resource('sms', \App\Http\Controllers\SmsResponseController::class);
    });

    Route::get('/admin/privilege/permission_roles/getpermission_rolesJson', [App\Http\Controllers\PermissionRoleController::class, 'getpermission_rolesJson'])->name('permission_roles.getdatajson');
    Route::middleware('access:create-user')->prefix('admin/privilege')->name('admin.privilege.')->group(function () {
        Route::resource('permission', \App\Http\Controllers\PermissionController::class);
        Route::resource('permission_roles', \App\Http\Controllers\PermissionRoleController::class);
        Route::resource('role', \App\Http\Controllers\RoleController::class);
        Route::resource('user', \App\Http\Controllers\UserController::class);
        Route::put('user/change/status/{user}', ['as' => 'user.change.status', 'uses' => '\App\Http\Controllers\UserController@changeStatus']);
        Route::get('/login/user/{id}', [\App\Http\Controllers\UserController::class, 'manuallogin'])->name('login.as.user');
        Route::get('/user/list/excel', [\App\Http\Controllers\UserController::class, 'downloadExcel'])->name('user.excel');
        Route::get('/user/companies/{id}', [\App\Http\Controllers\UserController::class, 'showCompanies'])->name('user.companies');
        Route::post('/user/companies/{id}', [\App\Http\Controllers\UserController::class, 'storeCompanies'])->name('user.companies.store');
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/dashboard', [\App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
        Route::post('filter/revenue', [App\Http\Controllers\DashboardController::class, 'filterRevenue'])->name('filter.revenue');
        Route::post('follow.update', [\App\Http\Controllers\DashboardController::class, 'followUpdate'])->name('follow.update');
        Route::get('/recentlyaddCustomer', [App\Http\Controllers\DashboardController::class, 'getrecentlyCustomer'])->name('getrecentlyaddCustomer');
        Route::get('/followupList', [App\Http\Controllers\DashboardController::class, 'getfollowupList'])->name('getfollowupList');
        Route::get('/lifeLeadershipList', [App\Http\Controllers\DashboardController::class, 'getLifeLeadershipList'])->name('getLifeLeadershipList');
        Route::get('/nonlifeLeadershipList', [App\Http\Controllers\DashboardController::class, 'getNonlifeLeaderList'])->name('getNonlifeLeadershipList');
        Route::get('/customer-kyc/{id}', [\App\Http\Controllers\KYCController::class, 'customerKyc'])->name('customer.kyc');
    });

    Route::middleware(['access:non-life-calculator-settings', 'agent:NonLife', 'cors', 'api'])->prefix('motor/manufacture/company')->name('motor.manufacture.company.')->group(function () {
        //manufactures
        Route::get('manufactures', [App\Http\Controllers\MotorManufactureCompanyController::class, 'index'])->name('list');
        Route::post('manufacture/store', [App\Http\Controllers\MotorManufactureCompanyController::class, 'store'])->name('list.store');
        Route::post('manufacture/update', [App\Http\Controllers\MotorManufactureCompanyController::class, 'update'])->name('list.update');
        Route::post('manufacture/delete/{id}', [App\Http\Controllers\MotorManufactureCompanyController::class, 'destroy'])->name('list.delete');
        Route::get('manufacture/status/{id}', [App\Http\Controllers\MotorManufactureCompanyController::class, 'statusChange'])->name('list.status');

        //Models
        Route::get('models', [App\Http\Controllers\MotorManufactureCompanyModelController::class, 'index'])->name('models');
        Route::post('model/excel/upload', [App\Http\Controllers\MotorManufactureCompanyModelController::class, 'uploadExcel'])->name('model.upload.excel');
        Route::post('model/delete/{id}', [App\Http\Controllers\MotorManufactureCompanyModelController::class, 'destroy'])->name('model.delete');
        Route::get('model/status/{id}', [App\Http\Controllers\MotorManufactureCompanyModelController::class, 'statusChange'])->name('model.status');
    });
    Route::middleware(['access:non-life-calculation', 'agent:NonLife', 'cors', 'api'])->prefix('nonLife/calculator')->name('nonLife.calculator.')->group(function () {
        Route::get('motor', [App\Http\Controllers\NonLifeCalculatorController::class, 'bikeCalculator'])->name('bike');
        Route::get('private-vehicle', [App\Http\Controllers\NonLifeCalculatorController::class, 'carCalculator'])->name('private.motor');
        Route::get('commercial-vehicle', [App\Http\Controllers\NonLifeCalculatorController::class, 'commercialCarCalculator'])->name('commercial.motor');

        Route::post('motor', [App\Http\Controllers\NonLifeCalculatorController::class, 'calculationMotor'])->name('motor.premium.calculate');
        Route::post('update-motor-info', [App\Http\Controllers\NonLifeCalculatorController::class, 'updateMotorInfo'])->name('update.motor.info');
        Route::post('customer/select', [App\Http\Controllers\NonLifeCalculatorController::class, 'selectCustomer'])->name('motor.customer.select');
        Route::get('motor-payment', [App\Http\Controllers\NonLifeCalculatorController::class, 'motorPayment'])->name('motor.payment');
        Route::post('compulsary-excess', [App\Http\Controllers\NonLifeCalculatorController::class, 'compulsaryExcess'])->name('motor.compulsary.excess');
        Route::post('excess-damage', [App\Http\Controllers\NonLifeCalculatorController::class, 'excessDamage'])->name('motor.excess.damage');
        Route::post('manufacture-model', [App\Http\Controllers\NonLifeCalculatorController::class, 'getModel'])->name('motor.manufacture.model');
        Route::post('check-policy-payment', [App\Http\Controllers\NonLifeCalculatorController::class, 'checkPolicyPayment'])->name('check.policy.payment');
        Route::post('make-draft-policy', [App\Http\Controllers\NonLifeCalculatorController::class, 'makeDraftPolicy'])->name('make.draft.policy');
        Route::get('make-policy', [App\Http\Controllers\NonLifeCalculatorController::class, 'makeOnlinePolicy'])->name('motor.make.policy');
        Route::get('pdf-view', [App\Http\Controllers\NonLifeCalculatorController::class, 'loadPdf'])->name('pdf.load');
        Route::get('document-upload', [App\Http\Controllers\NonLifeCalculatorController::class, 'documentUpload'])->name('document.upload');
        Route::get('policy-details', [App\Http\Controllers\NonLifeCalculatorController::class, 'policyDone'])->name('policy.done');

        Route::get('policy-view', [App\Http\Controllers\NonLifeCalculatorController::class, 'viewPolicy'])->name('policy.view');
        Route::get('draft-policy-view', [App\Http\Controllers\NonLifeCalculatorController::class, 'viewDraftPolicy'])->name('draft.policy.view');

        Route::get('imepay/success', [App\Http\Controllers\NonLifeCalculatorController::class, 'imePaySuccess'])->name('imepay.success');
        Route::get('imepay/unsuccess', [App\Http\Controllers\NonLifeCalculatorController::class, 'imePayCancil'])->name('imepay.cancil');

        Route::get('payment/success', [App\Http\Controllers\NonLifeCalculatorController::class, 'paymentSuccess'])->name('payment.success');
        Route::get('payment/unsuccess', [App\Http\Controllers\NonLifeCalculatorController::class, 'paymentCancil'])->name('payment.cancil');

        Route::get('esewa/success', [App\Http\Controllers\EsewaPaymetController::class, 'successTransaction'])->name('esewa.success');
        Route::get('esewa/failed', [App\Http\Controllers\EsewaPaymetController::class, 'failedTransaction'])->name('esewa.failed');

        Route::post('category-type', [App\Http\Controllers\NonLifeCalculatorController::class, 'getCatType'])->name('motor.category.type');
        Route::get('debit-note', [App\Http\Controllers\NonLifeCalculatorController::class, 'getDebitNote'])->name('motor.make.debit.note');
        Route::post('status-change', [App\Http\Controllers\NonLifeCalculatorController::class, 'updateStatus'])->name('motor.policy.status.change');
        Route::get('claim-kyc', [App\Http\Controllers\NonLifeCalculatorController::class, 'claimByKyc'])->name('claim.kyc');
        Route::get('claim-status', [App\Http\Controllers\NonLifeCalculatorController::class, 'claimStatus'])->name('claim.status');
        Route::get('claim-assessment', [App\Http\Controllers\NonLifeCalculatorController::class, 'claimAssessmentSummary'])->name('claim.assessment');
        Route::get('claim-survey', [App\Http\Controllers\NonLifeCalculatorController::class, 'claimSurveyorData'])->name('claim.survey');
        Route::get('claim-intimate', [App\Http\Controllers\NonLifeCalculatorController::class, 'intimateClaim'])->name('claim.intimate');
        Route::get('currency-exchange', [App\Http\Controllers\NonLifeCalculatorController::class, 'foreignCurrencyExchange'])->name('currency.exchange');
        Route::get('intimate-list', [App\Http\Controllers\NonLifeCalculatorController::class, 'claimIntimationList'])->name('intimate.list');
        Route::get('claim-doc', [App\Http\Controllers\NonLifeCalculatorController::class, 'claimDocumentData'])->name('claim.doc');
        Route::get('motor-list', [App\Http\Controllers\NonLifeCalculatorController::class, 'motorDocList'])->name('motor.list');
        Route::get('motor-info', [App\Http\Controllers\NonLifeCalculatorController::class, 'motorInfo'])->name('motor.info');
        Route::get('proforma', [App\Http\Controllers\NonLifeCalculatorController::class, 'proformaDetails'])->name('proforma');
        Route::get('proforma-payment', [App\Http\Controllers\NonLifeCalculatorController::class, 'makePayment'])->name('proforma.payment');
        Route::get('policy-kyc', [App\Http\Controllers\NonLifeCalculatorController::class, 'policyByKYC'])->name('policy.kyc');

    });
    Route::middleware('auth')->prefix('user/kyc')->group(function () {
        Route::get('entry', [App\Http\Controllers\KYCController::class, 'index'])->name('user.kyc.entry');
        Route::post('entry', [App\Http\Controllers\KYCController::class, 'store'])->name('user.kyc.store');
        Route::post('get-district', [App\Http\Controllers\KYCController::class, 'getDistrict'])->name('province.district');
        Route::post('get-mnuvdc', [App\Http\Controllers\KYCController::class, 'getMnuVdc'])->name('district.mnuvdc');
    });


    Route::get('user/profile/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('user.profile.edit');
    Route::post('user/profile/update/{id}', [App\Http\Controllers\UserController::class, 'modify'])->name('user.profile.modify');

    Route::fallback(function () {
        return response(view('fallback'), 404);
//     return view('fallback');
    });

    Route::middleware('auth')->prefix('user')->name('user.')->group(function () {
        Route::get('calendar', [App\Http\Controllers\CalendarController::class, 'index'])->name('calendar.index');
        Route::get('calendar/view', [App\Http\Controllers\CalendarController::class, 'calendar']);
        Route::get('/calendar/new/event', [App\Http\Controllers\CalendarController::class, 'getNewEventForm'])->name('calendar.new.event');
        Route::post('/calendar/new/event', [App\Http\Controllers\CalendarController::class, 'createNewEvent'])->name('calendar.new.event.store');
        Route::post('/calendar/event/delete', [App\Http\Controllers\CalendarController::class, 'delete'])->name('calendar.event.delete');
        Route::post('/calendar/details/user', [App\Http\Controllers\CalendarController::class, 'outlookDetails'])->name('outlook.details');
    });

    Route::middleware('auth')->prefix('notifications')->group(function () {
        Route::get('/', [App\Http\Controllers\CalendarController::class, 'index'])->name('notifications.index');
    });

//UserActivity
    Route::get('activity/user', [App\Http\Controllers\LogController::class, 'userlogs'])->name('user.activity');
    Route::get('activity-logs', [App\Http\Controllers\LogController::class, 'userlogNotification'])->name('user.activity.logs');
    Route::get('notification-list', [App\Http\Controllers\LogController::class, 'notificationList'])->name('user.notification.list');

    Route::get('/signin', [App\Http\Controllers\AuthController::class, 'signin']);
    Route::get('/callback', [App\Http\Controllers\AuthController::class, 'callback']);
    Route::get('/signout', [App\Http\Controllers\AuthController::class, 'signout']);

    /* if (\App\Models\GeneralSetting::where('key', 'maintainance_mode')->first()->value == 1){
        Route::get('{any}', function () {
            return view('frontend.maintainance');
        })->where('any', '.*');
    }*/

    Route::get('notifications', [App\Http\Controllers\NotificationController::class, 'index'])->name('user.notifications');
    Route::post('notifications/read-all', [App\Http\Controllers\NotificationController::class, 'readAll'])->name('user.notifications.read');

    Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {
        //mail template routes
        Route::resource('mail', \App\Http\Controllers\MailTemplateController::class);
        Route::post('mail/status/{id}', [App\Http\Controllers\MailTemplateController::class, 'changeStatus'])->name('mail.status');
        Route::get('mail/reset/{id}', [App\Http\Controllers\MailTemplateController::class, 'resetMail'])->name('mail.reset');
        //sms template routes
        Route::resource('smsTemplate', \App\Http\Controllers\SMSTemplateController::class);
        Route::post('smsTemplate/status/{id}', [App\Http\Controllers\SMSTemplateController::class, 'changeStatus'])->name('smsTemplate.status');
        Route::get('smsTemplate/reset/{id}', [App\Http\Controllers\SMSTemplateController::class, 'resetMail'])->name('smsTemplate.reset');
        Route::get('smsSend/{id}', [\App\Http\Controllers\SMSTemplateController::class, 'smsSend'])->name('smsSend');
        Route::post('smsSend/{id}', [\App\Http\Controllers\SMSTemplateController::class, 'smsToCustomer'])->name('smsSend.customer');
    });

    require_once('calculator.php');
    //banner routes
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::get('banner', [App\Http\Controllers\BannerController::class, 'index'])->name('banner.index');
        Route::get('banner/create', [App\Http\Controllers\BannerController::class, 'create'])->name('banner.create');
        Route::post('banner/create', [App\Http\Controllers\BannerController::class, 'store'])->name('banner.store');
        Route::get('banner/edit/{id}', [App\Http\Controllers\BannerController::class, 'edit'])->name('banner.edit');
        Route::post('banner/edit/{id}', [App\Http\Controllers\BannerController::class, 'update'])->name('banner.update');
        Route::get('banner/delete/{id}', [App\Http\Controllers\BannerController::class, 'delete'])->name('banner.delete');
    });
});
Route::get('subscription/plan', [App\Http\Controllers\SubscriptionController::class, 'index'])->name('subscription.plan');
Route::post('subscription/invoice', [App\Http\Controllers\SubscriptionController::class, 'subscriptionInvoice'])->name('subscription.invoice')->middleware('auth');
Route::get('subscription/payment/success', [App\Http\Controllers\SubscriptionController::class, 'paymentSuccess'])->name('subscription.payment');
Route::get('subscription/cancle', [App\Http\Controllers\SubscriptionController::class, 'canclePayment'])->name('subscription.cancel');

//Route::middleware('auth')->group(function () {
//
//    Route::get('premium/child/create', [App\Http\Controllers\PremiumController::class, 'child'])->name('premium.child.create');
//    Route::post('premium/child', [App\Http\Controllers\PremiumController::class, 'childpost'])->name('premium.child.store');
//    Route::get('premium/child/jeevan', [App\Http\Controllers\PremiumController::class, 'childvidya'])->name('premium.child.jeevan');
//    Route::post('premium/child/jeevanvidya', [App\Http\Controllers\PremiumController::class, 'childjeevanvidya'])->name('premium.child.jeevan.vidya');
//
//    Route::get('dhanBristi', [\App\Http\Controllers\DhanBristiController::class, 'create'])->name('admin.dhanBristi.create');
//    Route::post('dhanBristi', [App\Http\Controllers\DhanBristiController::class, 'store'])->name('admin.dhanBristi.store');
//
//    Route::get('wholelife', [\App\Http\Controllers\WholelifeController::class, 'create'])->name('admin.Wholelife.create');
//    Route::post('wholelife', [App\Http\Controllers\WholelifeController::class, 'store'])->name('admin.wholelife.store');
//
//});
//
//Route::resources([
//    'premium' => PremiumController::class,
//    'benefit' => \App\Http\Controllers\BenefitController::class,
//]);
